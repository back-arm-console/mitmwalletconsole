package com.nirvasoft.rp.framework;

import java.util.ArrayList;
import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ResultTwo {

	private double amount = 0.00;

	private boolean state = false;
	private String merchantID = "";
	private String userId = "";
	private String msgCode = "";
	private String msgDesc = "";
	private String Keyst = "";
	private String keyString = "";
	private long keyResult = 0;
	private String loginID = ""; // Login ID
	private String phNo = "";
	private String sessionID = "";
	private String parentID = "";
	private String bankName = "";
	private ArrayList<Long> longResult = new ArrayList<Long>();
	private ArrayList<String> stringResult = new ArrayList<String>();

	private long[] key;

	public ResultTwo() {
		clearProperties();
	}

	private void clearProperties() {
		state = false;
		merchantID = "";
		userId = "";
		msgCode = "";
		msgDesc = "";
		keyResult = 0;

		longResult = new ArrayList<Long>();
		stringResult = new ArrayList<String>();
	}

	public double getAmount() {
		return amount;
	}

	public String getBankName() {
		return bankName;
	}

	public long[] getKey() {
		return key;
	}

	public long getKeyResult() {
		return keyResult;
	}

	public String getKeyst() {
		return Keyst;
	}

	public String getKeyString() {
		return keyString;
	}

	public String getLoginID() {
		return loginID;
	}

	public ArrayList<Long> getLongResult() {
		return longResult;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public String getParentID() {
		return parentID;
	}

	public String getPhNo() {
		return phNo;
	}

	public String getSessionID() {
		return sessionID;
	}

	public ArrayList<String> getStringResult() {
		return stringResult;
	}

	public String getUserId() {
		return userId;
	}

	public boolean isState() {
		return state;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public void setKey(long[] key) {
		this.key = key;
	}

	public void setKeyResult(long keyResult) {
		this.keyResult = keyResult;
	}

	public void setKeyst(String keyst) {
		Keyst = keyst;
	}

	public void setKeyString(String keyString) {
		this.keyString = keyString;
	}

	public void setLoginID(String loginID) {
		this.loginID = loginID;
	}

	public void setLongResult(ArrayList<Long> longResult) {
		this.longResult = longResult;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public void setParentID(String parentID) {
		this.parentID = parentID;
	}

	public void setPhNo(String phNo) {
		this.phNo = phNo;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public void setStringResult(ArrayList<String> stringResult) {
		this.stringResult = stringResult;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "Result [amount=" + amount + ", state=" + state + ", merchantID=" + merchantID + ", userId=" + userId
				+ ", msgCode=" + msgCode + ", msgDesc=" + msgDesc + ", Keyst=" + Keyst + ", keyString=" + keyString
				+ ", keyResult=" + keyResult + ", loginID=" + loginID + ", phNo=" + phNo + ", sessionID=" + sessionID
				+ ", parentID=" + parentID + ", bankName=" + bankName + ", longResult=" + longResult + ", stringResult="
				+ stringResult + ", key=" + Arrays.toString(key) + "]";
	}

}
