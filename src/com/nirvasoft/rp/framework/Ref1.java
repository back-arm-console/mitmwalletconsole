package com.nirvasoft.rp.framework;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Ref1 {

	private String value;
	private String caption;

	public Ref1() {
		clearProperty();
	}

	void clearProperty() {
		value = "";
		caption = "";
	}

	public String getCaption() {
		return caption;
	}

	public String getValue() {
		return value;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
