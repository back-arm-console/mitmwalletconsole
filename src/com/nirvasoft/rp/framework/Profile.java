package com.nirvasoft.rp.framework;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

import com.nirvasoft.rp.shared.DepositAccData;

@XmlRootElement
public class Profile {
	private String userID;
	private String password;
	private String sessionID;
	private String code;
	private String desc;
	private String passcode;
	private String username;
	private String domain;
	private String logotext;
	private String otp;
	private String logolink;
	private int userType;
	private long role;
	private boolean commandcenter;
	private Menu[] menus;
	private Menu[] rightmenus;
	private DepositAccData[] debitacc;
	private int loginStatus;
	private long syskey;

	private String t1;

	private String t2;

	private String t3;

	private String t4;

	private String t5;

	private String t6;

	private double n1;

	private double n2;

	private double n3;
	private String iv;
	private String dm;
	private String salt;

	public Profile() {
		username = "";
		logotext = "DC";
		logolink = "Welcome";
		otp = "000000";
		role = 99;
		sessionID = "";
		code = "";
		desc = "";
		commandcenter = true;
		// standard menu
		menus = new Menu[5];
		Menu obj = new Menu();
		obj.setMenuItem("Menu01");
		obj.setCaption("Customers");
		menus[0] = obj;
		obj = new Menu();
		obj.setMenuItem("Menu02");
		obj.setCaption("Services");
		menus[1] = obj;
		obj = new Menu();
		obj.setMenuItem("Menu03");
		obj.setCaption("Products");
		menus[2] = obj;
		obj = new Menu();
		obj.setMenuItem("");
		obj.setCaption("Sub Menu");
		Menu[] menuitems = new Menu[3];
		Menu subobj = new Menu();
		subobj.setMenuItem("Menu01");
		subobj.setCaption("AAA");
		menuitems[0] = subobj;
		subobj = new Menu();
		subobj.setMenuItem("Menu02");
		subobj.setCaption("BBB");
		menuitems[1] = subobj;
		subobj = new Menu();
		subobj.setMenuItem("Menu03");
		subobj.setCaption("CCC");
		menuitems[2] = subobj;
		obj.setMenuItems(menuitems);
		menus[3] = obj;
		rightmenus = new Menu[2];
		obj = new Menu();
		obj.setMenuItem("");
		obj.setCaption("Settings");
		menuitems = new Menu[3];
		subobj = new Menu();
		subobj.setMenuItem("Menu01");
		subobj.setCaption("AAA");
		menuitems[0] = subobj;
		subobj = new Menu();
		subobj.setMenuItem("Menu02");
		subobj.setCaption("BBB");
		menuitems[1] = subobj;
		subobj = new Menu();
		subobj.setMenuItem("Menu03");
		subobj.setCaption("CCC");
		menuitems[2] = subobj;
		obj.setMenuItems(menuitems);
		rightmenus[0] = obj;

		obj = new Menu();
		obj.setMenuItem("Login");
		obj.setCaption("Sign Out");
		rightmenus[1] = obj;
		DepositAccData[] accList = new DepositAccData[1];
		DepositAccData accobj = new DepositAccData();
		accobj.setAvlbal(1000);
		accobj.setCcy("MMK");
		accobj.setDepositacc("00730100702451801");
		accList[0] = accobj;
		loginStatus = 0;
		iv = "";
		dm = "";
		salt = "";

	}

	public String getCode() {
		return code;
	}

	public boolean getCommandCenter() {
		return commandcenter;
	}

	public DepositAccData[] getDebitAcc() {
		return debitacc;
	}

	public String getDesc() {
		return desc;
	}

	public String getDm() {
		return dm;
	}

	public String getDomain() {
		return domain;
	}

	public String getIv() {
		return iv;
	}

	public int getLoginStatus() {
		return loginStatus;
	}

	public String getLogoLink() {
		return logolink;
	}

	public String getLogoText() {
		return logotext;
	}

	public Menu[] getMenus() {
		return menus;
	}

	public double getN1() {
		return n1;
	}

	public double getN2() {
		return n2;
	}

	public double getN3() {
		return n3;
	}

	public String getOtp() {
		return otp;
	}

	public String getPasscode() {
		return passcode;
	}

	public String getPassword() {
		return password;
	}

	public Menu[] getRightMenus() {
		return rightmenus;
	}

	public long getRole() {
		return role;
	}

	public String getSalt() {
		return salt;
	}

	public String getSessionID() {
		return sessionID;
	}

	public long getSysKey() {
		return syskey;
	}

	public String getT1() {
		return t1;
	}

	public String getT2() {
		return t2;
	}

	public String getT3() {
		return t3;
	}

	public String getT4() {
		return t4;
	}

	public String getT5() {
		return t5;
	}

	public String getT6() {
		return t6;
	}

	public String getUserID() {
		return userID;
	}

	public String getUserName() {
		return username;
	}

	public int getUserType() {
		return userType;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setCommandCenter(boolean p) {
		this.commandcenter = p;
	}

	public void setDebitAcc(DepositAccData[] p) {
		this.debitacc = p;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setDm(String dm) {
		this.dm = dm;
	}

	public void setDomain(String p) {
		this.domain = p;
	}

	public void setIv(String iv) {
		this.iv = iv;
	}

	public void setLoginStatus(int loginStatus) {
		this.loginStatus = loginStatus;
	}

	public void setLogoLink(String p) {
		this.logolink = p;
	}

	public void setLogoText(String p) {
		this.logotext = p;
	}

	public void setMenus(Menu[] p) {
		this.menus = p;
	}

	public void setN1(double p) {
		this.n1 = p;
	}

	public void setN2(double p) {
		this.n2 = p;
	}

	public void setN3(double p) {
		this.n3 = p;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public void setPasscode(String p) {
		this.passcode = p;
	}

	public void setPassword(String p) {
		this.password = p;
	}

	public void setRightMenus(Menu[] p) {
		this.rightmenus = p;
	}

	public void setRole(long p) {
		this.role = p;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setSysKey(long p) {
		this.syskey = p;
	}

	public void setT1(String p) {
		this.t1 = p;
	}

	public void setT2(String p) {
		this.t2 = p;
	}

	public void setT3(String p) {
		this.t3 = p;
	}

	public void setT4(String p) {
		this.t4 = p;
	}

	public void setT5(String p) {
		this.t5 = p;
	}

	public void setT6(String p) {
		this.t6 = p;
	}

	public void setUserID(String p) {
		this.userID = p;
	}

	public void setUserName(String p) {
		this.username = p;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	@Override
	public String toString() {
		return "Profile [userID=" + userID + ", password=" + password + ", sessionID=" + sessionID + ", code=" + code
				+ ", desc=" + desc + ", passcode=" + passcode + ", username=" + username + ", domain=" + domain
				+ ", logotext=" + logotext + ", otp=" + otp + ", logolink=" + logolink + ", userType=" + userType
				+ ", role=" + role + ", commandcenter=" + commandcenter + ", menus=" + Arrays.toString(menus)
				+ ", rightmenus=" + Arrays.toString(rightmenus) + ", debitacc=" + Arrays.toString(debitacc)
				+ ", loginStatus=" + loginStatus + ", syskey=" + syskey + ", t1=" + t1 + ", t2=" + t2 + ", t3=" + t3
				+ ", t4=" + t4 + ", t5=" + t5 + ", t6=" + t6 + ", n1=" + n1 + ", n2=" + n2 + ", n3=" + n3 + "]";
	}

}
