package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.rp.framework.Lov3;
import com.nirvasoft.rp.framework.Ref;
import com.nirvasoft.rp.shared.LOVSetupAllData;
import com.nirvasoft.rp.shared.LOVSetupAllDataArr;
import com.nirvasoft.rp.shared.LOVSetupDetailData;

public class LOVDAO {

	public Lov3 getAccountTransferTypes(Connection conn) throws SQLException {
		Lov3 response = null;

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			String sql = "SELECT Code, Description FROM LOVDetails WHERE HKey = (SELECT SysKey FROM LOVHeader WHERE Description = 'Transfer Type') ORDER BY SysKey;";
			preparedStatement = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			resultSet = preparedStatement.executeQuery();
			resultSet.last();
			int totalRow = resultSet.getRow();

			if (totalRow == 0) {
				response = new Lov3();
				response.setMsgCode("0014");
				response.setMsgDesc("There Is No Transfer Type At Database.");
				response.setRefAccountTransferType(null);
			}

			if (totalRow != 0) {
				resultSet.beforeFirst();

				Ref[] refArray = new Ref[totalRow];
				int i = 0;

				while (resultSet.next()) {
					Ref ref = new Ref();
					ref.setvalue(resultSet.getString("Code"));
					ref.setcaption(resultSet.getString("Description"));

					refArray[i++] = ref;
				}

				response = new Lov3();
				response.setMsgCode("0000");
				response.setMsgDesc("Select Successfully.");
				response.setRefAccountTransferType(refArray);
			}
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}

			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}

		return response;
	}	

	public LOVSetupAllData getLOVbyCode(String lovNo, Connection l_Conn) throws SQLException {
		LOVSetupAllData ret = new LOVSetupAllData();
		int srno = 1;
		ArrayList<LOVSetupDetailData> datalist = new ArrayList<LOVSetupDetailData>();

		String query = "SELECT SysKey,Code, Description FROM LOVHeader where Code = ?";
		PreparedStatement pstmt = l_Conn.prepareStatement(query);
		pstmt.setString(1, lovNo);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			ret.setSysKey(rs.getLong("SysKey"));
			ret.setLovNo(rs.getString("Code"));
			ret.setLovDesc2(rs.getString("Description"));
		}
		pstmt.close();
		rs.close();

		query = "SELECT Code, Description,Price from LOVDetails where HKey = ?";
		pstmt = l_Conn.prepareStatement(query);
		pstmt.setLong(1, ret.getSysKey());

		rs = pstmt.executeQuery();
		while (rs.next()) {
			LOVSetupDetailData data = new LOVSetupDetailData();
			data.setSrno(String.valueOf(srno++));
			data.setLovCde(rs.getString("Code"));
			data.setLovDesc1(rs.getString("Description"));
			data.setPrice(rs.getDouble("Price"));
			datalist.add(data);
		}
		LOVSetupDetailData[] dataarry = new LOVSetupDetailData[datalist.size()];
		for (int i = 0; i < datalist.size(); i++) {
			dataarry[i] = datalist.get(i);
		}
		ret.setData(dataarry);
		return ret;
	}

	public LOVSetupDetailData[] getLOVbyHKey(long sysKey, Connection Conn) throws SQLException {

		int srno = 1;
		ArrayList<LOVSetupDetailData> datalist = new ArrayList<LOVSetupDetailData>();

		String query = "";
		PreparedStatement pstmt = null;
		;
		query = "SELECT Code, Description from LOVDetails where HKey = ?";
		pstmt = Conn.prepareStatement(query);
		pstmt.setLong(1, sysKey);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			LOVSetupDetailData data = new LOVSetupDetailData();
			data.setSrno(String.valueOf(srno++));
			data.setLovCde(rs.getString("Code"));
			data.setLovDesc1(rs.getString("Description"));
			datalist.add(data);
		}
		LOVSetupDetailData[] dataarry = new LOVSetupDetailData[datalist.size()];
		for (int i = 0; i < datalist.size(); i++) {
			dataarry[i] = datalist.get(i);
		}
		return dataarry;

	}

	public LOVSetupAllData getLOVbySysKey(long sysKey, Connection Conn) throws SQLException {
		LOVSetupAllData ret = new LOVSetupAllData();
		int srno = 1;
		ArrayList<LOVSetupDetailData> datalist = new ArrayList<LOVSetupDetailData>();

		String query = "SELECT SysKey, Code, Description FROM LOVHeader where SysKey = ?";
		PreparedStatement pstmt = Conn.prepareStatement(query);
		pstmt.setLong(1, sysKey);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			ret.setSysKey(rs.getLong("SysKey"));
			ret.setLovNo(rs.getString("Code"));
			ret.setLovDesc2(rs.getString("Description"));
		}
		pstmt.close();
		rs.close();

		query = "SELECT Code, Description,Price from LOVDetails where HKey = ?";
		pstmt = Conn.prepareStatement(query);
		pstmt.setLong(1, sysKey);
		rs = pstmt.executeQuery();
		while (rs.next()) {
			LOVSetupDetailData data = new LOVSetupDetailData();
			data.setSrno(String.valueOf(srno++));
			data.setLovCde(rs.getString("Code"));
			data.setLovDesc1(rs.getString("Description"));
			data.setPrice(rs.getDouble("Price"));
			datalist.add(data);
		}
		LOVSetupDetailData[] dataarry = new LOVSetupDetailData[datalist.size()];
		for (int i = 0; i < datalist.size(); i++) {
			dataarry[i] = datalist.get(i);
		}
		ret.setData(dataarry);
		return ret;

	}

	public String getMaxLovNo(Connection pConn) throws SQLException {
		String l_Key = "";

		String l_Query = "SELECT MAX(Code) as maxkey  from LOVHeader";
		PreparedStatement pstmt = pConn.prepareStatement(l_Query);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			l_Key = rs.getString("maxkey");
		}
		return l_Key;
	}

	public Ref[] getRefLOVByLOV(Connection l_Conn) {
		Ref ref = new Ref();
		Ref[] arr = null;
		int count = 0;
		try {
			String l_Query = "select  count(Distinct Code) c from LOVHeader";
			PreparedStatement pstmt = l_Conn.prepareStatement(l_Query);

			ResultSet rs = pstmt.executeQuery();

			if (rs.next()) {
				count = rs.getInt("c");
			}
			arr = new Ref[count];

			l_Query = "select Distinct Code, Description from LOVHeader ";
			pstmt = l_Conn.prepareStatement(l_Query);
			rs = pstmt.executeQuery();

			int index = 0;
			while (rs.next()) {
				ref = new Ref();
				ref.setcaption(rs.getString("Description"));
				ref.setvalue(rs.getString("Code"));
				arr[index] = ref;
				index++;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return arr;
	}

	public Ref[] getRefLOVBYLOV1(Connection l_Conn) {
		Ref ref = new Ref();
		Ref[] arr = null;
		int count = 0;
		try {
			String l_Query = "select  count(*) c from LOVDetails";
			PreparedStatement pstmt = l_Conn.prepareStatement(l_Query);

			ResultSet rs = pstmt.executeQuery();

			if (rs.next()) {
				count = rs.getInt("c");
			}
			arr = new Ref[count];

			l_Query = "select LOVHeader.SysKey,LOVHeader.Code,LOVHeader.Description,LOVDetails.Code as srno,LOVDetails.Description from LOVDetails join LOVHeader on LOVDetails.HKey = LOVHeader.SysKey";
			pstmt = l_Conn.prepareStatement(l_Query);
			rs = pstmt.executeQuery();

			int index = 0;
			while (rs.next()) {
				ref = new Ref();
				arr[index] = ref;
				index++;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return arr;
	}

	public Lov3 getTicketStatus(Connection conn) throws SQLException {
		Lov3 response = null;

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			String query = "SELECT Code, Description, Price FROM LOVDetails WHERE HKey = (SELECT SysKey FROM LOVHeader WHERE Description = 'Ticket Status') ORDER BY SysKey;";
			preparedStatement = conn.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			resultSet = preparedStatement.executeQuery();

			int totalRow = 0;
			resultSet.last();
			totalRow = resultSet.getRow();

			if (totalRow == 0) {
				response = new Lov3();
				response.setMsgCode("0014");
				response.setMsgDesc("There is no ticket status at database.");
				response.setRefTicketStatus(null);
			}

			if (totalRow > 0) {
				resultSet.beforeFirst();

				Ref[] refArray = new Ref[totalRow];
				int i = 0;

				while (resultSet.next()) {
					Ref ref = new Ref();
					ref.setvalue(resultSet.getString("Code"));
					ref.setcaption(resultSet.getString("Description"));
					ref.setProcessingCode(resultSet.getString("Price"));
					refArray[i++] = ref;
				}

				response = new Lov3();
				response.setMsgCode("0000");
				response.setMsgDesc("Select successfully.");
				response.setRefTicketStatus(refArray);
			}
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}

			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}

		return response;
	}

	public long inserLOVHeader(LOVSetupAllData aData, Connection aConn) throws SQLException {
		boolean headerCheck = false;
		String code = "";
		long key = 0;

		String query = "INSERT INTO LOVHeader (Code, Description) VALUES (?,?)";
		PreparedStatement pstmt = aConn.prepareStatement(query);
		pstmt.setString(1, aData.getLovNo());
		pstmt.setString(2, aData.getLovDesc2());

		code = aData.getLovNo();
		if (pstmt.executeUpdate() > 0) {
			headerCheck = true;
		}
		pstmt.close();

		query = "SELECT SysKey from LOVHeader WHERE Code = ?";
		pstmt = aConn.prepareStatement(query);
		pstmt.setString(1, code);
		ResultSet rs = pstmt.executeQuery();
		rs.next();
		key = rs.getLong("SysKey");
		pstmt.close();
		rs.close();

		return key;
	}

	public Boolean insertLOVDetails(LOVSetupAllData aData, long Hkey, Connection aConn) throws SQLException {
		boolean ret = false;
		for (int i = 0; i < aData.getData().length; i++) {
			String query = "INSERT INTO LOVDetails (HKey, Code, Description,Price) VALUES (?,?,?,?)";
			PreparedStatement pstmt = aConn.prepareStatement(query);
			pstmt.setLong(1, Hkey);
			pstmt.setString(2, aData.getData()[i].getLovCde());
			pstmt.setString(3, aData.getData()[i].getLovDesc1());
			pstmt.setDouble(4, aData.getData()[i].getPrice());

			if (pstmt.executeUpdate() > 0) {
				ret = true;
			}
			pstmt.close();
		}
		return ret;
	}

	public LOVSetupAllDataArr LOVSetuplist(String searchText, int pageSize, int currentPage, Connection aConn)
			throws SQLException {
		LOVSetupAllDataArr ret = new LOVSetupAllDataArr();
		ArrayList<LOVSetupAllData> datalist = new ArrayList<LOVSetupAllData>();
		int l_startRecord = (currentPage - 1) * pageSize;
		int l_endRecord = l_startRecord + pageSize;
		String whereClause = "";
		if (!(searchText.equalsIgnoreCase(null))) {
			if ((!searchText.isEmpty()) && (searchText.trim().length() > 0)) {
				whereClause += " WHERE (Code LIKE ? OR Description LIKE ?)";
			}
		} else if (searchText.equalsIgnoreCase("")) {
			whereClause = "WHERE 1=1";
		}

		String l_Query = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY [SysKey] ASC) AS RowNum, "
				+ " [SysKey], [Code],[Description] FROM [LOVHeader]" + whereClause + ") AS RowConstrainedResult"
				+ " WHERE ( RowNum > " + l_startRecord + " and RowNum <= " + l_endRecord + " )";

		PreparedStatement pstmt = aConn.prepareStatement(l_Query);
		if (!(searchText.equalsIgnoreCase(null))) {
			if ((!searchText.isEmpty()) && (searchText.trim().length() > 0)) {
				int i = 1;
				pstmt.setString(i++, "%" + searchText + "%");
				pstmt.setString(i++, "%" + searchText + "%");
			}
		}
		ResultSet rs = pstmt.executeQuery();

		while (rs.next()) {
			LOVSetupAllData data = new LOVSetupAllData();// **
			data.setSysKey(rs.getLong("SysKey"));
			data.setLovNo(rs.getString("Code"));
			data.setLovDesc2(rs.getString("Description"));
			datalist.add(data);
		}

		LOVSetupAllData[] dataarry = new LOVSetupAllData[datalist.size()];
		for (int i = 0; i < datalist.size(); i++) {
			dataarry[i] = datalist.get(i);
		}
		ret.setData(dataarry);
		ret.setSearchText(searchText);
		ret.setCurrentPage(currentPage);
		ret.setPageSize(pageSize);

		PreparedStatement stat = aConn.prepareStatement("SELECT COUNT(*) AS tot FROM LOVHeader " + whereClause);
		if (!(searchText.equalsIgnoreCase(null))) {
			if ((!searchText.isEmpty()) && (searchText.trim().length() > 0)) {
				int i = 1;
				stat.setString(i++, "%" + searchText + "%");
				stat.setString(i++, "%" + searchText + "%");
			}
		}
		ResultSet result = stat.executeQuery();
		result.next();
		ret.setTotalCount(result.getInt("tot"));
		return ret;
	}

	public boolean updateLOVDetails(LOVSetupAllData aData, long Hkey, Connection l_Conn) throws SQLException {
		boolean ret = false;

		String query = "DELETE FROM LOVDetails WHERE HKey = ?";
		PreparedStatement pstmt = l_Conn.prepareStatement(query);
		pstmt.setLong(1, Hkey);
		pstmt.executeUpdate();
		pstmt.close();

		for (int i = 0; i < aData.getData().length; i++) {
			query = "INSERT INTO LOVDetails (HKey,Code,Description,Price) VALUES (?,?,?,?)";
			pstmt = l_Conn.prepareStatement(query);
			pstmt.setLong(1, Hkey);
			pstmt.setString(2, aData.getData()[i].getLovCde());
			pstmt.setString(3, aData.getData()[i].getLovDesc1());
			pstmt.setDouble(4, aData.getData()[i].getPrice());
			if (pstmt.executeUpdate() > 0) {
				ret = true;
			}
		}

		return ret;
	}

	public long updateLOVHeader(LOVSetupAllData aData, Connection l_Conn) throws SQLException {
		long key = aData.getSysKey();
		boolean headerCheck = false;

		String query = "UPDATE LOVHeader SET Description=? WHERE SysKey = ?";
		PreparedStatement pstmt = l_Conn.prepareStatement(query);
		pstmt.setString(1, aData.getLovDesc2());
		pstmt.setLong(2, aData.getSysKey());
		if (pstmt.executeUpdate() > 0) {
			headerCheck = true;
		}
		pstmt.close();

		return key;
	}

}