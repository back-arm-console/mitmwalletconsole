package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.rp.shared.CityStateData;

public class UserDataDao {
	// atn
	public ArrayList<CityStateData> getStateListByUserID(String userid, Connection conn) throws SQLException {

		ArrayList<CityStateData> datalist = new ArrayList<CityStateData>();
		PreparedStatement stmt;
		String sql = "";
		String regioncode = "";
		String sql1 = "select code,DespEng,DespMyan from addressref a ,UVM005_A as b where a.code=b.t5 " + "and b.t1='"
				+ userid + "' ORDER BY Code";
		stmt = conn.prepareStatement(sql1);
		ResultSet res1 = stmt.executeQuery();
		while (res1.next()) {
			regioncode = res1.getString("code");
			if (regioncode.equals("00000000")) {
				// sql = "select code,DespEng,DespMyan from addressref where
				// code like '%000000' ORDER BY Code";
				sql = "select code,DespEng,DespMyan from addressref where code in ('00000000','13000000','10000000') ORDER BY Code";
			} else {
				sql = "select code,DespEng,DespMyan from addressref where code ='" + regioncode + "' ORDER BY Code";
			}
			stmt = conn.prepareStatement(sql);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				CityStateData combo = new CityStateData();
				combo.setDespEng(res.getString("DespEng"));
				combo.setCode(res.getString("code"));
				datalist.add(combo);
			}
		}
		return datalist;
	}
	/*
	 * public DivisionComboDataSet getStatusList(long status,Connection conn)
	 * throws SQLException { DivisionComboDataSet dataset = new
	 * DivisionComboDataSet(); ArrayList<DivisionComboData> datalist = new
	 * ArrayList<DivisionComboData>(); DivisionComboData combo; String sql = "";
	 * if(status==1){ //contend writer sql =
	 * "select n1,t1 from status where n1 in(6,1,2) order by n1 Desc"; }
	 * if(status==2){ //editor sql =
	 * "select n1,t1 from status where recordstatus<>4 and(n1=2 or n1=4 or n1=6)"
	 * ; } if(status==3){ //publisher sql =
	 * "select n1,t1 from status where recordstatus<>4 and(n1=4 or n1=5 or n1=6)"
	 * ; } if(status==4){ //master sql =
	 * "select n1,t1 from status where recordstatus<>4 and(n1=1 or n1=2 or n1=4 or n1=5 or n1=6)"
	 * ; } if(status==5){ sql =
	 * "select n1,t1 from status where recordstatus<>4 and(n1=1 or n1=2 or n1=4 or n1=5 or n1=6)"
	 * ;
	 * 
	 * } PreparedStatement stmt = conn.prepareStatement(sql); ResultSet res =
	 * stmt.executeQuery(); while (res.next()) { combo = new
	 * DivisionComboData(); combo.setCaption(res.getString("t1"));
	 * combo.setValue(res.getString("n1")); datalist.add(combo); }
	 * DivisionComboData[] dataarray = new DivisionComboData[datalist.size()];
	 * dataarray = datalist.toArray(dataarray); dataset.setData(dataarray);
	 * return dataset; }
	 */

	// atn
	public ArrayList<CityStateData> getStateListByUserIDWithAll(String userid, Connection conn) throws SQLException {

		ArrayList<CityStateData> datalist = new ArrayList<CityStateData>();
		PreparedStatement stmt;
		String sql = "";
		String regioncode = "";
		String sql1 = "select code,DespEng,DespMyan from addressref a ,UVM005_A as b where a.code=b.t5 " + "and b.t1='"
				+ userid + "' ORDER BY Code";
		stmt = conn.prepareStatement(sql1);
		ResultSet res1 = stmt.executeQuery();
		while (res1.next()) {
			regioncode = res1.getString("code");
			if (regioncode.equals("00000000")) {
				// sql = "select code,DespEng,DespMyan from addressref where
				// code like '%000000' ORDER BY Code";
				sql = "select code,DespEng,DespMyan from addressref where code in ('00000000','13000000','10000000') ORDER BY Code";
			} else if (regioncode.equals("10000000")) {
				sql = "select code,DespEng,DespMyan from addressref where code in ('00000000','10000000') ORDER BY Code";
			} else if (regioncode.equals("13000000")) {
				sql = "select code,DespEng,DespMyan from addressref where code in ('00000000','13000000') ORDER BY Code";
			}
			stmt = conn.prepareStatement(sql);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				CityStateData combo = new CityStateData();
				combo.setDespEng(res.getString("DespEng"));
				combo.setCode(res.getString("code"));
				datalist.add(combo);
			}
		}
		return datalist;
	}

}
