package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;
import com.nirvasoft.rp.framework.ResultTwo;
//import com.nirvasoft.rp.data.CityStateComboDataSet;
import com.nirvasoft.rp.shared.CityStateData;
import com.nirvasoft.rp.users.data.UCJunction;
import com.nirvasoft.rp.users.data.UserData;
import com.nirvasoft.rp.users.data.UserRole;
import com.nirvasoft.rp.users.data.UserViewData;
import com.nirvasoft.rp.users.data.UserViewDataset;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.ServerUtil;

public class UserDao {
	// generate password
	public static String autogeneratePassword(int length) {

		String pwd = "";
		if (length == 0) {
			length = 6;
		}
		int pwdlength = length;

		char ch;
		String num_list = "0123456789";
		String char_list = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		// String specialchar_list = "!#$%&*?@";
		// String specialchar_list = "!@#$%^&*()+`~";
		// String list =
		// "!#$%&*?@abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		String list = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		StringBuffer randStr = new StringBuffer();

		for (int i = 0; i < pwdlength; i++) {
			Random randomGenerator = new Random();
			int number = 0;
			if (i == 0) {
				number = randomGenerator.nextInt(char_list.length());
				ch = char_list.charAt(number);
			} /*
				 * else if (i == 1) { number =
				 * randomGenerator.nextInt(specialchar_list.length()); ch =
				 * specialchar_list.charAt(number); }
				 */ else if (i == 2) {
				number = randomGenerator.nextInt(num_list.length());
				ch = num_list.charAt(number);
			} else {
				number = randomGenerator.nextInt(list.length());
				ch = list.charAt(number);
			}
			randStr.append(ch);
		}
		pwd = String.valueOf(randStr);
		return pwd;

	}

	public static boolean canDelete(long key, Connection conn) throws SQLException {

		ArrayList<Long> dbrs = new ArrayList<Long>();
		String sql = "Select n1 From JUN002_A Where n2=?";
		PreparedStatement stat = conn.prepareStatement(sql);
		stat.setLong(1, key);
		ResultSet result = stat.executeQuery();
		while (result.next()) {
			dbrs.add(result.getLong("n1"));

		}
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}
	// get user setup list
	/*
	 * public UserViewDataset getAllUserData(String searchtext, Connection conn,
	 * String operation) throws SQLException { int recordStatus = 0, n7 = 0;
	 * String searchText = searchtext.replace("'", "''"); String whereClause =
	 * ""; if (operation.equalsIgnoreCase("alluserprofile")) { if
	 * (searchText.equals("")) { whereClause =
	 * "where RecordStatus<>4 and role = 1 "; } else { whereClause =
	 * "where RecordStatus<>4 and role = 1" + " and (" + " t1 like '%" +
	 * searchText + "%' or UN like '%" + searchText + "%' )"; }
	 * 
	 * } else if (operation.equalsIgnoreCase("allbankuser")) { if
	 * (searchText.equals("")) { whereClause =
	 * "where RecordStatus<>4 and role = 2 "; } else { whereClause =
	 * "where RecordStatus<>4 and role = 2" + " and (" + " t1 like '%" +
	 * searchText + "%' or UN like '%" + searchText + "%')"; } } else if
	 * (operation.equalsIgnoreCase("activate")) { if (searchText.equals("")) {
	 * whereClause =
	 * " where (RecordStatus = 21 or RecordStatus = 1) and role = 1 "; } else {
	 * whereClause =
	 * " where ((RecordStatus = 21 or RecordStatus = 1) and role = 1)" + " and "
	 * + "(t1 like '%" + searchText + "%' or UN like '%" + searchText + "%') ";
	 * }
	 * 
	 * } else { if (operation.equalsIgnoreCase("lock")) { recordStatus = 2; n7 =
	 * 0;
	 * 
	 * } else if (operation.equalsIgnoreCase("deactivate")) { recordStatus = 2;
	 * n7 = 0;
	 * 
	 * } else if (operation.equalsIgnoreCase("unlock")) { recordStatus = 2; n7 =
	 * 11;
	 * 
	 * }
	 * 
	 * if (searchText.equals("")) { whereClause = "where RecordStatus =" +
	 * recordStatus + " and lock =" + n7 + "and role = 1"; } else { whereClause
	 * = "where RecordStatus =" + recordStatus + " and lock =" + n7 +
	 * "and role = 1 and " + "(t1 like '%" + searchText + "%' or UN like '%" +
	 * searchText + "%') "; } }
	 * 
	 * ArrayList<UserViewData> ret = new ArrayList<UserViewData>();
	 * ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(defineView(), whereClause,
	 * "ORDER BY t1,UN", conn);
	 * 
	 * for (int i = 0; i < dbrs.size(); i++) {
	 * ret.add(getDBViewRecord(dbrs.get(i))); }
	 * 
	 * UserViewDataset dataSet = new UserViewDataset(); dataSet.setArlData(ret);
	 * return dataSet; }
	 */

	public static boolean checkCanDelete(long key, Connection conn) throws SQLException {

		ArrayList<String> dbrs = new ArrayList<String>();
		String userId = "";
		String sql = "SELECT t1 FROM UVM005_A WHERE syskey = ?";
		PreparedStatement pstmt = conn.prepareStatement(sql);
		pstmt.setLong(1, key);
		ResultSet result = pstmt.executeQuery();
		if (result.next()) {
			userId = result.getString("t1");
		}
		String query = "SELECT SessionID FROM tblSession  WHERE UserID = ? AND LogInDateTime=(SELECT MAX(LogInDateTime) FROM tblSession WHERE Status = 0 AND UserID = ? )";
		PreparedStatement stmt = conn.prepareStatement(query);
		int j = 1;
		stmt.setString(j++, userId);
		stmt.setString(j++, userId);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			dbrs.add(res.getString("SessionID"));

		}

		if (dbrs.size() > 0) {
			return false;
		} else {
			return true;
		}
	}

	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("UVM005_A");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("CreatedDate", (byte) 5));
		ret.getFields().add(new DBField("ModifiedDate", (byte) 5));
		ret.getFields().add(new DBField("UserId", (byte) 5));
		ret.getFields().add(new DBField("UserName", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 1));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 1));
		ret.getFields().add(new DBField("n6", (byte) 1));
		ret.getFields().add(new DBField("n7", (byte) 1));
		ret.getFields().add(new DBField("n8", (byte) 2));

		return ret;
	}

	/////////////

	public static DBRecord define(String tabName) {
		DBRecord ret = new DBRecord();
		ret.setTableName(tabName);
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 1));
		ret.getFields().add(new DBField("n4", (byte) 1));
		ret.getFields().add(new DBField("n5", (byte) 2));

		return ret;
	}

	///////////
	public static DBRecord defineView() {
		DBRecord ret = new DBRecord();
		ret.setTableName("V_U001_A");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("autokey", (byte) 2));
		ret.getFields().add(new DBField("CreatedDate", (byte) 5));
		ret.getFields().add(new DBField("ModifiedDate", (byte) 5));
		ret.getFields().add(new DBField("UserId", (byte) 5));
		ret.getFields().add(new DBField("UserName", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		// ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		// ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("UN", (byte) 5));
		ret.getFields().add(new DBField("lock", (byte) 1));
		ret.getFields().add(new DBField("role", (byte) 1));
		ret.getFields().add(new DBField("customerID", (byte) 5));
		return ret;
	}

	public static ResultTwo delete(long syskey, String modifieduserId, String parentID, String loginId, Connection conn)
			throws SQLException {
		ResultTwo res = new ResultTwo();
		String date = new SimpleDateFormat("yyyyMMdd").format(new Date());

		if (!checkCanDelete(syskey, conn))
		{
			res.setState(false);
			res.setMsgDesc("Cannot delete.Please forced Sign Out.");
		} else {
			/*String sql_par = "Select Count (*) Count from Agent Where ParentId = ? And n1 = 0";
			PreparedStatement stmt_par = conn.prepareStatement(sql_par);
			stmt_par.setString(1, loginId);
			ResultSet result = stmt_par.executeQuery();
			int p_count = 0;
			while (result.next()) {
				p_count = result.getInt("Count");
			}
			if (p_count > 0) {
				res.setState(false);
				res.setMsgDesc("Parent Account cannot delete.");
				return res;
			}*/
			res = PersonDao.deletePerson(syskey, modifieduserId, conn);

			if (res.isState()) {
				String sql = "delete From JUN002_A Where n1= ?";
				PreparedStatement stmt = conn.prepareStatement(sql);
				stmt.setLong(1, syskey);
				stmt.executeUpdate();

				/*sql = "Delete from Agent  WHERE loginId= (Select t1 From UVM005_A Where RecordStatus<>4 And syskey =?) ";
				stmt = conn.prepareStatement(sql);
				stmt.setLong(1, syskey);
				stmt.executeUpdate();*/

				sql = "UPDATE UVM005_A SET RecordStatus=4,modifieddate=? WHERE Syskey=?";
				stmt = conn.prepareStatement(sql);
				int j = 1;
				stmt.setString(j++, date);
				stmt.setLong(j++, syskey);
				int rs = stmt.executeUpdate();
				if (rs > 0) {
					res.setState(true);
					res.setMsgDesc("Deleted Successfully");
				} else {
					res.setState(false);
					res.setMsgDesc("Deleted Fail");
				}
			}
		}
		return res;
	}

	public static String generatePassword(Connection conn) throws SQLException {
		String num_list = "0123456789";
		String lowerchar_list = "abcdefghijklmnopqrstuvwxyz";
		String upperchar_list = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String specialchar_list = "!#$%&*?@";
		// String list =
		// "!#$%&*?@abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		String list = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

		int minimunlength = 0;
		int splength = 0;
		int uppercharlength = 0;
		int lowercharlength = 0;
		int numberlength = 0;

		StringBuffer randStr = new StringBuffer();
		int number = 0;

		String sql = "SELECT MinimumLength,SpecialCharacter,UpperCaseCharacter,LowerCaseCharacter,Number FROM tblPasswordpolicy";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet res = ps.executeQuery();
		while (res.next()) {
			minimunlength = res.getInt("MinimumLength");
			splength = res.getInt("SpecialCharacter");// 2
			uppercharlength = res.getInt("UpperCaseCharacter");// 1
			lowercharlength = res.getInt("LowerCaseCharacter");// 1
			numberlength = res.getInt("Number");// 0
		}

		String pwd = "";
		if (splength == 0 && uppercharlength == 0 && lowercharlength == 0 && numberlength == 0) {
			pwd = autogeneratePassword(minimunlength);
		} else {
			char ch;
			int count = 0;
			Random randomGenerator = new Random();
			if (splength != 0) {
				number = randomGenerator.nextInt(specialchar_list.length());
				ch = specialchar_list.charAt(number);
				randStr.append(ch);
				splength = splength - 1;
				count++;
			}
			if (uppercharlength != 0) {
				number = randomGenerator.nextInt(upperchar_list.length());
				ch = upperchar_list.charAt(number);
				randStr.append(ch);
				uppercharlength = uppercharlength - 1;
				count++;
			}
			if (lowercharlength != 0) {
				number = randomGenerator.nextInt(lowerchar_list.length());
				ch = lowerchar_list.charAt(number);
				randStr.append(ch);
				lowercharlength = lowercharlength - 1;
				count++;
			}
			if (numberlength != 0) {
				number = randomGenerator.nextInt(num_list.length());
				ch = num_list.charAt(number);
				randStr.append(ch);
				numberlength = numberlength - 1;
				count++;
			}
			if (splength != 0) {
				for (int f = 0; f < splength; f++) {
					number = randomGenerator.nextInt(specialchar_list.length());
					ch = specialchar_list.charAt(number);
					randStr.append(ch);
					count++;
				}
			}

			if (uppercharlength != 0) {
				for (int d = 0; d < uppercharlength; d++) {
					number = randomGenerator.nextInt(upperchar_list.length());
					ch = upperchar_list.charAt(number);
					randStr.append(ch);
					count++;
				}

			}
			if (numberlength != 0) {
				for (int j = 0; j < numberlength; j++) {
					number = randomGenerator.nextInt(num_list.length());
					ch = num_list.charAt(number);
					randStr.append(ch);
					count++;
				}
			}

			if (lowercharlength != 0) {
				for (int k = 0; k < lowercharlength; k++) {
					number = randomGenerator.nextInt(lowerchar_list.length());
					ch = lowerchar_list.charAt(number);
					randStr.append(ch);
					count++;
				}
			}

			if (count < minimunlength) {
				for (int i = 0; i < (minimunlength - count); i++) {
					number = randomGenerator.nextInt(list.length());
					ch = list.charAt(number);
					randStr.append(ch);
				}
			}
			pwd = String.valueOf(randStr);
		}
		return pwd;
	}

	// ///////////////////////////////////////////////////////////////
	public static UserViewDataset getAllUCJunctionData(String searchtext, Connection conn) throws SQLException {
		String searchText = searchtext.replace("'", "''");
		ArrayList<UserViewData> ret = new ArrayList<UserViewData>();
		String whereClause = "";
		if (searchText.equals("") || searchText.equals(null)) {
			whereClause = " where a.RecordStatus<>4 and a.n2=1 and a.n7<>11 and b.RecordStatus<>21";
		} else {
			whereClause = " where a.RecordStatus<>4 and a.n2=1 and a.n7<>11 and b.RecordStatus<>21" + " and "
					+ "(a.t1 like ? or a.t7 like ? or b.t2 like ? )";
		}
		String l_Query = "select b.t2 'UserName',a.t1,a.t7 from UVM005_A a Join UVM012_A b ON a.n4=b.syskey"
				+ whereClause;

		l_Query += " order by a.t1,b.t2,a.t7";

		PreparedStatement pstmt = conn.prepareStatement(l_Query);
		if (searchText.equals("") || searchText.equals(null)) {
		} else {
			for (int i = 1; i < 4; i++) {
				pstmt.setString(i, "%" + searchText + "%");
			}
		}
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			UserViewData data = new UserViewData();
			{
				data.setT1(rs.getString("t1"));
				data.setUsername(rs.getString("UserName"));
				data.setT7(rs.getString("t7"));
				ret.add(data);
			}
		}
		UserViewDataset dataSet = new UserViewDataset();
		dataSet.setArlData(ret);

		return dataSet;

	}

	public static UserViewDataset getAllUserCIFDataList(String searchtext, Connection conn) throws SQLException {
		// new
		String searchText = searchtext.replace("'", "''");
		ArrayList<UserViewData> ret = new ArrayList<UserViewData>();
		String whereClause = "";
		if (searchText.equals("")) {
			whereClause = "where u.RecordStatus<>4 and m.mobileUserID = u.t1";
		} else {
			whereClause = "where (u.RecordStatus<>4 and m.mobileUserID = u.t1 and m.mobileUserID like ?) or "
					+ "(u.RecordStatus<>4 and m.mobileUserID = u.t1 and u.t2 like ?)";
		}
		String l_Query = "select distinct u.t1 as userid,u.t2 as username from MobileCAJunction m , UVM012_A u "
				+ whereClause;
		l_Query += " order by u.t1,u.t2";

		PreparedStatement pstmt = conn.prepareStatement(l_Query);
		if (searchText.equals("")) {
		} else {
			int i = 1;
			pstmt.setString(i++, "%" + searchText + "%");
			pstmt.setString(i++, "%" + searchText + "%");
		}
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			UserViewData data = new UserViewData();
			{
				data.setT1(rs.getString("userid"));
				data.setUserName(rs.getString("username"));
				ret.add(data);
			}
		}
		UserViewDataset dataSet = new UserViewDataset();
		dataSet.setArlData(ret);
		return dataSet;
	}

	public static UserData getDBRecord(DBRecord adbr) {
		UserData ret = new UserData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreatedDate(adbr.getString("CreatedDate"));
		ret.setModifiedDate(adbr.getString("ModifiedDate"));
		ret.setUserId(adbr.getString("UserId"));
		ret.setUserName(adbr.getString("UserName"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));

		ret.setUsersyskey(adbr.getLong("usersysKey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(ServerUtil.decryptPIN(adbr.getString("t2")));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setT7(adbr.getString("t7"));
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getInt("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getLong("n4"));
		ret.setN5(adbr.getInt("n5"));
		ret.setN6(adbr.getInt("n6"));
		ret.setN7(adbr.getInt("n7"));
		ret.setN8(adbr.getLong("n8"));

		return ret;
	}

	public static UserRole getDBRecords(DBRecord adbr) {
		UserRole ret = new UserRole();
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));

		ret.setUsersyskey(adbr.getLong("usersysKey"));

		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getInt("n3"));
		ret.setN4(adbr.getInt("n4"));
		ret.setN5(adbr.getInt("n5"));

		return ret;
	}

	public static UserViewData getDBViewRecord(DBRecord adbr) {
		UserViewData ret = new UserViewData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setAutokey(adbr.getLong("autokey"));
		ret.setCreatedDate(adbr.getString("CreatedDate"));
		ret.setModifiedDate(adbr.getString("ModifiedDate"));
		ret.setUserId(adbr.getString("UserId"));
		ret.setUserName(adbr.getString("UserName"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setUsersyskey(adbr.getLong("usersysKey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT7(adbr.getString("t7"));
		ret.setUsername(adbr.getString("UN"));
		ret.setN7(adbr.getInt("lock"));
		ret.setN2(adbr.getInt("role"));
		ret.setCustomerid(adbr.getString("customerID"));
		return ret;
	}

	public static long getPersonSyskey(long usys, Connection con) throws SQLException {

		String sql = "SELECT * FROM UVM005_A WHERE syskey=?";
		PreparedStatement stat = con.prepareStatement(sql);
		stat.setLong(1, usys);
		ResultSet result = stat.executeQuery();
		long u = 0;
		while (result.next()) {
			u = result.getLong("n4");
		}

		return u;

	}

	public static ResultTwo insert(boolean isUserProfile, UserData obj, Connection conn) throws SQLException {
		ResultTwo res = new ResultTwo();
		if (!isCodeExist(obj, conn)) {
			try {
				String sql = DBMgr.insertString(define(), conn);
				PreparedStatement stmt = conn.prepareStatement(sql);
				DBRecord dbr = setDBRecord(obj);
				DBMgr.setValues(stmt, dbr);
				int rs = stmt.executeUpdate();
				if (rs > 0) {
					if (!isUserProfile) {
						UserRole jun = new UserRole();
						for (long l : obj.getRolesyskey()) {

							if (l != 0) {

								jun.setRecordStatus(obj.getRecordStatus());
								jun.setSyncBatch(obj.getSyncBatch());
								jun.setSyncStatus(obj.getSyncStatus());
								jun.setUsersyskey(obj.getSyskey());
								jun.setN1(obj.getSyskey());
								jun.setN2(l);
								res = insertUserRole(jun, conn);

							}
						}

						if (res.isState()) {

							res.setState(true);
							res.setMsgDesc("Saved Successfully");

						} else
							res.setMsgDesc("Cannot Save");
					} else {
						String query = "Update UVM012_A set N10 = ? Where recordStatus <> 4 and t1=?";
						PreparedStatement stmt1 = conn.prepareStatement(query);
						int i = 1;
						stmt1.setInt(i++, 1);
						stmt1.setString(i++, obj.getT1());
						int rst = stmt1.executeUpdate();
						if (rst > 0) {

							UserDao dao = new UserDao();
							res = dao.insertAgent(obj, conn);

							/*
							 * if (obj.getN3() == 1) { UserDao dao = new
							 * UserDao(); res = dao.insertAgent(obj, conn); }
							 */

							if (res.isState()) {
								res.setState(true);
								res.setMsgDesc("Saved Successfully");
								res.setLoginID(obj.getT1());
								res.setPhNo(obj.getT4());
							}

							/*
							 * res.setState(true); res.setMsgDesc(
							 * "Saved Successfully");
							 * res.setLoginID(obj.getT1());
							 * res.setPhNo(obj.getT4());
							 */
						} else {
							res.setState(false);
							res.setMsgDesc("Save Fail");
						}

					}

				} else
					res.setMsgDesc("Cannot Save");

			}

			catch (Exception e) {
				res.setMsgDesc("Cannot Save");
				e.printStackTrace();
			}

		} else
			res.setMsgDesc("Data already exist");

		return res;
	}

	public static ResultTwo insertUserRole(UserRole obj, Connection conn) throws SQLException {
		ResultTwo res = new ResultTwo();
		String sql = DBMgr.insertString(define("JUN002_A"), conn);
		PreparedStatement stmt = conn.prepareStatement(sql);
		DBRecord dbr = setDBRecord(obj);
		DBMgr.setValues(stmt, dbr);
		int rs = stmt.executeUpdate();

		if (rs > 0) {
			res.setState(true);
		}

		return res;
	}

	// MMPPM
	public static boolean isCodeExist(UserData obj, Connection conn) throws SQLException {

		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				" WHERE RecordStatus<>4 AND syskey = " + obj.getSyskey(), "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}

	}

	public static UserData readBranchUserDataBySyskey(long syskey, Connection conn) throws SQLException {

		UserData ret = new UserData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "WHERE RecordStatus<>4 AND syskey=" + syskey, "", conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));

		return ret;

	}

	public static DBRecord setDBRecord(UserData data) {
		DBRecord ret = define();
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("CreatedDate", data.getCreatedDate());
		ret.setValue("ModifiedDate", data.getModifiedDate());
		ret.setValue("UserId", data.getUserId());
		ret.setValue("UserName", data.getUserName());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());

		ret.setValue("usersysKey", data.getUsersyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t3", data.getT3());
		ret.setValue("t5", data.getT5());
		ret.setValue("t6", data.getT6());
		ret.setValue("t7", data.getT7());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		ret.setValue("n6", data.getN6());
		ret.setValue("n7", data.getN7());
		ret.setValue("n8", data.getN8());
		return ret;
	}

	public static DBRecord setDBRecord(UserRole data) {
		DBRecord ret = define("JUN002_A");
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());
		ret.setValue("usersysKey", data.getUsersyskey());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		return ret;
	}

	public static ResultTwo update(boolean isUserProfile, UserData obj, Connection conn) throws SQLException {
		ResultTwo res = new ResultTwo();
		String userPwd = "";

		if (ServerUtil.decryptPIN(obj.getT2()).equals("")) // for immediately
															// update data after
															// saving
		{
			String query = "Select t2 from UVM005_A where recordstatus <> 4 and syskey = ?";
			PreparedStatement stmt1 = conn.prepareStatement(query);
			stmt1.setLong(1, obj.getSyskey());
			ResultSet result = stmt1.executeQuery();
			boolean flag = false;
			while (result.next()) {
				userPwd = result.getString("t2");
				obj.setT2(userPwd);
				flag = true;
			}
			if (!flag) {
				res.setState(false);
				res.setMsgDesc("Cannot Update");
			}
		}

		String sql = "UPDATE UVM005_A SET n2=? , n3 = ?, t1=? ,t2=? ,t3=?,t4=? ,t5=?,"
				+ "t6=? ,t7=? , modifieddate = ?  Where RecordStatus<>4 AND Syskey=?";
		/*
		 * String sql = "UPDATE UVM005_A SET n2=" + obj.getN2() + " , n3 = " +
		 * obj.getN3() + ", t2='" + obj.getT2() + "' ,t3='" + obj.getT3() +
		 * "' ,t4='" + obj.getT4() + "' ,t5='" + obj.getT5() + "',t6='" +
		 * obj.getT6() + "' ,t7='" + obj.getT7() + "' , modifieddate = '" +
		 * GeneralUtil.datetoString() + "' Where RecordStatus<>4 AND Syskey=" +
		 * obj.getSyskey();
		 */
		PreparedStatement stmt = conn.prepareStatement(sql);
		int i = 1;
		stmt.setInt(i++, obj.getN2());
		stmt.setLong(i++, obj.getN3());
		stmt.setString(i++, obj.getT1());
		stmt.setString(i++, obj.getT2());
		stmt.setString(i++, obj.getT3());
		stmt.setString(i++, obj.getT4());
		stmt.setString(i++, obj.getT5());
		stmt.setString(i++, obj.getT6());
		stmt.setString(i++, obj.getT7());
		stmt.setString(i++, GeneralUtil.datetoString());
		stmt.setLong(i++, obj.getSyskey());
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			if (!isUserProfile) {
				UserRole jun = new UserRole();
				for (long l : obj.getRolesyskey()) {
					System.out.println("Rolesyskye :" + l);
					jun.setN1(obj.getSyskey());
					jun.setN2(l);
					sql = "DELETE FROM JUN002_A WHERE n1=?";
					stmt = conn.prepareStatement(sql);
					stmt.setLong(1, jun.getN1());
					stmt.executeUpdate();
				}

				for (long l : obj.getRolesyskey()) {
					if (l != 0) {
						jun.setRecordStatus(obj.getRecordStatus());
						jun.setSyncBatch(obj.getSyncBatch());
						jun.setSyncStatus(obj.getSyncStatus());
						jun.setUsersyskey(obj.getUsersyskey());
						jun.setN1(obj.getSyskey());
						jun.setN2(l);
						res = insertUserRole(jun, conn);
					}
				}
				if (res.isState()) {
					res.setState(true);
					res.setMsgDesc("Updated Successfully");

				} else
					res.setMsgDesc("Cannot Update");
			} else {

				res.setState(true);
				res.setMsgDesc("Updated Successfully");
				res.setLoginID(obj.getT1());
				// System.out.println(obj.getT1()+"This is loginid update
				// function in userdao");
				res.setPhNo(obj.getT4());
			}
		}
		return res;
	}

	/*
	 * public ResultTwo activateDeactivateUser(long syskey, Connection conn,
	 * String status, String userId) throws SQLException { ResultTwo res = new
	 * ResultTwo(); String date = new SimpleDateFormat("yyyyMMdd").format(new
	 * Date()); String sql = ""; int restatus = 0; if
	 * (status.equalsIgnoreCase("Activate")) { restatus = 2;
	 * res.setMsgCode("0000"); } else if (status.equalsIgnoreCase("Deactivate"))
	 * { restatus = 21; res = checkLockDeactive(status, syskey, conn); } try {
	 * if (res.getMsgCode().equals("0000")) {
	 * 
	 * sql =
	 * "UPDATE UVM012_A SET RecordStatus=?,t5=?,modifieddate=? WHERE Syskey IN(SELECT N4 FROM UVM005_A WHERE RecordStatus <> 4 and Syskey=?)"
	 * ;
	 * 
	 * PreparedStatement stmt = conn.prepareStatement(sql); int i = 1;
	 * stmt.setInt(i++, restatus); stmt.setString(i++, userId);
	 * stmt.setString(i++, date); stmt.setLong(i++, syskey); int rs =
	 * stmt.executeUpdate(); if (rs > 0) { sql =
	 * "UPDATE UVM005_A SET RecordStatus= ?,modifieddate=? WHERE RecordStatus <> 4 and Syskey=?"
	 * ; stmt = conn.prepareStatement(sql); int j = 1; stmt.setInt(j++,
	 * restatus); stmt.setString(j++, date); stmt.setLong(j++, syskey); rs =
	 * stmt.executeUpdate(); if (rs > 0) { res.setState(true); } else {
	 * res.setState(false); } }
	 * 
	 * } else { res.setState(false);
	 * 
	 * } } catch (Exception e) { e.printStackTrace(); }
	 * 
	 * return res; }
	 */

	public ResultTwo canUpdate(String status, String userId, Connection conn) throws SQLException {

		ResultTwo result = new ResultTwo();
		int totalCount = 0;
		String sql = "Select count(*) Count from TblSession Where UserId =? And Status = 0";

		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, userId);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			totalCount = res.getInt("Count");
		}

		if (totalCount > 0) {
			result.setMsgCode("0014");
			result.setMsgDesc("Cannot " + status + ".Please forced Sign Out");
		} else {
			result.setMsgCode("0000");
			result.setMsgDesc("Success");
		}

		return result;
	}

	public ResultTwo checkAgentParent(String parentID, Connection conn) throws SQLException {
		ResultTwo res = new ResultTwo();
		int count = 0;
		String query1 = "Select Count(*) Count  From Agent Where LogInID <> ParentID  And N1 = 0 And ParentID =  ? ";

		PreparedStatement stmt2 = conn.prepareStatement(query1);
		stmt2.setString(1, parentID);
		ResultSet ret = stmt2.executeQuery();
		while (ret.next()) {
			count = ret.getInt("Count");
		}
		if (count == 0) {
			res.setState(true);
		} else {
			res.setState(false);
			res.setMsgDesc("Cannot update.Please remove sub agent frist");
		}
		return res;
	}

	public ResultTwo checkCustomerID(long syskey, UCJunction[] data, Connection conn) throws SQLException {
		ResultTwo res = new ResultTwo();
		String cid = "";
		ArrayList<String> CusidList = new ArrayList<>();

		for (UCJunction da : data) {
			CusidList.add(da.getCustomerid());

		}
		;
		String customerid = "";
		for (int i = 0; i < CusidList.size(); i++) {
			if (i == CusidList.size() - 1) {
				customerid += "'" + CusidList.get(i) + "'";
			} else {
				customerid += "'" + CusidList.get(i) + "'" + ",";
			}
		}
		;

		String l_Query = "Select customerid from UCJunction where customerid in (?)";

		PreparedStatement pstmt = conn.prepareStatement(l_Query);
		pstmt.setString(1, customerid);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			cid = rs.getString("customerid");
		}
		if (cid.equalsIgnoreCase("")) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		return res;
	}
	/*
	 * public Result checkCustomerID(long syskey, UCJunction[] data, Connection
	 * conn) throws SQLException { Result res = new Result(); String cid = "";
	 * PreparedStatement stmt2 = conn.prepareStatement(query1);
	 * stmt2.setString(1, customerid); stmt2.setLong(2, syskey);
	 * ArrayList<String> CusidList=new ArrayList<>();
	 * 
	 * for(UCJunction da: data){ CusidList.add(da.getCustomerid());
	 * 
	 * }; String customerid= ""; for(int i=0;i<CusidList.size();i++){
	 * if(i==CusidList.size()-1){ customerid+="'"+CusidList.get(i)+"'"; }else{
	 * customerid +="'"+CusidList.get(i)+"'"+","; } }; // String query1 =
	 * "Select customerid from UCJunction where customerid in (?)"; String
	 * query1 =
	 * "Select customerid from UCJunction where customerid in ('013000020')";
	 * 
	 * PreparedStatement stmt2 = conn.prepareStatement(query1); //
	 * stmt2.setString(1, customerid);
	 * 
	 * ResultSet resph = stmt2.executeQuery(); while (resph.next()) { cid =
	 * resph.getString("customerid"); } if (cid.equalsIgnoreCase("")) {
	 * res.setState(true); } else { res.setState(false); } return res; }
	 */

	public ResultTwo checkCustomerID(UCJunction[] data, Connection conn) throws SQLException {
		ResultTwo res = new ResultTwo();
		String cid = "";
		// String query1 = "Select t7 from UVM012_A where recordstatus <> 4 and
		// t7= ?";

		ArrayList<String> CusidList = new ArrayList<>();

		for (UCJunction da : data) {
			CusidList.add(da.getCustomerid());

		}
		;
		String customerid = "";
		for (int i = 0; i < CusidList.size(); i++) {
			if (i == CusidList.size() - 1) {
				customerid += "'" + CusidList.get(i) + "'";
			} else {
				customerid += "'" + CusidList.get(i) + "'" + ",";
			}
		}
		;

		String l_query = "Select customerid from UCJunction where customerid in (?)";
		PreparedStatement p_st = conn.prepareStatement(l_query);
		p_st.setString(1, customerid);
		ResultSet rs = p_st.executeQuery();
		while (rs.next()) {
			cid = rs.getString("customerid");
		}

		if (cid.equalsIgnoreCase("")) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		return res;
	}

	public boolean checkFirstTimeLogin(long syskey, Connection conn) {
		boolean result = false;
		int flag = 0;
		String query = "SELECT N10 FROM UVM012_A WHERE Syskey IN(SELECT N4 FROM UVM005_A a WHERE a.Syskey=?)";
		try {
			PreparedStatement stmt1 = conn.prepareStatement(query);
			int k = 1;
			stmt1.setLong(k++, syskey);
			ResultSet rs = stmt1.executeQuery();
			while (rs.next()) {
				flag = rs.getInt("N10");
				if (flag == 1) {
					result = true;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public ResultTwo checkLockDeactive(String status, long syskey, Connection conn) throws SQLException {

		ResultTwo result = new ResultTwo();
		int totalCount = 0;
		String sql = "Select count(*) Count from TblSession Where UserId =(Select t1 from  UVM005_A Where Syskey = ?) And Status = 0";

		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			totalCount = res.getInt("Count");
		}

		if (totalCount > 0) {
			result.setMsgCode("0014");
			result.setMsgDesc("Cannot " + status + ".Please forced Sign Out.");
		} else {
			result.setMsgCode("0000");
			result.setMsgDesc("Success");
		}

		return result;
	}

	public boolean checkOtp(String userId, String phoneNo, Connection conn) throws SQLException {
		boolean result = false;

		String query = "select Top(1) T4 from UVM005_A Where RecordStatus <> 4 and t1 = ? and t4 = ? ";
		PreparedStatement ps = conn.prepareStatement(query);
		int i = 1;
		ps.setString(i++, userId);
		ps.setString(i++, phoneNo);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			result = true;
		}
		rs.close();
		ps.close();
		return result;
	}

	public ResultTwo checkParentAgent(String loginID, String parentID, Connection conn) throws SQLException {
		ResultTwo res = new ResultTwo();
		int count = 0;
		String query1 = "Select Count(*) Count  From Agent Where N1 = 1 And LogInID = ?  And ParentID = ? ";
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;
		stmt2.setString(i++, loginID);
		stmt2.setString(i++, parentID);
		ResultSet ret = stmt2.executeQuery();
		while (ret.next()) {
			count = ret.getInt("Count");
		}
		if (count == 0) {

			res.setState(true);
		} else {
			res.setState(false);
		}
		return res;
	}

	public ResultTwo checkPhoneNo(long syskey, String phNo, Connection conn) throws SQLException {
		ResultTwo res = new ResultTwo();
		String phno = "";
		String query1 = "Select t4 from UVM005_A where recordstatus <> 4 and n2 = 1 and t4= ? and  n4 <> ?";
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		stmt2.setString(1, phNo);
		stmt2.setLong(2, syskey);
		ResultSet resph = stmt2.executeQuery();
		while (resph.next()) {
			phno = resph.getString("T4");
		}
		if (phno.equalsIgnoreCase("")) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		return res;
	}

	public int checkSessionStatus(String userId, Connection conn) throws SQLException {
		int result = 1;
		String Id = "";
		int loginstatus = 0;
		String sql = "SELECT UserID,Status FROM tblSession WHERE UserID = ? AND LogInDateTime IN (SELECT MAX(LogInDateTime) FROM tblSession WHERE UserID = ? AND Status <> 8)";
		PreparedStatement pstmt = conn.prepareStatement(sql);
		int j = 1;
		if(userId.contains("959")){
			pstmt.setString(j++, '+' +userId);
			pstmt.setString(j++, '+' +userId);
		}else{
			pstmt.setString(j++, userId);
			pstmt.setString(j++, userId);
		}
		
		ResultSet res = pstmt.executeQuery();
		while (res.next()) {
			Id = res.getString("UserID");
			loginstatus = res.getInt("Status");

		}
		if (Id.equalsIgnoreCase("")) {
			result = 0;
		} else if (loginstatus != 0) {
			result = 0;
		}
		return result;
	}

	public ResultTwo checkUpateSubAgent(String loginID, String parentID, Connection conn) throws SQLException {
		ResultTwo res = new ResultTwo();
		int count = 0;

		if (parentID.equalsIgnoreCase("") || parentID.equalsIgnoreCase("null")) {
			res.setState(true);
		} else {
			String query1 = "Select Count (*) Count from Agent Where ParentId = ? And n1 = 0";
			System.out.println(" Sub parent agent : " + query1);
			PreparedStatement stmt2 = conn.prepareStatement(query1);
			stmt2.setString(1, loginID);
			ResultSet ret = stmt2.executeQuery();
			while (ret.next()) {
				count = ret.getInt("Count");
			}
			if (count == 0) {
				res.setState(true);
			} else {
				res.setState(false);
			}
		}
		return res;
	}

	public ResultTwo forcedlogoutbyId(String userId, Connection conn) throws SQLException {
		ResultTwo res = new ResultTwo();
		String query = "UPDATE tblSession SET Status = 6 WHERE UserID = ? AND status <> 8";
		PreparedStatement pstmt = conn.prepareStatement(query);
		int i = 1;
		if(userId.contains("959")){
			pstmt.setString(i++, '+' +userId);
		}else{
			pstmt.setString(i++, userId);
		}
		int rs = pstmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgCode("0000");
			res.setMsgDesc("Forced Sign Out Successfully");
		} else {
			res.setState(false);
			res.setMsgCode("0014");
			res.setMsgDesc("Forced Sign Out Failed");
		}
		return res;
	}

	// MMPPM
	public ArrayList<String> getActiveAcctListByLoginUserID(String aLoginUserID, Connection conn) throws SQLException {
		ArrayList<String> ret = new ArrayList<String>();
		String sql = "select AccountNo from MobileCAJunction where MobileUserID = ? and n1 = ? ";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, aLoginUserID);
		ps.setInt(2, 1);// 1 is active
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			String aCustomerID = rs.getString("AccountNo");
			ret.add(aCustomerID);
		}
		ps.close();
		rs.close();
		return ret;
	}

	// zmth

	/*
	 * public UserData readUserProfileDataBySyskey(long syskey, Connection conn)
	 * throws SQLException {
	 * 
	 * UserData ret = new UserData(); ArrayList<DBRecord> dbrs =
	 * DBMgr.getDBRecords(define(), "WHERE RecordStatus<>4 AND syskey=" +
	 * syskey, "", conn); if (dbrs.size() > 0) ret = getDBRecord(dbrs.get(0));
	 * 
	 * return ret;
	 * 
	 * }
	 */

	public ArrayList<ResultTwo> getAllAgent(Connection conn) throws SQLException {

		ArrayList<ResultTwo> datalist = new ArrayList<ResultTwo>();
		String agentName = "";

		String sql = "Select LogInID,Name from Agent Where RecordStatus <> 4 and n1 =1 ORDER BY ID";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			ResultTwo combo = new ResultTwo();
			agentName = res.getString("LogInID") + " , " + res.getString("Name");
			combo.setKeyString(agentName);
			combo.setKeyst(res.getString("LogInID"));
			datalist.add(combo);
		}

		return datalist;
	}

	public ArrayList<ResultTwo> getAllFeatures(Connection conn) throws SQLException {

		ArrayList<ResultTwo> datalist = new ArrayList<ResultTwo>();

		String sql = "select Code,Description from LOVDetails where hkey = (select syskey from LOVHeader where description = 'Feature') ORDER BY Syskey";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			ResultTwo combo = new ResultTwo();
			combo.setKeyst(res.getString("Code"));
			combo.setKeyString(res.getString("Description"));
			datalist.add(combo);
		}

		return datalist;
	}

	// get user setup list
	public UserViewDataset getAllUserData(String searchtext, Connection conn, String operation, boolean master)
			throws SQLException {
		int recordStatus = 0, n7 = 0;
		String searchText = searchtext.replace("'", "''");
		String whereClause = "";
		if (operation.equalsIgnoreCase("alluserprofile")) {
			if (searchText.equals("")) {
				whereClause = "where RecordStatus<>4 and role = 1 ";
			} else {
				whereClause = "where RecordStatus<>4 and role = 1" + " and (" + " t1 like '%" + searchText
						+ "%' or UN like '%" + searchText + "%' or t7 like '%" + searchText + "%' )";
			}

		} else if (operation.equalsIgnoreCase("allbankuser")) {
			if (master) {
				if (searchText.equals("")) {
					whereClause = "where RecordStatus<>4 and role <> 1 ";
				} else {
					whereClause = "where RecordStatus<>4 and role <> 1" + " and (" + " t1 like '%" + searchText
							+ "%' or UN like '%" + searchText + "%' or customerID like '%" + searchText + "%' )";
				}
			} else {
				if (searchText.equals("")) {
					whereClause = "where Master=0 and RecordStatus<>4 and role <> 1 ";
				} else {
					whereClause = "where Master=0 and RecordStatus<>4 and role <> 1" + " and (" + " t1 like '%"
							+ searchText + "%' or UN like '%" + searchText + "%' or customerID like '%" + searchText
							+ "%' )";
				}
			}

		} else if (operation.equalsIgnoreCase("activate")) {
			if (searchText.equals("")) {
				whereClause = " where (RecordStatus = 21 or RecordStatus = 1) and role = 1 ";
			} else {
				whereClause = " where ((RecordStatus = 21 or RecordStatus = 1) and role = 1)" + " and " + "(t1 like '%"
						+ searchText + "%' or UN like '%" + searchText + "%' or customerID like '%" + searchText
						+ "%' ) ";
			}

		} else {
			if (operation.equalsIgnoreCase("lock")) {
				recordStatus = 2;
				n7 = 0;

			} else if (operation.equalsIgnoreCase("deactivate")) {
				recordStatus = 2;
				n7 = 0;

			} else if (operation.equalsIgnoreCase("unlock")) {
				recordStatus = 2;
				n7 = 11;

			}

			if (searchText.equals("")) {
				whereClause = "where RecordStatus =" + recordStatus + " and lock =" + n7 + "and role = 1";
			} else {
				whereClause = "where RecordStatus =" + recordStatus + " and lock =" + n7 + "and role = 1 and "
						+ "(t1 like '%" + searchText + "%' or UN like '%" + searchText + "%' or customerID like '%"
						+ searchText + "%' ) ";
			}
		}

		ArrayList<UserViewData> ret = new ArrayList<UserViewData>();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(defineView(), whereClause, "ORDER BY t1,UN,customerID", conn);

		for (int i = 0; i < dbrs.size(); i++) {
			ret.add(getDBViewRecord(dbrs.get(i)));
		}

		UserViewDataset dataSet = new UserViewDataset();
		dataSet.setArlData(ret);
		return dataSet;
	}

	/*
	 * public CityStateComboDataSet getCityCombolist(String state, Connection
	 * conn) throws SQLException {
	 * 
	 * CityStateComboDataSet dataset = new CityStateComboDataSet();
	 * ArrayList<CityStateData> datalist = new ArrayList<CityStateData>();
	 * 
	 * String splitstate = ""; if (!(state.equals("") ||
	 * state.equalsIgnoreCase("null"))) { splitstate = state.substring(0, 4);
	 * 
	 * String sql =
	 * "select code,DespEng,DespMyan from addressref where code like '" +
	 * splitstate + "%' AND code <> '" + splitstate +
	 * "0000'  AND code <> '00000000' and DespEng <> 'All' ORDER BY Code";
	 * PreparedStatement stmt = conn.prepareStatement(sql); ResultSet res =
	 * stmt.executeQuery(); while (res.next()) { CityStateData combo = new
	 * CityStateData(); combo.setDespEng(res.getString("DespEng"));
	 * combo.setCode(res.getString("code")); datalist.add(combo); }
	 * 
	 * CityStateData[] dataarray = new CityStateData[datalist.size()]; dataarray
	 * = datalist.toArray(dataarray);
	 * 
	 * dataset.setData(dataarray); }
	 * 
	 * return dataset; }
	 */

	// public Result checkUpateSubAgent(String loginID, String parentID,
	// Connection conn) throws SQLException {
	// boolean response = false;
	// Result res = new Result();
	// int count = 0;
	//
	// String query1 = "Select Count (*) Count from Agent Where ParentId = '" +
	// loginID + "' And n1 = 0";
	// System.out.println(" Sub parent agent : " + query1);
	// PreparedStatement stmt2 = conn.prepareStatement(query1);
	// ResultSet ret = stmt2.executeQuery();
	// while (ret.next()) {
	// count = ret.getInt("Count");
	// }
	// if (count == 0) {
	//
	// res.setState(true);
	// } else {
	// res.setState(false);
	// }
	// return res;
	// }
	public UCJunction[] getCustIDByUID(String userid, Connection conn) {
		ArrayList<UCJunction> ret = new ArrayList<UCJunction>();
		UCJunction data = new UCJunction();
		try {
			String sql = "SELECT customerid FROM UCJunction WHERE UserId = ? ";

			PreparedStatement stat = conn.prepareStatement(sql);
			stat.setString(1, userid);
			ResultSet result = stat.executeQuery();

			while (result.next()) {
				data = new UCJunction();
				data.setCustomerid(result.getString("customerid"));
				ret.add(data);

			}

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		UCJunction[] dataarray = new UCJunction[ret.size()];

		for (int i = 0; i < ret.size(); i++) {
			dataarray[i] = ret.get(i);
		}
		return dataarray;
	}

	// MMPPM
	public ArrayList<String> getCustomerID(String aUserID, Connection conn) throws SQLException {
		ArrayList<String> ret = new ArrayList<String>();
		String sql = "select CustomerID from MobileCAJunction where MobileUserID = ? ";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, aUserID);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			String aCustomerID = rs.getString("CustomerID");
			ret.add(aCustomerID);
		}
		ps.close();
		rs.close();
		return ret;
	}

	public String getlasttimelogin(String userid, Connection con) {
		String lasttimelogin = "";
		String time = "";
		// String sql = "SELECT * FROM LoginHistory WHERE UserID = '"+userid+"'
		// AND Time = (SELECT MAX(Time) FROM LoginHistory WHERE UserID =
		// '"+userid+"' AND Activity = 'mobilesignin' AND Date = (SELECT
		// MAX(Date) FROM LoginHistory))";
		String sql = "SELECT * FROM LoginHistory WHERE Time = (SELECT MAX(Time) FROM LoginHistory WHERE  "
				+ "Activity = 'mobilesignin' AND UserID = ? AND Date = (SELECT MAX(Date) FROM LoginHistory "
				+ "WHERE UserID = ? ))";
		String date = "";
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			int i = 1;
			stmt.setString(i++, userid);
			stmt.setString(i++, userid);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				time = res.getString("Time");
				date = res.getString("Date");
			}
			if (!date.equalsIgnoreCase("") && !time.equalsIgnoreCase("")) {
				lasttimelogin = date.substring(6, date.length()) + "-" + date.substring(4, 6) + "-"
						+ date.substring(0, 4) + " " + time + "";
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return lasttimelogin;
	}

	public ArrayList<ResultTwo> getOrderList(Connection conn) throws SQLException {

		ArrayList<ResultTwo> datalist = new ArrayList<ResultTwo>();

		String sql = "select Code,Description from LOVDetails where hkey = (select syskey from LOVHeader where description = 'Order') ORDER BY Syskey";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			ResultTwo combo = new ResultTwo();
			combo.setKeyst(res.getString("Code"));
			combo.setKeyString(res.getString("Description"));
			datalist.add(combo);
		}

		return datalist;

	}

	/*
	 * public Result resetPasswordById(String userId, String adminId, Connection
	 * conn) throws SQLException { = new Result(); String phoneNo = ""; String
	 * result = ""; Long syskey = 0L; // boolean flag=false; res =
	 * canUpdate("reset", userId, conn); if (res.getMsgCode().equals("0000")) {
	 * String pwd = ServerUtil.encryptPIN(generatePassword(conn)); String query2
	 * = "update UVM005_A SET t2='" + pwd + "' " +
	 * " WHERE recordStatus <> 4 and t1='" + userId + "'"; PreparedStatement
	 * stmt2 = conn.prepareStatement(query2); int count = stmt2.executeUpdate();
	 * if (count > 0) { // after reset,update flag String query =
	 * "update UVM012_A SET n8=2 , n10 = 1 , t14 = '" +
	 * GeneralUtil.datetoString() + "'  WHERE recordStatus <> 4 and t1='" +
	 * userId + "'"; PreparedStatement stmt = conn.prepareStatement(query); int
	 * count2 = stmt.executeUpdate(); if (count2 > 0) { // get phone no String
	 * query3 =
	 * "SELECT t4,syskey from UVM005_A WHERE RecordStatus <> 4 and t1 ='" +
	 * userId + "'"; PreparedStatement stmt3 = conn.prepareStatement(query3);
	 * ResultSet rs = stmt3.executeQuery(); while (rs.next()) { phoneNo =
	 * rs.getString("t4"); syskey = rs.getLong("syskey"); } // before sending
	 * sms, update flag String query4 =
	 * "update UVM012_A SET n8=3 WHERE RecordStatus <> 4 and t1='" + userId +
	 * "'"; PreparedStatement stmt4 = conn.prepareStatement(query4); int count3
	 * = stmt4.executeUpdate(); if (count3 > 0) { // String smsMsg = "Dear
	 * Customer,Your new password : // "+ServerUtil.decryptPIN(pwd)+""; String
	 * Msg = ConnAdmin.readExternalUrl("SMSPwd"); String smsMsg =
	 * Msg.replace("<msg>", "" + ServerUtil.decryptPIN(pwd) + ""); OTPReqData
	 * smspwd = sendSMSPassword(phoneNo, smsMsg); // send // SMS // after
	 * sending sms,update flag String query5 =
	 * "update UVM012_A SET n8=? WHERE RecordStatus <> 4 and t1='" + userId +
	 * "'"; PreparedStatement stmt5 = conn.prepareStatement(query5); if
	 * (smspwd.getCode().equals("0000")) // send // successfully // case {
	 * stmt5.setInt(1, 4); } else if (smspwd.getCode().equals("0014")) // send
	 * // failed // or // invalid // phone no // case { stmt5.setInt(1, 5); }
	 * int count4 = stmt5.executeUpdate(); if (count4 > 0) { String query6 =
	 * "update UVM012_A SET t12=?,t13=? WHERE RecordStatus <> 4 and t1='" +
	 * userId + "'"; PreparedStatement stmt6 = conn.prepareStatement(query6);
	 * int j = 1; stmt6.setString(j++, smspwd.getCode()); stmt6.setString(j++,
	 * smspwd.getDesc()); int count5 = stmt6.executeUpdate(); if (count5 > 0) {
	 * 
	 * res = lockUnlockUser(adminId, syskey, conn, "unLock"); if (res.isState())
	 * { res.setMsgDesc("Password reset successfully."); } else {
	 * res.setMsgDesc("Password reset fail."); } } } } else {
	 * res.setState(false); res.setMsgDesc("Password reset fail."); }
	 * 
	 * } else { res.setState(false); res.setMsgDesc("Password reset fail."); } }
	 * else { res.setState(false); res.setMsgDesc("Password reset fail."); } }
	 * else { res.setState(false); }
	 * 
	 * return res; }
	 */

	public String getPhoneNo(String userId, Connection conn) throws SQLException {
		String phno = new String();

		String query = "select t4 from UVM005_A Where RecordStatus <> 4 and t1 = ? ";
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, userId);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			phno = rs.getString("T4");
		}
		rs.close();
		ps.close();
		return phno;
	}

	// CityState Combo //
	/*
	 * public CityStateComboDataSet getStateTypeCombolist(Connection conn)
	 * throws SQLException {
	 * 
	 * CityStateComboDataSet dataset = new CityStateComboDataSet();
	 * ArrayList<CityStateData> datalist = new ArrayList<CityStateData>();
	 * 
	 * String sql =
	 * "select code,DespEng,DespMyan from addressref where code like '%000000'  AND CODE <> '00000000' and DespEng <> 'All' ORDER BY Code"
	 * ; PreparedStatement stmt = conn.prepareStatement(sql); ResultSet res =
	 * stmt.executeQuery(); while (res.next()) { CityStateData combo = new
	 * CityStateData(); combo.setDespEng(res.getString("DespEng"));
	 * combo.setCode(res.getString("code")); datalist.add(combo); }
	 * 
	 * CityStateData[] dataarray = new CityStateData[datalist.size()]; dataarray
	 * = datalist.toArray(dataarray);
	 * 
	 * dataset.setData(dataarray); return dataset; }
	 */

	// atn
	public ArrayList<CityStateData> getStateListByUserID(String userid, Connection conn) throws SQLException {

		ArrayList<CityStateData> datalist = new ArrayList<CityStateData>();
		PreparedStatement stmt;
		String sql = "";
		String regioncode = "";
		String sql1 = "select code,DespEng,DespMyan from addressref a ,UVM005_A as b where a.code=b.t5 " + "and b.t1='"
				+ userid + "' ORDER BY Code";
		stmt = conn.prepareStatement(sql1);
		ResultSet res1 = stmt.executeQuery();
		while (res1.next()) {
			regioncode = res1.getString("code");
			if (regioncode.equals("00000000")) {
				// sql = "select code,DespEng,DespMyan from addressref where
				// code like '%000000' ORDER BY Code";
				sql = "select code,DespEng,DespMyan from addressref where code in ('00000000','13000000','10000000') ORDER BY Code";
			} else {
				sql = "select code,DespEng,DespMyan from addressref where code ='" + regioncode + "' ORDER BY Code";
			}
			stmt = conn.prepareStatement(sql);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				CityStateData combo = new CityStateData();
				combo.setDespEng(res.getString("DespEng"));
				combo.setCode(res.getString("code"));
				datalist.add(combo);
			}
		}
		return datalist;
	}

	// atn
	public ArrayList<CityStateData> getStateListByUserIDWithAll(String userid, Connection conn) throws SQLException {

		ArrayList<CityStateData> datalist = new ArrayList<CityStateData>();
		PreparedStatement stmt;
		String sql = "";
		String regioncode = "";
		String sql1 = "select code,DespEng,DespMyan from addressref a ,UVM005_A as b where a.code=b.t5 " + "and b.t1='"
				+ userid + "' ORDER BY Code";
		stmt = conn.prepareStatement(sql1);
		ResultSet res1 = stmt.executeQuery();
		while (res1.next()) {
			regioncode = res1.getString("code");
			if (regioncode.equals("00000000")) {
				// sql = "select code,DespEng,DespMyan from addressref where
				// code like '%000000' ORDER BY Code";
				sql = "select code,DespEng,DespMyan from addressref where code in ('00000000','13000000','10000000') ORDER BY Code";
			} else if (regioncode.equals("10000000")) {
				sql = "select code,DespEng,DespMyan from addressref where code in ('00000000','10000000') ORDER BY Code";
			} else if (regioncode.equals("13000000")) {
				sql = "select code,DespEng,DespMyan from addressref where code in ('00000000','13000000') ORDER BY Code";
			}
			stmt = conn.prepareStatement(sql);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				CityStateData combo = new CityStateData();
				combo.setDespEng(res.getString("DespEng"));
				combo.setCode(res.getString("code"));
				datalist.add(combo);
			}
		}
		return datalist;
	}

	/*public ArrayList<CityStateData> getStateTypeCombolistNew(Connection conn) throws SQLException {

		ArrayList<CityStateData> datalist = new ArrayList<CityStateData>();

		String sql = "select code,DespEng,DespMyan from addressref where code like '%000000'  AND CODE <> '00000000' and DespEng <> 'All' ORDER BY Code";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			CityStateData combo = new CityStateData();
			combo.setDespEng(res.getString("DespEng"));
			combo.setCode(res.getString("code"));
			datalist.add(combo);
		}

		return datalist;
	}*/

	public ResultTwo getUserId(String phNo, Connection con) {
		String userId = "";
		ResultTwo result = new ResultTwo();

		String query = "select t1 from UVM005_A where RecordStatus<>4 and n2 = 1 and t4=? ";
		try {
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setString(1, phNo);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				userId = res.getString("t1");
			}
			if (!userId.equals("")) {
				result.setPhNo(phNo);
				result.setUserId(userId);
				result.setMsgCode("0000");
				result.setMsgDesc("Registered User with requested phone number exists");
			} else {
				result.setPhNo(phNo);
				result.setUserId(userId);
				result.setMsgCode("0014");
				result.setMsgDesc("Registered User with requested phone number doesn't exist");

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public String getUserName(String userid, Connection con) {
		String username = "";
		String sql = "select t2 from UVM012_A where RecordStatus<>4 and t1=? ";
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, userid);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				username = res.getString("t2");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return username;
	}

	public UserData getUserNameAndNrc(String userId, Connection conn) throws SQLException {

		UserData ret = new UserData();
		String pwd = "";
		String query = "Select  a.T1 'Id',a.T7 'Nrc',a.T2 'Password',b.T2 'Name' from UVM005_A a Join UVM012_A b ON a.T1=b.t1 where "
				+ "(a.t1=? COLLATE SQL_Latin1_General_CP1_CS_AS ) and a.recordStatus = 2 and a.n7 = 0 and a.n2 = 1";
		/*
		 * String query =
		 * "Select  a.T4 'Id',a.T7 'Nrc',a.T2 'Password',b.T2 'Name' from UVM005_A a Join UVM012_A b ON a.T1=b.t1 where (a.t4='"
		 * + userId +
		 * "' COLLATE SQL_Latin1_General_CP1_CS_AS ) and a.recordStatus = 2 and a.n7 = 0 and a.n2 = 1"
		 * ;
		 */
		PreparedStatement stmt1 = conn.prepareStatement(query);
		stmt1.setString(1, userId);
		ResultSet result = stmt1.executeQuery();
		while (result.next()) {
			ret.setT1(result.getString("Id"));
			ret.setT7(result.getString("Nrc"));
			ret.setName(result.getString("Name"));
			pwd = result.getString("Password");
			ret.setT2(ServerUtil.decryptPIN(pwd));
		}
		return ret;
	}

	public UserData getUserNameAndStatus(String userId, Connection conn) throws SQLException {

		UserData ret = new UserData();
		String query = "";
		PreparedStatement stmt;
		if(userId.contains("959")){
			query = "select userid 'Id',name from UserRegistration where userid = ?";	
			stmt = conn.prepareStatement(query);
			stmt.setString(1,'+'+userId);
		}else{
			query = "Select  a.T1 'Id',b.T2 'Name' from UVM005_A a Join UVM012_A b ON a.T1=b.t1 where"
					+ " (a.t1=? COLLATE SQL_Latin1_General_CP1_CS_AS ) and a.recordStatus <> 4 and a.n7 = 0 ";
			stmt = conn.prepareStatement(query);
			stmt.setString(1,userId);
		}	
		
		ResultSet result = stmt.executeQuery();
		while (result.next()) {
			ret.setT1(result.getString("Id").replace("+",""));
			ret.setName(result.getString("Name"));
		}
		ret.setLoginstatus(checkSessionStatus(userId, conn));

		return ret;
	}

	public ResultTwo insertAgent(UserData data, Connection conn) throws SQLException {
		ResultTwo res = new ResultTwo();

		String l_query = "delete from Agent where LogInId = ?";
		PreparedStatement st = conn.prepareStatement(l_query);
		st.setString(1, data.getT1());
		st.executeUpdate();

		if (data.getN3() == 1) {
			String query = "INSERT INTO Agent (LogInID ,AgentAccNumber ,Name ,RecordStatus , ParentID ,n1,n2 )"
					+ "VALUES (?,?,?,?,?,?,?)";
			PreparedStatement psmt = conn.prepareStatement(query);

			int i = 1;
			psmt.setString(i++, data.getT1());

			psmt.setString(i++, data.getAccountNumber().trim());
			psmt.setString(i++, data.getName());
			psmt.setInt(i++, 1);
			if (data.getParentID().equals("0") || data.getParentID().equals("") || data.getParentID().equals("null")) {
				psmt.setString(i++, data.getT1()); // data.getParentID()
				psmt.setInt(i++, 1);
			} else {
				psmt.setString(i++, data.getParentID());
				psmt.setInt(i++, 0);
			}
			System.out.println("check account => " + data.getChkAccount());
			if (data.getChkAccount().equals("true")) {
				psmt.setInt(i++, 0);
			} else if (data.getChkGL().equals("true")) {
				psmt.setInt(i++, 1);
			} else {
				res.setState(false);
			}

			if (psmt.executeUpdate() > 0) {
				res.setState(true);
			} else {
				res.setState(false);
			}
		} else {
			res.setState(true);
		}

		return res;
	}

	public ResultTwo lockUnlockUser(String userid, long syskey, Connection conn, String status) throws SQLException {
		ResultTwo res = new ResultTwo();
		String sql = "";
		int restatus = 0;
		int retryCount = 3;
		if (status.equalsIgnoreCase("Lock")) {
			restatus = 11;
			retryCount = 3;
			res = checkLockDeactive(status, syskey, conn);
		} else if (status.equalsIgnoreCase("Unlock")) {
			restatus = 0;
			retryCount = 0;
			res.setMsgCode("0000");
		}
		try {
			if (res.getMsgCode().equals("0000")) {
				sql = "UPDATE UVM005_A SET modifieddate = ?, n7=?, n1=? WHERE Syskey=?";
				PreparedStatement stmt = conn.prepareStatement(sql);
				int j = 1;
				stmt.setString(j++, GeneralUtil.datetoString());
				stmt.setInt(j++, restatus);
				stmt.setInt(j++, retryCount);
				stmt.setLong(j++, syskey);
				int rs = stmt.executeUpdate();
				if (rs > 0) {
					String query = "UPDATE UVM012_A SET modifieddate = ? ,t5 = ? WHERE Syskey IN(SELECT N4 FROM UVM005_A WHERE Syskey=?)";
					PreparedStatement stmt1 = conn.prepareStatement(query);
					int i = 1;
					stmt1.setString(i++, GeneralUtil.datetoString());
					stmt1.setString(i++, userid);
					stmt1.setLong(i++, syskey);
					int rst = stmt1.executeUpdate();
					if (rst > 0) {
						res.setState(true);// lock/unlock successfully
					} else {
						res.setState(false);
					}
				}

			} else {
				res.setState(false);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return res;
	}

	/*
	 * public ResultTwo markPrinted(String userId, Connection conn) throws
	 * SQLException { ResultTwo res = new ResultTwo(); int printCount = 0; try {
	 * String query =
	 * "Select n8 From UVM012_A WHERE Syskey IN(SELECT N4 FROM UVM005_A WHERE recordStatus =2 and t1=?)"
	 * ; PreparedStatement stmt1 = conn.prepareStatement(query);
	 * stmt1.setString(1, userId); ResultSet result = stmt1.executeQuery();
	 * while (result.next()) { printCount = result.getInt("n8"); } if
	 * (printCount == 1) { res.setState(false); } else {// pwd send sms part
	 * String query3 =
	 * "update UVM012_A SET n8=? WHERE RecordStatus <> 4 and t1=? ";
	 * PreparedStatement stmt3 = conn.prepareStatement(query3);
	 * 
	 * stmt3.setInt(1, 1); stmt3.setString(2, userId);
	 * 
	 * if (stmt3.executeUpdate() > 0) { res.setState(true); } else {
	 * res.setState(false); }
	 * 
	 * String name = "", pwd = "", phoneNo = "", loginID = ""; String query2 =
	 * "Select a.T1 'loginID', a.T4 'Phone',a.T2 'Password',b.T2 'Name' from UVM005_A a Join UVM012_A b ON a.T1=b.t1 where "
	 * +
	 * "(a.t1=? COLLATE SQL_Latin1_General_CP1_CS_AS ) and a.recordStatus = 2 and a.n7 = 0 and a.n2 = 1"
	 * ; PreparedStatement p_stmt = conn.prepareStatement(query2);
	 * p_stmt.setString(1, userId); ResultSet set = p_stmt.executeQuery(); while
	 * (set.next()) { name = set.getString("Name"); pwd =
	 * ServerUtil.decryptPIN(set.getString("Password")); phoneNo =
	 * set.getString("Phone"); loginID = set.getString("loginID"); }
	 * 
	 * ArrayList<SMSSettingData> adata = new ArrayList<SMSSettingData>();
	 * SMSSettingDao sdao = new SMSSettingDao(); SMSParameter smsParam = new
	 * SMSParameter(); SMSMgr smgr = new SMSMgr(); adata =
	 * sdao.getMessageSetting("2", "11", "", conn); smsParam.setPassword(pwd);
	 * smsParam.setFromName(name); smsParam.setLoginID(loginID);
	 * smgr.sendMessageService(adata, smsParam, phoneNo, ""); } } catch
	 * (Exception e) { e.printStackTrace(); }
	 * 
	 * return res; }
	 */

	public UserData readByUserID(String aUserID, Connection conn) throws SQLException {

		UserData ret = new UserData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "WHERE RecordStatus<>4 AND t1='" + aUserID + "'", "",
				conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));

		return ret;

	}

	public UserData readUserProfileDataBySyskey(long syskey, Connection conn) throws SQLException {
		UserData ret = new UserData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "WHERE RecordStatus<>4 AND syskey=" + syskey, "", conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));

		return ret;

	}

	/*
	 * public ResultTwo resetPasswordById(String userId, String adminId,
	 * Connection conn) throws SQLException { ResultTwo res = new ResultTwo();
	 * String phoneNo = ""; Long syskey = 0L; String name = ""; String loginID =
	 * ""; // boolean flag=false; res = canUpdate("reset", userId, conn); if
	 * (res.getMsgCode().equals("0000")) { String pwd =
	 * ServerUtil.encryptPIN(generatePassword(conn)); String query2 =
	 * "update UVM005_A SET t2=?  WHERE recordStatus <> 4 and t1=? ";
	 * PreparedStatement stmt2 = conn.prepareStatement(query2); int i = 1;
	 * stmt2.setString(i++, pwd); stmt2.setString(i++, userId); int count =
	 * stmt2.executeUpdate(); if (count > 0) { // after reset,update flag String
	 * query =
	 * "update UVM012_A SET n8=2 , n10 = 1 , t14 = ?  WHERE recordStatus <> 4 and t1=? "
	 * ; PreparedStatement stmt = conn.prepareStatement(query); int k = 1;
	 * stmt.setString(k++, GeneralUtil.datetoString()); stmt.setString(k++,
	 * userId); int count2 = stmt.executeUpdate(); if (count2 > 0) { // get
	 * phone no and name String query3 =
	 * "SELECT a.t1,a.t4,a.syskey, b.t2 from UVM005_A a inner join UVM012_A b "
	 * + "on a.t1 = b.t1 WHERE a.RecordStatus <> 4 and  a.t1 =? ";
	 * PreparedStatement stmt3 = conn.prepareStatement(query3);
	 * stmt3.setString(1, userId); ResultSet rs = stmt3.executeQuery(); while
	 * (rs.next()) { loginID = rs.getString("t1"); phoneNo = rs.getString("t4");
	 * syskey = rs.getLong("syskey"); name = rs.getString("t2"); } // before
	 * sending sms, update flag String query4 =
	 * "update UVM012_A SET n8=3 WHERE RecordStatus <> 4 and t1=? ";
	 * PreparedStatement stmt4 = conn.prepareStatement(query4);
	 * stmt4.setString(1, userId); int count3 = stmt4.executeUpdate(); if
	 * (count3 > 0) {
	 * 
	 * String Msg = ConnAdmin.readExternalUrl("SMSPwd"); String smsMsg =
	 * Msg.replace("<msg>", "" + ServerUtil.decryptPIN(pwd) + ""); OTPReqData
	 * smspwd = sendSMSPassword(phoneNo, smsMsg); // send // SMS
	 * 
	 * ArrayList<SMSSettingData> adata = new ArrayList<SMSSettingData>();
	 * SMSSettingDao sdao = new SMSSettingDao(); ResponseData ret = new
	 * ResponseData(); SMSParameter smsParam = new SMSParameter(); SMSMgr smgr =
	 * new SMSMgr(); adata = sdao.getMessageSetting("2", "7", "", conn);
	 * smsParam.setPassword(ServerUtil.decryptPIN(pwd));
	 * smsParam.setFromName(name); smsParam.setLoginID(loginID); ret =
	 * smgr.sendMessageService(adata, smsParam, phoneNo, "");// send // SMS //
	 * after // sending // sms, // update // flag
	 * 
	 * String query5 =
	 * "update UVM012_A SET n8=? WHERE RecordStatus <> 4 and t1=? ";
	 * PreparedStatement stmt5 = conn.prepareStatement(query5); if
	 * (ret.getCode().equals("0000")) { // send successfully // case
	 * stmt5.setInt(1, 4); } else if (ret.getCode().equals("0014")) {// send
	 * failed // or // invalid // phone no // case stmt5.setInt(1, 5); }
	 * stmt5.setString(2, userId); int count4 = stmt5.executeUpdate(); if
	 * (count4 > 0) { String query6 =
	 * "update UVM012_A SET t12=?,t13=? WHERE RecordStatus <> 4 and t1=? ";
	 * PreparedStatement stmt6 = conn.prepareStatement(query6); int j = 1;
	 * stmt6.setString(j++, ret.getCode()); stmt6.setString(j++, ret.getDesc());
	 * stmt6.setString(j++, userId); int count5 = stmt6.executeUpdate(); if
	 * (count5 > 0) { res = lockUnlockUser(adminId, syskey, conn, "unLock"); if
	 * (res.isState()) { res.setMsgDesc("Password reset successfully."); } else
	 * { res.setMsgDesc("Password reset fail."); } } } } else {
	 * res.setState(false); res.setMsgDesc("Password reset fail."); } } else {
	 * res.setState(false); res.setMsgDesc("Password reset fail."); } } else {
	 * res.setState(false); res.setMsgDesc("Password reset fail."); } } else {
	 * res.setState(false); } return res; }
	 */

	/*
	 * public OTPReqData sendSMSPassword(String phoneNo, String smsMsg) {
	 * OTPReqData data = new OTPReqData(); try {
	 * 
	 * // GeneralUtil.readConfig(); // if //
	 * (com.nirvasoft.rp.shared.ServerGlobal.getSMSAggregator().equalsIgnoreCase
	 * ("MIT")) // { String smsSetting =
	 * GeneralUtil.readConfig("SMSAggregator"); if
	 * (smsSetting.equalsIgnoreCase("MIT")) { Response smsresponse =
	 * MITMessageService.sendSMSMessage(phoneNo, smsMsg, "text");
	 * 
	 * if (smsresponse.getRESULT() != null) {// PURE SUCCESS if
	 * (smsresponse.getRESULT().toLowerCase().contains("success")) {
	 * data.setCode("0000"); data.setDesc("Success"); data.setphno(phoneNo);
	 * 
	 * } else { data.setCode("0014"); data.setDesc("Unknown SMS Error"); } }
	 * else { data.setCode("0014"); data.setDesc(smsresponse.getERRORCODE()); }
	 * } else if
	 * (com.nirvasoft.rp.shared.ServerGlobal.getSMSAggregator().equalsIgnoreCase
	 * ("TCB")) { ResponseData tcb_data =
	 * TCBMessageService.sendMailSMSService(phoneNo, "", smsMsg, ""); if
	 * (tcb_data.getCode() != null) {// PURE SUCCESS if
	 * (tcb_data.getCode().equals("0000")) { data.setCode("0000");
	 * data.setDesc("Success"); data.setphno(phoneNo);
	 * 
	 * } else { data.setCode("0014"); data.setDesc("Unknown SMS Error"); } }
	 * else { data.setCode("0014"); data.setDesc(tcb_data.getDesc()); } }
	 * 
	 * } catch (Exception e) { e.printStackTrace(); data.setCode("0014");
	 * data.setDesc("Failed"); // data.setResetpwd(pwd); } return data; }
	 */

	public int updateCreatedDate(long syskey, Connection conn) {
		int rs = 0;
		String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sql = "UPDATE UVM012_A SET createddate=? WHERE Syskey IN(SELECT N4 FROM UVM005_A a WHERE a.Syskey=?)";
		try {
			PreparedStatement stmt = conn.prepareStatement(sql);
			int j = 1;
			stmt.setString(j++, date);
			stmt.setLong(j++, syskey);
			rs = stmt.executeUpdate();
			if (rs > 0) {
				return rs;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return rs;
	}

}