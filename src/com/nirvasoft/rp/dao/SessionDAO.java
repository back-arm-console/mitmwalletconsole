package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.nirvasoft.rp.framework.ResultTwo;
import com.nirvasoft.rp.shared.MSigninResponseData;
import com.nirvasoft.rp.shared.ResponseData;
import com.nirvasoft.rp.util.GeneralUtil;

public class SessionDAO {

	private String mTableName = "tblSession";

	public MSigninResponseData checkLogin(String userId, Connection conn) throws SQLException {
		MSigninResponseData response = new MSigninResponseData();
		String lastlogintimesuccess = "";
		String max_time = "";
		int status = -1;
		int res = 0;
		String date = "";
		String lastActivity_dtime = "";
		GeneralUtil gUtil = new GeneralUtil();

		try {
			String sql = "SELECT * FROM " + mTableName
					+ "  WHERE UserID = ? AND LogInDateTime=(SELECT MAX(LogInDateTime) FROM " + mTableName
					+ " WHERE Status <> 8 AND (UserID = ? COLLATE SQL_Latin1_General_CP1_CS_AS))";
			PreparedStatement ps = conn.prepareStatement(sql);
			int i = 1;
			ps.setString(i++, userId);
			ps.setString(i++, userId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				status = rs.getInt("Status");
				lastActivity_dtime = rs.getString("LastActivityDateTime");
			}
			if (status != 0) {
				response.setCode("0000");
			}
			if (status == 0) {
				/*
				 * if (!lastActivity_dtime.equalsIgnoreCase("") &&
				 * !lastActivity_dtime.equalsIgnoreCase("null")) { max_time =
				 * lastActivity_dtime.replace("-", "").substring(0, 17); }
				 * String currentdtime = new SimpleDateFormat(
				 * "yyyyMMdd HH:mm:ss").format(new Date()); String sessionLimit
				 * = ConnAdmin.readExternalUrl("ADMSESSIONTIME"); // SESSIONTIME
				 * if (gUtil.checkTimeStamp(max_time, currentdtime,
				 * sessionLimit) == true) { response.setCode("0014"); } else {
				 * // forcelogout }
				 */
				res = forceLogout(userId, conn); // force logout -- user can
				// signin
				if (res > 0) // Updating status for force logout succeed
				{
					response.setCode("0000");
				} else // Updating status for force logout failed
				{
					response.setCode("0014");
				}
				// response.setCode("0014");
			}

			ps.close();
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public int forceLogout(String userId, Connection conn) throws SQLException, ParseException {
		int res = 0;
		String updatequery = "Update " + mTableName
				+ " set Status = ? WHERE UserID = ? AND LogInDateTime=(SELECT MAX(LogInDateTime) FROM " + mTableName
				+ " WHERE Status <> 8 AND UserID = ?)";
		PreparedStatement ps = conn.prepareStatement(updatequery);
		int i = 1;
		ps.setInt(i++, 5);
		ps.setString(i++, userId);
		ps.setString(i++, userId);
		res = ps.executeUpdate();
		return res;
	}

	private long generateKey(String aRef, Connection mConn) throws SQLException {
		long l_RefKey = 0;
		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		String sql = "Insert Into " + mTableName + "(UserID,LogInDateTime,Status) values(?,?,?)";
		PreparedStatement preparedstatement = mConn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		int i = 1;
		preparedstatement.setString(i++, aRef);
		preparedstatement.setString(i++, date);
		preparedstatement.setInt(i++, 0);
		preparedstatement.executeUpdate();
		ResultSet rs = preparedstatement.getGeneratedKeys();
		if (rs != null && rs.next()) {
			l_RefKey = rs.getLong(1);
		}

		rs.close();
		preparedstatement.close();

		return l_RefKey;
	}

	public ResultTwo insertSession(String userId, Connection conn) throws SQLException {
		ResultTwo res = new ResultTwo();
		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		long key = 0L;
		key = generateKey(userId, conn);
		String query = "UPDATE " + mTableName
				+ " SET UserID = ?, SessionID = ?,LogInDateTime = ?,LastActivityDateTime = ?,Status = ? WHERE AutoKey = ?";
		PreparedStatement pstmt = conn.prepareStatement(query);
		int i = 1;
		pstmt.setString(i++, userId);
		pstmt.setString(i++, String.valueOf(key));
		pstmt.setString(i++, date);
		pstmt.setString(i++, date);
		pstmt.setInt(i++, 0);
		pstmt.setLong(i++, key);
		int rst = pstmt.executeUpdate();
		if (rst > 0) {
			res.setState(true);
			res.setMsgCode("0000");
			res.setMsgDesc("Insert Successfully");
			res.setSessionID(String.valueOf(key));

		} else {
			res.setState(false);
			res.setMsgCode("0014");
			res.setMsgDesc("Insert Failed");
		}
		pstmt.close();
		return res;
	}

	public ResultTwo logout(String sessionId, Connection conn) throws SQLException {
		ResultTwo res = new ResultTwo();
		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		String logoutquery = "Update " + mTableName
				+ " set Status = ?,LastActivityDateTime = ?,LogOutDateTime = ?  WHERE sessionID = ?";
		PreparedStatement ps = conn.prepareStatement(logoutquery);
		int i = 1;
		ps.setInt(i++, 9);
		ps.setString(i++, date);
		ps.setString(i++, date);
		ps.setString(i++, sessionId);
		int result = ps.executeUpdate();
		if (result > 0) {
			res.setState(true);
			res.setMsgCode("0000");
			res.setMsgDesc("Update Successfully");
		} else {
			res.setState(false);
			res.setMsgCode("0014");
			res.setMsgDesc("Update Failed");
		}

		return res;
	}

	public ResponseData updateActivityTime(String sessionId, String userID,String formID, Connection conn) throws SQLException {
		ResponseData res = new ResponseData();
		String lastactivitydt = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
		String Status = "";

		String sql = "select Status from " + mTableName + " Where SessionID = ? AND UserID = ?;";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, sessionId);
		ps.setString(2, userID);
		ResultSet rs = ps.executeQuery();

		if (rs.next()) {
			Status = rs.getString("Status");
			if (Status.equals("0")) {
				String query1 = "UPDATE " + mTableName
						+ " SET LastActivityDateTime = ?  Where SessionID = ? AND UserID = ?;";
				PreparedStatement p_stmt1 = conn.prepareStatement(query1);
				p_stmt1.setString(1, lastactivitydt);
				p_stmt1.setString(2, sessionId);
				p_stmt1.setString(3, userID);
				int row = p_stmt1.executeUpdate();

				if (row > 0) {
					if(!formID.equals("")){
						String query = "SELECT t2,t3 FROM UVM022_A  WHERE syskey IN ("
								+ "SELECT n2 FROM UVM023_A WHERE  n1 IN ("
								+ "SELECT n2 FROM JUN002_A WHERE n1 IN ("
								+ "SELECT syskey FROM UVM005_A WHERE (t1=?)))) "
								+ "AND t3=? and RecordStatus<> 4  and n1=1 order by n6";
						PreparedStatement pstmt = conn.prepareStatement(query);
						pstmt.setString(1, userID);
						pstmt.setString(2, formID);
						ResultSet rset = pstmt.executeQuery();
						if (rset.next()) {
							res.setCode("0000");
							res.setDesc("Permission access");
						}else {
							res.setCode("0016");
							res.setDesc("User don't get permission");
						}
					}else{
						res.setCode("0000");
						res.setDesc("Update successfully");
					}

				} else {
					res.setCode("0016");
					res.setDesc("Updated Failed");
				}

				p_stmt1.close();
			} else {
				res.setCode("0016");
				res.setDesc("Your session has expired.Please signin again...");
			}
		} else {
			res.setCode("0016");
			res.setDesc("Your session has expired.Please signin again...");
		}

		ps.close();

		return res;
	}

	public ResponseData updateActivityTime_Old(String sessionId, Connection conn) throws SQLException {
		ResponseData res = new ResponseData();
		String lastactivitydt = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
		String Status = "";

		String sql = "select Status from " + mTableName + " Where SessionID = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, sessionId);
		ResultSet rs = ps.executeQuery();

		if (rs.next()) {
			Status = rs.getString("Status");
			if (Status.equals("0")) {
				String query1 = "UPDATE " + mTableName + " SET LastActivityDateTime = ?  Where SessionID = ? ";
				PreparedStatement p_stmt1 = conn.prepareStatement(query1);
				p_stmt1.setString(1, lastactivitydt);
				p_stmt1.setString(2, sessionId);
				int row = p_stmt1.executeUpdate();

				if (row > 0) {
					res.setCode("0000");
					res.setDesc("Updated Successfully");

				} else {
					res.setCode("0014");
					res.setDesc("Updated Failed");
				}

				p_stmt1.close();
			} else {
				res.setCode("0016");
				res.setDesc("Your session has expired.Please signin again...");
			}
		} else {
			res.setCode("0016");
			res.setDesc("Your session has expired.Please signin again...");
		}

		ps.close();

		return res;
	}

	public ResponseData updateAndcheckActivityTime(String sessionId, String userId, Connection conn)
			throws SQLException {
		ResponseData res = new ResponseData();
		String lastactivitydt = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
		String Status = "";

		String sql = "select Status from " + mTableName + " Where SessionID = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, sessionId);
		ResultSet rs = ps.executeQuery();

		if (rs.next()) {
			Status = rs.getString("Status");
			if (Status.equals("0")) {
				String query1 = "UPDATE " + mTableName + " SET LastActivityDateTime = ?  Where SessionID = ? ";
				PreparedStatement p_stmt1 = conn.prepareStatement(query1);
				p_stmt1.setString(1, lastactivitydt);
				p_stmt1.setString(2, sessionId);
				int row = p_stmt1.executeUpdate();

				if (row > 0) {
					res.setCode("0000");
					res.setDesc("Updated Successfully");

				} else {
					res.setCode("0014");
					res.setDesc("Updated Failed");
				}

				p_stmt1.close();
			} else {
				res.setCode("0016");
				res.setDesc("Your session has expired.Please signin again...");
			}

		} else {
			res.setCode("0016");
			res.setDesc("Your session has expired.Please signin again...");
		}

		ps.close();

		return res;
	}

}
