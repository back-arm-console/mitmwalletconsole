package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.nirvasoft.rp.shared.VersionHistoryData;
import com.nirvasoft.rp.shared.VersionHistoryDataSet;

public class VersionHistoryDao {

	public VersionHistoryData delete(long autoKey, Connection conn) throws SQLException {
		VersionHistoryData response = new VersionHistoryData();

		PreparedStatement preparedStatement = null;

		try {
			String query = "DELETE FROM VersionHistory WHERE AutoKey = ?;";
			preparedStatement = conn.prepareStatement(query);
			preparedStatement.setLong(1, autoKey);

			if (preparedStatement.executeUpdate() > 0) {
				response.setMsgCode("0000");
				response.setMsgDesc("Deleted Successfully.");
				response.setState(true);
			} else {
				response.setMsgCode("0014");
				response.setMsgDesc("Deleted Fail.");
				response.setState(false);
			}
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}

		return response;
	}

	public VersionHistoryDataSet getAll(String searchText, int pageSize, int currentPage, Connection conn)
			throws SQLException {
		VersionHistoryDataSet response = null;

		String query = "";
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int startPage = (currentPage - 1) * pageSize;
		int endPage = pageSize + startPage;

		try {
			if (searchText.equals("")) {
				query = query.concat("SELECT * FROM ("
						+ "SELECT ROW_NUMBER() OVER (ORDER BY AutoKey DESC) AS RowNum, AutoKey, AppCode, VersionKey, Version, VersionTitle, Description, StartDate, DueDate, Remark, Status "
						+ "FROM VersionHistory " + ") AS RowConstrainedResult WHERE (RowNum > ? AND RowNum <= ?);");

				preparedStatement = conn.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE,
						ResultSet.CONCUR_READ_ONLY);
				preparedStatement.setInt(1, startPage);
				preparedStatement.setInt(2, endPage);
				resultSet = preparedStatement.executeQuery();
			} else {
				searchText = searchText.replace("'", "''");

				query = query.concat("SELECT * FROM ("
						+ "SELECT ROW_NUMBER() OVER (ORDER BY AutoKey DESC) AS RowNum, AutoKey, AppCode, VersionKey, Version, VersionTitle, Description, StartDate, DueDate, Remark, Status "
						+ "FROM VersionHistory "
						+ "WHERE AppCode IN (SELECT ld1.code FROM LOVDetails ld1 WHERE ld1.Description LIKE ? AND ld1.HKey = (SELECT lh1.SysKey FROM LOVHeader lh1 WHERE lh1.Description = 'App Code')) "
						+ "OR CAST(status AS NVARCHAR) IN (SELECT ld2.code FROM LOVDetails ld2 WHERE ld2.Description LIKE ? AND ld2.HKey = (SELECT lh2.SysKey FROM LOVHeader lh2 WHERE lh2.Description = 'Version Status Code')) "
						+ "OR VersionKey LIKE ? " + "OR Version LIKE ? " + "OR VersionTitle LIKE ? "
						+ "OR Description LIKE ? " + "OR StartDate LIKE ? " + "OR DueDate LIKE ? " + "OR Remark LIKE ? "
						+ ") AS RowConstrainedResult WHERE (RowNum > ? AND RowNum <= ?);");
				preparedStatement = conn.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE,
						ResultSet.CONCUR_READ_ONLY);

				for (int i = 1; i < 10; i++) {
					preparedStatement.setString(i, "%" + searchText + "%");
				}

				preparedStatement.setInt(10, startPage);
				preparedStatement.setInt(11, endPage);
				resultSet = preparedStatement.executeQuery();
			}

			int totalRow = 0;
			resultSet.last();
			totalRow = resultSet.getRow();

			if (totalRow == 0) {
				response = new VersionHistoryDataSet();
				response.setMsgCode("0000");
				response.setMsgDesc("Data Not Found.");
				response.setSearchText(searchText);
				response.setPageSize(pageSize);
				response.setCurrentPage(currentPage);
				response.setData(null);
				response.setTotalCount(0);
			}

			if (totalRow != 0) {
				resultSet.beforeFirst();
				VersionHistoryData[] dataarr = new VersionHistoryData[totalRow];
				int x = 0;

				while (resultSet.next()) {
					VersionHistoryData data = new VersionHistoryData();
					data.setAutokey(resultSet.getString("AutoKey"));
					data.setAppcode(resultSet.getString("AppCode"));
					data.setVersionkey(resultSet.getString("VersionKey"));
					data.setVersion(resultSet.getString("Version"));
					data.setVersiontitle(resultSet.getString("VersionTitle"));
					data.setDescription(resultSet.getString("Description"));
					data.setStartdate(resultSet.getString("StartDate"));
					data.setStartdate(data.getStartdate().substring(6) + "/" + data.getStartdate().substring(4, 6) + "/"
							+ data.getStartdate().substring(0, 4));
					data.setDuedate(resultSet.getString("DueDate"));
					data.setDuedate(data.getDuedate().substring(6) + "/" + data.getDuedate().substring(4, 6) + "/"
							+ data.getDuedate().substring(0, 4));
					data.setRemark(resultSet.getString("Remark"));
					data.setStatus(resultSet.getString("Status"));
					dataarr[x++] = data;
				}

				preparedStatement.close();
				resultSet.close();
				query = "";
				preparedStatement = null;
				resultSet = null;

				if (searchText.equals("")) {
					query = query.concat("SELECT COUNT(*) AS total FROM VersionHistory;");
					preparedStatement = conn.prepareStatement(query);
					resultSet = preparedStatement.executeQuery();
					resultSet.next();
				} else {
					query = query.concat("SELECT COUNT(*) AS total " + "FROM VersionHistory "
							+ "WHERE AppCode IN (SELECT ld1.code FROM LOVDetails ld1 WHERE ld1.Description LIKE ? AND ld1.HKey = (SELECT lh1.SysKey FROM LOVHeader lh1 WHERE lh1.Description = 'App Code')) "
							+ "OR CAST(status AS NVARCHAR) IN (SELECT ld2.code FROM LOVDetails ld2 WHERE ld2.Description LIKE ? AND ld2.HKey = (SELECT lh2.SysKey FROM LOVHeader lh2 WHERE lh2.Description = 'Version Status Code')) "
							+ "OR VersionKey LIKE ? " + "OR Version LIKE ? " + "OR VersionTitle LIKE ? "
							+ "OR Description LIKE ? " + "OR StartDate LIKE ? " + "OR DueDate LIKE ? "
							+ "OR Remark LIKE ?;");
					preparedStatement = conn.prepareStatement(query);

					for (int i = 1; i < 10; i++) {
						preparedStatement.setString(i, "%" + searchText + "%");
					}

					resultSet = preparedStatement.executeQuery();
					resultSet.next();
				}

				response = new VersionHistoryDataSet();
				response.setMsgCode("0000");
				response.setMsgDesc("Selected Successfully.");
				response.setSearchText(searchText);
				response.setPageSize(pageSize);
				response.setCurrentPage(currentPage);
				response.setData(dataarr);
				response.setTotalCount(resultSet.getInt("total"));
			}
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}

			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}

		return response;
	}

	/*public String getAutoKeyByAppCodeAndVersionKey(String appCode, String versionKey, Connection conn)
			throws SQLException {
		String autoKey = "";

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			String query = "SELECT AutoKey As AutoKeyString FROM VersionHistory WHERE AppCode = ? AND VersionKey = ?;";
			preparedStatement = conn.prepareStatement(query);
			preparedStatement.setString(1, appCode);
			preparedStatement.setString(2, versionKey);

			resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				autoKey = resultSet.getString("AutoKeyString");
			}
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}

			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}

		return autoKey;
	}*/

	/*public String getLastVersionKeyByAppCode(String appCode, Connection conn) throws SQLException {
		String lastVersionKey = "";

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			String query = "SELECT MAX(VersionKey) AS LastVersionKey FROM VersionHistory where AppCode = ?;";
			preparedStatement = conn.prepareStatement(query);
			preparedStatement.setString(1, appCode);

			resultSet = preparedStatement.executeQuery();
			resultSet.next();
			lastVersionKey = resultSet.getString("LastVersionKey");

			if (lastVersionKey == null) {
				lastVersionKey = "0";
			}
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}

			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}

		return lastVersionKey;
	}*/

	public VersionHistoryData getOneByAutoKey(long autoKey, Connection conn) throws SQLException {
		VersionHistoryData response = new VersionHistoryData();

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			String query = "SELECT * FROM VersionHistory WHERE AutoKey = ?;";
			preparedStatement = conn.prepareStatement(query);
			preparedStatement.setLong(1, autoKey);
			resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				response.setAutokey(resultSet.getString("AutoKey"));
				response.setAppcode(resultSet.getString("AppCode"));
				response.setVersionkey(resultSet.getString("VersionKey"));
				response.setVersion(resultSet.getString("Version"));
				response.setVersiontitle(resultSet.getString("VersionTitle"));
				response.setDescription(resultSet.getString("Description"));
				response.setStartdate(resultSet.getString("StartDate"));
				response.setStartdate(response.getStartdate().substring(0, 4) + "/"
						+ response.getStartdate().substring(4, 6) + "/" + response.getStartdate().substring(6));
				response.setDuedate(resultSet.getString("DueDate"));
				response.setDuedate(response.getDuedate().substring(0, 4) + "/" + response.getDuedate().substring(4, 6)
						+ "/" + response.getDuedate().substring(6));
				response.setRemark(resultSet.getString("Remark"));
				response.setStatus(resultSet.getString("Status"));
				response.setMsgCode("0000");
				response.setMsgDesc("Selected Successfully.");
				response.setState(true);
			} else {
				response.setMsgCode("0014");
				response.setMsgDesc("There Is No Version History With This AutoKey.");
				response.setState(false);
			}
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}

			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}

		return response;
	}

	/*public boolean isExistedByAppCodeAndVersion(String appCode, String version, Connection conn) throws SQLException {
		String query = "SELECT COUNT(*) AS total FROM VersionHistory WHERE AppCode = ? AND Version = ?;";
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		boolean isExisted = true;

		try {
			preparedStatement = conn.prepareStatement(query);
			preparedStatement.setString(1, appCode);
			preparedStatement.setString(2, version);

			resultSet = preparedStatement.executeQuery();
			resultSet.next();
			int i = resultSet.getInt("total");

			if (i > 0) {
				isExisted = true;
			} else {
				isExisted = false;
			}
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}

			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}

		return isExisted;
	}*/

	/*public VersionHistoryData save(VersionHistoryData request, Connection conn) throws SQLException {
		VersionHistoryData response = new VersionHistoryData();

		PreparedStatement preparedStatement = null;

		try {
			String query = "INSERT INTO VersionHistory (AppCode, VersionKey, Version, VersionTitle, Description, StartDate, DueDate, Remark, Status)"
					+ " VALUES (?,?,?,?,?,?,?,?,?);";
			preparedStatement = conn.prepareStatement(query);
			int i = 1;
			preparedStatement.setString(i++, request.getAppcode());
			preparedStatement.setLong(i++, Long.parseLong(request.getVersionkey()));
			preparedStatement.setString(i++, request.getVersion());
			preparedStatement.setString(i++, request.getVersiontitle());
			preparedStatement.setString(i++, request.getDescription());
			preparedStatement.setString(i++, request.getStartdate());
			preparedStatement.setString(i++, request.getDuedate());
			preparedStatement.setString(i++, request.getRemark());
			preparedStatement.setLong(i, Long.parseLong(request.getStatus()));

			if (preparedStatement.executeUpdate() > 0) {
				response.setMsgCode("0000");
				response.setMsgDesc("Saved Successfully.");
				response.setState(true);
			} else {
				response.setMsgCode("0014");
				response.setMsgDesc("Saved Fail.");
				response.setState(false);
			}
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}

		return response;
	}*/

	/*public VersionHistoryData update(VersionHistoryData request, Connection conn) throws SQLException {
		VersionHistoryData response = new VersionHistoryData();

		PreparedStatement preparedStatement = null;

		try {
			String query = "UPDATE VersionHistory SET Version = ?, VersionTitle = ?, Description = ?, StartDate = ?, DueDate = ?, Remark = ?, Status = ? WHERE AutoKey = ?;";
			preparedStatement = conn.prepareStatement(query);
			int i = 1;
			preparedStatement.setString(i++, request.getVersion());
			preparedStatement.setString(i++, request.getVersiontitle());
			preparedStatement.setString(i++, request.getDescription());
			preparedStatement.setString(i++, request.getStartdate());
			preparedStatement.setString(i++, request.getDuedate());
			preparedStatement.setString(i++, request.getRemark());
			preparedStatement.setLong(i++, Long.parseLong(request.getStatus()));
			preparedStatement.setLong(i, Long.parseLong(request.getAutokey()));

			if (preparedStatement.executeUpdate() > 0) {
				response.setMsgCode("0000");
				response.setMsgDesc("Updated Successfully.");
				response.setState(true);
			} else {
				response.setMsgCode("0014");
				response.setMsgDesc("Updated Fail.");
				response.setState(false);
			}
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}

		return response;
	}*/

}