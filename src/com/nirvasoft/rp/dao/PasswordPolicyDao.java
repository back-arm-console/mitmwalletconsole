package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.nirvasoft.rp.framework.ResultTwo;

public class PasswordPolicyDao {

	public ResultTwo checkPasswordPolicyPattern(String pwd, Connection con) throws SQLException {
		ResultTwo response = new ResultTwo();
		int sc = 0, uc = 0, lc = 0, no = 0, maxno = 0, minno = 0;
		String sql = "SELECT * FROM tblPasswordpolicy";
		PreparedStatement stat = con.prepareStatement(sql);
		ResultSet result = stat.executeQuery();
		while (result.next()) {
			sc = result.getInt("SpecialCharacter");
			uc = result.getInt("UpperCaseCharacter");
			lc = result.getInt("LowerCaseCharacter");
			no = result.getInt("Number");
			minno = result.getInt("MinimumLength");
			maxno = result.getInt("MaximumLength");

		}

		String pattern = "(?=(.*[a-z]){" + lc + "})(?=(.*[A-Z]){" + uc + "})(?=(.*[0-9]){" + no + "})(?=(.*[!#$%&*?@]){"
				+ sc + "}).{" + minno + "," + maxno + "}";

		System.out.println("Regular Expression :: " + pattern);
		boolean ret = pwd.matches(pattern);
		if (ret) {
			response.setMsgCode("0000");
			response.setMsgDesc("Success");
			response.setState(true);
		} else {
			response.setMsgCode("0014");
			String desc = "";
			desc = "Password Format (" + pwd + ") is not correct! * Password is case-sensitive and at least " + minno
					+ " characters and no more than " + maxno + " characters.";

			if (sc > 0) {
				desc += "* Password must contain " + sc + " special characters";
			}
			if (uc > 0) {
				desc += ", " + uc + " uppercase characters";
			}
			if (lc > 0) {
				desc += ", " + lc + " lowercase characters";
			}
			if (no > 0) {
				desc += " and " + no + " number.";
			}

			response.setMsgDesc(desc);
			response.setState(false);
		}
		return response;
	}
}
