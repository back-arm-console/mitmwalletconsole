package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class VersionHistoryData {

	private String sessionID;
	private String userID;
	private String msgCode;
	private String msgDesc;
	private boolean state;

	private String autokey;
	private String appcode;
	private String versionkey;
	private String version;
	private String versiontitle;
	private String description;
	private String startdate;
	private String duedate;
	private String remark;
	private String status;
	private String t1;
	private String t2;
	private String t3;
	private String n1;
	private String n2;
	private String n3;

	public VersionHistoryData() {
		this.clearProperties();
	}

	private void clearProperties() {
		this.sessionID = "";
		this.userID = "";
		this.msgCode = "";
		this.msgDesc = "";

		this.autokey = "";
		this.appcode = "";
		this.versionkey = "";
		this.version = "";
		this.versiontitle = "";
		this.description = "";
		this.startdate = "";
		this.duedate = "";
		this.remark = "";
		this.status = "";
		this.t1 = "";
		this.t2 = "";
		this.t3 = "";
		this.n1 = "";
		this.n2 = "";
		this.n3 = "";
	}

	public String getAppcode() {
		return appcode;
	}

	public String getAutokey() {
		return autokey;
	}

	public String getDescription() {
		return description;
	}

	public String getDuedate() {
		return duedate;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public String getN1() {
		return n1;
	}

	public String getN2() {
		return n2;
	}

	public String getN3() {
		return n3;
	}

	public String getRemark() {
		return remark;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getStartdate() {
		return startdate;
	}

	public String getStatus() {
		return status;
	}

	public String getT1() {
		return t1;
	}

	public String getT2() {
		return t2;
	}

	public String getT3() {
		return t3;
	}

	public String getUserID() {
		return userID;
	}

	public String getVersion() {
		return version;
	}

	public String getVersionkey() {
		return versionkey;
	}

	public String getVersiontitle() {
		return versiontitle;
	}

	public boolean isState() {
		return state;
	}

	public void setAppcode(String appcode) {
		this.appcode = appcode;
	}

	public void setAutokey(String autokey) {
		this.autokey = autokey;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDuedate(String duedate) {
		this.duedate = duedate;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public void setN1(String n1) {
		this.n1 = n1;
	}

	public void setN2(String n2) {
		this.n2 = n2;
	}

	public void setN3(String n3) {
		this.n3 = n3;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void setVersionkey(String versionkey) {
		this.versionkey = versionkey;
	}

	public void setVersiontitle(String versiontitle) {
		this.versiontitle = versiontitle;
	}

	@Override
	public String toString() {
		return "VersionHistoryData [sessionID=" + sessionID + ", userID=" + userID + ", msgCode=" + msgCode
				+ ", msgDesc=" + msgDesc + ", state=" + state + ", autokey=" + autokey + ", appcode=" + appcode
				+ ", versionkey=" + versionkey + ", version=" + version + ", versiontitle=" + versiontitle
				+ ", description=" + description + ", startdate=" + startdate + ", duedate=" + duedate + ", remark="
				+ remark + ", status=" + status + ", t1=" + t1 + ", t2=" + t2 + ", t3=" + t3 + ", n1=" + n1 + ", n2="
				+ n2 + ", n3=" + n3 + "]";
	}

}