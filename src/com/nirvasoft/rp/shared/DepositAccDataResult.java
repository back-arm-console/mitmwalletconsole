package com.nirvasoft.rp.shared;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DepositAccDataResult {

	public String code = "";
	public String desc = "";
	public DepositAccData[] datalist = null;

	public DepositAccDataResult() {
		clearProperty();
	}

	void clearProperty() {
		code = "";
		desc = "";
		datalist = null;
	}

	public String getCode() {
		return code;
	}

	public DepositAccData[] getDatalist() {
		return datalist;
	}

	public String getDesc() {
		return desc;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDatalist(DepositAccData[] datalist) {
		this.datalist = datalist;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	public String toString() {
		return "DepositAccDataResult [code=" + code + ", desc=" + desc + ", datalist=" + Arrays.toString(datalist)
				+ "]";
	}

}
