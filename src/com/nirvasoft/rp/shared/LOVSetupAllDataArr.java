package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class LOVSetupAllDataArr {

	private LOVSetupAllData[] data;
	private String searchText;
	private int totalCount;
	private int currentPage;
	private int pageSize;

	public LOVSetupAllDataArr() {
		clearProperty();
	}

	void clearProperty() {
		data = null;
		searchText = "";
		totalCount = 0;
		currentPage = 0;
		pageSize = 0;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public LOVSetupAllData[] getData() {
		return data;
	}

	public int getPageSize() {
		return pageSize;
	}

	public String getSearchText() {
		return searchText;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setData(LOVSetupAllData[] data) {
		this.data = data;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

}
