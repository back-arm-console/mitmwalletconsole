package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SessionData {

	private String sessionID;
	private String userID;

	public SessionData() {
		this.clearProperties();
	}

	private void clearProperties() {
		this.sessionID = "";
		this.userID = "";
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getUserID() {
		return userID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	@Override
	public String toString() {
		return "SessionData [sessionID=" + sessionID + ", userID=" + userID + "]";
	}

}