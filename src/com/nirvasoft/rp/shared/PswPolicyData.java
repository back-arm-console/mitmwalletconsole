package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class PswPolicyData {

	private int pswminlength;
	private int pswmaxlength;
	private int spchar;
	private int upchar;
	private int lowerchar;
	private int pswno;

	private String msgCode = "";
	private String msgDesc = "";
	private String sessionID = "";
	private String userID = "";

	public PswPolicyData() {
		clearProperties();
	}

	private void clearProperties() {

		this.pswminlength = 0;
		this.pswmaxlength = 0;
		this.spchar = 0;
		this.upchar = 0;
		this.pswno = 0;
		this.lowerchar = 0;
		this.sessionID = "";
		this.msgCode = "";
		this.msgDesc = "";
		this.userID = "";
	}

	public int getLowerchar() {
		return lowerchar;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public int getPswmaxlength() {
		return pswmaxlength;
	}

	public int getPswminlength() {
		return pswminlength;
	}

	public int getPswno() {
		return pswno;
	}

	public String getSessionID() {
		return sessionID;
	}

	public int getSpchar() {
		return spchar;
	}

	public int getUpchar() {
		return upchar;
	}

	public String getUserID() {
		return userID;
	}

	public void setLowerchar(int lowerchar) {
		this.lowerchar = lowerchar;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public void setPswmaxlength(int pswmaxlength) {
		this.pswmaxlength = pswmaxlength;
	}

	public void setPswminlength(int pswminlength) {
		this.pswminlength = pswminlength;
	}

	public void setPswno(int pswno) {
		this.pswno = pswno;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setSpchar(int spchar) {
		this.spchar = spchar;
	}

	public void setUpchar(int upchar) {
		this.upchar = upchar;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

}