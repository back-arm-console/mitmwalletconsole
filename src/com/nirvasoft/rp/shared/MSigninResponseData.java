package com.nirvasoft.rp.shared;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MSigninResponseData {
	private String code;
	private String desc;
	private String sessionID;
	private String userid;
	private DepositAccData[] debitacc;

	public MSigninResponseData() {
		this.code = "";
		this.desc = "";
		this.sessionID = "";
		this.userid = "";
		debitacc = null;
	}

	public String getCode() {
		return code;
	}

	public DepositAccData[] getDebitacc() {
		return debitacc;
	}

	public String getDesc() {
		return desc;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getUserid() {
		return userid;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDebitacc(DepositAccData[] debitacc) {
		this.debitacc = debitacc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	@Override
	public String toString() {
		return "{code=" + code + ", desc=" + desc + ", sessionID=" + sessionID + ", userid=" + userid + ", debitacc="
				+ Arrays.toString(debitacc) + "}";
	}

}
