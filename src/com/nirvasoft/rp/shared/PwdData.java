package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PwdData {

	private long syskey;
	private String password;
	private String newpassword;
	private String confirmnewpassword;
	private String userid;
	private String sessionID;
	private String iv;
	private String dm;
	private String salt;

	public PwdData() {
		clearProperties();
		// TODO Auto-generated constructor stub
	}

	private void clearProperties() {
		// TODO Auto-generated method stub
		syskey = 0;
		password = "";
		newpassword = "";
		confirmnewpassword = "";
		userid = "";
		sessionID = "";
		iv = "";
		dm = "";
		salt = "";
	}

	public String getConfirmnewpassword() {
		return confirmnewpassword;
	}

	public String getDm() {
		return dm;
	}

	public String getIv() {
		return iv;
	}

	public String getNewpassword() {
		return newpassword;
	}

	public String getPassword() {
		return password;
	}

	public String getSalt() {
		return salt;
	}

	public String getSessionID() {
		return sessionID;
	}

	public long getSyskey() {
		return syskey;
	}

	public String getUserid() {
		return userid;
	}

	public void setConfirmnewpassword(String confirmnewpassword) {
		this.confirmnewpassword = confirmnewpassword;
	}

	public void setDm(String dm) {
		this.dm = dm;
	}

	public void setIv(String iv) {
		this.iv = iv;
	}

	public void setNewpassword(String newpassword) {
		this.newpassword = newpassword;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	@Override
	public String toString() {
		return "PwdData [syskey=" + syskey + ", password=" + password + ", newpassword=" + newpassword
				+ ", confirmnewpassword=" + confirmnewpassword + ", userid=" + userid + ", sessionId=" + sessionID
				+ "]";
	}

}
