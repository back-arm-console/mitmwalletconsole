package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DepositAccData {

	private String depositacc;
	private String ccy;
	private double avlbal;
	private String acctype;

	public String getAcctype() {
		return acctype;
	}

	public double getAvlbal() {
		return avlbal;
	}

	public String getCcy() {
		return ccy;
	}

	public String getDepositacc() {
		return depositacc;
	}

	public void setAcctype(String acctype) {
		this.acctype = acctype;
	}

	public void setAvlbal(double avlbal) {
		this.avlbal = avlbal;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public void setDepositacc(String depositacc) {
		this.depositacc = depositacc;
	}

	@Override
	public String toString() {
		return "DepositAccData [depositacc=" + depositacc + ", ccy=" + ccy + ", avlbal=" + avlbal + ", acctype="
				+ acctype + "]";
	}

}
