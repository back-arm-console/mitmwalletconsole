package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class VersionHistoryDataSet {

	private String sessionID;
	private String userID;
	private String msgCode = "";
	private String msgDesc = "";
	private String searchText;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private VersionHistoryData[] data;

	public VersionHistoryDataSet() {
		clearProperty();
	}

	private void clearProperty() {
		sessionID = "";
		userID = "";
		msgCode = "";
		msgDesc = "";
		searchText = "";
		totalCount = 0;
		currentPage = 0;
		pageSize = 0;
		data = null;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public VersionHistoryData[] getData() {
		return data;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public int getPageSize() {
		return pageSize;
	}

	public String getSearchText() {
		return searchText;
	}

	public String getSessionID() {
		return sessionID;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public String getUserID() {
		return userID;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setData(VersionHistoryData[] data) {
		this.data = data;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

}