
package com.nirvasoft.rp.service;

import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.framework.Bean001;
import com.nirvasoft.rp.framework.Lov3;
import com.nirvasoft.rp.framework.Menu;
import com.nirvasoft.rp.framework.Profile;
import com.nirvasoft.rp.framework.Ref;
import com.nirvasoft.rp.framework.ResultTwo;
import com.nirvasoft.rp.framework.ServerSession;
import com.nirvasoft.rp.mgr.DBLOVSetupMgr;
import com.nirvasoft.rp.mgr.MenuDataMgr;
import com.nirvasoft.rp.mgr.PwdMgr;
import com.nirvasoft.rp.mgr.RoleDataMgr;
import com.nirvasoft.rp.mgr.RoleMenuDataMgr;
import com.nirvasoft.rp.mgr.Service001Mgr;
import com.nirvasoft.rp.mgr.SessionMgr;
import com.nirvasoft.rp.mgr.UserDataMgr;
import com.nirvasoft.rp.mgr.UserRoleViewDataMgr;
import com.nirvasoft.rp.mgr.VersionHistoryMgr;
import com.nirvasoft.rp.shared.CityStateData;
import com.nirvasoft.rp.shared.LOVSetupAllData;
import com.nirvasoft.rp.shared.LOVSetupAllDataArr;
import com.nirvasoft.rp.shared.MSigninResponseData;
import com.nirvasoft.rp.shared.PswPolicyData;
import com.nirvasoft.rp.shared.PwdData;
import com.nirvasoft.rp.shared.ResponseData;
import com.nirvasoft.rp.shared.SessionData;
import com.nirvasoft.rp.shared.VersionHistoryData;
import com.nirvasoft.rp.shared.VersionHistoryDataSet;
import com.nirvasoft.rp.users.data.MenuData;
import com.nirvasoft.rp.users.data.MenuDataArr;
import com.nirvasoft.rp.users.data.MenuViewDataArr;
import com.nirvasoft.rp.users.data.RoleData;
import com.nirvasoft.rp.users.data.RoleDatas;
import com.nirvasoft.rp.users.data.RoleMenuData;
import com.nirvasoft.rp.users.data.UserData;
import com.nirvasoft.rp.users.data.UserRoleViewData;
import com.nirvasoft.rp.users.data.UserViewDataArr;
import com.nirvasoft.rp.util.AesUtil;

@Path("/service001")
public class Service001 {
	public static String userid = "";
	public static String username = "";
	@Context
	HttpServletRequest request;
	@Context
	private HttpServletResponse response;
	@javax.ws.rs.core.Context
	ServletContext context;

	@POST
	@Path("changePassword")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ResultTwo changePassword(PwdData pData) {
		getPath();
		ResponseData sessionRes = new ResponseData();
		sessionRes = new SessionMgr().updateActivityTime(pData.getSessionID(), pData.getUserid(), "change-password");

		PwdMgr mgr = new PwdMgr();
		ResultTwo ret = new ResultTwo();
		if (sessionRes.getCode().equalsIgnoreCase("0000")) {
			if (!pData.getIv().equals("")) {
				AesUtil util = new AesUtil(128, 1000);
				String password = util.decrypt(pData.getSalt(), pData.getIv(), pData.getPassword());
				pData.setPassword(password);
				String newpassword = util.decrypt(pData.getSalt(), pData.getIv(), pData.getNewpassword());
				pData.setNewpassword(newpassword);
			}
			ret = mgr.checkPasswordPolicyPattern(pData.getNewpassword());

			if (ret.getMsgCode().equals("0000")) {
				ret = new ResultTwo();
				ret = mgr.changePwd(pData);
			}
		} else {
			ret.setMsgCode(sessionRes.getCode());
			ret.setMsgDesc(sessionRes.getDesc());
		}

		return ret;
	}

	@GET
	@Path("checkSession")
	@Produces(MediaType.APPLICATION_JSON)
	public ResultTwo checkSession(@QueryParam("sessionID") String sessionId) {
		ResultTwo res = new ResultTwo();
		ResponseData sessionRes = new ResponseData();
		SessionMgr mgr = new SessionMgr();
		getPath();
		sessionRes = mgr.updateActivityTime_Old(sessionId);
		res.setMsgCode(sessionRes.getCode());
		res.setMsgDesc(sessionRes.getDesc());
		return res;
	}

	//
	@POST
	@Path("checkSessionTime")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ResponseData checkSessionPost(SessionData request) {
		getPath();
		ResponseData response = null;
		response = new SessionMgr().updateActivityTime(request.getSessionID(), request.getUserID(), "");
		return response;
	}

	@POST
	@Path("deleteMenu")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ResultTwo deleteMenuData(MenuData meu) {
		getPath();
		ResponseData sessionRes = new ResponseData();
		sessionRes = new SessionMgr().updateActivityTime(meu.getSessionId(), meu.getUserId(), "Menu");

		ResultTwo res = new ResultTwo();

		if (sessionRes.getCode().equalsIgnoreCase("0000")) {
			String skey = meu.get_key();
			String isParent = meu.getIsParent();
			long syskey = Long.parseLong(skey);
			res = MenuDataMgr.deleteMenuData(syskey, isParent);
		} else {
			res.setMsgCode(sessionRes.getCode());
			res.setMsgDesc(sessionRes.getDesc());
		}

		return res;
	}

	@POST
	@Path("deleteRole")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ResultTwo deleteRoleData(String skey, @QueryParam("sessionID") String sessionId,
			@QueryParam("userID") String userID) {

		getPath();
		ResultTwo res = new ResultTwo();
		ResponseData sessionRes = new ResponseData();
		SessionMgr session_mgr = new SessionMgr();

		skey = skey.substring(1, skey.length() - 1);
		long syskey = 0;
		syskey = Long.parseLong(skey);

		sessionRes = session_mgr.updateActivityTime(sessionId, userID, "Role");
		if (sessionRes.getCode().equalsIgnoreCase("0000")) {
			res = RoleDataMgr.deleteRoleData(syskey);
		} else {
			res.setMsgCode(sessionRes.getCode());
			res.setMsgDesc(sessionRes.getDesc());
		}

		return res;
	}

	@POST
	@Path("deleteUser")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ResultTwo deleteUserData(UserData data, @QueryParam("sessionID") String sessionID,
			@QueryParam("userIDForSession") String userID) {
		getPath();
		ResultTwo res = new ResultTwo();
		SessionMgr session_mgr = new SessionMgr();
		ResponseData sessionRes = new ResponseData();
		sessionRes = session_mgr.updateActivityTime(sessionID, userID, "user-setup");
		if (sessionRes.getCode().equalsIgnoreCase("0000")) {
			res = UserDataMgr.deleteUserData(data.getSyskey(), data.getUserId(), data.getParentID(), data.getT1());
		} else {
			res.setMsgCode(sessionRes.getCode());
			res.setMsgDesc(sessionRes.getDesc());
		}
		return res;

	}

	@POST
	@Path("deleteVersionHistory")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public VersionHistoryData deleteVersionHistory(VersionHistoryData request) {
		VersionHistoryData response = null;

		getPath();
		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime(request.getSessionID(), request.getUserID(),
				"version-history");

		if (sessionState.getCode().equals("0000")) {
			if (request.getAutokey() != null && request.getAutokey().trim().length() != 0) {
				breakForParseException: {
					try {
						Long.parseLong(request.getAutokey().trim());
					} catch (Exception e) {
						response = new VersionHistoryData();
						response.setMsgDesc("Invalid Autokey.");
						response.setState(false);

						break breakForParseException;
					}

					response = new VersionHistoryMgr().delete(Long.parseLong(request.getAutokey().trim()));
				}
			} else {
				response = new VersionHistoryData();
				response.setMsgDesc("Blank Autokey.");
				response.setState(false);
			}
		} else {
			response = new VersionHistoryData();
			response.setMsgCode(sessionState.getCode());
			response.setMsgDesc(sessionState.getDesc());
			response.setState(false);
		}

		return response;
	}

	@GET
	@Path("forcedlogoutbyId")
	@Produces(MediaType.APPLICATION_JSON)
	public ResultTwo forcedlogoutbyId(@QueryParam("userId") String userId, @QueryParam("sessionID") String sessionId,
			@QueryParam("userIDForSession") String userIDForSession) {
		getPath();
		ResultTwo res = new ResultTwo();
		UserDataMgr u_mgr = new UserDataMgr();
		SessionMgr session_mgr = new SessionMgr();
		ResponseData sessionRes = new ResponseData();

		sessionRes = session_mgr.updateActivityTime(sessionId, userIDForSession, "forced-sign-out");
		if (sessionRes.getCode().equalsIgnoreCase("0000")) {
			res = u_mgr.forcedlogoutbyId(userId);
		} else {
			res.setMsgCode(sessionRes.getCode());
			res.setMsgDesc(sessionRes.getDesc());
		}

		return res;
	}

	@POST
	@Path("getAllVersionHistory")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public VersionHistoryDataSet getAllVersionHistory(VersionHistoryDataSet request) {
		VersionHistoryDataSet response = null;

		getPath();
		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime(request.getSessionID(), request.getUserID(),
				"version-history-list");

		if (sessionState.getCode().equals("0000")) {
			response = new VersionHistoryMgr().getAll(request.getSearchText().trim(), request.getPageSize(),
					request.getCurrentPage());
		} else {
			response = new VersionHistoryDataSet();
			response.setMsgCode(sessionState.getCode());
			response.setMsgDesc(sessionState.getDesc());
		}

		return response;
	}

	@POST
	@Path("getLOVbySysKey")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LOVSetupAllData getLOVbySysKey(LOVSetupAllData p) {
		LOVSetupAllData res = new LOVSetupAllData();
		DAOManager.AbsolutePath = context.getRealPath("");
		DBLOVSetupMgr lovmgr = new DBLOVSetupMgr();
		res = lovmgr.getLOVbySysKey(p.getSysKey());
		return res;
	}

	@GET
	@Path("getMainList")
	@Produces(MediaType.APPLICATION_JSON)
	public MenuDataArr getmainlist() {
		getPath();
		MenuDataArr res = new MenuDataArr();
		res = MenuDataMgr.getmainmenulist();
		return res;
	}

	@GET
	@Path("getmenulist")
	@Produces(MediaType.APPLICATION_JSON)
	public MenuViewDataArr getMenulist(@QueryParam("searchVal") String searchText, @QueryParam("pagesize") int pageSize,
			@QueryParam("currentpage") int currentPage, @QueryParam("sessionID") String sessionId,
			@QueryParam("userID") String userID) {
		getPath();
		MenuViewDataArr res = new MenuViewDataArr();
		ResponseData sessionRes = new ResponseData();
		SessionMgr session_mgr = new SessionMgr();
		Service001Mgr mgr = new Service001Mgr();
		boolean master = false;
		sessionRes = session_mgr.updateActivityTime(sessionId, userID, "");
		if (sessionRes.getCode().equalsIgnoreCase("0000")) {
			master = mgr.isMasterUser(userID);
			res = MenuDataMgr.getAllMenuData(searchText, pageSize, currentPage, master);
		} else {
			res.setMsgCode(sessionRes.getCode());
			res.setMsgDesc(sessionRes.getDesc());
		}

		return res;
	}

	@POST
	@Path("getOneVersionHistoryByAutoKey")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public VersionHistoryData getOneVersionHistoryByAutoKey(VersionHistoryData request) {
		VersionHistoryData response = null;

		getPath();
		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime(request.getSessionID(), request.getUserID(),
				"version-history");

		if (sessionState.getCode().equals("0000")) {
			if (request.getAutokey() != null && request.getAutokey().trim().length() != 0) {
				breakForParseException: {
					try {
						Long.parseLong(request.getAutokey().trim());
					} catch (Exception e) {
						response = new VersionHistoryData();
						response.setMsgCode("0014");
						response.setMsgDesc("Invalid Autokey.");
						response.setState(false);

						break breakForParseException;
					}

					response = new VersionHistoryMgr().getOneByAutoKey(Long.parseLong(request.getAutokey().trim()));
				}
			} else {
				response = new VersionHistoryData();
				response.setMsgCode("0014");
				response.setMsgDesc("Blank Autokey.");
				response.setState(false);
			}
		} else {
			response = new VersionHistoryData();
			response.setMsgCode(sessionState.getCode());
			response.setMsgDesc(sessionState.getDesc());
			response.setState(false);
		}

		return response;
	}

	private void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
	}

	public Menu[] getProfileMainMenu(String[] promenu, Profile p) {
		Menu[] menus = new Menu[promenu.length];
		Menu obj = new Menu();
		Service001Mgr mgr = new Service001Mgr();

		if (promenu.length == 1)
			menus = new Menu[promenu.length + 1];

		for (int i = 0; i < promenu.length; i++) {
			obj = new Menu();
			obj.setMenuItem("");
			obj.setCaption(promenu[i]);
			getPath();

			/*
			 * String[] arr = Service001Mgr.getSubMenuItem(p, promenu[i]);
			 * Menu[] menuitems = new Menu[arr.length]; menuitems =
			 * getProfileSubMenuItem(arr); obj.setMenuItems(menuitems); menus[i]
			 * = obj;
			 */

			Menu[] menuitems = null;
			if (mgr.isMasterUser(p.getUserID())) {
				menuitems = mgr.getProfileSubMenuItem(p, promenu[i], true);
			} else {
				menuitems = mgr.getProfileSubMenuItem(p, promenu[i], false);
			}

			obj.setMenuItems(menuitems);
			menus[i] = obj;
		}

		return menus;

	}

	public Menu[] getProfileRightMenu() {

		Menu[] right = new Menu[2];
		Menu obj = new Menu();
		obj.setMenuItem("Login");
		obj.setCaption("Sign Out");
		right[0] = obj;
		return right;
	}

	// ATM locator //
	public Menu[] getProfileSubMenuItem(String[] promenu) {
		Menu[] items = new Menu[promenu.length];
		Menu subobj = new Menu();

		if (promenu.length == 1)
			items = new Menu[promenu.length + 1];

		for (int j = 0; j < promenu.length; j++) {
			subobj = new Menu();
			subobj.setMenuItem(promenu[j]);
			subobj.setCaption(promenu[j]);
			items[j] = subobj;
		}
		return items;
	}

	@GET
	@Path("getUserRolelist")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserData getRolelist() {
		UserRoleViewData[] dataarray;
		UserData res = new UserData();
		Service001Mgr mgr = new Service001Mgr();
		getPath();
		ResponseData sessionState = new ResponseData();
		String sessionID = request.getParameter("sessionID");
		String userID = request.getParameter("userID");
		sessionState = new SessionMgr().updateActivityTime(sessionID, userID, "");
		if (sessionState.getCode().equals("0000")) {
			dataarray = UserRoleViewDataMgr.getUserRoleList(mgr.isMasterUser(userID));
			res.setUserrolelist(dataarray);
		} else
			res = null;
		return res;
	}

	@GET
	@Path("getRoleList")
	@Produces(MediaType.APPLICATION_JSON)
	public RoleDatas getRoleList(@QueryParam("searchVal") String searchText, @QueryParam("pagesize") int pageSize,
			@QueryParam("currentpage") int currentPage, @QueryParam("sessionID") String sessionId,
			@QueryParam("userID") String userID) {
		getPath();
		RoleDatas res = new RoleDatas();
		RoleDataMgr r_mgr = new RoleDataMgr();
		ResponseData sessionRes = new ResponseData();
		SessionMgr session_mgr = new SessionMgr();
		Service001Mgr mgr = new Service001Mgr();
		boolean master = false;
		sessionRes = session_mgr.updateActivityTime(sessionId, userID, "");
		if (sessionRes.getCode().equalsIgnoreCase("0000")) {
			master = mgr.isMasterUser(userID);
			res = r_mgr.getAllRoleListData(searchText, pageSize, currentPage, master);
		} else {
			res.setMsgCode(sessionRes.getCode());
			res.setMsgDesc(sessionRes.getDesc());
		}
		return res;
	}

	@GET
	@Path("getRoleMenuList")
	@Produces(MediaType.APPLICATION_JSON)
	public RoleData getRoleMenuList(@QueryParam("sessionID") String sessionId, @QueryParam("userID") String userID) {
		getPath();
		RoleMenuData[] dataarray;
		RoleData res = new RoleData();
		ResponseData sessionRes = new ResponseData();
		SessionMgr session_mgr = new SessionMgr();
		Service001Mgr mgr = new Service001Mgr();
		boolean master = false;
		sessionRes = session_mgr.updateActivityTime(sessionId, userID, "Role");
		if (sessionRes.getCode().equalsIgnoreCase("0000")) {
			master = mgr.isMasterUser(userID);
			dataarray = RoleMenuDataMgr.getRoleMenuList(master);
			res.setMenu(dataarray);
		} else {
			res.setMsgCode(sessionRes.getCode());
			res.setMsgDesc(sessionRes.getDesc());
		}
		return res;
	}

	@GET
	@Path("getStateListByUserID")
	@Produces(MediaType.APPLICATION_JSON)
	public Lov3 getStateListByUserID(@QueryParam("userID") String userID, @QueryParam("type") String type) {
		UserDataMgr u_mgr = new UserDataMgr();
		ArrayList<CityStateData> res = new ArrayList<CityStateData>();
		Lov3 lov = new Lov3();
		getPath();
		res = u_mgr.getStateListByUserID(userID, type);

		Ref statearr[] = new Ref[res.size()];
		for (int i = 0; i < res.size(); i++) {
			Ref ref = new Ref();
			ref.setvalue(res.get(i).getCode());
			ref.setcaption(res.get(i).getDespEng());

			statearr[i] = ref;

		}
		lov.setRefstate(statearr);
		return lov;
	}

	@GET
	@Path("getUserListing")
	@Produces(MediaType.APPLICATION_JSON)
	public UserViewDataArr getUserlisting(@QueryParam("searchVal") String searchText,
			@QueryParam("pagesize") int pageSize, @QueryParam("currentpage") int currentPage,
			@QueryParam("operation") String operation, @QueryParam("sessionID") String sessionId,
			@QueryParam("userID") String userID) {
		getPath();
		ResponseData sessionRes = new ResponseData();
		sessionRes = new SessionMgr().updateActivityTime(sessionId, userID, "");
		Service001Mgr mgr = new Service001Mgr();

		UserViewDataArr res = new UserViewDataArr();

		if (sessionRes.getCode().equalsIgnoreCase("0000")) {
			res = new UserDataMgr().getAllUserData(searchText, pageSize, currentPage, operation,
					mgr.isMasterUser(userID));
		} else {
			res.setMsgCode(sessionRes.getCode());
			res.setMsgDesc(sessionRes.getDesc());
		}
		return res;
	}

	@GET
	@Path("getUserNameAndStatus")
	@Produces(MediaType.APPLICATION_JSON)
	public UserData getUserNameAndStatus(@QueryParam("id") String userId, @QueryParam("sessionID") String sessionId,
			@QueryParam("userIDForSession") String userIDForSession) {

		getPath();
		UserData data = new UserData();
		UserDataMgr u_mgr = new UserDataMgr();
		SessionMgr session_mgr = new SessionMgr();
		ResponseData sessionRes = new ResponseData();
		sessionRes = session_mgr.updateActivityTime(sessionId, userIDForSession, "forced-sign-out");
		if (sessionRes.getCode().equalsIgnoreCase("0000")) {
			data = u_mgr.getUserNameAndStatus(userId);
		} else {
			data.setMsgCode(sessionRes.getCode());
			data.setMsgDesc(sessionRes.getDesc());
		}
		return data;
	}

	@POST
	@Path("lovSetuplist")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LOVSetupAllDataArr lovSetuplist(LOVSetupAllDataArr data) {
		DAOManager.AbsolutePath = context.getRealPath("");
		LOVSetupAllDataArr res = new LOVSetupAllDataArr();
		DBLOVSetupMgr dmgr = new DBLOVSetupMgr();
		res = dmgr.lovSetuplist(data);
		return res;
	}

	@POST
	@Path("getBranchUserData")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserData readBranchUserDataBySyskey(String skey, @QueryParam("sessionID") String sessionID,
			@QueryParam("userID") String userID) {

		UserData res = new UserData();
		skey = skey.substring(1, skey.length() - 1);
		long syskey = Long.parseLong(skey);
		getPath();
		SessionMgr s_mgr = new SessionMgr();
		ResponseData response = new ResponseData();
		Service001Mgr mgr = new Service001Mgr();
		response = s_mgr.updateActivityTime(sessionID, userID, "user-setup");
		if (response.getCode().equals("0000")) {
			res = UserDataMgr.readBranchUserDataBySyskey(syskey, mgr.isMasterUser(userID));
		} else {
			res.setMsgCode(response.getCode());
			res.setMsgDesc(response.getDesc());
		}
		return res;
	}

	@POST
	@Path("getMenuData")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public MenuData readMenuDataBySyskey(String skey, @QueryParam("sessionID") String sessionId,
			@QueryParam("userID") String userID) {
		getPath();
		ResponseData sessionRes = new ResponseData();
		sessionRes = new SessionMgr().updateActivityTime(sessionId, userID, "Menu");
		MenuData res = new MenuData();
		if (sessionRes.getCode().equalsIgnoreCase("0000")) {
			skey = skey.substring(1, skey.length() - 1);
			long syskey = Long.parseLong(skey);
			res = MenuDataMgr.readDataBySyskey(syskey);
		} else {
			res.setMsgCode(sessionRes.getCode());
			res.setMsgDesc(sessionRes.getDesc());
		}
		return res;
	}

	@GET
	@Path("readPswPolicy")
	@Produces(MediaType.APPLICATION_JSON)
	public PswPolicyData readPswPolicy(@QueryParam("sessionID") String sessionID, @QueryParam("userID") String userID,
			@QueryParam("formID") String formID) {
		getPath();

		PswPolicyData pwdData = new PswPolicyData();
		PwdMgr pmgr = new PwdMgr();

		SessionMgr s_mgr = new SessionMgr();
		ResponseData response = new ResponseData();
		if (formID.equalsIgnoreCase("pwdpolicy"))
			formID = "password-policy";
		else if (formID.equalsIgnoreCase("changepwd"))
			formID = "change-password";
		response = s_mgr.updateActivityTime(sessionID, userID, formID);
		if (response.getCode().equals("0000")) {
			pwdData = pmgr.readPswPolicy();
		} else {
			pwdData.setMsgCode("0016");
			pwdData.setMsgDesc(response.getDesc());
		}
		return pwdData;
	}

	@POST
	@Path("getRoleData")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public RoleData readRoleDataBySyskey(String skey, @QueryParam("sessionID") String sessionId,
			@QueryParam("userID") String userID) {
		getPath();
		RoleData res = new RoleData();
		SessionMgr session_mgr = new SessionMgr();
		ResponseData sessionRes = new ResponseData();
		Service001Mgr mgr = new Service001Mgr();
		boolean master = false;
		skey = skey.substring(1, skey.length() - 1);
		long syskey = Long.parseLong(skey);
		sessionRes = session_mgr.updateActivityTime(sessionId, userID, "Role");
		if (sessionRes.getCode().equalsIgnoreCase("0000")) {
			master = mgr.isMasterUser(userID);
			res = RoleDataMgr.readDataBySyskey(syskey, master);
		} else {
			res.setMsgCode(sessionRes.getCode());
			res.setMsgDesc(sessionRes.getDesc());
		}
		return res;
	}

	// save Branch User
	@POST
	@Path("saveBranchUser")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON)
	public ResultTwo saveBranchUser(UserData data) {

		getPath();
		ResultTwo res = new ResultTwo();
		SessionMgr session_mgr = new SessionMgr();
		ResponseData sessionRes = new ResponseData();

		sessionRes = session_mgr.updateActivityTime(data.getSessionId(), data.getUserId(), "user-setup");
		if (sessionRes.getCode().equalsIgnoreCase("0000")) {
			if (data.getRolesyskey() != null && data.getRolesyskey().length != 0) {
				res = new PwdMgr().checkPasswordPolicyPattern(data.getT2());
				if (res.getMsgCode().equals("0000")) {
					res = UserDataMgr.saveBranchUserData(data);
				} else {
					res.setState(false);
					res.setMsgCode("0014");
					res.setMsgDesc(res.getMsgDesc());
				}
			} else {
				res.setState(false);
				res.setMsgCode("0001");
				res.setMsgDesc("Please select Role");
			}

		} else {
			res.setMsgCode(sessionRes.getCode());
			res.setMsgDesc(sessionRes.getDesc());
		}
		return res;
	}

	// for LOV Setup
	@POST
	@Path("savelovSetup")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LOVSetupAllData savelovSetUp(LOVSetupAllData aData) {
		System.out.print("Path: " + context.getRealPath("/"));
		DAOManager.AbsolutePath = context.getRealPath("");
		DBLOVSetupMgr lovmgr = new DBLOVSetupMgr();
		aData = lovmgr.saveLOVSetup(aData);
		return aData;
	}

	@POST
	@Path("saveMenu")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON)
	public ResultTwo saveMenu(MenuData data) {
		getPath();
		ResponseData sessionRes = new ResponseData();
		sessionRes = new SessionMgr().updateActivityTime(data.getSessionId(), data.getUserId(), "Menu");

		ResultTwo res = new ResultTwo();

		if (sessionRes.getCode().equalsIgnoreCase("0000")) {
			res = MenuDataMgr.saveMenuData(data);
		} else {
			res.setMsgCode(sessionRes.getCode());
			res.setMsgDesc(sessionRes.getDesc());
		}

		return res;
	}

	@POST
	@Path("savePswPolicy")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ResultTwo savePswPolicy(PswPolicyData psw) {
		getPath();
		ResultTwo ret = new ResultTwo();
		PwdMgr pmgr = new PwdMgr();

		SessionMgr s_mgr = new SessionMgr();
		ResponseData response = new ResponseData();
		response = s_mgr.updateActivityTime(psw.getSessionID(), psw.getUserID(), "password-policy");
		if (response.getCode().equals("0000")) {
			ret = pmgr.savePasswordPolicy(psw);
		} else {
			ret.setMsgCode("0016");
			ret.setMsgDesc(response.getDesc());
			ret.setState(false);
		}
		return ret;
	}

	@POST
	@Path("saveRole")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON)
	public ResultTwo saveRole(RoleData data) {

		getPath();
		ResultTwo res = new ResultTwo();
		SessionMgr session_mgr = new SessionMgr();
		ResponseData sessionRes = new ResponseData();

		sessionRes = session_mgr.updateActivityTime(data.getSessionId(), data.getUserId(), "Role");
		if (sessionRes.getCode().equalsIgnoreCase("0000")) {
			if (data.getParentsyskey() != null && data.getParentsyskey().length > 0) {
				res = RoleDataMgr.saveRoleData(data, RoleDataMgr.isMasterRole(data.getT1()));
			} else {
				res.setMsgDesc("Please select menu");
			}
		} else {
			res.setMsgCode(sessionRes.getCode());
			res.setMsgDesc(sessionRes.getDesc());
		}
		return res;
	}
	
	@POST
	@Path("signin")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Profile signin(Profile p) {
		getPath();
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		String password = "";
		if (p != null && p.getUserID() != null && p.getPassword() != null) {
			if (!p.getIv().equals("")) {
				AesUtil util = new AesUtil(128, 1000);
				password = util.decrypt(p.getSalt(), p.getIv(), p.getPassword());
			}
			ResultTwo result1 = new ResultTwo();
			String sessionId = "";
			MSigninResponseData result = new MSigninResponseData();
			SessionMgr session_mgr = new SessionMgr();

			p.setUserID(p.getUserID());
			p.setLoginStatus(4);
			getPath();
			String u1 = Service001Mgr.login(p, password);
			p.setT1(Service001Mgr.getSyskey(p));
			if (u1.equals("")) {
				p.setCode("0014");
				p.setDesc("Invalid User ID or Password");
				p.setRole(0);
			} else {
				result = session_mgr.checkLogin(p.getUserID());
				if (result.getCode().equalsIgnoreCase("0000")) {
					result1 = session_mgr.insertSession(p.getUserID());
					if (!result1.isState()) {
						p.setCode("0014");
						p.setDesc("User already logged in");
						p.setRole(0);
						return p;
					}
					sessionId = result1.getSessionID();
					p.setSessionID(sessionId);

					String[] a2 = Service001Mgr.getMainMenu(p);
					if (a2.length > 0)
						p.setMenus(getProfileMainMenu(a2, p));
					p.setRightMenus(getProfileRightMenu());
					p.setUserName(u1);
					p.setCode("0000");
					p.setDesc("Login Success");
				} else {
					p.setCode("0014");
					p.setDesc("User already logged in");
					p.setRole(0);
				}
			}
			p.setPassword("");
			p.setDm("");
			p.setIv("");
			p.setSalt("");
		} else {
			p.setRole(0);
			p.setCode("0014");
			p.setDesc("Invalid User ID or Password");
		}
		return p;
	}

	@POST
	@Path("signout")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ResultTwo signout(String userid, @QueryParam("sessionID") String sessionId) {
		getPath();
		ResultTwo res = new ResultTwo();
		SessionMgr session_mgr = new SessionMgr();
		res = session_mgr.logout(sessionId);

		return res;
	}

	@GET
	@Path("method001")
	@Produces(MediaType.APPLICATION_JSON)
	public Bean001 test1() {
		Bean001 res = new Bean001();
		return res;
	}
}	
