package com.nirvasoft.rp.users.data;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RoleDataset {

	private String searchText;
	private int totalCount;
	private int currentPage;
	private int pageSize;

	private ArrayList<RoleData> arlData;

	private RoleData[] roledata;

	public ArrayList<RoleData> getArlData() {
		return arlData;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public RoleData[] getRoledata() {
		return roledata;
	}

	public String getSearchText() {
		return searchText;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setArlData(ArrayList<RoleData> arlData) {
		this.arlData = arlData;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setRoledata(RoleData[] roledata) {
		this.roledata = roledata;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

}
