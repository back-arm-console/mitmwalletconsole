package com.nirvasoft.rp.users.data;

public class UserViewData extends UserData {

	private String username;

	public UserViewData() {
		clearProperties();
	}

	@Override
	public void clearProperties() {
		super.clearProperties();
		this.username = "";
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public void setUsername(String username) {
		this.username = username;
	}
}
