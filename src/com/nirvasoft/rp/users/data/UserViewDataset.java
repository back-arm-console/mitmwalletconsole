package com.nirvasoft.rp.users.data;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserViewDataset {

	private int totalCount;
	private int currentPage;
	private int pageSize;

	private ArrayList<UserViewData> arlData;

	private void clearProperty() {
		totalCount = 0;
		currentPage = 0;
		pageSize = 0;
	}

	public ArrayList<UserViewData> getArlData() {
		return arlData;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setArlData(ArrayList<UserViewData> ret) {
		this.arlData = ret;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

}
