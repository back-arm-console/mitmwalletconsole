package com.nirvasoft.rp.util.dfs;
/* 
TUN THURA THET 2011 04 21
*/
import java.util.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.text.*;
import java.io.*;

import com.nirvasoft.rp.dao.DAOManager;


/**
 *   
 * TUN THURA THET @ NTU 2007-2009
 */
public class FileUtil{
	static public String LogFile;
	public static String Save_Success = "";
	public static String Save_Fail = "";
	public static String Update_Success = "";
	public static String Update_Fail = "";
	public static String Delete_Success = "";
	public static String Delete_Fail = "";

    public static ArrayList<String> readTable(String pTable){
        ArrayList<String> arlRecords =null;
        try{
        	arlRecords = new ArrayList<String>();
            File f = new File(pTable); 
            File [] fileList = f.listFiles();
            for (int i=0;i<fileList.length;i++){
                if (fileList[i].isFile() ){
                	String fname= fileList[i].getAbsolutePath();
                	//System.out.println(fname);
                	if (fname.endsWith("txt")) arlRecords.add(readALine(fname) + "__|" + fname);
                }
            }
        } catch (Exception e){
            System.out.println(e.toString());
        }
        return arlRecords;
    }
    
    public static String formatDBDate2MIT(String p) {
    	return formatYYYYMMDD2MIT(p);     // <<<<<<<<<<<<<<<<<<<<<<<<<<<<  COFIG
    }
    public static String formatYYYYMMDD2MIT(String p) {
    	String ret="";
    	try{
    		if (p.length()>=10) {
    			ret = p.replaceAll("-", "").substring(0, 8);
			}
    		else
    		{
    			ret = "19000101";
    		}

    	}catch (Exception exp){}
    	return ret;
}
    
    
    public static void writeTable(String pTable, String id, String data){
        try{
        	writeText(pTable +"/"+ id + ".txt", data, false);
        } catch (Exception e){
            System.out.println(e.toString());
        }
    }

    public static boolean deleteFile( String f) {
     boolean isdeleted=false;
       File file = new File(f);
       try{
       isdeleted =   file.delete();  

       } catch (Exception e){
           System.out.println("File Delete: " + e.toString());
       }
     	return isdeleted;
     }
    public static ArrayList<String> getFileList(String strPath){
        ArrayList<String> arlFiles =null;
        try{
            arlFiles = new ArrayList<String>();
            File f = new File(strPath); 
            File [] fileList = f.listFiles();
            for (int i=0;i<fileList.length;i++){
                if (fileList[i].isFile() ){
                    arlFiles.add(fileList[i].getAbsolutePath());
                } else {
                    ArrayList<String> arlSubFiles = getFileList(fileList[i].getAbsolutePath());
                    arlFiles.addAll(arlSubFiles);
                }
            }
        } catch (Exception e){
            System.out.println(e.toString());
        }
        return arlFiles;
    }
    public static String [] getFileArray(String strPath){
        ArrayList<String> arl = getFileList(strPath);
        String []  a = new String[arl.size()];
        a =    arl.toArray(a);
        return a;
    }
    public static String readText(String strPath){
        StringBuffer sbTemp=new StringBuffer();
        try {
            BufferedReader in = new BufferedReader(new FileReader(strPath));
            String str;
            while ((str = in.readLine()) != null) {
                sbTemp.append(str + "\r");
            }
            in.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return sbTemp.toString();
    }   
    public static String readALine(String strPath){
        StringBuffer sbTemp=new StringBuffer();
        try {
            BufferedReader in = new BufferedReader(new FileReader(strPath));
            String str;
            while ((str = in.readLine()) != null) {
                sbTemp.append(str);
            }
            in.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return sbTemp.toString();
    }
    
    public static String  getMessageDescription(String messageCode)
	{
		
		String retMsgDesc = "";
		try {
			ArrayList<String> arl = FileUtil.readList(DAOManager.AbsolutePath +"/reference/"+ "MessageCode.txt");
			for (int i=0;i<arl.size();i++) {
				if (!arl.get(i).equals("")){
					
						if (!arl.get(i).equals("")){
							
							if(arl.get(i).split(":")[0].equalsIgnoreCase(messageCode)){
								System.out.println("msgdesc"+arl.get(i).split(":")[1]);
								retMsgDesc = arl.get(i).split(":")[1];
							}
						}	
					
				}
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return retMsgDesc;
	}
    
    public static void logTime(String strMsg){  
    	logTime(strMsg,LogFile);
    }
    public static void logTime(String strMsg,String File){  
        logln(getTime() + " : "+strMsg+ "\r\n", File);
    }
    public static String getTime(){  
        Date d = new Date(System.currentTimeMillis()); //liftOffApollo11.getTime();
        DateFormat df1 = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
        String s1 = df1.format(d);
        return s1;
      }
    
    public static String datetoString()
	{
		String l_date = "";
		java.util.Date l_Date = new java.util.Date();
		SimpleDateFormat l_sdf = new SimpleDateFormat("yyyy-MM-dd");	
		l_date = l_sdf.format(l_Date);
		
		return l_date;
	}
    
    
    public static void log(String strContent){
    	log(strContent, LogFile);
    }
    public static void log(String strContent, String File){
    	//System.out.print(strContent);
    	writeText(File,strContent,true);
    }
    public static void logln(String strContent){
    	logln(strContent,LogFile);
    }
    public static void logln(String strContent, String File){
    	log(strContent+"\r\n",File);
    }
    public static void logEmpty(){
    	writeText(LogFile,"",false);
    }
    public static void writeText(String strPath,String strContent,boolean bAppend){
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(strPath, bAppend));
            out.write(strContent);
            out.close();
        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }
    public static void writeALine(String strPath,String strContent,boolean bAppend){
    	writeText(strPath,strContent + (char) 13 + (char) 10,bAppend);
    }
    public static PrintWriter openWriter(String strPath,boolean bAppend){
    PrintWriter out = null;
        try {
            out = new PrintWriter( new FileWriter(strPath,bAppend));
        }catch (IOException e) {}
        return out;
    }
    public static void writeln(String strContent,PrintWriter out){
        try {
            out.println(strContent);
        } catch (Exception e) {}
    }
    public static void writeList(String strPath,ArrayList<String> arlContent,boolean bAppend){
        StringBuffer sbTemp=new StringBuffer();
        for (int i=0; i < arlContent.size();i++){
            sbTemp.append(arlContent.get(i) + (char) 13 + (char) 10);
        }
        writeText(strPath,sbTemp.toString(),bAppend);
    }
    public static ArrayList<String> readList(String strPath){
        return readList(strPath,false);
    }
    public static ArrayList<String> readList(String strPath,boolean stem){
        ArrayList<String> arlTemp=new ArrayList<String> ();
        try {
            BufferedReader in = new BufferedReader(new FileReader(strPath));
            String str;
            while ((str = in.readLine()) != null) {
                String strOK= str;
                if (stem){
                    strOK =str;
                }
                if (!str.equals(""))
                    arlTemp.add(strOK);
            }
            in.close();
        } catch (Exception e) { System.out.println("Error @ readList" +  e.getMessage());}
        return arlTemp;
    }
    public static BufferedReader  openReader(String strPath){
        BufferedReader in= null;
        try {
            in = new BufferedReader(new FileReader(strPath));
        } catch (Exception e) {}
        return in;
    }
    public static String readln(BufferedReader in){
        String str="";
        try {
            str=in.readLine();
        } catch (Exception e) {}
        return str;
    }
    public static boolean deleteFiles( String d) {
     boolean isdeleted=false;
     File dir = new File(d);
     String[] list = dir.list();
     File file;
     if (list.length == 0) 
      	return isdeleted;
     for (int i = 0; i < list.length; i++) {
       file = new File(d + list[i]);
       isdeleted =   file.delete();      
       }
     	return isdeleted;
     }
   public static boolean deleteFiles( String d, String start) {
        boolean isdeleted=false;
        File dir = new File(d);
        String[] list = dir.list();
        File file;
        if (list.length == 0) 
         	return isdeleted;
        for (int i = 0; i < list.length; i++) {
        	if (list[i].startsWith(start))	{
	        	file = new File(d + list[i]);
	        	isdeleted =   file.delete(); 
        		System.out.println("deleted: " +d + list[i]);	    
        	}
          }
        	return isdeleted;
   }
   public static void makeDir(String d){
        try {new File(d).mkdirs();} catch (Exception e){}
   }
    public static String runFile(String exe) {
        //setPath();
        String sbs=new String();
        StringBuffer sb= new StringBuffer();
        try{
            StringBuffer buff = new StringBuffer();
            String modelCommand = exe;
            Runtime r1 = Runtime.getRuntime();
            Process p = r1.exec(modelCommand);
            BufferedInputStream bufIn = new BufferedInputStream(p.getInputStream());

            int ch;
            while ((ch = bufIn.read()) > -1) {
                buff.append((char)ch);
            }
            bufIn.close();
            sbs = buff.toString();
            System.out.println(sbs);
            sb.append(sbs);
            System.out.println(sbs);
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return sb.toString();
    }
	
    public static String datetoDBString()
  	{
  		String l_date = "";
  		java.util.Date l_Date = new java.util.Date();
  		SimpleDateFormat l_sdf = new SimpleDateFormat("yyyyMMdd");	
  		l_date = l_sdf.format(l_Date);
  		
  		return l_date;
  	}
    
    public static String getTodayDate(){
  		String l_date = "";
  		java.util.Date l_Date = new java.util.Date();
  		SimpleDateFormat l_sdf = new SimpleDateFormat("yyyy-MM-dd");	
  		l_date = l_sdf.format(l_Date);
  		System.out.println("Today date --> " + l_date);
  		return l_date;
  	}
    
    public static String changeDateFormatforClient(String date){
    	String l_date = "";
    	if(date != null){
    		try{
    			String[] d = date.split("-");
    			l_date = d[2]+"/"+d[1]+"/"+d[0];
    		}catch(Exception e){
    			l_date = "-";
    		}
    	}else{
    		l_date = "-";
    	}
    	return l_date;
    }
    
    public static String changeDateTimeFormatforClient(String date){
    	String l_date = "";
    	if(date != null){
    		try{
    			String [] dt = date.split(" ");
    			String[] d = dt[0].split("-");
    			l_date = d[2]+"-"+d[1]+"-"+d[0];
    		}catch(Exception e){
    			l_date = "-";
    		}
    	}else{
    		l_date = "-";
    	}
    	return l_date;
    }
    
    public static String changeDateTimeFormatforWebClient(String date){
    	String l_date = "";
    	if(date != null){
    		try{
    			String [] dt = date.split(" ");
    			String[] d = dt[0].split("-");
    			l_date = d[2]+"/"+d[1]+"/"+d[0];
    		}catch(Exception e){
    			l_date = "-";
    		}
    	}else{
    		l_date = "-";
    	}
    	return l_date;
    }
    
    public static  String get24HoursFormat(){
    	String l_date = "";
		java.util.Date l_Date = new java.util.Date();
		
		//SimpleDateFormat l_sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss aa");//output:2017-02-11 13:22:44 PM
		SimpleDateFormat l_sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		l_date = l_sdf.format(l_Date);
		
		return l_date;
		//output:2017-02-11 13:22:44
    }
    
    public static  String get24HoursFormat1(){
    	String l_date = "";
		java.util.Date l_Date = new java.util.Date();
		
		//SimpleDateFormat l_sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss aa");//output:2017-02-11 13:22:44 PM
		SimpleDateFormat l_sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		l_date = l_sdf.format(l_Date);
		
		return l_date;
		//output:2017-02-11 13:22:44
    }
    
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
    
    public static String changeDateFormatfoSave(String date){
    	String l_date = "";
    	if(date != null){
    		try{
    			String[] d = date.split("/");
    			l_date = d[2]+"-"+d[1]+"-"+d[0];
    		}catch(Exception e){
    			l_date = "-";
    		}
    	}else{
    		l_date = "-";
    	}
    	return l_date;
    }
    
    public static boolean writeLog(ArrayList<String> pErrorList,String folderName) {
		boolean l_result = false;
		try {
			
			String l_Path = DAOManager.AbsolutePath+ "\\MobileServiceReq\\log";
			File dir = new File(l_Path);
			dir.mkdirs();
			FileUtil.writeList(l_Path +"\\"+ folderName +".txt", pErrorList, true);
			l_result = true;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			l_result = false;
		}
		return l_result;
		
	}
    
    public static boolean writeLogMobileService(ArrayList<String> pErrorList,String folderName) {
		boolean l_result = false;
		try {
			String l_Date = getTodayDate();
			String l_Path = DAOManager.AbsolutePath+ "\\MobileServiceReq\\"+folderName;
			File dir = new File(l_Path);
			dir.mkdirs();
			FileUtil.writeList(l_Path +"\\"+ l_Date +".txt", pErrorList, true);
			l_result = true;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			l_result = false;
		}
		return l_result;
		
	}
    public static String formatDBDateTime2MIT(String p) {
    	return formatYYYYMMDDHHMMSS2MIT(p);  
    }

	public static String formatYYYYMMDDHHMMSS2MIT(String p) {
    	String ret="";
    	try{
    		if (p.length()>=10) {
    			ret = p.replaceAll("-", "");
			}
    		else
    		{
    			ret = "19000101 00:00:00";
    		}

    	}catch (Exception exp){}
    	return ret;
    }
	
	public static ArrayList<String> readJSON(){

        JSONParser parser = new JSONParser();
        ArrayList<String> ret = new ArrayList<String>();
        try {
 
            Object obj = parser.parse(new FileReader(DAOManager.AbsolutePath+"/json/"+ "transactionlist.json")); 
            //JSONObject jsonObject = (JSONObject) obj;
            JSONArray companyList = (JSONArray) obj;
 
            Iterator<String> iterator = companyList.iterator();
            while (iterator.hasNext()) {
                ret.add(iterator.next());
            }
 
        } catch (Exception e) {
            e.printStackTrace();
        }
		return ret;
	}
    
	//nlkm 
	public static String datetoDBString1()
  	{
  		String l_date = "";
  		java.util.Date l_Date = new java.util.Date();
  		SimpleDateFormat l_sdf = new SimpleDateFormat("yyyy/MM/dd");	
  		l_date = l_sdf.format(l_Date);
  		
  		return l_date;
  	}
	
	public static String formatDBDate2MIT1(String p) {
    	return formatYYYYMMDD2MIT1(p);     
    }
    public static String formatYYYYMMDD2MIT1(String p) {
    	// 2018-12-26 to 2018/12/26
    	String ret="";
    	try{
    		if (p.length()>=10) {
    			ret = p.replaceAll("-", "/");
			}
    		else
    		{
    			ret = "1900/01/01";
    		}

    	}catch (Exception exp){}
    	return ret;
    }
	
}