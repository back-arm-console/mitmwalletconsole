package com.nirvasoft.rp.util;

import java.io.File;
import java.util.Base64;

public class FileTest {
	private static String decrypt(String str) {
		byte[] valueDecoded = Base64.getDecoder().decode(str);
		return new String(valueDecoded);
	}

	private static String encrypt(String str) {
		byte[] bytesEncoded = Base64.getEncoder().encode(str.getBytes());
		return new String(bytesEncoded);
	}

	private static void getFileList(String folderName) {
		File file = new File(folderName);
		File[] fileList = file.listFiles();

		// js file
		for (File f : fileList) {
			if (f.getName().endsWith(".js")) {
				String url = f.getAbsolutePath();
				System.out.println("'" + url.substring(url.indexOf("app")).replace("\\", "/") + "',");
				// System.out.println("'"+url.substring(url.indexOf("css")).replace("\\",
				// "/")+"',");
			}
		}

		// directory
		for (File f : fileList) {
			if (f.isDirectory())
				getFileList(f.getAbsolutePath());
		}

	}

	public static void main(String[] args) {
		getFileList("D:/A_WORKSPACE/SERVICE/WALLET/WalletConsole/WalletConsole/WebContent/dist/app");
		// getFileList("E:/MITProject/pac/WebContent/css");
		// getFileList("D:/MIT/hms-angular2-new/WebPOS/WebContent/app");
		// getFileList("D:/MIT/hms-angular2-new/LoyaltySystem/WebContent/app");
		// System.out.println(UUID.randomUUID().toString());
		// System.out.println(UUID.randomUUID().toString());
		// System.out.println(UUID.randomUUID().toString());

		// System.out.println(encrypt("TUlUOnF5UnVweDdUQ0JmdkVaYnprdkZIWg=="));
		// System.out.println(encrypt("MIT:qyRupx7TCBfvEZbzkvFHZi"));
		// System.out.println(encrypt("oITnilR0A2smmDUDvU83492P19uRIYE7PYj/x21etjM="));
	}
}
