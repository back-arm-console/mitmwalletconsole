package com.nirvasoft.rp.mgr;

import java.sql.Connection;

import com.nirvasoft.rp.dao.LOVDAO;
import com.nirvasoft.rp.dao.VersionHistoryDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Lov3;
import com.nirvasoft.rp.shared.VersionHistoryData;
import com.nirvasoft.rp.shared.VersionHistoryDataSet;
import com.nirvasoft.rp.util.ServerUtil;

public class VersionHistoryMgr {

	public VersionHistoryData delete(long autoKey) {
		VersionHistoryData response = null;

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			response = new VersionHistoryDao().delete(autoKey, conn);
		} catch (Exception e) {
			response.setMsgCode("0014");
			response.setMsgDesc("Server Error!");
			response.setState(false);
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}

	public VersionHistoryDataSet getAll(String searchText, int pageSize, int currentPage) {
		VersionHistoryDataSet response = null;

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			response = new VersionHistoryDao().getAll(searchText, pageSize, currentPage, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}

	public VersionHistoryData getOneByAutoKey(long autoKey) {
		VersionHistoryData response = null;

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			response = new VersionHistoryDao().getOneByAutoKey(autoKey, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}		

}