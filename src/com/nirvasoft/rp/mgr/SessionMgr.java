package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;

import com.nirvasoft.rp.dao.SessionDAO;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.ResultTwo;
import com.nirvasoft.rp.shared.MSigninResponseData;
import com.nirvasoft.rp.shared.ResponseData;
import com.nirvasoft.rp.util.ServerUtil;

public class SessionMgr {

	public MSigninResponseData checkLogin(String userId) {
		MSigninResponseData res = new MSigninResponseData();
		SessionDAO s_dao = new SessionDAO();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = s_dao.checkLogin(userId, conn);

		} catch (Exception e) {
			res.setCode("0014");
			res.setDesc(e.getMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				res.setCode("0014");
				res.setDesc(e.getMessage());
			}
		}
		return res;
	}

	public ResultTwo insertSession(String userId) {
		ResultTwo res = new ResultTwo();
		Connection conn = null;
		SessionDAO s_DAO = new SessionDAO();

		try {
			conn = ConnAdmin.getConn("001", "");
			res = s_DAO.insertSession(userId, conn);
		} catch (Exception e) {
			res.setMsgCode("0014");
			res.setMsgDesc(e.getMessage());
			res.setState(false);
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	public ResultTwo logout(String sessionId) {
		ResultTwo res = new ResultTwo();
		Connection conn = null;
		SessionDAO s_DAO = new SessionDAO();

		try {
			conn = ConnAdmin.getConn("001", "");
			res = s_DAO.logout(sessionId, conn);
		} catch (Exception e) {
			e.printStackTrace();
			res.setMsgCode("0014");
			res.setMsgDesc(e.getMessage());
			res.setState(false);
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return res;
	}

	public ResponseData updateActivityTime(String sessionId, String userID, String formID) {
		ResponseData res = null;
		Connection conn = null;
		SessionDAO s_DAO = new SessionDAO();

		try {
			conn = ConnAdmin.getConn("001", "");
			res = s_DAO.updateActivityTime(sessionId, userID,formID, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return res;
	}

	public ResponseData updateActivityTime_Old(String sessionId) {
		ResponseData res = new ResponseData();
		Connection conn = null;
		SessionDAO s_DAO = new SessionDAO();

		try {
			conn = ConnAdmin.getConn("001", "");
			res = s_DAO.updateActivityTime_Old(sessionId, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	public ResponseData updateAndcheckActivityTime(String sessionId, String userId) {
		ResponseData res = new ResponseData();
		Connection conn = null;
		SessionDAO s_DAO = new SessionDAO();

		try {
			conn = ConnAdmin.getConn("001", "");
			res = s_DAO.updateAndcheckActivityTime(sessionId, userId, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return res;
	}

	public ResponseData validateID(String sessionId, String userId) {
		ResponseData response = new ResponseData();
		if (sessionId.equalsIgnoreCase("null") || sessionId.equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("Session ID is mandatory");
		} else if (userId.equalsIgnoreCase("null") || userId.equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("User ID is mandatory");
		} else {
			response.setCode("0000");
			response.setDesc("Success");
		}
		return response;
	}

}