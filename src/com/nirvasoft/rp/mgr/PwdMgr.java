package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;

import com.nirvasoft.rp.dao.PasswordPolicyDao;
import com.nirvasoft.rp.dao.PwdDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.ResultTwo;
import com.nirvasoft.rp.shared.PswPolicyData;
import com.nirvasoft.rp.shared.PwdData;
import com.nirvasoft.rp.util.ServerUtil;

public class PwdMgr {

	public ResultTwo changePwd(PwdData data) {
		// TODO Auto-generated method stub
		ResultTwo ret = new ResultTwo();
		PwdDao t_DAO = new PwdDao();
		Connection l_Conn = null;

		try {
			l_Conn = ConnAdmin.getConn("001", "");
			ret = t_DAO.updatePwdData(data, l_Conn);
		} catch (SQLException e) {

			e.printStackTrace();
			ret.setMsgDesc(e.getMessage());
			ret.setState(false);

		} finally {

			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
				// l_DAO.deregisterDriver();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				ret.setMsgDesc(e.getMessage());
				ret.setState(false);
			}
		}
		return ret;
	}

	public ResultTwo checkPasswordPolicyPattern(String newPwd) {
		// TODO Auto-generated method stub
		PasswordPolicyDao t_DAO = new PasswordPolicyDao();
		ResultTwo ret = new ResultTwo();
		Connection l_Conn = null;

		try {
			l_Conn = ConnAdmin.getConn("001", "");
			ret = t_DAO.checkPasswordPolicyPattern(newPwd, l_Conn);
		} catch (SQLException e) {

			e.printStackTrace();
			ret.setMsgCode("0014");
			ret.setMsgDesc(e.getMessage());

		} finally {
			ServerUtil.closeConnection(l_Conn);
		}
		return ret;
	}

	public ResultTwo forcechangePassword(PwdData data) throws ParseException {
		// TODO Auto-generated method stub
		ResultTwo ret = new ResultTwo();
		PwdDao t_DAO = new PwdDao();
		Connection l_Conn = null;

		try {
			l_Conn = ConnAdmin.getConn("001", "");
			ret = t_DAO.forcechangePassword(data, l_Conn);
		} catch (SQLException e) {

			e.printStackTrace();
			ret.setKeyst("1");// Error Message
			ret.setMsgDesc(e.getMessage());

		} finally {

			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
				// l_DAO.deregisterDriver();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

				ret.setKeyst("1");// Error Message
				ret.setMsgDesc(e.getMessage());
			}
		}
		return ret;
	}

	public PswPolicyData readPswPolicy() {
		PswPolicyData pwdData = new PswPolicyData();
		PwdDao p_dao = new PwdDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			pwdData = p_dao.readPswPolicy(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return pwdData;
	}

	public ResultTwo savePasswordPolicy(PswPolicyData aData) {
		ResultTwo res = new ResultTwo();
		Connection conn = null;
		String msg = "";
		PwdDao u_dao = new PwdDao();
		try {
			conn = ConnAdmin.getConn("001", "");
			res = u_dao.savePasswordPolicy(aData, conn);

		} catch (SQLException e) {
			e.printStackTrace();
			res.setMsgCode("0014");
			res.setMsgDesc(e.getMessage());
		}

		return res;
	}

}
