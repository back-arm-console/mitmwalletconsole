package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.dao.LOVDAO;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Lov3;
import com.nirvasoft.rp.framework.Ref;
import com.nirvasoft.rp.shared.LOVSetupAllData;
import com.nirvasoft.rp.shared.LOVSetupAllDataArr;
import com.nirvasoft.rp.util.FileUtil;

public class DBLOVSetupMgr {

	public Lov3 getLoadLOV() {

		Lov3 ret = new Lov3();
		Ref[] arr = null;
		Connection l_Conn = null;
		LOVDAO l_LOVDAO = new LOVDAO();

		try {
			l_Conn = ConnAdmin.getConn("001", "");
			arr = l_LOVDAO.getRefLOVByLOV(l_Conn);
			ret.setRef032(arr);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return ret;
	}

	public LOVSetupAllData getLOVbyCode(String lovNo) {
		// TODO Auto-generated method stub
		LOVSetupAllData ret = new LOVSetupAllData();
		Connection l_Conn = null;
		LOVDAO l_RefLOVDAO = new LOVDAO();
		try {
			l_Conn = ConnAdmin.getConn("001", "");
			ret = l_RefLOVDAO.getLOVbyCode(lovNo, l_Conn);
		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
			}
		}
		return ret;
	}

	public LOVSetupAllData getLOVbySysKey(long pdata) {
		LOVSetupAllData ret = new LOVSetupAllData();
		Connection l_Conn = null;
		LOVDAO l_RefLOVDAO = new LOVDAO();
		try {
			l_Conn = ConnAdmin.getConn("001", "");
			ret = l_RefLOVDAO.getLOVbySysKey(pdata, l_Conn);
		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
			}
		}
		return ret;
	}

	public Lov3 getProcessingCodeLov() {

		Lov3 ret = new Lov3();
		Ref[] arr = null;

		try {
			arr = FileUtil.readListOfValue("ProcessingCodeLov");
			ret.setRef028(arr);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		return ret;
	}

	public LOVSetupAllDataArr lovSetuplist(LOVSetupAllDataArr data) {
		LOVSetupAllDataArr ret = new LOVSetupAllDataArr();
		Connection l_Conn = null;
		LOVDAO l_DAO = new LOVDAO();
		try {
			l_Conn = ConnAdmin.getConn("001", "");
			String searchText = data.getSearchText();
			int pageSize = data.getPageSize();
			int currentPage = data.getCurrentPage();

			System.out.println("SearchText=" + data.getSearchText());
			System.out.println("PageSize=" + data.getPageSize());
			ret = l_DAO.LOVSetuplist(searchText, pageSize, currentPage, l_Conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return ret;
	}

	public LOVSetupAllData saveLOVSetup(LOVSetupAllData aData) {
		DAOManager l_DAO = new DAOManager();
		Connection l_Conn = null;
		LOVDAO l_RefLOVDAO = new LOVDAO();
		String id = "";
		long Hkey = 0;
		try {
			l_Conn = ConnAdmin.getConn("001", "");
			l_Conn.setAutoCommit(false);

			if (aData.getLovNo().equalsIgnoreCase("TBA")) {

				id = l_DAO.getNewLOVNumber(l_Conn);
				aData.setLovNo(id);

				l_Conn = ConnAdmin.getConn("001", "");
				Hkey = l_RefLOVDAO.inserLOVHeader(aData, l_Conn);
				aData.setSysKey(Hkey);
				if (l_RefLOVDAO.insertLOVDetails(aData, Hkey, l_Conn)) {
					l_Conn.setAutoCommit(true);
					aData.setMessageCode("0000");
					aData.setMessageDesc(FileUtil.getMessageDescription(aData.getMessageCode()));
				} else {
					aData.setMessageCode("0050");
					aData.setMessageDesc(FileUtil.getMessageDescription(aData.getMessageCode()));
				}
				// aData.setLovNo(id);

			} else {

				System.out.println("Update");
				Hkey = l_RefLOVDAO.updateLOVHeader(aData, l_Conn);
				if (l_RefLOVDAO.updateLOVDetails(aData, Hkey, l_Conn)) {
					l_Conn.setAutoCommit(true);
					aData.setMessageCode("0001");
					aData.setMessageDesc(FileUtil.getMessageDescription(aData.getMessageCode()));
				} else {
					aData.setMessageCode("0051");
					aData.setMessageDesc(FileUtil.getMessageDescription(aData.getMessageCode()));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			aData.setMessageDesc(e.getMessage());
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				aData.setMessageDesc(e.getMessage());
			}
		}
		return aData;
	}
}
