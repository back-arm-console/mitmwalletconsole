import java.text.DecimalFormat;

public class testing {
	public static void main(String[] args) {
		// substring
		String url = "http/abc/def/ghi";
		url = url.substring(0, url.lastIndexOf('/') + 1);
		// System.out.println(url);

		// substring
		String str = new String("2018-07-23 15:36:48.000");
		System.out.println("Subs tring starting from index 15:");
		System.out.println(str.substring(15));
		System.out.println("Substring starting from index 15 and ending at 20:");
		System.out.println(str.substring(11, 16));
		System.out.println(str.substring(0, 10));

		// substring
		String inputFileName = "pdf-sample.pdf!";
		String fileName = inputFileName.substring(0, inputFileName.lastIndexOf(".") + 1);
		// System.out.println("inputFileName="+fileName);

		// substring
		String dateformat = "August-9-2018 THUR 12:27 pm";
		String date = dateformat.substring(0, dateformat.indexOf(" "));
		String time = dateformat.substring(dateformat.indexOf(" ") + 1, dateformat.length());
		// System.out.println("date="+date);
		// System.out.println("time="+time);

		String l_result = "0.00";
		DecimalFormat l_df = new DecimalFormat("#,###.##");
		l_result = l_df.format(0.00);
		System.out.println("Amount=" + l_result);

	}
}
