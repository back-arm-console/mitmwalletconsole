import { Component, ElementRef, enableProdMode, Renderer } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { ActivatedRoute, Router } from '@angular/router';
import { IMyDpOptions } from 'mydatepicker';
import { Subscription } from 'rxjs/Subscription';
import { RpHttpService } from '../framework/rp-http.service';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpReferences } from '../framework/rp-references';
import { ClientUtil } from '../util/rp-client.util';
import { FrmDashboardService } from './frmdashboard-service';
import { DashboardService } from '../framework/dashboard-service';
declare var jQuery: any;
declare var google: any;

enableProdMode();
@Component({
  selector: 'dashboard',
  template: `
  <div  class="container-fluid">
  <div class="row clearfix">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
      <form class="form-horizontal" ngNoForm>
          <legend> <h4 style="margin-top: 0px;"> Dashboard</h4>  </legend> 
    <div class="cardview list-height" style="background-color:#ffffff">                  
      <div>&nbsp;</div>
        <div class="col-md-12">
          <div class="col-md-4" style="padding-left: 2px; padding-right: 2px;">
            <!-- <div class="dbox"> -->
          <div> 
            <div [hidden] = "!_mflag" style="text-align: center;">All Transactions by Transaction Type</div>  
            <div>&nbsp;</div>                   
          <div class="body" id="newSummary"></div>
        </div>
      </div>
   
      <div class="col-md-4" style="padding-left: 2px; padding-right: 2px;">
     <!-- <div class="col-md-12"> -->
        <div class="col-md-4">
          <select [(ngModel)]="_period"  (change)="changeTo($event.target.options.selectedIndex)"class="form-control input-sm" [ngModelOptions]="{standalone: true}" required>
            <option *ngFor="let c of lovstatusType" value="{{c.value}}">{{c.caption}}</option>
          </select>
        </div>
      <!--</div> -->
      
        <div> 
        
          <div [hidden] = "!_mflag" style="text-align: center;">Wallet to Wallet</div>  
          <div>&nbsp;</div>                   
          <div class="body" id="newSummary1"></div>
        </div>
      </div>
    </div>     
        </div>
           
       </form>
    </div>
    <div>&nbsp;</div>
    <!--<div>&nbsp;</div> -->
  </div>      
</div> 
<div [hidden] = "_mflag">
<div class="modal" id="loader"></div>
</div>		
     `
})
export class frmDashBoard {
  subscription: Subscription;
  sub: any;
  _util: ClientUtil = new ClientUtil();
  today = this._util.getTodayDate();
  myDatePickerOptions: IMyDpOptions = this._util.getDatePicker();
  dateobj: { date: { year: number, month: number, day: number } };
  _dates = { "fromDate": this.dateobj, "toDate": this.dateobj };


  //for dashboard
  _mflag = true;
  tempAllTransactions: any;
  W_t_W = [];
  M_P = [];
  A_t_W = [];
  W_t_A = [];
   dataSeries = [];
    minBalance = 0;
    allTransactionsForFourDays = [];
  //_period : any;
  lovstatusType = [{ "value": 1, "caption": "Daily" },
  { "value": 2, "caption": "Weekly" },
  { "value": 3, "caption": "Monthly" }
  ];
  _period = this.lovstatusType[0].value;

  lovTransType = [{ "value": "All", "caption": "All" },
  { "value": "1", "caption": "Wallet to wallet" },
  { "value": "2", "caption": "Merchant payment" },
  { "value": "4", "caption": "Agent to wallet" },
  { "value": "5", "caption": "Wallet to agent" }
  ];

  lovMonths =[{"value":"Jan","caption":"January"},            
              {"value":"Feb","caption":"February"},
              {"value":"Mar","caption":"March"},
              {"value":"Apr","caption":"April"},
              {"value":"May","caption":"May"},
              {"value":"Jun","caption":"June"},
              {"value":"Jul","caption":"July"},
              {"value":"Aug","caption":"August"},
              {"value":"Sep","caption":"September"},
              {"value":"Nov","caption":"November"},
              {"value":"Oct","caption":"October"},
              {"value":"Dec","caption":"December"},];
  lovYears = [{"value":"1","caption":"2019"},
              {"value":"2","caption":"2020"}];
  

  _ButtonInfo = { "role": "", "formname": "", "button": "" };


  _sessionObj = this.getSessionObj();
  getSessionObj() {
    return { "sessionID": "", "userID": "" };
  }
  _obj = this.getDefaultObj();
  
  getDefaultObj() {
    return {
      "sessionID": "", "userID": "", "fromDate": "", "toDate": "", "fromAccount": "", "alldate": true, "t1": "", "t2": "", "transtype": "All", "condition": "", "period": "",
      "toAccount": "", "totalCount": 0, "currentPage": 1, "pageSize": 10, "fileType": "PDF","months":"January","year":"2019"
    };
  }

  constructor(private el: ElementRef, private l_util: ClientUtil, private renderer: Renderer, private ics: RpIntercomService, private _router: Router, private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences, private dashboard: DashboardService, private sanitizer: DomSanitizer) {
    if (!ics.getRole() || ics.getRole() == 0) this._router.navigate(['/login']);
    this.gotoBarChart();
    this.WalletToWallet();
    this.WalletToWalletforWeekly();
  }

  setTodayDateObj() {
    this._dates.fromDate = this._util.changestringtodateobject(this.today);
  }
  setDateObjIntoString() {
    this._obj.fromDate = this._util.getDatePickerDateymd(this._dates.fromDate);
  }

  ngOnInit() {
    //this.gotoBarChart();

  }

  formatNumber(amt) {

    if (amt != undefined && amt != "0") {
      return this.thousand_sperator(parseFloat(amt).toFixed(2));
    } else {
      return "0.00";
    }
  }
  thousand_sperator(num) {
    if (num != "" && num != undefined && num != null) {
      num = num.replace(/,/g, "");
    }
    var parts = num.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts;// return 100,000.00
  }



  restrictSpecialCharacter(event, fid, value) {
    if ((event.key >= '0' && event.key <= '9') || event.key == '.' || event.key == ',' || event.key == 'Home' || event.key == 'End' ||
      event.key == 'ArrowLeft' || event.key == 'ArrowRight' || event.key == 'Delete' || event.key == 'Backspace' || event.key == 'Tab' ||
      (event.which == 67 && event.ctrlKey === true) || (event.which == 88 && event.ctrlKey === true) ||
      (event.which == 65 && event.ctrlKey === true) || (event.which == 86 && event.ctrlKey === true)) {
      if (fid == 101) {
        if (/^[\\\"\'\;\:\>\|~`!@#\$%^&*\(\)]$/i.test(event.key)) {
          event.preventDefault();
        }
      }

      if (value.includes(".")) {
        if (/^[\\\"\'\;\:\>\|~`!@#\$%^&*.\(\)]$/i.test(event.key)) {
          event.preventDefault();
        }
      }
    }
    else {
      event.preventDefault();
    }
  }

  showMsg(msg, bool) {
    if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
    if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
    if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
  }
  logout() {
    this._router.navigate(['/login']);
  }

  changeTo(aIndex)
  {
    this._period = this.lovstatusType[aIndex].value;
    if (this._period == 1){
      this.dashboard.generateBarChartWTW(
        'newSummary1',
        '',
        '',
        '',
        ''
      );
      this.WalletToWallet();
    }
    else if (this._period == 2){
      
      this.WalletToWalletforWeekly();
        }
  }


  gotoBarChart() {
      try {
      let p = { "userID": this.ics._profile.userID, "sessionID": this.ics._profile.sessionID };
      let url: string = this.ics.cmsurl + 'ServiceReportAdm/getDashBoardData?period=' + this._obj.period + '&transType=' + this._obj.transtype;
      let json: any = p;
     // jQuery("#loader").modal();
     this._mflag = false;
      this.http.doPost(url, json).subscribe(
        data => {
         
          let d; let day1, day2, day3, day4;
          let fourDays ;
          let tempObj;
          let allTrans;
          if (data != undefined && data != null && data != '') {
            if (data.code == '0000') {
              let m = [];
              m[0] = data.wldata;
              data.wldata = m;

            }
            d = data.arr;
            this.tempAllTransactions = data.wldata;
            let tempArray = [];
            if (!Array.isArray(this.tempAllTransactions)) {
              tempArray.push(this.tempAllTransactions);
              this.tempAllTransactions = tempArray;
            }
            let W_to_WList = [];
            let M_PaymentList = [];
            let A_to_WalletList = [];
            let W_to_AgentList = [];
            let t1 = 0, t2 = 0, t3 = 0, t4 = 0;
            let c1 = 0, c2 = 0, c3 = 0, c4 = 0;
            let d1 = [], d2 = [], d3 = [], d4 = [];
            for (let i = 0; i < this.tempAllTransactions.length; i++) {
              if (this.tempAllTransactions[i].transType == 1) {
                t1 += parseInt(this.tempAllTransactions[i].amount);
                c1++;                
              }
             else if (this.tempAllTransactions[i].transType == 2) {
              t2 += parseInt(this.tempAllTransactions[i].amount);
              c2++;
             }
              else if (this.tempAllTransactions[i].transType == 4) {
                t3 += parseInt(this.tempAllTransactions[i].amount);
                c3++;               
              }
              else if (this.tempAllTransactions[i].transType == 5) {
                t4 += parseInt(this.tempAllTransactions[i].amount);
                c4++;              
              }
            }       
            
            W_to_WList = [{ count: c1 }, { total: t1 }];
            M_PaymentList = [{ count: c2 }, { total: t2 }];
            A_to_WalletList  = [{ count: c3 }, { total: t3 }];
            W_to_AgentList = [{ count: c4 }, { total: t4 }];
            allTrans = [{name:"Wallet to Wallet",transType:W_to_WList},
                        {name:"Merchant Payment",transType:M_PaymentList},
                        {name:"Agent to Wallet",transType:A_to_WalletList},
                        {name:"Wallet to Agent",transType:W_to_AgentList}];
            let allTransactions = [];
            let index = 0;
            let dataSeries = [];
            let color = ["#337AB7", "#FF6384", "#36A2EB", "#FFCE56", "#CC2EFA", "#FF4000", "#0101DF", "#3B0B17", "#8A0829", "#DF013A",
              "#337AB7", "#FF6384", "#36A2EB", "#FFCE56", "#CC2EFA", "#FF4000", "#0101DF", "#3B0B17", "#8A0829", "#DF013A"];
              let i = 1;
              //let tempObj ;
            for ( let j = 0; j <allTrans.length;j++){
              
              allTransactions.push(allTrans[j].name);
              tempObj = {
              name  :  allTrans[j].name,
              y     :  allTrans[j].transType[i].total,
              count :  allTrans[j].transType[0].count,
              color :  color[index]
              };
            dataSeries.push(tempObj);
            index ++;
            }       
            
            for (let i = 0; i < dataSeries.length; i++) {
              if (this.minBalance > dataSeries[i].y) {
                this.minBalance = dataSeries[i].y;
              }
            }

            this.dashboard.generateBarChart(
              'newSummary',
              [{ "name": " ", "data": dataSeries, color: "White" }],
              allTransactions,
              '',
              this.minBalance
            );
            this._mflag = true;
          }
        },
        error => {
          // Hide loading animation
          jQuery("#loader").modal('hide');
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
          else {

          }
        }, () => { });
    } catch (e) {
      alert("Invalid URL");
    }

  }
  
  WalletToWallet(){
    try {
      let p = { "userID": this.ics._profile.userID, "sessionID": this.ics._profile.sessionID };
      let url: string = this.ics.cmsurl + 'ServiceReportAdm/walletTowallet?period=' + this._period;
      let json: any = p;
     // jQuery("#loader").modal();
     this._mflag = false;
      this.http.doPost(url, json).subscribe(
        data => {
         
          let d; let day1, day2, day3, day4;
          let fourDays ;
          let tempObj;
          let allTrans;
          if (data != undefined && data != null && data != '') {
            if (data.code == '0000') {
              let m = [];
              m[0] = data.wldata;
              data.wldata = m;

            }
            d = data.arr;
            this.tempAllTransactions = data.wldata;
            let tempArray = [];
            if (!Array.isArray(this.tempAllTransactions)) {
              tempArray.push(this.tempAllTransactions);
              this.tempAllTransactions = tempArray;
            }
            let W_to_WList = [];
            let M_PaymentList = [];
            let A_to_WalletList = [];
            let W_to_AgentList = [];
            let t1 = 0, t2 = 0, t3 = 0, t4 = 0;
            let c1 = 0, c2 = 0, c3 = 0, c4 = 0;
            let d1 = [], d2 = [], d3 = [], d4 = [];
            if (this._period == 1){
            for (let i = 0; i < this.tempAllTransactions.length; i++) {
              if (this.tempAllTransactions[i].transType == 1) {
                let j = 0;
                if (this.tempAllTransactions[i].day == d[j++]) {
                  t1 += parseInt(this.tempAllTransactions[i].amount);
                  c1++;
                }
                else if (this.tempAllTransactions[i].day == d[j++]) {
                  t2 += parseInt(this.tempAllTransactions[i].amount);
                  c2++;
                }
                else if (this.tempAllTransactions[i].day == d[j++]) {
                  t3 += parseInt(this.tempAllTransactions[i].amount);
                  c3++;
                }
                else if (this.tempAllTransactions[i].day == d[j++]) {
                  t4 += parseInt(this.tempAllTransactions[i].amount);
                  c4++;
                }
              }             
            }
          } 
          
          else if(this._period == 2){

          }
            day1 = [{ count: c1 }, { total: t1 }];
            day2 = [{ count: c2 }, { total: t2 }];
            day3 = [{ count: c3 }, { total: t3 }];
            day4 = [{ count: c4 }, { total: t4 }];
            fourDays = [{name:"Day1",day:day1},
                        {name:"Day2",day:day2},
                        {name:"Day3",day:day3},
                        {name:"Day4",day:day4}];
            let allTransactionsForFourDays = [];
            let index = 0;
           
            let color = ["#337AB7", "#FF6384", "#36A2EB", "#FFCE56", "#CC2EFA", "#FF4000", "#0101DF", "#3B0B17", "#8A0829", "#DF013A",
              "#337AB7", "#FF6384", "#36A2EB", "#FFCE56", "#CC2EFA", "#FF4000", "#0101DF", "#3B0B17", "#8A0829", "#DF013A"];
              let i = 1;
              //let tempObj ;
            for ( let j = 0; j <fourDays.length;j++){
              
              allTransactionsForFourDays.push(fourDays[j].name);
              tempObj = {
              name  :  fourDays[j].name,
              y     :  fourDays[j].day[i].total,
              count :  fourDays[j].day[0].count,
              color :  color[index]
              };
            this.dataSeries.push(tempObj);
            index ++;
            }
            let minBalance = 0;
            for (let i = 0; i < this.dataSeries.length; i++) {
              if (minBalance > this.dataSeries[i].y) {
                minBalance = this.dataSeries[i].y;
              }
            }

            this.dashboard.generateBarChartWTW(
              'newSummary1',
              [{ "name": " ", "data": this.dataSeries, color: "White" }],
              allTransactionsForFourDays,
              '',
              minBalance
            );
            this._mflag = true;
          }
        },
        error => {
          // Hide loading animation
          jQuery("#loader").modal('hide');
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
          else {

          }
        }, () => { });
    } catch (e) {
      alert("Invalid URL");
    }
  }

  WalletToWalletforWeekly(){
    try {
      let p = { "userID": this.ics._profile.userID, "sessionID": this.ics._profile.sessionID };
      let url: string = this.ics.cmsurl + 'ServiceReportAdm/walletTowallet?period=' + this._period;
      let json: any = p;
     // jQuery("#loader").modal();
     this._mflag = false;
      this.http.doPost(url, json).subscribe(
        data => {
         
          let week; let week1,week2, week3, week4;
          let fourWeeks ;
          let tempObj;
          let allTrans;
          if (data != undefined && data != null && data != '') {
            if (data.code == '0000') {
              let m = [];
              m[0] = data.wldata;
              data.wldata = m;

            }
            week = data.arrWeek;
            this.tempAllTransactions = data.wldata;
            let tempArray = [];
            if (!Array.isArray(this.tempAllTransactions)) {
              tempArray.push(this.tempAllTransactions);
              this.tempAllTransactions = tempArray;
            }
            let firstWeek = [];
            let secondWeek = [];
            let thirdWeek = [];
            let fourWeek = [];
            let t1 = 0, t2 = 0, t3 = 0, t4 = 0;
            let c1 = 0, c2 = 0, c3 = 0, c4 = 0;
            let d1 = [], d2 = [], d3 = [], d4 = [];
            week1 =week[0];
            week2 =week[1];
            week3 =week[2];
            week4 =week[3];
            let week1start = week1[0];
            let week1End = week1[week1.length - 1];
            let week2start = week2[0];
            let week2End = week2[week2.length - 1];
            let week3start = week3[0];
            let week3End = week3[week3.length - 1];
            let week4start = week4[0];
            let week4End = week4[week4.length - 1];
            if (this._period == 2){
              for (let j = 0 ;j < week1.length;j++){
                for(let i = 0 ;i < this.tempAllTransactions.length;i++){
                  if (this.tempAllTransactions[i].day == week1[j] ){
                    t1 += parseInt(this.tempAllTransactions[i].amount);
                    c1++;
                    this.tempAllTransactions.splice(i, 1);
                    i--;
                  }
                }
              }
              for (let j = 0 ;j < week2.length;j++){
                for(let i = 0 ;i < this.tempAllTransactions.length;i++){
                  if (this.tempAllTransactions[i].day == week2[j] ){
                    t2 += parseInt(this.tempAllTransactions[i].amount);
                    c2++;
                    this.tempAllTransactions.splice(i, 1);
                    i--;
                  }
                }
              }
              for (let j = 0 ;j < week3.length;j++){
                for(let i = 0 ;i < this.tempAllTransactions.length;i++){
                  if (this.tempAllTransactions[i].day == week3[j] ){
                    t3 += parseInt(this.tempAllTransactions[i].amount);
                    c3++;
                    this.tempAllTransactions.splice(i, 1);
                    i--;
                  }
                }
              }
              for (let j = 0 ;j < week4.length;j++){
                for(let i = 0 ;i < this.tempAllTransactions.length;i++){
                  if (this.tempAllTransactions[i].day == week4[j] ){
                    t4 += parseInt(this.tempAllTransactions[i].amount);
                    c4++;
                    this.tempAllTransactions.splice(i, 1);
                    i--;
                  }
                }
              }
            // for (let i = 0; i < this.tempAllTransactions.length; i++) {
            //     if (this.tempAllTransactions[i].day >= week1start && this.tempAllTransactions[i].day <= week1End ){
            //       t1 += parseInt(this.tempAllTransactions[i].amount);
            //       c1++;
            //     }
            //     else if (this.tempAllTransactions[i].day >= week2start && this.tempAllTransactions[i].day <= week2End ){
            //       t2 += parseInt(this.tempAllTransactions[i].amount);
            //       c2++;
            //     }
            //     else if (this.tempAllTransactions[i].day >= week3start && this.tempAllTransactions[i].day <= week3End ){
            //       t3 += parseInt(this.tempAllTransactions[i].amount);
            //       c3++;
            //     }
            //     else if (this.tempAllTransactions[i].day >= week4start && this.tempAllTransactions[i].day <= week4End ){
            //       t4 += parseInt(this.tempAllTransactions[i].amount);
            //       c4++;
            //     }


              
            // }
          } 
          
            week1 = [{ count: c1 }, { total: t1 }];
            week2 = [{ count: c2 }, { total: t2 }];
            week3  = [{ count: c3 }, { total: t3 }];
            week4 = [{ count: c4 }, { total: t4 }];
            fourWeeks = [{name:"Week1",day:week1},
                        {name:"Week2",day:week2},
                        {name:"Week3",day:week3},
                        {name:"Week4",day:week4}];
            
            let index = 0;
            let dataSeries = [];
            let color = ["#337AB7", "#FF6384", "#36A2EB", "#FFCE56", "#CC2EFA", "#FF4000", "#0101DF", "#3B0B17", "#8A0829", "#DF013A",
              "#337AB7", "#FF6384", "#36A2EB", "#FFCE56", "#CC2EFA", "#FF4000", "#0101DF", "#3B0B17", "#8A0829", "#DF013A"];
              let i = 1;
              //let tempObj ;
            for ( let j = 0; j <fourWeeks.length;j++){
              
              this.allTransactionsForFourDays.push(fourWeeks[j].name);
              tempObj = {
              name  :  fourWeeks[j].name,
              y     :  fourWeeks[j].day[i].total,
              count :  fourWeeks[j].day[0].count,
              color :  color[index]
              };
            dataSeries.push(tempObj);
            index ++;
            }
            let minBalance = 0;
            for (let i = 0; i < dataSeries.length; i++) {
              if (minBalance > dataSeries[i].y) {
                minBalance = dataSeries[i].y;
              }
            }

            this.dashboard.generateBarChartWTW(
              'newSummary1',
              [{ "name": " ", "data": dataSeries, color: "White" }],
              this.allTransactionsForFourDays,
              '',
              minBalance
            );
            this._mflag = true;
          }
        },
        error => {
          // Hide loading animation
          jQuery("#loader").modal('hide');
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
          else {

          }
        }, () => { });
    } catch (e) {
      alert("Invalid URL");
    }
  }


}


