import { NgModule, CUSTOM_ELEMENTS_SCHEMA }       from '@angular/core';
import { FormsModule }    from '@angular/forms';
import { CommonModule }   from '@angular/common';
import { RpInputModule } from '../framework/rp-input.module';
import { MyDatePickerModule } from 'mydatepicker';
import { RpHttpService } from '../framework/rp-http.service';
import { AdminPagerModule } from '../util/adminpager.module';
import { PagerModule } from '../util/pager.module';
import { AdvancedSearchModule } from '../util/advancedsearch.module';
import {MultiselectModule} from '../util/multiselect.module';
import { DashBoardRouting }from './dashboard.routing';
import { frmDashBoard} from './frmDashBoard.component';
import { FrmDashboardService} from './frmdashboard-service';
import { DashboardService } from '../framework/dashboard-service';
//import { ChartsModule } from 'ng2-charts';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RpInputModule,    
    AdminPagerModule,
    PagerModule,
    MyDatePickerModule,
    MultiselectModule,
    AdvancedSearchModule,
    DashBoardRouting,
   // ChartsModule
  ],
  exports : [],
  declarations: [   
    frmDashBoard,
  ],
  providers: [
    RpHttpService,
    FrmDashboardService,
    DashboardService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class dashBoardModule {}