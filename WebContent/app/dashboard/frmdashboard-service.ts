import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {Subject}    from 'rxjs/Subject'; 

declare var Highcharts;
@Injectable()
export class FrmDashboardService {

  private _rpDashboardSource = new Subject<any>();
	rpDashboard$ = this._rpDashboardSource.asObservable();
	_obj = this.getObj();
	constructor() {
		
	}

	getObj(){
		return {
			"caption":"","value":0
		};
	}

  generatePieChart(id, d,maintitle,param,psize) {
	    	Highcharts.setOptions({
                        chart: {
                                style: {
                                        fontFamily: 'Pyidaungsu'
                                }
                        }
        });
				
    		Highcharts.chart(id,
				{
					chart : { plotBackgroundColor : null, plotBorderWidth : null, plotShadow : false, type : 'pie',marginTop:1 },
					title : { text : maintitle },
					tooltip : { pointFormat : '</b>{point.percentage:.2f}%</b>' },
					plotOptions : {
						pie : {
							allowPointSelect : true,
							cursor : 'pointer',
							size:psize,
						  events: {
								
								click: (event) => {
									if(param != ""){
										this.goClick(event.point.options.val,param)
									}
									
								},
							
							},
							dataLabels : {
								enabled : true,
								format : '{point.percentage:.2f}%',
								style : { color : (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black' }
							},
							showInLegend : true
						}
					},
					credits : { enabled : false },
					series : [{ name : 'Brands', colorByPoint : true, data : d }],
					legend: {align: 'left', layout: 'vertical',verticalAlign: 'left', font: '10px',x: 1,y:150,}
				
    
			
				}
    		);
    	}


	generateLineChart(id, d, c,xt, yt, minNumber,title,param,pointerflag,paramJsonArr,paramJsonArrflag){
		Highcharts.chart(id, {
	            chart: { type: 'line' },
	            title: { text: title },
	            subtitle: { text: '' },
	            xAxis: { 
	            	categories: c 
	            },
	            yAxis: {
	                title: {
	                    text: yt
	                }
	            }, 
	            credits: { enabled: false },
	            plotOptions: {
	                line: {
	                    dataLabels: {
	                        enabled: true
	                    },
	                    enableMouseTracking: false
	                }
	            },
	            series: d
		   });
	}

	generateBarChart(id, d, c,xt, yt, minNumber,txttitle,param,pointerflag,paramJsonArr,paramJsonArrflag) {
		Highcharts.chart(id, {
			chart: { type: 'column'},
			title: { text: txttitle },
			subtitle: { text: '' },
			xAxis: {
				categories: c,
				crosshair: true,
				title: { text: xt }
			},
			yAxis: {
				min: minNumber,
				title: { text: yt }
			},
			credits: { enabled: false },
			tooltip: {
				headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y:.2f}</b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				column: {
					pointPadding: 0.2,
					pointWidth: 28,
					borderWidth: 0,
					allowPointSelect : pointerflag,
					cursor : 'pointer',
					events: {
						
						click: (event) => {
							if(param == ""){
								this.goto(event.point.category,param,paramJsonArr,paramJsonArrflag);
							}
							
						},
					
					}
				}
			},
			series: d
			
		});
	}

	generate3DPieChart(id, d, title,psize){
		Highcharts.chart(id, {
			chart: {
			  type: 'pie',
			  options3d: {
				enabled: true,
				alpha: 45,
				beta: 0
			  }
			},
			title: {
			  text: title
			},
			tooltip: {
			  pointFormat: ' <b>{point.percentage:.2f}%</b>'
			},
			plotOptions: {
			  pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				depth: 35,
				size:psize,
				dataLabels: {
				  enabled: true,
				  format: '{point.name}'
				}
			  }
			},
			credits : { enabled : false },
			series: [{
				type: 'pie',
				data: d
			  }]
		  });
	}

	goClick(val,param){
		this._obj.caption = param;
		this._obj.value = val;
		this._rpDashboardSource.next(this._obj);
	}

	goto(val,param,paramArr,paramflag){ 
		if(paramflag){
			for(let i = 0; i < paramArr.length; i++){
				if(val == paramArr[i].caption){
					val = paramArr[i].value;
					break;
				}
			}
		}
		this._obj.caption = param;
		this._obj.value = val;
		this._rpDashboardSource.next(this._obj);
	}

}
