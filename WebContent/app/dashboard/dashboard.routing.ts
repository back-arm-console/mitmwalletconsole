import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyDatePickerModule } from 'mydatepicker';
import { frmDashBoard} from './frmDashBoard.component';

const DashBoardRoutes: Routes = [
  
  { path: 'dash_board', component: frmDashBoard },
  { path: 'dash_board/:cmd', component: frmDashBoard },
  { path: 'dash_board/:cmd/:id', component: frmDashBoard }, 

  
  
];

export const DashBoardRouting: ModuleWithProviders = RouterModule.forChild(DashBoardRoutes);