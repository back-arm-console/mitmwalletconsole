import { Component, ElementRef, enableProdMode, Renderer } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { ActivatedRoute, Router } from '@angular/router';
import { IMyDpOptions } from 'mydatepicker';
import { Subscription } from 'rxjs/Subscription';
import { RpHttpService } from '../framework/rp-http.service';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpReferences } from '../framework/rp-references';
import { ClientUtil } from '../util/rp-client.util';
declare var jQuery: any;

enableProdMode();
@Component({
  selector: 'dailyreport',
  template: ` 
  <div *ngIf='_rpList' >
  <div class="container-fluid">
  <div class="row clearfix">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
      <form class= "form-horizontal" ngNoForm>        
  				<legend>Daily Report</legend>
  				<div class="cardview list-height"> 
  				  <div class="row col-md-12">  
  				  <button class="btn btn-sm btn-primary" type="button" (click)="goClear()">Clear</button>              
  				  <button class="btn btn-sm btn-primary" type="button" (click)="goPrint()">Print</button>          
                   </div>
           <div class="row col-md-12">&nbsp;</div>
                <div class="form-group"> 
                   <div class="col-md-12">
                       <div class="form-group">
                       <div class="col-md-2" >&nbsp;</div>
                           <div class="col-md-2" >
                               <input type="radio" value="daybook" name="rdType" [(ngModel)]="obj.rdoType" (click)="switchRadio($event.target.value)" [checked]="rdoDayBook"> <label style="font-weight:normal;" disabled>DayBook</label>
                            </div>
                            <div class="col-md-2" >  
                                <input type="radio" value="transferscroll" name="rdType" [(ngModel)]="obj.rdoType" (click)="switchRadio($event.target.value)" [checked]="rdoTransferScroll"> <label style="font-weight:normal;" disabled>Transfer Scroll</label>
                            </div>
                            <div class="col-md-2" >  
                                <input type="radio" value="cleancash" name="rdType" [(ngModel)]="obj.rdoType" (click)="switchRadio($event.target.value)" [checked]="rdoCleanCash"> <label style="font-weight:normal;" disabled>Clean Cash</label>
                            </div>
                       </div> 
                       <div class="form-group">
                           <label class="col-md-2" >Product Type</label>
                           <div class="col-md-3">
                             <select  required="true" class="form-control col-md-0"  [(ngModel)]="obj.cboDayBookType"  >
                             <option *ngFor="let obj of _ProductTypeList" value="{{obj.value}}">{{obj.caption}}</option>
                             </select>                     
                           </div>
                           <label class="col-md-2" >Order By</label>
                               <div class="col-md-3">
                                   <select  required="true"   [(ngModel)]="obj.orderBy"   class="form-control col-md-0"  >
                                   <option *ngFor="let obj of _OrderByList" value="{{obj.value}}">{{obj.caption}}</option>
                                   </select>                      
                               </div>
                        </div>
                        <div class="form-group">
                               <label class="col-md-2" >Serial No.</label>
                               <div class="col-md-3">
                                   <input id="idSerialNo1" type="text" class="input-sm fontSizeSmall fontWeightNormal" style="width: 46%;  border-radius:5px;border:1px #D2D2D2 solid; " [ngModelOptions]="{standalone: true}" 
                                    [(ngModel)]="obj.serialNoFrom" (keypress)="keyPressNumber($event)" >
                                    <label >-</label>
                                    <input id="idSerialNo2" type="text" class="input-sm fontSizeSmall fontWeightNormal" style="width: 50%;  border-radius:5px;border:1px #D2D2D2 solid; " [ngModelOptions]="{standalone: true}" 
                                    [(ngModel)]="obj.serialNoTo"  (keypress)="keyPressNumber($event)" >
                                </div> 
                                <label class="col-md-2">Date</label>								
                                <div class="col-md-3">
                                    <my-date-picker name="mydate"   [options]="myDatePickerOptions"  [(ngModel)]="_dates.date" (ngModelChange)="changedate()" ngDefaultControl></my-date-picker>                  
                                </div>
                        </div>
                        <div class="form-group">  
                            <label class="col-md-2">Currency</label>
                            <div class="col-md-3">
                            <select  required="true"  [(ngModel)]="obj.curCode"     class="form-control col-md-0"  >
                            <option *ngFor="let code of _CurrencyList" value="{{code.value}}">{{code.caption}}</option>
                             </select>
                            </div>
                            <label class="col-md-2" >Report By</label>
                            <div class="col-md-3">
                              <select   required="true"   [(ngModel)]="obj.reportBy"  class="form-control col-md-0"  >
                                          <option *ngFor="let item  of _ReportByList" value="{{item.value}}" >{{item.caption}}</option> 
                              </select>                     
                          </div>
                        </div>
                        <div class="form-group">
                					<label class="col-md-2" >Report Format</label>
                						<div class="col-md-3">
                							<select   required="true"   [(ngModel)]="obj.cboRFormat"  class="form-control col-md-0"  >
                                            <option *ngFor="let obj of _ReportFormatList" value="{{obj.value}}">{{obj.caption}}</option>
                							</select>                      
                						</div>
                        </div>
                        <div class="form-group">
                              <label class="col-md-2" >Report Options</label>
                              <div *ngIf="_hidechkReversal"  class="col-md-2">
                                    <label>
                                    <input type='checkbox' [(ngModel)]="obj.chkReversal" [disabled]="_disablechkReversal" [ngModelOptions]="{standalone: true}" >
                                        Reversal Inclusive
                                    </label>                 
                                </div>
                                <div  *ngIf="_hidechkOrder" class="col-md-2">
                                    <label>
                                    <input type='checkbox'[(ngModel)]="obj.chkOrder" [disabled]="_disablechkOrder" [ngModelOptions]="{standalone: true}" >
                                    Descending Order
                                    </label>                 
                                </div>
                               
                                <div  *ngIf="_hidechkSummary" class="col-md-2">
                                    <label>
                                    <input type='checkbox' [(ngModel)]="obj.chkSummary"  [disabled]="_disablechkSummary" [ngModelOptions]="{standalone: true}" >
                                    Summary
                                    </label>                 
                                </div>
                                <div  *ngIf="_hidechkByMonth" class="col-md-2">
                                    <label>
                                    <input type='checkbox'  [(ngModel)]="obj.chkByMonth"  [disabled]="_disablechkByMonth" [ngModelOptions]="{standalone: true}" >
                                    By Month
                                    </label>                 
                                </div>
                        </div>
                    </div>
              </div>
            </div>
      </form>
      </div>
      <div>
      <div id="downloadPdf" style="display: none;width: 0px;height: 0px;"></div>
      <div id="downloadExcel" style="display: none;width: 0px;height: 0px;"></div>
   </div>
  </div>
</div>
</div>     
    <div [hidden] = "_mflag">
      <div class="modal" id="loader"></div>
   </div> 
   <rpt-container *ngIf='_divPrint' (rpClose)="goClosePrint()" [rpSrc]="_printUrl"></rpt-container> 																																			
  `,
})
export class DailyReport {
  subscription: Subscription;
  _util: ClientUtil = new ClientUtil();
  today = this._util.getTodayDate();
  myDatePickerOptions: IMyDpOptions = this._util.getDatePicker();
  dateobj: { date: { year: number, month: number, day: number } };
  _dates = { "date": this.dateobj};
  _printUrl: SafeResourceUrl;
  
  _pdfdata: boolean;
  _mflag = true;
  _rpList=true;
  _divPrint=false;
  _OrderByList=[];
  _ReportByList=[];
   _ReportFormatList=[];  
   _CurrencyList=[{"value":"MMK","caption":"MMK"}];
   _ProductTypeList=[{"value":"00","caption":"Domestic Account"},{"value":"01","caption":"Wallet Account"}];
   _CounterList=[{"value":"ALL","caption":"ALL"}];
   bnk:any=15;
   _hide=true;
   _curname="";
   isDBanking = false;	
 

  ////////
  _hidechkOD=false;
   _hidechkSchedule=false;
   _hidechkReversal=true;
   _hidechkOrder=true; 
   _hidechkRevaluate=true; 
   _hidechkSummary=false;
   _hidechkByMonth=false;
   ////////
   _disablechkrevaluate=true;
   _disablechkReversal=false;
   _disablechkOrder=false;
   _disablechkSummary=false;
   _disablechkByMonth=false;
   _disablechkOD=false;
   _disablechkSchedule=false;
   
   _disablecurCode=false;
   /////
 
  getSessionObj() {
      return { "sessionID": "", "userID": "" };
  }
 
  obj = this.getDefaultObj();
  getDefaultObj() {
    return {"rdoType":"daybook",
        "cboDayBookType" : "", 
        "branchCode" : "",
        "myDate" : "",
        "reportBy" : "2",
        "reportOptions" : "",
        "orderBy" : "A",        
        "serialNoFrom" : "1",
        "serialNoTo" : "9999999999",        
        "curCode" : "MMK",
        "counterType":"ALL",
        "cboRFormat" : "",
        "_disablechkReversalI" : false,
        "_disablechkDO" : false,
        "_disablechkrevaluate" : false,
        "chkReversal":false,
        "chkOrder":false,
        "chkrevaluate":false,
        "chkSummary":false,
        "chkByMonth":false,            
        "chkOD":false,
        "chkSchedule":false
        //"cboCounter":""
    };
}
  constructor(private el: ElementRef, private renderer: Renderer, private ics: RpIntercomService, private _router: Router, private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences, private sanitizer: DomSanitizer) {
    if (!ics.getRole() || ics.getRole() == 0) this._router.navigate(['/login']);
    this.setTodayDateObj();
    this.setDateObjIntoString();
  }
  ngOnInit() {
    this.orderByList();
    this.ReportByList();
    this.switchRadio("daybook");
  }
  setTodayDateObj() {
    this._dates.date = this._util.changestringtodateobject(this.today);
   
  }
  setDateObjIntoString() {
    this.obj.myDate = this._util.getDatePickerDateymd(this._dates.date);
    console.log('My Date'+this.obj.myDate);
  }
  showMsg(msg, bool) {
    if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
    if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
    if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
  }
  logout() {
    this._router.navigate(['/login']);
  }
  
  isDomesticBanking(){
    if(this.bnk==1){
        this.isDBanking = true;
    }else{
        this.isDBanking = false;
    }
}

  keyPressNumber(event: any) {
    const pattern =/[0-9]/;
     let inputChar = String.fromCharCode(event.charCode);
     if (!pattern.test(inputChar)) {
         event.preventDefault();
     }
 } 
 orderByList(){
    this._OrderByList = [];
    this._OrderByList = [{ "caption": "Account Number", "value": "A" }, { "caption": "Trans No", "value": "T" }];
    if(this._OrderByList.length > 0){
            this.obj.orderBy=this._OrderByList[0].value;
    }
}
ReportByList(){
    this._ReportByList = [];
    this._ReportByList = [{ "caption": "GL Code", "value": "2" }];
    if(this._ReportByList.length > 0){
            this.obj.reportBy=this._ReportByList[0].value;
    }
    if(this._ProductTypeList.length>0){
      this.obj.cboDayBookType=this._ProductTypeList[0].value;
    }
}
  switchRadio(rdoType) {
    if (rdoType == "daybook") {
      this._hidechkReversal = true;
      this._hidechkOrder = true;
      this._hidechkSchedule = false;
      this._hidechkRevaluate = true;
      this._disablechkrevaluate = false;
      this.obj.chkrevaluate = true;
      this._hidechkSummary = false;
      this._hidechkByMonth = false;
      this._hide = true;
      this._ReportFormatList = [{ "caption": "Format 1", "value": "1" }, { "caption": "Format 3", "value": "3" }, { "caption": "Format 4", "value": "4" }];
      if (this._ReportFormatList.length > 0) {
        this.obj.cboRFormat = this._ReportFormatList[0].value;
      }

      this._hidechkRevaluate = true;//setVisible(true) 
      
    } else if (rdoType == "transferscroll") {
      this._hidechkReversal = true;
      this._hidechkOrder = true;
      this._hidechkSchedule = false;
      this._hidechkRevaluate = true;
      this._disablechkrevaluate = false;
      this.obj.chkrevaluate = false;
      this._hidechkSummary = false;
      this._hidechkByMonth = false;
      this._hide = true;
      this._ReportFormatList = [{ "caption": "Format 1", "value": "1" }, { "caption": "Format 3", "value": "3" }];
      if (this._ReportFormatList.length > 0) {
        this.obj.cboRFormat = this._ReportFormatList[0].value;
      }
      //////
      this._disablecurCode = false;
      this._hidechkRevaluate = true;//setVisible(true)
      this._hidechkSummary = false;
      if (this.bnk == 1) {
        //this.showCounter(true);
        //bindCounter();
      } else {
        //this.showCounter(false);
      }
      this.obj.chkrevaluate = false;
      this._disablechkrevaluate = false;
    }  else if (rdoType == "cleancash") {
      this._hidechkReversal = true;
      this._hidechkOrder = true;
      this._hidechkSchedule = false;
      this._hidechkRevaluate = false;
      this._hidechkSummary = false;
      this._hidechkByMonth = false;
      this._hide = false;
      this._ReportFormatList = [{ "caption": "Format 1", "value": "1" }];
      if (this._ReportFormatList.length > 0) {
        this.obj.cboRFormat = this._ReportFormatList[0].value;
      }
      ///////////
      this._disablecurCode = false;
      this._hidechkRevaluate = false;
      this._hidechkSummary = false;
      //this.showCounter(false);
      //bindCounter();
      this.obj.chkrevaluate = false;
      this._disablechkrevaluate = false;
    }
  } 
  goPrint(){
    this._rpList = false;
    this._divPrint = true;
    this._printUrl = "";
    //var BCode =this._branchcode;
    var DBDate=this._util.getDatePickerDateymd(this._dates.date);
    var PType=this.obj.cboDayBookType;
    if(PType=="0000" || PType=="00"){
         PType = "GL";
    }
    var tsf=this.obj.serialNoFrom;
    var tst=this.obj.serialNoTo;
		var PS="1";
    var RF="";
    if(this.obj.curCode=="Base" && PType!="GL"){
      RF="1";
      }else{
      RF=this.obj.cboRFormat;
    }
       var RB="";
        RB=this.obj.reportBy;
        var BM="";
        if(this.obj.chkByMonth==true){
            BM="1";
        }  
        var OB=this.obj.orderBy;
        var incReversal=this.obj.chkReversal;
        var t1=false;
        var order =this.obj.chkOrder;
        var t2=false;
        var DBType="";
        var pBCode="";
        var RC="";
        if(this.obj.rdoType=="daybook"){
          DBType="DB";
          PType=PType;
          OB=OB;
          tsf=tsf;
          tst=tst;
          t1=incReversal;
          t2=order;
          DBDate=DBDate;
          pBCode="002";
          PS=PS;
          RC=this.obj.curCode;
          RF=RF;
          RB=RB;
          BM=BM;
       }else if(this.obj.rdoType=="transferscroll"){
          DBType="TS";
          PType=PType;
          OB=OB;
          tsf=tsf;
          tst=tst;
          t1=incReversal;
          t2=order;
          DBDate=DBDate;
          pBCode="002";
          PS=PS;
          RC=this.obj.curCode;
          RF=RF;
          RB=RB;
          BM=BM;
       }
       else if(this.obj.rdoType=="cleancash"){
        DBType="CC";
        PType="";
        OB="";
        tsf="";
        tst="";
        t1=incReversal;
        t2=false;
        DBDate=DBDate;
        pBCode="002";
        PS=PS;
        RC=this.obj.curCode;
        RF=RF;
        RB=RB;
        BM=BM;
     }
     let url = this.ics.icbsrpturl + "/PrepareReport.jsp?";
        //url += "ip="+this.ics._profile.serverIP;
        url += "&UID="+this.ics._profile.userID;
        url += "&RT="+DBType;
        url += "&PType="+PType;
        url += "&Date="+this._util.getDatePickerDateymd(this._dates.date);
        url += "&OB="+OB;
        url += "&SNF="+tsf;
        url += "&SNT="+tst;
        url += "&IRT="+t1;
        url += "&IDO="+t2;
        url += "&BC="+pBCode;
        url += "&PS="+PS;
        url += "&RC="+this.obj.curCode;
        url += "&RF="+RF;
        url += "&RB="+RB;
        url += "&BM="+BM;
        url+="&PDL="+this.obj.cboDayBookType;
        if(this.obj.rdoType=="daybook" || this.obj.rdoType=="transferscroll"){
          url +="&IREV=false"; 
          if(this.bnk==1){
          if(this.obj.branchCode!="999"){
            if(this.obj.rdoType=="daybook" && this.obj.cboDayBookType=="All" || this.obj.rdoType=="transferscroll"){
        }
      }
    }
    else {
      url += "&CID="+"";
   }
  }
  if(this.obj.chkOD==true){
    url += "&PF="+ "OD";
   }       
  //    if(this.isCard){
  //   url += "&PF="+ "CARD";
  //  }
    if(this.obj.chkSchedule==true){
     url += "&Type=" +this.obj.chkSchedule;
   }else{
           url += "&Type=false";
     }
     if(this.obj.cboDayBookType=="daybook" && this.obj.cboDayBookType.includes("Army")){
         url += "&Fields=Army";
     }
     if(this.obj.cboDayBookType=="daybook" && this.obj.cboDayBookType.includes("Schedule")){
         url += "&Fields=Schedule";
     }	
     var Summary ="";	
     if(this.obj.cboDayBookType=="GL"  && this.obj.rdoType=="daybook"){
         if(this.obj.chkSummary==true){
             Summary="SummaryOn";
         }else{
             Summary="SummaryOff";
         }
         url += "&AccName="+ Summary;
     }
      url += "&FBC=002"; 
     this._printUrl = url;       
}
goClosePrint(){
  this._divPrint = false;
  this._rpList = true;
}
goClear(){
  this.obj=this.getDefaultObj();
  this.obj.cboRFormat="1";
  this.obj.cboDayBookType="00";
}
 
}
