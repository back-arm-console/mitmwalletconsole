import { Component, enableProdMode, ElementRef, Renderer } from '@angular/core';
import { IMyDpOptions } from 'mydatepicker';
import { ClientUtil } from '../util/rp-client.util';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { Router, ActivatedRoute } from '@angular/router';
import { RpHttpService } from '../framework/rp-http.service';
import { RpReferences } from '../framework/rp-references';
declare var jQuery: any;
enableProdMode();
@Component({
  selector: 'wallettopup-report',
  template: ` 
  <div *ngIf="!_pdfdata">  
  <div class="container-fluid">
  <div class="row clearfix">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
		<form class="form-horizontal" ngNoForm>
			<legend>Settlement Report</legend>
			<div class="cardview list-height"> 
				<div class="row col-md-12">
          <button class="btn btn-sm btn-primary" type="button" (click)="goClear()">Clear</button>
          <button class="btn btn-sm btn-primary" type="button" (click)="goShow()">Show</button>              
          <button class="btn btn-sm btn-primary" type="button" (click)="goProcess()">Process</button> 
					<button class="btn btn-sm btn-primary" type="button" (click)="goPrint()">Export</button>
				</div>

				<div class="row col-md-12">&nbsp;</div>
				  <div class="form-group">
					<div class=" col-md-12">
						<div class="form-group">            
							<label class="col-md-2">From Date</label>								
							<div class="col-md-3">
								<my-date-picker name="mydate"  [options]="myDatePickerOptions" [(ngModel)]="_dates.fromDate" ngDefaultControl></my-date-picker>                  
							</div> 
						</div>				
						<!--<div class="form-group">
						<rp-input  [(rpModel)]="_obj.fileType" rpRequired ="true" rpLabelClass = "col-md-2" rpClass="col-md-3" rpType="ref010" rpLabel="File Type" ></rp-input>
						</div>  -->			
					</div>
					<div class=" col-md-12" style="overflow-x:auto">       
              <table  class="table table-striped table-condense table-hover tblborder" id="tblstyle" *ngIf="tableflag">
              <thead>
                <tr>
                <th style="background-color: #e6f2ff; color: #333333"><div style="width:50">No. </div></th>
                <th style="background-color: #e6f2ff; color: #333333"><div style="width:150">From Account </div></th>
                <th style="background-color: #e6f2ff; color: #333333"><div style="width:150">To Account</div></th>
                <th style="background-color: #e6f2ff; color: #333333"><div style="width:100">Date </div></th>
                <th style="background-color: #e6f2ff; color: #333333"><div style="width:150">Merchant Name</div></th>  
                <th style="background-color: #e6f2ff; color: #333333"><div style="width:100;float:right">Amount</div></th>
                <th style="background-color: #e6f2ff; color: #333333"><div style="widtsh:150;float:right">Commission Amount</div></th>  
                </tr>
              </thead>
                <tbody>
                <tr *ngFor="let obj of _list.wldata; let i= index"> 
                  <td>{{i+1}}</td>
                  <td>{{obj.drAccNumber}}</td>
                  <td> {{obj.crAccNumber}}</td> 
                  <td class="uni"> {{obj.settlementDate}} </td>
                  <td class="uni"> {{obj.merchantName}} </td>    
                  <td class="uni" style="float:right"> {{obj.amount}}</td>
                  <td class="uni" class="right"> {{obj.commissionCharges}} </td> 
                </tr> 
                </tbody>                     
              </table>
          </div>
				</div>				
			</div>
        </form> 
    </div>
        </div>
        <div id="downloadExcel" style="display: none;width: 0px;height: 0px;"></div>       				
        <div id="downloadPdf" style="display: none;width: 0px;height: 0px;"></div>       				
	</div>
</div>    
	
	<div [hidden] = "_mflag">
	  <div class="modal" id="loader"></div>
	</div>																									
  `,
})
export class settlementReport {
    _util: ClientUtil = new ClientUtil();
    myDatePickerOptions: IMyDpOptions = this._util.getDatePicker();
    dateobj: { date: { year: number, month: number, day: number } };
    _dates = { "fromDate": this.dateobj, "toDate": this.dateobj };
    today = this._util.getTodayDate();
    _obj = {"sessionID":"","userID":"","fileType":"EXCEL","fromDate":"",};
    _mflag = true;
    _list = {"wldata": []};
    tableflag = false;
    constructor(private el: ElementRef, private renderer: Renderer, private ics: RpIntercomService, private _router: Router, 
        private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences) {
        if (!ics.getRole() || ics.getRole() == 0) this._router.navigate(['/login']);
        this.setTodayDateObj();
        this.setDateObjIntoString();
      }
      setTodayDateObj() {
        this._dates.fromDate = this._util.changestringtodateobject(this.today);
      }
      setDateObjIntoString() {
        this._obj.fromDate = this._util.getDatePickerDateymd(this._dates.fromDate);
      }
    goShow(){
      this._mflag = false;
        this.setDateObjIntoString();
        try {
            let url: string = this.ics.cmsurl + 'ServiceReportAdm/getSettlementReport?download=';
            let json: any = this._obj;
            this._obj.sessionID = this.ics._profile.sessionID;
            this._obj.userID = this.ics._profile.userID;
          this.http.doPost(url, json).subscribe(
            data => {
              this._mflag = true;
                this._list = data;
                if (data.wldata.length != 0) {
                  if (!(data.wldata instanceof Array)) {
                    let m = [];
                    m[0] = data.wldata;
                    data.wldata = m;
                  }
                  this._list.wldata = data.wldata;
                  this.tableflag = true;          
                }else{
                  this.showMsg("No Record Found",false);
                  this.tableflag = false;
                }
            },
            error => {
              this.tableflag = true;
              this._mflag = true;
              this.showMsg("HTTP Error Type ", false);
            },
            () => {
              this._mflag = true;
            }
          );
        } catch (e) {
            alert("Invalid URL.");
        }
    }
      
    goPrint() {
        this._mflag = false;
        this.setDateObjIntoString();
        try {
            let url: string = this.ics.cmsurl + 'ServiceReportAdm/getSettlementReport?download=1';
            let json: any = this._obj;
            this._obj.sessionID = this.ics._profile.sessionID;
            this._obj.userID = this.ics._profile.userID;
            this.http.doPost(url, json).subscribe(
                data => {
                    this._mflag = true;
                    if (data.msgCode = "0016") {
                        this.showMsg(data.msgDesc, false);
                    }
                    if (data.state) {
                        let url2: string = this.ics.cmsurl + 'ServiceReportAdm/downloadReport?fileType=' + this._obj.fileType +
                            "&fileName=" + data.stringResult[0] + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
                        if (this._obj.fileType == "PDF") {
                            this._mflag = true;
                            jQuery("#downloadPdf").html("<iframe src=" + url2 + "></iframe>");
                            this.showMsg("Download Successfully", true);
                        }
                        if (this._obj.fileType == "EXCEL") {
                            this._mflag = true;
                            jQuery("#downloadExcel").html("<iframe src=" + url2 + "></iframe>");
                            this.showMsg("Download Successfully", true);
                        }
                    }
                },
                error => {
                    this._mflag = true;
                    this.showMsg("HTTP Error Type ", false);
                },
                () => {
                    this._mflag = true;
                }
            );
        } catch (e) {
            alert("Invalid URL.");
        }

    }
    goProcess() {
      this._mflag = false;
      this.setDateObjIntoString();
      try {
          let url: string = this.ics.cmsurl + 'ServiceReportAdm/gopostTransaction';
          let json: any = this._obj;
          this._obj.sessionID = this.ics._profile.sessionID;
          this._obj.userID = this.ics._profile.userID;
          this.http.doPost(url, json).subscribe(
            data => {
              this._mflag = true;
              if (data.msgCode == "0000") {
                this._list = data;
                if (data.wldata != null) {
                  if (!(data.wldata instanceof Array)) {
                    let m = [];
                    m[0] = data.wldata;
                    data.wldata = m;
                  }
                  this._list.wldata = data.wldata;
                  this.tableflag = true;  
                  if(data.state == true){ this.showMsg(data.msgDesc, true);  }
                  else {this.showMsg(data.msgDesc, false); } 
                }
              } else if (data.msgCode == "0016") {
                this.showMsg(data.msgDesc, false);
              }
            },
              error => {
                  this._mflag = true;
                  this.showMsg("HTTP Error Type ", false);
              },
              () => {
                  this._mflag = true;
              }
          );
      } catch (e) {
          alert("Invalid URL.");
      }

  }
    /* changeFileType(options){
        let value = options[options.selectedIndex].value;
        for(var i=0; i< this.ref._lov1.ref010.length; i++){
          if(this.ref._lov1.ref010[i].value = value)
          this._obj.fileType = value;
        }
      } */
    showMsg(msg, bool) {
        if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
        if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
        if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
    }
    goClear(){
      this._list = {"wldata": []};
      this.tableflag = false;
    }
}    