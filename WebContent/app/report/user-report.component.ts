import { Component, ElementRef, enableProdMode, Renderer } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { ActivatedRoute, Router } from '@angular/router';
import { IMyDpOptions } from 'mydatepicker';
import { Subscription } from 'rxjs/Subscription';
import { RpHttpService } from '../framework/rp-http.service';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpReferences } from '../framework/rp-references';
import { ClientUtil } from '../util/rp-client.util';
declare var jQuery: any;

enableProdMode();
@Component({
  selector: 'fmraccounttrans',
  template: ` 
  <div *ngIf="!_pdfdata">
  <div class="container-fluid">
  <div class="row clearfix">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
      <form class= "form-horizontal" ngNoForm>        
          <legend>User Report
            <button type="button" class="btn btn-sm btn-primary" style="cursor:pointer;text-align:center;margin-left: 930px;width:30px;height:34px; color:white ;font-size:14px" (click)="goPrint()" title="Download PDF File">
              <i  class="glyphicon glyphicon-save" style="color:white;margin-left:-4px;"></i>
            </button> 
            <button type="button" class="btn btn-sm btn-primary" style="cursor:pointer;text-align:center;margin-left: 10px;width:30px;height:34px; color:white ;font-size:20px" (click)="goPrintExcel()" title="Download Excel File">
              <i  class="fa fa-file-excel-o" style="color:white;margin-left:-4px;"></i>
            </button>
          <div style="float:right;text-align:right;">                                
          <div style="display:inline-block;padding-right:0px;width:280px;">
            <div class="input-group"> 
              <input class="form-control" type="text" id="isInputSearch" placeholder="Search" autocomplete="off" spellcheck="false" [(ngModel)]="_SearchString" [ngModelOptions]="{standalone: true}" (keyup.enter)="filterSearch()">

                <span id="btnSimpleSearch" class="input-group-addon">
                  <i class="glyphicon glyphicon-search" style="cursor: pointer;color:#2e8690" (click)="filterSearch()"></i>
                </span>
            </div>
          </div>

            <button id="btnToggleSearch" class="btn btn-sm btn-primary" type="button" style="cursor:pointer;text-align:center;margin-top:-24px;height:34px;" (click)="btnToggleSearch_onClick()" title="Collapse">
              <i id="lblToggleSearch" class="glyphicon glyphicon-menu-down"></i> 
            </button>

            <button *ngIf="false" id="btnTogglePagination" type="button" class="btn btn-sm btn-primary" style="cursor:pointer;text-align:center;margin-left:25px;margin-top:-24px;width:30px;height:34px;" (click)="btnTogglePagination_onClick()" title="Collapse">
              <i id="lblTogglePagination" class="glyphicon glyphicon-eject" style="color:#2e8690;margin-left:-4px;transform: rotate(90deg)"></i>
            </button>

           <!-- <div id="divPagination" style="display:none;margin-left:13px;margin-top:-7px;margin-bottom:5px;">
              <adminpager id="pgPagination" rpPageSizeMax="100" [(rpModel)]="_ListingDataset.totalCount" (rpChanged)="changedPager($event)" style="font-size:14px;text-align:right;margin-top:-7px;margin-right:10px;"></adminpager>
            </div> -->

            <button id="btnClose" type="button" class="btn btn-sm btn-primary" (click)="closePage()" (mouseenter)="btnClose_MouseEnter()" (mouseleave)="btnClose_MouseLeave()" title="Close" style="cursor:pointer;height:34px;text-align:center;margin-top:-24px;">
              <i id="lblClose" class="glyphicon glyphicon-remove"></i> 
            </button>                     
        </div>
          </legend>
  				<div class="cardview list-height"> 
  				  <!--<div class="row col-md-12">  
            <button class="btn btn-sm btn-primary" type="button" (click)="goClear()">Clear</button> 
            <button class="btn btn-sm btn-primary" type="button" (click)="goList()">Show</button>             
  				  <button class="btn btn-sm btn-primary" type="button" (click)="goDownload()">Download</button>          
           </div> -->
           <advanced-search id="asAdvancedSearch" [FilterList]="_FilterList" [TypeList]="_TypeList" rpVisibleItems=5 (rpHidden)="renderAdvancedSearch($event)" (rpChanged)="filterAdvancedSearch($event)" style="display:none;"></advanced-search>
           <!--<div class="row col-md-12">&nbsp;</div> -->
          <div class="form-group"> 
            <div class="col-md-12">
  						<!--<div class="form-group">            
  							<label class="col-md-2">From Date</label>								
  							  <div class="col-md-3">
  								  <my-date-picker name="mydate"  [options]="myDatePickerOptions" [(ngModel)]="_dates.fromDate" ngDefaultControl></my-date-picker>                  
  							  </div>
  							<label class="col-md-2">To Date</label>
  							  <div class="col-md-3">
  								  <my-date-picker name="mydate" [options]="myDatePickerOptions" [(ngModel)]="_dates.toDate" ngDefaultControl></my-date-picker>                           
  							  </div>								
  							<div class="col-md-2">
  							<label>
  							<input type='checkbox' [(ngModel)]="_obj.alldate" [ngModelOptions]="{standalone: true}" (click)='getAllDate($event)'>
  							ALL
  							</label>
  							</div>
  						</div>
  						<div class="form-group">
  						  <label class="col-md-2" >User Type</label>
  						  <div class="col-md-3">
  							<select [(ngModel)]="_obj.usertype" [ngModelOptions]="{standalone: true}"  (change)="changeUserType($event)"  required="true"   class="form-control col-md-0"  >
  							   <option *ngFor="let item of ref._lov3.refUserType" value="{{item.value}}" >{{item.caption}}</option> 
  							</select>                     
  						  </div>
  						  <label class="col-md-2" >Status</label>
  							  <div class="col-md-3">
  								  <select [(ngModel)]="_obj.status" [ngModelOptions]="{standalone: true}"  (change)="changeStatus($event)"  required="true"   class="form-control col-md-0"  >
  									<option *ngFor="let item of ref._lov1.ref021" value="{{item.value}}" >{{item.caption}}</option> 
  								  </select>                      
  							  </div>
  						</div>
  						<div class="form-group">
  						  <label class="col-md-2" > User Name </label>
  							  <div class="col-md-3">
  								  <input type="text"  class="form-control" [(ngModel)]="_obj.username" [ngModelOptions]="{standalone: true}" />
  							  </div>
  							  <label class="col-md-2" > NRC</label>
  							<div class="col-md-3">
  								<input type="text"  class="form-control" [(ngModel)]="_obj.nrc" [ngModelOptions]="{standalone: true}" />
  							</div>
  						</div>
  						<div class="form-group">
  							<label class="col-md-2" >Phone No.</label>
  							<div class="col-md-3">
  								<input type="text"  class="form-control" [(ngModel)]="_obj.phoneno" [ngModelOptions]="{standalone: true}" />
                </div>
                <rp-input  [(rpModel)]="_obj.fileType" rpRequired ="true" rpLabelClass = "col-md-2" rpClass="col-md-3" rpType="ref010" rpLabel="File Type" (ngModelChange)="changeFileType($event)"></rp-input>
              </div>  -->
            
            <div [hidden]="_entryhide">
              <div style = "margin-top : 10px">
              <adminpager rpPageSizeMax="100" [(rpModel)]="_list.totalCount" (rpChanged)="changedPager($event)" ></adminpager>
              </div>
              <button id="btnClose" type="button" class="btn btn-sm btn-primary" (click)="closeList()" (mouseenter)="btnClose_MouseEnter()" (mouseleave)="btnClose_MouseLeave()" title="Close" style="cursor:pointer;height:34px;text-align:center;">
                <i id="lblClose" class="glyphicon glyphicon-remove"></i> 
              </button>
               <div class="row col-md-12" style="overflow-x:auto;">
                 <table class="table table-striped table-condensed table-hover tblborder" id="tblstyle">
                   <thead >
                    <tr>          
                      <th class="right" width="5%" style="background-color: #e6f2ff; color: #333333">No</th>
                      <th class="left" width="10%" style="background-color: #e6f2ff; color: #333333">User ID</th>
                      <th width="10%" style="background-color: #e6f2ff; color: #333333"> User Name</th>
                      <th width="10%" style="background-color: #e6f2ff; color: #333333">NRC</th>
                      <th width="10%" style="background-color: #e6f2ff; color: #333333"> Phone No</th>
                      <th width="10%" style="background-color: #e6f2ff; color: #333333">Created Date</th>
                      <th width="10%" style="background-color: #e6f2ff; color: #333333"> Modified </th>
                      <th width="10%" style="background-color: #e6f2ff; color: #333333">User Status</th>
                    </tr>
                   </thead>
                   <tbody>
                     <tr *ngFor="let obj of _list.data" >
                      <td class="right">{{obj.syskey}}</td>
                      <td >{{obj.userID}}</td> 
                      <td class="left">{{obj.username}}</td> 
                      <td class="left">{{obj.nrc}}</td> 
                      <td class="left">{{obj.phoneno}}</td> 
                      <td>{{obj.createddate}}</td> 
                      <td>{{obj.modifieddate}}</td> 
                      <td class="left">{{obj.userstatus}}</td>
                      </tr>  
                   </tbody>
                  </table>
               </div>
           </div>
          </div>
          </div>
          </div>
      </form>
      </div>
      <div>
      <div id="downloadPdf" style="display: none;width: 0px;height: 0px;"></div>
      <div id="downloadExcel" style="display: none;width: 0px;height: 0px;"></div>
   </div>
  </div>
</div>
</div>     
    <div [hidden] = "_mflag">
      <div class="modal" id="loader"></div>
   </div> 																									
  `,
})
export class UserReport {
  subscription: Subscription;
  _util: ClientUtil = new ClientUtil();

  today = this._util.getTodayDate();
  myDatePickerOptions: IMyDpOptions = this._util.getDatePicker();
  dateobj: { date: { year: number, month: number, day: number } };
  _dates = { "fromDate": this.dateobj, "toDate": this.dateobj };
  _printUrl: SafeResourceUrl;
  _entryhide = true;
  _formhide: boolean;
  _pdfdata: boolean;
  _mflag = true;
  _sessionObj = this.getSessionObj();
//ep
  _FilterList: any;
  _TypeList: any;
  _toggleSearch = true;
  _SearchString = "";
  i = 0;
  file_type = "";
  _OperationMode = "";
  getSessionObj() {
    return { "sessionID": "", "userID": "" };
  }
  _obj = this.getDefaultObj();
  getDefaultObj() {
    return {
      "sessionID": "", "msgCode": "", "msgDesc": "", "alldate": true, "fileType": "EXCEL",
      "aFromDate": "", "aToDate": "", "select": "EXCEL", "totalCount": 0, "currentPage": 1, "pageSize": 10, "msgstatus": "",
      "username": "", "nrc": "", "accountNo": "", "phoneno": "", "usertype": "", "status": "all", "branchCode": "", "userID": "", "click": 0,"condition": "","simpleSearch" :""
    };
  }
  _pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 1 };
  walletuserArr = [["all", "All"], ["save", "Save"], ["delete", "Delete"]];
  consoleuserArr = [["all", "All"], ["save", "Save"], ["activate", "Activate"], ["deactivate", "Deactivate"], ["lock", "Lock"], ["delete", "Delete"]];
  _list = {
    "data": [{
      "cif": "", "createdby": "", "createddate": "", "modifiedby": "", "modifieddate": "",
      "n1": 0, "n7": 0,
      "nrc": "", "phoneno": "", "recordStatus": 0, "syskey": 0, "userID": "", "username": "", "userstatus": ""
    }],
    "aFromDate": "", "aToDate": "", "alldate": "", "branchCode": "", "click": "", "fileType": "", "msgCode": "", "msgDesc": "", "msgstatus": "", "nrc": "", "phoneno": "", "select": "", "sessionID": "", "status": "", "totalCount": 0
    , "currentPage": 1, "pageSize": 10
  };
  _FilterDataset =
    {
      filterList:
        [
          { "itemid": "", "caption": "", "fieldname": "", "datatype": "", "condition": "", "t1": "", "t2": "" }
        ],

      "pageNo": 1, "pageSize": 10, "filterSource": 0, "sessionID": "", "userID": "", "userName": this.ics._profile.userName
    };
  constructor(private el: ElementRef,private l_util: ClientUtil, private renderer: Renderer, private ics: RpIntercomService, private _router: Router, private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences, private sanitizer: DomSanitizer) {
    if (!ics.getRole() || ics.getRole() == 0) this._router.navigate(['/login']);
    this.setTodayDateObj();
    this.setDateObjIntoString();
    this.getUserType();
    this._pdfdata = false;
  }

  ngOnInit() {
    //this.filterSearch();
    this.loadAdvancedSearchData();
  }
  filterSearch() {
    
    if (this._toggleSearch) this.filterCommonSearch();
    else this.ics.send001("FILTER");
  }

  filterCommonSearch() {
    var l_DateRange;
    var l_SearchString = "";

    this._FilterDataset.filterList = [];
    this._FilterDataset.filterSource = 0;

    if (jQuery.trim(this._SearchString) != "") {
      l_SearchString = jQuery.trim(this._SearchString);


      this._FilterDataset.filterList =
        [
          { "itemid": "1", "caption": "Search String", "fieldname": "SearchString", "datatype": "simplesearch", "condition": "c", "t1": l_SearchString, "t2": "" }
        ];
    }

    //  l_DateRange = { "itemid": "4", "caption": "Date Range", "fieldname": "DateRange", "datatype": "date", "condition": "eq", "t1": this.l_util.getTodayDate(), "t2": "" };
    //   this._FilterDataset.filterList.push(l_DateRange);

    if (this._OperationMode != "prepareFilter") {
      if (this._FilterDataset.pageNo == 1) this.goList();
      else this._FilterDataset.pageNo = 1;
    }
  }
  setTodayDateObj() {
    this._dates.fromDate = this._util.changestringtodateobject(this.today);
    this._dates.toDate = this._util.changestringtodateobject(this.today);
  }
  setDateObjIntoString() {
    this._obj.aFromDate = this._util.getDatePickerDateymd(this._dates.fromDate);
    this._obj.aToDate = this._util.getDatePickerDateymd(this._dates.toDate);
  }
  getAllDate(event) {
    this._entryhide = true;
    this._list.currentPage = 1;
    this._list.totalCount = 0;
    this._obj.alldate = event.target.checked;
  }

  loadAdvancedSearchData() {
    this._TypeList =
    {
      "lovStatus": [],
      "lovFileType":
        [
          { "value": "1", "caption": "EXCEL" },
          { "value": "2", "caption": "PDF" }
        ],
        "lovUserType":
        [
          { "value": "1", "caption": "Console User" },
          { "value": "2", "caption": "Wallet User" },
          
        ],
        "lovStatusType":
        [
          { "value": "all", "caption": "All"},
          { "value": "save", "caption": "Save"},
          { "value": "delete", "caption": "Delete"}
        ]
      //    "merchant": []
    };
    //this.loadStatus();  
    //this.getAllMerchant();
    //this.loadMerchantType();


    this._FilterList =
      [
        { "itemid": "1", "caption": "Search String", "fieldname": "SearchString", "datatype": "simplesearch", "condition": "c", "t1": "", "t2": "", "t3": "" },
        { "itemid": "2", "caption": "Date", "fieldname": "FromDate", "datatype": "date", "condition": "bt", "t1": this.l_util.getTodayDate(), "t2": this.l_util.getTodayDate(), "t3": "true" },

        { "itemid": "3", "caption": "User Type", "fieldname": "UserType", "datatype": "lovUserType", "condition": "", "t1": "", "t2": "", "t3": "" },
        { "itemid": "4", "caption": "Status", "fieldname": "Status", "datatype": "lovStatusType", "condition": "c", "t1": "", "t2": "", "t3": "" },
        { "itemid": "5", "caption": "User Name", "fieldname": "UserName", "datatype": "simplesearch", "condition": "c", "t1": "", "t2": "", "t3": "" },
        { "itemid": "6", "caption": "NRC", "fieldname": "NRC", "datatype": "simplesearch", "condition": "c", "t1": "", "t2": "", "t3": "true" },

        { "itemid": "7", "caption": "Phone No.", "fieldname": "PhoneNo", "datatype": "simplesearch", "condition": "", "t1": "", "t2": "", "t3": "" },
       // { "itemid": "8", "caption": "File Type", "fieldname": "FileType", "datatype": "lovFileType", "condition": "c", "t1": "", "t2": "", "t3": "" }
        //{ "itemid": "4", "caption": "Date Range", "fieldname": "DateRange", "datatype": "date", "condition": "bt", "t1": this.l_util.getTodayDate(), "t2": this.l_util.getTodayDate(), "t3": "true" },
      ];
  }
  closePage() {
    this._router.navigate(['000', { cmd: "READ", p1: this.ics._profile.userID }]);
  }
  closeList() {
    this.goClear();
  }
  btnToggleSearch_onClick() {
    this.toggleSearch(!this._toggleSearch);
  }
  toggleSearch(aToggleSearch) {
    // Set Flag
    this._toggleSearch = aToggleSearch;

    // Clear Simple Search
    if (!this._toggleSearch) this._SearchString = '';

    // Enable/Disabled Simple Search Textbox
    jQuery('#isInputSearch').prop('disabled', !this._toggleSearch);

    // Hide Advanced Search
    if (this._toggleSearch) {
      this.ics.send001("CLEAR");
      //this._FilterDataset = null;
      this._obj = this.getDefaultObj();
    }

    // Show/Hide Advanced Search    
    //  
    if (this._toggleSearch) jQuery("#asAdvancedSearch").css("display", "none");                                                                             // true     - Simple Search
    else {
      jQuery("#asAdvancedSearch").css("display", "inline-block");                                                                     // false    - Advanced Search
      this._obj = this.getDefaultObj();
    }
    // Set Icon 
    //  
    if (this._toggleSearch) jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-up").addClass("glyphicon glyphicon-menu-down");                // true     - Simple Search
    else jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-down").addClass("glyphicon glyphicon-menu-up");                // false    - Advanced Search

    // Set Tooltip
    //
    let l_Tooltip = (this._toggleSearch) ? "Expand" : "Collapse";
    jQuery('#btnToggleSearch').attr('title', l_Tooltip);
  }

  renderAdvancedSearch(event) {
    this.toggleSearch(!event);
  }
  filterAdvancedSearch(event) {
    this._FilterDataset.filterSource = 1;
    this._FilterDataset.filterList = [];
    this._FilterDataset.filterList = event;

    if (this._OperationMode != "prepareFilter") {
      if (this._FilterDataset.pageNo == 1) this.goList();
      else
        this._FilterDataset.pageNo = 1;
    }
  }


  goPrint() {
    let l_Data: any = [];
    l_Data = this._FilterDataset;
    this._mflag = false;
    this.setDateObjIntoString();
    if (this._obj.aFromDate > this._obj.aToDate) {
      this._mflag = true;
      this.showMsg("From Date Should Not Exceed To Date.", false);
    } /* if (!/^(([1-9]|1[0-9])\/[A-Za-z]+\([N|n]\)[0-9]{6})$/.test(this._obj.nrc)) {
      this.showMsg("Invalid NRC No.", false);
    } */else {
      if (l_Data != null) {

        for (this.i = 0; this.i < l_Data.filterList.length; this.i++) {

          if (l_Data.filterList[this.i].itemid == "1") {
            this._obj.simpleSearch = l_Data.filterList[this.i].t1;
          }
          else if (l_Data.filterList[this.i].itemid == "2") {
            this._obj.aFromDate = l_Data.filterList[this.i].t1;
            this._obj.aToDate = l_Data.filterList[this.i].t2;
            this._obj.condition = l_Data.filterList[this.i].condition;
          }
          else if (l_Data.filterList[this.i].itemid == "3") {
            this._obj.usertype = l_Data.filterList[this.i].t1;
          }
          else if (l_Data.filterList[this.i].itemid == "4") {
            this._obj.status = l_Data.filterList[this.i].t1;
          }
          else if (l_Data.filterList[this.i].itemid == "5") {
            this._obj.username = l_Data.filterList[this.i].t1;
          }
          else if (l_Data.filterList[this.i].itemid == "6") {
            this._obj.nrc = l_Data.filterList[this.i].t1;
          }
          else if (l_Data.filterList[this.i].itemid == "7") {
            this._obj.phoneno = l_Data.filterList[this.i].t1;
          }
          /* else if (l_Data.filterList[this.i].itemid == "8") {

            this.file_type = l_Data.filterList[this.i].t1;
            if (this.file_type == "1") {
              this._obj.fileType = "EXCEL";
            }
            else
              this._obj.fileType = "PDF";
          } */


        }


      }
      try {
        this._obj.fileType = "PDF";
        let url: string = this.ics.cmsurl + 'ServiceReportAdm/getUserReportList1?download=1';
        let json: any = this._obj;
        this._obj.sessionID = this.ics._profile.sessionID;
        this._obj.userID = this.ics._profile.userID;
        this.http.doPost(url, json).subscribe(
          data => {
            this._mflag = true;
            if (data.msgCode = "0016") {
              this.showMsg(data.msgDesc, false);
            }
            if (data.state) {
              let url2: string = this.ics.cmsurl + 'ServiceReportAdm/downloadReport?fileType=' + this._obj.fileType +
                '&fileName=' + data.stringResult[0] + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
              if (this._obj.fileType == "PDF") {
                this._mflag = true;
                jQuery("#downloadPdf").html("<iframe src=" + url2 + "></iframe>");
                this.showMsg("Download Successfully", true);
              }
              if (this._obj.fileType == "EXCEL") {
                this._mflag = true;
                jQuery("#downloadExcel").html("<iframe src=" + url2 + "></iframe>");
                this.showMsg("Download Successfully", true);
              }
            }
          },
          error => {
            this._mflag = true;
            this.showMsg("HTTP Error Type ", false);
          },
          () => {
            this._mflag = true;
          }
        );
      } catch (e) {
        alert("Invalid URL.");
      }
    }
  }

  goPrintExcel() {
    let l_Data: any = [];
    l_Data = this._FilterDataset;
    this._mflag = false;
    this.setDateObjIntoString();
    if (this._obj.aFromDate > this._obj.aToDate) {
      this._mflag = true;
      this.showMsg("From Date Should Not Exceed To Date.", false);
    } /* if (!/^(([1-9]|1[0-9])\/[A-Za-z]+\([N|n]\)[0-9]{6})$/.test(this._obj.nrc)) {
      this.showMsg("Invalid NRC No.", false);
    } */else {
      if (l_Data != null) {

        for (this.i = 0; this.i < l_Data.filterList.length; this.i++) {

          if (l_Data.filterList[this.i].itemid == "1") {
            this._obj.simpleSearch = l_Data.filterList[this.i].t1;
          }
          else if (l_Data.filterList[this.i].itemid == "2") {
            this._obj.aFromDate = l_Data.filterList[this.i].t1;
            this._obj.aToDate = l_Data.filterList[this.i].t2;
            this._obj.condition = l_Data.filterList[this.i].condition;
          }
          else if (l_Data.filterList[this.i].itemid == "3") {
            this._obj.usertype = l_Data.filterList[this.i].t1;
          }
          else if (l_Data.filterList[this.i].itemid == "4") {
            this._obj.status = l_Data.filterList[this.i].t1;
          }
          else if (l_Data.filterList[this.i].itemid == "5") {
            this._obj.username = l_Data.filterList[this.i].t1;
          }
          else if (l_Data.filterList[this.i].itemid == "6") {
            this._obj.nrc = l_Data.filterList[this.i].t1;
          }
          else if (l_Data.filterList[this.i].itemid == "7") {
            this._obj.phoneno = l_Data.filterList[this.i].t1;
          }
          /* else if (l_Data.filterList[this.i].itemid == "8") {

            this.file_type = l_Data.filterList[this.i].t1;
            if (this.file_type == "1") {
              this._obj.fileType = "EXCEL";
            }
            else
              this._obj.fileType = "PDF";
          } */


        }


      }
      try {
        this._obj.fileType = "EXCEL";
        let url: string = this.ics.cmsurl + 'ServiceReportAdm/getUserReportList1?download=1';
        let json: any = this._obj;
        this._obj.sessionID = this.ics._profile.sessionID;
        this._obj.userID = this.ics._profile.userID;
        this.http.doPost(url, json).subscribe(
          data => {
            this._mflag = true;
            if (data.msgCode = "0016") {
              this.showMsg(data.msgDesc, false);
            }
            if (data.state) {
              let url2: string = this.ics.cmsurl + 'ServiceReportAdm/downloadReport?fileType=' + this._obj.fileType +
                '&fileName=' + data.stringResult[0] + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
              if (this._obj.fileType == "PDF") {
                this._mflag = true;
                jQuery("#downloadPdf").html("<iframe src=" + url2 + "></iframe>");
                this.showMsg("Download Successfully", true);
              }
              if (this._obj.fileType == "EXCEL") {
                this._mflag = true;
                jQuery("#downloadExcel").html("<iframe src=" + url2 + "></iframe>");
                this.showMsg("Download Successfully", true);
              }
            }
          },
          error => {
            this._mflag = true;
            this.showMsg("HTTP Error Type ", false);
          },
          () => {
            this._mflag = true;
          }
        );
      } catch (e) {
        alert("Invalid URL.");
      }
    }
  }
  goClear() {
    this._list.currentPage = 1;
    this._list.totalCount = 0;
    this._list.data = [];
    this._entryhide = true;
    this._obj = this.getDefaultObj();
    this.setTodayDateObj();
    this._obj.usertype = this.ref._lov3.refUserType[0].value;
    let combo = [];
    if (this._obj.usertype == "1") {
      for (let j = 0; j < 3; j++) {
        let l = 0;
        combo.push({ "value": this.walletuserArr[j][l++], "caption": this.walletuserArr[j][l++] })
      }
      this.ref._lov1.ref021 = combo;
    }
    else if (this._obj.usertype == "2") {
      for (let j = 0; j < 6; j++) {
        let l = 0;
        combo.push({ "value": this.consoleuserArr[j][l++], "caption": this.consoleuserArr[j][l++] })
      }
      this.ref._lov1.ref021 = combo;
    }
  }
  changeStatus(event) {
    let options = event.target.options;
    let k = options.selectedIndex;//Get Selected Index
    let value = options[options.selectedIndex].value;//Get Selected Index's Value 
    if (value == "ALL" || value == "") {
      this._obj.status = "ALL";
    } else {
      this._obj.status = value;
      for (var i = 1; i < this.ref._lov1.ref021.length; i++) {
        if (this.ref._lov1.ref021[i].value == value) {
          this._obj.status = this.ref._lov1.ref021[i].value;
        }
      }
    }
  }

  changeUserType(event) {
    this._entryhide = true;
    this._list.currentPage = 1;
    this._list.pageSize = 10;
    this._list.totalCount = 0;
    let options = event.target.options;
    let k = options.selectedIndex;
    let value = options[options.selectedIndex].value;
    let combo = [];
    if (value == 1) {
      for (let j = 0; j < 3; j++) {
        let l = 0;
        combo.push({ "value": this.walletuserArr[j][l++], "caption": this.walletuserArr[j][l++] })
      }
      this.ref._lov1.ref021 = combo;
    }
    else if (value == 2) {
      for (let j = 0; j < 6; j++) {
        let l = 0;
        combo.push({ "value": this.consoleuserArr[j][l++], "caption": this.consoleuserArr[j][l++] })
      }
      this.ref._lov1.ref021 = combo;
    }
  }

  changeFileType(options) {
    let value = options[options.selectedIndex].value;
    for (let i = 0; i < this.ref._lov1.ref010.length; i++) {
      if (this.ref._lov1.ref010[i].value = value) {
        this._obj.fileType = value;
      }
    }
  }
  getUserType() {
    try {
      let url: string = this.ics.cmsurl + 'serviceAdmLOV/getUserType';

      this._sessionObj.sessionID = this.ics._profile.sessionID;
      this._sessionObj.userID = this.ics._profile.userID;
      let json: any = this._sessionObj;

      this.http.doPost(url, json).subscribe(
        data => {
          if (data != null) {
            if (data.msgCode == "0016") {
              this.showMsg(data.msgDesc, false);
            }
            if (data.msgCode == "0000") {
              if (!(data.refUserType instanceof Array)) {
                let m = [];
                m[0] = data.refUserType;
                this.ref._lov3.refUserType = m;
              } else {
                this.ref._lov3.refUserType = data.refUserType;
              }
              this._obj.usertype = this.ref._lov3.refUserType[0].value;

              let combo = [];
              if (this._obj.usertype == "1") {
                for (let j = 0; j < 3; j++) {
                  let l = 0;
                  combo.push({ "value": this.walletuserArr[j][l++], "caption": this.walletuserArr[j][l++] })
                }
                this.ref._lov1.ref021 = combo;
              }
              else if (this._obj.usertype == "2") {
                for (let j = 0; j < 6; j++) {
                  let l = 0;
                  combo.push({ "value": this.consoleuserArr[j][l++], "caption": this.consoleuserArr[j][l++] })
                }
                this.ref._lov1.ref021 = combo;
              }
            }
          }
        },
        error => {
          if (error._body.type == "error") {
            alert("Connection Timed Out.");
          }
        },
        () => { }
      );
    } catch (e) {
      alert("Invalid URL.");
    }
  }

  showMsg(msg, bool) {
    if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
    if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
    if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
  }
  logout() {
    this._router.navigate(['/login']);
  }


  goList() {
   // let l_Data: any = [];
    this._FilterDataset.sessionID = this.ics._profile.sessionID;
    this._FilterDataset.userID = this.ics._profile.userID;
    let l_Data: any = this._FilterDataset;

    l_Data = this._FilterDataset;
    this._mflag = false;
    this.setDateObjIntoString();
    if (this._obj.aFromDate > this._obj.aToDate) {
      this._mflag = true;
      this.showMsg("From Date Should Not Exceed To Date.", false);
    }
    else {
      if (l_Data != null) {

        for (this.i = 0; this.i < l_Data.filterList.length; this.i++) {

          if (l_Data.filterList[this.i].itemid == "1") {
            this._obj.simpleSearch = l_Data.filterList[this.i].t1;
          }
          else if (l_Data.filterList[this.i].itemid == "2") {
            this._obj.aFromDate = l_Data.filterList[this.i].t1;
            this._obj.aToDate = l_Data.filterList[this.i].t2;
            this._obj.condition = l_Data.filterList[this.i].condition;
          }
          else if (l_Data.filterList[this.i].itemid == "3") {
            this._obj.usertype = l_Data.filterList[this.i].t1;
          }
          else if (l_Data.filterList[this.i].itemid == "4") {
            this._obj.status = l_Data.filterList[this.i].t1;
          }
          else if (l_Data.filterList[this.i].itemid == "5") {
            this._obj.username = l_Data.filterList[this.i].t1;
          }
          else if (l_Data.filterList[this.i].itemid == "6") {
            this._obj.nrc = l_Data.filterList[this.i].t1;
          }
          else if (l_Data.filterList[this.i].itemid == "7") {
            this._obj.phoneno = l_Data.filterList[this.i].t1;
          }
          else if (l_Data.filterList[this.i].itemid == "8") {

            this.file_type = l_Data.filterList[this.i].t1;
            if (this.file_type == "1") {
              this._obj.fileType = "EXCEL";
            }
            else
              this._obj.fileType = "PDF";
          }


        }


      }
      let url: string = this.ics.cmsurl + 'ServiceReportAdm/getUserReportList1?download=0';
      let json: any = this._obj;
      this._obj.sessionID = this.ics._profile.sessionID;
      this._obj.userID = this.ics._profile.userID;
      this.http.doPost(url, json).subscribe(
        data => {
          this._mflag = true;

          if (data != null) {
            this._obj.totalCount = data.totalCount;
            if (this._obj.totalCount == 0) {
              this.showMsg("Data not Found!", false);
              this._entryhide = true;
            }
            else {
              this._obj.currentPage = data.currentPage;
              this._obj.pageSize = data.pageSize;
              this._list = data;
              if (data.usdata != null) {
                if (!(data.usdata instanceof Array)) {
                  let m = [];
                  m[0] = data.usdata;
                  data.usdata = m;
                }
                this._list.data = data.usdata;
                if (this._obj.totalCount != 0) this._entryhide = false;
              }
            }
          }
        }
      )
    }

  }
  changedPager(event) {
    this._pgobj = event;
    this._obj.currentPage = this._pgobj.current;
    this._obj.pageSize = this._pgobj.size;
    this._obj.totalCount = this._pgobj.totalcount;
    if (this._pgobj.totalcount > 1) {
      //this._obj.click = 1;
      this.goList();
    }
  }
  /* 
  goClose() {
    this._pdfdata = false;
    this.setTodayDateObj();
    this._obj = this.getDefaultObj();
    this._obj.usertype = this.ref._lov3.refUserType[0].value;    
    let combo = [];
    if (this._obj.usertype == "1") {
      for (let j = 0; j < 3; j++) {
        let l = 0;
        combo.push({ "value": this.walletuserArr[j][l++], "caption": this.walletuserArr[j][l++] })

      }
      this.ref._lov1.ref021 = combo;
    }
    else if (this._obj.usertype == "2") {
      for (let j = 0; j < 6; j++) {
        let l = 0;
        combo.push({ "value": this.consoleuserArr[j][l++], "caption": this.consoleuserArr[j][l++] })

      }
      this.ref._lov1.ref021 = combo;
    }
  } */
}
