import { Component, ElementRef, enableProdMode, Renderer } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { ActivatedRoute, Router } from '@angular/router';
import { IMyDpOptions } from 'mydatepicker';
import { Subscription } from 'rxjs/Subscription';
import { RpHttpService } from '../framework/rp-http.service';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpReferences } from '../framework/rp-references';
import { ClientUtil } from '../util/rp-client.util';
declare var jQuery: any;

enableProdMode();
@Component({
  selector: 'wallettopup-report',
  template: ` 
  <div *ngIf="!_divexport">  
    <div class="container-fluid">
      <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
		      <form class="form-horizontal" ngNoForm>
            <legend>Merchant Transfer Report &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <!-- <i class="fa fa-file-excel-o"  (click)="goPrintExcel()></i>  -->
            <!--<button type="button"  class="fa fa-file-excel-o"  (click)="goPrintExcel()" title="Download Excel File"></button>   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->
            <!-- <button type="button" class="glyphicon glyphicon-save" (click)="goPrint()" title="Download PDF File" style="background-color: #3b5998!important;color:white"></button> -->
    
              

              <!--<i  class="glyphicon glyphicon-file" style="margin-left:-4px;"></i> -->
              <!--<i  class="glyphicon-glyphicon-download-alt" style="margin-left:-4px;"></i> -->
              <!-- <i class="glyphicon-glyphicon-export" style="margin-left:-4px;"></i> -->
              <button type="button" class="btn btn-sm btn-primary" style="cursor:pointer;text-align:center;margin-left: 750px;width:30px;height:34px; color:white ;font-size:14px" (click)="goPrint()" title="Download PDF File">
                    <i  class="glyphicon glyphicon-save" style="color:white;margin-left:-4px;"></i>
              </button> 
              <button type="button" class="btn btn-sm btn-primary" style="cursor:pointer;text-align:center;margin-left: 9px;width:30px;height:34px; color:white ;font-size:20px" (click)="goPrintExcel()" title="Download Excel File">
                    <i  class="fa fa-file-excel-o" style="color:white;margin-left:-4px;"></i>
              </button>
             
              <div style="float:right;text-align:right;">                                
                <div style="display:inline-block;padding-right:0px;width:280px;">
                  <div class="input-group"> 
                    <input class="form-control" type="text" id="isInputSearch" placeholder="Search" autocomplete="off" spellcheck="false" [(ngModel)]="_SearchString" [ngModelOptions]="{standalone: true}" (keyup.enter)="filterSearch()">

                      <span id="btnSimpleSearch" class="input-group-addon">
                        <i class="glyphicon glyphicon-search" style="cursor: pointer;color:#2e8690" (click)="filterSearch()"></i>
                      </span>
                  </div>
                </div>

                  <button id="btnToggleSearch" class="btn btn-sm btn-primary" type="button" style="cursor:pointer;text-align:center;margin-top:-24px;height:34px;" (click)="btnToggleSearch_onClick()" title="Collapse">
                    <i id="lblToggleSearch" class="glyphicon glyphicon-menu-down"></i> 
                  </button>

                  <button *ngIf="false" id="btnTogglePagination" type="button" class="btn btn-sm btn-primary" style="cursor:pointer;text-align:center;margin-left:25px;margin-top:-24px;width:30px;height:34px;" (click)="btnTogglePagination_onClick()" title="Collapse">
                    <i id="lblTogglePagination" class="glyphicon glyphicon-eject" style="color:#2e8690;margin-left:-4px;transform: rotate(90deg)"></i>
                  </button>

                 <!-- <div id="divPagination" style="display:none;margin-left:13px;margin-top:-7px;margin-bottom:5px;">
                    <adminpager id="pgPagination" rpPageSizeMax="100" [(rpModel)]="_ListingDataset.totalCount" (rpChanged)="changedPager($event)" style="font-size:14px;text-align:right;margin-top:-7px;margin-right:10px;"></adminpager>
                  </div> -->

                  <button id="btnClose" type="button" class="btn btn-sm btn-primary" (click)="closePage()" (mouseenter)="btnClose_MouseEnter()" (mouseleave)="btnClose_MouseLeave()" title="Close" style="cursor:pointer;height:34px;text-align:center;margin-top:-24px;">
                    <i id="lblClose" class="glyphicon glyphicon-remove"></i> 
                  </button>                     
              </div>
            </legend>
				  <div class="cardview list-height"> 
					  <div class="row col-md-12">
             <!-- <button class="btn btn-sm btn-primary" type="button" (click)="goClear()">Clear</button> 
              <button class="btn btn-sm btn-primary" type="button" (click)="goList()">Show</button>              
				      <button class="btn btn-sm btn-primary" type="button" (click)="goDownload()">Download</button> -->
            </div>
            <advanced-search id="asAdvancedSearch" [FilterList]="_FilterList" [TypeList]="_TypeList" rpVisibleItems=5 (rpHidden)="renderAdvancedSearch($event)" (rpChanged)="filterAdvancedSearch($event)" style="display:none;"></advanced-search>
          
           <!-- <div class="row col-md-12">&nbsp;</div> -->
              <div class="form-group">
                <div class="col-md-12">
                
                  <div [hidden]="_entryhide">
                    <div class="form-group">
                      <div class="col-md-8">
                        <div style="margin-top: 10px;">  
                          <adminpager rpPageSizeMax="100" [(rpModel)]="_list.totalCount" (rpChanged)="changedPager($event)"></adminpager>
                        </div>
                        <button id="btnClose" type="button" class="btn btn-sm btn-primary" (click)="closeList()" (mouseenter)="btnClose_MouseEnter()" (mouseleave)="btnClose_MouseLeave()" title="Close" style="cursor:pointer;height:34px;text-align:center;">
                            <i id="lblClose" class="glyphicon glyphicon-remove"></i> 
                        </button>
                      </div>

                      <div class="col-md-4" style="margin-top: 30px;overflow-x:auto;">      
                        <table style="align: right; font-size: 14px;float:right;" cellpadding="10px" cellspacing="10px">
                          <tbody>
                            <colgroup>
                             <col span="1" style="width: 50%;">
                             <col span="1" style="width: 50%;">                                  
                            </colgroup>                           
                            <tr>
                              <td style="color: blue;" class="left">
                                <p><b>Grand Total Amount </b></p>
                              </td>
                              <td style="color: blue;" class="center"><p>&nbsp;&nbsp;:&nbsp;&nbsp;</p></td>
                              <td style="color: blue;" class="right">
                              <p><b>{{formatNumber(this.total_Amount)}}</b></p>
                              </td>
                            </tr>
                            <tr>
                              <td style="color: blue;" class="left">
                                <p><b>Grand Total Bank Charges</b></p>
                              </td>
                              <td style="color: blue;" class="center"><p>&nbsp;&nbsp;:&nbsp;&nbsp;</p></td>
                              <td style="color: blue;" class="right">
                                <p><b>{{formatNumber(this.total_CommissionCharges)}}</b></p>           
                              </td>
                            </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
               <div class="row col-md-12" style="overflow-x:auto;">
                 <table class="table table-striped table-condensed table-hover tblborder" id="tblstyle">
                   <thead>
                    <tr>          
                      <th class="right" width="1%" style="background-color: #e6f2ff; color: #333333">No</th>
                      <th class="right" width="5%" style="background-color: #e6f2ff; color: #333333">Transfer </th>
                      <th width="10%" style="background-color: #e6f2ff; color: #333333"> From Name</th>
                      <th width="10%" style="background-color: #e6f2ff; color: #333333">From Account </th>
                      <th width="5%" style="background-color: #e6f2ff; color: #333333"> Transfer Date/Time</th>
                      <th width="5%" class="right" style="background-color: #e6f2ff; color: #333333">Amount </th>
                      <th width="10%" class="right" style="background-color: #e6f2ff; color: #333333">Commission Charges</th>
                    </tr>
                   </thead>
                   <tbody>
                     <tr *ngFor="let obj of _list.data" >
                      <td class="right">{{obj.autokey}}</td>
                      <td class="right" >{{obj.t3}}</td> 
                      <td class="left">{{obj.t1}}</td> 
                      <td class="left">{{obj.t2}}</td> 
                      <td>{{obj.t4}}</td> 
                      <td class="right">{{formatNumber(obj.n1)}}</td> 
                      <td  class="right">{{formatNumber(obj.n2)}}</td> 
                      </tr>  
                   </tbody>
                  </table>
                </div>
            </div>								
          </div>				
          </div>
        </div>
        </form> 
      </div>
    </div> 
    <div id="downloadExcel" style="display: none;width: 0px;height: 0px;"></div>       				
    <div id="downloadPdf" style="display: none;width: 0px;height: 0px;"></div>                    				
	</div>
</div>    
<div *ngIf="_divexport">    
	  <button type="button" class="close"  (click)="goClose()">&times;</button>
	  <iframe id="frame1" [src]="_printUrl" width="100%" height="100%"></iframe>
	</div>   
	<div [hidden] = "_mflag">
	  <div class="modal" id="loader"></div>
	</div>																									
  `,
})
export class merchantTransferReport {
  subscription: Subscription;
  sub: any;
  _util: ClientUtil = new ClientUtil();

  today = this._util.getTodayDate();
  myDatePickerOptions: IMyDpOptions = this._util.getDatePicker();
  dateobj: { date: { year: number, month: number, day: number } };
  _dates = { "fromDate": this.dateobj, "toDate": this.dateobj };
  _printUrl: SafeResourceUrl;
  _entryhide = true;
  _formhide: boolean;
  _pdfdata: boolean;
  _mflag = true;
  _divexport = false;
  // merchant = [];
  total_Amount = "0.00";
  total_CommissionCharges = "0.00";
  _sessionObj = this.getSessionObj();
  //////////////////////////////////////////////////////////////////////////////////////
  _toggleSearch = true;
  _SearchString = "";
  _FilterList: any;
  _TypeList: any;
  _OperationMode = "";
  i = 0;
  file_type = "";

  getSessionObj() {
    return { "sessionID": "", "userID": "", "lovDesc": "" };
  }
  _obj = this.getDefaultObj();
  _pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 1 };
  _list = {
    "data": [{
      "i1": 0, "n1": "", "n2": 0, "state": "", "t1": "",
      "t2": "", "t3": "", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "",
      "t9": ""
    }],
    "totalCount": 0, "currentPage": 1, "pageSize": 10
  };
  _FilterDataset =
    {
      filterList:
        [
          { "itemid": "", "caption": "", "fieldname": "", "datatype": "", "condition": "", "t1": "", "t2": "" }
        ],

      "pageNo": 1, "pageSize": 10, "filterSource": 0, "sessionID": "", "userID": "", "userName": this.ics._profile.userName
    };


  getDefaultObj() {
    return {
      "merchantID": "All", "sessionID": "", "userID": "", "fromDate": "", "toDate": "", "fromAccount": "", "alldate": true, "t1": "", "t2": "", "transtype": "",
      "toAccount": "", "totalCount": 0, "currentPage": 1, "pageSize": 10, "fileType": "EXCEL", "condition": "", "simpleSearch": ""
    };
  }
  constructor(private el: ElementRef, private l_util: ClientUtil, private renderer: Renderer, private ics: RpIntercomService, private _router: Router, private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences, private sanitizer: DomSanitizer) {
    if (!ics.getRole() || ics.getRole() == 0) this._router.navigate(['/login']);
    this.setTodayDateObj();
    this.setDateObjIntoString();
    //this.getAllMerchant();
    //this.transfertype();
  }

  ngOnInit() {
    //this.filterSearch();
    this.loadAdvancedSearchData();
  }

  filterSearch() {
    
    if (this._toggleSearch) this.filterCommonSearch();
    else this.ics.send001("FILTER");
  }

  filterCommonSearch() {
    var l_DateRange;
    var l_SearchString = "";

    this._FilterDataset.filterList = [];
    this._FilterDataset.filterSource = 0;

    if (jQuery.trim(this._SearchString) != "") {
      l_SearchString = jQuery.trim(this._SearchString);


      this._FilterDataset.filterList =
        [
          { "itemid": "1", "caption": "Search String", "fieldname": "SearchString", "datatype": "simplesearch", "condition": "c", "t1": l_SearchString, "t2": "" }
        ];
    }

    //  l_DateRange = { "itemid": "4", "caption": "Date Range", "fieldname": "DateRange", "datatype": "date", "condition": "eq", "t1": this.l_util.getTodayDate(), "t2": "" };
    //   this._FilterDataset.filterList.push(l_DateRange);

    if (this._OperationMode != "prepareFilter") {
      if (this._FilterDataset.pageNo == 1) this.goList();
      else this._FilterDataset.pageNo = 1;
    }
  }
  setTodayDateObj() {
    this._dates.fromDate = this._util.changestringtodateobject(this.today);
    this._dates.toDate = this._util.changestringtodateobject(this.today);
  }
  setDateObjIntoString() {
    this._obj.fromDate = this._util.getDatePickerDateymd(this._dates.fromDate);
    this._obj.toDate = this._util.getDatePickerDateymd(this._dates.toDate);
  }
  getAllDate(event) {
    this._entryhide = true;
    this._list.currentPage = 1;
    this._list.totalCount = 0;
    this._obj.alldate = event.target.checked;
  }
  closeList() {
    this.goClear();
  }
  goClose() {
    this._obj = this.getDefaultObj();
    this.today = this._util.getTodayDate();
    this.setTodayDateObj();
    this._divexport = false;
  }
  btnToggleSearch_onClick() {
    this.toggleSearch(!this._toggleSearch);
  }
  toggleSearch(aToggleSearch) {
    // Set Flag
    this._toggleSearch = aToggleSearch;

    // Clear Simple Search
    if (!this._toggleSearch) this._SearchString = '';

    // Enable/Disabled Simple Search Textbox
    jQuery('#isInputSearch').prop('disabled', !this._toggleSearch);

    // Hide Advanced Search
    if (this._toggleSearch) {
      this.ics.send001("CLEAR");
      //this._FilterDataset = null;
      this._obj = this.getDefaultObj();
    }

    // Show/Hide Advanced Search    
    //  
    if (this._toggleSearch) jQuery("#asAdvancedSearch").css("display", "none");                                                                             // true     - Simple Search
    else {
      jQuery("#asAdvancedSearch").css("display", "inline-block");                                                                     // false    - Advanced Search
      this._obj = this.getDefaultObj();
    }
    // Set Icon 
    //  
    if (this._toggleSearch) jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-up").addClass("glyphicon glyphicon-menu-down");                // true     - Simple Search
    else jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-down").addClass("glyphicon glyphicon-menu-up");                // false    - Advanced Search

    // Set Tooltip
    //
    let l_Tooltip = (this._toggleSearch) ? "Expand" : "Collapse";
    jQuery('#btnToggleSearch').attr('title', l_Tooltip);
  }

  renderAdvancedSearch(event) {
    this.toggleSearch(!event);
  }
  filterAdvancedSearch(event) {
    this._FilterDataset.filterSource = 1;
    this._FilterDataset.filterList = [];
    this._FilterDataset.filterList = event;
    

    if (this._OperationMode != "prepareFilter") {
      if (this._FilterDataset.pageNo == 1) this.goList();
      else
        this._FilterDataset.pageNo = 1;
    }
  }

  goPrint() {
    let l_Data: any = [];
    l_Data = this._FilterDataset;

    this._mflag = false;
    this.setDateObjIntoString();
    if (this._obj.fromDate > this._obj.toDate) {
      this._mflag = true;
      this.showMsg("From Date Should Not Exceed To Date.", false);
    } /* if (!/^(([1-9]|1[0-9])\/[A-Za-z]+\([N|n]\)[0-9]{6})$/.test(this._obj.nrc)) {
      this.showMsg("Invalid NRC No.", false);
    } */else {
      if (l_Data != null) {

        for (this.i = 0; this.i < l_Data.filterList.length; this.i++) {

          if (l_Data.filterList[this.i].itemid == "1") {
            this._obj.simpleSearch = l_Data.filterList[this.i].t1;
          }
          else if (l_Data.filterList[this.i].itemid == "2") {
            this._obj.fromDate = l_Data.filterList[this.i].t1;
            this._obj.toDate = l_Data.filterList[this.i].t2;
            this._obj.condition = l_Data.filterList[this.i].condition;
          }
          else if (l_Data.filterList[this.i].itemid == "3") {
            this._obj.merchantID = l_Data.filterList[this.i].t1;
          }
          else if (l_Data.filterList[this.i].itemid == "4") {

            this.file_type = l_Data.filterList[this.i].t1;
            if (this.file_type == "1") {
              this._obj.fileType = "EXCEL";
            }
            else
              this._obj.fileType = "PDF";
          }


        }


      }

      try {
        let url: string = this.ics.cmsurl + 'ServiceReportAdm/getWalletTopupReportJasperDownload';
        let json: any = this._obj;
        this._obj.sessionID = this.ics._profile.sessionID;
        this._obj.userID = this.ics._profile.userID;
        this.http.doPost(url, json).subscribe(
          data => {
            this._mflag = true;
            if (data.state) {
              this._divexport = true;
              let url2: string = this.ics.cmsurl + 'ServiceReportAdm/MerchantTransferReport';
              this._printUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url2);
              // this.showMsg("Download Successfully",true);
            } else {
              this.showMsg("Data Not Found!", false);
            }
          },
          error => {
            this._mflag = true;
            this.showMsg("HTTP Error Type ", false);
          },
          () => {
            this._mflag = true;
          }
        );
      } catch (e) {
        alert("Invalid URL.");
      }
    }
  }

  
  goClear() {
    this._obj = this.getDefaultObj();
    this.setTodayDateObj();
    this._entryhide = true;
  }

  changeFileType(options) {
    let value = options[options.selectedIndex].value;
    for (var i = 0; i < this.ref._lov1.ref010.length; i++) {
      if (this.ref._lov1.ref010[i].value = value)
        this._obj.fileType = value;
    }
  }

  /*changeMerchant(event) {
   let options = event.target.options;
   let k = options.selectedIndex;//Get Selected Index
   let value = options[options.selectedIndex].value;//Get Selected Index's Value    
   this._obj.merchantID = value;
   for (var i = 0; i < this.merchant.length; i++) {
     if (this.merchant[i].value == value) {
       this._obj.merchantID = this.merchant[i].value;            
       break;
     }

   }
 }*/

  showMsg(msg, bool) {
    if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
    if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
    if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
  }
  logout() {
    this._router.navigate(['/login']);
  }
  goList() {
    let l_Data: any = [];
    l_Data = this._FilterDataset;

    this._mflag = false;
    this.setDateObjIntoString();
    if (this._obj.fromDate > this._obj.toDate) {
      this._mflag = true;
      this.showMsg("From Date Should Not Exceed To Date.", false);
    }
    else {
      if (l_Data != null) {

        for (this.i = 0; this.i < l_Data.filterList.length; this.i++) {

          if (l_Data.filterList[this.i].itemid == "1") {
            this._obj.simpleSearch = l_Data.filterList[this.i].t1;
          }
          else if (l_Data.filterList[this.i].itemid == "2") {
            this._obj.fromDate = l_Data.filterList[this.i].t1;
            this._obj.toDate = l_Data.filterList[this.i].t2;
            this._obj.condition = l_Data.filterList[this.i].condition;
          }
          else if (l_Data.filterList[this.i].itemid == "3") {
            this._obj.merchantID = l_Data.filterList[this.i].t1;
          }
          else if (l_Data.filterList[this.i].itemid == "4") {

            this.file_type = l_Data.filterList[this.i].t1;
            if (this.file_type == "1") {
              this._obj.fileType = "EXCEL";
            }
            else
              this._obj.fileType = "PDF";
          }


        }


      }
      let url: string = this.ics.cmsurl + 'ServiceReportAdm/getWalletTopupReportJasper?download=0';
      let json: any = this._obj;
      this._obj.sessionID = this.ics._profile.sessionID;
      this._obj.userID = this.ics._profile.userID;
      this.http.doPost(url, json).subscribe(
        data => {
          this._mflag = true;
          this.total_Amount = data.total_Amount;
          this.total_CommissionCharges = data.total_CommissionCharges;

          if (data != null) {
            this._obj.totalCount = data.totalCount;
            if (this._obj.totalCount == 0) {
              this.showMsg("Data not Found!", false);
              this._entryhide = true;
            
            }
            else {
              this._obj.currentPage = data.currentPage;
              this._obj.pageSize = data.pageSize;
              this._list = data;
              if (data.merchantdata != null) {
                if (!(data.merchantdata instanceof Array)) {
                  let m = [];
                  m[0] = data.merchantdata;
                  data.merchantdata = m;
                }
                this._list.data = data.merchantdata;
                this._entryhide = false;
                if (this._obj.totalCount != 0) this._entryhide = false;
              }
            }
          }
        }
      )
    }

  }

  changedPager(event) {
    this._pgobj = event;
    this._obj.currentPage = this._pgobj.current;
    this._obj.pageSize = this._pgobj.size;
    this._obj.totalCount = this._pgobj.totalcount;
    if (this._pgobj.totalcount > 1) {
      //this._obj.click = 1;
      this.goList();
    }
  }
  formatNumber(amt) {

    if (amt != undefined && amt != "0") {
      return this.thousand_sperator(parseFloat(amt).toFixed(2));
    } else {
      return "0.00";
    }
  }
  thousand_sperator(num) {
    if (num != "" && num != undefined && num != null) {
      num = num.replace(/,/g, "");
    }
    var parts = num.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts;// return 100,000.00
  }


  closePage() {
    this._router.navigate(['000', { cmd: "READ", p1: this.ics._profile.userID }]);
  }
  loadAdvancedSearchData() {
    this._TypeList =
    {
      "lovStatus": [],
      "lovFileType":
        [
          { "value": "1", "caption": "EXCEL" },
          { "value": "2", "caption": "PDF" }
        ],
      //    "merchant": []
    };
    //this.loadStatus();  
    //this.getAllMerchant();
    this.loadMerchantType();


    this._FilterList =
      [
        { "itemid": "1", "caption": "Search String", "fieldname": "SearchString", "datatype": "simplesearch", "condition": "c", "t1": "", "t2": "", "t3": "" },
        { "itemid": "2", "caption": "Transaction Date", "fieldname": "FromDate", "datatype": "date", "condition": "bt", "t1": this.l_util.getTodayDate(), "t2": this.l_util.getTodayDate(), "t3": "true" },

        { "itemid": "3", "caption": "Merchant Type", "fieldname": "MerchantType", "datatype": "lovStatus", "condition": "", "t1": "", "t2": "", "t3": "" },
       // { "itemid": "4", "caption": "File Type", "fieldname": "SearchString", "datatype": "lovFileType", "condition": "c", "t1": "", "t2": "", "t3": "" }
        //{ "itemid": "4", "caption": "Date Range", "fieldname": "DateRange", "datatype": "date", "condition": "bt", "t1": this.l_util.getTodayDate(), "t2": this.l_util.getTodayDate(), "t3": "true" },
      ];
  }


  loadMerchantType() {

    try {
      let url: string = this.ics.cmsurl + 'serviceAdmLOV/getmerchantidlistdetail_adv';
      this._sessionObj.sessionID = this.ics._profile.sessionID;
      this._sessionObj.userID = this.ics._profile.userID;
      this._sessionObj.lovDesc = "StatusType";
      let json: any = this._sessionObj;
      this.http.doPost(url, json).subscribe(
        data => {
          if (data != null) {
            if (data.msgCode == "0016") {
              this.showMsg(data.msgDesc, false);
            }
            if (data.msgCode == "0000") {
              if (!(data.lovType instanceof Array)) {
                let m = [];
                m[0] = data.lovType;
                this.ref._lov3.MerchantType = m;
              } else {
                this.ref._lov3.MerchantType = data.lovType;
              }
              this._TypeList.lovStatus = [{ "value": "All", "caption": "All" }];
              this.ref._lov3.MerchantType.forEach((iItem) => {
                let l_Item = { "value": "", "caption": "" };

                l_Item['caption'] = iItem.caption;
                l_Item['value'] = iItem.value;

                this._TypeList.lovStatus.push(iItem);
              });
            }
          }
        },
        error => {
          if (error._body.type == "error") {
            alert("Connection Timed Out.");
          }
        },
        () => { }
      );
    } catch (e) {
      alert("Invalid URL.");
    }
  }

  goDownload(){
    if (this._obj.fileType == 'EXCEL') this.goPrintExcel();
    else this.goPrint();
  }


  goPrintExcel() {
    this._mflag = false;
    this.setDateObjIntoString();
    let l_Data: any = [];
    l_Data = this._FilterDataset;
    if (l_Data != null) {

      for (this.i = 0; this.i < l_Data.filterList.length; this.i++) {

        if (l_Data.filterList[this.i].itemid == "1") {
          this._obj.simpleSearch = l_Data.filterList[this.i].t1;
        }
        else if (l_Data.filterList[this.i].itemid == "2") {
          this._obj.fromDate = l_Data.filterList[this.i].t1;
          this._obj.toDate = l_Data.filterList[this.i].t2;
          this._obj.condition = l_Data.filterList[this.i].condition;
        }
        else if (l_Data.filterList[this.i].itemid == "3") {
          this._obj.merchantID = l_Data.filterList[this.i].t1;
        }
        else if (l_Data.filterList[this.i].itemid == "4") {

          this.file_type = l_Data.filterList[this.i].t1;
          if (this.file_type == "1") {
            this._obj.fileType = "EXCEL";
          }
          else
            this._obj.fileType = "PDF";
        }


      }


    }

    if (this._obj.fromDate > this._obj.toDate) {
      this._mflag = true;
      this.showMsg("From Date Should Not Exceed To Date.", false);
    } /* if (!/^(([1-9]|1[0-9])\/[A-Za-z]+\([N|n]\)[0-9]{6})$/.test(this._obj.nrc)) {
    this.showMsg("Invalid NRC No.", false);
  } */else {
      try {
        this._obj.fileType = "EXCEL";
        let url: string = this.ics.cmsurl + 'ServiceReportAdm/getMerchantReportList1?download=1';
        let json: any = this._obj;
        this._obj.sessionID = this.ics._profile.sessionID;
        this._obj.userID = this.ics._profile.userID;
        this.http.doPost(url, json).subscribe(
          data => {
            this._mflag = true;
            if (data.msgCode = "0016") {
              this.showMsg(data.msgDesc, false);
            }
            if (data.state) {
              let url2: string = this.ics.cmsurl + 'ServiceReportAdm/downloadReport?fileType=' + this._obj.fileType +
                "&fileName=" + data.stringResult[0] + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
              if (this._obj.fileType == "PDF") {
                this._mflag = true;
                jQuery("#downloadPdf").html("<iframe src=" + url2 + "></iframe>");
                this.showMsg("Download Successfully", true);
              }
              if (this._obj.fileType == "EXCEL") {
                this._mflag = true;
                jQuery("#downloadExcel").html("<iframe src=" + url2 + "></iframe>");
                this.showMsg("Download Successfully", true);
              }
            }
          },
          error => {
            this._mflag = true;
            this.showMsg("HTTP Error Type ", false);
          },
          () => {
            this._mflag = true;
          }
        );
      } catch (e) {
        alert("Invalid URL.");
      }
    }
  }



  /*loadStatus(){
       
    try {
       let url: string = this.ics.cmsurl + 'serviceAdmLOV/getLovType';
       this._sessionObj.sessionID = this.ics._profile.sessionID;
       this._sessionObj.userID = this.ics._profile.userID;
       this._sessionObj.lovDesc ="StatusType";
       let json: any = this._sessionObj;
       this.http.doPost(url, json).subscribe(
           data => {
                if (data != null) {
                    if (data.msgCode == "0016") {
                        this.showMsg(data.msgDesc, false);
                    }
                    if (data.msgCode == "0000") {
                        if (!(data.lovType instanceof Array)) {
                            let m = [];
                            m[0] = data.lovType;
                            this.ref._lov3.StatusType = m;
                        } else {
                            this.ref._lov3.StatusType = data.lovType;
                        }
  
                        this.ref._lov3.StatusType.forEach((iItem) => {
                            let l_Item = { "value": "", "caption": "" };
  
                            l_Item['caption'] = iItem.caption;
                            l_Item['value'] = iItem.value;
  
                            this._TypeList.lovStatus.push(iItem);
                        });
                    }
                }
            },
            error => {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            },
            () => { }
        );
    } catch (e) {
        alert("Invalid URL.");
    }
  }*/
  getAllMerchant() {
    this.http.doGet(this.ics.cmsurl + 'serviceAdmLOV/getmerchantidlistdetail?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(
      data => {
        this.ref._lov3.ref015 = data.ref015;
        if (this.ref._lov3.ref015 != null && this.ref._lov3.ref015 != undefined) {
          if (!(this.ref._lov3.ref015 instanceof Array)) {
            let m = [];
            m[1] = this.ref._lov3.ref015;
            this._TypeList.merchant.push(m[1]);
          }
          this._TypeList.merchant = [{ "value": "All", "caption": "All" }];
          for (let j = 0; j < this.ref._lov3.ref015.length; j++) {
            this._TypeList.merchant.push({ "value": this.ref._lov3.ref015[j].value, "caption": this.ref._lov3.ref015[j].caption });
            //this.merchant.push(this.ref._lov3.ref015[j]);
          }
        }
        /* if(this._obj.transtype != "2"){
            this.ref._lov3.ref015 =[];
        }else{
            this.ref._lov3.ref015 = this.merchant;
        } */
      },
      error => {
        if (error._body.type == 'error') {
          alert("Connection Timed Out!");
        }
      }, () => { }
    );
  }


}






/*<div class="form-group">
<label class="col-md-2">From Date</label>
<div class="col-md-3">
  <my-date-picker name="mydate"  [options]="myDatePickerOptions" [(ngModel)]="_dates.fromDate" ngDefaultControl></my-date-picker>
</div>
<label class="col-md-2">To Date</label>
<div class="col-md-3">
  <my-date-picker name="mydate" [options]="myDatePickerOptions" [(ngModel)]="_dates.toDate" ngDefaultControl></my-date-picker>
  <!-- <input class="form-control" type="date" [(ngModel)]="_obj.toDate" required="true" [ngModelOptions]="{standalone: true}">  -->
</div>
<div class="col-md-2">
  <label>
    <input type='checkbox' [(ngModel)]="_obj.alldate" [ngModelOptions]="{standalone: true}" (click)='getAllDate($event)'>
    ALL
  </label>
</div>
</div>
<div class="form-group">
<label class="col-md-2">Merchant Type</label>
<div class="col-md-3">
  <select  [(ngModel)]="_obj.merchantID" (change)="changeMerchant($event)" [ngModelOptions]="{standalone: true}" class="form-control input-sm" required>
    <option *ngFor="let item of merchant" value="{{item.value}}" >{{item.caption}}</option>
  </select>
</div>
<label class="col-md-2">File Type</label>
<div class="col-md-3">
  <select [(ngModel)]="_obj.select" [ngModelOptions]="{standalone: true}" (change)="changeSelectVal($event.target.options)" class="form-control col-md-0" required="true">
    <option *ngFor="let item of ref._lov1.ref010" value="{{item.value}}">{{item.caption}}</option>
  </select>
</div>
<!--<rp-input  [(rpModel)]="_obj.fileType" rpRequired ="true" rpLabelClass = "col-md-2" rpClass="col-md-3" rpType="ref010" rpLabel="File Type" (ngModelChange)="changeFileType($event)"></rp-input>-->
</div>*/