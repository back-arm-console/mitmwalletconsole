import { NgModule, CUSTOM_ELEMENTS_SCHEMA }       from '@angular/core';
import { FormsModule }    from '@angular/forms';
import { CommonModule }   from '@angular/common';
import { RpInputModule } from '../framework/rp-input.module';
import { MyDatePickerModule } from 'mydatepicker';
import { RpHttpService } from '../framework/rp-http.service';
import { AdminPagerModule } from '../util/adminpager.module';
import { PagerModule } from '../util/pager.module';
import { AdvancedSearchModule } from '../util/advancedsearch.module';
import {MultiselectModule} from '../util/multiselect.module';
import { ReportRouting }from './report.routing';
import { accountTransfer }from './accounttransfer.component';
import { UserReport }from './user-report.component';
import { wtopupReport }from './wtopup-report.component';
import { TrialBalanceReport } from './trialbalancereport.component';
import { ReportContainerComponent } from './frmreportcontainer.component';
import { DailyReport } from './dailyreport.component';
import { TransactionReport } from './transactionreport.component';
import { settlementReport } from './settlement-report.component';
import { merchantTransferReport } from './merchantTransferReport.component';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RpInputModule,    
    AdminPagerModule,
    PagerModule,
    MyDatePickerModule,
    MultiselectModule,
    AdvancedSearchModule,
    ReportRouting,
  ],
  exports : [],
  declarations: [   
    accountTransfer,
    UserReport,
    wtopupReport,
    TrialBalanceReport,
    ReportContainerComponent,
    DailyReport,
    TransactionReport,
    settlementReport,
    merchantTransferReport,
    accountTransfer
  ],
  providers: [
    RpHttpService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class reportModule {}