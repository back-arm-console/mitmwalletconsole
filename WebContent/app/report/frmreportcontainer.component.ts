import { Component, Input, Output,EventEmitter, ElementRef, ViewChild } from "@angular/core";
import { RpIntercomService } from "../framework/rp-intercom.service";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { RpHttpService } from "../framework/rp-http.service";

declare var jsPDF : any;
declare var jQuery;
@Component({
    selector : 'rpt-container',
    template : `
        <div style="margin-top:-6px;">
            <button id="btnClose" type="button" class="btn btn-md btn-secondary" (click)="closePrint()"  title="Close" style="cursor:pointer;text-align:center;width:34px;height:34px;padding:0;float:right;margin-bottom:3px;">
                <i id="lblClose" class="glyphicon glyphicon-remove" style="font-size:11px;"></i> 
            </button>
            <button title="Print" id="btnPrint" class="btn btn-primary btn-md" (click)="pdf_print()" type="button" style="float:right;margin-right:3px;cursor:pointer;text-align:center;height:34px;margin-bottom:3px;">
                <i class="glyphicon glyphicon-print" style="cursor: pointer"></i>
                <span style="font-size: small;">&nbsp;&nbsp;Print</span>    
            </button>
            <button title="Export PDF" id="btnPDF" class="btn btn-primary btn-md" (click)="pdf_download()" type="button" style="float:right;margin-right:3px;cursor:pointer;text-align:center;height:34px;margin-bottom:3px;">
                <i class="glyphicon glyphicon-export"  style="cursor: pointer"></i>
                <span style="font-size: small;">&nbsp;&nbsp;PDF</span>    
            </button>
            <button title="Export Excel" id="btnNew" class="btn btn-primary btn-md" (click)="exl_download()" type="button" style="float:right;margin-right:3px;cursor:pointer;text-align:center;height:34px;margin-bottom:3px;">
                <i class="glyphicon glyphicon-export"  style="cursor: pointer"></i>
                <span style="font-size: small;">&nbsp;&nbsp;Excel</span>    
            </button>
             <iframe *ngIf="_show" id="frame1" style="width: 100%;height: 100%;" [src]="_srcPrint"></iframe>  
             <iframe *ngIf="_showPDF" id="framePDF" style="width: 100%;height: 100%;" (load)="reset()" [src]="_srcPDF"></iframe>  
             <iframe *ngIf="_showExcel" id="frameExl" style="width: 100%;height: 100%;" (load)="reset()" [src]="_srcExcel"></iframe>  
             
             <!--
            <div id="content" #content>
                <h1>Title</h1>
                <p>
                Letter writing can be fun, help children learn to compose written text, and provide handwriting practice — and letters are valuable keepsakes. This guide was written for England's "Write a Letter Week" and contains activities to help children ages 5–9 put pen to paper and make someone's day with a handwritten letter.
                </p>
            </div>
            -->
        </div>
    `
}   
)

export class ReportContainerComponent{
    _srcPrint : SafeResourceUrl ;
    _srcPDF : SafeResourceUrl;
    _srcExcel : SafeResourceUrl;
   
    _show = true;
    _showPDF = false;
    _showExcel = false;

    @Input() set rpSrc(src : any){
        if(src != undefined && src != ""){
            this._srcPrint = this.sanitizer.bypassSecurityTrustResourceUrl(src);
        }
    }

    @Output() rpClose : any = new EventEmitter();
    constructor(private ics : RpIntercomService, private sanitizer : DomSanitizer, private http : RpHttpService){}

    closePrint(){
        
        if(this._show)
            this.rpClose.emit();
        else if(this._showPDF || this._showExcel){
            this.reset();
        }
    }

    pdf_print()
    {       
        let printContents, popupWin;
        printContents = document.getElementById('frame1').outerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
            <html>
                <head>
                    <title>Print tab</title>
                    <style>
                    //........Customized style.......
                    </style>
                </head>
            <body onload="window.print();window.close()">${printContents}</body>
            </html>`
        );
        popupWin.document.close();
    }

    pdf_download()
    {       
        setTimeout(() =>
        {
            this._show = false;
            this._showPDF = true;
            this._showExcel = false;
            
            let url = this.ics.icbsrpturl + "/blank.jsp?exportType=downloadpdf";
            
            this._srcPDF = this.sanitizer.bypassSecurityTrustResourceUrl(url);
        }, 50);
        
       
    }
    exl_download()
    {       
        setTimeout(() =>
        {
            this._show = false;
            this._showPDF = false;
            this._showExcel = true;
            let url = this.ics.icbsrpturl + "/blank.jsp?exportType=excel";
            
            this._srcExcel = this.sanitizer.bypassSecurityTrustResourceUrl(url);
        }, 500);
    }

    reset(){        
        setTimeout(()=>{
            this._show = true;
            this._showPDF = false;
            this._showExcel = false;
        },500);      
    }

}