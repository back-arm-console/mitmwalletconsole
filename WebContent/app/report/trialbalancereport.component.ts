import { Component, ElementRef, enableProdMode, Renderer } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { ActivatedRoute, Router } from '@angular/router';
import { IMyDpOptions } from 'mydatepicker';
import { Subscription } from 'rxjs/Subscription';
import { RpHttpService } from '../framework/rp-http.service';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpReferences } from '../framework/rp-references';
import { ClientUtil } from '../util/rp-client.util';
declare var jQuery: any;

enableProdMode();
@Component({
  selector: 'fmtrialbalance',
  template: ` 
  <div *ngIf='_rpList' >
  <div class="container-fluid">
  <div class="row clearfix">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
      <form class= "form-horizontal" ngNoForm>        
  				<legend>Trial Balance Report</legend>
  				<div class="cardview list-height"> 
  				  <div class="row col-md-12">  
  				  <button class="btn btn-sm btn-primary" type="button" (click)="goClear()">Clear</button>              
  				  <button class="btn btn-sm btn-primary" type="button" (click)="goPrint()">Print</button>          
                   </div>
           <div class="row col-md-12">&nbsp;</div>
                <div class="form-group"> 
                   <div class="col-md-12">
                       <div class="form-group">
                       <div class="col-md-2"></div>
                           <div class="col-md-2" >
                               <input type="radio" name="G" value="G"  [(ngModel)]="obj.type" >
                               <label style="font-weight:normal;">&nbsp;General</label>
                            </div>
                            <div class="col-md-2" >  
                                    <input type="radio" name="D" value="D"  [(ngModel)]="obj.type" >
                                    <label style="font-weight:normal;">&nbsp;Detail</label>
                            </div>
                       </div>
  						
  						<div class="form-group">
  						  <label class="col-md-2" >Display</label>
  						  <div class="col-md-3">
  							<select  required="true"  [(ngModel)]="obj.display"   class="form-control col-md-0"  >
  							   <option *ngFor="let item  of _display" value="{{item.value}}" >{{item.caption}}</option> 
  							</select>                     
                </div>
                <label class="col-md-2">Date</label>								
  							    <div class="col-md-3">
                        <my-date-picker name="mydate"   [options]="myDatePickerOptions"  [(ngModel)]="_dates.date" (ngModelChange)="changedate()" ngDefaultControl></my-date-picker> 
                                                                       
  							    </div>
  						 
                 </div>
                    <div class="form-group">            
  							    
  							    <label class="col-md-2">Currency</label>
  							    <div class="col-md-3">
                                  <select  required="true"  [(ngModel)]="obj.currency"   class="form-control col-md-0"  >
                                  <option *ngFor="let item  of _currency" value="{{item.value}}" >{{item.caption}}</option>
                                   </select>
                    </div>
                    <label class="col-md-2" >Report By</label>
              					<div class="col-md-3">
              						<select   required="true"  [(ngModel)]="obj.reportCode"  class="form-control col-md-0"  >
                                        <option *ngFor="let item  of _reportBy" value="{{item.value}}" >{{item.caption}}</option> 
              						</select>                     
          						</div>
                      </div>
                      <div class="form-group">
              						<label class="col-md-2" >Report Format</label>
              							<div class="col-md-3">
              								<select   required="true"   [(ngModel)]="obj.reportFormat"  class="form-control col-md-0"  >
                                                <option *ngFor="let item  of _reportFormat" value="{{item.value}}" >{{item.caption}}</option> 
              								</select>                      
                            </div>
                            <label class="col-md-2" >&nbsp;</label>
                              <div *ngIf="flag" class="col-md-3">
                                  <select   required="true"  class="form-control col-md-0" [(ngModel)]="obj.reftrial" (change)="changeTrial(t.value)" [disabled]="editMode" #t >
                                    <option *ngFor="let item  of _trial" value="{{item.value}}" >{{item.caption}}</option> 
                                  </select>                     
                              </div>
                              <div  *ngIf='chkflag' class="col-md-2">
                                <label>
                                <input type='checkbox' [(ngModel)]="_chkTrial" [ngModelOptions]="{standalone: true}" >
                               Trial Save
                                </label>                 
                                </div>
                      </div>
                    </div>
              </div>
            </div>
      </form>
      </div>
      <div>
      <div id="downloadPdf" style="display: none;width: 0px;height: 0px;"></div>
      <div id="downloadExcel" style="display: none;width: 0px;height: 0px;"></div>
   </div>
  </div>
</div>
</div>     
    <div [hidden] = "_mflag">
      <div class="modal" id="loader"></div>
   </div> 
   <rpt-container *ngIf='_divPrint' (rpClose)="goClosePrint()" [rpSrc]="_printUrl"></rpt-container> 																																			
  `,
})
export class TrialBalanceReport {
  subscription: Subscription;
  _util: ClientUtil = new ClientUtil();
  today = this._util.getTodayDate();
  myDatePickerOptions: IMyDpOptions = this._util.getDatePicker();
  dateobj: { date: { year: number, month: number, day: number } };
  _dates = { "date": this.dateobj}
  _printUrl: SafeResourceUrl;
  
  _pdfdata: boolean;
  _mflag = true;
  _rpList=true;
  _divPrint=false;
  type:any;
  flag=false;
  chkflag=false;
  _trial=[{"value":"1","caption":"Complete Trial"},{"value":"2","caption":"First Trial"},{"value":"3","caption":"Second Trial"}];
  _reportBy=[{"value":"2","caption":"GL Code"}];
  _currency=[{"value":"MMK","caption":"MMK"}];
  _display=[{"value":"A","caption":"Display Debit(Assets)First"},{"value":"L","caption":"Display Debit(Liabilities)First"}];
  _reportFormat=[{"value":"1","caption":"Format1"},{"value":"2","caption":"Format2"},{"value":"4","caption":"Format4"}];
  getSessionObj() {
      return { "sessionID": "", "userID": "" };
  }
  obj = this.getTrialObject();
  getTrialObject(){
    return{"type":"G","display":"A","date":{},"reportCode":"2","branch":"002","currency":"MMK","reportFormat":"1","reftrial":"1"};

}
  constructor(private el: ElementRef, private renderer: Renderer, private ics: RpIntercomService, private _router: Router, private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences, private sanitizer: DomSanitizer) {
    if (!ics.getRole() || ics.getRole() == 0) this._router.navigate(['/login']);
    this.setTodayDateObj();
    this.setDateObjIntoString();
    
  }
  showMsg(msg, bool) {
    if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
    if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
    if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
  }
  logout() {
    this._router.navigate(['/login']);
  }
  setTodayDateObj() {
    this._dates.date = this._util.changestringtodateobject(this.today);
   
  }
  setDateObjIntoString() {
    this.obj.date = this._util.getDatePickerDateymd(this._dates.date);
    console.log('My Date'+this.obj.date);
  }
   
  changedate() {
                
    let date1={};
    this.chkflag=false;

    date1=this._util.getDatePickerDateymd(this._dates.date);
    var year=date1.toString().substring(0,4);
    var month=date1.toString().substring(4,6);
    var day=date1.toString().substring(6,8);
    if(month=='03'&& day=='31'){
        this.flag=true;
    
    }
    else if(month=='09'&& day=='30'){
            this.flag=true;
          
        }
        else{
            this.flag=false;
        } 
}
convertDate3(m: any) {
    if (m != null) {
   // var returnDate = m.date.year + "-" + ("0" + m.date.month).slice(-2) + "-" + ("0" + m.date.day).slice(-2);
      var returnDate = ("0" + m.date.day).slice(-2) + "/" + ("0" + m.date.month).slice(-2) + "/" + m.date.year ;
      return returnDate;
    }
  }
    changeTrial(value) {
        this.type = value;

        if (this.type == 1) {
            this.chkflag = false;

        }
        else {
            this.chkflag = true;
        }
    }
    goClear(){
        // this._BranchList=[];
            this. _display=[{"value":"A","caption":"Display Debit(Assets)First"},{"value":"L","caption":"Display Debit(Liabilities)First"}];
            this._reportFormat=[{"value":"1","caption":"Format1"},{"value":"2","caption":"Format2"},{"value":"4","caption":"Format4"},{"value":"6","caption":"Format6"}];
            
            this.obj.display=this._display[0].value;
            this.obj.reportFormat=this._reportFormat[0].value;
            this.obj.type="G";
            this.flag=false;
    }
    
    goClosePrint() {
        this._rpList = true;
        this._divPrint = false;
       
    }
    convertDate(m: any) {   //yyyy-mm-dd
        if (m != null) {
            var returnDate = m.date.year + ("0" + m.date.month).slice(-2) + ("0" + m.date.day).slice(-2);
            return returnDate;
        }
    }
    goPrint(){
        this._rpList = false;
        this._divPrint = true;
        this._printUrl = "";
        // let url =this.ics.icbsrpturl+"/PrepareReportThree.jsp?";
        let url =this.ics.icbsrpturl+"/PrepareReportThree.jsp?";
       
       
            //url += "&ip=" + this.ics._profile.serverIP;
            url += "&UID="+ this.ics._profile.userID;
            //url += "ip=103.233.205.144";
            //url += "&UID=SysAdmin";
            url += "&RT=TRIAL";
            url += "&TT=" +this.obj.type;
            url +="&Date=" +this._util.getDatePickerDateymd(this._dates.date);;
            url +="&DO=" +this.obj.display;
            url += "&BC=ALL" 
            url += "&WS=ALL" ;
            url += "&RC=" +this.obj.currency;
            url +="&PS=1";
            url +="&RF=" +this.obj.reportFormat;
            url +="&BNK=15" ;
            url +="&RB=" +this.obj.reportCode;
            url +="&FBC=999";
            if(this.obj.reftrial=='2'){
                url +="&YE=FT" ;
                
            }
            else if(this.obj.reftrial=='3'){
                url +="&YE=ST" ;

            }
            if(this.type==1){
            url +="&TS=false";
            }
            else{
                url +="&TS=true";
                

            }
            this._printUrl = url;
    }
}
