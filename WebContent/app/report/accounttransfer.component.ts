import { Component, ElementRef, enableProdMode, Renderer } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { ActivatedRoute, Router } from '@angular/router';
import { IMyDpOptions } from 'mydatepicker';
import { Subscription } from 'rxjs/Subscription';
import { RpHttpService } from '../framework/rp-http.service';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpReferences } from '../framework/rp-references';
import { ClientUtil } from '../util/rp-client.util';
declare var jQuery: any;
declare var google: any;

enableProdMode();
@Component({
  selector: 'wallettopup-report',
  template: ` 
  <div *ngIf="!_pdfdata">  
    <div class="container-fluid">
      <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
		      <form class="form-horizontal" ngNoForm>
            <legend>Wallet Transfer Report
            <button type="button" class="btn btn-sm btn-primary" style="cursor:pointer;text-align:center;margin-left: 830px;width:30px;height:34px; color:white ;font-size:14px" (click)="goPrint()" title="Download PDF File">
                    <i  class="glyphicon glyphicon-save" style="color:white;margin-left:-4px;"></i>
              </button> 
              <button type="button" class="btn btn-sm btn-primary" style="cursor:pointer;text-align:center;margin-left: 10px;width:30px;height:34px; color:white ;font-size:20px" (click)="goPrintExcel()" title="Download Excel File">
                    <i  class="fa fa-file-excel-o" style="color:white;margin-left:-4px;"></i>
              </button>
      
              <div style="float:right;text-align:right;">                                
                <div style="display:inline-block;padding-right:0px;width:280px;">
                  <div class="input-group"> 
                    <input class="form-control" type="text" id="isInputSearch" placeholder="Search" autocomplete="off" spellcheck="false" [(ngModel)]="_SearchString" [ngModelOptions]="{standalone: true}" (keyup.enter)="filterSearch()">

                      <span id="btnSimpleSearch" class="input-group-addon">
                        <i class="glyphicon glyphicon-search" style="cursor: pointer;color:#2e8690" (click)="filterSearch()"></i>
                      </span>
                  </div>
                </div>

                <button id="btnToggleSearch" class="btn btn-sm btn-primary" type="button" style="cursor:pointer;text-align:center;margin-top:-24px;height:34px;" (click)="btnToggleSearch_onClick()" title="Collapse">
                  <i id="lblToggleSearch" class="glyphicon glyphicon-menu-down"></i> 
                </button>

                <button *ngIf="false" id="btnTogglePagination" type="button" class="btn btn-sm btn-primary" style="cursor:pointer;text-align:center;margin-left:25px;margin-top:-24px;width:30px;height:34px;" (click)="btnTogglePagination_onClick()" title="Collapse">
                  <i id="lblTogglePagination" class="glyphicon glyphicon-eject" style="color:#2e8690;margin-left:-4px;transform: rotate(90deg)"></i>
                </button>

                <div id="divPagination" style="display:none;margin-left:13px;margin-top:-7px;margin-bottom:5px;">
                  <adminpager id="pgPagination" rpPageSizeMax="100" [(rpModel)]="totalCount" (rpChanged)="changedPager($event)" style="font-size:14px;text-align:right;margin-top:-7px;margin-right:10px;"></adminpager>
                </div>

                <button id="btnClose" type="button" class="btn btn-sm btn-primary" (click)="closePage()" (mouseenter)="btnClose_MouseEnter()" (mouseleave)="btnClose_MouseLeave()" title="Close" style="cursor:pointer;height:34px;text-align:center;margin-top:-24px;">
                  <i id="lblClose" class="glyphicon glyphicon-remove"></i> 
                </button>                     
              </div>
      
            </legend>
				    <div class="cardview list-height"> 
					    <!--<div class="row col-md-12">
                <button class="btn btn-sm btn-primary" type="button" (click)="goClear()">Clear</button> 
                 <button class="btn btn-sm btn-primary" type="button" (click)="goList()">Show </button>     
                <button class="btn btn-sm btn-primary" type="button" (click)="goPrint()">Download </button> -->
              <!--  <button class="btn btn-sm btn-primary" type="button" (click)="goPrintPDF()">Download PDF</button> -->
             <!-- </div>-->
            <!-- <div class="row col-md-12">
              <button class="btn btn-sm btn-primary" type="button" (click)="gotoBarChart()">Bar Chart</button> 
             </div> -->
              <div style="padding: 5px 10px;"></div>
              <advanced-search id="asAdvancedSearch" [FilterList]="_FilterList" [TypeList]="_TypeList" rpVisibleItems=5 (rpHidden)="renderAdvancedSearch($event)" (rpChanged)="filterAdvancedSearch($event)" style="display:none; margin-left:5px"></advanced-search>
             <!-- <div class="row col-md-12">&nbsp;</div> -->
                <div class="form-group">
                  <div class="col-md-12">	
                    <div [hidden]="_entryhide">
                      <div class="form-group">
                        <div class="col-md-8">
                          <div style="margin-top: 10px;">  
                            <adminpager rpPageSizeMax="100" [(rpModel)]="_list.totalCount" (rpChanged)="changedPager($event)"></adminpager>
                          </div>
                           <button id="btnClose" type="button" class="btn btn-sm btn-primary" (click)="closeList()" (mouseenter)="btnClose_MouseEnter()" (mouseleave)="btnClose_MouseLeave()" title="Close" style="cursor:pointer;height:34px;text-align:center;">
                            <i id="lblClose" class="glyphicon glyphicon-remove"></i> 
                          </button>
                        
                        </div>

                        <div class="col-md-4" style="margin-top: 30px;overflow-x:auto;">      
                          <table style="align: right; font-size: 14px;float:right;" cellpadding="10px" cellspacing="10px">
                            <tbody>
                              <colgroup>
                                <col span="1" style="width: 50%;">
                                <col span="1" style="width: 50%;">                                  
                              </colgroup>                           
                              <tr>
                                <td style="color: blue;" class="left">
                                  <p><b>Grand Total Amount </b></p>
                                </td>
                                <td style="color: blue;" class="center"><p>&nbsp;&nbsp;:&nbsp;&nbsp;</p></td>
                                <td style="color: blue;" class="right">
                                  <p><b>{{formatNumber(this.total_Amount)}}</b></p>
                                </td>
                              </tr>
                              <tr>
                               
                                <td style="color: blue;" class="left">
                                  <p><b>Grand Total Bank Charges</b></p>
                                </td>
                                <td style="color: blue;" class="center"><p>&nbsp;&nbsp;:&nbsp;&nbsp;</p></td>
                                <td style="color: blue;" class="right">
                                  <p><b>{{formatNumber(this.total_BankCharges)}}</b></p>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                     

						          <!-- <div *ngIf="_shownull!=false" style="color:#ccc;">No result found!</div>-->

              
                      <div class="row col-md-12" style="overflow-x:auto;">
                      <table class="table table-striped table-condensed table-hover tblborder" id="tblstyle">
                          <thead style="background-color: #e6f2ff;">
                            <tr> 
                             <!-- <th width="1%" class="left">
                                <button id="btnClose" type="button" class="btn btn-sm btn-primary" (click)="closeList()" (mouseenter)="btnClose_MouseEnter()" (mouseleave)="btnClose_MouseLeave()" title="Close" style="cursor:pointer;height:34px;text-align:center;">
                                  <i id="lblClose" class="glyphicon glyphicon-remove"></i> 
                                </button>
                              </th>  -->       
                              <th width="1%" class="right" style="background-color: #e6f2ff; color: #333333">No</th>
                              <th  width="5%" class="right" style="background-color: #e6f2ff; color: #333333">Transfer ID</th>
                              <th width="10%" style="background-color: #e6f2ff; color: #333333"> From Name</th>
                              <th width="10%" style="background-color: #e6f2ff; color: #333333">To Name </th>
                              <th width="10%" style="background-color: #e6f2ff; color: #333333"> From Account</th>
                              <th width="10%" style="background-color: #e6f2ff; color: #333333">To Account</th>
                              <th width="10%" style="background-color: #e6f2ff; color: #333333"> Transfer Date/Time </th>
                              <th width="5%" class="right" style="background-color: #e6f2ff; color: #333333">Amount</th>
                              <th width="10%" class="right" style="background-color: #e6f2ff; color: #333333">Commission Charges</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr *ngFor="let obj of _list.data" style="background-color: #ffffff;">
                              <!--<td></td> -->
                              <td class="right" style="padding:8px">{{obj.autokey}}</td>
                              <td class="right">{{obj.transID}}</td> 
                              <td class="left">{{obj.t1}}</td> 
                              <td class="left">{{obj.t2}}</td> 
                              <td class="left">{{obj.fromAccount}}</td> 
                              <td class="left">{{obj.toAccount}}</td> 
                              <td class="left">{{obj.transDate}}</td> 
                              <td class="right">{{obj.amount}}</td> 
                              <td class="right">{{obj.commissionCharges}}
                            </tr>  
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <!--<div class="form-group">
                      <rp-input  [(rpModel)]="_obj.fileType" rpRequired ="true" rpLabelClass = "col-md-2" rpClass="col-md-3" rpType="ref010" rpLabel="File Type" (ngModelChange)="changeFileType($event)"></rp-input>
						        </div>-->
                  </div>            										
                </div>
              </div>
            </form> 
          </div>
        </div>
        <div id="downloadExcel" style="display: none;width: 0px;height: 0px;"></div>       				
        <div id="downloadPdf" style="display: none;width: 0px;height: 0px;"></div>       				
	</div>
</div>    
		<div [hidden] = "_mflag">
	  <div class="modal" id="loader"></div>
	</div>																								
  `,
})
export class accountTransfer {
  subscription: Subscription;
  sub: any;
  _util: ClientUtil = new ClientUtil();

  today = this._util.getTodayDate();
  myDatePickerOptions: IMyDpOptions = this._util.getDatePicker();
  dateobj: { date: { year: number, month: number, day: number } };
  _dates = { "fromDate": this.dateobj, "toDate": this.dateobj };
  _printUrl: SafeResourceUrl;
  _entryhide = true;
  _formhide: boolean;
  _pdfdata: boolean;
  showflag:boolean=false;
  total_Amount = "0.00";
  total_BankCharges = "0.00";
  _isLoading = true;
 _SearchString = "";
  changePage = true;
  current = 0;
  size = 0;
  i = 0;
 _showListing = false;
 _showPagination = false;
 //_FilterList: any;
 _toggleSearch = true;                       // true - Simple Search, false - Advanced Search
 _togglePagination = true;
 _OperationMode = "";

 _TypeList: any;
 _FilterList: any;
 _shownull = true;
//  _mflag = false;
_mflag=true;
_tflag = false;

 _ButtonInfo = { "role": "", "formname": "", "button": "" };

 
  _sessionObj = this.getSessionObj();
  getSessionObj() {
      return { "sessionID": "", "userID": "" };
  }
    _obj = this.getDefaultObj();
    /////////////////////////////////////////////
  getDefaultObj() {
    return {
      "sessionID": "", "userID": "", "fromDate": "", "toDate": "", "fromAccount": "","alldate":true,"t1":"","t2":"","transtype":"All","condition" :"","simpleSearch" :"",
      "toAccount": "", "totalCount": 0, "currentPage": 1, "pageSize": 10,"fileType":"PDF"
    };
  } 
  _pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 1 };
  _list = {
    "data": [{
      "amount": "", "autokey": 0, "bankCharges": "","code":"","crAccNumber":"",
      "createDate": "", "currencyCode": "", "drAccNumber": "", "fromAccount": "", "createddate": "", "modifieddate": "","commissionCharges":"",
      "fromDate": "","i1":0,  "mbankingkey": "", "merchantID": "", "merchantName":"","modifiedDate":"","n1":0,"n2":0,"n3":0,"settlementDate":"state","t1":"","t2":"","t3":"","t4":"",
      "t5":"","t6":"","t7":"","t8":"","toAccount":"","toDate":"","transID":"","transType":"","userID":""
    }],
    "totalCount": 0, "currentPage": 1, "pageSize": 10
  }; 
  _FilterDataset =
 {
     filterList:
     [
         { "itemid": "", "caption": "", "fieldname": "", "datatype": "", "condition": "", "t1": "", "t2": "" }
     ],

     "pageNo": 1, "pageSize": 10, "filterSource": 0, "sessionID": "", "userID": "", "userName": this.ics._profile.userName
 };

 _ListingDataset =
 {
    //  contentData:
    //  [
    //      {
    //          "srno": 0, "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": "", "modifiedDate": "",
    //          "modifiedTime": "", "userId": "", "userName": "", "modifiedUserId": "", "modifiedUserName": "",
    //          "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "",
    //          "t2": "", "t3": "", "t4": "", "t5": "", "t6": "",
    //          "t7": "", "t8": "", "t9": "", "t10": "", "t11": "",
    //          "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0,
    //          "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0,
    //          "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0,"statusdata":"",
    //          "upload": null, "answer": null, "videoUpload": null, "comData": null, "ansData": null,
    //          "cropdata": null, "agrodata": null, "ferdata": null, "towndata": null, "crop": null,
    //          "fert": null, "agro": null, "addTown": null, "uploadlist": null, "resizelist": null,
    //          "uploadDatalist": null, "uploadedPhoto": null
    //      }
    //  ],

     "pageNo": 0, "pageSize": 10, "totalCount": 1, "userID": "", "userName": ""
 };
  constructor(private el: ElementRef, private l_util: ClientUtil, private renderer: Renderer, private ics: RpIntercomService, private _router: Router, private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences, private sanitizer: DomSanitizer) {
    if (!ics.getRole() || ics.getRole() == 0) this._router.navigate(['/login']);
    this.setTodayDateObj();
    this.setDateObjIntoString();
    //this.transfertype();
  }
  
 ngOnInit() {
  // this._mflag = false;
 // this.filterSearch();
 this.goList();
  this.loadAdvancedSearchData();
}
  setTodayDateObj() {
    this._dates.fromDate = this._util.changestringtodateobject(this.today);
    this._dates.toDate = this._util.changestringtodateobject(this.today);
  }
  setDateObjIntoString() {
    this._obj.fromDate = this._util.getDatePickerDateymd(this._dates.fromDate);
    this._obj.toDate = this._util.getDatePickerDateymd(this._dates.toDate);
  }
  getAllDate(event) {
    this._entryhide=true;
    this._list.currentPage=1;
    this._list.totalCount=0;
    this._obj.alldate = event.target.checked;
  }
  btnToggleSearch_onClick() {
    
    this.toggleSearch(!this._toggleSearch);
}

btnTogglePagination_onClick() {
    this.togglePagination(!this._togglePagination);
}
toggleSearch(aToggleSearch) {
  // Set Flag
  this._toggleSearch = aToggleSearch;

  // Clear Simple Search
  if (!this._toggleSearch)     this._SearchString = '';       

  // Enable/Disabled Simple Search Textbox
  jQuery('#isInputSearch').prop('disabled', !this._toggleSearch);

  // Hide Advanced Search
  if (this._toggleSearch) {
    this.ics.send001("CLEAR");
    // this._obj.fromAccount = "";
    // this._obj.toAccount = "";
    // this._obj.fromDate = "";
    // this._obj.toDate = "";
    // this._obj.t1 = "";
    // this._obj.t2 = "";
    // this._obj.pageSize
    this._obj = this.getDefaultObj();

  }

  // Show/Hide Advanced Search    
  //  
  if (this._toggleSearch) jQuery("#asAdvancedSearch").css("display", "none");                                                                             // true     - Simple Search
  else {
    
    jQuery("#asAdvancedSearch").css("display", "inline-block");                                                                     // false    - Advanced Search
  }
  // Set Icon 
  //  
  if (this._toggleSearch) jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-up").addClass("glyphicon glyphicon-menu-down");                // true     - Simple Search
  else jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-down").addClass("glyphicon glyphicon-menu-up");                // false    - Advanced Search

  // Set Tooltip
  //
  let l_Tooltip = (this._toggleSearch) ? "Expand" : "Collapse";
  jQuery('#btnToggleSearch').attr('title', l_Tooltip);
}

togglePagination(aTogglePagination) {
  // Set Flag
  this._togglePagination = aTogglePagination;

  // Show/Hide Pagination
  //
  if (this._showPagination && this._togglePagination) jQuery("#divPagination").css("display", "inline-block");
  else jQuery("#divPagination").css("display", "inline-block");

  // Rotate Icon
  //
  if (!this._togglePagination) jQuery("#lblTogglePagination").css("transform", "rotate(270deg)");
  else jQuery("#lblTogglePagination").css("transform", "rotate(90deg)");

  // Set Icon Position
  if (!this._togglePagination) jQuery("#lblTogglePagination").css({ "margin-left": "-5px", "margin-top": "0px" });
  else jQuery("#lblTogglePagination").css({ "margin-left": "-5px", "margin-top": "-1px" });

  // Set Tooltip
  //
  let l_Tooltip = (this._togglePagination) ? "Collapse" : "Expand";
  jQuery('#btnTogglePagination').attr('title', l_Tooltip);
}

closePage() {
  this._router.navigate(['000', { cmd: "READ", p1: this.ics._profile.userID }]);
}

closeList() {
  this.goClear();
}

  goList(){
    this._mflag = false;
    this.changePage = true;
    this.setDateObjIntoString();
    
    if (this._obj.fromDate > this._obj.toDate) {
      this._mflag = true;
      this.showMsg("From Date Should Not Exceed To Date.", false);
    } 
    else{
      let url: string = this.ics.cmsurl + 'ServiceReportAdm/getWalletTopupReport?download=0' ;
      let json: any = this._obj;
        this._obj.sessionID = this.ics._profile.sessionID;
        this._obj.userID = this.ics._profile.userID;
        
        
        this.http.doPost(url, json).subscribe(
          data => {
            this._mflag = true;
            this.total_Amount = data.total_Amount;
            this.total_BankCharges = data.total_BankCharges
            if (this._obj.alldate == true){
              this.total_Amount = data.total_Allamount;
            }

        
            if (data != null) {
              this._obj.totalCount = data.totalCount;
              if (this._obj.totalCount == 0) {
                this.showMsg("Data not Found!", false);
                this._entryhide = true;
                

              }
              else{
              this._obj.currentPage = data.currentPage;
              this._obj.pageSize = data.pageSize;
              this._list = data;
              if (data.wldata != null) {
                if (!(data.wldata instanceof Array)) {
                  let m = [];
                  m[0] = data.wldata;
                  data.wldata = m;
                }
                this._list.data = data.wldata;
                if (this._obj.totalCount == 0){
                  this._entryhide = true;
                }
                else{
                  this._entryhide = false;
                  this._mflag = true;
                }
              }
          }
        }
        }
        )
    }

  }
  goPrint() {
    this._mflag = false;
    this.setDateObjIntoString();
    let l_Data : any = [];
     l_Data = this._FilterDataset;
    if(l_Data != null ){
  
    for( this.i= 0;this.i<l_Data.filterList.length;this.i++){

      if(l_Data.filterList[this.i].itemid == "1"){
        this._obj.simpleSearch = l_Data.filterList[this.i].t1;
      }
     else if(l_Data.filterList[this.i].itemid == "2"){
        this._obj.fromDate = l_Data.filterList[this.i].t1;
        this._obj.toDate = l_Data.filterList[this.i].t2;
        this._obj.condition = l_Data.filterList[this.i].condition;
      }
      else if(l_Data.filterList[this.i].itemid == "3"){
        this._obj.fromAccount = l_Data.filterList[this.i].t1;
      }
     else if(l_Data.filterList[this.i].itemid == "4"){
        this._obj.toAccount = l_Data.filterList[this.i].t1;
      }
     else  if(l_Data.filterList[this.i].itemid == "7"){
        this._obj.transtype = l_Data.filterList[this.i].t1;
      }
     // this._obj.fileType = "EXCEL";
     /*  if(l_Data.filterList[this.i].itemid == "8"){
       if(l_Data.filterList[this.i].t1 == "1")
        this._obj.fileType = "EXCEL";
      
       else if(l_Data.filterList[this.i].t1 == "2")
       this._obj.fileType = "PDF";
      } */
    
      
     }
      

    }
  
    if (this._obj.fromDate > this._obj.toDate) {
      this._mflag = true;
      this.showMsg("From Date Should Not Exceed To Date.", false);
    } /* if (!/^(([1-9]|1[0-9])\/[A-Za-z]+\([N|n]\)[0-9]{6})$/.test(this._obj.nrc)) {
      this.showMsg("Invalid NRC No.", false);
    } */else {
      try {
        this._obj.fileType = "PDF";
        let url: string = this.ics.cmsurl + 'ServiceReportAdm/getWalletTopupReport?download=1';
        let json: any = this._obj;
        this._obj.sessionID = this.ics._profile.sessionID;
        this._obj.userID = this.ics._profile.userID;
        this.http.doPost(url, json).subscribe(
          data => {
            this._mflag = true;
            if(data.msgCode="0016"){
              this.showMsg(data.msgDesc,false);
            }            
            if (data.state) {
              let url2: string = this.ics.cmsurl + 'ServiceReportAdm/downloadReport?fileType=' + this._obj.fileType +
                "&fileName=" + data.stringResult[0] + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
              if (this._obj.fileType == "PDF") {
                this._mflag = true;
                jQuery("#downloadPdf").html("<iframe src=" + url2 + "></iframe>");
                this.showMsg("Download Successfully",true);
              }
              if (this._obj.fileType == "EXCEL") {
                this._mflag = true;
                jQuery("#downloadExcel").html("<iframe src=" + url2 + "></iframe>");
                this.showMsg("Download Successfully",true);
              }
            }
          },
          error => {
            this._mflag = true;
            this.showMsg("HTTP Error Type ", false);
          },
          () => {
            this._mflag = true;
          }
        );
      } catch (e) {
        alert("Invalid URL.");
      }
    }
  }

  goPrintExcel() {
    this._mflag = false;
    this.setDateObjIntoString();
    let l_Data : any = [];
     l_Data = this._FilterDataset;
    if(l_Data != null ){
  
    for( this.i= 0;this.i<l_Data.filterList.length;this.i++){

      if(l_Data.filterList[this.i].itemid == "1"){
        this._obj.simpleSearch = l_Data.filterList[this.i].t1;
      }
     else if(l_Data.filterList[this.i].itemid == "2"){
        this._obj.fromDate = l_Data.filterList[this.i].t1;
        this._obj.toDate = l_Data.filterList[this.i].t2;
        this._obj.condition = l_Data.filterList[this.i].condition;
      }
      else if(l_Data.filterList[this.i].itemid == "3"){
        this._obj.fromAccount = l_Data.filterList[this.i].t1;
      }
     else if(l_Data.filterList[this.i].itemid == "4"){
        this._obj.toAccount = l_Data.filterList[this.i].t1;
      }
     else  if(l_Data.filterList[this.i].itemid == "7"){
        this._obj.transtype = l_Data.filterList[this.i].t1;
      }
     // this._obj.fileType = "EXCEL";
     /*  if(l_Data.filterList[this.i].itemid == "8"){
       if(l_Data.filterList[this.i].t1 == "1")
        this._obj.fileType = "EXCEL";
      
       else if(l_Data.filterList[this.i].t1 == "2")
       this._obj.fileType = "PDF";
      } */
    
      
     }
      

    }
  
    if (this._obj.fromDate > this._obj.toDate) {
      this._mflag = true;
      this.showMsg("From Date Should Not Exceed To Date.", false);
    } /* if (!/^(([1-9]|1[0-9])\/[A-Za-z]+\([N|n]\)[0-9]{6})$/.test(this._obj.nrc)) {
      this.showMsg("Invalid NRC No.", false);
    } */else {
      try {
        this._obj.fileType = "EXCEL";
        let url: string = this.ics.cmsurl + 'ServiceReportAdm/getWalletTopupReport?download=1';
        let json: any = this._obj;
        this._obj.sessionID = this.ics._profile.sessionID;
        this._obj.userID = this.ics._profile.userID;
        this.http.doPost(url, json).subscribe(
          data => {
            this._mflag = true;
            if(data.msgCode="0016"){
              this.showMsg(data.msgDesc,false);
            }            
            if (data.state) {
              let url2: string = this.ics.cmsurl + 'ServiceReportAdm/downloadReport?fileType=' + this._obj.fileType +
                "&fileName=" + data.stringResult[0] + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
              if (this._obj.fileType == "PDF") {
                this._mflag = true;
                jQuery("#downloadPdf").html("<iframe src=" + url2 + "></iframe>");
                this.showMsg("Download Successfully",true);
              }
              if (this._obj.fileType == "EXCEL") {
                this._mflag = true;
                jQuery("#downloadExcel").html("<iframe src=" + url2 + "></iframe>");
                this.showMsg("Download Successfully",true);
              }
            }
          },
          error => {
            this._mflag = true;
            this.showMsg("HTTP Error Type ", false);
          },
          () => {
            this._mflag = true;
          }
        );
      } catch (e) {
        alert("Invalid URL.");
      }
    }
  }

 /* goPrintPDF() {
    this._mflag = false;
    this.setDateObjIntoString();
    let l_Data: any = this._FilterDataset;
    if(l_Data != null ){
  
    for( this.i= 0;this.i<l_Data.filterList.length;this.i++){

      if(l_Data.filterList[this.i].itemid == "1"){
        this._obj.simpleSearch = l_Data.filterList[this.i].t1;
      }
      else if(l_Data.filterList[this.i].itemid == "2"){
        this._obj.fromDate = l_Data.filterList[this.i].t1;
        this._obj.toDate = l_Data.filterList[this.i].t2;
        this._obj.condition = l_Data.filterList[this.i].condition;
      }
      else if(l_Data.filterList[this.i].itemid == "3"){
        this._obj.fromAccount = l_Data.filterList[this.i].t1;
      }
     else if(l_Data.filterList[this.i].itemid == "4"){
        this._obj.toAccount = l_Data.filterList[this.i].t1;
      }
     else  if(l_Data.filterList[this.i].itemid == "7"){
        this._obj.transtype = l_Data.filterList[this.i].t1;
      }
    //  else if(l_Data.filterList[this.i].itemid == "8"){
    //    if(l_Data.filterList[this.i].t1 == "1")
    //     this._obj.fileType = "EXCEL";
    //   }
    //   else if(l_Data.filterList[this.i].t1 == "2"){
    //   this._obj.fileType = "PDF";
    this._obj.fileType = "PDF";
      
     }
      

    }
  
    if (this._obj.fromDate > this._obj.toDate) {
      this._mflag = true;
      this.showMsg("From Date Should Not Exceed To Date.", false);
    } /* if (!/^(([1-9]|1[0-9])\/[A-Za-z]+\([N|n]\)[0-9]{6})$/.test(this._obj.nrc)) {
      this.showMsg("Invalid NRC No.", false);
    } */ /*else {
      try {
        this._obj.fileType = "PDF";
        let url: string = this.ics.cmsurl + 'ServiceReportAdm/getWalletTopupReport?download=1';
        let json: any = this._obj;
        this._obj.sessionID = this.ics._profile.sessionID;
        this._obj.userID = this.ics._profile.userID;
        this.http.doPost(url, json).subscribe(
          data => {
            this._mflag = true;
            if(data.msgCode="0016"){
              this.showMsg(data.msgDesc,false);
            }            
            if (data.state) {
              let url2: string = this.ics.cmsurl + 'ServiceReportAdm/downloadReport?fileType=' + this._obj.fileType +
                "&fileName=" + data.stringResult[0] + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
              if (this._obj.fileType == "PDF") {
                this._mflag = true;
                jQuery("#downloadPdf").html("<iframe src=" + url2 + "></iframe>");
                this.showMsg("Download Successfully",true);
              }
              if (this._obj.fileType == "EXCEL") {
                this._mflag = true;
                jQuery("#downloadExcel").html("<iframe src=" + url2 + "></iframe>");
                this.showMsg("Download Successfully",true);
              }
            }
          },
          error => {
            this._mflag = true;
            this.showMsg("HTTP Error Type ", false);
          },
          () => {
            this._mflag = true;
          }
        );
      } catch (e) {
        alert("Invalid URL.");
      }
    }
  }  */
  goClear(){
    this._entryhide=true;
    this._list.currentPage=1;
    this._list.totalCount=0;
    this._list.data=[];
    this._obj = this.getDefaultObj();
    this.setTodayDateObj();
  }
  changeTransType(event){
    this._entryhide=true;
    this._list.currentPage=1;
    this._list.totalCount=0;
    let options = event.target.options;  
    let k = options.selectedIndex;//Get Selected Index
    let value = options[options.selectedIndex].value;//Get Selected Index's Value  
    for(var i=0; i< this.ref._lov1.ref011.length; i++){
      if(this.ref._lov1.ref011[i].value == value)
      this._obj.transtype = value;
    }    
  }
  transfertype() {
    try {
      this._mflag = false;
      let url: string = this.ics.cmsurl + 'serviceAdmLOV/getTransferTypes';

      this._sessionObj.sessionID = this.ics._profile.sessionID;
      this._sessionObj.userID = this.ics._profile.userID;
      let json: any = this._sessionObj;

      this.http.doPost(url, json).subscribe(
        data => {
          this._mflag = true;

          if (data != null) {
            if (data.msgCode == "0016") {
              this.showMsg(data.msgDesc, false);
              this.logout();
            }

            if (data.msgCode != "0000") {
              this.showMsg(data.msgDesc, true);
            }

            if (data.msgCode == "0000") {
              if (data.refAccountTransferType != null) {
                if (!(data.refAccountTransferType instanceof Array)) {
                  let m = [];
                  m[0] = data.refAccountTransferType;
                  this.ref._lov3.refAccountTransferType = m;
                  this._obj.transtype = this.ref._lov3.refAccountTransferType[0].value;
                } else {
                  this.ref._lov3.refAccountTransferType = data.refAccountTransferType;
                  this._obj.transtype = this.ref._lov3.refAccountTransferType[0].value;
                }
              }
            }
          }
          this._mflag = true;
        },
        error => {
          if (error._body.type == "error") {
            alert("Connection Timed Out!");
            this._mflag = true;
          } else {
          }
        },
        () => { }
      );
    } catch (e) {
      alert(e);
      this._mflag = true;
    }
  }  
  changeTransferType(options) {
    let value1 = options[options.selectedIndex].value;

    for (var i = 1; i < this.ref._lov3.refAccountTransferType.length; i++) {
      if (this.ref._lov3.refAccountTransferType[i].value == value1) {
        this._obj.transtype = value1;
        break;
      }
    }
  }
  filterSearch() {
    if (this._toggleSearch) this.filterCommonSearch();
    else this.ics.send001("FILTER");
  }
  filterCommonSearch() {
    var l_DateRange;
    var l_SearchString = "";

    this._FilterDataset.filterList = [];
    this._FilterDataset.filterSource = 0;

    if (jQuery.trim(this._SearchString) != "") {
      l_SearchString = jQuery.trim(this._SearchString);


      this._FilterDataset.filterList =
        [
          { "itemid": "1", "caption": "Search String", "fieldname": "SearchString", "datatype": "simplesearch", "condition": "c", "t1": l_SearchString, "t2": "" }
        ];
    }

    //  l_DateRange = { "itemid": "4", "caption": "Date Range", "fieldname": "DateRange", "datatype": "date", "condition": "eq", "t1": this.l_util.getTodayDate(), "t2": "" };
    //   this._FilterDataset.filterList.push(l_DateRange);

    if (this._OperationMode != "prepareFilter") {
      if (this._FilterDataset.pageNo == 1) this.filterRecords();
      else this._FilterDataset.pageNo = 1;
    }
  }
  changeFileType(options) {
    let value = options[options.selectedIndex].value;
    for (var i = 0; i < this.ref._lov1.ref010.length; i++) {
      if (this.ref._lov1.ref010[i].value = value)
        this._obj.fileType = value;
    }
  }

  formatNumber(amt) {

    if (amt != undefined && amt != "0") {
      return this.thousand_sperator(parseFloat(amt).toFixed(2));
    } else {
      return "0.00";
    }
  }
  thousand_sperator(num) {
    if (num != "" && num != undefined && num != null) {
      num = num.replace(/,/g, "");
    }
    var parts = num.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts;// return 100,000.00
  }

  
  
  restrictSpecialCharacter(event, fid, value) {
    if ((event.key >= '0' && event.key <= '9') || event.key == '.' || event.key == ',' || event.key == 'Home' || event.key == 'End' ||
      event.key == 'ArrowLeft' || event.key == 'ArrowRight' || event.key == 'Delete' || event.key == 'Backspace' || event.key == 'Tab' ||
      (event.which == 67 && event.ctrlKey === true) || (event.which == 88 && event.ctrlKey === true) ||
      (event.which == 65 && event.ctrlKey === true) || (event.which == 86 && event.ctrlKey === true)) {
      if (fid == 101) {
        if (/^[\\\"\'\;\:\>\|~`!@#\$%^&*\(\)]$/i.test(event.key)) {
          event.preventDefault();
        }
      }

      if (value.includes(".")) {
        if (/^[\\\"\'\;\:\>\|~`!@#\$%^&*.\(\)]$/i.test(event.key)) {
          event.preventDefault();
        }
      }
    }
    else {
      event.preventDefault();
    }
  }

  showMsg(msg, bool) {
    if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
    if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
    if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
  }
  logout() {
    this._router.navigate(['/login']);
  }
  changedPager(event) {
    this._pgobj = event;
    this.current = this._pgobj.current;
    this.size = this._pgobj.size;
    this._obj.currentPage = this._pgobj.current;
    this._obj.pageSize = this._pgobj.size;
    this._obj.totalCount = this._pgobj.totalcount;
    if (this._pgobj.totalcount > 1) {
      //this._obj.click = 1;
      if (this.changePage == true) {
        this.goList();
      }


      else {
        this.filterRecords();
      }
    }
  }
  loadAdvancedSearchData() {
    this._TypeList =
    {
      "lovStatus": [],
      "lovFileType":
        [
          { "value": "1", "caption": "EXCEL" },
          { "value": "2", "caption": "PDF" }
        ],

      // 4--agent to wallet
      // 1--wallet to wallet
      //2--merchant payment
      // 5--wallet to agent

      "lovTransferType":
        [
          { "value": "All", "caption": "All" },
          { "value": "1", "caption": "Wallet to wallet" },
          { "value": "2", "caption": "Merchant payment" },
          { "value": "4", "caption": "Agent to wallet" },
          { "value": "5", "caption": "Wallet to agent" }
        ],

    };
    // this.loadStatus();  

    this._FilterList =
      [{ "itemid": "1", "caption": "Search String", "fieldname": "SearchString", "datatype": "simplesearch", "condition": "c", "t1": "", "t2": "", "t3": "" },
      { "itemid": "2", "caption": "Transaction Date", "fieldname": "FromDate", "datatype": "date", "condition": "bt", "t1": this.l_util.getTodayDate(), "t2": this.l_util.getTodayDate(), "t3": "true" },
      //{ "itemid": "3", "caption": "To Date", "fieldname": "ToDate", "datatype": "date", "condition": "", "t1": this.l_util.getTodayDate(), "t2":"", "t3": "true"  },
      { "itemid": "3", "caption": "From Account", "fieldname": "FromAccount", "datatype": "simplesearch", "condition": "", "t1": "", "t2": "", "t3": "" },
      { "itemid": "4", "caption": "To Account", "fieldname": "ToAccount", "datatype": "simplesearch", "condition": "", "t1": "", "t2": "", "t3": "" },
      { "itemid": "5", "caption": "From Name", "fieldname": "FromName", "datatype": "simplesearch", "condition": "", "t1": "", "t2": "", "t3": "" },
      { "itemid": "6", "caption": "To Name", "fieldname": "ToName", "datatype": "simplesearch", "condition": "", "t1": "", "t2": "", "t3": "" },
      { "itemid": "7", "caption": "Transfer Type", "fieldname": "SearchString", "datatype": "lovTransferType", "condition": "c", "t1": "", "t2": "", "t3": "" },
    //  { "itemid": "8", "caption": "File Type", "fieldname": "SearchString", "datatype": "lovFileType", "condition": "c", "t1": "", "t2": "", "t3": "" }
      ];
  } 
  renderAdvancedSearch(event) {
    this.toggleSearch(!event);
  }

filterAdvancedSearch(event) {
  this._FilterDataset.filterSource = 1;
  this._FilterDataset.filterList = [];
  this._FilterDataset.filterList = event;

  if (this._OperationMode != "prepareFilter") {
      if (this._FilterDataset.pageNo == 1) this.filterRecords();
      else 
      this._FilterDataset.pageNo = 1;
  }
}

  filterRecords() {
    this._mflag = false;

    this._FilterDataset.sessionID = this.ics._profile.sessionID;
    this._FilterDataset.userID = this.ics._profile.userID;
    let l_Data: any = this._FilterDataset;

    // this._obj.fromDate = this._FilterDataset.filterList[0].t1;
    // this._obj.toDate = this._FilterDataset.filterList[0].t2;
    // this._obj.fromAccount = this._FilterDataset.filterList[1].t1;
    // this._obj.toAccount = this._FilterDataset.filterList[2].t1;
    // this._obj.transtype = this._FilterDataset.filterList[6].t1;
    // this._obj.fileType = this._FilterDataset.filterList[7].t1;


    let l_ServiceURL: string = this.ics.cmsurl + 'ServiceReportAdm/getWalletTopupReportWithAdvancedSearch?currentPage=' + this.current + "&size=" + this.size;
    // Show loading animation
    this.http.doPost(l_ServiceURL, l_Data).subscribe
      (
        data => {
         // this._mflag = false;
          this._mflag = true;
          this.changePage = false;
          this.total_Amount = data.total_Amount;
          this._entryhide = true;
          // this.total_BankCharges = data.total_BankCharges
          // if (this._obj.alldate == true){
          this.total_Amount = data.total_Allamount;
          // }


          if (data != null) {
            this._obj.totalCount = data.totalCount;
            if (this._obj.totalCount == 0) {
              this.showMsg("Data not Found!", false);
              this._entryhide = true;
              this._mflag = true;
            }
            else {
              this._obj.currentPage = data.currentPage;
              this._obj.pageSize = data.pageSize;
              this._list = data;
              if (data.wldata != null) {
                if (!(data.wldata instanceof Array)) {
                  let m = [];
                  m[0] = data.wlata;
                  data.wldata = m;
                }
                this._list.data = data.wldata;
                if (this._obj.totalCount == 0) {
                  this._entryhide = true;
                }
                else {
                  this._entryhide = false;
                  this._mflag = true;
                }
              }
            }
          }
        }
      )
  }
}
