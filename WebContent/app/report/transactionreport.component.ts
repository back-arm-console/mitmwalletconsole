import { Router, ActivatedRoute } from '@angular/router';

import { Component } from '@angular/core';
import { ClientUtil } from '../util/rp-client.util';

import { RpHttpService } from '../framework/rp-http.service';
import { RpIntercomService } from '../framework/rp-intercom.service';
import {RpReferences} from '../framework/rp-references';

declare var jQuery: any;

@Component(
{
    selector: 'frmtransactionlist',
    template: `
    <div class="container-fluid" style="padding-top:15px;">
        <div class="row clearfix">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
                <form class="form-horizontal">
                    <fieldset>
                        <legend>
                            <div style="float:left;display:inline;font-size:18px;margin-top:-3px;margin-left:10px;">
                                Transaction
                               
                           </div>

                        <div style="float:right;text-align:right;margin-top:-8px;margin-bottom:5px;">
                            <div style="display:inline-block;padding-right:0px;width:280px;">
                                    <div class="input-group"> 
                                        <input class="form-control" type="text" id="isInputSearch" placeholder="Search" autocomplete="off" spellcheck="false" [(ngModel)]="_SearchString" [ngModelOptions]="{standalone: true}" (keyup.enter)="filterSearch()">

                                        <span id="btnSimpleSearch" class="input-group-addon">
                                            <i class="glyphicon glyphicon-search" style="cursor: pointer" (click)="filterSearch()"></i>
                                        </span>
                                    </div>
                                </div>

                                <button id="btnToggleSearch" class="btn btn-md btn-secondary" type="button" style="cursor:pointer;text-align:center;margin-top:-24px;height:34px;" (click)="btnToggleSearch_onClick()" title="Collapse">
                                    <i id="lblToggleSearch" class="glyphicon glyphicon-menu-down"></i> 
                                </button>

                                <button *ngIf="false" id="btnTogglePagination" type="button" class="btn btn-md btn-default" style="cursor:pointer;text-align:center;margin-left:25px;margin-top:-24px;width:30px;height:34px;" (click)="btnTogglePagination_onClick()" title="Collapse">
                                    <i id="lblTogglePagination" class="glyphicon glyphicon-eject" style="color:#2e8690;margin-left:-4px;transform:rotate(90deg)"></i>
                                </button>

                                <div id="divPagination" style="display:none;margin-left:13px;margin-top:-7px;margin-bottom:5px;">
                                    <pager6 id="pgPagination" rpPageSizeMax="100" [(rpModel)]="_ListingDataset.totalRecords" (rpChanged)="changedPager($event)" style="font-size:14px;text-align:right;margin-top:-7px;margin-right:10px;"></pager6>
                                </div>

                                <button id="btnClose" type="button" class="btn btn-md btn-secondary" (click)="closePage()" (mouseenter)="btnClose_MouseEnter()" (mouseleave)="btnClose_MouseLeave()" title="Close" style="cursor:pointer;height:34px;text-align:center;margin-top:-24px;">
                                    <i id="lblClose" class="glyphicon glyphicon-remove"></i> 
                                </button>                      
                            </div>
                        </legend>

                        <advanced-search id="asAdvancedSearch" [FilterList]="_FilterList" [TypeList]="_TypeList" rpVisibleItems=5 (rpHidden)="renderAdvancedSearch($event)" (rpChanged)="filterAdvancedSearch($event)" style="display:none;"></advanced-search>
                        <div *ngIf="!_showListing" style="color:#ccc;">No result found!</div>
                        
                        <div *ngIf="_showListing">
                            <div class="table-responsive">
                                <table class="table table-striped table-condense table-hover tblborder table-bordered" style="font-size: small; margin-bottom: 0px !important;">
                                    <thead>
                                        <tr>
                                            <th *ngFor="let iItemID of _ItemList"  title="{{ getCaption(iItemID) }}">
                                                <div *ngIf="iItemID=='1'" style="min-width: 80px;table-layout:fixed;vertical-align:middle;text-align:right;">{{ getCaption(iItemID) }}</div>
                                                <div *ngIf="iItemID=='2'" style="min-width: 85px;table-layout:fixed;vertical-align:middle;text-align:center;">{{ getCaption(iItemID) }}</div>
                                                <div *ngIf="iItemID=='3'" style="min-width: 140px;table-layout:fixed;vertical-align:middle;text-align:left;">{{ getCaption(iItemID) }}</div>
                                                <div *ngIf="iItemID=='4'" style="min-width: 470px;table-layout:fixed;vertical-align:middle;text-align:left;">{{ getCaption(iItemID) }}</div>
                                                <div *ngIf="iItemID=='5'" style="min-width: 110px;table-layout:fixed;vertical-align:middle;text-align:right;">{{ getCaption(iItemID) }}</div>
                                                <div *ngIf="iItemID=='6'" style="min-width: 110px;table-layout:fixed;vertical-align:middle;text-align:right;">{{ getCaption(iItemID) }}</div>
                                                <div *ngIf="iItemID=='7'" style="min-width: 50px;table-layout:fixed;vertical-align:middle;text-align:center;">{{ getCaption(iItemID) }}</div>
                                                <div *ngIf="iItemID=='8'" style="min-width: 80px;table-layout:fixed;vertical-align:middle;text-align:center;">{{ getCaption(iItemID) }}</div>
                                                <div *ngIf="iItemID=='9'" style="min-width: 80px;table-layout:fixed;vertical-align:middle;text-align:left;">{{ getCaption(iItemID) }}</div>
                                                <div *ngIf="iItemID=='10'" style="min-width: 100px;table-layout:fixed;vertical-align:middle;text-align:left;">{{ getCaption(iItemID) }}</div>
                                                <div *ngIf="iItemID=='11'" style="min-width: 60px;table-layout:fixed;vertical-align:middle;text-align:center;">{{ getCaption(iItemID) }}</div>
                                                <div *ngIf="iItemID=='12'" style="min-width: 280px;table-layout:fixed;vertical-align:middle;text-align:left;">{{ getCaption(iItemID) }}</div>
                                                <div *ngIf="iItemID=='13'" style="min-width: 100px;table-layout:fixed;vertical-align:middle;text-align:left;">{{ getCaption(iItemID) }}</div>
                                                <div *ngIf="iItemID=='14'" style="min-width: 150px;table-layout:fixed;vertical-align:middle;text-align:left;">{{ getCaption(iItemID) }}</div>
                                                <div *ngIf="iItemID=='15'" style="min-width: 110px;table-layout:fixed;vertical-align:middle;text-align:right;">{{ getCaption(iItemID) }}</div>
                                                <div *ngIf="iItemID=='16'" style="min-width: 70px;table-layout:fixed;vertical-align:middle;text-align:right;">{{ getCaption(iItemID) }}</div>
                                                <div *ngIf="iItemID=='17'" style="min-width: 80px;table-layout:fixed;vertical-align:middle;text-align:right;">{{ getCaption(iItemID) }}</div>
                                                <div *ngIf="iItemID=='18'" style="min-width: 100px;table-layout:fixed;vertical-align:middle;text-align:center;">{{ getCaption(iItemID) }}</div>
                                                <div *ngIf="iItemID=='19'" style="min-width: 200px;table-layout:fixed;vertical-align:middle;text-align:left;">{{ getCaption(iItemID) }}</div>
                                                <div *ngIf="iItemID=='20'" style="min-width: 70px;table-layout:fixed;vertical-align:middle;text-align:center;">{{ getCaption(iItemID) }}</div>
                                                <div *ngIf="iItemID=='21'" style="min-width: 50px;table-layout:fixed;vertical-align:middle;text-align:center;">{{ getCaption(iItemID) }}</div>
                                                <div *ngIf="iItemID=='22'" style="min-width: 70px;table-layout:fixed;vertical-align:middle;text-align:center;">{{ getCaption(iItemID) }}</div>
                                                <div *ngIf="iItemID=='23'" style="min-width: 140px;table-layout:fixed;vertical-align:middle;text-align:left;">{{ getCaption(iItemID) }}</div>
                                                <div *ngIf="iItemID=='24'" style="min-width: 470px;table-layout:fixed;vertical-align:middle;text-align:left;">{{ getCaption(iItemID) }}</div>
                                                <div *ngIf="iItemID=='25'  && !_isMRHF" style="min-width: 80px;table-layout:fixed;vertical-align:middle;text-align:left;">{{ getCaption(iItemID) }}</div>
                                            </th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr *ngFor="let iItem of _ListingDataset.dataList">
                                           <td *ngFor="let iItemID of _ItemList" title="{{ getValue(iItemID, iItem) }}">
                                                <div *ngIf="iItemID=='1'" style="min-width: 80px;table-layout:fixed;vertical-align:middle;text-align:right;">{{ getValue(iItemID, iItem) }}</div>
                                                <div *ngIf="iItemID=='2'" style="min-width: 85px;table-layout:fixed;vertical-align:middle;text-align:center;">{{ getValue(iItemID, iItem) }}</div>
                                                <div *ngIf="iItemID=='3'" style="min-width: 140px;table-layout:fixed;vertical-align:middle;text-align:left;">{{ getValue(iItemID, iItem) }}</div>
                                                <div *ngIf="iItemID=='4'" style="min-width: 470px;table-layout:fixed;vertical-align:middle;text-align:left;">{{ getValue(iItemID, iItem) }}</div>
                                                <div *ngIf="iItemID=='5'" style="min-width: 110px;table-layout:fixed;vertical-align:middle;text-align:right;">{{ getValue(iItemID, iItem) }}</div>
                                                <div *ngIf="iItemID=='6'" style="min-width: 110px;table-layout:fixed;vertical-align:middle;text-align:right;">{{ getValue(iItemID, iItem) }}</div>
                                                <div *ngIf="iItemID=='7'" style="min-width: 50px;table-layout:fixed;vertical-align:middle;text-align:center;">{{ getValue(iItemID, iItem) }}</div>
                                                <div *ngIf="iItemID=='8'" style="min-width: 80px;table-layout:fixed;vertical-align:middle;text-align:center;">{{ getValue(iItemID, iItem) }}</div>
                                                <div *ngIf="iItemID=='9'" style="min-width: 80px;table-layout:fixed;vertical-align:middle;text-align:left;">{{ getValue(iItemID, iItem) }}</div>
                                                <div *ngIf="iItemID=='10'" style="min-width: 100px;table-layout:fixed;vertical-align:middle;text-align:left;">{{ getValue(iItemID, iItem) }}</div>
                                                <div *ngIf="iItemID=='11'" style="min-width: 60px;table-layout:fixed;vertical-align:middle;text-align:center;">{{ getValue(iItemID, iItem) }}</div>
                                                <div *ngIf="iItemID=='12'" style="min-width: 280px;table-layout:fixed;vertical-align:middle;text-align:left;">{{ getValue(iItemID, iItem) }}</div>
                                                <div *ngIf="iItemID=='13'" style="min-width: 100px;table-layout:fixed;vertical-align:middle;text-align:left;">{{ getValue(iItemID, iItem) }}</div>
                                                <div *ngIf="iItemID=='14'" style="min-width: 150px;table-layout:fixed;vertical-align:middle;text-align:left;">{{ getValue(iItemID, iItem) }}</div>
                                                <div *ngIf="iItemID=='15'" style="min-width: 110px;table-layout:fixed;vertical-align:middle;text-align:right;">{{ getValue(iItemID, iItem) }}</div>
                                                <div *ngIf="iItemID=='16'" style="min-width: 70px;table-layout:fixed;vertical-align:middle;text-align:right;">{{ getValue(iItemID, iItem) }}</div>
                                                <div *ngIf="iItemID=='17'" style="min-width: 80px;table-layout:fixed;vertical-align:middle;text-align:right;">{{ getValue(iItemID, iItem) }}</div>
                                                <div *ngIf="iItemID=='18'" style="min-width: 100px;table-layout:fixed;vertical-align:middle;text-align:center;">{{ getValue(iItemID, iItem) }}</div>
                                                <div *ngIf="iItemID=='19'" style="min-width: 200px;table-layout:fixed;vertical-align:middle;text-align:left;">{{ getValue(iItemID, iItem) }}</div>
                                                <div *ngIf="iItemID=='20'" style="min-width: 70px;table-layout:fixed;vertical-align:middle;text-align:center;">{{ getValue(iItemID, iItem) }}</div>
                                                <div *ngIf="iItemID=='21'" style="min-width: 50px;table-layout:fixed;vertical-align:middle;text-align:center;">{{ getValue(iItemID, iItem) }}</div>
                                                <div *ngIf="iItemID=='22'" style="min-width: 70px;table-layout:fixed;vertical-align:middle;text-align:center;">{{ getValue(iItemID, iItem) }}</div>
                                                <div *ngIf="iItemID=='23'" style="min-width: 140px;table-layout:fixed;vertical-align:middle;text-align:left;">{{ getValue(iItemID, iItem) }}</div>
                                                <div *ngIf="iItemID=='24'" style="min-width: 470px;table-layout:fixed;vertical-align:middle;text-align:left;">{{ getValue(iItemID, iItem) }}</div>
                                                <div *ngIf="iItemID=='25' && !_isMRHF" style="min-width: 80px;table-layout:fixed;vertical-align:middle;text-align:left;">
                                                    <div class="dropdown">
                                                        <button class="btn btn-link dropdown-toggle" type="button" id="customMenu" data-toggle="dropdown" style="text-decoration: none; outline: none; font-size: 12px; padding-left: 0px; padding-right: 0px;">
                                                            Actions
                                                            <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu" style="top: 100%; right: 0; font-size: 12px; left: auto;" role="menu" aria-labelledby="customMenu" data-container="body">
                                                            <li role="presentation"><a role="menuitem" tabindex="-1" (click)="goTo(iItem)">Account Inquiry</a></li>
                                                            <li role="presentation"><a role="menuitem" tabindex="-1" (click)="goAccountOpeningForm(iItem)">Account Opening</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                             </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div *ngIf="_showListing" style="text-align:center">Total {{ _ListingDataset.totalRecords }}</div>
                    </fieldset> 
                </form>
                <div id="ExcelDownload" style="display: none;width: 0px;height: 0px;"></div>
                <div class="loader modal" id="loader" style="top:40%;left:40%"></div>
            </div> 
        </div>
    </div>
    ` ,
})

export class TransactionReport 
{
    _isLoading = true;
    _OperationMode = "";
    _ParamCaller;
    _CallerPage = "";

    _SearchString = "";

    _showListing = false;
    _showPagination = false;

    _toggleSearch = true;                       // true - Simple Search, false - Advanced Search
    _togglePagination = true;

    _TypeList: any;
    _FilterList: any;
    _ItemList :any;

    _FormData =
    {
        "Posting": false, "TransNo": "TBA", "effectiveDate": "", "myDate": {}, "Amount": "", "AmountInWords": "", "Remark": "",
        "AccountNo1": "", "CurrencyCode1": "", "GLCheck1": false, "Description1": "", "AccountNo2": "", "CurrencyCode2": "", "GLCheck2": false, "Description2": "",
    };

    _FilterDataset =
    {
        filterList :
        [
            { "itemid": "", "caption": "", "fieldname": "", "datatype": "", "condition": "", "t1": "", "t2": "" }
        ],

        "pageNo": 1, "pageSize": 10, "filterSource": 0, "userID": "", "userName": "","productType":"","subtype":"","loancode":""
    };

    _ListingDataset = 
    {
        dataList :
        [
            { 
                "transNo": "", "terminalID": "", "transRef": "", "userID": "", "accountNumber": "", "effectiveDate": "", "description": "", "chequeNo": "", "currencyCode": "", 
                "amount": "", "previousBalance": "", "transactionType": "", "remark": "", "status": "", "subRef": "", "serialNo": "", "transactionTime": "", "supervisorID": "", "branchCode": "", "accRef": "","branchName":"","glDescription":""
            }
        ],

        "pageNo": 0, "pageSize": 0, "totalRecords": 1, "userID": "", "userName": ""
    };

    //added
    _appCodeCombo = [];
    _appCodeComboData = {"datalist":[{"productType": "","description": "","syskey":"","data":[{"mdCode":"","type":"","typeDesc":""}]}]};
    _appData="";
    _appCodeData = "";
    _appCodeDataList = {"data":[{"mdCode":"","type":"","typeDesc":""}]};
    _subtypecombo= [{ "value":"", "caption":"" }];
    _groupType=""; 

    _isMRHF = false;
    
    constructor(private ics: RpIntercomService, private _router: Router, private http: RpHttpService, private route:ActivatedRoute,
         private l_util: ClientUtil,private ref: RpReferences) 
    {
        if (!ics.getRole() || ics.getRole() == 0) 
        {
            this.http.doGet(this.ics._apiurl + 'service001/signout?userid=' + this.ics._profile.userID + "&sessionid=" + this.ics._profile.sessionID).subscribe
            (
                data => { },

                error => 
                {
                    this.ics.sendBean({ t1: "rp-error", t2: "Server connection error." });
                },
                () => { }
            );

            this._router.navigate(['Login', { p1: '*' }]);
        }
    }

    ngOnInit() 
    {
        // Stored Caller Form Data
        //
        if (this.ics.getBean() != null && this.ics.getBean().FormData != null) 
        {
            this._FormData = this.ics.getBean().FormData;
        }
    
        // Check Caller Page
        //
        this._ParamCaller = this.route.params.subscribe
        (
            params => 
            {
                this._CallerPage = params['pCaller'];

                if (this._CallerPage != undefined)
                {
                    if (this._CallerPage != null && this._CallerPage == "FromAccountListForm")
                    {
                        let _SearchString = params['paramFromAccountListForm'];

                        if (_SearchString != undefined && _SearchString != null && _SearchString != "")
                        {
                            this._SearchString = _SearchString;
                        }
                    }

                    this.filterCommonSearch();
                }else{
                    this.filterRecords();
                }
            }
        );
       
        this.loadAdvancedSearchData();
        this.loadTransactionColumn();
        this._isMRHF = false;
    }

    ngAfterViewChecked()
    {
        this._isLoading = false;
    }

    loadAdvancedSearchData()
    {
        this._TypeList = 
        {
            "lovTransType" :
            [
                { "value": "0", "caption": "Debit Transaction" }, 
                { "value": "1", "caption": "Credit Transaction" }, 
                { "value": "2", "caption": "Reversal Transaction" }        
            ],
            
            "lovTransType1" :
            [
                { "value": "0", "caption": "Contracted" }, 
                { "value": "1", "caption": "Disbursed" }, 
                { "value": "2", "caption": "Repaid" },
                { "value": "3", "caption": "Closed" },
                { "value": "4", "caption": "Deposit" },
                { "value": "5", "caption": "Withdraw" },
                { "value": "6", "caption": "Transfer" }
            ]
        };
        

        this._FilterList = 
        [
            { "itemid": "1", "caption": "Trans No", "fieldname": "TransNo", "datatype": "numeric", "condition": "bt", "t1": "", "t2": "", "t3": "true" },
            { "itemid": "2", "caption": "Terminal ID", "fieldname": "WorkStation", "datatype": "string", "condition": "c", "t1": "", "t2": "", "t3": "" },
            { "itemid": "3", "caption": "Trans Ref", "fieldname": "TransRef", "datatype": "numeric", "condition": "bt", "t1": "", "t2": "", "t3": "true" },
            { "itemid": "4", "caption": "User ID", "fieldname": "TellerID", "datatype": "string", "condition": "c", "t1": "", "t2": "", "t3": "" },
            { "itemid": "5", "caption": "Account Number", "fieldname": "AccNumber", "datatype": "string", "condition": "c", "t1": "", "t2": "", "t3": "" },
            { "itemid": "6", "caption": "Effective Date", "fieldname": "EffectiveDate", "datatype": "date", "condition": "bt", "t1": "", "t2": "", "t3": "true" },
            { "itemid": "7", "caption": "Description", "fieldname": "Description", "datatype": "string", "condition": "c", "t1": "", "t2": "", "t3": "" },
            { "itemid": "8", "caption": "Cheque No", "fieldname": "ChequeNo", "datatype": "string", "condition": "c", "t1": "", "t2": "", "t3": "" },
            { "itemid": "9", "caption": "Currency Code", "fieldname": "CurrencyCode", "datatype": "string", "condition": "c", "t1": "", "t2": "", "t3": "" },
            { "itemid": "10", "caption": "Amount", "fieldname": "Amount", "datatype": "numeric", "condition": "bt", "t1": "", "t2": "", "t3": "true" },
            { "itemid": "11", "caption": "Previous Balance", "fieldname": "PrevBalance", "datatype": "numeric", "condition": "bt", "t1": "", "t2": "", "t3": "true" },
            { "itemid": "12", "caption": "Transaction Type (In Digit)", "fieldname": "TransType", "datatype": "numeric", "condition": "bt", "t1": "", "t2": "", "t3": "true" },
            { "itemid": "13", "caption": "Remark", "fieldname": "Remark", "datatype": "string", "condition": "c", "t1": "", "t2": "", "t3": "" },
            { "itemid": "15", "caption": "Sub Ref", "fieldname": "SubRef", "datatype": "string", "condition": "c", "t1": "", "t2": "", "t3": "" },
            { "itemid": "16", "caption": "Filter Type (dr/cr/rev)", "fieldname": "TransType", "datatype": "lovTransType", "condition": "", "t1": "", "t2": "", "t3": "" },
            { "itemid": "17", "caption": "Product Name", "fieldname": "ProductName", "datatype": "string", "condition": "c", "t1": "", "t2": "", "t3": "" },
            { "itemid": "19", "caption": "Supervisor ID", "fieldname": "SupervisorID", "datatype": "string", "condition": "c", "t1": "", "t2": "", "t3": "" },
            { "itemid": "20", "caption": "Serial No", "fieldname": "SerialNo", "datatype": "numeric", "condition": "bt", "t1": "", "t2": "", "t3": "true" },
            { "itemid": "21", "caption": "Transaction Date", "fieldname": "TransTime", "datatype": "date", "condition": "bt", "t1": "", "t2": "", "t3": "true" },
            { "itemid": "23", "caption": "Currency Rate", "fieldname": "CurrencyRate", "datatype": "numeric", "condition": "bt", "t1": "", "t2": "", "t3": "true" },
            { "itemid": "24", "caption": "Previous Update", "fieldname": "PrevUpdate", "datatype": "date", "condition": "bt", "t1": "", "t2": "", "t3": "true" },
            { "itemid": "25", "caption": "Contra Date", "fieldname": "ContraDate", "datatype": "date", "condition": "bt", "t1": "", "t2": "", "t3": "true" },
            { "itemid": "26", "caption": "Acc Ref", "fieldname": "AccRef", "datatype": "string", "condition": "c", "t1": "", "t2": "", "t3": "" },
            { "itemid": "29", "caption": "Event Type", "fieldname": "TransType", "datatype": "lovTransType1", "condition": "", "t1": "", "t2": "", "t3": "" },
        ];
        
    }
    
    loadTransactionColumn()
    {
        this.http.doGet('json/transactionlist.json?random=' + Math.random()).subscribe
        (   
            data => 
            {
                if (data != null) 
                {   
                    this._ItemList = data;   
                    if(!this._isMRHF){
                        this._ItemList = this._ItemList.filter(item => item != "22");
                    }
                    else{
                        this._ItemList = this._ItemList.filter(item => item != "25");
                    }
                }
            }
        );
    }
    btnToggleSearch_onClick()
    {
        this.toggleSearch(!this._toggleSearch);
    }

    btnTogglePagination_onClick()
    {
        this.togglePagination(!this._togglePagination);
    }

    toggleSearch(aToggleSearch)
    {
        // Set Flag
        this._toggleSearch = aToggleSearch;
        
        // Clear Simple Search
        if (!this._toggleSearch)        jQuery("#isInputSearch").val("");

        // Enable/Disabled Simple Search Textbox
        jQuery('#isInputSearch').prop('disabled', !this._toggleSearch);

        // Hide Advanced Search
        if (this._toggleSearch)         this.ics.send001("CLEAR");

        // Show/Hide Advanced Search    
        //  
        if (this._toggleSearch)         jQuery("#asAdvancedSearch").css("display", "none");                                                                             // true     - Simple Search
        else                            jQuery("#asAdvancedSearch").css("display", "inline-block");                                                                     // false    - Advanced Search

        // Set Icon 
        //  
        if (this._toggleSearch)         jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-up").addClass("glyphicon glyphicon-menu-down");                // true     - Simple Search
        else                            jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-down").addClass("glyphicon glyphicon-menu-up");                // false    - Advanced Search

        // Set Tooltip
        //
        let l_Tooltip = (this._toggleSearch)? "Expand" : "Collapse";
        jQuery('#btnToggleSearch').attr('title', l_Tooltip);
    }

    togglePagination(aTogglePagination)
    {
        // Set Flag
        this._togglePagination = aTogglePagination;

        // Show/Hide Pagination
        //
        if (this._showPagination && this._togglePagination)         jQuery("#divPagination").css("display", "inline-block");
        else                                                        jQuery("#divPagination").css("display", "none");

        // Rotate Icon
        //
        if (!this._togglePagination)        jQuery("#lblTogglePagination").css("transform", "rotate(270deg)");
        else                                jQuery("#lblTogglePagination").css("transform", "rotate(90deg)");

        // Set Icon Position
        if (!this._togglePagination)        jQuery("#lblTogglePagination").css({ "margin-left": "-5px", "margin-top": "0px" });
        else                                jQuery("#lblTogglePagination").css({ "margin-left": "-5px", "margin-top": "-1px" });

        // Set Tooltip
        //
        let l_Tooltip = (this._togglePagination)? "Collapse" : "Expand";
        jQuery('#btnTogglePagination').attr('title', l_Tooltip);
    }

    goBack()
    {
        var l_URL = "";

        switch(this._CallerPage)
        {
            case "Receipt": l_URL = "icbs/Receipt1"; break;
            case "Payment": l_URL = "icbs/Payment1"; break;
            case "Transfer": l_URL = "icbs/Transfer1"; break;
            case "FromAccountListForm": l_URL = "icbs/AccOpeningListing"; break;
            default: break;
        }

        if (l_URL != "")
        {
            this.ics.sendBean({ "FormData": this._FormData });
            this._router.navigate([ l_URL ]);
        }
    }

    btnClose_MouseEnter()
    {
        jQuery("#btnClose").removeClass('btn btn-md btn-secondary').addClass('btn btn-md btn-danger');
    }

    btnClose_MouseLeave()
    {
        jQuery("#btnClose").removeClass('btn btn-md btn-danger').addClass('btn btn-md btn-secondary');
    }
    
    closePage()
    {
        if (this._CallerPage != undefined)
        {
            this.goBack();
        }
        else
        {
            this._router.navigate(['000']);
        }
    }

    renderAdvancedSearch(event)
    {
        this.toggleSearch(!event);
    }

    changedPager(event)
    {
        if (!this._isLoading)
        {
            let l_objPagination = event.obj;
        
            this._FilterDataset.pageNo = l_objPagination.current;
            this._FilterDataset.pageSize = l_objPagination.size;

            this.filterRecords();
        }
    }

    filterSearch()
    {
        if (this._toggleSearch)         this.filterCommonSearch();
        else                            this.ics.send001("FILTER");
    }

    filterCommonSearch() 
    {
        let l_SearchString = "";

        this._FilterDataset.filterList = [];
        this._FilterDataset.filterSource = 0;

        if (jQuery.trim(this._SearchString) != "")
        {
            l_SearchString = jQuery.trim(this._SearchString);

            this._FilterDataset.filterList = 
            [
                { "itemid": "1", "caption": "Trans No", "fieldname": "TransNo", "datatype": "numeric", "condition": "eq", "t1": l_SearchString, "t2": "" },
                { "itemid": "3", "caption": "Trans Ref", "fieldname": "TransRef", "datatype": "numeric", "condition": "eq", "t1": l_SearchString, "t2": "" },
                { "itemid": "5", "caption": "Account Number", "fieldname": "AccNumber", "datatype": "string", "condition": "c", "t1": l_SearchString, "t2": "" },
                { "itemid": "26", "caption": "Acc Ref", "fieldname": "AccRef", "datatype": "string", "condition": "c", "t1": l_SearchString, "t2": ""}
            ];
        }
        
        if (this._OperationMode == "prepareFilter")
        {
            this._OperationMode = "";
        }else
        {
           if (this._FilterDataset.pageNo == 1)    this.filterRecords();
        else                                    this._FilterDataset.pageNo = 1;
      
        }
    }

    filterAdvancedSearch(event)
    {
        this._FilterDataset.filterSource = 1;
        this._FilterDataset.filterList = event;
        
        if (this._OperationMode == "prepareFilter")
        {
            this._OperationMode = "";
        }
        else
        {
            if (this._FilterDataset.pageNo == 1)    this.filterRecords();
            else                                    this._FilterDataset.pageNo = 1;
   
        }
   }

    filterRecords()
    {
        this._FilterDataset.subtype = this._groupType;
        this._FilterDataset.productType = this._appData;
        this._FilterDataset.loancode = this._appCodeData;
        let l_Data: any = this._FilterDataset;
        let l_ServiceURL: string = this.ics.walleturl + 'serviceDBS/getTransactionListingDataset';
    
        // Show loading animation
        jQuery("#loader").modal();  

        this.http.doPost(l_ServiceURL, l_Data).subscribe
        (
            data => 
            {
                if (data != null) 
                {
                    this._ListingDataset = data;

                    // Convert to array for single item
                    if (this._ListingDataset.dataList != undefined)
                    {
                        this._ListingDataset.dataList = this.l_util.convertToArray(this._ListingDataset.dataList);

                        // Add thousand separator
                        //
                        for(let iIndex = 0; iIndex < this._ListingDataset.dataList.length; iIndex++)
                        {
                            this._ListingDataset.dataList[iIndex].amount = this.l_util.thousand_sperator(parseFloat(this._ListingDataset.dataList[iIndex].amount).toFixed(2));
                            this._ListingDataset.dataList[iIndex].previousBalance = this.l_util.thousand_sperator(parseFloat(this._ListingDataset.dataList[iIndex].previousBalance).toFixed(2));
                        }
                    }

                    // Show / Hide Listing
                    this._showListing = (this._ListingDataset.dataList != undefined);

                    // Show / Hide Pagination
                    this._showPagination = (this._showListing && (this._ListingDataset.totalRecords > 10));
                    
                    // Show / Hide Pagination
                    this.togglePagination(this._showListing); 

                    // Hide loading animation
                    jQuery("#loader").modal('hide');
                }
            },
            error => 
            {   
                // Hide loading animation
                jQuery("#loader").modal('hide');

                // Show error message
                this.ics.sendBean({ t1:"rp-error", t2: "Server connection error." });
             },
             () => { }
        );
    }
    
   btnExport_click() 
    { 
        this._OperationMode = "prepareFilter";
        this.filterSearch();

        if (this._OperationMode == "")
        {
            jQuery("#loader").modal();

            let l_Data: any = this._FilterDataset;
            let l_Url = this.ics.walleturl + 'serviceDBS/transactionListDownload';
        
            this.http.doPost(l_Url, l_Data).subscribe
            (
                res =>
                { 
                    if(res!=null)
                    {
                        let l_FileName = res.msgCode;
                        this.downloadFile(l_FileName);
                    }

                    this._OperationMode = "";
                    jQuery("#loader").modal('hide'); 
                },
                error => 
                {   
                    this._OperationMode = "";
                    jQuery("#loader").modal('hide');
                    
                    this.ics.sendBean({ t1 : "rp-error", t2: "Server connection error." });
                },
                () => { }
            );
        }
        else
        {
            this._OperationMode == "";
            this.ics.sendBean({ t1: "rp-error", t2: "Please select the selection criteria." });
        }
    }

    downloadFile(aFileName)
    {
        let l_Url = this.ics.walleturl + 'serviceDBS/downloadexcel?filename=' + aFileName;
        jQuery("#ExcelDownload").html("<iframe src=" + l_Url + "></iframe>");
    }
  
    getCaption(aItemID)
    {
        var l_Caption = '';

        switch (aItemID)
        {
            case '1':   l_Caption = 'Trans. No.'; break;
            case '2':   l_Caption = 'Terminal ID'; break;
            case '3':   l_Caption = 'Account Number'; break;
            case '4':   l_Caption = 'Description'; break;
            case '5':   l_Caption = 'Debit Amount'; break;
            case '6':   l_Caption = 'Credit Amount'; break;
            case '7':   l_Caption = 'Type'; break;
            case '8':   l_Caption = 'Cheque No.'; break;
            case '9':   l_Caption = 'Sub Ref.'; break;
            case '10':  l_Caption = 'Acc. Ref.'; break;
            case '11':  l_Caption = 'Currency'; break;
            case '12':  l_Caption = 'Remark'; break;
            case '13':  l_Caption = 'Effective Date'; break;
            case '14':  l_Caption = 'Trans. Time'; break;
            case '15':  l_Caption = 'Previous Bal.'; break;
            case '16':  l_Caption = 'Serial No.'; break;
            case '17':  l_Caption = 'Trans. Ref.'; break;
            case '18':  l_Caption = 'Branch Code'; break;
            case '19':  l_Caption = 'User ID'; break;
            case '20':  l_Caption = 'Counter'; break;
            case '21':  l_Caption = 'Status'; break;
            case '22':  l_Caption = 'Event Type'; break;
            case '23':  l_Caption = 'Branch Name'; break;
            case '24':  l_Caption = 'Account Description'; break;
            case '25':  l_Caption = 'Actions'; break;

            default : break;
        }

        return l_Caption;
    }

    getValue(aItemID, aItem)
    {
        var l_Value = '';

        switch (aItemID)
        {
            case '1':   l_Value = aItem.transNo; break;
            case '2':   l_Value = aItem.terminalID; break;
            case '3':   l_Value = aItem.accountNumber; break;
            case '4':   l_Value = aItem.description; break;
            case '5':   l_Value = (aItem.transactionType >= 500)? aItem.amount : ''; break;
            case '6':   l_Value = (aItem.transactionType < 500)? aItem.amount : ''; break;
            case '7':   l_Value = aItem.transactionType; break;
            case '8':   l_Value = aItem.chequeNo; break;
            case '9':   l_Value = aItem.subRef; break;
            case '10':  l_Value = aItem.accRef; break;
            case '11':  l_Value = aItem.currencyCode; break;
            case '12':  l_Value = aItem.remark; break;
            case '13':  
                        if(this._isMRHF) l_Value = this.l_util.convertDate7(aItem.effectiveDate); 
                        else l_Value = aItem.effectiveDate;
                        break;
            case '14':  
                        if(this._isMRHF) 
                            l_Value = aItem.transactionTime; 
                        else l_Value = this.l_util.convertDBDateTime1(aItem.transactionTime);
                        break;

            case '15':  l_Value = aItem.previousBalance; break;
            case '16':  l_Value = aItem.serialNo; break;
            case '17':  l_Value = aItem.transRef; break;
            case '18':  l_Value = aItem.branchCode; break;
            case '19':  l_Value = aItem.userID; break;
            case '20':  l_Value = aItem.supervisorID; break;
            case '21':  l_Value = aItem.status; break;
            case '22':  l_Value = aItem.transactionType1; break;
            case '23':  l_Value = aItem.branchName; break;
            case '24':  l_Value = aItem.glDescription; break;
            case '25':  l_Value = ""; break;
            

            default : break;
        }

        console.log('Item ID :', aItemID, ', Value : ', l_Value);
        return l_Value;
    } 

    goTo(obj){
        let accNo = obj.accountNumber;
        console.log("accNo is"+accNo);
        this._router.navigate(['icbs/AccOpeningListing', {cmd: "FromTransactionListForm", p1: accNo}]);
    }
    goAccountOpeningForm(obj){
    let accNumber = obj.accountNumber;
    console.log("accNumber is"+accNumber);
    this._router.navigate(['icbs/AccOpening', {cmd: "FromTransactionListForm", p1: accNumber}]);
   }
  
}