import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyDatePickerModule } from 'mydatepicker';
import { accountTransfer }       from './accounttransfer.component';
import { UserReport }from './user-report.component';
import { wtopupReport }from './wtopup-report.component';
import { TrialBalanceReport } from './trialbalancereport.component';
import { DailyReport } from './dailyreport.component';
import { TransactionReport } from './transactionreport.component';
import { settlementReport } from './settlement-report.component';
import { merchantTransferReport } from './merchantTransferReport.component';
const ReportRoutes: Routes = [
  { path: 'acctransfer', component: accountTransfer },
  { path: 'acctransfer/:cmd', component: accountTransfer },
  { path: 'acctransfer/:cmd/:id', component: accountTransfer }, 

  { path: 'merchant-transfer', component: merchantTransferReport },
  { path: 'merchant-transfer/:cmd', component: merchantTransferReport },
  { path: 'merchant-transfer/:cmd/:id', component: merchantTransferReport },
  { path: 'User Report',  component: UserReport }, 
  { path: 'wtopup Report', component: wtopupReport },
  { path: 'trialbalance', component: TrialBalanceReport },
  { path: 'dailyreport', component: DailyReport },
  { path: 'transactionreport', component: TransactionReport },
   { path: 'settlement-report', component: settlementReport },

];

export const ReportRouting: ModuleWithProviders = RouterModule.forChild(ReportRoutes);