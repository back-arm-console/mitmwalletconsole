import { Component, OnInit, OnDestroy, ElementRef, ViewChild, Input, Renderer } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { enableProdMode } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpInputComponent } from '../framework/rp-input.component';
import { RpHttpService } from '../framework/rp-http.service';
import { RpBean } from '../framework/rp-bean';
import { RpReferences } from '../framework/rp-references';
import { ClientUtil } from '../util/rp-client.util';
import { IMyDpOptions, IMyDateModel } from 'mydatepicker';
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
declare var jQuery: any;

enableProdMode();
@Component({
  selector: 'wallettopup-report',
  template: ` 
  <div *ngIf="!_pdfdata">  
	<div class="container col-md-12 col-sm-12 col-xs-12" style="padding: 0px 5%;">
		<form class="form-horizontal" ngNoForm>
			<legend>Wallet Top up Report</legend>
				<div class="cardview list-height"> 
					<div class="col-md-12" style="margin-bottom: 10px;">
          <button class="btn btn-sm btn-primary" type="button" (click)="goPrint('pdf')">Preview</button>              
				  <button class="btn btn-sm btn-primary" type="button" (click)="goPrint('excel')">Export</button>
          </div>
          <div class="col-md-12">
							<div class="form-group">            
								<label class="col-md-2">From Date</label>								
								<div class="col-md-3">
									<my-date-picker name="mydate"  [options]="myDatePickerOptions" [(ngModel)]="_dates.fromDate" ngDefaultControl></my-date-picker>                  
								</div>
								<label class="col-md-2">To Date</label>
								<div class="col-md-3">
									<my-date-picker name="mydate" [options]="myDatePickerOptions" [(ngModel)]="_dates.toDate" ngDefaultControl></my-date-picker>
									<!-- <input class="form-control" type="date" [(ngModel)]="_obj.toDate" required="true" [ngModelOptions]="{standalone: true}">  -->                            
								</div>								
								<div class="col-md-2">
								<label>
								<input type='checkbox' [(ngModel)]="_obj.alldate" [ngModelOptions]="{standalone: true}" (click)='getAllDate($event)'>
								ALL
								</label>
								</div>
							</div>	
							<div class="form-group">
								<label class="col-md-2">From Account</label>
								<div class="col-md-3">
								  <input type="text" [(ngModel)]="_obj.fromAccount" class="form-control input-sm" (keydown)="restrictSpecialCharacter($event, 101 ,_obj.fromAccount)" pattern="[0-9.,]+" required autofocus/>
								</div>
								<label class="col-md-2">To Account</label>
								<div class="col-md-3">
								  <input type="text" [(ngModel)]="_obj.toAccount" (keydown)="restrictSpecialCharacter($event, 101 ,_obj.toAccount)" pattern="[0-9.,]+"  class="form-control input-sm" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2">From Name</label>
								<div class="col-md-3">
								  <input type="text" [(ngModel)]="_obj.t1" class="form-control input-sm" required autofocus/>
								</div>
								<label class="col-md-2">To Name</label>
								<div class="col-md-3">
								  <input type="text" [(ngModel)]="_obj.t2" class="form-control input-sm" />
								</div>
              </div>
            </div>
							<!--<div class="form-group">
							  <label class="col-md-2">Transfer Type</label>
							  <div class="col-md-3">
							  <select [(ngModel)]="_obj.transtype" [ngModelOptions]="{standalone: true}"  (change)="changeTransferType($event.target.options)"  required="true"   class="form-control input-sm"  >
								 <option *ngFor="let item of ref._lov3.refAccountTransferType" value="{{item.value}}" >{{item.caption}}</option>
							  </select>                     
							</div>
							</div>-->				
				</div>
        </form> 
        <div id="accdownload" style="display: none;width: 0px;height: 0px;"></div>       				
	</div>
</div>    	

	<div *ngIf="_pdfdata">
    <img style='margin-left: 10px;cursor: pointer;' src='image/Excel-icon.png'  (click)="goExport('excel')">
	  <button type="button" class="close"  (click)="goClose()">&times;</button>
	  <iframe id="frame1" [src]="_printUrl" width="100%" height="100%"></iframe>
    <div id="accdownload" style="display: none;width: 0px;height: 0px;"></div>
	</div>
	
	<div [hidden] = "_mflag">
	  <div class="modal" id="loader"></div>
	</div>																									
  `,
})
export class wtopupReport {
  subscription: Subscription;
  sub: any;
  _util: ClientUtil = new ClientUtil();

  today = this._util.getTodayDate();
  myDatePickerOptions: IMyDpOptions = this._util.getDatePicker();
  dateobj: { date: { year: number, month: number, day: number } };
  _dates = { "fromDate": this.dateobj, "toDate": this.dateobj };
  _printUrl: SafeResourceUrl;
  _excelUrl: SafeResourceUrl;
  _entryhide: boolean;
  _formhide: boolean;
  _pdfdata: boolean;
  _mflag = true;
  _sessionObj = this.getSessionObj();
  getSessionObj() {
      return { "sessionID": "", "userID": "" };
  }
    _obj = this.getDefaultObj();
  getDefaultObj() {
    return {
      "sessionID": "", "userID": "", "fromDate": "", "toDate": "", "fromAccount": "","alldate":true,"t1":"","t2":"","transtype":"4",
      "toAccount": "", "totalCount": 0, "currentPage": 1, "pageSize": 10,
    };
  }  
  constructor(private el: ElementRef, private renderer: Renderer, private ics: RpIntercomService, private _router: Router, private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences, private sanitizer: DomSanitizer) {
    if (!ics.getRole() || ics.getRole() == 0) this._router.navigate(['/login']);
    this.setTodayDateObj();
    this.setDateObjIntoString();
    //this.transfertype();

    this._pdfdata = false;
  }
  setTodayDateObj() {
    this._dates.fromDate = this._util.changestringtodateobject(this.today);
    this._dates.toDate = this._util.changestringtodateobject(this.today);
  }
  setDateObjIntoString() {
    this._obj.fromDate = this._util.getDatePickerDateymd(this._dates.fromDate);
    this._obj.toDate = this._util.getDatePickerDateymd(this._dates.toDate);
  }
  getAllDate(event) {
    this._obj.alldate = event.target.checked;
  }
  goPrint(fileType) {
    this._mflag = false;
    this.setDateObjIntoString();
    if (this._obj.fromDate > this._obj.toDate) {
      this._mflag = true;
      this.showMsg("From Date Should Not Exceed To Date.", false);
    } /* if (!/^(([1-9]|1[0-9])\/[A-Za-z]+\([N|n]\)[0-9]{6})$/.test(this._obj.nrc)) {
      this.showMsg("Invalid NRC No.", false);
    } */else {
      try {
        let url: string = this.ics.cmsurl + 'ServiceReportAdm/getWalletTopupReport?fileType=' + fileType;
        let json: any = this._obj;
        this._obj.sessionID = this.ics._profile.sessionID;
        this._obj.userID = this.ics._profile.userID;
        this.http.doPost(url, json).subscribe(
          data => {
            this._mflag = true;
            if (data.state) {
              let url2: string = this.ics.cmsurl + 'ServiceReportAdm/downloadReport?fileType=' + fileType +
                "&fileName=" + data.stringResult[0] + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
              if (fileType == "pdf") {
                this._printUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url2);
                this._pdfdata = true;
              } else if (fileType == "excel") {
                jQuery("#accdownload").html("<iframe src=" + url2 + "></iframe>");
              }

            } else {
              this.showMsg(data.msgDesc, false);
            }
          },
          error => {
            this._mflag = true;
            this.showMsg("HTTP Error Type ", false);
          },
          () => {
            this._mflag = true;
          }
        );
      } catch (e) {
        alert("Invalid URL.");
      }
    }
  }
  goClose() {
    this._pdfdata = false;
    //this.setTodayDateObj();
    //this._obj = this.getDefaultObj();   
  }
 
/*   transfertype() {
    try {
      this._mflag=false;
      let url: string = this.ics.cmsurl + 'serviceAdmLOV/getTransferTypes';

      this._sessionObj.sessionID = this.ics._profile.sessionID;
      this._sessionObj.userID = this.ics._profile.userID;
      let json: any = this._sessionObj;

      this.http.doPost(url, json).subscribe(
        data => {
          this._mflag = true;

          if (data != null) {
            if (data.msgCode == "0016") {
              this.showMsg(data.msgDesc,false);
              this.logout();
            }

            if (data.msgCode != "0000") {
              this.showMsg(data.msgDesc,true);
            }

            if (data.msgCode == "0000") {
              if (data.refAccountTransferType != null) {
                if (!(data.refAccountTransferType instanceof Array)) {
                  let m = [];
                  m[0] = data.refAccountTransferType;
                  this.ref._lov3.refAccountTransferType = m;
                  this._obj.transtype = this.ref._lov3.refAccountTransferType[0].value;
                } else {
                  this.ref._lov3.refAccountTransferType = data.refAccountTransferType;
                  this._obj.transtype = this.ref._lov3.refAccountTransferType[0].value;
                }
              }
            }
          }
          this._mflag=true;
        },
        error => {
          if (error._body.type == "error") {
            alert("Connection Timed Out!");
            this._mflag=true;
          } else {
          }
        },
        () => { }
      );
    } catch (e) {
      alert(e);
      this._mflag=true;
    }
  } */  
  changeTransferType(options) {
    let value1 = options[options.selectedIndex].value;

    for (var i = 1; i < this.ref._lov3.refAccountTransferType.length; i++) {
      if (this.ref._lov3.refAccountTransferType[i].value == value1) {
        this._obj.transtype = value1;
        break;
      }
    }
  }

  showMsg(msg, bool) {
    if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
    if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
    if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
  }
  restrictSpecialCharacter(event, fid, value) {
    if ((event.key >= '0' && event.key <= '9') || event.key == '.' || event.key == ',' || event.key == 'Home' || event.key == 'End' ||
        event.key == 'ArrowLeft' || event.key == 'ArrowRight' || event.key == 'Delete' || event.key == 'Backspace' || event.key == 'Tab' ||
        (event.which == 67 && event.ctrlKey === true) || (event.which == 88 && event.ctrlKey === true) ||
        (event.which == 65 && event.ctrlKey === true) || (event.which == 86 && event.ctrlKey === true)) {
        if (fid == 101) {
            if (/^[\\\"\'\;\:\>\|~`!@#\$%^&*\(\)]$/i.test(event.key)) {
                event.preventDefault();
            }
        }

        if (value.includes(".")) {
            if (/^[\\\"\'\;\:\>\|~`!@#\$%^&*.\(\)]$/i.test(event.key)) {
                event.preventDefault();
            }
        }
    }
    else {
        event.preventDefault();
    }
}
  logout() {
    this._router.navigate(['/login']);
  }
}
