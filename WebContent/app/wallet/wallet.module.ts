import { NgModule, CUSTOM_ELEMENTS_SCHEMA }       from '@angular/core';
import { FormsModule }    from '@angular/forms';
import { CommonModule }   from '@angular/common';
import { RpInputModule } from '../framework/rp-input.module';
import { walletRouting }     from './wallet.routing';
import { RpHttpService } from '../framework/rp-http.service';
import { AdminPagerModule } from '../util/adminpager.module';
import {walletList} from './walletList.component';
import { PagerModule } from '../util/pager.module';
import { AdvancedSearchModule } from '../util/advancedsearch.module';
import { MyDatePickerModule } from 'mydatepicker';
import {WalletComponent} from './wallet.component';
import { FrmActivateUserComponent }  from './activateuser.component';
import { FrmActivateUserListComponent } from './activateuser-List.component';
import { FrmDeactivateUserComponent } from './deactivateuser.component';
import { FrmDeactivateUserListComponent } from './deactivateuser-List.component';
import { FrmLockUserComponent } from './lockuser.component'; 
import { FrmLockUserListComponent } from './lockuser-List.component'; 
import { FrmUnlockUserComponent } from './unlockuser.component'; 
import { FrmUnlockUserListComponent } from './unlockuser-List.component';  
import { ResetPasswordComponent } from './resetpassword.component'; 

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RpInputModule,
    walletRouting,
    AdminPagerModule,
    PagerModule,
    AdvancedSearchModule,
    MyDatePickerModule,    
    
  ],
  exports : [],
  declarations: [
   walletList,
   WalletComponent,
   FrmActivateUserComponent,
   FrmActivateUserListComponent,
   FrmDeactivateUserComponent,
   FrmDeactivateUserListComponent,
   FrmLockUserComponent,
   FrmLockUserListComponent,
   FrmUnlockUserComponent,
   FrmUnlockUserListComponent,
   ResetPasswordComponent,
  ],
  providers: [
    RpHttpService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class walletModule {}