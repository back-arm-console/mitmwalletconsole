import { Component, Input, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { enableProdMode } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
// RP Framework
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpInputComponent } from '../framework/rp-input.component';
import { RpHttpService } from '../framework/rp-http.service';
import { RpBean } from '../framework/rp-bean';
declare var jQuery: any;
// Application Specific
import { Pager } from '../util/pager.component';
import {AdminPager} from '../util/adminpager.component';
import { ClientUtil } from '../util/rp-client.util';
enableProdMode();
@Component({
    selector: 'deactivateuser-list',
    template: ` 
    <div class="container-fluid">
    <form class="form-horizontal"> 
		<legend>User List</legend>
		<div class="cardview list-height">  
			<div class="row col-md-12">
				<div class="row col-md-3">
					<div  class="input-group">
						<span class="input-group-btn">
							<button class="btn btn-sm btn-primary" type="button" (click)="goBack();">Back</button>
						</span> 
						<input id="textinput" name="textinput" type="text"  placeholder="Search" [(ngModel)]="_userobj.searchText" (keyup)="searchKeyup($event)" maxlength="30" class="form-control input-sm">
						<span class="input-group-btn">
							<button class="btn btn-sm btn-primary" type="button" (click)="Searching()" >
								<span class="glyphicon glyphicon-search"></span>Search
							</button>
						</span>        
					</div> 
				</div>		
                    <div class="pagerright">
					    <adminpager rpPageSizeMax="100" [(rpModel)]="_userobj.totalCount" (rpChanged)="changedPager($event)" ></adminpager> 
					</div>
			</div>
		 
			<table class="table table-striped table-condensed table-hover tblborder" id="tblstyle">
				<thead>
				<tr>
                    <th style="width:6%">ID </th>
                    <th style="width:10%">Name </th>      
                    <th style="width:20%">Status</th>     
                </tr>
				</thead>
				<tbody>
    				<tr *ngFor="let obj of _userobj.data">
                        <td><a (click)="goto(obj.syskey)">{{obj.t1}}</a></td>
                        <td>{{obj.t3}}</td>
                        <td *ngIf="obj.n1 == 11" >Lock</td>
                        <td *ngIf="obj.recordStatus == 1 " >Save</td>
                        <td *ngIf="obj.recordStatus == 2 && obj.n1 == 0" >Activate</td>
                        <td *ngIf="obj.recordStatus == 21 " >Deactivate</td>
                    </tr>   
				</tbody>
			</table>
		</div>
	</form>
  </div> 
  
    <div [hidden] = "_mflag">
        <div class="modal" id="loader"></div>
    </div>
   `
})

export class FrmDeactivateUserListComponent {
    // RP Framework 
    subscription: Subscription;
    // Application Specific
    _searchVal = ""; // simple search
    _flagas = true; // flag advance search
    _col_list = [{ "col_no": "1", "flag": false }, { "col_no": "2", "flag": false }]
    _sorting = { _sort_type: "asc", _sort_col: "1" };
    _mflag = true;
    _util: ClientUtil = new ClientUtil();
    sessionAlertMsg = ""; _recordhide: boolean; _msghide: boolean;
    _userobj = {
        "data": [{ "syskey": 0, "userId": "", "userName": "", "recordStatus": 0, "t1": "", "n1": 0 }],
        "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0, "msgCode": "", "msgDesc": ""
    };
    _pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 0 };

    constructor(private ics: RpIntercomService, private _router: Router, private http: RpHttpService) {
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(x => { })
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        } else {
            //jQuery("#loader").modal('hide');
            this._mflag = false;
            this.search();
        }
    }
    // list sorting part
    changeDefault() {
        for (let i = 0; i < this._col_list.length; i++) {
            this._col_list[i].flag = false;
        }
    }
    addSort(e) {
        for (let i = 0; i < this._col_list.length; i++) {
            if (this._col_list[i].col_no == e) {
                let _tmp_flag = this._col_list[i].flag;
                this.changeDefault();
                this._col_list[i].flag = !_tmp_flag;
                this._sorting._sort_type = !_tmp_flag ? "desc" : "asc";
                this._sorting._sort_col = e;
                break;
            }
        }
    }

    changedPager(event) {

        if (this._userobj.totalCount != 0) {
            this._pgobj = event;
            let current = this._userobj.currentPage;
            let size = this._userobj.pageSize;
            this._userobj.currentPage = this._pgobj.current;
            this._userobj.pageSize = this._pgobj.size;
            if (this._pgobj.current != current || this._pgobj.size != size) {
                this.search();
            }
        }
    }

search() {
    try {
        this._mflag = false;
        this.http.doGet(this.ics.cmsurl + 'service001/getUserListing?searchVal=' + encodeURIComponent(this._userobj.searchText) + '&pagesize=' + this._userobj.pageSize + '&currentpage=' + this._userobj.currentPage + '&operation=deactivate&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(
            data => {
                if (data.msgCode == '0016') {
                    this.sessionAlertMsg = data.msgDesc;
                    this.showMessage(data.msgDesc,false);
                }
                else {
                    if (data != null && data != undefined) {
                        this._pgobj.totalcount = data.totalCount;
                        this._userobj = data;
                        if (this._userobj.totalCount == 0) {
                            this.showMessage("Data not found!",false);
                        }
                    }
                }
                this._mflag = true;
            },
            error => {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            },
            () => { }
        );
    } catch (e) {
        alert(e);
    }

}

    searchKeyup(e: any) {

        if (e.which == 13) { // check enter key
            this._userobj.currentPage = 1;
            this._msghide = true;
            this._recordhide = false;
            this.search();
        }

    }

    Searching() {
        this._userobj.currentPage = 1;
        this._msghide = true;
        this._recordhide = false;
        this.search();
    }

    goto(p) {
        this._router.navigate(['/Deactivate User', 'read', p]);

    }
    goBack() {
        this._router.navigate(['/Deactivate User', 'new']);
    }

    //showMessage() {
    //    jQuery("#sessionalert").modal();
    //    Observable.timer(3000).subscribe(x => {
    //        this.goLogOut();
    //    });
    //}

    goLogOut() {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    }
    showMessage(msg, bool) {
        if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
        if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
        if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "primary", "t3": msg }); }
    }
}