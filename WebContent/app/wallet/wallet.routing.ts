import { ModuleWithProviders }   from '@angular/core';
import { Routes, RouterModule }  from '@angular/router';
import {walletList} from './walletList.component';
import {WalletComponent} from './wallet.component';
import { FrmActivateUserComponent }  from './activateuser.component';
import { FrmActivateUserListComponent } from './activateuser-List.component';
import { FrmDeactivateUserComponent } from './deactivateuser.component';
import { FrmDeactivateUserListComponent } from './deactivateuser-List.component';
import { FrmLockUserComponent } from './lockuser.component'; 
import { FrmLockUserListComponent } from './lockuser-List.component'; 
import { FrmUnlockUserComponent } from './unlockuser.component'; 
import { FrmUnlockUserListComponent } from './unlockuser-List.component'; 
import { ResetPasswordComponent } from './resetpassword.component'; 

const pocRoutes: Routes = [      
    { path: 'walletList',  component: walletList },
    { path: 'walletData',  component: WalletComponent},
    { path: 'walletData/:cmd', component: WalletComponent}, 
    { path: 'walletData/:cmd/:id', component: WalletComponent},

    { path: 'Activate User',  component: FrmActivateUserComponent },  
    { path: 'Activate User/:cmd', component: FrmActivateUserComponent }, 
    { path: 'Activate User/:cmd/:id', component: FrmActivateUserComponent },
    { path: 'ActivateUserList', component: FrmActivateUserListComponent },
    { path: 'Deactivate User',  component: FrmDeactivateUserComponent },  
    { path: 'Deactivate User/:cmd', component: FrmDeactivateUserComponent }, 
    { path: 'Deactivate User/:cmd/:id', component: FrmDeactivateUserComponent },
    { path: 'deactivateuserlist', component: FrmDeactivateUserListComponent },
    { path: 'Lock User',  component: FrmLockUserComponent },  
    { path: 'Lock User/:cmd', component: FrmLockUserComponent }, 
    { path: 'Lock User/:cmd/:id', component: FrmLockUserComponent },
    { path: 'lockuserlist', component: FrmLockUserListComponent },
    { path: 'Unlock User',  component: FrmUnlockUserComponent },  
    { path: 'Unlock User/:cmd', component: FrmUnlockUserComponent }, 
    { path: 'Unlock User/:cmd/:id', component: FrmUnlockUserComponent },
    { path: 'unlockuserlist', component: FrmUnlockUserListComponent },  

    { path: 'Reset Password', component: ResetPasswordComponent},
];

export const walletRouting: ModuleWithProviders = RouterModule.forChild(pocRoutes);