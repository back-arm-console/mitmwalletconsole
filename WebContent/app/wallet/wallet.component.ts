import { Component, enableProdMode } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { RpHttpService } from '../framework/rp-http.service';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpReferences } from '../framework/rp-references';
import { ClientUtil } from '../util/rp-client.util';
declare function isMyanmar(pwd:String) : boolean;
declare function ZgtoUni(pwd: String): string;
declare var jQuery: any;
declare var google: any;
enableProdMode();
@Component({
    selector: 'mobileUser',
    template: `
    <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
         <form class="form-horizontal" ngNoForm>
               <legend>Wallet User</legend>
                    <div class="cardview list-height">      
                         <div class="row col-md-12">
                              <div class="col-md-12" style="margin-bottom: 10px;">
                                   <button class="btn btn-sm btn-primary" id="vh_list_btn" type="button" (click)="goList()">List</button>
                              </div>
                            <div >
                       <div class="row col-md-12">&nbsp;</div>
                              <div class="form-group">
                                <div class="row1 col-md-6">
                                <div class="form-group">						
						                  <label class="col-md-4">User ID </label>						
                                                 <div class="col-md-8">
                                                       <input disabled readonly type="text" [(ngModel)]="_obj.t1" class="form-control input-sm" [ngModelOptions]="{standalone: true}">
                                                 </div>
						             </div>
						
						             <div class="form-group">
						                  <label class="col-md-4">User Name</label>
                                                 <div class="col-md-8">
                                                      <div class="uni">
                                                           <input disabled readonly type="text" [(ngModel)]="_obj.t3" class="form-control input-sm" [ngModelOptions]="{standalone: true}">
                                                      </div> 
                                                  </div>
						             </div>
                          
                                </div>

                                <div class="row1 col-md-6">
                                      <div class="form-group">	
                                           <div class="row col-md-3">
                                                <img src="{{image}}" alt={{img}} style="width:70px;height:70px;"/>
                                           </div>
                                      </div>
                                 </div> 
									 
						
						             <!--<div class="row1 col-md-6">
						             <div class="form-group">  
                                          <label class="col-md-4">Address</label>
                                                 <div class="col-md-8">
                                                      <div class="uni">
                                                           <textarea disabled readonly id="textarea-custom" [(ngModel)]="_obj.t24" class="form-control" [ngModelOptions]="{standalone: true}"></textarea>
                                                      </div>
                                                  </div>
					                 </div>
                                    </div>-->
                                    <div class="row1 col-md-6">
                                    <div class="form-group">		
						                  <label class= "col-md-4">Father Name</label>
                                                 <div class= "col-md-8">
                                                      <div class="uni">                  
                                                           <input disabled readonly type="text" [(ngModel)]="_obj.t20" class="form-control input-sm" [ngModelOptions]="{standalone: true}">
                                                      </div>
                                                 </div>
                                     </div>
                                     </div>

                                    <div class="row1 col-md-6">
                                    <div class="form-group">						
						                  <label class="col-md-4">NRC </label>
                                                 <div class="col-md-8">
                                                      <div class="uni">
                                                           <input disabled readonly type="text" [(ngModel)]="_obj.t21" class="form-control input-sm" [ngModelOptions]="{standalone: true}">
                                                      </div>
                                                 </div>
                                     </div>
                                     </div>
                                    
                                    <div class="row1 col-md-6">
					                 <div class="form-group">	
					                       <label class="col-md-4">Date Of Birth</label>
                                                  <div class="col-md-8">
                                                       <div class="uni">
                                                             <input disabled readonly type="text" [(ngModel)]="_obj.t25" class="form-control input-sm" [ngModelOptions]="{standalone: true}">
                                                       </div>
                                                  </div>	
                                     </div>
                                     </div>
                                    
                                    <div class="row1 col-md-6">
						             <div class="form-group">
                                          <label class="col-md-4">Register date</label>
                                                 <div class="col-md-8">
                                                       <input disabled readonly type="text" [(ngModel)]="_obj.createdDate" class="form-control input-sm" [ngModelOptions]="{standalone: true}">
                                                 </div>
                                     </div>
                                     </div>	

                                    <!-- <div class="row1 col-md-6">
					                 <div class="form-group">	
					                       <label class="col-md-4">Date Of Birth</label>
                                                  <div class="col-md-8">
                                                       <div class="uni">
                                                             <input disabled readonly type="text" [(ngModel)]="_obj.t25" class="form-control" [ngModelOptions]="{standalone: true}">
                                                       </div>
                                                  </div>	
                                     </div>
                                     </div>-->	
                                     <div class="row1 col-md-6">
						             <div class="form-group">  
                                          <label class="col-md-4">Address</label>
                                                 <div class="col-md-8">
                                                      <div class="uni">
                                                           <textarea disabled readonly id="textarea-custom" [(ngModel)]="_obj.t24" class="form-control input-sm" [ngModelOptions]="{standalone: true}"></textarea>
                                                      </div>
                                                  </div>
					                 </div>
                                    </div>
                                    
                                    <div class="row1 col-md-6">
                                     <div class="form-group">						
						                  <label class="col-md-4">AccNumber </label>
                                                 <div class="col-md-8">                             
                                                       <input disabled readonly type="text" [(ngModel)]="_obj.accNumber" class="form-control input-sm" [ngModelOptions]="{standalone: true}">                            
                                                 </div>
                                     </div>
                                     
                                     </div>
                        
						
						        <!--</div>-->
						    </div>
                     </div>
               </div>
               </div>
          </form>
      </div>
     </div>
</div>
 
  <div [hidden]="_mflag">
    <div  id="loader" class="modal" ></div>
  </div>
`
})

export class WalletComponent {

    subscription: Subscription;
    sub: any
    _util: ClientUtil = new ClientUtil();
    imageLink: any;
    image = "";
    _mflag = false;

    _obj = this.getDefaultObj();
    getDefaultObj() {
        return {
            "syskey": "", "autokey": "", "t1": "", "t3": "","t20": "", "t21": "", "t24": "", "t25": "","createdDate": "","t16":"","accNumber":"","state": false
        };
    }

    constructor(private ics: RpIntercomService, private _router: Router, private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences) {
        this.subscription = ics.rpbean$.subscribe(x => { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        } else {
            this.imageLink = this.ics._profileImage1;
            this._obj = this.getDefaultObj();
        }
    }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            let cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
              var _List = this.ics.getBean().walletdata;
              this._mflag=true;
              this._obj.t1=_List.t1;
              this._obj.t3=_List.t3;
              this._obj.t20=_List.t20;
              this._obj.t21=_List.t21;
              this._obj.t25=_List.t25;
              this._obj.t24=_List.t24;
              this._obj.createdDate=_List.createdDate;
              this._obj.t16=_List.t16;
              this._obj.accNumber=_List.accNumber;
              this.image = this.imageLink + _List.t16;
            }
        });
    }
   
    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    goList() {
        this._router.navigate(['/walletList']);
    }

    showMsg(msg, bool) {
        if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
        if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
        if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
    }

    logout() {
        this._router.navigate(['/login']);
    }

}