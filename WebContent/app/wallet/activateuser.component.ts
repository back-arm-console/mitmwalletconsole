import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { enableProdMode } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpInputComponent } from '../framework/rp-input.component';
import { RpHttpService } from '../framework/rp-http.service';
import { RpBean } from '../framework/rp-bean';
import { RpReferences } from '../framework/rp-references';
import { ClientUtil } from '../util/rp-client.util';
import { Observable } from 'rxjs/Rx';
declare var jQuery: any;
enableProdMode();
@Component({
  selector: 'activateuser',
  template: `
  <div class="container-fluid">
  <div class="row clearfix">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0"> 
    <form class= "form-horizontal"> 
    <!-- Form Name -->
      <legend>Activate User</legend>
      <div class="cardview list-height">
        <div class="row col-md-12">    
          <button class="btn btn-sm btn-primary" type="button"  (click)="goList()">List</button>
          <button class="btn btn-sm btn-primary" disabled id="myactivate"  type="button"  (click)="confirmActivate()">Activate</button>
        </div>
           
        <div class="row col-md-12">&nbsp;</div>
                <div class="form-group">
        <div class="col-md-8">
          <div class="form-group">
            <rp-input [(rpModel)]="_obj.t1" rpRequired ="true" rpType="text" rpClass="col-md-4" rpLabelClass ="col-md-2 control-label" rpLabel="Login ID" rpReadonly="true"></rp-input>
          </div>

          <div class="form-group">
            <rp-input [(rpModel)]="_obj.t3" rpRequired ="true" rpType="text" rpClass="col-md-4" rpLabelClass ="col-md-2 control-label" rpLabel="Name" rpReadonly="true"></rp-input>
          </div>

          <div class="form-group">
            <rp-input  rpType="text" rpClass="col-md-4" rpLabelClass ="col-md-2 control-label" rpLabel="NRC" [(rpModel)]="_obj.t21"  rpReadonly="true"></rp-input>
          </div>

          <!--<div class="form-group">
            <rp-input  rpType="text" rpLabel="Phone No." [(rpModel)]="_obj.t4"  rpReadonly="true"></rp-input>   
          </div>-->

          <div class="form-group">
            <label class="col-md-2">Registered Date</label>
            <div class="col-md-4"> 
              <input [(ngModel)]="_obj.createdDate" Readonly type="date" class="form-control input-md" [ngModelOptions]="{standalone: true}"/>
            </div>
          </div>

        </div> 

        <div class="col-md-2">
          <div [hidden]="msghide">
          <h3 align="right"><b>{{msg}}</b></h3>
          </div>
        </div>
      </div>
      </div>
    </form>
  </div>
  </div>
  </div>

  <div id="activateconfirm" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
          <p>Do you want to Activate?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-primary" (click)="goActivate()">Yes</button>
          <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
  
  <div id="sessionalert" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <p>{{sessionAlertMsg}}</p>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>

<div [hidden] = "_mflag">
<div class="modal" id="loader"></div>
</div>
    
  `
})
export class FrmActivateUserComponent {
  // RP Framework 
  subscription: Subscription;
  sub: any;
  _returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };

  msghide: boolean;
  messagehide: boolean;
  _obj = this.getDefaultObj();
  sessionAlertMsg = "";
  registeredDate: String;
  _key = ""; msg: String;
  _mflag = false;
  _dates = { "date1": "", "date2": "" };
  _util: ClientUtil = new ClientUtil();
  constructor(private ics: RpIntercomService, private _router: Router, private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences) {
    this.subscription = ics.rpbean$.subscribe(x => { })
    if (!ics.getRole() || ics.getRole() == 0) {
      this._router.navigate(['/login']);
    } else {
      this._mflag = true;
      this.checkSession();
      jQuery("#myactivate").prop("disabled", true);
      this.msghide = true;
      this._obj = this.getDefaultObj();
    }
  }
  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let cmd = params['cmd'];
      if (cmd != null && cmd != "" && cmd == "read") {
        let id = params['id'];
        this._key = id;
        this.goReadBySyskey(id);
      }
    });
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  getDefaultObj() {
    return { "createdDate": "", "t1": "", "t3": "", "t21": "", "t41": "" };
  }

  goReadBySyskey(p) {
    try {
      this._mflag = false;
      let url: string = this.ics.cmsurl + 'service001/getUserProfileData?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID +'&operation=activate';
      let json: any = p;

      this.http.doPost(url, json).subscribe(
        data => {
          if (data.msgCode == '0016') {
            this.sessionAlertMsg = data.msgDesc;
            this.showMsg(data.msgDesc,false);
          }
          else {
            this._obj = data;
            this.registeredDate = this._obj.createdDate;
            this._obj.createdDate = this.registeredDate.substring(0, 4) + "-" + this.registeredDate.substring(4, 6) + "-" + this.registeredDate.substring(6, this.registeredDate.length);
            jQuery("#myactivate").prop("disabled", false);
          }
          this._mflag = true;
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
        },
        () => { }
      );

    } catch (e) {
      alert(e);
    }
  }

  goList() {
    this._router.navigate(['/ActivateUserList']);
  }
  showMessage() {
    jQuery("#sessionalert").modal();
    Observable.timer(3000).subscribe(x => {
      this.goLogOut();
    });
  }

  goLogOut() {
    jQuery("#sessionalert").modal('hide');
    this._router.navigate(['/login']);
  }
    
  confirmActivate() {
    jQuery("#activateconfirm").modal('show');
  }

  showMessageAlert(msg) {
    // if (bool == "true") { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
    // if (bool == "false") { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
    // if (bool == "undefined") { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
   // if (bool == "true") { this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg }); }
   // if (bool == "false") { this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg }); }
   // if (bool == "undefined") { this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg }); }
   this.ics.sendBean({ "t1": "rp-msg", "t2": "Success", "t3": msg });
  }

  messagealert() {
    this.messagehide = false;
    setTimeout(() => this.messagehide = true, 3000);
  }

  goActivate() {
    try {
      jQuery("#activateconfirm").modal('hide');
      this._mflag = false;
      let url: string = this.ics.cmsurl + 'service001/activatedeactivateUser?k=activate&id=' + this.ics._profile.userID + '&sessionID=' + this.ics._profile.sessionID;
      let json: any = this._key;
      this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
      this.http.doPost(url, json).subscribe(
        data => {
          if (data.msgCode == '0016') {
            this.sessionAlertMsg = data.msgDesc;
            this.showMsg(data.msgDesc,false);
          }
          else {
            this._returnResult = data;
            if (this._returnResult.state) {
              this.msg = "Activated";
              this.msghide = false;
              jQuery("#myactivate").prop("disabled", true);
            }
            this.showMsg(data.msgDesc,true);
          }
          this._mflag = true;
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
          else {

          }
        },
        () => { }
      );
    } catch (e) {
      alert(e);
    }
  }

  // about session validation
  sessionTimeoutMsg = "";
  _sessionObj = this.getSessionObj();
  getSessionObj() {
    return { "sessionID": "", "userID": "" };
  }

  checkSession() {
    try {
      let url: string = this.ics._apiurl + 'service001/checkSessionTime';

      this._sessionObj.sessionID = this.ics._profile.sessionID;
      this._sessionObj.userID = this.ics._profile.userID;
      let json: any = this._sessionObj;

      this.http.doPost(url, json).subscribe(
        data => {
          if (data != null) {
            if (data.code == "0016") {
              this.showMsgAlert(data.desc);
              this.logout();
            }

            if (data.code == "0014") {
              this.showMsgAlert(data.desc);
            }
          }
        },
        error => {
          if (error._body.type == "error") {
            alert("Connection Timed Out.");
          }
        },
        () => { }
      );
    } catch (e) {
      alert("Invalid URL.");
    }
  }

  showMsgAlert(msg) {
    this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
  }

  logout() {
    this._router.navigate(['/login']);
  }
  showMsg(msg, bool) {
    if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
    if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
    if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
  }

}