import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { enableProdMode } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpInputComponent } from '../framework/rp-input.component';
import { RpHttpService } from '../framework/rp-http.service';
import { RpBean } from '../framework/rp-bean';
import { RpReferences } from '../framework/rp-references';
import { ClientUtil } from '../util/rp-client.util';
import { Observable } from 'rxjs/Rx';
declare var jQuery: any;
enableProdMode();
@Component({
  selector: 'resetpwd',
  template: `
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0"> 
        <form class= "form-horizontal"> 
          <!-- Form Name -->
          <legend>Reset Password</legend>
          <div class="cardview list-height">
            <div class="row col-md-12">   
              <button class="btn btn-sm btn-primary" type="button" disabled id="myreset" (click)="goReset()" >Reset</button>
              <button class="btn btn-sm btn-primary" type="button" id="myclear" (click)="goClear()" >Clear</button>  
            </div>
            <div class="row col-md-12">&nbsp;</div>
              <div class="form-group">
              <div class="col-md-8">
              <div class="form-group">
                <label class="col-md-2"> Login ID <font class="mandatoryfont" >*</font> </label>
                <div class="col-md-4">
                      <div class="input-group">
                        <input type="text" [(ngModel)]=_sessionObj.loginID [ngModelOptions]="{standalone: true}" required="true"  class="form-control input-sm">
                        <span class="input-group-btn">
                          <button class="btn btn-sm btn-primary" type="button" (click)="goCheck()"><i class="glyphicon glyphicon-ok-sign"></i> Check </button>
                        </span>
                      </div>
                </div>
                <div class="col-md-2">
                  <label style="color:green;font-size:20px">&nbsp;{{isvalidate}}</label>   
                </div>
              </div>  
              <div class="form-group">
                <rp-input [(rpModel)]="_obj.t3" rpRequired ="true" rpType="text" rpClass="col-md-4" rpLabelClass ="col-md-2 control-label" rpLabel="Name" rpReadonly="true"></rp-input>
              </div>
              <div class="form-group">
                <rp-input  rpType="text" rpClass="col-md-4" rpLabelClass ="col-md-2 control-label" rpLabel="NRC" [(rpModel)]="_obj.t21"  rpReadonly="true"></rp-input>
              </div>
            </div>
          </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div id="sessionalert" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <p>{{sessionAlertMsg}}</p>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>

  <div [hidden] = "_mflag">
    <div class="modal" id="loader"></div>
  </div>
  `
})

export class ResetPasswordComponent{

  subscription: Subscription;
  _returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
  _obj = this.getDefaultObj();
  sessionAlertMsg = "";
  isvalidate = "";
  _mflag = false;
  _util: ClientUtil = new ClientUtil();

  constructor(private ics: RpIntercomService, private _router: Router, private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences) {
    this.subscription = ics.rpbean$.subscribe(x => { })
    if (!ics.getRole() || ics.getRole() == 0) {
      this._router.navigate(['/login']);
    } else {
      this._mflag = true;
      this.checkSession();
      jQuery("#myreset").prop("disabled", true);
      this._obj = this.getDefaultObj();
    }
  }

  getDefaultObj() {
    return { "createdDate": "", "t1": "", "t3": "", "t21": "", "t41": "","sessionID":"","userID":"" };
  }

  showMessage() {
    jQuery("#sessionalert").modal();
    Observable.timer(3000).subscribe(x => {
      this.goLogOut();
    });
  }

  goLogOut() {
    jQuery("#sessionalert").modal('hide');
    this._router.navigate(['/login']);
  }  

  goCheck() {
    try {
      this._mflag = false;
      let url: string = this.ics.cmsurl + 'service001/getUserNameAndNrc';
      let json: any =this._sessionObj;
      //this.http.doGet(this.ics.cmsurl + 'service001/getUserNameAndNrc?id=' + this._obj.t1 + '&sessionID=' + this.ics._profile.sessionID + '&userIDForSession=' + this.ics._profile.userID).subscribe(
      this.http.doPost(url,json).subscribe(
        data => {
          if (data.msgCode == '0016') {
            this.sessionAlertMsg = data.msgDesc;
            this.showMsg(data.msgDesc,false);
          } else {
            if (data.t1 == '') {
              this.isvalidate = "";
              jQuery("#myreset").prop("disabled", true);
              this.showMsg("Invalid User ID",false);
            } else {
              this._obj = data;
              this.isvalidate = "Validate";
              jQuery("#myreset").prop("disabled", false);
            }
          }

          this._mflag = true;
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
        },
        () => { }
      );
    } catch (e) {
      alert(e);
    }
  }

  goClear() {
    this.isvalidate = "";
    this._obj = this.getDefaultObj();
  }

  goReset() {
    try {
      this._mflag = false;
      //this.http.doGet(this.ics.cmsurl + 'service001/resetPasswordbyId?userId=' + this._obj.t1 + '&adminId=' + this.ics._profile.userID + '&sessionID=' + this.ics._profile.sessionID).subscribe(
          let url: string = this.ics.cmsurl + 'service001/resetPasswordbyId';
          let json: any =this._sessionObj;        
          this.http.doPost(url,json).subscribe(
            data => {
          if (data.msgCode == '0016') {
            this.sessionAlertMsg = data.msgDesc;
            this.showMsg(data.msgDesc,false);
          } else {
            this._returnResult.msgDesc = data.msgDesc;
            this._returnResult.state = data.state;
            jQuery("#myreset").prop("disabled", true);
            this.showMsg(data.msgDesc,true);
          }
          this._mflag = true;
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
        },
        () => { }
      );
    } catch (e) {
      alert(e);
    }
  }

  showMessageAlert(msg) {
    this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
  }

  // about session validation
  sessionTimeoutMsg = "";
  _sessionObj = this.getSessionObj();
  getSessionObj() {
    return { "sessionID": this.ics._profile.sessionID, "userID": this.ics._profile.userID,"loginID":"" };
  }

  checkSession() {
    try {
      let url: string = this.ics._apiurl + 'service001/checkSessionTime';

      //this._sessionObj.sessionID = this.ics._profile.sessionID;
      //this._sessionObj.userID = this.ics._profile.userID;
      let json: any = this._sessionObj;

      this.http.doPost(url, json).subscribe(
        data => {
          if (data != null) {
            if (data.code == "0016") {
              this.showMsgAlert(data.desc);
              this.logout();
            }

            if (data.code == "0014") {
              this.showMsgAlert(data.desc);
            }
          }
        },
        error => {
          if (error._body.type == "error") {
            alert("Connection Timed Out.");
          }
        },
        () => { }
      );
    } catch (e) {
      alert("Invalid URL.");
    }
  }

  showMsgAlert(msg) {
    this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
  }

  logout() {
    this._router.navigate(['/login']);
  }
  showMsg(msg, bool) {
    if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
    if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
    if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
  }


}