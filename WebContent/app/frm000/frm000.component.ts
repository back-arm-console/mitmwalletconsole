import { Component, enableProdMode } from '@angular/core';
import { Router,ActivatedRoute} from '@angular/router'; 
import { Subscription }   from 'rxjs/Subscription';
import {RpHttpService} from '../framework/rp-http.service';
import { RpIntercomService } from '../framework/rp-intercom.service';
enableProdMode(); 
@Component({
  template:  `
  <!--<div class="container">
      <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
          <fieldset>  
            <div align="center" class="form-group" >
              <legend>  <h1>{{_welcometext}}</h1> </legend>    
              <img [src]=_imgurl style = "width:350px; height:250px">
            </div>
          </fieldset>
        </div>
      </div>
    </div>-->
  `
})
export class Frm000Component {  
  subscription: Subscription;
  sub :any
  _imgurl = "";
  _welcometext = "";
  constructor(private ics: RpIntercomService, private route: ActivatedRoute,private _router: Router, private http: RpHttpService) {
    this.subscription = ics.rpbean$.subscribe(x => { })
    if (!this.ics.getRole() || this.ics.getRole() == 0) this._router.navigate(['/login']);  
    else {
      this._imgurl = ics._imglogo;
      this._welcometext=this.ics._welcometext;
      //this.getwelcometext();  
      this.ics.confirmUpload(true);
    }
  }
  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let cmd = params['cmd'];
      if (cmd != null && cmd != "" && cmd == "read") {
        let userID = params['p1'];
      }
    });
  }

/*   getwelcometext() {
    this.http.doGet('json/config.json?random=' + Math.random()).subscribe(
      data => {
        this._welcometext = data.welcometext;
      },
      error => alert(error),
      () => { }
    );
  } */
}
