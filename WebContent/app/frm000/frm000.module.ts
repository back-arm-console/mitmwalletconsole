import { NgModule, CUSTOM_ELEMENTS_SCHEMA }       from '@angular/core';
import { FormsModule }    from '@angular/forms';
import { CommonModule }   from '@angular/common';
import { RpInputModule } from '../framework/rp-input.module';

import { Frm000Component }       from './frm000.component';
//import { SearchComponent }       from './Frmsearch.component';
import { Frm000Routing }     from './frm000.routing';

import { RpHttpService } from '../framework/rp-http.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RpInputModule,
    Frm000Routing
  ],
  exports : [],
  declarations: [
     Frm000Component,
     //SearchComponent 
  ],
  providers: [
    RpHttpService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Frm000Module {}