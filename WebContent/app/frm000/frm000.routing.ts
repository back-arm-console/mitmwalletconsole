import { ModuleWithProviders }   from '@angular/core';
import { Routes, RouterModule }  from '@angular/router';

import { Frm000Component }       from './frm000.component';
//import { SearchComponent }       from './Frmsearch.component';

const Frm000Routes: Routes = [
  { path: '000',  component: Frm000Component },
  { path: '000/:cmd',  component: Frm000Component },  
  { path: 'Welcome',  component: Frm000Component },
  //{ path: 'command',  component: SearchComponent },
  //{ path: 'command/:cmd',  component: SearchComponent }  
];

export const Frm000Routing: ModuleWithProviders = RouterModule.forChild(Frm000Routes);