import { Injectable } from '@angular/core';
import { IMyDpOptions } from 'mydatepicker';
declare var AesUtil: any;
declare var CryptoJS: any;
@Injectable()
export class ClientUtil {
  constructor() {
  }
  _datepickerOpts = {
    showWeekNumbers: false,
    todayBtnTxt: 'Today',
    placeholder:'From',
    dateFormat: 'dd/mm/yyyy',
    firstDayOfWeek: 'mo',
    sunHighlight: true,
    height: '34px',
    width: '260px',
    inline: false,
    disableUntil: { year: 1950, month: 0, day: 0 },
    alignSelectorRight: false,
    editableDateField: false,
    showClearDateBtn: false
  }
  changeDatefromat(dt) {
    if (dt != undefined) { return this.changeStringtoDate(this.changeDatetoString(dt)).substring(0, 10); }
  }
  
  getDatePicker() {
    return this._datepickerOpts;
  }
  //20160526 2:09pm YMK...
  changeDatetoString(dt) {
    if (dt != null) {
      var datepattern = /(\d{4})?[- ]?(\d{2})?[- ]?(\d{2})/;
      return dt.replace(datepattern, '$1$2$3');
    } else {
      return "";
    }
  }
  //2018-09-08
  getDatePickerDate(dt) {
    if (dt != null) {
      var datestring = dt.date.year + "-" + ("0" + (dt.date.month)).slice(-2) + "-" + ("0" + dt.date.day).slice(-2);
      return datestring;
    } else {
      return "";
    }
  }
  //20180908
  getDatePickerDateymd(dt) {
    if (dt != null) {
      var datestring = dt.date.year + ("0" + (dt.date.month)).slice(-2) + ("0" + dt.date.day).slice(-2);
      return datestring;
    } else {
      return "";
    }
  }
  //dmy
  getDatePickerDatebydmy(dt) {
    if (dt != null) {
      var datestring = ("0" + dt.date.day).slice(-2)+ "-" + ("0" + (dt.date.month)).slice(-2) + "-" + dt.date.year;
      return datestring;
    } else {
      return "";
    }
  }
  getDate(dt) {
    if (dt != null) {
      var datestring = dt.date.year + ("0" + (dt.date.month)).slice(-2) + ("0" + dt.date.day).slice(-2);
      return datestring;
    } else {
      return "";
    }
  }

  changestringtodateobject(dt: any) {
    if (dt != null && dt != "1900-01-01 00:00:00.0" && dt != "") {
      return {
        date: {
          year: Number(dt.slice(0, 4)),
          month: Number(dt.slice(5, 7)),
          day: Number(dt.slice(8, 10))
        }
      };
    } else {
      return null;
    }

  }
  changeStringDashToDateObj(dt: any) {
    if (dt != null && dt != "19000101" && dt != "") {
      return {
        date: {
          year: Number(dt.slice(0, 4)),
          month: Number(dt.slice(5, 7)),
          day: Number(dt.slice(8, 10))
        }
      };
    } else {
      return null;
    }
  }
//version history
  changestringtodateobj(dt: any) {
    if (dt != null && dt != "19000101 00:00:00.0" && dt != "") {
      return {
        date: {
          year: Number(dt.slice(0, 4)),
          month: Number(dt.slice(4, 6)),
          day: Number(dt.slice(6, 8))
        }
      };
    } else {
      return null;
    }

  }

  changeStringtoDateFromDB(dt) {
    var pattern = /(\d{4})(\d{2})(\d{2})/;
    return dt.replace(pattern, '$3/$2/$1');
  }

  //for YYYY-MM-DD fomat
  changeDateFromYYYYDashMMDashDDtoYYYYMMDD(dt) {
    if (dt != null) {
      dt = dt.substring(0, 10);
      var datepattern = /(\d{4})?[- ]?(\d{2})?[- ]?(\d{2})/;
      return dt.replace(datepattern, '$1$2$3');
    } else {
      return "";
    }
  }

  //for DD-MM-YYYY fomat
  changeDatetoStringDMY(dt) {
    if (dt != null) {
      dt = dt.substring(0, 10);
      var datepattern = /(\d{4})?[- ]?(\d{2})?[- ]?(\d{2})/;
      return dt.replace(datepattern, '$3/$2/$1');
    } else {
      return "";
    }
  }
  changeStringtoDate(dt) {
    if (dt != null) {
      var pattern = /(\d{4})(\d{2})(\d{2})/;
      return dt.replace(pattern, '$1-$2-$3');
    } else {
      return "";
    }
  }

  changeStringtoDateTime(dt) {
    if (dt != null) {
      var pattern = /(\d{4})(\d{2})(\d{2})/;
      dt = dt.substring(0, 10);
      return dt.replace(pattern, '$1-$2-$3');
    } else {
      return "";
    }
  }
  changeStringTimetoDate(dt) {
    if (dt != null) {
      var pattern = /(\d{4})(\d{2})(\d{2})/;
      return dt.replace(pattern, '$1/$2/$3');
    } else {
      return "";
    }
  }
  changeStringtoDateDDMMYYYY(dt) {
    if (dt != null) {
      var pattern = /(\d{4})(\d{2})(\d{2})/;
      return dt.replace(pattern, '$3/$2/$1');
    } else {
      return "";
    }
  }
  getTodayDate() {
    var d = new Date();
    var datestring = d.getFullYear() + "-" + ("0" + (d.getMonth() + 1)).slice(-2) + "-" + ("0" + d.getDate()).slice(-2);
    return datestring;
  }

  getTodayTime() {
    var d = new Date();
    var hours = d.getHours();
    var minutes = d.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    var minute = (minutes < 10 ? '0' + minutes : minutes);
    var strTime = hours + ':' + minute + ' ' + ampm;
    return strTime;

  }
  
  getCurrentYear() {
    var d = new Date();
    var datestring = d.getFullYear();
    return datestring;
  }
  validateEmail(d) {
    var pattern = /[A-Z0-9._%+-]+@[A-Z0-9-]+.+.[A-Z]{2,4}/igm;
    return pattern.test(d);
  };
  validateIR(d) {
    var pattern = /IR(\d{2})(\d{2})/;
    return pattern.test(d);
  };
  compareStringLength(str, strln) {
    if (str.length <= strln) {
      return true;
    } else {
      return false;
    }
  }
  checkNumber(num) {
    return isNaN(num);
  }
  changeArray(data, obj, num) {
    let arr = [];
    if (data instanceof Array) {
      arr = data;
      return arr;
    } else {
      if (num == 0) {
        arr[0] = obj;
        arr[1] = data;
        return arr;
      }
      if (num == 1) {
        arr[0] = data;
        arr[1] = obj;
        return arr;
      }
    }
  }
  currencyFormat(p) {
    p.toFixed(2);
  }  
  //money format #,###.## - ymk 20160908
  formatMoney(n) {
    return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
  }

  validateUrl(d) {
    var pattern = /(http:\/\/|https:\/\/)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?$/;
    return pattern.test(d);
  };    

  validateLanguage(d) {
    var IS_UNICODE_MY = /[ဃငဆဇဈဉညတဋဌဍဎဏဒဓနဘရဝဟဠအ]်|ျ[က-အ]ါ|ျ[ါ-း]|[^\1031]စ် |\u103e|\u103f|\u1031[^\u1000-\u1021\u103b\u1040\u106a\u106b\u107e-\u1084\u108f\u1090]|\u1031$|\u100b\u1039|\u1031[က-အ]\u1032|\u1025\u102f|\u103c\u103d[\u1000-\u1001]/;
    return IS_UNICODE_MY.test(d);
  };
  validateLang(d) {
    var IS_ZAWGYI = /|\u0020[\u103b\u107e-\u1084][က-အ]|\u0020\u1031[က-အ\u1040]|\u1031\u1005\u103A/;

    return IS_ZAWGYI.test(d);
  };
  validateEng(d) {
    var IS_Eng = /^[0-9a-zA-Z\s\r\n~@!#\$\^&*\'\"/;`%()_+=\'%[\]{\}|\\,.?: -]*$|[e'gal]/;
    return IS_Eng.test(d);
  };  

  convertToArray(aObject: any)
  {
      let l_Object;
      l_Object = aObject;

      if (!(l_Object instanceof Array)) 
      {
          let m = [];
          m[0] = l_Object;
          l_Object = m;
      } 
      
      return l_Object;
  }

  getIvs() {
    return CryptoJS.lib.WordArray.random(128 / 8).toString(CryptoJS.enc.Hex);
  }

  getEncryptText(iv, salt, dm, password) {
    var plaintext = password;
    var aesUtil = new AesUtil(128, 1000);
    password = aesUtil.encrypt(salt, iv, '!@#$29!@#$Gp**&*', plaintext);
    return password;
  }
  thousand_sperator(num) {
    if (num != "" && num != undefined && num != null) {
      num = num.replace(/,/g, "");
    }   
    var parts = num.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts;// return 100,000.00
  }
  convertDate7(aDate: any)
    {
        var l_DateString = '';
  
        if (aDate != null)
        {
            l_DateString = aDate.slice(6, 10) + '-' + aDate.slice(3, 5) + '-' + aDate.slice(0, 2);
        }
  
        return l_DateString;
    }

  convertDBDateTime1(m: any) {
    // DD/MM/YYYY hh:mm:ss from yyyy-mm-dd hh:mm:ss
    let d1 = m.split(" ");
    var returnDate = "";
    if (d1[0] != null) {
      returnDate = d1[0].slice(8, 10) + "/" + d1[0].slice(5, 7) + "/" + d1[0].slice(0, 4);
    }

    return returnDate + " " + d1[1];
  }

  myDatePickerOptions1: IMyDpOptions = {
    // other options...
    showWeekNumbers: false,
    todayBtnTxt: 'Today',
    dateFormat: 'dd/mm/yyyy',
    firstDayOfWeek: 'mo',
    sunHighlight: true,
    height: '34px',
    //width: '160px',
    inline: false,
    disableUntil: { year: 1800, month: 7, day: 8 },
    alignSelectorRight: false,
    editableDateField: false,
    showClearDateBtn: false
  };

  addDayToDate(startDate, count) {
    let dateObj = new Date(this.convertDate8(startDate));
    dateObj.setDate(dateObj.getDate() + count);
    var l_DateFormat = dateObj.getFullYear() + ("0" + (dateObj.getMonth() + 1)).slice(-2) + ("0" + dateObj.getDate()).slice(-2);
    return l_DateFormat;
  }

  convertDate8(m: any) {
    // YYYY-MM-DD
    //
    if (m != null) {
      var returnDate = m.slice(0, 4) + "-" + m.slice(4, 6) + "-" + m.slice(6, 8);
      return returnDate;
    }
  }

  changestringtodateobject1(m: any) {
    // convert 'YYYYMMDD' to date object
    //
    if (m != null && m != "19000101" && m != "") {
      return {
        date: {
          year: Number(m.slice(0, 4)),
          month: Number(m.slice(4, 6)),
          day: Number(m.slice(6, 8))
        }
      };
    }
    else {
      return null;
    }
  }

  convertDatetoYYYYMMDD(m: any) {   //yyyymmdd
    if (m != null) {
      var returnDate = m.date.year + ("0" + m.date.month).slice(-2) + ("0" + m.date.day).slice(-2);
      return returnDate;
    }
  }

  getResponseMsg(code) {
    var desc = "";
    if (code == "001") {
      desc = "Saved Successfully!";
    }
    else if (code == "005") {
      desc = "Updated Successfully!";
    } else if (code == "07-048") {
      desc = "NRC already exists!";
    }
    else if (code == "010") {
      desc = "Saved fail!";
    }
    else if (code == "011") {
      desc = "Reversed successfully!";
    }
    else if (code == "015") {
      desc = "Post Fail!";
    }
    else if (code == "18-001") {
      desc = "Processing date must be less than equal today date!";
    }
    else if (code == "07-056") {
      desc = "Invalid Drawing Accnumber!";
    }
    else if (code == "20-001") {
      desc = "This Account is Link Account!";
    }
    else if (code == "22-022") {
      desc = "Duplicate Fax Number!";
    }
    else if (code == "22-023") {
      desc = "From Branch and From Account Number does not match!";
    }
    else if (code == "22-021") {
      desc = "Transaction is already posted!";
    }
    else if (code == "40-002") {
      desc = "Denomination already exist!";
    }
    else if (code == "11-006") {
      desc = "Amount is less than Minimum Opening Balance.";
    } else if (code == "10-001") {
      desc = "Invalid Account Number!";
    } else if (code == "10-002") {
      desc = "Allow only Current Account!";
    } else if (code == "10-003") {
      desc = "Account status must be 'Active'!";
    } else if (code == "10-015") {
      desc = "Maximum number of cheque is 50!";
    } else if (code == "10-004") {
      desc = "Invalid No: of Check!";
    } else if (code == "10-085") {
      desc = "Check series is out of range";
    } else if (code == "10-014") {
      desc = "Cheque number already exists";
    } else if (code == "05-000") {
      desc = "No records found!";
    } else if (code == "002") {
      desc = "Deleted successfully!";
    } else if (code == "10-007") {
      desc = "Cheque book already in used!";
    } else if (code == "20-008") {
      desc = "Account Status changed from 'Active' to 'Closed Pending'";
    } else if (code == "05-005") {
      desc = "Blank";
    } else if (code == "07-101") {
      desc = "Account Number";
    } else if (code == "07-123") {
      desc = "Cheque Number";
    } else if (code == "21-015") {
      desc = "Only Allow Local Branch Code!";
    } else if (code == "20-011") {
      desc = "Account's Ref No Status changed from 'Active' to 'Closing Pending'.";
    } else if (code == "10-008") {
      desc = "Cheque is paid cheque!";
    } else if (code == "20-012") {
      desc = "Account Status changed from 'Active' to 'Closed'";
    } else if (code == "20-014") {
      desc = "Account Status changed from 'New' to 'Closed'";
    } else if (code == "50-034") {
      desc = "Already Approve Cheque Book !";
    } else if (code == "026") {
      desc = "Error in currency rate!";
    } else if (code == "10-008") {
      desc = "Cheque is paid cheque!";
    } else if (code == "10-009") {
      desc = "Cheque is canceled successfully!";
    } else if (code == "10-010") {
      desc = "Cheque is stopped successfully!";
    } else if (code == "10-011") {
      desc = "Cheque is reversed successfully!";
    } else if (code == "10-012") {
      desc = "Select Cheque Number!";
    } else if (code == "20-019") {
      desc = "Account's Ref No. Status changed from 'New' to 'Closed'";
    } else if (code == "10-068") {
      desc = "Allow only Local Account";
    }
    else if (code == '07-135') {
      desc = "Parent's Account Type and Child's Account Type must be equal!";
    }
    else if (code == "49-001") {
      desc = "One of Child Account must be Savings Account and one of Child Account must be Call Account.";
    }
    else if (code == "07-081") {
      desc = "OD account will not be allowed!";
    }
    else if (code == "20-005") {
      desc = "This Account is Back Account of Hire Purchase";
    }
    else if (code == "20-002") {
      desc = "This Account is Back Account of Loan Account";
    }
    else if (code == "21-004") {
      desc = "Schedule Name already exist!";
    }
    else if (code == "29-004") {
      desc = "This schedule name is already posted.Please change the schedule name.";
    }
    else if (code == "15-008") {
      desc = "Parent Account must be Active!";
    }
    else if (code == "05-017") {
      desc = "Account Number Not Found!";
    } else if (code == "31-003") {
      desc = "Transactions Not Exist";
    }
    else if (code == "31-002") {
      desc = "All Transactions Exist!";
    } else if (code == "003") {
      desc = "Posted successfully!";
    }
    else if (code == "001") {
      desc = "Reversed successfully!";
    }
    else if (code == "50-038") {
      desc = "Allow Only Customer Account!";
    }
    else if (code == "50-039") {
      desc = "Allow Only GL Account!";
    } else if (code == "31-001") {
      desc = "File Already Saved!";
    } else if (code == "21-007") {
      desc = "Can't delete, status is posted!";
    } else if (code == "19-001") {
      desc = "First, save the transaction and then post!";
    } else if (code == "46-001") {
      desc = "Please Choose At Least one Status!";
    }
    else {
      desc = code;
    }
    return desc;
  }

  changeDatetoString1(dt) {
    var datepattern = /(\d{4})?[- ]?(\d{2})?[- ]?(\d{2})/;
    return dt.replace(datepattern, '$3/$2/$1');
  }

  getTodayDate2() {
    var l_Date = new Date();
    var l_DateFormat = l_Date.getFullYear() + ("0" + (l_Date.getMonth() + 1)).slice(-2) + ("0" + l_Date.getDate()).slice(-2);

    return l_DateFormat;
  }

}
