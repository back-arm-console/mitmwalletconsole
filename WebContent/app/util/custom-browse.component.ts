import { Component, Output, EventEmitter, enableProdMode, ViewChild, ElementRef } from '@angular/core';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { Subscription } from 'rxjs/Subscription';
enableProdMode();
@Component({
    selector: 'custom-browse',
    template: `  
    <input type="file" (click)="resetValue()" (change)="uploadedFile($event)" #file  style="visibility:hidden; width: 2px;"/>
    <div class="input-group stylish-input-group" style="width: 300px;">
        <input type="text" class="form-control input-sm" [(ngModel)]="uploadedFileName" (click)="file.click()"  class="form-control input-sm" style="width: 230px;height:34px">
        <span class="input-group-btn input-sm"> 
            <button type="button" class="btn btn-primary input-sm" (click)="file.click()">
                 <span class="glyphicon glyphicon-folder-open"></span> Browse
            </button>
        </span>
    </div>
    `,
})
export class CustomBrowse {
    subscription: Subscription;
    uploadedFileName: string;
    uploadFile: File;
    @ViewChild('file') fileInput: ElementRef;
    @Output() cuBrowse: any = new EventEmitter();

    constructor(private ics: RpIntercomService) {
        this.subscription = ics.rpbean$.subscribe(x => { })
    }

    uploadedFile(event) {
        if (event.target.files.length == 1) {
            this.uploadedFileName = event.target.files[0].name;
            this.uploadFile = event.target.files[0];
            let data = { "fileName": this.uploadedFileName, "file": this.uploadFile };
            this.cuBrowse.emit(data);

        } else {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "info", "t3": "Upload File!" });
        }
    }

    resetValue() {
        this.fileInput.nativeElement.value = "";
        this.uploadedFileName = "";
    }

}