import {Component, Input,Output,EventEmitter} from '@angular/core'; 
import {RpIntercomService} from '../framework/rp-intercom.service';  

import {RpHttpService} from '../framework/rp-http.service';
declare var jQuery: any;

@Component({
    selector: 'pager6',
    template:`  
    <div>
        <nav>
            <ul class="pagination" style="margin:0px;">
                <li class="page-item" title="First">
                    <a class="page-link" style="height:34px;" (click)="goFirst()"><i class="glyphicon glyphicon-fast-backward" style="margin-top:2px;"></i></a>
                </li>
                
                <li class="page-item" title="Previous">
                    <a class="page-link" style="height:34px;" (click)="goPrev()"><i class="glyphicon glyphicon-backward" style="margin-top:2px;"></i></a>
                </li>

                <li class="page-item" title="Page No">
                    <span id="lblPageNo" class="page-link" style="height:34px;">
                        <select [(ngModel)]="currentPage" class="page-item" (ngModelChange)="changPage()" style="min-width:50px;margin-top:-5px">
                            <option *ngFor="let item of _pages" value="{{item.value}}" >{{item.caption}}</option>
                        </select>
                    </span>
                </li>

                <li class="page-item" title="Next"> 
                    <a class="page-link" style="height:34px;" (click)="goNext()"><i class="glyphicon glyphicon-forward" style="margin-top:2px;"></i></a> 
                </li>
                
                <li class="page-item" title="Last">
                    <a class="page-link" style="height:34px;" (click)="goLast()"><i class="glyphicon glyphicon-fast-forward" style="margin-top:2px;"></i></a>
                </li>

                <li class="page-item active" title="Page Size">
                    <span class="page-link" style="background:#337ab7;border-color:#337ab7;height:34px;">
                        <select [(ngModel)]="pageSize" class="page-item text-primary" style="margin-top:-5px;" (ngModelChange)="changRows()">
                            <option class="text-primary" *ngFor="let item of _sizes" value="{{item.value}}" >{{item.caption}}</option>
                        </select>
                    </span>
                </li>
            </ul>
        </nav>
    </div>
    `,
    providers: [RpHttpService]
})

export class Pager6 
{
    @Input() rpId: string;
    @Input() rpClass: string;

    @Input() rpPageSize: string;
    @Input() rpPageSizeMax: number;
    
    @Input() rpModel: number;
    @Input() rpCurrentPage: number;
    
    @Output() rpModelChange: any = new EventEmitter();
    @Output() rpChanged: any = new EventEmitter();

    _outputconstant: string;

    _pages = [];
    _sizes = [];

    currentPage = 1;
    prev = 1;
    last = 1;
    next = 2;
    start = 1;
    end = 10;
    pageSize = 10;
    totalCount = 1;

    _flag = false;
    _obj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 1 };

    constructor(private ics: RpIntercomService, private http: RpHttpService) 
    {
        if (this.rpId == null || this.rpId == "") this.rpId = "myid";
        if (this.rpClass == null || this.rpClass == "") this.rpClass = "col-md-3";
        if (this.rpPageSize == null || this.rpPageSize == "") this.rpPageSize = "10";
        if (this.rpPageSizeMax == null || this.rpPageSizeMax == 0) this.rpPageSizeMax = 100;    
        if (this.rpModel == null || this.rpModel < 0) this.rpModel = 1;
        if (this.rpCurrentPage == null || this.rpCurrentPage <0 ) this.rpCurrentPage =1; 
    }

    ngOnInit()
    {
        jQuery("#lblFirst").css("height", "34px");
        jQuery("#lblPrevious").css("height", "34px");
        jQuery("#lblNext").css("height", "34px");
        jQuery("#lblLast").css("height", "34px");
    }

    ngAfterContentInit() 
    {
        this.totalCount = +this.rpModel; 
        this.last = Math.ceil(this.totalCount / this.pageSize);

        this.fillPageSizes(this.rpPageSizeMax);
        this.fillPages();
    }
    
    ngOnChanges(rpModel) 
    {
        this.totalCount = +this.rpModel;

        if(this.rpCurrentPage !=null && this.rpCurrentPage >0 )     this.currentPage = + this.rpCurrentPage;
        else                                                        this.currentPage = 1;
      
        if(this.rpPageSize !=null && this.rpPageSize > "10")        this.pageSize = + this.rpPageSize;
        else                                                        this.pageSize = 10;
        
        this.end = 10;
        this.last = Math.ceil(this.totalCount / this.pageSize);

        this.fillPages();
        this.updatePager(this.currentPage);
    }

    initializePager(num)
    {
        this._obj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 1, "size": 10, "totalcount": this.totalCount };
    }

    updatePager(k) 
    {
        let num = +k;
        this.currentPage = num;

        this.prev = num > 1 ? num - 1 : 1;
        this.next = (num + 1) < this.last ? (num + 1) : this.last;

        this.start = this.totalCount < 1 ? this.totalCount : ((num - 1) * this.pageSize) + 1;
        this.end = (num * this.pageSize) > this.totalCount ? this.totalCount : (num * this.pageSize);
        
        this._obj = 
        { 
            "current": this.currentPage, "prev": this.prev, "last": this.last, "next": this.next, "start": this.start, "end": this.end, "size": this.pageSize, "totalcount": this.totalCount 
        };

        let data = { "obj" : this._obj, "flag" : this._flag };

        this.rpChanged.emit(data);
        this._flag = false;
    }

    fillPageSizes(pg)
    {
        let size = +pg;
        this._obj.current = 1;

        let total = size / 10;

        for (let i = 1; i <= total; i++) 
        {
            let k = { "value": 0, "caption": "" };

            k.value = i * 10;
            k.caption = "" + (i * 10);
            
            this._sizes.push(k);
        }
    }

    fillPages()
    {
        this._pages = [];
        this._obj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 1, "size": 10, "totalcount": this.totalCount };

        let num = Math.ceil(this.totalCount / this.pageSize);

        for (let i = 1; i <= num; i++)
        {
            let k = { "value": 0, "caption": "" };
            k.value = i;
            k.caption = "" + i;
            this._pages.push(k);
        }
    }

    goFirst() 
    {
        this._flag = true;
        this.currentPage = 1;

        this.updatePager(this.currentPage);
    }

    goLast() 
    {
      this._flag = true;
      this.currentPage = Math.ceil(this.totalCount / this.pageSize);

      this.updatePager(this.currentPage);
    }

    goPrev() 
    {
        this._flag = true;
        let k = this.prev;

        this.updatePager(k);
    }

    goNext()
    {
        this._flag = true;
        let k = this.next;

        this.updatePager(k);
    }

    changRows() 
    {
      this.fillPages();    
      this.last = Math.ceil(this.totalCount / this.pageSize);

      this.updatePager(1);
    }

    changPage() 
    {
      this._flag = true;
      this.updatePager(this.currentPage);
    }
}