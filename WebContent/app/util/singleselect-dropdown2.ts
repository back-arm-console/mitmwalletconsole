/*
 * Angular 2 Dropdown Multiselect for Bootstrap
 * Current version: 0.1.0
 *
 * Simon Lindh
 * https://github.com/softsimon/angular-2-dropdown-multiselect
 */

import { Component, Pipe, OnInit, HostListener, Input, ElementRef, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { FormControl, FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts,SearchFilter } from './multiselect-pipe';
declare var jQuery : any;
declare function isMyanmar(pwd:String) : boolean;
declare function isZawgyi(pwd:String) : boolean;
declare function ZgtoUni(pwd:String) : string;
declare function isUnicode_my(pwd:String) : boolean;
declare function UnitoZg(pwd:String) : string;
declare function ZgtoUni(pwd:String) : string;
declare function systemFont() : string;
declare function wdth(pwd:String) : string;  

@Component({
    selector: 'ss-singleselect-dropdown2',
	styles: [`
		a { outline: none; }
	`],
    template: `
        <div class="btn-group">
            <button id="button" type="button" class="dropdown-toggle btn col-md-12" style="min-height:32px;" [ngClass]="settings.buttonClasses" (click)="toggleDropdown()" title="{{getFullManualAmount(getTitle())}}">{{ getTitle() }}<span class="pull-right"><span class="caret"></span></span></button>
            <ul id="test" *ngIf="isVisible" class="dropdown-menu" [class.pull-right]="settings.pullRight" [style.max-height]="settings.maxHeight" style="display: block; height: auto; overflow-y: auto;">
                <li *ngIf="settings.enableSearch">
                    <div class="input-group input-group-sm" >
                        <!-- <span class="input-group-addon" id="sizing-addon3"><i class="fa fa-search"></i></span> -->
                        <span class="input-group-addon"><i class="glyphicon glyphicon-search" style="cursor: pointer"></i></span>
                        <input type="text" class="form-control" placeholder="{{ texts.searchPlaceholder }}" aria-describedby="sizing-addon3" [formControl]="search">
                        <span class="input-group-btn" *ngIf="searchFilterText.length > 0">
                            <button class="btn btn-default" type="button" (click)="clearSearch()"><i class="fa fa-times"></i></button>
                        </span>
                    </div>
                </li>
                
                <li *ngFor="let option of options | searchFilter:searchFilterText" title="{{getFullManualAmount(option.name)}}">
                    <a  role="menuitem" tabindex="-1" (click)="setSelected($event, option)" class="singleselectwrap">
                        <span *ngIf="settings.checkedStyle == 'glyphicon'" style="width: 16px;" class="glyphicon" [class.glyphicon-ok]="isSelected(option)"></span>
                        {{ option.name }}
                    </a>
                </li>
            </ul>
        </div>
    `,
    styleUrls: ['css/singleselect2.css']
})
export class SingleselectDropdown2 implements OnInit {
    @Input() options: Array<IMultiSelectOption>;
    @Input() settings: IMultiSelectSettings;
    @Input() texts: IMultiSelectTexts;
    @Input('defaultModel') selectedModel: Array<number> = [];
    @Output('selectedModel') model = new EventEmitter();
    @Output() selectionLimitReached = new EventEmitter();
    @HostListener('document: click', ['$event.target'])
    onClick(target) {
        let parentFound = false;
        while (target !== null && !parentFound) {
            if (target === this.element.nativeElement ) {
                parentFound = true;
            }
            target = target.parentElement;
        }
        if (!parentFound) {
            this.isVisible = false;
        }
    }
    private search = new FormControl();
    private numSelected: number = 0;
    private isVisible: boolean = false;
    private searchFilterText: string = '';
    private defaultSettings: IMultiSelectSettings = {
        pullRight: false,
        enableSearch: false,
        checkedStyle: 'checkboxes',
        buttonClasses: 'btn btn-default',
        selectionLimit: 0,
        closeOnSelect: false,
        dynamicTitleMaxItems:3,
        maxHeight: '300px',
    };
    private defaultTexts: IMultiSelectTexts = {
        checked: 'selected',
        checkedPlural: 'selected',
        searchPlaceholder: 'Search...',
        defaultTitle: 'Select',
    };

    constructor(
        private element: ElementRef
    ) { }

    ngOnInit() {
        this.settings = Object.assign(this.defaultSettings, this.settings);
        this.texts = Object.assign(this.defaultTexts, this.texts);
        this.updateNumSelected();
        this.search.valueChanges
            .subscribe((text: string) => {
                this.searchFilterText = text;
            });
    }

    clearSearch() {
       this.search.setValue('');
    }

    toggleDropdown() {
        this.search.setValue('');
        this.isVisible = !this.isVisible;
        
    }

    modelChanged() {
        this.updateNumSelected();
        this.model.emit(this.selectedModel);
    }

    isSelected(option: IMultiSelectOption): boolean {
        return this.selectedModel.indexOf(parseInt(option.id)) > -1;
    }

    setSelected(event: Event, option: IMultiSelectOption) {
        this.selectedModel[0] = 0;
        var index = this.selectedModel.indexOf(parseInt(option.id));
        if (index > 0) {
            
        } 
        else {
            this.selectedModel[0] = parseInt(option.id);
        }
        
        jQuery("#test").hide();
        if (this.settings.closeOnSelect) {
            this.toggleDropdown();
        }
        this.modelChanged();
        
    }

    getTitle() {
        
        if(this.selectedModel[0] == 0){
            return this.texts.defaultTitle;
        }
        if (this.numSelected === 0) {
            return this.texts.defaultTitle;
        }
        
        if (this.settings.dynamicTitleMaxItems >= this.numSelected) {
            return this.options
                .filter((option: IMultiSelectOption) => this.selectedModel.indexOf(parseInt(option.id)) > -1)
                .map((option: IMultiSelectOption) => option.name)
                .join(', ');
        }
      
        return this.numSelected + ' ' + (this.numSelected === 1 ? this.texts.checked : this.texts.checkedPlural);
    }

    updateNumSelected() {
        this.numSelected = this.selectedModel.length;
    }
    
    getFullManualAmount(str){
      if(str != undefined && str != null){
        if(localStorage.getItem("systemfont")!="uni"){
          if(isMyanmar(str))
            str = ZgtoUni(str);
      }
            return str;
      }else{
          return '';
      }
  }


}
