/*
 * Angular 2 Dropdown Multiselect for Bootstrap
 * Current version: 0.1.0
 *
 * Simon Lindh
 * https://github.com/softsimon/angular-2-dropdown-multiselect
 */

import { Component, Pipe, OnInit, HostListener, Input, ElementRef, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { FormControl, FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts,SearchFilter } from './multiselect-pipe';
declare var jQuery : any;
declare function isMyanmar(pwd:String) : boolean;
declare function ZgtoUni(pwd:String) : string;
declare function UnitoZg(pwd:String) : string;
declare function systemFont() : string;

@Component({
    selector: 'ss-multiselect-dropdown2',
	styles: [`
		a { outline: none; }
	`],
    template: `
        <div class="btn-group">
        <input id="button" type="button" class="dropdown-toggle btn col-md-12" style="min-height:32px;" [ngClass]="settings.buttonClasses" [(ngModel)]="selectedItem" [ngModelOptions]="{standalone: true}" (ngModelChange)="updateData($event)" (click)="toggleDropdown()" title="{{getFullName(getTitle())}}"><span class="pull-right"><span class="caret"></span></span>
         <!--   <button id="button" type="button" class="dropdown-toggle btn" style="min-height:32px;min-width:360px;" [ngClass]="settings.buttonClasses" (click)="toggleDropdown()" title="{{getFullName(getTitle())}}" >{{ getTitle() }}<span class="pull-right"><span  class="caret"></span></span></button> -->
            <ul *ngIf="isVisible" class="dropdown-menu" [class.pull-right]="settings.pullRight" [style.max-height]="settings.maxHeight" style="display: block; height: auto; overflow-y: auto;">
                <li style="margin: 0px 5px 5px 5px;" *ngIf="settings.enableSearch">
                    <div class="input-group input-group-sm">
                        <span class="input-group-addon" id="sizing-addon3"><i class="fa fa-search"></i></span>
                        <input type="text" class="form-control"  placeholder="{{ texts.searchPlaceholder }}" aria-describedby="sizing-addon3" [formControl]="search">
                        <span class="input-group-btn" *ngIf="searchFilterText.length > 0">
                            <button class="btn btn-default" type="button" (click)="clearSearch()"><i class="fa fa-times"></i></button>
                        </span>
                    </div>
                </li>
                <li class="divider" *ngIf="settings.enableSearch"></li>
                <li *ngIf="settings.showCheckAll">
                    <a role="menuitem" tabindex="-1" (click)="checkAll()">
                        <span style="width: 16px;" class="glyphicon glyphicon-ok"></span>
                        {{ texts.checkAll }}
                    </a>
                </li>
                <li *ngIf="settings.showUncheckAll">
                    <a role="menuitem" tabindex="-1" (click)="uncheckAll()">
                        <span style="width: 16px;" class="glyphicon glyphicon-remove"></span>
                        {{ texts.uncheckAll }}
                    </a>
                </li>
                <li >
                    <a  role="menuitem" tabindex="-1" >
                         <input *ngIf="settings.checkedStyle == 'checkboxes'" type="checkbox" [(ngModel)]="checkallflag" name="checkallflag" [ngModelOptions]='{standalone: true}'  (click)="checkuncheckAll($event)" [checked]="isAll()"/>
                        <span *ngIf="settings.checkedStyle == 'glyphicon'" style="width: 16px;" class="glyphicon"></span>
                        ALL
                    </a>
                </li>
                <li *ngIf="settings.showCheckAll || settings.showUncheckAll" class="divider"></li>
                <li *ngFor="let option of options | searchFilter:searchFilterText" title="{{getFullName(option.name)}}">
                    <a  role="menuitem" tabindex="-1" (click)="setSelected(option.id)" class="singleselectwrap">
                        <input *ngIf="settings.checkedStyle == 'checkboxes'" type="checkbox" [checked]="isSelected(option)" />
                        <span *ngIf="settings.checkedStyle == 'glyphicon'" style="width: 16px;" class="glyphicon" [class.glyphicon-ok]="isSelected(option)"></span>
                        {{ option.name }}
                    </a>
                </li>
            </ul>
        </div>
    `,
    styleUrls: ['css/multiselect2.css']
})
export class MultiselectDropdown2 implements OnInit {
    @Input() options: Array<IMultiSelectOption>;
    @Input() settings: IMultiSelectSettings;
    @Input() texts: IMultiSelectTexts;
    @Output() selectedItemChange = new EventEmitter();
    @Input() selectedItem: any;
    //@Input() rpReadonly : string;
    @Input('defaultModel') selectedModel: Array<number> = [];
    @Output('selectedModel') model = new EventEmitter();
    @Output() selectionLimitReached = new EventEmitter();
    @HostListener('document: click', ['$event.target'])
    onClick(target) {
        let parentFound = false;
        while (target !== null && !parentFound) {
            if (target === this.element.nativeElement ) {
                parentFound = true;
            }
            target = target.parentElement;
        }
        if (!parentFound) {
            this.isVisible = false;
        }
    }
    checkallflag: boolean = false;
    private search = new FormControl();
    private numSelected: number = 0;
    private isVisible: boolean = false;
    private searchFilterText: string = '';
    private defaultSettings: IMultiSelectSettings = {
        pullRight: false,
        enableSearch: false,
        checkedStyle: 'checkboxes',
        buttonClasses: 'btn btn-default',
        selectionLimit: 9,
        closeOnSelect: false,
        showCheckAll: false,
        showUncheckAll: false,
        dynamicTitleMaxItems:3,
        maxHeight: '300px',
    };
    private defaultTexts: IMultiSelectTexts = {
        checkAll: 'Check all',
        uncheckAll: 'Uncheck all',
        checked: 'selected',
        checkedPlural: 'selected',
        searchPlaceholder: 'Search...',
        defaultTitle: 'Select',
    };

    constructor(
        private element: ElementRef
    ) { }

    ngOnInit() {
        this.settings = Object.assign(this.defaultSettings, this.settings);
        this.texts = Object.assign(this.defaultTexts, this.texts);
        this.updateNumSelected();
        this.search.valueChanges
            .subscribe((text: string) => {
                this.searchFilterText = text;
            });
    }

    clearSearch() {
       this.search.setValue('');
    }

    toggleDropdown() {
        this.search.setValue('');
        this.isVisible = !this.isVisible;
        
    }

    modelChanged() {
        this.updateNumSelected();
        this.model.emit(this.selectedModel);
        this.selectedItemChange.emit(this.selectedItem);
    }

    isSelected(option: IMultiSelectOption): boolean {
        return this.selectedModel.indexOf(parseInt(option.id)) > -1;
    }

    setSelected(id) {
        var index = this.selectedModel.indexOf(parseInt(id));
        if (index > -1) {
            this.selectedModel.splice(index, 1);
            this.selectedItem = "";
            if(this.selectedModel != null && this.selectedModel.length>0){
                for(let i=0;i<this.selectedModel.length;i++){
                    let val = "";
                    for(let j=0;j<this.options.length;j++){
                        if(this.selectedModel[i] == Number(this.options[j].id)){
                            val = this.options[j].name;
                            break;
                        }
                    }
                    if(val != ""){
                        if(this.selectedItem==""){
                            this.selectedItem = val;
                        }else{
                            this.selectedItem += ","+val;
                        }
                    }
                    
                }
            }
            this.checkallflag = false;
        } else {
            if (this.settings.selectionLimit === 0 || this.selectedModel.length < this.settings.selectionLimit) {
                this.selectedModel.push(parseInt(id));
                this.selectedItem = "";
                if(this.selectedModel != null && this.selectedModel.length>0){
                    for(let i=0;i<this.selectedModel.length;i++){
                        let val = "";
                        for(let j=0;j<this.options.length;j++){
                            if(this.selectedModel[i] == Number(this.options[j].id)){
                                val = this.options[j].name;
                                break;
                            }
                        }
                        if(val != ""){
                            if(this.selectedItem==""){
                                this.selectedItem = val;
                            }else{
                                this.selectedItem += ","+val;
                            }
                        }
                        
                    }
                }
            } else {
                this.selectionLimitReached.emit(this.selectedModel.length);
                return;
            }
            
        }
        
        if (this.settings.closeOnSelect) {
            this.toggleDropdown();
        }
        this.modelChanged();
    }

    getTitle() {
        if (this.numSelected === 0) {
            return this.texts.defaultTitle;
        }
        if (this.settings.dynamicTitleMaxItems >= this.numSelected) {
            return this.options
                .filter((option: IMultiSelectOption) => this.selectedModel.indexOf(parseInt(option.id)) > -1)
                .map((option: IMultiSelectOption) => option.name)
                .join(', ');
        }
      
        return this.numSelected + ' ' + (this.numSelected === 1 ? this.texts.checked : this.texts.checkedPlural);
    }

    updateNumSelected() {
        this.numSelected = this.selectedModel.length;
    }
    
    updateData(event) { 
       this.selectedItem = event;
       this.selectedItemChange.emit(event);
    } 

    checkAll() {
        this.selectedModel = this.options.map(option => parseInt(option.id));
        this.modelChanged();
    }
    
    uncheckAll() {
        this.selectedModel = [];
        this.modelChanged();
    }
    isAll(){
        if(this.options.length==this.selectedModel.length)
        return true;
        return false;
    }
    
    checkuncheckAll(event) {
        if (event.target.checked) {
             if(this.options != null && this.options.length>0){
                for(let i=0;i<this.options.length;i++){
               // if (this.settings.selectionLimit === 0 || this.selectedModel.length < this.settings.selectionLimit) {
                if(this.selectedModel.indexOf(parseInt(this.options[i].id)) < 0){
                    this.selectedModel.push(parseInt(this.options[i].id));
                    this.selectedItem = "";
                    if(this.selectedModel != null && this.selectedModel.length>0){
                        for(let i=0;i<this.selectedModel.length;i++){
                            let val = "";
                            for(let j=0;j<this.options.length;j++){
                                if(this.selectedModel[i] == Number(this.options[j].id)){
                                    val = this.options[j].name;
                                    break;
                                }
                            }
                            if(val != ""){
                                if(this.selectedItem==""){
                                    this.selectedItem = val;
                                }else{
                                    this.selectedItem += ","+val;
                                }
                            }
                            
                        }
                    }
                } 
            // } else {
            //     this.selectionLimitReached.emit(this.selectedModel.length);
            //     return;
            // }
            
          }
      }
    }else{
        if(this.options != null && this.options.length>0){
                for(let i=0;i<this.options.length;i++){
                if(this.selectedModel.indexOf(parseInt(this.options[i].id)) > -1){
                    this.selectedModel.splice(this.selectedModel.indexOf(parseInt(this.options[i].id)), 1);
                    this.selectedItem = "";
                    if(this.selectedModel != null && this.selectedModel.length>0){
                        for(let i=0;i<this.selectedModel.length;i++){
                            let val = "";
                            for(let j=0;j<this.options.length;j++){
                                if(this.selectedModel[i] == Number(this.options[j].id)){
                                    val = this.options[j].name;
                                    break;
                                }
                            }
                            if(val != ""){
                                if(this.selectedItem==""){
                                    this.selectedItem = val;
                                }else{
                                    this.selectedItem += ","+val;
                                }
                            }
                            
                        }
                    }
                } 
            
            
          }
      }
    }
    if (this.settings.closeOnSelect) {
            this.toggleDropdown();
    }
    this.modelChanged();  
    }


    
    getFullName(str){
      if(str != undefined && str != null){
        if(localStorage.getItem("systemfont")!="uni"){
          if(isMyanmar(str))
            str = ZgtoUni(str);
      }
            return str;
      }else{
          return '';
      }
  }

}
