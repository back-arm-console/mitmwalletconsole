import {Component, Input, Output, EventEmitter, OnInit } from '@angular/core'; 
import {RpIntercomService} from '../framework/rp-intercom.service';  
import {RpHttpService} from '../framework/rp-http.service';
import { Subscription }   from 'rxjs/Subscription';
import { enableProdMode } from '@angular/core';
enableProdMode(); 
@Component({
    selector: 'adminpager',
    template:`
    <div>
    <table>
    <tr>
    <td>
    <nav>
      <ul class="pagination">
        <li class="page-item">
          <a class="page-link1" (click)="goFirst()" style="cursor: pointer">First</a>
        </li>
        <li class="page-item">
          <a class="page-link1" (click)="goPrev()" style="cursor: pointer">Prev</a>
        </li>
        <li class="page-item">
          <span class="page-link">
            <select [(ngModel)]="currentPage" class="page-item" (ngModelChange)="changPage()">
              <option *ngFor="let item of _pages" value="{{item.value}}" >{{item.caption}}</option>
            </select>
          </span>
        </li>
        <li class="page-item">
          <a class="page-link1" (click)="goNext()" style="cursor: pointer">Next</a>
        </li>
        <li class="page-item">
          <a class="page-link1" (click)="goLast()" style="cursor: pointer">Last</a>
        </li>        
        <li class="page-item  active">
          <span class="page-link">
            <select [(ngModel)]="pageSize" class="page-item text-primary" (ngModelChange)="changRows()">
              <option class="text-primary" *ngFor="let item of _sizes" value="{{item.value}}" >{{item.caption}}</option>
            </select>
          </span>
        </li>
      </ul>
      </nav>
      </td>
      <td><label id="custom-label">{{_calcPag}}</label></td>
      </tr>
      </table>
    </div>
    <!--<div class="input-group">
      <label id="custom-label" style="padding-right: 10px;">{{_calcPag}}</label>
    </div>-->
    `,
})
export class AdminPager {
    //bindModelData >
    @Input() rpId: string;
    @Input() rpClass: string;
    @Input() rpPageSize: string;
    @Input() rpPageSizeMax: number;
    @Input() rpModel: number;
    @Output() rpModelChange: any = new EventEmitter();
    @Output() rpChanged: any = new EventEmitter();
  
    _outputconstant: string;  
    _pages = [];
    _sizes = [];  
    currentPage = 1;
    prev = 1;
    last = 1;
    next = 2;
    start = 1;
    end = 10;
    pageSize = 10;
    totalCount = 1;  
    _flag = false;  
    _obj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 1 };
    _calcPag="";
    constructor(private ics: RpIntercomService, private http: RpHttpService) {
      if (this.rpId == null || this.rpId == "") this.rpId = "myid";
      if (this.rpClass == null || this.rpClass == "") this.rpClass = "col-md-3";
      if (this.rpPageSize == null || this.rpPageSize == "") this.rpPageSize = "10";
      if (this.rpPageSizeMax == null || this.rpPageSizeMax == 0) this.rpPageSizeMax = 100;    
      if (this.rpModel == null || this.rpModel < 0) this.rpModel = 1;
    }
    
    ngAfterContentInit() {
      //console.log("ngInit: "+JSON.stringify(this.rpModel));
      this.totalCount = +this.rpModel; 
      this.last = Math.ceil(this.totalCount / this.pageSize);
      this.fillPageSizes(this.rpPageSizeMax);
      this.fillPages();
      this.updatePager(this.currentPage);
    }
    
    ngOnChanges(rpModel) {
      //console.log("ngOnChanges: "+JSON.stringify(this.rpModel));
      this.totalCount = +this.rpModel;
      this.currentPage = 1;
      this.end = 10;
      this.pageSize = 10;
      this.last = Math.ceil(this.totalCount / this.pageSize);
      this.fillPages();
      this.updatePager(this.currentPage);
    }
    initializePager(num){
      this._obj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 1, "size": 10, "totalcount": this.totalCount };
    }
    updatePager(k) {
      //custom pager by hms... 20160614 11:00pm... ymk...
      let num = +k;
      this.currentPage = num;
      this.prev = num > 1 ? num - 1 : 1;
      this.next = (num + 1) < this.last ? (num + 1) : this.last;
      this.start = this.totalCount < 1 ? this.totalCount : ((num - 1) * this.pageSize) + 1;
      this.end = (num * this.pageSize) > this.totalCount ? this.totalCount : (num * this.pageSize);
      
      this._obj = { 
        "current": this.currentPage, "prev": this.prev, "last": this.last, 
        "next": this.next, "start": this.start, "end": this.end, "size": this.pageSize, 
        "totalcount": this.totalCount 
      };
  
      let t = Number(this._obj.totalcount);
      let i = Number(this._obj.size);
      let c = Number(this._obj.current);
      let s = t > 0? ((c - 1) * i) + 1:0;
      let m = (((c - 1) * i) + i) > t ? t : (((c - 1) * i) + i);
      this._calcPag = s + " - " + m + " of " + t;
  
      this.rpChanged.emit(this._obj);
      //console.log(JSON.stringify(this._obj));
    }
    fillPageSizes(pg) {
      let size = +pg;
      this._obj.current = 1;
      let total = size / this.pageSize;
      //console.log("total" + total);
      for (let i = 1; i <= total; i++) {
        let k = { "value": 0, "caption": "" };
        k.value = i * 10;
        k.caption = "" + (i * 10);
        this._sizes.push(k);
      }
    }
    fillPages() {
      this._pages = [];
      this._obj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 1, "size": 10, "totalcount": this.totalCount };
      //console.log("running fillPages...");
      let num = Math.ceil(this.totalCount / this.pageSize);
      for (let i = 1; i <= num; i++) {
        let k = { "value": 0, "caption": "" };
        k.value = i;
        k.caption = "" + i;
        this._pages.push(k);
      }
    }
    goFirst() {
      this.currentPage = 1;
      this.updatePager(this.currentPage);
    }
    goLast() {
      this.currentPage = Math.ceil(this.totalCount / this.pageSize);
      this.updatePager(this.currentPage);
    }
   goPrev() {
      let k = this.prev;
      this.updatePager(k);
   }
    goNext() {
      let k = this.next;
      this.updatePager(k);
    }
    changRows() {
      this.fillPages();    
      this.last = Math.ceil(this.totalCount / this.pageSize);
      this.updatePager(1);
    }
  
    changPage() {
      this.updatePager(this.currentPage);
    }
}