import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { RpInputModule } from '../framework/rp-input.module';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AdvancedSearch } from './advancedsearch.component';

@NgModule({
  imports: 
  [
    CommonModule,
    FormsModule,
    RpInputModule
  ],
  declarations: 
  [ 
    AdvancedSearch
  ],
  exports: [
    AdvancedSearch
  ],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AdvancedSearchModule { }