import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { enableProdMode } from '@angular/core';
import { Subscription }   from 'rxjs/Subscription';
import { RpHttpService } from './framework/rp-http.service';
import { RpIntercomService } from './framework/rp-intercom.service';
import { RpBean } from './framework/rp-bean';
import { ClientUtil } from './util/rp-client.util';
enableProdMode(); 
@Component({
  selector: 'rp-login',
  template: `
  <div class="container">
    <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
            <form class="form-horizontal" (ngSubmit)="goPost()">
                <div class="card card-container">
                    <div class="row col-md-12">&nbsp;</div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
                            <h3 style="text-align:center;">{{_signintext}}</h3>
                        </div>
                    </div>

                    <div class="row col-md-12">&nbsp;</div>

                    <div style="margin-bottom: 30px" class="input-group">
                        <span class="input-group-addon">
                            <i class="glyphicon glyphicon-user"></i>
                        </span>
                        <input id="nameid" class="form-control" Required type="text" [(ngModel)]="_user" [ngModelOptions]="{standalone: true}" placeholder="User ID"
                            autofocus>
                    </div>

                    <div style="margin-bottom: 30px" class="input-group">
                        <span class="input-group-addon">
                            <i class="glyphicon glyphicon-lock"></i>
                        </span>
                        <input id="itemid" class="form-control" Required type="password" [(ngModel)]="_pw" [ngModelOptions]="{standalone: true}"
                            placeholder="Password">
                    </div>

                    <!-- <div class="row">
                            <div class="col-md-offset-0 col-md-10"> 
                                <div class="checkbox">
                                <label><input type="checkbox" value="remember-me" [(ngModel)]=remembercheck [ngModelOptions]="{standalone: true}" (click)="setremember(cname,_user,pname,_pw,$event)" > Remember me</label>
                                </div>
                            </div>
                    </div>-->

                   <div style="margin-bottom: 50px" class="form-group" align="center">
                        <button class="btn input-sm btn-primary btn-block" style="width:91%" type="submit">Sign In</button>
                    </div>
                    <div class="row col-md-12">&nbsp;</div>
                    <div align="center" style="margin-bottom: 50px">
                        <span *ngIf="_result !=null" class="label" style="color: red;">{{_result}} </span>
                    </div>
                    <div class="row col-md-12">&nbsp;</div>
                </div>
            </form>
        </div>
    </div>
  </div>
  <div [hidden]="_mflag">
  <div  id="loader" class="modal" ></div>
</div>
  `
})
export class RpLoginComponent implements OnDestroy {
  _user: string;
  _pw: string;
  _mflag=true;
  url: string = "url";
  cname: string = "username";
  pname: string = "password";
  _signintext:string = "";
  remembercheck: boolean = true;
  _result: string;
  _remember: boolean;
  subscription: Subscription;
  _rpbean: RpBean;
  apiUrl: string;
  constructor(private _util: ClientUtil, private ics: RpIntercomService, private _router: Router, private http: RpHttpService) {
    this.subscription = ics.rpbean$.subscribe(x => { });
    this.ics._profile.role = 0;
    this.checkCookie();
    this.ics.sendBean(new RpBean()); 
    ics.confirmUpload(false);
    this.getsignintext();
  }
  checkCookie() {
    let value = this.readCookieValue('username');
    if (value != "" && value != null) {
      this._user = value;
    }
    else {
      this._user = "";
      this.remembercheck = false;
    }
  }

  goPost() {
    try{
      //this.ics.sendBean({ t1: "rp-wait", t2: "Signing in ..." });
      this._mflag = false;
      var iv = this._util.getIvs();
      var dm = this._util.getIvs();
      var salt = this._util.getIvs();
      let profile: any = { "userID": this._user, "password": this._pw, "userType": 2 , "iv": iv, "dm": dm, "salt": salt};
      let url: string = this.ics._apiurl + 'service001/signin';
      profile.password = this._util.getEncryptText(iv, salt, dm, profile.password)
      this.http.doPost(url, profile).subscribe(
        data => {
          console.log("Sign in response data=" +JSON.stringify(data));
          this.ics.sendBean({ t1: "rp-msg-off" });
          if(data.code == "0000"){
            this.authorize(data);
            this._mflag=true;
          }else{
             this._result = data.desc //"Invalid User ID or Password";
             this._mflag=true;
          }
          
        },
        error => {
          this._mflag=true;
          this.ics.sendBean({ t1: "rp-error", t2: "HTTP Error Type " + error.type });
        },
        () => { }
      );
    }catch(e){
      alert("Invalid URL");
    }
  } 

  // goPost() {
  //   let url: string = this.ics._apiurl + 'service001/signin';
  //   let profile: any = { "userID": this._user, "password": this._pw, "commandCenter": this._remember };
  //   this.http.doPost(url, profile).subscribe(
  //     data => {
  //       if (this.apiUrl == null && this.apiUrl == undefined) {
  //         this.ics.sendBean({ t1: "rp-msg-off" });
  //       }
  //       if(data.code == "0000"){
  //         this.authorize(data);
  //       }else{
  //          this._result = data.desc //"Invalid User ID or Password";
  //       }
  //       // if (data.role > 0) {
  //       //   this.authorize(data);
  //       // } else {
  //       //   this._result = "Invalid User ID or Password";
  //       // }
  //     },
  //     error => {
  //       this.ics.sendBean({ t1: "rp-error", t2: "HTTP Error Type " + error.type });
  //     },
  //     () => { }
  //   );
  // }
  authorize(data: any) {
    this.ics._profile = data;
    this.ics._profile.userID = this._user;
    let value = this.readCookieValue('pager');
    if (value != "" && value != null) {
      this.ics._profile.n1 = Number(value);
    }
    else {
      this.ics._profile.n1 = 10;
    }
    this.ics.sendBean(new RpBean());
    this._router.navigate(['/000']);
  }
 
  setremember(cname, _user, pname, _pw, event) {
    if (event.target.checked) {
      if (cname == "" && _pw == "") {
        document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
        document.cookie = "password=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
        document.cookie = "url=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
      }
      else {
        let d = new Date();
        d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
        let expires = "expires=" + d.toDateString();
        document.cookie = cname + "=" + _user + ";" + expires;
        document.cookie = pname + "=" + _pw + ";" + expires;
        document.cookie = this.url + "=" + this.ics._apiurl + ";" + expires;
      }
    }
    else {
      document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
      document.cookie = "password=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
      document.cookie = "url=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
    }
  }
  readCookieValue(name) {
    let nameEQ = name + "=";
    let ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ')
        c = c.substring(1, c.length);
      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
  }

  getsignintext(){
    this.http.doGet('json/config.json?random=' +Math.random()).subscribe(
       data =>{
         
         this._signintext  = data.signintext;
       },
       error => alert(error),
       () => {}
     ); 
 }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}