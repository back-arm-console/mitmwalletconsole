import { Component, enableProdMode } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { RpHttpService } from '../framework/rp-http.service';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpReferences } from '../framework/rp-references';
declare var jQuery: any;
enableProdMode();
@Component({
  selector: 'merchantprofile',
  template: `
  <div class="container-fluid">
  <div class="row clearfix">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0"> 
      <form class= "form-horizontal" > <!--(ngSubmit)="goPost()"-->
        <!-- Form Name -->
        <legend>Merchant</legend>

        <div class="cardview list-height">

        <div class="row  col-md-12">    
          <button class="btn btn-sm btn-primary" type="button" (click)="goList()">List</button>
          <button class="btn btn-sm btn-primary" type="button" (click)="goNew()">New</button>      
          <button class="btn btn-sm btn-primary" type="button" (click)="goPost()">Save</button>
          <button class="btn btn-sm btn-primary" disabled id="mydelete" type="button" (click)="goDelete();">Delete</button> 
        </div>
        <div class="row col-md-12">&nbsp;</div>
        <div class="col-md-12" id="custom-form-alignment-margin">
          <div  class="col-md-6">
      <div class="form-group">
              <rp-input [(rpModel)]="_obj.userId" rpRequired ="true" rpType="text" rpLabelRequired='true' rpLabel="Merchant ID" rpReadonly="true" rpClass="col-md-8 control-label" rpLabelClass="col-md-4"></rp-input>
      </div>
      <div class="form-group" style="margin-top:-7;">
              <rp-input rpType="text" rpLabel=" Company Name" [(rpModel)]="_obj.userName" rpLabelRequired='true' rpReadonly="false" rpClass="col-md-8 control-label" rpLabelClass="col-md-4" rpRequired="true"></rp-input>
            </div>      
      <div class="form-group">
              <label class="col-md-4" align="left">Feature&nbsp; <font class="mandatoryfont">*</font></label>
              <div class="col-md-8" > 
                <select [(ngModel)]="_obj.feature"  class="form-control col-md-0" [ngModelOptions]="{standalone: true}">
                  <option *ngFor="let item of ref._lov3.refFeature" value="{{item.value}}">{{item.caption}}</option>
                </select> 
              </div> 
      </div>
      <div class="form-group">
              <label class="col-md-4" align="left"> Branch Code <font size="4" color="#FF0000">*</font></label>
              <div class="col-md-8">
                <select [(ngModel)]="_obj.branchCode" class="form-control col-md-0" [ngModelOptions]="{standalone: true}">
                  <option *ngFor="let item of ref._lov3.ref018" value="{{item.value}}">{{item.value}}</option> 
                </select>                
              </div> 
            </div>
		<div class="form-group">
        <label class="col-md-4"> Account Number/GL </label>
        <div class="col-md-8">
        <div class="input-group">
          <input type="text" [(ngModel)]='_obj.accountNumber' [ngModelOptions]="{standalone: true}" class="form-control col-md-0" readonly><!--*ngIf="isvalidate != ''"-->
          <span class="input-group-btn">
          <button class="btn btn-sm btn-primary" type="button" (click)="goPopupList()" style='height:34px'>&equiv;</button>&nbsp;
          <!--<button class="btn btn-sm btn-primary" id="myadd" style="margin-left:5px" (click)="goAddAll()">Add</button>						-->
          </span>              
        </div>
        </div>				  
      </div>

      <!--<div class="form-group">
        <label class="col-md-4" >  &nbsp; &nbsp;&nbsp;</label>
        <div class="col-md-8">
          <input type = "checkbox" [(ngModel)]=isGL [ngModelOptions]="{standalone: true}" (change)="checkGL($event)"> &nbsp; Is GL 
          <label style="color:green;font-size:15px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{isvalidate}}</label>                      
        </div> 
      </div>-->
      <div class="form-group">
      <label class="col-md-4" >  &nbsp; &nbsp;&nbsp;</label>
        <div class="col-md-8">
        <label class="radio-inline">
          <input #m [checked]="_obj.chk == m.value" (click)="_obj.chk = m.value" name="chk" value="0" type="radio" (change)="radioChecked()"> GL
        </label>
        <label class="radio-inline">
          <input  #c [checked]="_obj.chk == c.value" (click)="_obj.chk = c.value" name="chk" value="1" type="radio" (change)="radioChecked()"> Wallet
        </label>
        </div>
      </div>
      
      <div class="form-group">
        <label class="col-md-4" for="imageUploadid"> Image Associated</label>
          <div class="col-md-8" id="imageUploadid">
            <input type="file" id="imageUpload"  class="file" (change)="uploadedFileImage($event)"/>
          		<div class="input-group">
          		<input type="text" id="imageUpload1" class="form-control input-sm" placeholder="No file chosen" maxlength="100">
          		<span class="input-group-btn">
          		<button class="browse btn btn-sm btn-primary" type="button"><i class="glyphicon glyphicon-search"></i> Browse</button>                         
          		</span>
          		</div>
          		<div class="col-md-4">(jpeg, png, jpg) </div> 
          </div>
      </div>
      <div class="form-group">
        <label class="col-md-4"> &nbsp; &nbsp;&nbsp;</label>
        <div calss="col-md-8" *ngIf="_obj.t13!=''">	
          <img src="{{setImgUrl(_obj.t13)}}" onError="this.src='./image/image_not_found.png';" alt={{img}} height="240" width="400"/><!--{{setImgUrl(_obj.uploadlist[0].name)}}-->	
        </div>
      </div>
          </div>
             
          <div  class="col-md-6">
      <div class="form-group">
              <rp-input rpType="text" rpLabel=" Contact Number" [(rpModel)]="_obj.t4" rpLabelRequired='true' rpReadonly="false" rpClass="col-md-8 control-label" rpLabelClass="col-md-4" rpRequired="true"></rp-input>
      </div>
      <div class="form-group" style="margin-top:-7;">
              <rp-input rpType="text" rpLabel="Contact Person" [(rpModel)]="_obj.t3" rpLabelRequired='true' rpReadonly="false" rpClass="col-md-8 control-label" rpLabelClass="col-md-4" rpRequired="true"></rp-input>
      </div>
      <div class="form-group" style="margin-top:-7;">
              <rp-input rpType="text" rpLabel="Email" [(rpModel)]="_obj.t5" rpLabelRequired='true' rpReadonly="false" rpClass="col-md-8 control-label" rpLabelClass="col-md-4" rpRequired="true"></rp-input>
            </div>
      <div class="form-group" style="margin-top:-7;">
              <rp-input rpType="textarea" rpLabel=" Address " [(rpModel)]="_obj.t10" rpReadonly="false" rpClass="col-md-8 control-label " rpLabelClass="col-md-4"></rp-input>  
            </div>
      <div class="form-group" style="margin-top:-7;">
              <rp-input rpType="textarea" rpLabel=" Remark " [(rpModel)]="_obj.t11" rpReadonly="false" rpClass="col-md-8 control-label " rpLabelClass="col-md-4"></rp-input>       
            </div>       
          </div>
        </div>
        <div class="col-md-2">
          <div [hidden]="msghide">
            <h3 align="right"><b>{{msg}}</b></h3>
          </div>
        </div>
        </div>
      </form>
    </div>
  </div>
</div>

<div id="lu001popup2" class="modal fade" role="dialog">
  <div id="lu001popupsize" class="modal-dialog modal-lg">  
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 id="lu001popuptitle" class="modal-title">Account Info</h4>
      </div> 
      <div id="lu001popupbody" class="modal-body">
        <div class="col-md-5" style = "margin-top : -20px;float: right; !important">
			  <adminpager rpPageSizeMax="100" [(rpModel)]="_userobj.totalCount" (rpChanged)="changedPager($event)" ></adminpager> 
			  </div>
			  <div class="col-md-4" style="margin-left: -15px;">
            <div class="input-group">
                <input class="form-control input-sm" type="text" [(ngModel)]="_userobj.searchText" (keyup)="searchKeyup($event)" tabindex="3">
                <span class="input-group-btn input-md">
                    <button class="btn btn-sm btn-primary input-md" type="button" ((keyup)="searchKeyup($event)" (click)="goPopupList()"
                        tabindex="4">
                        <span class="glyphicon glyphicon-search"></span>Search
                    </button>
                </span>
            </div>
        </div> 
        <table class="table table-striped table-condensed table-hover tblborder">
          <thead>
            <tr>
              <th class="col-md-6">Phone No.</th>
              <th class="col-md-6">Name</th>                
            </tr>
          </thead>
          <tbody>
            <tr *ngFor="let obj of  _userobj.data">
              <td><a (click)="gotoWLAccount(obj.accountNumber)">{{obj.accountNumber}}</a></td>
              <td>{{obj.username}}</td> 
            </tr> 
          </tbody>
        </table>  
      </div>
    </div>
  </div>
</div>

<div id="glpopup" class="modal fade" role="dialog">
  <div id="lu001popupsize" class="modal-dialog modal-lg">  
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 id="lu001popuptitle" class="modal-title">GL Info</h4>
      </div> 
      <div id="lu001popupbody" class="modal-body"> 
        <label> GL Description : {{_customer.glDesp}} </label>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
  
<div id="sessionalert" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <p>{{sessionAlertMsg}}</p>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<div [hidden] = "_mflag">
  <div class="modal" id="loader"></div>
</div>   
  `
})

export class MerchantSetup {

  _features = [];
  private _selectref: number[] = [];

  subscription: Subscription;
  // Application Specific
  sub: any;
  msghide: boolean;
  messagehide: boolean;
  _obj = this.getDefaultObj();
  _note = "";
  confirmpwd = "";
  _output1 = "";
  _returnResult = { "keyResult": 0, "longResult": "", "state": "", "stringResult": "", "userId": "", "msgCode": "", "msgDesc": "" };
  //_returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
  _brCode = { "startBrCode": 0, "endBrCode": 0 };
  _mID = "";
  chkAcBr: "";
  msgclass: String;
  _featurevalue = "";
  _featurecaption = "";
  _ordervalue = "";
  _ordercaption = "";
  hasAcc = false;  

  //_obj = { "sessionID": "", "msgCode": "", "msgDesc": "", "sysKey": 0, "t1": "TBA", "t3": "", "t4": "", "t5": "", "t6": "", "t11": "", "t12": "", "userName": "", "userId": "", "p1": "AA", "accountNumber": "", "branchname": "", "createdUserID": "", "branchCode": "", "processingCode": "", "chkAccount": "false", "chkGL": "false", "feature": "", "data": [] };
  //_list = { "data": [{ "feature": "", "showfeature": "", "accountNumber": "", "branchCode": "", "order": "" }] };
  //_showlist = { "data": [{ "feature": "", "accountNumber": "", "branchCode": "","order":"" }] };
  _customer = { "customerID": "", "name": "", "nrc": "", "accountNo": "", "balance": "", "message": "", "glDesp": "", "chkAccount": "false", "chkGL": "false", "other": "false" };
  _obj1 = { "a1": "admin" };
  _key = "";
  _ans = "";
  catch = "";
  message = "";
  isvalidate = "";
  _classname = "";
  _mflag = false;
  //isGL: boolean;

  constructor(private ics: RpIntercomService, private _router: Router, private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences) {
    // RP Framework 
    this.subscription = ics.rpbean$.subscribe(x => { })
    if (!ics.getRole() || ics.getRole() == 0) {
      this._router.navigate(['/login']);
    } else {
      this._mflag = false;
      this.ics.confirmUpload(false);
      this._obj = this.getDefaultObj();
      this.checkSession();
      this.getBrCode();
      this.getAllBranch();
      this.getAllProcessingCode();
      this.getAllFeature();
      this.messagehide = true;
      jQuery("#mydelete").prop("disabled", true);
      jQuery("#mySave").prop("disabled", true);
      this.msghide = true;

    }
  }

  showMessageAlert(msg) {
    this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(
      params => {
        let cmd = params['cmd'];

        if (cmd != null && cmd != "" && cmd == "read") {
          let id = params['id'];
          this._key = id;
          this.goReadBysysKey(id);
        }
      }
    );
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  setImgUrl(str) {
    console.log("" + JSON.stringify(this.ics._imgurl + '/upload/smallImage/merchantImage/' + str))
    return this.ics._imgurl + '/upload/smallImage/merchantImage/' + str;
  }
  //uploadDataListResize = [];
  uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
  myimg = null;
  _fileName = '';
  _size = 0;
  temp = "";
  _file = null;
  flagImg = false;

  uploadedFileImage(event) {
    this._mflag = true;
   // this.uploadDataListResize = [];

    if (event.target.files.length == 1) {
     /*  if ((this._obj.uploadlist[0] != null)) {
        this.myimg = event.target.files[0];
        jQuery("#sessionalert").modal();
      } else { */
        this._fileName = event.target.files[0].name;
        this._size = event.target.files[0].size;
        if (this._size >= 1048576) { this.temp = (this._size / 1048576).toFixed(2) + " MB"; }
        else if (this._size >= 1024) { this.temp = (this._size / 1024).toFixed(2) + " KB"; }
        else if (this._size > 1) { this.temp = this._size + " bytes"; }
        else if (this._size == 1) { this.temp = this._size + " byte"; }
        else { this.temp = "0 bytes"; }

        this._file = event.target.files[0];

        var index = this._fileName.lastIndexOf(".");
        var imagename = this._fileName.substring(index);
        imagename = imagename.toLowerCase();
        if (imagename == ".jpg" || imagename == ".jpeg" || imagename == ".png") {
          console.log("imgUrl: " + this.ics._imgurl);
          let url = this.ics.cmsurl + 'fileAdm/fileupload?f=upload&fn=' + this._fileName + '&id=' + this.ics._profile.t1 + '&type=11' + '&imgUrl=' + this.ics._imgurl;
          this.http.upload(url, this._file).subscribe(
            data => {
              if (data.code === 'SUCCESS') {
                this.flagImg = true;
                this.uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
                this.uploadData.name = data.fileName;
                this.uploadData.url = data.url;
                //this._obj.uploadlist[0] = this.uploadData;
                //this.uploadDataListResize.push(data.sfileName);
                this._obj.t13 = this._fileName;
                this._obj.t14 = this.temp;
                this.showMsg("Upload Successful", true);

              } else {
                this.showMsg("Upload Unsuccessful Please Try Again...", false);
              }
              this._mflag = true;
            },
            error => {
              if (error._body.type == 'error') {
                alert("Connection Timed Out!");
              } else {
              }
            },
            () => { }
          );
        }
        else {
          this._mflag = true;
          jQuery("#imageUpload").val("");
          this.showMsg("Choose Image Associated", false);
        }
      //}
    }

  }	

  goReadBysysKey(p) {
    try {
      this._key = p;
      let json: any = this._key;
      let url: string = this.ics.cmsurl + 'serviceCMS/getMerchantDataByID?id=' + this._key + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;

      this.http.doGet(url).subscribe(
        data => {
          if (data != null) {
              this._obj = data;
              this._obj.chk = data.t12;
              for (let i = 0; i < this.ref._lov3.refFeature.length; i++) {
                for (let j = 0; j < data.data.length; j++) {
                  if (data.data[j].feature == this.ref._lov3.refFeature[i].value) {
                    this.ref._lov3.refFeature[i].value=data.data[j].feature;                 
                    break;
                  }
                }
              } 
          }

          this._output1 = JSON.stringify(data);
          this.confirmpwd = data.t2;

          if (this._obj.msgCode == '0016') {
            this.showMsg(this._obj.msgDesc,false);
          }
          this.hasAcc = true;
          jQuery("#mydelete").prop("disabled", false);
          jQuery("#mySave").prop("disabled", false);
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          } else {
          }
        },
        () => { }
      );
    } catch (e) {
      alert(e);
    }
  }

  getDefaultObj() {
    return {"uploadlist": [],"sessionID": "", "msgCode": "", "msgDesc": "", "sysKey": 0, "t1": "TBA", "t3": "", "t4": "", "t5": "", "t6": "", "t11": "", "t12": "", "userName": "", "userId": "", "p1": "AA", "accountNumber": "", "branchname": "", "createdUserID": "", "branchCode": "002", "processingCode": "", "chkAccount": "false", "chkGL": "false", "feature": "Topup", "order": "","chk":"0","t13":"","t14":"" };// "data": [],
  }

  checkGL(event) {
    if (event.target.checked) {
     // this.isGL = true;
      this._obj.accountNumber = '';
      jQuery("#mySave").prop("disabled", true);
      this.isvalidate = "";
    }
    else {
      //this.isGL = false;
      this._obj.accountNumber = '';
      jQuery("#mySave").prop("disabled", true);
      this.isvalidate = "";
    }
  }

  goDelete() {
    try {
      this._mflag = false;
      let url: string = this.ics.cmsurl + 'serviceCMS/deleteMerchant';
      this._obj.sessionID = this.ics._profile.sessionID;
      this._obj.createdUserID = this.ics._profile.userID;
      let json: any = this._obj;
      this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "TBA", "msgCode": "" };

      this.http.doPost(url, json).subscribe(
        data => {
          this._output1 = JSON.stringify(data);
          this._obj.userId = data.userId;
          this._returnResult = data;

          if (this._returnResult.state) {
            //this._obj = { "sessionID": "", "msgCode": "", "msgDesc": "", "sysKey": 0, "t1": "TBA", "t3": "", "t4": "", "t5": "", "t6": "", "t11": "", "t12": "", "userName": "", "userId": "", "p1": "AA", "accountNumber": "", "branchname": "", "createdUserID": "", "branchCode": "", "processingCode": "", "chkAccount": "false", "chkGL": "false", "feature": "", "order": "","chk":"0","t13":"","t14":"" };// "data": [],
            this._obj =  this.getDefaultObj();
            jQuery("#mydelete").prop("disabled", true);
            jQuery("#mySave").prop("disabled", false);

            this.isvalidate = "";
            this.showMsg(this._returnResult.msgDesc,true);
            this._selectref = [];
          } else {
            if (this._returnResult.msgCode == '0016') {
              this._returnResult.msgDesc = this._returnResult.msgDesc;
              this.showMsg(this._returnResult.msgDesc,false);
            } else {
              this.showMsg(this._returnResult.msgDesc,false);
            }
          }

          this._mflag = true;
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
        },
        () => { }
      );

      this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "", "msgCode": "" };
    } catch (e) {
      alert(e);
    }
  }

  validateEmail(mail) {
    if (/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(mail)) {
      return (true);
    }

    return (false);
  }

  goPost() {
    this._mflag = false;
    this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "", "msgCode": "" };

    if (!/^([0-9]{7,20})$/.test(this._obj.t4)) {
      this._returnResult.msgDesc = "Contact number is invalid";
      this.showMsg(this._returnResult.msgDesc,false);
      this._mflag = true;
      return false;
    }

    if (this._obj.accountNumber == '') {
      this._returnResult.msgDesc = "Please Fill Account Number";
      this.showMsg(this._returnResult.msgDesc,false);
      this._mflag = true;
      return false;
    }

    if (this._obj.feature == '' || this._obj.feature == 'undefined') {
      this._returnResult.msgDesc = "Please Select Feature";
      this.showMsg(this._returnResult.msgDesc,false);
      this._mflag = true;
      return false;
    }

    if (this._obj.branchCode == '' || this._obj.branchCode == 'undefined') {
      this._returnResult.msgDesc = "Please Select Branch Code";
      this.showMsg(this._returnResult.msgDesc,false);
      this._mflag = true;
      return false;
    }

    if (this._obj.t5.length > 0) {
      if (!this.validateEmail(this._obj.t5)) {
        this._returnResult.msgDesc = "Email address is invalid";
        this.showMsg(this._returnResult.msgDesc,false);
        this._mflag = true;
        return false;
      }
    }

    if (this._returnResult.msgDesc == '') {
      try {
        this._obj.createdUserID = this.ics._profile.userID;
        this._obj.sessionID = this.ics._profile.sessionID;
        this._mID = this._obj.userId;
        let url: string = this.ics.cmsurl + 'serviceCMS/saveMerchant';
        let json: any = this._obj;

        this.http.doPost(url, json).subscribe(
          data => {
            this._output1 = JSON.stringify(data);
            this._obj.userId = data.userId;
            this._returnResult = data;
            this.isvalidate = "";

            if (!(this._returnResult.state)) {
              if (this._returnResult.msgCode == '0016') {
                this.showMsg(this._returnResult.msgDesc,false);
              }

              this.isvalidate = "";
              this._obj.sysKey = this._returnResult.keyResult;
              this._obj.userId = this._mID;
              this.message = "";
              this._key = this._returnResult.keyResult + "";
            } else {
              jQuery("#mydelete").prop("disabled", false);
              this.showMsg(this._returnResult.msgDesc,true);
            }
            this._mflag = true;
          },
          error => {
            if (error._body.type == 'error') {
              alert("Connection Timed Out!");
            }
          },
          () => { }
        );
      } catch (e) {
        alert(e);
      }
    }
    else {
      this.showMsg(this._returnResult.msgDesc,false);
    }
  }

  goList() {
    this._router.navigate(['/Merchantlist']);
  }

  goNew() {
    this.clearData();
  }

  clearData() {
    this._obj = this.getDefaultObj();
    this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "", "msgCode": "" };
    jQuery("#mydelete").prop("disabled", true);
    this.isvalidate = "";
    //this.isGL = false;
    jQuery("#mySave").prop("disabled", true);
    this._selectref = [];
  }

  getAllProcessingCode() {
    try {
      this.http.doGet(this.ics.cmsurl + 'serviceAdmLOV/getAllProcessingCode?sessionID='+ this.ics._profile.sessionID + '&userID='+ this.ics._profile.userID).subscribe(
        data => {
          if (data != null && data != undefined) {
            this.ref._lov3.refProcessingCode = data.refProcessingCode;
          } else {
            this.ref._lov3.refProcessingCode = [{ "value": "", "caption": "-" }];
          }
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          } else {
          }
        },
        () => { }
      );
    } catch (e) {
      alert(e);
    }
  }

  getBrCode() {
    try {
      this.http.doGet(this.ics.cmsurl + 'serviceCMS/getBrCode?sessionID='+ this.ics._profile.sessionID + '&userID='+ this.ics._profile.userID).subscribe(
        data => {
          if(data != null){
            this._brCode = data;
          }else {
            this._brCode = { "startBrCode": 0, "endBrCode": 0 };
          }          
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          } else {
          }
        },
        () => { }
      );
    } catch (e) {
      alert(e);
    }
  }

  getAllBranch() {
    try {
      this.http.doGet(this.ics.cmsurl + 'serviceCMS/getAllBranch').subscribe(
        data => {
          if (data != null && data != undefined) {
            this.ref._lov3.ref018 = data.ref018;
            //this.ref._lov3 = data.ref018;
          } else {
            this.ref._lov3.ref018 = [{ "value": "", "caption": "Empty" }];
          }
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          } else {
          }
        },
        () => { }
      );
    } catch (e) {
      alert(e);
    }
  }
  _userobj = {
    "data": [{ "syskey": 0, "userId": "", "userName": "", "recordStatus": 0, "t1": "", "username": "", "n7": 0, "t7": "" }],
    "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0, "msgCode": "", "msgDesc": ""
  };
  _pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 0 };//atn
  changedPager(event) {//atn

    if (this._userobj.totalCount != 0) {
      this._pgobj = event;
      let current = this._userobj.currentPage;
      let size = this._userobj.pageSize;
      this._userobj.currentPage = this._pgobj.current;
      this._userobj.pageSize = this._pgobj.size;
      if (this._pgobj.current != current || this._pgobj.size != size) {
        this.goPopupList();
      }
    }
  }  
goPopupList() {
    try {
        this._mflag = false;
        this.http.doGet(this.ics.cmsurl + 'service001/showAccountList?searchVal=' +encodeURIComponent(this._userobj.searchText)+ '&pagesize=' + this._userobj.pageSize + '&currentpage=' + this._userobj.currentPage + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID +'&chk=' + this._obj.chk).subscribe(
            data => {
                if (data.msgCode == '0016') {
                    this.showMsg(data.msgDesc,false);
                }
                else {
                    if (data != null && data != undefined) {
                        this._pgobj.totalcount = data.totalCount;
                        this._userobj = data;
                         if (this._userobj.totalCount == 0) {
                            this.showMsg("Data not found!",false);
                        } 
                    }
                    jQuery("#lu001popup2").modal();
                }
                this._mflag = true;
            },
            error => {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            },
            () => { }
        );
    } catch (e) {
        alert(e);
    }

}

gotoWLAccount(data){
  this._obj.accountNumber = data;
  this.hasAcc = true;
  jQuery("#lu001popup2").modal('hide');
}

 /*  goCheck() {
    this._mflag = false;
    try {
      let p = this._obj.accountNumber;
      let check = '';
      if (this.isGL) {
        check = 'GL';
      } else {
        check = 'Account';
      }
      this.http.doGet(this.ics.cmsurl + 'service001/getCustomerNameandNrcByAccount?account=' + p + '&check=' + check +'&userID='+ this.ics._profile.userID +'&sessionID='+ this.ics._profile.sessionID).subscribe(
        data => {
          if (data.msgCode == '0000') {
            this._customer = data;
            this._obj.chkAccount = this._customer.chkAccount;
            this._obj.chkGL = this._customer.chkGL;
            this._obj.accountNumber = this._obj.accountNumber.trim();
            //this._list.data.push(null);
            if (this._customer.chkAccount) {
              jQuery("#mySave").prop("disabled", false);
              jQuery("#lu001popup2").modal();
              this.isvalidate = "Validate";
            } else if (this._customer.chkGL) {
              jQuery("#mySave").prop("disabled", false);
              jQuery("#glpopup").modal();
              this.isvalidate = "Validate";
            }
          } else {
            this._returnResult.msgDesc = "Invalid Account Number/GL";
            this.isvalidate = "";
            this.showMsg(this._returnResult.msgDesc,false);
            // this.messagealert();
          }
          this._mflag = true;
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          } else {
          }
        },
        () => { }
      );
    } catch (e) {
      alert(e);
    }
  } */

  getOrderList() {
    try {
      this._mflag = false;

      this.http.doGet(this.ics.cmsurl + 'serviceAdmLOV/getOrderList').subscribe(
        data => {
          if (data != null && data != undefined) {
            if (!(data.refOrder instanceof Array)) {//Object
              let m = [];
              m[0] = data.refOrder;
              this.ref._lov3.refOrder = m;
            } else {
              this.ref._lov3.refOrder = data.refOrder;
            }
          } else {
            this.ref._lov3.refOrder = [{ "value": "", "caption": "-" }];
          }

          this._mflag = true;
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          } else {
          }
        },
        () => { }
      );
    } catch (e) {
      alert(e);
    }
  }

  getAllFeature() {
    try {
      this._mflag = false;

      this.http.doGet(this.ics.cmsurl + 'serviceAdmLOV/getAllFeatures').subscribe(
        data => {
          if (data != null && data != undefined) {
            if (!(data.refFeature instanceof Array)) {//Object
              let m = [];
              m[0] = data.refFeature;
              this.ref._lov3.refFeature = m;
            } else {
              this.ref._lov3.refFeature = data.refFeature;
            }
          } else {
            this.ref._lov3.refFeature = [{ "value": "", "caption": "-" }];
          }
          this._obj.feature = this.ref._lov3.refFeature[0].value;

          this._mflag = true;
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          } else {
          }
        },
        () => { }
      );
    } catch (e) {
      alert(e);
    }
  }

  /* goAddAll(p) {
    this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "", "msgCode": "" };

    if (this.isvalidate == "") {
      this._returnResult.msgDesc = "Please check Account Number/GL again";
      this.showMsg(this._returnResult.msgDesc,false);
      return false;
    }

    if (this._obj.accountNumber == '') {
      this._returnResult.msgDesc = "Please Fill Account Number";
      this.showMsg(this._returnResult.msgDesc,false);
      return false;
    }

    if (this._obj.feature == '' || this._obj.feature == 'undefined') {
      this._returnResult.msgDesc = "Please Select Feature";
      this.showMsg(this._returnResult.msgDesc,false);
      return false;
    }

    if (this._obj.branchCode == '' || this._obj.branchCode == 'undefined') {
      this._returnResult.msgDesc = "Please Select Branch Code";
      this.showMsg(this._returnResult.msgDesc,false);
      return false;
    }

    if (this._customer.chkAccount) {
      let accbrcode = this._obj.accountNumber.substring(this._brCode.startBrCode, this._brCode.endBrCode)

      if (accbrcode != this._obj.branchCode) {
        this._returnResult.msgDesc = "Invalid Branch Code";
        this.showMsg(this._returnResult.msgDesc,false);
        return false;
      }
    }

    if (this._list.data.length > 0) {
      for (let i = 0; i < this._list.data.length; i++) {
        if (this._obj.feature == this._list.data[i].feature) {
          this._returnResult.msgDesc = "Duplicate Feature";
          this.showMsg(this._returnResult.msgDesc,false);
          return false;
        }
      }
    }

    if (this._returnResult.msgDesc == '') {
      if (this._obj.feature != "") {
        for (let i = 0; i < this.ref._lov3.refFeature.length; i++) {
          if (this._obj.feature == this.ref._lov3.refFeature[i].value) {
            this._featurecaption = this.ref._lov3.refFeature[i].caption;
            break;
          }
        }
      }

      if (this._obj.order != "") {
        for (let i = 0; i < this.ref._lov3.refOrder.length; i++) {
          if (this._obj.order == this.ref._lov3.refOrder[i].value) {
            this._ordercaption = this.ref._lov3.refOrder[i].caption;
            break;
          }
        }
      }

      //this._showlist.data.push({ "feature": this._featurecaption, "accountNumber": this._obj.accountNumber, "branchCode": this._obj.branchCode,"order":this._ordercaption });
      this._list.data.push({ "feature": this._obj.feature, "showfeature": this._featurecaption, "accountNumber": this._obj.accountNumber, "branchCode": "02", "order": "1" });//this._obj.branchCode
      this.isvalidate = '';
      this._obj.accountNumber='';
      this._obj.branchCode='';
      this._obj.feature='';
      this.isGL=false;

    } else {
      this.showMsg(this._returnResult.msgDesc,false);
    }

  }  */

  /* goRemove(i) {
    let sindex = this._list.data.indexOf(i);
    this._list.data.splice(sindex, 1);
  } */

  /* checkSession() {
    try {
      let url: string = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
      this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "TBA", "msgCode": "" };

      this.http.doGet(url).subscribe(
        data => {
          if (data.msgCode == '0016') {
            this._returnResult.msgDesc = data.msgDesc;
            this.showMessage();
          }
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
        },
        () => { }
      );
    } catch (e) {
      alert(e);
    }
  } */

  // about session validation
  sessionTimeoutMsg = "";
  _sessionObj = this.getSessionObj();
  getSessionObj() {
    return { "sessionID": "", "userID": "" };
  }

  checkSession() {
    try {
      let url: string = this.ics._apiurl + 'service001/checkSessionTime';

      this._sessionObj.sessionID = this.ics._profile.sessionID;
      this._sessionObj.userID = this.ics._profile.userID;
      let json: any = this._sessionObj;

      this.http.doPost(url, json).subscribe(
        data => {
          if (data != null) {
            if (data.code == "0016") {
              this.showMsg(data.desc,false);
              this.logout();
            }

            if (data.code == "0014") {
              this.showMsg(data.desc,false);
            }
          }
        },
        error => {
          if (error._body.type == "error") {
            alert("Connection Timed Out.");
          }
        },
        () => { }
      );
    } catch (e) {
      alert("Invalid URL.");
    }
  }
  radioChecked(){
    this._obj.accountNumber = "";
    this._userobj.searchText="";
  }

  showMsgAlert(msg) {
    this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
  }

  logout() {
    this._router.navigate(['/login']);
  }
  showMsg(msg, bool) {
    if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
    if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
    if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
  }

}