import { Component, Input, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { enableProdMode } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
// RP Framework
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpInputComponent } from '../framework/rp-input.component';
import { RpHttpService } from '../framework/rp-http.service';
import { RpBean } from '../framework/rp-bean';
declare var jQuery: any;
// Application Specific
import { Pager } from '../util/pager.component';
import { AdminPager } from '../util/adminpager.component';
import { ClientUtil } from '../util/rp-client.util';
enableProdMode();
@Component({
    selector: 'smssettingsetup-list',
    template: ` 
    <div class="container col-md-12 col-sm-12 col-xs-12">
	<form class="form-horizontal">
		<legend>Message Setting List</legend>
		<div class="cardview list-height">  
			<div class="row col-md-12">
				<div class="row col-md-3">
					<div  class="input-group">
						<span class="input-group-btn">
							<button class="btn btn-sm btn-primary" type="button" (click)="goNew();">New</button>
						</span> 
						<input id="textinput" name="textinput" type="text"  placeholder="Search" [(ngModel)]="_smsobj.searchText" (keyup)="searchKeyup($event)" maxlength="30" class="form-control input-sm">
						<span class="input-group-btn">
							<button class="btn btn-sm btn-primary" type="button" (click)="Searching()" >
								<span class="glyphicon glyphicon-search"></span>Search
							</button>
						</span>        
					</div> 
				</div>		
                    <div class="pagerright">
					    <adminpager rpPageSizeMax="100" [(rpModel)]="_smsobj.totalCount" (rpChanged)="changedPager($event)" ></adminpager> 
					</div>
            </div>
            <div *ngIf="_showListing">
    		  <table class="table table-striped table-condensed table-hover tblborder" id="tblstyle">
    			<thead>
    			 <tr>
    				<th> ID. </th>
    				<th>Service </th>
    				<th>Function</th>
    				<th>Site</th>
            <th>From Message</th>
            <th>To Message</th>
    				<th class="center">Merchant ID</th>
    				<!--<th>Operator</th>-->
            <th>Active</th>
            <!--<th>Language</th>-->     
    			</tr>
    		  </thead>
    		  <tbody>
    			<tr *ngFor="let obj of _smsobj.data">
    				<td><a (click)="goto(obj.id)">{{obj.id}}</a></td>
    				<td>{{obj.serviceDesc}}</td>
    				<td>{{obj.funDesc}}</td>
    				<td *ngIf ="obj.from == '1' " >From</td>
            <td *ngIf ="obj.to == '1' " >To</td>
            <td *ngIf ="obj.both == '1' " >Both</td>
    				<td>{{obj.fromMsg}} </td>
    				<td > {{obj.toMsg}} </td>
    				<td class="center">{{obj.merchantID}}</td>
    				<!--<td >{{obj.operatorType}}</td>-->
    				<td *ngIf ="obj.active == 0">No</td>
            <td *ngIf ="obj.active == 1">Yes</td>
            <!--<td *ngIf="obj.language==''"></td>
            <td *ngIf="obj.language==null"></td>
            <td *ngIf ="obj.language == 1">English</td>
            <td *ngIf ="obj.language == 2">Myanmar</td>-->
    			</tr>  
    		  </tbody>
              </table>
            </div>
            <div align="center" *ngIf="_showListing" >
                Total {{_smsobj.totalCount}}
            </div>
		</div> 
	</form>
</div>
    <div [hidden] = "_mflag">
        <div class="loader modal"id="loader"></div>
    </div>
   `
})

export class SMSSettingSetupListComponent {
    // RP Framework 
    subscription: Subscription;
    // Application Specific
    _searchVal = ""; // simple search
    _flagas = true; // flag advance search
    _showListing=false;
    _col_list = [{ "col_no": "1", "flag": false }, { "col_no": "2", "flag": false }]
    _sorting = { _sort_type: "asc", _sort_col: "1" };
    _mflag = true;
    _util: ClientUtil = new ClientUtil();
    sessionAlertMsg = ""; _recordhide: boolean; _msghide: boolean;
    _smsobj = {
        "data": [{"code":"","desc":"","id":0, "syskey": 0, "serviceCode": "", "serviceDesc": "", "funCode": "", "funDesc": "", "site": "", "from": "", "to": "", "fromMsg": "", "toMsg": "", "message": "", "merchantID": "", "operatorType": "", "active": "", "sessionID": "", "userID": "" }],
        "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0
      };
    _pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 0 };

    constructor(private ics: RpIntercomService, private _router: Router, private http: RpHttpService) {
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(x => { })
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        } else {
            this.search();
        }
    }
    // list sorting part
    changeDefault() {
        for (let i = 0; i < this._col_list.length; i++) {
            this._col_list[i].flag = false;
        }
    }
    addSort(e) {
        for (let i = 0; i < this._col_list.length; i++) {
            if (this._col_list[i].col_no == e) {
                let _tmp_flag = this._col_list[i].flag;
                this.changeDefault();
                this._col_list[i].flag = !_tmp_flag;
                this._sorting._sort_type = !_tmp_flag ? "desc" : "asc";
                this._sorting._sort_col = e;
                break;
            }
        }
    }
    changedPager(event) {
        if (this._smsobj.totalCount != 0) {
            this._pgobj = event;
            let current = this._smsobj.currentPage;
            let size = this._smsobj.pageSize;
            this._smsobj.currentPage = this._pgobj.current;
            this._smsobj.pageSize = this._pgobj.size;
            if (this._pgobj.current != current || this._pgobj.size != size) {
                this.search();
            }
        }
    }

    search() {
        try {
            this._mflag = false;
            this.http.doGet(this.ics.cmsurl + 'serviceCMS/getSMSSettingList?searchVal=' + this._smsobj.searchText + '&pagesize=' + this._smsobj.pageSize + '&currentpage=' + this._smsobj.currentPage + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(
              response => {
                this._mflag = true;      
                if (response.msgCode == "0016") {
                  this.sessionAlertMsg = response.msgDesc;
                  this.showMessage(response.msgDesc,false);
                } else {
                  if (response.totalCount == 0) {
                    this._smsobj = response;
                    this.showMessage("Data not found!",false);
                  }
                  if (response.data != null) {
                      this._showListing=true;
                    if (!(response.data instanceof Array)) {
                      let m = [];
                      m[0] = response.data;
                      this._smsobj.data = m;
                      this._smsobj.totalCount = response.totalCount;
                    }
                    else {
                      this._smsobj = response;
                    }
                  }
                }
              },
              error => {
                if (error._body.type == 'error') {
                  alert("Connection Timed Out!");
                }
              }, () => { });
          } catch (e) {
            alert(e);
          }
    }

    searchKeyup(e: any) {
        if (e.which == 13) { // check enter key
            this._smsobj.currentPage = 1;
            this._msghide = true;
            this._recordhide = false;
            this.search();
        }
    }

    Searching() {
        this._smsobj.currentPage = 1;
        this._msghide = true;
        this._recordhide = false;
        this.search();
    }

    goto(p) {
        this._router.navigate(['/SMS Setting', 'read', p]);
    }

   // showMessage() {
    //    jQuery("#sessionalert").modal();
    //    Observable.timer(3000).subscribe(x => {
     ///       this.goLogOut();
    //    });
    //}

    goLogOut() {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    }

    goNew() {
        this._router.navigate(['/SMS Setting', 'new']);
    }
    showMessage(msg, bool) {
        if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
        if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
        if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "primary", "t3": msg }); }
    }
}