import { Component, Input, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { enableProdMode } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
// RP Framework
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpInputComponent } from '../framework/rp-input.component';
import { RpHttpService } from '../framework/rp-http.service';
import { RpBean } from '../framework/rp-bean';
declare var jQuery: any;
// Application Specific
import { Pager } from '../util/pager.component';
import { AdminPager } from '../util/adminpager.component';
import { ClientUtil } from '../util/rp-client.util';
enableProdMode();
@Component({
    selector: 'merchant-list',
    template: ` 
    <div class="container-fluid">
	<form class="form-horizontal"> 
        <legend>Merchant List</legend>
		<div class="cardview list-height">		
			<div class="row col-md-12">
				<div class="row col-md-3">
					<div  class="input-group">
						<span class="input-group-btn">
							<button class="btn btn-sm btn-primary" type="button" (click)="goNew();">New</button>
						</span> 
						<input id="textinput" name="textinput" type="text"  placeholder="Search" [(ngModel)]="_merchantobj.searchText" (keyup)="searchKeyup($event)" [ngModelOptions]="{standalone: true}" maxlength="30" class="form-control input-sm">
						<span class="input-group-btn">
							<button class="btn btn-sm btn-primary" type="button" (click)="Searching()" >
								<span class="glyphicon glyphicon-search"></span>Search
							</button>
						</span>        
					</div> 
				</div>		
				  <div class="pagerright">				  
					    <adminpager rpPageSizeMax="100" [(rpModel)]="_merchantobj.totalCount" (rpChanged)="changedPager($event)" ></adminpager> 
					</div>
            </div>
            <div class="row col-md-12" style="overflow-x:auto">
			<table class="table table-striped table-condensed table-hover tblborder" id="tblstyle">
				<thead>
					<tr>
						<th class="center">Merchant ID</th>
						<th>Merchant Name</th>
						<th>Contact Person</th>
						<th>Contact No.</th>
						<th>Email</th>
						<th>Address</th>
						<th>Remark</th>   
					</tr>
				</thead>
				<tbody>
					<tr *ngFor="let obj of  _merchantobj.data">
						<td class='center'><a (click)="goto(obj.userId)">{{obj.userId}}</a></td>
						<td>{{obj.userName}}</td>
						<td>{{obj.t3}}</td>
						<td>{{obj.t4}}</td>
						<td>{{obj.t5}}</td>
						<td>{{obj.t10}}</td>
						<td>{{obj.t11}}</td>
					</tr>  
				</tbody>
            </table>
            </div>
		</div>
	</form>
</div> 
  
    <div [hidden] = "_mflag">
        <div class="modal" id="loader"></div>
    </div>
   `
})

export class MerchantList {

    // RP Framework 
    subscription: Subscription;
    // Application Specific
    _searchVal = ""; // simple search
    _flagas = true; // flag advance search
    _col_list = [{ "col_no": "1", "flag": false }, { "col_no": "2", "flag": false }]
    _sorting = { _sort_type: "asc", _sort_col: "1" };
    _mflag = true;
    _util: ClientUtil = new ClientUtil();
    sessionAlertMsg = ""; _recordhide: boolean; _msghide: boolean;
    _merchantobj = {
        "data": [{ "syskey": 0, "t1": "TBA", "t3": "", "t4": "", "t5": "", "t6": "", "t11": "", "t12": "", "userName": "", "userId": "", "accountNumber": "" }],
        "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0
    };
    _pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 0 };

    constructor(private ics: RpIntercomService, private _router: Router, private http: RpHttpService) {
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(x => { })

        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        } else {
            // jQuery("#loader").modal('hide');
            this._mflag = false;
            this.search();
        }
    }

    // list sorting part
    changeDefault() {
        for (let i = 0; i < this._col_list.length; i++) {
            this._col_list[i].flag = false;
        }
    }

    addSort(e) {
        for (let i = 0; i < this._col_list.length; i++) {
            if (this._col_list[i].col_no == e) {
                let _tmp_flag = this._col_list[i].flag;
                this.changeDefault();
                this._col_list[i].flag = !_tmp_flag;
                this._sorting._sort_type = !_tmp_flag ? "desc" : "asc";
                this._sorting._sort_col = e;
                break;
            }
        }
    }

    changedPager(event) {
        if (this._merchantobj.totalCount != 0) {
            this._pgobj = event;
            let current = this._merchantobj.currentPage;
            let size = this._merchantobj.pageSize;
            this._merchantobj.currentPage = this._pgobj.current;
            this._merchantobj.pageSize = this._pgobj.size;

            if (this._pgobj.current != current || this._pgobj.size != size) {
                this.search();
            }
        }
    }

    search() {
        try {
            this._mflag = false;

            this.http.doGet(this.ics.cmsurl + 'serviceCMS/getMerchantList?searchVal=' + encodeURIComponent(this._merchantobj.searchText) + '&pagesize=' + this._merchantobj.pageSize + '&currentpage=' + this._merchantobj.currentPage + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(
                data => {
                    if (data.msgCode == '0016') {
                        this.sessionAlertMsg = data.msgDesc;
                        this.showMessage(data.msgDesc,false);
                    } else {
                        if (data != null && data != undefined) {
                            this._pgobj.totalcount = data.totalCount;
                            this._merchantobj = data;

                            if (this._merchantobj.totalCount == 0) {
                                this.showMessage("Data not found!",false);
                            }
                        }
                    }

                    this._mflag = true;
                },
                error => {
                    if (error._body.type == 'error') {
                        alert("Connection Timed Out!");
                    }
                },
                () => { }
            );
        } catch (e) {
            alert(e);
        }
    }

    searchKeyup(e: any) {
        if (e.which == 13) { // check enter key
            this._merchantobj.currentPage = 1;
            this._msghide = true;
            this._recordhide = false;
            this.search();
        }
    }

    Searching() {
        this._merchantobj.currentPage = 1;
        this._msghide = true;
        this._recordhide = false;
        this.search();
    }

    goto(p) {
        this._router.navigate(['/Merchant Profile', 'read', p]);
    }

    //showMessage() {
    //    jQuery("#sessionalert").modal();
    //    Observable.timer(3000).subscribe(x => {
    //        this.goLogOut();
    //    });
    //}

    goLogOut() {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    }

    goNew() {
        this._router.navigate(['/Merchant Profile', 'new']);
    }
    showMessage(msg, bool) {
        if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
        if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
        if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "primary", "t3": msg }); }
    }

}