import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyDatePickerModule } from 'mydatepicker';
import { VideoComponent } from './video.component';
import { VideoList } from './video-list.component';
import { videoViewComponent } from './video-view.component';
import { ContentMenu } from './contentmenu.component';
import { ContentMenuListComponent } from './contentmenu-list.component';
import { EduViewComponent } from './edu-view.component';
import { SMSSettingComponent } from './smssettingsetup.component';
import { SMSSettingSetupListComponent } from './SMSSettingSetupList.component';
import { MerchantCommMappingSetup } from './merchantcommmappingsetup.component';
import { MerchantCommMappingSetupList } from './merchantcommmappingsetup-list.component';
import { FlexCommRateSetup } from './flexcommratesetup.component';
import { FlexCommRateSetupList } from './flexcommratesetup-list.component';  
import { MerchantSetup } from './merchant-setup.component';
import { MerchantList } from './merchant-list.component';

const AdministratorRoutes: Routes = [
  { path: 'contentmenu', component: ContentMenu },
  { path: 'contentmenu/:cmd', component: ContentMenu },
  { path: 'contentmenu/:cmd/:id', component: ContentMenu },
  { path: 'contentmenulist', component: ContentMenuListComponent },
  { path: 'contentmenulist/:cmd', component: ContentMenuListComponent },
  { path: 'contentmenulist/:cmd/:id', component: ContentMenuListComponent },
  { path: 'eduView/:cmd/:id', component: EduViewComponent },
  { path: 'videomenu', component: VideoComponent },
  { path: 'videomenu/:cmd', component: VideoComponent },
  { path: 'videomenu/:cmd/:id', component: VideoComponent },
  { path: 'videoList', component: VideoList },
  { path: 'videoAdminView', component: videoViewComponent },
  { path: 'videoAdminView/:cmd/:id', component: videoViewComponent },

  { path: 'SMS Setting', component: SMSSettingComponent },
  { path: 'SMS Setting/:cmd', component: SMSSettingComponent },
  { path: 'SMS Setting/:cmd/:id', component: SMSSettingComponent },
  { path: 'smssettinglist', component: SMSSettingSetupListComponent },

  { path: 'Merchant Comm Mapping', component: MerchantCommMappingSetup },
  { path: 'Merchant Comm Mapping/:cmd', component: MerchantCommMappingSetup },
  { path: 'Merchant Comm Mapping/:cmd/:id', component: MerchantCommMappingSetup },
  { path: 'merchantcommmappinglist', component: MerchantCommMappingSetupList },

  { path: 'Flex Comm Setup', component: FlexCommRateSetup },
  { path: 'Flex Comm Setup/:cmd', component: FlexCommRateSetup },
  { path: 'Flex Comm Setup/:cmd/:id', component: FlexCommRateSetup },
  { path: 'flexcommratelist', component: FlexCommRateSetupList },

  { path: 'Merchant Profile', component: MerchantSetup },
  { path: 'Merchant Profile/:cmd', component: MerchantSetup },
  { path: 'Merchant Profile/:cmd/:id', component: MerchantSetup },
  { path: 'Merchantlist', component: MerchantList },
];

export const AdministratorRouting: ModuleWithProviders = RouterModule.forChild(AdministratorRoutes);