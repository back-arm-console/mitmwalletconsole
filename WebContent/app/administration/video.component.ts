
import { Component, OnInit, OnDestroy, ElementRef, ViewChild, Input, Renderer } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { enableProdMode } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpInputComponent } from '../framework/rp-input.component';
import { RpHttpService } from '../framework/rp-http.service';
import { RpBean } from '../framework/rp-bean';
import { RpReferences } from '../framework/rp-references';
import { ClientUtil } from '../util/rp-client.util';
declare var jQuery: any;
declare function capture(fileName: String): any;
declare function save(): any;
declare function convertCanvasToImage(): any;
declare function loadData(): any;
declare function displayImage(fileName: String): any;
declare function isMyanmar(pwd: String): boolean;
declare function isZawgyi(pwd: String): boolean;
declare function ZgtoUni(pwd: String): string;
declare function isUnicode_my(pwd: String): boolean;
declare function UnitoZg(pwd: String): string;
declare function ZgtoUni(pwd: String): string;
declare function systemFont(): string;
declare function wdth(pwd: String): string;
declare function identifyFont(pwd: String): string;

enableProdMode();
@Component({
  selector: 'fmr-video',
  template: `
  <div class="container-fluid col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
	<form class="form-horizontal" ngNoForm>
		<legend>Video</legend>
			<div class="cardview list-height">  
				<div class="col-md-12" style="margin-bottom: 10px;">
				  <button class="btn btn-sm btn-primary" type="button" (click)="goList()">List</button>
				  <button class="btn btn-sm btn-primary" [disabled]="flagnew" id="new" type="button" (click)="goNew()">New</button>
				  <button class="btn btn-sm btn-primary" [disabled]="flagsave" id="save" type="button" (click)="goPost()">Save</button>
				  <button class="btn btn-sm btn-primary" [disabled]="flagdelete" id="delete" type="button" (click)="goDelete();">Delete</button>
				  <!--<button *ngIf="!flagview" class="btn btn-sm btn-primary" type="button" (click)="goView(_viewsk);">View</button>-->
				</div>
  
				<div class="col-md-12">
					<div class="form-group">
					<label class="col-md-2">Title</label>
						<div class="col-md-4">
						<input type="text" [(ngModel)]="_obj.t1" class="form-control input-sm uni" maxlength="255"/>
						</div>
					<label class="col-md-2">Status</label>
						<div class="col-md-4">
							<select [(ngModel)]="_obj.n7"  class="form-control input-sm" [ngModelOptions]="{standalone: true}" required>
							<option *ngFor="let c of lovstatusType" value="{{c.value}}">{{c.caption}}</option>
							</select>
						</div>
					</div>   
						  
					<div class="form-group">
						<label class="col-md-2">Content</label>
						<div class="col-md-4 uni">
						<textarea type="text" *ngIf="td" [(ngModel)]="_obj.t2" class="form-control input-md uni" rows="6"></textarea>
						<textarea type="text" *ngIf="!td" [(ngModel)]="_obj.t2" class="form-control input-md uni" rows="6"></textarea>										
            </div>	
            <div style="margin-bottom: 38px !important;">														
							<rp-input rpLabelClass = "col-md-2" [(rpModel)]="_obj.n10" (change)="changeBrowe(_obj.n10)" rpRequired="true" rpType="videostatus" rpLabel="Type"></rp-input>
            </div>
            <div style="margin-bottom: 76px !important;">
              <label class="col-md-2">File Size</label>
							<div class="col-md-4">
							<input type="text" [(ngModel)]="_obj.t6" class="form-control input-sm" maxlength="50" disabled />
							</div>
            </div>
            <div>
              <label class="col-md-2">Video Duration Time</label>
							<div class="col-md-4">
							  <input type="text" [(ngModel)]="_obj.t7" class="form-control input-sm" maxlength="50" [disabled]='_obj.n10==0'/>
              </div>
            </div>  
					</div>
					<div class="form-group"> 
						<label class="col-md-2" [hidden]="flagImage" for="imageUploadid">Image Associated </label>
							<div class="col-md-4" [hidden]="flagImage" id="imageUploadid">
							<input type="file" id="imageUpload"  class="file" (change)="uploadedFileImage($event)"/><!--#myFileInput-->
							<div class="input-group">
							<input type="text" id="imageUpload1" class="form-control input-sm" placeholder="No file chosen" maxlength="100">
							<span class="input-group-btn">
							<button class="browse btn btn-sm btn-primary" type="button"><i class="glyphicon glyphicon-search"></i> Browse</button>                         
							</span>
							</div>
								<div class="col-md-4">(jpeg, png, jpg) </div>                  
							</div> 
							
						<label class="col-md-2" [hidden]="flagURL">URL Link</label>
							<div class="col-md-4" [hidden]="flagURL">
							<input type="text" [(ngModel)]="_obj.t8" class="form-control input-sm" />
							</div>
				
						<label class="col-md-2" [hidden]="flagvideo" for="uploadVideoid">Video Associated </label>
							<div class="col-md-4" [hidden]="flagvideo" id="uploadVideoid">
							  <input type="file" id="uploadVideo" class="file"  (change)="uploadedFile($event)" placeholder="Upload video..."/>
							<div class="input-group">
							<input type="text"  id="uploadVideo1" class="form-control input-sm" placeholder="No file chosen"  maxlength="100">
							<span class="input-group-btn">
							<button class="browse btn btn-sm btn-primary" type="button"><i class="glyphicon glyphicon-search"></i> Browse</button>
							</span>
							</div>
							<div class="col-md-4" *ngIf='flagvideo==false'>(mp4)</div> 
							</div>
						
					</div>
           			  					  			  						
				</div>
       
				<div class="col-md-6" *ngIf="_obj.uploadlist.length!=0">
					<div class="col-md-4"></div>
					<div calss="col-md-8">
						<div><!--*ngFor="let img of _obj.uploadlist" -->
						  <img src="{{setImgUrl(_obj.uploadlist[0].name)}}" onError="this.src='./image/image_not_found.png';" alt={{img}} height="240" width="400"/><!--{{setImgUrl(_obj.uploadlist[0].name)}}-->
						</div>
					</div>
				</div>
				<div class="col-md-6" *ngIf="_obj.videoUpload.length!=0"></div>
				<div class="col-md-6" *ngIf="_obj.videoUpload.length!=0">
					<div class="col-md-4"></div>
					<div calss="col-md-8">
						<div *ngIf='flagURL==true'>
							<div *ngFor="let img of _obj.videoUpload;let num = index" >																		
								<video #videoPlayer id="{{img}}" type="video/mp4" (loadedmetadata)="onMetadata($event, videoPlayer)" controls height="240" width="400">
								<source src="{{videoUrl}}/{{setVideoUrl(img)}}" type="video/mp4" />							
								</video>
								<canvas id="canvas" style="display:none"></canvas>
							</div>
						</div>
					</div>
				</div>
		</div>	
    </form>
</div>
<div id="sessionalert" class="modal" tabindex="-1" role="dialog">
<div class="modal-dialog modal-sm" id="rootpopupsize">
  <div class="modal-content">
  <div class="modal-header">
  
  <button type="button" class="close" data-dismiss="modal" aria-label="Close" (click)="goNo()">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
    <div class="modal-body">
      <p>Do you want to Overwrite?</p>
    </div>
    <div class="modal-footer">
      <button class="btn btn-sm btn-primary" type="button" (click)="goYes()">OK</button>
      <button class="btn btn-sm btn-primary" type="button" (click)="goNo()">Cancle</button>
    </div>
  </div>
</div>
</div>
<div id="sessionalert1" class="modal" tabindex="-1" role="dialog">
<div class="modal-dialog modal-sm" id="rootpopupsize">
  <div class="modal-content">
  <div class="modal-header">
  
  <button type="button" class="close" data-dismiss="modal" aria-label="Close" (click)="goCancle()">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
    <div class="modal-body">
      <p>Do you want to Overwrite?</p>
    </div>
    <div class="modal-footer">
      <button class="btn btn-sm btn-primary" type="button" (click)="goOK()">OK</button>
      <button class="btn btn-sm btn-primary" type="button" (click)="goCancle()">Cancle</button>
    </div>
  </div>
</div>
</div>
  <!-- processing image modal -->
  <div id="imagepopup" class="modal fade" role="dialog" style=" margin-top: 200px; margin-left: 150px;">
    <div id="imagepopupsize" class="modal-dialog modal-lg" style="width : 240px">
      <div class="modal-content">
        <div id="imagepopupbody" class="modal-body">
          <img src="image/processing.gif" style="padding-top:30px;">
        </div>
      </div>
    </div>
  </div>
  <div [hidden]="_mflag">
  <div  id="loader" class="modal" ></div>
`
})

export class VideoComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  sub: any;
  td: boolean = true;
  @ViewChild('myFileInput')
  myInputVariable: ElementRef;
  _viewsk = "";
  alert: boolean = false;
  stateval = "";
  statusval = 0;
  _mflag: boolean;
  flagview = true;
  flagvideo = false;
  flagURL = true;
  flagImage = false;
  photoObj: any;
  //myimg=[];
  flagnew = true;
  flagsave = true;
  flagdelete = true;
  dates = { "_date": null };
  uploadDataList = [];
  uploadDataListResize = [];
  uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
  _sessionObj = this.getSessionObj();
  getSessionObj() {
    return { "sessionID": "", "userID": "","lovDesc":"" };
  }
  lovdomainType = [];
  _img = "";
  _file = null;
  myimg = null;
  _fileName = '';
  _Time = 0;
  _util: ClientUtil = new ClientUtil();
  _datepickerOpts: any;
  _time = "";
  flagcanvas = true;
  _imageObj = [{ "value": "", "caption": "", "flag": false }];
  alertTime = "";
  //_crop = [{ "value": "", "caption": "", "flag": false }];
  //_obj = { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": null, "modifiedDate": "", "modifiedTime": null, "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "Video", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [], "videoUpload": [], "cropdata": null, "agrodata": null, "ferdata": null, "towndata": null, "crop": null, "fert": null, "agro": null, "addTown": null, "resizelist": [], "modifiedUserId": "", "modifiedUserName": "", "uploadlist": [] };
  _obj = this. getDefaultObj();
  getDefaultObj(){
  return { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": "", "modifiedDate": "", "modifiedTime": "", "alertDate": "", "alertTime": "","sessionId":"", "userId": "", "userName": "", "modifiedUserId": "", "modifiedUserName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "Video", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "temp": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [], "videoUpload": [], "comData": [], "cropdata": [], "agrodata": [], "ferdata": [], "towndata": [], "uploadlist": [], "resizelist": [] };
}
  _roleval = "";
  _checkcw = false;
  _checkeditor = false;
  _checkpubisher = false;
  _checkmaster = false;
  _checkadmin = false;
  flagImg = false;
  lovstatusType = [];
  videoUrl: any;
  _size = 0;
  temp = "";
  tests = "";
  noti = { "flag": false };
  alertDate = { "_date": null };
  constructor(private el: ElementRef, private renderer: Renderer, private ics: RpIntercomService, private _router: Router, private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences) {
    if (!ics.getRole() || ics.getRole() == 0) this._router.navigate(['/login']);
    this._roleval = ics._profile.t1;
    this._datepickerOpts = this.ics._datepickerOpts;
    this._datepickerOpts.format = 'dd/mm/yyyy';
    this.alertDate._date = new Date();
    this.flagdelete = true;
    this.flagnew = false;
    this.flagsave = false;
    this.flagview = true;
    this.flagURL = true;
    this.alert = true;
    this.flagImage = false;
    this.flagvideo = false;
    this._obj.n7 = 5;
    this.getStatusType();
    this._time = this._util.getTodayTime();
    this._obj.createdTime = this._time;
    this._obj.modifiedTime = this._time;
    this.videoUrl = this.ics._imgurl;
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let cmd = params['cmd'];
      if (cmd != null && cmd != "" && cmd == "new") {
        this.goNew();
      } else if (cmd != null && cmd != "" && cmd == "read") {
        let idsys = params['id'];
        this.goGet(idsys);
        this._viewsk = idsys;
      }


    });
  }
  updateCheckedNoti(event) {
    if (event.target.checked) {
      this._obj.n9 = 1;
    }
    else {
      this._obj.n9 = 0;
    }
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  goGet(p) {
    this.td = false;
    this._mflag = false;
    this.flagview = false;
    this.flagdelete = false;
    this.flagnew = false;
    this.flagsave = false;
    this._obj.createdTime = this._time;
    this._obj.modifiedTime = this._time;

    this.http.doGet(this.ics.cmsurl + 'serviceVideoAdm/readBySyskeyNew?key=' + p +'&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(
      data => {
        this._obj = data;
        if (this._obj != null) {
        this.alertTime = this._obj.alertTime;
        console.log("data obj=" + JSON.stringify(data));
        if (this._obj.n10 == 0) {
          this.flagvideo = false;
          this.flagURL = true;
          this.flagImage = false;
        }
        if (this._obj.n10 == 1 || this._obj.n10 == 2) {
          this.flagURL = false;
          this.flagvideo = true;
          this.flagImage = false;
        }
        for (let i = 0; i < this._obj.upload.length; i++) {
          var index = this._obj.upload[i].lastIndexOf(".");
          var fe = this._obj.upload[i].substring(index);

        }
        if (this._obj.resizelist.length > 0) {
          this.uploadDataListResize = this._obj.resizelist;
          this._obj.resizelist = [];
        }
        if (this._obj.n9 == 1) {
          this.noti.flag = true;
        }

        if (this._obj.uploadlist[0].name != "") {
          this.flagImg = true;
          jQuery("#uploadVideo1").val(this._obj.videoUpload[0]);
          jQuery("#imageUpload1").val(this._obj.uploadlist[0].name);

        }
        else {
          this.flagImg = false;
        }
      }else this._obj = this.getDefaultObj();   
        this._mflag = true;
      },
      error => { },
      () => { }
    );
  }
  goNew() {
    this.td = true;
    this._datepickerOpts = this.ics._datepickerOpts;
    this._datepickerOpts.format = 'dd/mm/yyyy';
    this.alertDate._date = new Date();
    this._time = this._util.getTodayTime();
    this.flagvideo = false;
    this.flagURL = true;
    //this.flagImage = true;
    this.flagImage = false;
    jQuery("#uploadVideo1").val("");
    jQuery("#imageUpload1").val("");
    jQuery("#save").prop("disabled", false);
    this.flagview = true;
    this.flagdelete = true;
    this.flagnew = false;
    this.flagsave = false;
    this.flagImg = false;
    this.uploadDataList = [];
    this._obj = this. getDefaultObj();
    this.test = "";
    this.tests = "";
    this._fileName = "";
    this._obj.t5 = this.stateval;
    this._obj.n7 = 5;
    this.alertTime = "";
  }
  stateobj = { id: "", name: "" };
  town_obj = { id: "", name: "" };
  _result = { "longResult": "", "msgDesc": "", "key": [], "keyResult": 0, "state": "", "stringResult": "" };
  goPost() {
    this.flagsave = true;
    console.log("profile=" + JSON.stringify(this.ics._profile));
    console.log("obj=" + JSON.stringify(this._obj));
    this._obj.createdTime = this._time;
    this._obj.modifiedTime = this._time;
    this._obj.resizelist = this.uploadDataListResize;
    this._obj.alertTime = this.alertTime;

    if (this.isValidate(this._obj)) {
      if (this._obj.syskey != 0) {
        this._obj.modifiedUserId = this.ics._profile.userID;
        this._obj.modifiedUserName = this.ics._profile.userName;
      }
        this._obj.userId = this.ics._profile.userID;
        this._obj.userName = this.ics._profile.userName;
      this._obj.sessionId = this.ics._profile.sessionID;
      let url: string = this.ics.cmsurl + 'serviceContentNew/goSave';
      this._obj.n5 = Number(this.ics._profile.t1);
      this._obj.t3 = "Video";
      let json: any = this._obj;
      console.log("json=" + JSON.stringify(json));
      if (isMyanmar(json.t2)) {
        if (identifyFont(json.t2) == "zawgyi") {
          json.t2 = ZgtoUni(json.t2);
        }
      }
      this.http.doPost(url, json).subscribe(
        data => {
          this._result = data;
          if (data.state) {
            this.showMessage(data.msgDesc, true);
            this.flagsave = false;
            this.flagdelete = false;
            this._obj.syskey = data.longResult[0];
          }
          else {
            this.showMessage(data.msgDesc, false);
            this.flagsave = false;
          }         

        },
        error => {
          this._mflag = true;
          console.log("data error=");
          this.flagsave = false;
          this.showMessage("Can't Saved This Record", undefined);
        },
        () => {
          this.flagsave = false;
          console.log("data error");
        }
      );
    }

  }
  goDelete() {
    this._mflag = false;
    if (this._obj.t2 != "") {
      this._obj.sessionId = this.ics._profile.sessionID;
      this._obj.userId = this.ics._profile.userID;
      let url: string = this.ics.cmsurl + 'serviceContentNew/goDelete';
      let json: any = this._obj;
      this.http.doPost(url, json).subscribe(
        data => {
          this._mflag = true;
          this.showMessage(data.msgDesc, data.state);
          if (data.state) {
            this.goNew();
          }
        },
        error => { },
        () => { }
      );
    } else {
      this._mflag = true;
      this.showMessage("No Article to Delete", undefined);
    }
  }
  goList() {
    this._router.navigate(['/videoList']);
  }
  goOK() {
    jQuery("#sessionalert1").modal('hide');
    this.flagcanvas = true;
    this.flagvideo = false;
    this._fileName = this.myimg.name;
    this._file = this.myimg;

    var index = this._fileName.lastIndexOf(".");
    var videoname = this._fileName.substring(index);
    videoname = videoname.toLowerCase();
    if (videoname == ".mp4" || videoname == ".flv" || videoname == ".webm" || videoname == ".3gp" || videoname == ".3gp2" || videoname == ".mpeg4" || videoname == ".mpeg" || videoname == ".wmv" || videoname == ".avi") {
      jQuery("#imagepopup").modal();

      let url = this.ics.cmsurl + 'fileAdm/fileupload?f=upload&fn=' + this._fileName + '&id=' + this.ics._profile.t1 + '&type=10&imgUrl=' + this.ics._imgurl;
      this.http.upload(url, this._file).subscribe(
        data => {
          jQuery("#imagepopup").modal('hide');
          if (data.code === 'SUCCESS') {
            //this.flagImg = true;

            this.showMessage("Upload Successful", true);
            this._obj.videoUpload[0] = data.fileName;
            //this.gettime();
            this.tests = this._fileName;
          } else {
            this.showMessage("Upload Unsuccessful Please Try Again...", false);
          }
        },
        error => { },
        () => { }
      );
    }
    else {
      jQuery("#uploadVideo1").val("");
      this._obj.videoUpload = [];//atn
      this.showMessage("Choose video Associated", false);
    }
    this.tests = "";
  }

  uploadedFile(event) {
    this.flagcanvas = true;
    this.flagvideo = false;
    if (event.target.files.length == 1) {
      if ((this._obj.videoUpload[0] != null)) {
        this.myimg = event.target.files[0];
        jQuery("#sessionalert1").modal();
      } else {

        this._fileName = event.target.files[0].name;
        this._file = event.target.files[0];

        var index = this._fileName.lastIndexOf(".");
        var videoname = this._fileName.substring(index);
        videoname = videoname.toLowerCase();
        if (videoname == ".mp4" || videoname == ".flv" || videoname == ".webm" || videoname == ".3gp" || videoname == ".3gp2" || videoname == ".mpeg4" || videoname == ".mpeg" || videoname == ".wmv" || videoname == ".avi") {
          jQuery("#imagepopup").modal();

          let url = this.ics.cmsurl + 'fileAdm/fileupload?f=upload&fn=' + this._fileName + '&id=' + this.ics._profile.t1 + '&type=10&imgUrl=' + this.ics._imgurl;
          this.http.upload(url, this._file).subscribe(
            data => {
              jQuery("#imagepopup").modal('hide');
              if (data.code === 'SUCCESS') {
                //this.flagImg = true;
                this.showMessage("Upload Successful", true);
                this.uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };

                this._obj.videoUpload[0] = data.fileName;
                this.tests = this._fileName;
                //this.gettime();
              } else {
                this.showMessage("Upload Unsuccessful Please Try Again...", false);
              }
            },
            error => { },
            () => { }
          );
        }
        else {
          jQuery("#uploadVideo").val("");
          this.showMessage("Choose video Associated", false);
        }


      }
    }
    /* else { 
      this._fileName=this.tests;
      this.showMessage("Upload Fail", undefined);
    } */

  }

  goCancle() {
    jQuery("#sessionalert1").modal('hide');
    jQuery("#uploadVideo1").val(this.tests);

  }
  goNo() {
    jQuery("#sessionalert").modal('hide');
    jQuery("#imageUpload1").val(this.test);

  }
  goYes() {
    jQuery("#sessionalert").modal('hide');
    this._fileName = this.myimg.name;
    this._size = this.myimg.size;
    if (this._size >= 1048576) { this.temp = (this._size / 1048576).toFixed(2) + " MB"; }
    else if (this._size >= 1024) { this.temp = (this._size / 1024).toFixed(2) + " KB"; }
    else if (this._size > 1) { this.temp = this._size + " bytes"; }
    else if (this._size == 1) { this.temp = this._size + " byte"; }
    else { this.temp = "0 bytes"; }

    this._file = this.myimg;

    var index = this._fileName.lastIndexOf(".");
    var imagename = this._fileName.substring(index);
    imagename = imagename.toLowerCase();
    if (imagename == ".jpg" || imagename == ".jpeg" || imagename == ".png") {
      console.log("imgUrl: " + this.ics._imgurl);
      let url = this.ics.cmsurl + 'fileAdm/fileupload?f=upload&fn=' + this._fileName + '&id=' + this.ics._profile.t1 + '&type=10' + '&imgUrl=' + this.ics._imgurl;
      this.http.upload(url, this._file).subscribe(
        data => {
          if (data.code === 'SUCCESS') {
            this.flagImg = true;
            console.log("videoImage: " + JSON.stringify(data));
            let _img = this._fileName;
            this.uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
            this.uploadData.name = data.fileName;
            this.uploadData.url = data.url;
            this._obj.uploadlist[0] = this.uploadData;
            this.uploadDataListResize.push(data.sfileName);
            this.test = this._fileName;
            this._obj.t6 = this.temp;
            this.showMessage("Upload Successful", true);

          } else {
            this.showMessage("Upload Unsuccessful Please Try Again...", false);
          }
          this._mflag = true;
        },
        error => { },
        () => { }
      );
    }
    else {
      this._mflag = true;
      jQuery("#imageUpload1").val("");
      this._obj.uploadlist = [];
      this.showMessage("Choose Image Associated", false);
    }

    this.test = "";

  }

  test = "";
  uploadedFileImage(event) {
    this._mflag = true;
    this.uploadDataListResize = [];


    if (event.target.files.length == 1) {
      //if(this.flagImg == true){
      if ((this._obj.uploadlist[0] != null)) {
        this.myimg = event.target.files[0];
        jQuery("#sessionalert").modal();
      } else {
        this._fileName = event.target.files[0].name;
        this._size = event.target.files[0].size;
        if (this._size >= 1048576) { this.temp = (this._size / 1048576).toFixed(2) + " MB"; }
        else if (this._size >= 1024) { this.temp = (this._size / 1024).toFixed(2) + " KB"; }
        else if (this._size > 1) { this.temp = this._size + " bytes"; }
        else if (this._size == 1) { this.temp = this._size + " byte"; }
        else { this.temp = "0 bytes"; }

        this._file = event.target.files[0];

        var index = this._fileName.lastIndexOf(".");
        var imagename = this._fileName.substring(index);
        imagename = imagename.toLowerCase();
        if (imagename == ".jpg" || imagename == ".jpeg" || imagename == ".png") {
          console.log("imgUrl: " + this.ics._imgurl);
          let url = this.ics.cmsurl + 'fileAdm/fileupload?f=upload&fn=' + this._fileName + '&id=' + this.ics._profile.t1 + '&type=10' + '&imgUrl=' + this.ics._imgurl;
          this.http.upload(url, this._file).subscribe(
            data => {
              if (data.code === 'SUCCESS') {
                this.flagImg = true;
                console.log("videoImage: " + JSON.stringify(data));
                let _img = this._fileName;
                this.uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
                this.uploadData.name = data.fileName;
                this.uploadData.url = data.url;
                this._obj.uploadlist[0] = this.uploadData;
                this.uploadDataListResize.push(data.sfileName);
                this.test = this._fileName;
                this._obj.t6 = this.temp;
                this.showMessage("Upload Successful", true);

              } else {
                this.showMessage("Upload Unsuccessful Please Try Again...", false);
              }
              this._mflag = true;
            },
            error => { },
            () => { }
          );
        }
        else {
          this._mflag = true;
          jQuery("#imageUpload").val("");
          this.showMessage("Choose Image Associated", false);
        }
      }
    }/*  else {       
        jQuery("#imageUpload").val(this.test);
        this.showMessage("Upload Fail", undefined);
      } */


  }
  getStatusType() {
    try {
        let url: string = this.ics.cmsurl + 'serviceAdmLOV/getLovType';
        this._sessionObj.sessionID = this.ics._profile.sessionID;
        this._sessionObj.userID = this.ics._profile.userID;
        this._sessionObj.lovDesc ="StatusType";
        let json: any = this._sessionObj;
        this.http.doPost(url, json).subscribe(
            data => {
                if (data != null) {
                    if (data.msgCode == "0016") {
                        this.showMessage(data.msgDesc, false);
                    }
                    if (data.msgCode == "0000") {
                        if (!(data.lovType instanceof Array)) {
                            let m = [];
                            m[0] = data.lovType;
                            this.ref._lov3.StatusType = m;
                        } else {
                            this.ref._lov3.StatusType = data.lovType;
                        }
                        this.ref._lov3.StatusType.forEach((iItem) => {
                            let l_Item = { "value": "", "caption": "" };

                            l_Item['caption'] = iItem.caption;
                            l_Item['value'] = iItem.value;

                            this.lovstatusType.push(iItem);
                        });
                    }
                }
                this._mflag = true;
            },
            error => {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            },
            () => { }
        );
    } catch (e) {
        alert("Invalid URL.");
    }
}

  showMessage(msg, bool) {
    if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
    if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
    if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "primary", "t3": msg }); }
  }
  isValidate(obj) {
    if (obj.t1 == "" && obj.t2 == "") {
      this.showMessage("Please fill Title and Content", false);
      this.flagsave = false;
      return false;
    }
    if (obj.t1 == "") {
      this.showMessage("Please fill Title", false);
      this.flagsave = false;
      return false;
    }
    if (obj.t2 == "") {
      this.showMessage("Please fill Content", false);
      this.flagsave = false;
      return false;
    }
    //if (obj.n10 == 0 && obj.upload)
    if (!this._util.validateLanguage(obj.t2) && !this._util.validateEng(obj.t2)) {
      this.showMessage("Please change Myanmar3 Font at Content ", false);
      this.flagsave = false;
      return false;
    }
    if (obj.t8 == "" && (obj.n10 == 1 || obj.n10 == 2)) {
      this.showMessage("Please fill URL Link", false);
      this.flagsave = false;
      return false;
    }
    if ((obj.videoUpload == null || obj.videoUpload.length == 0) && obj.n10 == 0) {
      this.showMessage("Choose Associated Video", false);
      this.flagsave = false;
      return false;
    }

    if (obj.n7 == "") {
      this.showMessage("Choose Status", false);
      this.flagsave = false;
      return false;
    }
    return true;
  }
  setBtns() {
    let k = this.ics.getBtns("/videoList");
    if (k != "" && k != undefined) {
      let strs = k.split(",");
      for (let i = 0; i < strs.length; i++) {
        if (strs[i] == "1") {
          this.flagnew = false;
        }
        if (strs[i] == "2") {
          this.flagsave = false;
        }
      }
    }
  }
  setVideoUrl(str) {
    console.log("" + JSON.stringify(this.ics._imgurl + 'upload/video/' + str))
    return 'upload/video/' + str;
  }
  setImgUrl(str) {
    console.log("" + JSON.stringify(this.ics._imgurl + '/upload/smallImage/videoImage/' + str))
    return this.ics._imgurl + '/upload/smallImage/videoImage/' + str;
  }

  videoplayer: any;
  toggleVideo(event: any) {
    this.videoplayer.nativeElement.play();
  }

  onMetadata(e, video) {
    console.log("Video duration...........")
    console.log('duration: ' + video.duration);
    this._obj.t7 = this.videoDurationForm(video.duration);
  }

  /* videoDurationForm(p : number){
    let hr: number;
    let hrs:String;
    let min: number;
    let mins: String;
    let sec: number;
    let secs: String;
    p = p/1000;
    sec = p%60;
    min = p/60;
    //min = min%60;
    hr = min/60;
    //sec = Math.ceil(sec);
    sec = Math.floor(sec);
    secs = this.count(sec);
    min = Math.floor(min);
    mins = this.count(min);
    hr = Math.floor(hr);
    hrs = this.count(hr);
    console.log(hrs + ":" + mins + ":" + secs);
    return hrs + ":" + mins + ":" + secs;
    
  } */
  videoDurationForm(p: number) {
    let hr: number;
    let hrs: String;
    let min: number;
    let mins: String;
    let sec: number;
    let secs: String;
    hr = Math.floor(p / 3600);
    hrs = this.count(hr);
    p %= 3600;
    min = Math.floor(p / 60);
    mins = this.count(min);
    sec = p % 60;
    sec = Math.floor(sec);
    secs = this.count(sec);
    console.log(hrs + ":" + mins + ":" + secs);
    return hrs + ":" + mins + ":" + secs;

  }
  count(p) {
    if (p < 10) {
      p = '0' + p;
    }
    return p;
  }

  obj = { id: "", name: "" };


  goView(p) {
    this._router.navigate(['/videoAdminView', 'read', p]);
  } 
  goRemove(obj, num) {
    this._fileName = "";
    let img = obj;
    this._obj.videoUpload = [];
  }

  changeBrowe(p) {
    if (p == 0) {
      this.flagvideo = false;
      this.flagURL = true;
      //this.flagImage = true;
      this.flagImage = false;
    }
    else {
      this.flagvideo = true;
      this.flagURL = false;
      //this.flagImage = false;
      this.flagImage = false;
      this._obj.videoUpload = [];
      jQuery("#uploadVideo1").val('');

    }
  }
  deleteFile(obj: any) {
    this.http.doGet(this.ics.cmsurl + 'fileAdm/fileRemove?fn=' + obj).subscribe(
      data => {
        console.log(data.code);
        if (data.code === 'SUCCESS') {
          this._obj.n10 = 0;
          let index = this.uploadDataList.indexOf(obj);
          this.uploadDataList.splice(index, 1);
          this.showMessage('Photo Removed!', true);
        }
      },
      error => { },
      () => { }
    );
  }  
}
