import { Component, enableProdMode } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { RpHttpService } from '../framework/rp-http.service';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpReferences } from '../framework/rp-references';
import { ClientUtil } from '../util/rp-client.util';
declare var jQuery: any;
declare var google: any;
enableProdMode();
@Component({
    selector: 'fmr-ticketList',
    template: `
    
    <div *ngIf="!_divexport" class="container-fluid">
    <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
            <form class="form-horizontal">
                <fieldset>
                    <legend>
                        <div style="display:inline;">
                        Videos  
                        <button id="btnNew" class="btn btn-sm btn-primary" type="button" style="cursor:pointer;height:34px;" (click)="goNew()" title="New">
                        <i id="lblNew" class="glyphicon glyphicon-file" style="padding-left:2px;"></i> 
                        </button>                            
                        </div>

                        <div style="float:right;text-align:right;">                                
                            <div style="display:inline-block;padding-right:0px;width:280px;">
                                <div class="input-group"> 
                                    <input class="form-control" type="text" id="isInputSearch" placeholder="Search" autocomplete="off" spellcheck="false" [(ngModel)]="_SearchString" [ngModelOptions]="{standalone: true}" (keyup.enter)="filterSearch()">

                                    <span id="btnSimpleSearch" class="input-group-addon">
                                        <i class="glyphicon glyphicon-search" style="cursor: pointer" (click)="filterSearch()"></i>
                                    </span>
                                </div>
                            </div>

                            <button id="btnToggleSearch" class="btn btn-md btn-secondary" type="button" style="cursor:pointer;text-align:center;margin-top:-24px;height:34px;" (click)="btnToggleSearch_onClick()" title="Collapse">
                                <i id="lblToggleSearch" class="glyphicon glyphicon-menu-down"></i> 
                            </button>

                            <button *ngIf="false" id="btnTogglePagination" type="button" class="btn btn-md btn-default" style="cursor:pointer;text-align:center;margin-left:25px;margin-top:-24px;width:30px;height:34px;" (click)="btnTogglePagination_onClick()" title="Collapse">
                                <i id="lblTogglePagination" class="glyphicon glyphicon-eject" style="color:#2e8690;margin-left:-4px;transform: rotate(90deg)"></i>
                            </button>

                            <div id="divPagination" style="display:none;margin-left:13px;margin-top:-7px;margin-bottom:5px;">
                                <pager6 id="pgPagination" rpPageSizeMax="100" [(rpModel)]="_ListingDataset.totalCount" (rpChanged)="changedPager($event)" style="font-size:14px;text-align:right;margin-top:-7px;margin-right:10px;"></pager6>
                            </div>

                            <button id="btnClose" type="button" class="btn btn-md btn-secondary" (click)="closePage()" (mouseenter)="btnClose_MouseEnter()" (mouseleave)="btnClose_MouseLeave()" title="Close" style="cursor:pointer;height:34px;text-align:center;margin-top:-24px;">
                                <i id="lblClose" class="glyphicon glyphicon-remove"></i> 
                            </button>                     
                        </div>
                    </legend>
                    <div class="cardview list-height">
                        <advanced-search id="asAdvancedSearch" [FilterList]="_FilterList" [TypeList]="_TypeList" rpVisibleItems=3 (rpHidden)="renderAdvancedSearch($event)" (rpChanged)="filterAdvancedSearch($event)" style="display:none;"></advanced-search>

                        <div *ngIf="_shownull!=false" style="color:#ccc;">No result found!</div>

                        <div *ngIf="_showListing" class="form-group">
                        <div class="col-md-12" style="overflow-x:auto;">
                            <table class="table table-striped table-condense table-hover tblborder">
                            
                                    <thead>
                                        <tr>
                                        <th class="right"style="width:2%">No.</th>
										<th class="centre"  style="width:15%" title="Title">Title</th>
                                        <th class="left" title="Content" style="width:30%">Content</th>
                                        <th class="left" title="Status" style="width:7%">Status</th>
                                        <th class="left" title="Date" style="width:12%">Date</th>                                        
                                        <th class="left" title="Posted By" style="width:10%">Posted By</th>
                                        <th class="left" title="Modified By" style="width:10%">Modified By</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr *ngFor="let obj of _ListingDataset.videoData let i=index">
                                        <td class="right"><a (click)="goto(obj.syskey)">{{obj.srno}}</a></td>
										<td class="textwrap uni" title="{{obj.t1}}">{{obj.t1}}</td>
                                        <td class="textwrap uni" title="{{obj.t2}}">{{obj.t2}}</td>
                                        <td>{{obj.statusdata}}</td>
                                        <td class="uni" >{{obj.modifiedDate}}&nbsp;&nbsp;{{obj.modifiedTime}}</td>
                                        <td class="uni">{{obj.userName}}</td>
                                        <td class="uni">{{obj.modifiedUserName}}</td>
                                        </tr> 
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div *ngIf="_showListing" style="text-align:center">Total {{ _ListingDataset.totalCount }}</div>
                    </div>
                </fieldset>
            </form>

            <div id="ExcelDownload" style="display: none;width: 0px;height: 0px;"></div>
        </div> 
    </div>
</div>

<div *ngIf='_divexport'>
    <button type="button" class="close" (click)="goClosePrint()" style="margin-top:-20px;">&times;</button>
    <iframe id="frame1" [src]="_printUrl" style="width: 100%;height: 95%;"></iframe> 
</div>
<div [hidden]="_mflag">
<div  id="loader" class="modal" ></div>
</div>
  `
})
export class VideoList {
    // RP Framework 
   subscription: Subscription;
    
   _OperationMode = "";

   _divexport = false;
   //_printUrl: SafeResourceUrl;

   _isLoading = true;
   _SearchString = "";

   _shownull=false;

   _showListing = false;
   _showPagination = false;

   _toggleSearch = true;                       // true - Simple Search, false - Advanced Search
   _togglePagination = true;

   _TypeList: any;
   _FilterList: any;
   _mflag=false;

   _ButtonInfo = { "role":"", "formname":"", "button":"" };

   _FilterDataset =
   {
       filterList :
       [
           { "itemid": "", "caption": "", "fieldname": "", "datatype": "", "condition": "", "t1": "", "t2": "" }
       ],

       "pageNo": 1, "pageSize": 10, "filterSource": 0, "sessionID": "", "userID": "", "userName":""
   };

   _ListingDataset = 
   {
       videoData :
       [
           { 
               "srno":0,"syskey": 0, "autokey": 0, "createdDate": "", "createdTime": null, "modifiedDate": "", "modifiedTime": null, "userId": "",
                "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "Video", 
                "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0,
                 "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [],"statusdata":"", 
                 "videoUpload": [], "cropdata": null, "agrodata": null, "ferdata": null, "towndata": null, "crop": null, "fert": null, 
                 "agro": null, "addTown": null, "modifiedUserId": "", "modifiedUserName": ""
           }
       ],
   
       "pageNo": 0, "pageSize": 0, "totalCount": 1, "userID": "", "userName": ""
   };

   _sessionObj = this.getSessionObj();
   getSessionObj() {
       return { "sessionID": "", "userID": "","lovDesc":"" };
   }
   mstatus = 0;
   _util = new ClientUtil();
   goto(p) {
       console.log("video keys=" + JSON.stringify(p));
       this._router.navigate(['/videomenu', 'read', p]);
   }

   constructor(private ics: RpIntercomService, private _router: Router,
       //private sanitizer: DomSanitizer, 
       private http: RpHttpService, private l_util: ClientUtil, private ref: RpReferences) {
          
           if (!ics.getRole() || ics.getRole() == 0) this._router.navigate(['/login']);            
           this.mstatus = ics._profile.loginStatus;
   }

   ngOnInit()
   {
       this.filterSearch();
       this.loadAdvancedSearchData();
   }

   ngAfterViewChecked()
   {
       this._isLoading = false;
   }
   goNew() {
       this._router.navigate(['/videomenu', 'new']);
   }

   loadAdvancedSearchData()
   {
       this._TypeList = 
       {
           "lovStatus": []
       };
       this.loadStatus();  
       this._FilterList = 
       [
           { "itemid": "1", "caption": "Search String", "fieldname": "SearchString", "datatype": "simplesearch", "condition": "c", "t1": "", "t2": "", "t3": "" },          
           { "itemid": "2", "caption": "Status", "fieldname": "Status", "datatype": "lovStatus", "condition": "", "t1": "", "t2": "", "t3": "" },
           //{ "itemid": "4", "caption": "Date Range", "fieldname": "DateRange", "datatype": "date", "condition": "bt", "t1": this.l_util.getTodayDate(), "t2": this.l_util.getTodayDate(), "t3": "true" },
       ];
   }

   /* loadState()
   {
       try {
           this.http.doGet(this.ics.ticketurl + 'serviceCMS/getStateListByUserID?type=1&userID=' + this.ics._profile.userID).subscribe(
               response => {
                   if (response.refstate != null && response.refstate != undefined) {
                       //this.ref._lov3.refstate = [{ "value": "", "caption": "" }];
                       this.ref._lov3.refstate = [];
                       if (!(response.refstate instanceof Array)) {
                           let m = [];
                           m[0] = response.data;
                           this.ref._lov3.refstate = m;
                       } else {
                           this.ref._lov3.refstate = response.refstate;
                       }

                       this.ref._lov3.refstate.forEach((iItem) => {
                           let l_Item = { "caption": "", "value": "" };

                           l_Item['caption'] = iItem.caption;
                           l_Item['value'] = iItem.value;

                           this._TypeList.lovState.push(iItem);
                       });
                   } else {
                       this.ref._lov3.refstate = [{ "value": "", "caption": "Empty" }];
                   }
               },
               error => {
                   if (error._body.type == 'error') {
                       alert("Connection Error!");
                   }
               }, () => { });
       } catch (e) {
           alert("Invalid URL");
       }
   } */

   loadStatus(){
       
       try {
        let url: string = this.ics.cmsurl + 'serviceAdmLOV/getLovType';
        this._sessionObj.sessionID = this.ics._profile.sessionID;
        this._sessionObj.userID = this.ics._profile.userID;
        this._sessionObj.lovDesc ="StatusType";
        let json: any = this._sessionObj;
        this.http.doPost(url, json).subscribe(
            data => {
                   if (data != null) {
                       if (data.msgCode == "0016") {
                           this.showMsg(data.msgDesc, false);
                       }
                       if (data.msgCode == "0000") {
                           if (!(data.lovType instanceof Array)) {
                               let m = [];
                               m[0] = data.lovType;
                               this.ref._lov3.StatusType = m;
                           } else {
                               this.ref._lov3.StatusType = data.lovType;
                           }
 
                           this.ref._lov3.StatusType.forEach((iItem) => {
                               let l_Item = { "value": "", "caption": "" };
 
                               l_Item['caption'] = iItem.caption;
                               l_Item['value'] = iItem.value;
 
                               this._TypeList.lovStatus.push(iItem);
                           });
                       }
                   }
                  // this._mflag=true;
               },
               error => {
                   if (error._body.type == "error") {
                       alert("Connection Timed Out.");
                   }
               },
               () => { }
           );
       } catch (e) {
           alert("Invalid URL.");
       }
   }

   btnToggleSearch_onClick()
   {
       this.toggleSearch(!this._toggleSearch);
   }

   btnTogglePagination_onClick()
   {
       this.togglePagination(!this._togglePagination);
   }

   toggleSearch(aToggleSearch)
   {
       // Set Flag
       this._toggleSearch = aToggleSearch;
       
       // Clear Simple Search
       if (!this._toggleSearch)        jQuery("#isInputSearch").val("");

       // Enable/Disabled Simple Search Textbox
       jQuery('#isInputSearch').prop('disabled', !this._toggleSearch);

       // Hide Advanced Search
       if (this._toggleSearch)         this.ics.send001("CLEAR");

       // Show/Hide Advanced Search    
       //  
       if (this._toggleSearch)         jQuery("#asAdvancedSearch").css("display", "none");                                                                             // true     - Simple Search
       else                            jQuery("#asAdvancedSearch").css("display", "inline-block");                                                                     // false    - Advanced Search

       // Set Icon 
       //  
       if (this._toggleSearch)         jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-up").addClass("glyphicon glyphicon-menu-down");                // true     - Simple Search
       else                            jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-down").addClass("glyphicon glyphicon-menu-up");                // false    - Advanced Search

       // Set Tooltip
       //
       let l_Tooltip = (this._toggleSearch)? "Expand" : "Collapse";
       jQuery('#btnToggleSearch').attr('title', l_Tooltip);
   }

   togglePagination(aTogglePagination)
   {
       // Set Flag
       this._togglePagination = aTogglePagination;

       // Show/Hide Pagination
       //
       if (this._showPagination && this._togglePagination)         jQuery("#divPagination").css("display", "inline-block");
       else                                                        jQuery("#divPagination").css("display", "none");

       // Rotate Icon
       //
       if (!this._togglePagination)    jQuery("#lblTogglePagination").css("transform", "rotate(270deg)");
       else                            jQuery("#lblTogglePagination").css("transform", "rotate(90deg)");
       
       // Set Icon Position
       if (!this._togglePagination)    jQuery("#lblTogglePagination").css({ "margin-left": "-5px", "margin-top": "0px" });
       else                            jQuery("#lblTogglePagination").css({ "margin-left": "-5px", "margin-top": "-1px" });

       // Set Tooltip
       //
       let l_Tooltip = (this._togglePagination)? "Collapse" : "Expand";
       jQuery('#btnTogglePagination').attr('title', l_Tooltip);
   }

   btnClose_MouseEnter()
   {
       jQuery("#btnClose").removeClass('btn btn-md btn-secondary').addClass('btn btn-md btn-danger');
   }

   btnClose_MouseLeave()
   {
       jQuery("#btnClose").removeClass('btn btn-md btn-danger').addClass('btn btn-md btn-secondary');
   }

   closePage()
   {
       this._router.navigate([ '000', { cmd: "READ", p1: this.ics._profile.userID } ]);
   }

   renderAdvancedSearch(event)
   {
       this.toggleSearch(!event);
   }

   filterAdvancedSearch(event)
   {
       this._FilterDataset.filterSource = 1;
       this._FilterDataset.filterList = event;

       if (this._OperationMode != "prepareFilter")
       {
           if (this._FilterDataset.pageNo == 1)    this.filterRecords();
           else                                    this._FilterDataset.pageNo = 1;
       }
   }

   changedPager(event)
   {
       if (!this._isLoading)
       {
           let l_objPagination = event.obj;
       
           this._FilterDataset.pageNo = l_objPagination.current;
           this._FilterDataset.pageSize = l_objPagination.size;

           this.filterRecords();
       }
   }

   filterSearch()
   {
       if (this._toggleSearch)         this.filterCommonSearch();
       else                            this.ics.send001("FILTER");
   }

   filterCommonSearch()
   {
       var l_DateRange;
       var l_SearchString = "";

       this._FilterDataset.filterList = [];
       this._FilterDataset.filterSource = 0;

       if (jQuery.trim(this._SearchString) != "")
       {
           l_SearchString = jQuery.trim(this._SearchString);

           this._FilterDataset.filterList = 
           [
               { "itemid": "1", "caption": "Search String", "fieldname": "SearchString", "datatype": "simplesearch", "condition": "c", "t1": l_SearchString, "t2": "" }
           ];
       }

       l_DateRange = { "itemid": "4", "caption": "Date Range", "fieldname": "DateRange", "datatype": "date", "condition": "eq", "t1": this.l_util.getTodayDate(), "t2": "" };
       this._FilterDataset.filterList.push(l_DateRange);

       if (this._OperationMode != "prepareFilter")
       {
           if (this._FilterDataset.pageNo == 1)    this.filterRecords();
           else                                    this._FilterDataset.pageNo = 1;
       }
   }

   filterRecords()
   {
        this._FilterDataset.sessionID = this.ics._profile.sessionID;
        this._FilterDataset.userID = this.ics._profile.userID;
        this._FilterDataset.userName = this.ics._profile.userName;
       let l_Data: any = this._FilterDataset;
       let l_ServiceURL: string = this.ics.cmsurl+ 'serviceVideoAdm/searchVideolist';

       this.http.doPost(l_ServiceURL, l_Data).subscribe
       (
           data => 
           {              
               if (data!= null && data != undefined && data != "") 
               {
                   this._ListingDataset = data;
                   this._shownull=false;
                   // Convert to array for single item
                   if (this._ListingDataset.videoData != undefined)
                   {
                       this._ListingDataset.videoData = this.l_util.convertToArray(this._ListingDataset.videoData);
                       for(let iIndex = 0; iIndex < this._ListingDataset.videoData.length; iIndex++)
                       {    
                           this._ListingDataset.videoData[iIndex].modifiedDate = this._util.changeDatetoStringDMY(this._ListingDataset.videoData[iIndex].modifiedDate);
                           for (let i = 0; i < this._ListingDataset.videoData.length; i++) {
                               for (let j = 0; j < this._TypeList.lovStatus.length; j++) {
                                   if (this._ListingDataset.videoData[i].n7.toString() == this._TypeList.lovStatus[j].value) {
                                       this._ListingDataset.videoData[i].statusdata = this._TypeList.lovStatus[j].caption;
                                   }
                               }
                           }
                       }
                   }
                   else{
                       this._shownull=true;
                   }

                   // Show / Hide Listing
                   this._showListing = (this._ListingDataset.videoData!= undefined);
                  // this._showListing = ( this._ListingDataset.totalCount!= 0);                

                   // Show / Hide Pagination
                   this._showPagination = (this._showListing && (this._ListingDataset.totalCount > 10));

                   // Show / Hide Pagination
                   this.togglePagination(this._showListing); 

                   // Hide loading animation
                   this._mflag = true;
               }
               
           },
           error => 
           {   
               // Hide loading animation
               this._mflag = true;
               },
               () => { }
       );
   }

   btnExport_click() 
   { 
   }

   downloadFile(aFileName)
   {
       let l_Url = this.ics._apiurl + 'service001/downloadexcel?filename=' + aFileName;
       jQuery("#ExcelDownload").html("<iframe src=" + l_Url + "></iframe>");
   }  

   btnPrint_click()
   {
       //this.goPrint();
   }

   goPrint()
   {   
      
   }

   goClosePrint()
   {
       this._divexport = false; 
   }
   showMsg(msg, bool) {
       if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
       if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
       if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
   }

   logout() {
       this._router.navigate(['/login']);
   }
}