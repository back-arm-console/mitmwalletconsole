import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpHttpService } from '../framework/rp-http.service';
import { RpReferences } from '../framework/rp-references';
import { ClientUtil } from '../util/rp-client.util';
declare var jQuery: any;
import 'tinymce/tinymce.min';
declare var tinymce: any;
import 'tinymce/themes/modern/theme';
import 'tinymce/plugins/link/plugin.js';
import 'tinymce/plugins/paste/plugin.js';
import 'tinymce/plugins/table/plugin.js';
import 'tinymce/plugins/advlist/plugin.js';
import 'tinymce/plugins/autoresize/plugin.js';
import 'tinymce/plugins/lists/plugin.js';
import 'tinymce/plugins/code/plugin.js';
import 'tinymce/plugins/image/plugin.js';
import 'tinymce/plugins/imagetools/plugin.js';
//import 'tinymce/plugins/emoticons/plugin.js';
import { CompilerConfig } from '@angular/compiler';

@Component({
    selector: 'contenntmenu',
    template: `
<div class="container-fluid" >
    <div class="row clearfix">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0"> 
			<form class= "form-horizontal" ngNoForm> 
			<legend>Content </legend>
			<div class="cardview list-height">  
				<div class="row col-md-12" style="margin-bottom: 10px;">
					<div  style="float: left;"> 
					    <button class="btn btn-sm btn-primary"  type="button"   (click)="goList()">List</button>
						<button class="btn btn-sm btn-primary" [disabled]="flagnew" id="flagnew" type="button" (click)="goNew()">New</button>      
						<button class="btn btn-sm btn-primary" [disabled]="flagsave" type="button" (click)="goPost()">Save</button> 
                        <button class="btn btn-sm btn-primary" [disabled]="flagdelete" id="delete" type="button" (click)="goDelete();" >Delete</button>					   
                       <!-- <button *ngIf="!flagview" class="btn btn-sm btn-primary"  type="button"  (click)="goView(_viewsk);" >View</button> -->
					</div>
				</div>
				
				<div class="row col-md-12" style="padding-left: 30px;">				
					<div class="form-group" >
						<label class="col-md-2">Title</label>
						<div class="col-md-4">
							<input  type="text" [(ngModel)]="_obj.t1" class="form-control input-md uni"/>				
						</div>		
						<label class="col-md-2">Status</label>
						<div class="col-md-4">
							<select [(ngModel)]="_obj.n7" class="form-control input-sm" [ngModelOptions]="{standalone: true}" required>
							<option *ngFor="let c of lovstatusType" value="{{c.value}}">{{c.caption}}</option>
							</select>
						</div>
					</div>		     
					
					<div class="form-group">
						<label class="col-md-2">Content</label>
						<div class="col-md-10">
						<div class="uni">
						<textarea class="form-control input-md uni" [(ngModel)]="_obj.t2" rows="10" ></textarea>
							<input name="image" type="file" id="upload" class="hidden" onchange="">
						</div>
						</div>
					</div>

					<div class="form-group" id="flagImageUpload">
						<label class="col-md-2">Image Associated (jpeg, png, jpg)</label>
						<div class="col-md-4">
							<input type="file" id="imageUpload" class="form-control input-md" (change)="uploadedFile($event,_obj.n4)" placeholder="Upload file..." />
						</div>     
					</div>

					<div class="form-group">
						<div *ngFor="let img of _obj.videoUpload;let num = index">
							<video id="{{img}}" controls style="display:none" height="240" width="400" type="video/mp4">
								<source src="{{setImgUrl(img)}}" onError="this.src='./image/image_not_found.png';" type="video/mp4" />  
							</video>                                
						<canvas id="canvas" style="display:none" ></canvas> 
						</div>
					</div>
				
					<div class="col-md-3 col-sm-4 col-xs-6" style="padding-right: 1px;" *ngFor="let obj of uploadDataList, let i=index"
					(dragstart)="onDragStart($event, i, obj, uploadDataList)" draggable ="true" (dragover)="allowDrop($event)"
					(dragover)="goSubstitute(event,i)" (drop)="onDrop($event, i, obj, uploadDataList)">
						<div id="img_container">
						<img src="{{setImgUrl(obj.name)}}" onError="this.src='./image/image_not_found.png';" alt={{obj.name}} title={{obj.name}} style="margin-top:20px;width:80%;height:200px;overflow: hidden;object-position: 25% 50%;transition: 1s width, 1s height;border-radius:5px;object-fit:cover;"/>
							<button  class="button_over_img" type="button" title="Remove Photo" (click)="deleteFile(obj)">
								<span  class="glyphicon glyphicon-remove"></span>
							</button>
							
							<textarea class="form-control input-md uni" [(ngModel)]="obj.desc" rows="3" style="width:220px;margin-top:10px;" placeholder="Type Image Description"></textarea>
						</div>
					</div>
				</div>
				<div class="form-group">&nbsp;</div>
			</div>
			</form>
		</div>
    </div>
</div>
    <!-- processing image modal -->
    <div id="imagepopup" class="modal fade" role="dialog" style=" margin-top: 200px; margin-left: 150px;">
        <div id="imagepopupsize" class="modal-dialog modal-lg" style="width : 240px">
            <div class="modal-content">
                <div id="imagepopupbody" class="modal-body">
                    <img src="image/processing.gif" style="padding-top:30px;">
                </div>
            </div>
        </div>
    </div>
`
})

export class ContentMenu implements OnInit, OnDestroy {
    _file = null;
    serial = 0;
    _viewsk = 0;
    content = 0;
    _fileName = '';
    _time = "";
    _str = "";
    _roleval = "";
    _image = "";
    _urllink = "";
    alertTime = "";
    flagnew = true;
    flagsave = true;
    flagaca = true;
    flagdelete = true;
    flagview = true;
    flagcanvas = true;
    _checkcw = false;
    _checkeditor = false;
    _checkpubisher = false;
    _checkadmin = false;
    statusval=0;
    stateval="";  
    alert: boolean = false;
    _mflag = false;
    uploadDataList = [];
    uploadDataListResize = [];
    sub: any;
    result: any;
    _datepickerOpts: any;
    public editor: any;
    uploadData = { "serial": 0, "name": "", "order": "", "url": "", "desc": "" };
    _obj = this.getDefaultObj();
    getDefaultObj() {
        return { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": "", "modifiedDate": "", "modifiedTime": "", "userId": "","sessionId":"", "userName": "", "modifiedUserId": "", "modifiedUserName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "News", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "temp": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [], "videoUpload": [],"uploadlist": [], "resizelist": [] };
    }
    _result = { "longResult": "", "msgDesc": "", "key": [], "keyResult": 0, "state": "", "stringResult": "" };

    subscription: Subscription;
    _util: ClientUtil = new ClientUtil();
    @Output() onEditorKeyup = new EventEmitter<any>();

    constructor(private ics: RpIntercomService, private _router: Router, private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences) {
        if (!ics.getRole() || ics.getRole() == 0) this._router.navigate(['/login']);
        //event
        jQuery("#flagdelete").hide();
        this.flagview = true;
        this._roleval = ics._profile.t1;
        this.flagsave = false;
        this.flagnew = false;
        this._obj.n7 = 5;
        this._datepickerOpts = this.ics._datepickerOpts;
        this._datepickerOpts.format = 'dd/mm/yyyy';        
        this._time = this._util.getTodayTime();
        this._obj.createdTime = this._time;
        this._obj.modifiedTime = this._time;
        this.getStatusType();      
    }

    ngOnInit() {
        var urllink1 = this._urllink;        
        tinymce.init({
            selector: 'textarea',
            menubar: false,
            statusbar: false,
            content_css: 'https://afeld.github.io/emoji-css/emoji.css',
            toolbar: "bold | alignleft aligncenter alignright alignjustify",//emoticons
            plugins: ['link', 'modern', 'paste', 'table', 'image', 'imagetools'],//'emoticons', 
            file_browser_callback: function (field_name, url, type, win) {
                if (type == 'image') jQuery('#upload input').click();
                win.document.getElementById(field_name).value = urllink1;
            },
            setup: editor => {
                this.editor = editor;
                editor.on('init', () => {
                    editor.setContent(this._obj.t2);
                });
                editor.on('keyup change', () => {
                    const content = editor.getContent();
                    this._obj.t2 = content;
                    this.onEditorKeyup.emit(content);
                })
            },
        });
        //
        this.sub = this.route.params.subscribe(params => {
            let cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "new") {
                this.content = 1;
                this.goNew();
                this.content = 0;
            } else if (cmd != null && cmd != "" && cmd == "read") {
                let id = params['id'];
                this.goGet(id);
                this._viewsk = id;
            }
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    goGet(p) {
        this.flagview = false;
        this.flagsave = false;
        this.flagdelete = false;
        this._obj.createdTime = this._time;
        this._obj.modifiedTime = this._time;
        this.http.doGet(this.ics.cmsurl + 'serviceContentNew/readBySyskey?key=' + p +'&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(
            data => {
                this._obj = data;
                if (this._obj != null) {
                    this.editor.setContent(this._obj.t2);

                    if (this._obj.uploadlist.length > 0) {
                        this.uploadDataList = this._obj.uploadlist;

                        this.serial = this.uploadDataList[this.uploadDataList.length - 1].serial;
                        this._obj.uploadlist = [];
                    }
                    if (this._obj.resizelist.length > 0) {
                        this.uploadDataListResize = this._obj.resizelist;
                        this._obj.resizelist = [];
                    }
                }else this._obj = this.getDefaultObj();              

            },
            error => { },
            () => { }
        );
    }

    goNew() {    
        this.uploadData = { "serial": 0, "name": "", "order": "", "url": "", "desc": "" };
        this._obj = this.getDefaultObj();
        this._result = { "longResult": "", "msgDesc": "", "key": [], "keyResult": 0, "state": "", "stringResult": "" };            
        this.serial = 0;
        this._fileName = "";
        this.uploadDataList = [];
        this.flagview = true;
        this.alertTime = "";
        this._time = this._util.getTodayTime();
        this._obj.createdTime = this._time;
        this._obj.modifiedTime = this._time;
        this._datepickerOpts = this.ics._datepickerOpts;
        this._datepickerOpts.format = 'dd/mm/yyyy';
        this._obj.t5 = this.stateval;
        this._obj.n7 = 5;
        this.flagdelete = true;
        this.flagsave = false;
        jQuery("#flagdelete").hide();
        jQuery("#flagsave").show();
        jQuery("#flagview").show();
        jQuery("#flagImageUpload").show();
        jQuery("#imageUpload").val("");
        jQuery("#flagdelete").val("");
        jQuery("#save").prop("disabled", false);
        if (this.content == 0) {
            this.editor.setContent("");
        }            
    }

    goPost() {        
        this.flagsave = true;
        this._obj.createdTime = this._time;
        this._obj.modifiedTime = this._time;
        this._obj.uploadlist = this.uploadDataList;
        this._obj.resizelist = this.uploadDataListResize;
        this._obj.t3 = "News";
        if (this.isValidate(this._obj)) {
            if (this._obj.syskey != 0) {
                this._obj.modifiedUserId = this.ics._profile.userID;
                this._obj.modifiedUserName = this.ics._profile.userName;
            } 
                this._obj.userId = this.ics._profile.userID;
                this._obj.userName = this.ics._profile.userName;

            this._obj.sessionId = this.ics._profile.sessionID;
            let url: string = this.ics.cmsurl + 'serviceContentNew/goSave';
            this._obj.n5 = Number(this.ics._profile.t1);
            let json: any = this._obj;
            console.log("Content Save JSON" + JSON.stringify(json));
            this.http.doPost(url, json).subscribe(
                data => {
                    this._result = data;
                    if (data.state) {
                        this.showMessage(data.msgDesc, true);
                        this.flagsave = false;
                        this.flagdelete = false;
                        this._obj.syskey = data.longResult[0];    
                    }
                    else {
                        this.showMessage(data.msgDesc, false);
                        this.flagsave = false;
                    }                                    
                },
                error => {
                    this.showMessage("Can't Saved This Record!", undefined);
                    this.flagsave = false;
                },
                () => { }
            );
        }
    }
    showMessage(msg, bool) {
        if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
        if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
        if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "primary", "t3": msg }); }
    }

    lovstatusType = [];
    _sessionObj = this.getSessionObj();
    getSessionObj() {
    return { "sessionID": "", "userID": "","lovDesc":"" };
    }
    
    getStatusType() {
        try {
            let url: string = this.ics.cmsurl + 'serviceAdmLOV/getLovType';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            this._sessionObj.lovDesc ="StatusType";
            let json: any = this._sessionObj;
            this.http.doPost(url, json).subscribe(
                data => {
                    if (data != null) {
                        if (data.msgCode == "0016") {
                            this.showMessage(data.msgDesc, false);
                        }
                        if (data.msgCode == "0000") {
                            if (!(data.lovType instanceof Array)) {
                                let m = [];
                                m[0] = data.lovType;
                                this.ref._lov3.StatusType = m;
                            } else {
                                this.ref._lov3.StatusType = data.lovType;
                            }
                            this.ref._lov3.StatusType.forEach((iItem) => {
                                let l_Item = { "value": "", "caption": "" };

                                l_Item['caption'] = iItem.caption;
                                l_Item['value'] = iItem.value;

                                this.lovstatusType.push(iItem);
                            });
                        }
                    }
                    this._mflag = true;
                },
                error => {
                    if (error._body.type == "error") {
                        alert("Connection Timed Out.");
                    }
                },
                () => { }
            );
        } catch (e) {
            alert("Invalid URL.");
        }
    }

    goDelete() {
            let url: string = this.ics.cmsurl + 'serviceContentNew/goDelete';
            this._obj.sessionId = this.ics._profile.sessionID;
            this._obj.userId = this.ics._profile.userID;
            let json: any = this._obj;
            this.http.doPost(url, json).subscribe(
                data => {
                    if (data.state){
                        this.goNew();
                        this.showMessage(data.msgDesc, true);
                    }else {
                        this.showMessage(data.msgDesc, false);                        
                    }
                },
                error => { },
                () => { }
            );
    }
    goList() {
        this._router.navigate(['/contentmenulist']);
    }
    goView(p) {
        this._router.navigate(['/eduView', 'read', p]);
    }

    uploadedFile(event, type) {
        this.flagcanvas = true;
        if (event.target.files.length == 1) {
            this._fileName = event.target.files[0].name;
            this._file = event.target.files[0];
            var index = this._fileName.lastIndexOf(".");
            var imagename = this._fileName.substring(index);
            imagename = imagename.toLowerCase();
            if (imagename == ".jpg" || imagename == ".jpeg" || imagename == ".png") {
                jQuery("#imagepopup").modal();
                let url = this.ics.cmsurl + 'fileAdm/fleUploadNew?f=upload&fn=' + this._fileName + '&id=' + this.ics._profile.t1 + '&type=0&imgUrl=' + this.ics._imgurl;
                this.http.upload(url, this._file).subscribe(
                    data => {
                        jQuery("#imagepopup").modal('hide');
                        if (data.code === 'SUCCESS') {
                            this.showMessage("Upload Successful", true);
                            let _img = this._fileName;
                            this.uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
                            this.uploadData.name = data.fileName;
                            this.uploadData.serial = ++this.serial;
                            this.uploadData.url = data.url;
                            this.uploadDataList.push(this.uploadData);
                            this.uploadDataListResize.push(data.sfileName);
                            this._fileName = "";
                        } else {
                            this.showMessage("Upload Unsuccessful  Please Try Again...", false);
                        }
                    },
                    error => { jQuery("#imagepopup").modal('hide'); },
                    () => { }
                );
            } else {                
                this.showMessage("Choose Image Associated!", undefined);
            }
        } else {
        }
    }

    isValidate(obj) {        
        if (obj.t1 == "") {
            this.showMessage("Please fill Title!", false);
            this.flagsave = false;
            return false;
        }
        else if (obj.t1.length > 500) {
            this.showMessage("Title is exceed limited 500 characters count!", false);
            this.flagsave = false;           
            return false;
        }
        else if (obj.t2 == "") {
            this.showMessage("Please fill Content!", false);
            this.flagsave = false;            
            return false;
        }        
        return true;
    }     

    setImgUrl(str) {
        console.log("" + JSON.stringify(this.ics._imgurl + 'upload/smallImage/contentImage/' + str))
        return this.ics._imgurl + 'upload/smallImage/contentImage/' + str;
    }

    /* goView(p) {
        this._router.navigate(['/eduView', 'read', p]);
    } */

    remove(index) {
        this.uploadDataList.splice(index, 1);
    }

    flag = false;    

    deleteFile(obj: any) {
        this.http.doGet(this.ics.cmsurl + 'fileAdm/fileRemoveContent?fn=' + obj.name + '&url=' + obj.url).subscribe(
            data => {
                if (data.code === 'SUCCESS') {
                    let index = this.uploadDataList.indexOf(obj);
                    this.uploadDataList.splice(index, 1);
                    this.showMessage("Photo Remove!", true);
                    jQuery("#imageUpload").val("");
                }
            },
            error => { },
            () => { }
        );
    }

    onDragStart(event, i, p, subarrname: Array<any>) {
        this.subarr = subarrname;
        this.pushcaption = p.name;
        this.splicearrindex1 = i;
        this.splicearrindex = -1;

    }

    subarr: Array<any>;
    pushcaption = "";
    splicearrindex = -1;
    splicearrindex1 = -1;

    substitueindex = 0;

    allowDrop(event) {
        event.preventDefault();
    }

    goSubstitute(event, p) {
        this.substitueindex = p;
    }    

    onDrop(event: any, i, p: any, arrname: Array<any>) {
        if (this.subarr.length > 0) {
            let sub = this.subarr[this.splicearrindex1];
            this.subarr.splice(this.splicearrindex1, 1);
            arrname.splice(this.substitueindex, 0, sub);
        }
    }        
}