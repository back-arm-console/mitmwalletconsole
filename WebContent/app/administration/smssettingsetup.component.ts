
import { hidden } from 'ansi-colors/types';
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { enableProdMode } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpInputComponent } from '../framework/rp-input.component';
import { RpHttpService } from '../framework/rp-http.service';
import { RpBean } from '../framework/rp-bean';
import { RpReferences } from '../framework/rp-references';
import { ClientUtil } from '../util/rp-client.util';
import { Observable } from 'rxjs/Rx';
declare var jQuery: any;
enableProdMode();
@Component({
  selector: 'smssetting-setup',
  template: `
  <div class="container-fluid">
  <div class="row clearfix">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
  <form class="form-horizontal" > 
  <!-- Form Name -->
  <legend>Message Setting</legend>
    <div class="cardview list-height">
      <div class="row col-md-12">  
        <button class="btn btn-sm btn-primary" type="button" (click)="goList()" >List</button> 
        <button class="btn btn-sm btn-primary" type="button" (click)="goNew()" >New</button>      
        <button class="btn btn-sm btn-primary" id="mySave" (click)="goSave()">Save</button>          
        <button class="btn btn-sm btn-primary" disabled id="mydelete"  type="button" (click)="goDelete();" >Delete</button> 
      </div>
      <div class="row col-md-12">&nbsp;</div> 
      <div class="form-group">    
      <div class="col-md-8">   			
        <div class="form-group">
          <label class="col-md-2">Service</label>
          <div class="col-md-4"> 
          <select *ngIf ="_chkread==true" disabled [(ngModel)]="_obj.serviceCode" [ngModelOptions]="{standalone: true}"  class="form-control input-sm" (change)="changeService($event)"  >
            <option *ngFor="let item of ref._lov3.refservice" value="{{item.value}}" >{{item.caption}}</option> 
          </select>    
        
          <select *ngIf ="_chkread !=true"  [(ngModel)]="_obj.serviceCode" [ngModelOptions]="{standalone: true}"  class="form-control input-sm" (change)="changeService($event)"  >
            <option *ngFor="let item of ref._lov3.refservice" value="{{item.value}}" >{{item.caption}}</option> 
          </select>           
          </div> 
        </div>
        <div class="form-group">
          <label class="col-md-2">Function</label>
          <div class="col-md-4" > 
          <select *ngIf ="_chkread==true" disabled [(ngModel)]="_obj.funCode" [ngModelOptions]="{standalone: true}" class="form-control input-sm" (change)="changeFuction($event)">
            <option *ngFor="let item of ref._lov3.reffunction" value="{{item.value}}" >{{item.caption}}</option> 
          </select> 
          <select *ngIf ="_chkread!=true" [(ngModel)]="_obj.funCode" [ngModelOptions]="{standalone: true}" class="form-control input-sm" (change)="changeFuction($event)">
            <option *ngFor="let item of ref._lov3.reffunction" value="{{item.value}}" >{{item.caption}}</option> 
          </select> 
          </div>  
        </div>
        <div class="form-group">
          <label class="col-md-2">Which Site</label>
          <div class="col-md-4" *ngIf="_onlyfrom == false"> 
          <select *ngIf ="_chkread==true" disabled [(ngModel)]="_obj.site" [ngModelOptions]="{standalone: true}" class="form-control input-sm">
            <option *ngFor="let item of ref._lov1.refsite" value="{{item.value}}" >{{item.caption}}</option>
          </select> 
          <select *ngIf ="_chkread!=true"  [(ngModel)]="_obj.site" [ngModelOptions]="{standalone: true}" class="form-control input-sm" (change)="changeSite($event)">
            <option *ngFor="let item of ref._lov1.refsite" value="{{item.value}}" >{{item.caption}}</option>
          </select>  
          </div>
          <div class="col-md-4"  *ngIf="_onlyfrom == true ">
          <select *ngIf ="_chkread==true" disabled  [(ngModel)]="_obj.site" [ngModelOptions]="{standalone: true}" class="form-control input-sm">
            <option *ngFor="let item of ref._lov1.reffrsite" value="{{item.value}}" >{{item.caption}}</option>                   
          </select>  
          <select *ngIf ="_chkread!=true"  [(ngModel)]="_obj.site" [ngModelOptions]="{standalone: true}" class="form-control input-sm" (change)="changeSite($event)">
            <option *ngFor="let item of ref._lov1.reffrsite" value="{{item.value}}" >{{item.caption}}</option>                   
          </select>                  
          </div> 
        </div> 
        <div *ngIf="_combohide == false">  
        <div class="form-group">
           <label class="col-md-2"> Merchant ID </label>
           <div class="col-md-4">
               <select [(ngModel)]="_obj.merchantID" [ngModelOptions]="{standalone: true}"  class="form-control input-sm" (change)="updateComboData($event.target.value)">
               <option *ngFor="let item of ref._lov3.ref015" value="{{item.value}}" >{{item.caption}}</option> 
              </select> 
           </div>
           <div class="col-md-1"> 
            {{_obj.merchantID}} 
            </div>
        </div>
         </div>
        <div class="form-group">
          <label class="col-md-2" >Active</label>
          <div class="col-md-4">
          <select [ngModelOptions]="{standalone: true}"  [(ngModel)]="_obj.active" class="form-control input-sm">
            <option *ngFor="let item of ref._lov1.refActive" value="{{item.value}}" >{{item.caption}}</option> 
          </select>                
          </div> 
        </div>
        <div class="form-group" *ngIf="languageFlat">
        <label class="col-md-2" >Language</label>
        <div class="col-md-4">
        <select [ngModelOptions]="{standalone: true}"  [(ngModel)]="_obj.language" class="form-control input-sm">
          <option *ngFor="let item of ref._lov1.refLanguage" value="{{item.value}}" >{{item.caption}}</option> 
        </select>                
        </div> 
      </div>
        <div class="form-group" [hidden]="frommsghide">
          <label class="col-md-2"> From Message </label>
          <div class="col-md-4">
          <textarea  class="form-control input-sm" [ngModelOptions]="{standalone: true}" [(ngModel)]="_obj.fromMsg" rows="7" maxlength="200"></textarea>                 
          </div> 
        </div>
        <div class="form-group" [hidden]="tomsghide">
          <label class="col-md-2">To Message </label>
          <div class="col-md-4">
          <textarea  class="form-control input-sm" [ngModelOptions]="{standalone: true}" [(ngModel)]="_obj.toMsg" rows="7" maxlength="200"></textarea>                 
          </div> 
        </div>	
      </div>
    </div>
    </div>
  </form>
  </div>
  </div>
</div>
     
  <div id="sessionalert" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <p>{{sessionAlertMsg}}</p>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>

<div [hidden] = "_mflag">
<div class="modal" id="loader"></div>
</div> 
    
  `
})
export class SMSSettingComponent {
  // RP Framework 
  subscription: Subscription;
  sub: any;
  _returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
  _obj = this.getDefaultObj();
  _mflag = false; _chkloginId = 'false'; _chkread: boolean;
  _onlyfrom: boolean; _combohide: boolean;
  msghide: boolean;
  frommsghide: boolean=false;
  tomsghide: boolean=true;
  _formhide:boolean;
  languageFlat:boolean=false;
  _key = ""; sessionAlertMsg = "";
  _util: ClientUtil = new ClientUtil();

  constructor(private ics: RpIntercomService, private _router: Router, private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences) {
    this.subscription = ics.rpbean$.subscribe(x => { })
    if (!ics.getRole() || ics.getRole() == 0) {
      this._router.navigate(['/login']);
    } else {
      this._formhide=false;
      this.checkSession();
      this.getAllMerchant();
      this.getServiceLov();
      this.getFunctionLov();
      this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
      this._obj = this.getDefaultObj();
    }
  }
  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let cmd = params['cmd'];
      if (cmd != null && cmd != "" && cmd == "read") {
        let id = params['id'];
        this._key = id;
        this.goReadBySyskey(id);       
      }
    });
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  getDefaultObj() {
    jQuery("#mydelete").prop("disabled", true);
    jQuery("#mySave").prop("disabled", false);
    this._onlyfrom = false; this._combohide = true; this._chkread = false;
    return { "code": "", "desc": "", "id": 0, "syskey": 0, "serviceCode": "", "serviceDesc": "", "funCode": "", "funDesc": "", "site": "", "from": "", "to": "","both":"", "fromMsg": "", "toMsg": "", "message": "", "merchantID": "", "operatorType": "", "active": "1", "sessionID": "", "userID": "" ,"language":"","languageDesc":""};
  }
  goReadBySyskey(p) {
    try { 
      this._formhide=true;     
      this._mflag = false;
      this._chkread = true;
      let url: string = this.ics.cmsurl + 'serviceCMS/getSMSSettingDataById?id=' + p + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
      this.http.doGet(url).subscribe(
        data => {          
          if (data.code == '0016') {
            this.sessionAlertMsg = data.desc;
            this.showMessage();
          }
          else {
            console.log('Your Service Coode is:'+this._obj.serviceCode);
            jQuery("#mydelete").prop("disabled", false);
            this._obj = data;
            console.log('Edit Data: '+JSON.stringify(this._obj));
            if (this._obj.serviceCode == "1") {
              this._onlyfrom = false;
              this.frommsghide = false;
            } else if (this._obj.serviceCode == "2") {
              this._onlyfrom = false;
              this.frommsghide = false;
            } else  if(this._obj.serviceCode=="3") {
              this._onlyfrom = true;
              this.frommsghide = true;
            }
            else{
              this._onlyfrom = false;
              this.frommsghide = false;
              this.tomsghide=false;
              this.languageFlat=true;
            }
            if (this._obj.from == "1") {
              this._obj.site = "From";
              this.frommsghide=false;
              this.tomsghide=true;
              
            } else if(this._obj.to=="1"){
              this._obj.site = "To";
              this.tomsghide=false;
              this.frommsghide=true;
            }
            else if(this._obj.both=="1"){
              this._obj.site ="Both";
            }
            if (this._obj.funCode == '1' || this._obj.funCode == '2') {
              this._combohide = false;
            } else {
              this._combohide = true;
            }
          }
          // for(let i=0;i<this.ref._lov3.refservice.length;i++){
          //   this.ref._lov3.refservice[i].code=this._obj.serviceCode;
          // }
          
          this._formhide=false; 
          this._mflag = true;
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
        }, () => { });
    } catch (e) {
      alert(e);
    }
  }
  goList() {
    this._router.navigate(['/smssettinglist']);
  }
  showMessage() {
    jQuery("#sessionalert").modal();
    Observable.timer(3000).subscribe(x => {
      this.goLogOut();
    });
  }

  goLogOut() {
    jQuery("#sessionalert").modal('hide');
    this._router.navigate(['/login']);
  }
  showMessageAlert(msg) {
    this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
  }
  clearData() {
    this._formhide=false;
    this.frommsghide=false;
    this.tomsghide=true;
    this.languageFlat=false;
    this._obj = this.getDefaultObj();
    this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
    this._chkloginId = 'false';
    jQuery("#mydelete").prop("disabled", true);
    jQuery("#mySave").prop("disabled", false);
  }
  goNew() {
    this.clearData();
  }
  changeFuction(event) {
    let options = event.target.options;
    let k = options.selectedIndex;//Get Selected Index
    let value = options[options.selectedIndex].value;//Get Selected Index's Value 
    this._obj.funCode = value;

    if (this._obj.funCode == '1' || this._obj.funCode == '2') {
      this._combohide = false;

    } else {
      this._combohide = true;
      this._obj.merchantID = "";
    }
    for (var i = 0; i < this.ref._lov3.reffunction.length; i++) {
      if (this.ref._lov3.reffunction[i].value == value) {
        this._obj.funCode = this.ref._lov3.reffunction[i].value;
        this._obj.funDesc = this.ref._lov3.reffunction[i].caption;
        break;
      }

    }
  }

  getFunctionLov() {
    this.http.doGet(this.ics.cmsurl + 'serviceAdmLOV/getFunctionLov?sessionID='+ this.ics._profile.sessionID + '&userID='+ this.ics._profile.userID).subscribe(
      data => {
        this.ref._lov3.reffunction = data.reffunction;
        let arr = [];
        if (this.ref._lov3.reffunction != null && this.ref._lov3.reffunction != undefined) {
          if (!(this.ref._lov3.reffunction instanceof Array)) {
            let m = [];
            m[1] = this.ref._lov3.reffunction;
            arr.push(m[1]);
          }
          for (let j = 0; j < this.ref._lov3.reffunction.length; j++) {
            arr.push(this.ref._lov3.reffunction[j]);
          }
          // this._obj.funCode = this.ref._lov3.reffunction[0].value;
        }else {
          this.ref._lov3.reffunction = [{ "value": "", "caption": "-" }];
        }
        this.ref._lov3.reffunction = arr;
        this._obj.active=this.ref._lov1.refActive[0].value;
        this._mflag = true;

      },
      error => {
        if (error._body.type == 'error') {
          alert("Connection Timed Out!");
        }
        else {

        }
      }, () => { }
    );
  }
  changeService(event) {
    let options = event.target.options;
    let k = options.selectedIndex;//Get Selected Index
    let value = options[options.selectedIndex].value;//Get Selected Index's Value 
    this._obj.serviceCode = value;
    if (this._obj.serviceCode == "1") {
      this._onlyfrom = false;
      this.frommsghide = false;
      this.tomsghide=true;
    } else if (this._obj.serviceCode == "2") {
      this._onlyfrom = false;
      this.frommsghide = false;
      this.tomsghide=true;
    } else if(this._obj.serviceCode=="3"){
      this._onlyfrom = true;
      this.frommsghide = true;
      this.tomsghide=true;
    }
    else if(this._obj.serviceCode=="4" &&this._obj.site=="Both" ){
      this._onlyfrom = false;
      this.frommsghide = false;
      this.tomsghide=false;
      this.languageFlat=true;
    }
    else{
      this._onlyfrom = false;
      this.frommsghide = false;
      this.tomsghide=true;
      this.languageFlat=true;
    }
    for (var i = 0; i < this.ref._lov3.refservice.length; i++) {
      if (this.ref._lov3.refservice[i].value == value) {
        this._obj.serviceCode = this.ref._lov3.refservice[i].value;
        this._obj.serviceDesc = this.ref._lov3.refservice[i].caption;
        break;
      }
    }
  }
  changeSite(event) {
    let options = event.target.options;
    let k = options.selectedIndex;//Get Selected Index
    let value = options[options.selectedIndex].value;//Get Selected Index's Value 
    this._obj.site = value;
    if (this._obj.site == "From") {
      this.frommsghide = false;
      this.tomsghide=true;
    } else if (this._obj.site == "To") {
      this.tomsghide = false;
      this.frommsghide=true;
    } else {
      this.frommsghide=false;
      this.tomsghide=false;
    }
    }
  getServiceLov() {
    this.http.doGet(this.ics.cmsurl + 'serviceAdmLOV/getServiceLov?sessionID='+ this.ics._profile.sessionID + '&userID='+ this.ics._profile.userID).subscribe(
      data => {
        if(data.refservice != null){
          this.ref._lov3.refservice = data.refservice;
          let arr = [];
          if (this.ref._lov3.refservice != null) {
            if (!(this.ref._lov3.refservice instanceof Array)) {
              let m = [];
              m[1] = this.ref._lov3.refservice;
              arr.push(m[1]);
            }
            for (let j = 0; j < this.ref._lov3.refservice.length; j++) {
              arr.push(this.ref._lov3.refservice[j]);
            }
            // this._obj.serviceCode = this.ref._lov3.refservice[0].value;
          }
          this.ref._lov3.refservice = arr;
          // this._obj.serviceCode=this.ref._lov3.refservice[0].value;
        }else this.ref._lov3.refservice =[{"sessionID":"","userID":""}];

      },
      error => {
        if (error._body.type == 'error') {
          //alert("Connection Timed Out!");
       }
        else {

      }
      }, () => { }
    );
  }

  getAllMerchant() {
    this.http.doGet(this.ics.cmsurl + 'serviceAdmLOV/getmerchantidlistdetail?sessionID='+ this.ics._profile.sessionID + '&userID='+ this.ics._profile.userID).subscribe(
      data => {
        this.ref._lov3.ref015 = data.ref015;
        if (this.ref._lov3.ref015 != null && this.ref._lov3.ref015 != undefined) {
          if (!(this.ref._lov3.ref015 instanceof Array)) {
            let m = [];
            m[0] = this.ref._lov3.ref015;
            this.ref._lov3.ref015 = m;
          }
        }else {
          this.ref._lov3.ref015 = [{ "value": "", "caption": "-" }];
        }

      },
      error => {
        // if (error._body.type == 'error') {
          //alert("Connection Timed Out!");
        //}
      }, () => { }
    );
  }
  goSave() {
    if (this._obj.serviceCode == '') {
      this.showMsg("Please select Service",false);
    }
    else if (this._obj.funCode == '') {
      this.showMsg("Please select Function",false);
    }
    else if (this._obj.site == '') {
      this.showMsg("Please select Site",false);
    }
    // else if (this._obj.message == '' && this._obj.serviceCode != '3') {
    //     this.showMsg("Please input Message",false);
    //   }    
    else if (this._obj.active == '') {
      this.showMsg("Please select Active",false);
    }
    else if (this._combohide == false) {
      if (this._obj.merchantID == '') {
        this.showMsg("Please select Merchant",false);
      }
      else {
        this.saveSMSSetting();
      }
    }
    else {
      this.saveSMSSetting();
    }
  }

  saveSMSSetting() {
    try {
      this._mflag = false;
      if (this._obj.site == "From") {
        this._obj.from = "1";
        this._obj.to = "0";
        this._obj.both="0";
      } else if(this._obj.site=="To"){
        this._obj.from = "0";
        this._obj.to = "1";
        this._obj.both="0";
      }
      else{
        this._obj.from = "0";
        this._obj.to = "0";
        this._obj.both="1";
      }

      for(let j=0;j<this.ref._lov3.reffunction.length;j++){
        if(this._obj.funCode==this.ref._lov3.reffunction[j].value){
          this._obj.funDesc=this.ref._lov3.reffunction[j].caption;
        }
      }
      for(let i=0;i<this.ref._lov3.refservice.length;i++){
        if(this._obj.serviceCode==this.ref._lov3.refservice[i].value){
          this._obj.serviceDesc=this.ref._lov3.refservice[i].caption;
        }
      }
      for(let k=0;k<this.ref._lov1.refLanguage.length;k++){
        if(this._obj.language==this. ref._lov1.refLanguage[k].value){
          this._obj.languageDesc=this. ref._lov1.refLanguage[k].caption;
        }
      }
      this._obj.sessionID = this.ics._profile.sessionID;
      this._obj.userID = this.ics._profile.userID;
      let url: string = this.ics.cmsurl + 'serviceCMS/saveSMSSetting';
      let json: any = this._obj;
      this.http.doPost(url, json).subscribe(
        data => {
          this._returnResult = data;
          if (this._returnResult.state == 'true') {
            this._obj.id = this._returnResult.keyResult;
            jQuery("#mydelete").prop("disabled", false);
            this.showMessageAlert(data.msgDesc);
          } else {
            if (this._returnResult.msgCode == '0016') {
              this.sessionAlertMsg = data.desc;
              this.showMsg(data.desc,false);
            }
            else {
              this.showMsg(data.msgDesc,true);
            }
          }
          this._mflag = true;
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
        }, () => { });
    }
    catch (e) {
      alert(e);
    }
  }

  goDelete() {
    try {
      this._mflag = false;
      this._obj.sessionID = this.ics._profile.sessionID;
      this._obj.userID = this.ics._profile.userID;
      let json: any = this._obj;
      let url: string = this.ics.cmsurl + 'serviceCMS/deleteSMSSetting';
      this.http.doPost(url, json).subscribe(
        data => {
          this._returnResult = data;
          if (data.msgCode == '0016') {
            this.sessionAlertMsg = data.desc;
            this.showMsg(data.desc,false);
          }
          else {
            this.showMsg(data.msgDesc,true);
            if (data.state) {
              this.clearData();
            }
          }
          this._mflag = true;
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
        }, () => { });
    } catch (e) {
      alert(e);
    }

  }
  getCodeFromLovdeatils() {
    try {
      this.http.doGet(this.ics.cmsurl + 'serviceCMS/getCodeFromLovdeatils?merchantId=' + this._obj.merchantID).subscribe(
        data => {
          if (data != null && data != undefined) {
            if (!(data.reflovdetails instanceof Array)) {//Object
              let m = [];
              m[0] = data.reflovdetails;
              this.ref._lov3.reflovdetails = m;
            }
            else {
              this.ref._lov3.reflovdetails = data.reflovdetails;
            }
          }
          else {
            this.ref._lov3.reflovdetails = [{ "value": "", "caption": "-" }];
          }
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
          else {

          }
        }, () => { });
    } catch (e) {
      alert(e);
    }
  }
  updateComboData(event) {
    this._obj.merchantID = event;
    this.getCodeFromLovdeatils();
  }

  // about session validation
  sessionTimeoutMsg = "";
  _sessionObj = this.getSessionObj();
  getSessionObj() {
    return { "sessionID": "", "userID": "" };
  }

  checkSession() {
    try {
      let url: string = this.ics._apiurl + 'service001/checkSessionTime';

      this._sessionObj.sessionID = this.ics._profile.sessionID;
      this._sessionObj.userID = this.ics._profile.userID;
      let json: any = this._sessionObj;

      this.http.doPost(url, json).subscribe(
        data => {
          if (data != null) {
            if (data.code == "0016") {
              this.showMsgAlert(data.desc);
              this.logout();
            }

            if (data.code == "0014") {
              this.showMsgAlert(data.desc);
            }
          }
        },
        error => {
          if (error._body.type == "error") {
            alert("Connection Timed Out.");
          }
        },
        () => { }
      );
    } catch (e) {
      alert("Invalid URL.");
    }
  }

  showMsgAlert(msg) {
    this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
  }

  logout() {
    this._router.navigate(['/login']);
  }
  showMsg(msg, bool) {
    if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
    if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
    if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
  }
}