import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { enableProdMode } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpInputComponent } from '../framework/rp-input.component';
import { RpHttpService } from '../framework/rp-http.service';
import { RpBean } from '../framework/rp-bean';
import { RpReferences } from '../framework/rp-references';
import { ClientUtil } from '../util/rp-client.util';
import { Observable } from 'rxjs/Rx';
declare var jQuery: any;
enableProdMode();
@Component({
  selector: 'flexcommrate-setup',
  template: `
  <div class="container-fluid">
  <div class="row clearfix">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0"> 
  <form class= "form-horizontal" (ngSubmit) = "goSave()"> 
  <!-- Form Name -->
  <legend>Commission</legend>
  <div class="cardview list-height">
  <div class="row col-md-12">  
    <button class="btn btn-sm btn-primary" type="button" (click)="goList()" >List</button> 
    <button class="btn btn-sm btn-primary" type="button" (click)="goNew()" >New</button>      
    <button class="btn btn-sm btn-primary" id="mySave" type="submit" >Save</button>          
    <button class="btn btn-sm btn-primary" disabled id="mydelete"  type="button" (click)="goDelete();" >Delete</button> 
  </div>
    
  <div class="row col-md-12">&nbsp;</div>
  <div class="form-group">
  <div class="col-md-10">

  <div class="form-group">
    <rp-input [(rpModel)]="_list.commRef" rpType="text" rpClass="col-md-4" rpLabelClass ="col-md-2 control-label" rpLabel="Commission Reference" rpReadonly="true"></rp-input>
  </div>

  <div class="form-group">
    <rp-input [(rpModel)]="_list.description"rpType="text" rpClass="col-md-4" rpLabelClass ="col-md-2 control-label" rpLabel="Description " rpLabelRequired="true"></rp-input>
  </div>

  <div class="form-group" >          
    <label class="col-md-2" > Main Type <font class="mandatoryfont">*</font></label> 
    <div class="col-md-4">
      <input type = "checkbox" [(ngModel)]="chkMainType" (change)="checkMainType($event)" [ngModelOptions]="{standalone: true}"> &nbsp; Is Tier                   
    </div>
  </div> 

  <div class="form-group">
    <label class="col-md-2"> Zone <font class="mandatoryfont">*</font> </label>
    <div class="col-md-4">
    <select [ngModelOptions]="{standalone: true}"  (change)="changeZone($event)"  [(ngModel)]="_obj.zone" class="form-control input-sm">
      <option *ngFor="let item of ref._lov1.ref026" value="{{item.value}}" >{{item.caption}}</option> 
    </select>                
    </div> 
  </div>

  <div class="form-group">
  <label class="col-md-2"> Commission Type <font class="mandatoryfont">*</font> </label>
  <div class="col-md-4">
    <select [ngModelOptions]="{standalone: true}"  [(ngModel)]="_obj.commType" (change)="changeCommissionType($event)"  class="form-control input-sm">
    <option *ngFor="let item of ref._lov1.ref027" value="{{item.value}}" >{{item.caption}}</option> 
    </select>                
    </div> 
  </div>
  
  <div *ngIf="isMainType == false">
    <div class="form-group">
    <rp-input [(rpModel)]="_obj.fromAmt" rpLabel="From Amount " rpType="text" rpClass="col-md-4"  (keydown)="restrictSpecialCharacter($event, 101 ,_obj.fromAmt)" pattern="[0-9.,]+" rpLabelClass ="col-md-2 control-label" rpLabelRequired="true"></rp-input>
    </div>

    <div class="form-group">
    <rp-input [(rpModel)]="_obj.toAmt" rpLabel="To Amount " rpType="text" rpClass="col-md-4" (keydown)="restrictSpecialCharacter($event, 101 ,_obj.toAmt)" pattern="[0-9.,]+" rpLabelClass ="col-md-2 control-label" rpLabelRequired="true"></rp-input>
    </div>
  </div>

  <div class="form-group">
    <rp-input [(rpModel)]="_obj.amount" rpLabel="Commission Amount " rpType="text" rpClass="col-md-4"  (keydown)="restrictSpecialCharacter($event, 101 ,_obj.amount)" pattern="[0-9.,]+" rpLabelClass ="col-md-2 control-label" rpLabelRequired="true"></rp-input>
  </div>

  <div *ngIf="isPercentage == false">
    <div class="form-group">
    <rp-input [(rpModel)]="_obj.minAmt" rpLabel="Minimum Amount " rpType="text" rpClass="col-md-4"  (keydown)="restrictSpecialCharacter($event, 101 ,_obj.minAmt)" pattern="[0-9.,]+" rpLabelClass ="col-md-2 control-label" rpLabelRequired="true"></rp-input>
    </div>

    <div class="form-group">
    <rp-input [(rpModel)]="_obj.maxAmt" rpLabel="Maximum Amount " rpType="text" rpClass="col-md-4"  (keydown)="restrictSpecialCharacter($event, 101 ,_obj.maxAmt)" pattern="[0-9.,]+" rpLabelClass ="col-md-2 control-label" rpLabelRequired="true"></rp-input>
    </div>
  </div>

  <div class="form-group" style="padding-left:15px"> 
    <form> <button class="btn btn-sm btn-primary" id="myadd" (click)="goAddAll()" >Add</button> </form>  
  </div> 
  </div>
    
  <!--Start Table-->
  <div class="row col-md-12" style="overflow-x:auto; padding-left:30px">
    <table class="table table-striped table-condensed table-hover tblborder">
    <thead>
      <tr>
      <th>Sr No.</th>
      <th>Zone</th>
      <th>Commission Type</th>
      <th style="text-align:right;">From Amount</th>
      <th style="text-align:right;">To Amount</th>
      <th style="text-align:right;">Commission Amount</th>
      <th style="text-align:right;">Minimum Amount </th>
      <th style="text-align:right;">Maximum Amount </th>  
      <th>&nbsp; </th> 
      </tr>
    </thead>                
    <tbody>
      <tr *ngFor = "let obj of _showlist.commdetailArr,let i=index">                  
      <td class = "col-md-1">  {{i+1 }}</td>
      <td class = "col-md-2" > {{obj.zone}} </td>                    
      <td class = "col-md-2" *ngIf="obj.commType == 0"> Fix </td>
      <td class = "col-md-2" *ngIf="obj.commType == 1"> Percentage </td>                    
      <td class = "col-md-2 right" > {{obj.fromAmt}}</td>
      <td class = "col-md-2 right" > {{obj.toAmt}} </td>
      <td class = "col-md-2 right" > {{obj.amount}} </td>  
      <td class = "col-md-2 right" > {{obj.minAmt}} </td>
      <td class = "col-md-2 right" > {{obj.maxAmt}} </td>  
      <td> <img src="image/remove.png" alt="remove.png" height="20" width="20"  (click)="goRemove(obj)"/></td>
      </tr>
    </tbody>
    </table> 
    </div>
  <!--End Table--> 
  </div>
  </div>
  </form>
  </div>
  </div>
  </div>
     
  <div id="sessionalert" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <p>{{sessionAlertMsg}}</p>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>

<div [hidden] = "_mflag">
<div class="modal" id="loader"></div>
</div> 
    
  `
})
export class FlexCommRateSetup {
  // RP Framework 
  subscription: Subscription;
  sub: any;
  _returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
  _list = { "commdetailArr": [{ "syskey": 0, "hkey": 0, "zone": "", "commType": "", "fromAmt": "", "toAmt": "", "amount": "", "minAmt": "", "maxAmt": "", "gLorAC": "" }], "mainType": "", "commRef": "TBA", "msgCode": "", "msgDesc": "", "createdUserID": "", "sessionID": "", "description": "" };
  _showlist = { "commdetailArr": [{ "syskey": 0, "hkey": "", "zone": "", "commType": "", "fromAmt": "", "toAmt": "", "amount": "", "minAmt": "", "maxAmt": "", "gLorAC": "" }], "mainType": "", "commRef": "TBA", "msgCode": "", "msgDesc": "", "createdUserID": "", "sessionID": "", "description": "" };
  _obj = this.getDefaultObj();
  _mflag = false; isMainType: boolean; chkMainType: boolean; isPercentage: boolean;
  _key = ""; hKey = 0; sessionAlertMsg = ""; _zonecaption = "";
  _util: ClientUtil = new ClientUtil();

  constructor(private ics: RpIntercomService, private _router: Router, private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences) {
    this.subscription = ics.rpbean$.subscribe(x => { })
    if (!ics.getRole() || ics.getRole() == 0) {
      this._router.navigate(['/login']);
    } else {
      this._mflag = false;
      this.checkSession();
      this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
      this._obj = this.getDefaultObj();
      this.getZone();
    }
  }
  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let cmd = params['cmd'];
      if (cmd != null && cmd != "" && cmd == "read") {
        let id = params['id'];
        this._key = id;
        this.goReadBySyskey(id);
      }
    });
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }


  goRemove(i) {
    let index = this._showlist.commdetailArr.indexOf(i);
    this._showlist.commdetailArr.splice(index, 1);
    this._list.commdetailArr.splice(index, 1);
   // this.hKey = 0;
  }

  goSave() {
    this._mflag = false;
    this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "msgCode": "" };
    if (this._list.commdetailArr.length < 1) {
      this._returnResult.state = 'false';
      this._returnResult.msgDesc = "No data to save";
      this._mflag = true;
    }
    if (this._list.description == '') {
      this._returnResult.state = "false";
      this._returnResult.msgDesc = "Blank Description";
      this._mflag = true;
    }
    if (this._returnResult.msgDesc == '') {
      try {
        this._list.createdUserID = this.ics._profile.userID;
        this._list.sessionID = this.ics._profile.sessionID;
        this._list.commdetailArr[0].hkey = this.hKey;
        console.log("commdetail arr hkey is here : " +JSON.stringify(this._list.commdetailArr[0].hkey));
        let url: string = this.ics.cmsurl + 'serviceCMS/saveFlexCommRate';
        let json: any = this._list;
        this.http.doPost(url, json).subscribe(
          data => {

            this._obj = data;
            this._list.commRef = this._obj.commRef;
            this._list.commdetailArr[0].hkey = this._obj.hkey;
            this.hKey = this._obj.hkey;

            this._returnResult.msgDesc = this._obj.msgDesc;
            this._returnResult.state = this._obj.state;

            if (this._obj.state == 'false') {
              if (this._obj.msgCode == '0016') {
                this.showMessage(this._obj.msgDesc,false);
              }

            } else {
              jQuery("#mydelete").prop("disabled", false);
            }

            this.showMessage(this._returnResult.msgDesc,true);
            this._mflag = true;
          },
          error => {
            if (error._body.type == 'error') {
              alert("Connection Timed Out!");
            }
          }, () => { });
      } catch (e) {
        alert(e);
      }
    }
    else {
      this.showMessage(this._returnResult.msgDesc,false);
    }

  }


  getDefaultObj() {
    this.isMainType = true;
    this.chkMainType = false;
    this.isPercentage = true;
    this._list = { "commdetailArr": [{ "syskey": 0, "hkey": 0, "zone": "", "commType": "", "fromAmt": "", "toAmt": "", "amount": "", "minAmt": "", "maxAmt": "", "gLorAC": "" }], "mainType": "", "commRef": "TBA", "msgCode": "", "msgDesc": "", "createdUserID": "", "sessionID": "", "description": "" };
    this._list.commdetailArr = [];
    this._showlist.commdetailArr = [];
    //this.goRemove(0);

    jQuery("#mydelete").prop("disabled", true);
    jQuery("#mySave").prop("disabled", false);
    return { "createdUserID": "", "sessionID": "", "hkey": 0, "msgCode": "", "msgDesc": "", "syskey": 0, "commRef": "TBA", "description": "", "mainType": "", "zone": "Same City", "amount": "", "fromAmt": "", "toAmt": "", "minAmt": "", "maxAmt": "", "commType": "Fix", "state": "" };
  }

  goReadBySyskey(p) {
    try {
      this._mflag = false;
      let url: string = this.ics.cmsurl + 'serviceCMS/getFlexCommRateDataByID?id=' + p + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
      this.http.doGet(url).subscribe(
        data => {
          if (data != null) {
            if (data.commdetailArr != null) {
              if (!(data.commdetailArr instanceof Array)) {
                let m = [];
                m[0] = data.commdetailArr;
                console.log("mdata is here : " + JSON.stringify(m[0]));
                this._list.commdetailArr = m;
                console.log("mdata is here : " + JSON.stringify(this._list.commdetailArr));
                this._list.commRef = data.commRef;
                this._list.description = data.description;
                this._list.mainType = data.mainType;

                for (let i = 0; i < this.ref._lov1.ref026.length; i++) {
                  if (data.commdetailArr.zone == this.ref._lov1.ref026[i].value) {
                    this._zonecaption = this.ref._lov1.ref026[i].caption;
                    this._showlist.commdetailArr.push({ "syskey": 0, "hkey": "", "zone": this._zonecaption, "commType": data.commdetailArr.commType, "fromAmt": String(Number.parseFloat(data.commdetailArr.fromAmt).toFixed(2)), "toAmt": String(Number.parseFloat(data.commdetailArr.toAmt).toFixed(2)), "amount": String(Number.parseFloat(data.commdetailArr.amount).toFixed(2)), "minAmt": String(Number.parseFloat(data.commdetailArr.minAmt).toFixed(2)), "maxAmt": String(Number.parseFloat(data.commdetailArr.maxAmt).toFixed(2)), "gLorAC": "" });
                    break;
                  }
                }
              }
              else {
                this._list = data;
                for (let j = 0; j < data.commdetailArr.length; j++) {
                  for (let i = 0; i < this.ref._lov1.ref026.length; i++) {
                    if (data.commdetailArr[j].zone == this.ref._lov1.ref026[i].value) {
                      this._zonecaption = this.ref._lov1.ref026[i].caption;
                      this._obj.zone = this.ref._lov1.ref026[i].value;
                      this._showlist.commdetailArr.push({ "syskey": 0, "hkey": "", "zone": this._zonecaption, "commType": data.commdetailArr[j].commType, "fromAmt": String(Number.parseFloat(data.commdetailArr[j].fromAmt).toFixed(2)), "toAmt": String(Number.parseFloat(data.commdetailArr[j].toAmt).toFixed(2)), "amount": String(Number.parseFloat(data.commdetailArr[j].amount).toFixed(2)), "minAmt": String(Number.parseFloat(data.commdetailArr[j].minAmt).toFixed(2)), "maxAmt": String(Number.parseFloat(data.commdetailArr[j].maxAmt).toFixed(2)), "gLorAC": "" });
                      break;
                    }
                  }
                }
              }
              this.hKey = this._list.commdetailArr[0].hkey;
              if (this._list.mainType == "0") {
                this.chkMainType = false;
                this.isMainType = true;
              } else {
                this.chkMainType = true;
                this.isMainType = false;
              }
            }
          }

          if (this._list.msgCode == '0016') {
            this._returnResult.msgDesc = this._list.msgDesc;
            this.showMessage(this._list.msgDesc,false);
          }
          jQuery("#mydelete").prop("disabled", false);
          jQuery("#mySave").prop("disabled", false);
          this._mflag = true;
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
        }, () => { });
    } catch (e) {
      alert(e);
    }

  }

  goList() {
    this._router.navigate(['/flexcommratelist']);
  }
 // showMessage() {
   // jQuery("#sessionalert").modal();
  //  Observable.timer(3000).subscribe(x => {
  //    this.goLogOut();
 //   });
 // }

  goLogOut() {
    jQuery("#sessionalert").modal('hide');
    this._router.navigate(['/login']);
  }

  /* checkSession() {
    try {
  
      let url: string = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
      this.http.doGet(url).subscribe(
        data => {
          if (data.msgCode == '0016') {
            this.sessionAlertMsg = data.msgDesc;
            this.showMessage();
          }
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
        }, () => { });
    } catch (e) {
      alert(e);
    }
  
  } */

  showMessageAlert(msg) {
    this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
  }

  clearData() {
    this._obj = this.getDefaultObj();
    this.hKey = 0;
    this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
    jQuery("#mySave").prop("disabled", false);
    jQuery("#mydelete").prop("disabled", true);
  }

  goNew() {
    this.clearData();
  }


  changeCommissionType(event) {
    let options = event.target.options;
    let k = options.selectedIndex;//Get Selected Index
    let value = options[options.selectedIndex].value;//Get Selected Index's Value  
    this._obj.commType = value;

    if (this._obj.commType == '1') {
      this.isPercentage = false;
    } else {
      this.isPercentage = true;
      this._obj.commType = this.ref._lov1.ref027[0].value;
    }

    for (var i = 1; i < this.ref._lov1.ref027.length; i++) {
      if (this.ref._lov1.ref027[i].value == value) {
        this._obj.commType = this.ref._lov1.ref027[i].value;
        //ethis._obj.commType = this.ref._lov1.ref027[0].value;
      }
      //this._obj.commType = this.ref._lov1.ref027[0].value;
    }
   // this._obj.commType = this.ref._lov1.ref027[0].value;
  }

  goAddAll(p) {

    this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "msgCode": "" };

    if (this._obj.amount == '') {
      this._returnResult.state = "false";
      this._returnResult.msgDesc = "Blank Commission Amount";
    } else if (!/^([0-9.]{1,50})$/.test(this._obj.amount)) {
      this._returnResult.state = 'false';
      this._returnResult.msgDesc = "Amount should be number";
    } else if (this._obj.zone == '') {
      this._returnResult.state = "false";
      this._returnResult.msgDesc = "Blank Zone";
    } 
   
    if (this.isMainType == false) {

      if (this._obj.fromAmt == '') {
        this._returnResult.state = "false";
        this._returnResult.msgDesc = "Blank From Amount";
      } else if (!/^([0-9.]{1,50})$/.test(this._obj.fromAmt)) {
        this._returnResult.state = 'false';
        this._returnResult.msgDesc = "From Amount should be number";
      }

      if (this._obj.toAmt == '') {
        this._returnResult.state = "false";
        this._returnResult.msgDesc = "Blank To Amount";
      } else if (!/^([0-9.]{1,50})$/.test(this._obj.toAmt)) {
        this._returnResult.state = 'false';
        this._returnResult.msgDesc = "To Amount should be number";
      }
    }

    if (this.isPercentage == false) {
      if (this._obj.minAmt == '') {
        this._returnResult.state = "false";
        this._returnResult.msgDesc = "Blank Minimum Amount";
      } else if (!/^([0-9.]{1,50})$/.test(this._obj.minAmt)) {
        this._returnResult.state = 'false';
        this._returnResult.msgDesc = "Minimum Amount should be number";
      }

      if (this._obj.maxAmt == '') {
        this._returnResult.state = "false";
        this._returnResult.msgDesc = "Blank Maximum Amount";
      } else if (!/^([0-9.]{1,50})$/.test(this._obj.maxAmt)) {
        this._returnResult.state = 'false';
        this._returnResult.msgDesc = "Maximum Amount should be number";
      }
    }

    if(this._obj.commType == ''){
      this._returnResult.state = 'false';
        this._returnResult.msgDesc = "Blank Commission Type.";
    }
 
    if (this._returnResult.msgDesc == '') {
      this._list.commdetailArr.push({ "syskey": 0, "hkey": 0, "zone": this._obj.zone, "commType": this._obj.commType, "fromAmt": this._obj.fromAmt, "toAmt": this._obj.toAmt, "amount": this._obj.amount, "minAmt": this._obj.minAmt, "maxAmt": this._obj.maxAmt, "gLorAC": "" });
      this._showlist.commdetailArr.push({ "syskey": 0, "hkey": "", "zone": this._zonecaption, "commType": this._obj.commType, "fromAmt": this._obj.fromAmt, "toAmt": this._obj.toAmt, "amount": this._obj.amount, "minAmt": this._obj.minAmt, "maxAmt": this._obj.maxAmt, "gLorAC": "" });

    } else {
      this.showMessage(this._returnResult.msgDesc,false);
    }

  }

  getZone() {
    try {
      this._mflag = false;
      this.http.doGet(this.ics.cmsurl + 'serviceAdmLOV/getZone').subscribe(
        data => {
          this.ref._lov1.ref026 = data.ref026;
          this._mflag = true;
          this._obj.zone = this.ref._lov1.ref026[0].value;
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
        },
        () => { }
      );

    } catch (e) {
      alert(e);
    }
  }

  changeZone(event) {
    let options = event.target.options;
    let k = options.selectedIndex;//Get Selected Index
    let value = options[options.selectedIndex].value;//Get Selected Index's Value  
    this._obj.zone = value;
    this._zonecaption = options[options.selectedIndex].text;
    for (var i = 1; i < this.ref._lov1.ref026.length; i++) {
      if (this.ref._lov1.ref026[i].value == value) {
        this._obj.zone = this.ref._lov1.ref026[i].value;
      }
    }
  }

  checkMainType(event) {

    if (event.target.checked) {
      this.isMainType = false;
      this._list.mainType = "1";
    }
    else {
      this.isMainType = true;
      this._list.mainType = "0";
    }

  }
  goDelete() {
    try {
      this._mflag = false;
      this._list.sessionID = this.ics._profile.sessionID;
      this._list.createdUserID = this.ics._profile.userID;
      let url: string = this.ics.cmsurl + 'serviceCMS/deleteFlexCommRate';
      let json: any = this._list;
      this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "msgCode": "" };
      this.http.doPost(url, json).subscribe(
        data => {
          this._returnResult = data;
          if (data.msgCode == '0016') {
            this.sessionAlertMsg = data.msgDesc;
            this.showMessage(data.msgDesc,false);
          }
          else {
            this.showMessage(data.msgDesc,true);
            if (data.state) {
              jQuery("#mydelete").prop("disabled", true);
              jQuery("#mySave").prop("disabled", false);
              this._obj = { "createdUserID": "", "sessionID": "", "msgCode": "", "hkey": 0, "msgDesc": "", "syskey": 0, "commRef": "TBA", "description": "", "mainType": "", "zone": "", "amount": "", "fromAmt": "", "toAmt": "", "minAmt": "", "maxAmt": "", "commType": "", "state": "" };
              this._list = {
                "commdetailArr": [{ "syskey": 0, "hkey": 0, "zone": "", "commType": "", "fromAmt": "", "toAmt": "", "amount": "", "minAmt": "", "maxAmt": "", "gLorAC": "" }], "mainType": "", "commRef": "TBA", "msgCode": "", "msgDesc": "", "createdUserID": "", "sessionID": "",
                "description": ""
              };
              this._showlist = { "commdetailArr": [{ "syskey": 0, "hkey": "", "zone": "", "commType": "", "fromAmt": "", "toAmt": "", "amount": "", "minAmt": "", "maxAmt": "", "gLorAC": "" }], "mainType": "", "commRef": "TBA", "msgCode": "", "msgDesc": "", "createdUserID": "", "sessionID": "", "description": "" };
              this.goRemove(0);
            }
          }
          this._mflag = true;
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
        }, () => { });
      this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "msgCode": "" };
    } catch (e) {
      alert(e);
    }
  }

  // about session validation
  sessionTimeoutMsg = "";
  _sessionObj = this.getSessionObj();
  getSessionObj() {
    return { "sessionID": "", "userID": "" };
  }

  checkSession() {
    try {
      let url: string = this.ics._apiurl + 'service001/checkSessionTime';

      this._sessionObj.sessionID = this.ics._profile.sessionID;
      this._sessionObj.userID = this.ics._profile.userID;
      let json: any = this._sessionObj;

      this.http.doPost(url, json).subscribe(
        data => {
          if (data != null) {
            if (data.code == "0016") {
              this.showMessage(data.desc,false);
              this.logout();
            }

            if (data.code == "0014") {
              this.showMessage(data.desc,false);
            }
          }
        },
        error => {
          if (error._body.type == "error") {
            alert("Connection Timed Out.");
          }
        },
        () => { }
      );
    } catch (e) {
      alert("Invalid URL.");
    }
  }

  showMsgAlert(msg) {
    this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
  }

  logout() {
    this._router.navigate(['/login']);
  }
  showMessage(msg, bool) {
    if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
    if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
    if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
  }
  restrictSpecialCharacter(event, fid, value) {
    if ((event.key >= '0' && event.key <= '9') || event.key == '.' || event.key == ',' || event.key == 'Home' || event.key == 'End' ||
        event.key == 'ArrowLeft' || event.key == 'ArrowRight' || event.key == 'Delete' || event.key == 'Backspace' || event.key == 'Tab' ||
        (event.which == 67 && event.ctrlKey === true) || (event.which == 88 && event.ctrlKey === true) ||
        (event.which == 65 && event.ctrlKey === true) || (event.which == 86 && event.ctrlKey === true)) {
        if (fid == 101) {
            if (/^[\\\"\'\;\:\>\|~`!@#\$%^&*\(\)]$/i.test(event.key)) {
                event.preventDefault();
            }
        }

        if (value.includes(".")) {
            if (/^[\\\"\'\;\:\>\|~`!@#\$%^&*.\(\)]$/i.test(event.key)) {
                event.preventDefault();
            }
        }
    }
    else {
        event.preventDefault();
    }
}

}