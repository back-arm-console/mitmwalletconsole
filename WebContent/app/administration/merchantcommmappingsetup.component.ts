import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { enableProdMode } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpInputComponent } from '../framework/rp-input.component';
import { RpHttpService } from '../framework/rp-http.service';
import { RpBean } from '../framework/rp-bean';
import { RpReferences } from '../framework/rp-references';
import { ClientUtil } from '../util/rp-client.util';
import { Observable } from 'rxjs/Rx';
declare var jQuery: any;
enableProdMode();
@Component({
  selector: 'merchantcommapping-setup',
  template: `
  <div class="container-fluid">
  <div class="row clearfix">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0"> 
  <form class= "form-horizontal" (ngSubmit) = "goSave()"> 
  <!-- Form Name -->
  <legend>Merchant Commission </legend>
  <div class="cardview list-height">
    <div class="row col-md-12"> 
      <button class="btn btn-sm btn-primary" type="button" (click)="goList()" >List</button> 
      <button class="btn btn-sm btn-primary" type="button" (click)="goNew()" >New</button>      
      <button class="btn btn-sm btn-primary" id="mySave" type="submit" >Save</button>          
      <button class="btn btn-sm btn-primary" disabled id="mydelete"  type="button" (click)="goDelete();" >Delete</button> 
    </div>
        
    <div class="row col-md-12">&nbsp;</div>
    <div class="form-group">
    <div class="col-md-8">
    <div class="form-group">
      <label class="col-md-2">Merchant ID</label>
      <div class="col-md-4" > 
        <select *ngIf ="_mercbohide==true" disabled [(ngModel)]="_obj.merchantID" (change)="changeMerchant($event)" [ngModelOptions]="{standalone: true}" class="form-control input-sm" required>
        <option *ngFor="let item of ref._lov3.ref015" value="{{item.value}}" >{{item.caption}}</option>
        </select> 
        
        <select *ngIf ="_mercbohide!=true" [(ngModel)]="_obj.merchantID" (change)="changeMerchant($event)" [ngModelOptions]="{standalone: true}" class="form-control input-sm" required>
        <option *ngFor="let item of ref._lov3.ref015" value="{{item.value}}" >{{item.caption}}</option>
        </select> 
      </div>   
      <div class="col-md-1"> 
      {{_obj.merchantID}} 
      </div>
    </div>

    <div class="form-group"> 
      <label class="col-md-2" ></label> 
      <div class="col-md-4" >  
      <label class="radio-inline">
        <input #m [checked]="_obj.kindOfComIssuer == m.value" (click)="_obj.kindOfComIssuer = m.value" name="kindOfComIssuer" value="0" type="radio"> Merchant
      </label>
      <label class="radio-inline">
        <input  #c [checked]="_obj.kindOfComIssuer == c.value" (click)="_obj.kindOfComIssuer = c.value" name="kindOfComIssuer" value="1" type="radio"> Customer
      </label>
      <label class="checkbox-inline" >
      <input  type="checkbox" [ngModelOptions]="{standalone: true}" [(ngModel)]="_obj.chkAdvance" (click)="update($event)"> Advanced
      </label>
      </div>         
    </div> 

     <div class="form-group" [hidden]="_combohide">
      <label class="col-md-2" >Operator Type <font class="mandatoryfont">*</font></label>
      <div class="col-md-4"  >
      <select [ngModelOptions]="{standalone: true}" [(ngModel)]="_obj.t1" class="form-control input-sm" id="comboadvance" >
        <option *ngFor="let item of ref._lov3.reflovdetails" value="{{item.value}}" >{{item.caption}}</option> 
      </select>                
      </div>      
      <div class="col-md-1"> 
      {{_obj.merchantID}} 
      </div>
    </div>  

    <div class="form-group" *ngIf="chkWaterBill == true">
      <label class="col-md-2"> Peanlty Days <font class="mandatoryfont">*</font></label>
      <div class="col-md-4">                        
      <input  class="form-control input-sm" type = "number" [ngModelOptions]="{standalone: true}"  [(ngModel)]="_obj.n2" required="true" >                              
      </div>
    </div> 
   
    <div class="form-group">
      <label class="col-md-2" > Charges 1 </label>
      <div class="col-md-4">
      <select [ngModelOptions]="{standalone: true}"  [(ngModel)]="_obj.commRef1" class="form-control input-sm">
        <option *ngFor="let item of ref._lov3.refcharges" value="{{item.value}}" >{{item.caption}}</option> 
      </select>                
      </div> 
    </div>

    <div class="form-group">
      <label class="col-md-2"> Charges 2 </label>
      <div class="col-md-4">
      <select [ngModelOptions]="{standalone: true}"  [(ngModel)]="_obj.commRef2" class="form-control input-sm">
        <option *ngFor="let item of ref._lov3.refcharges" value="{{item.value}}" >{{item.caption}}</option> 
      </select>                
      </div> 
    </div>

    <div class="form-group">
    <label class="col-md-2"> Charges 3 </label>
    <div class="col-md-4">
      <select [ngModelOptions]="{standalone: true}"  [(ngModel)]="_obj.commRef3" class="form-control input-sm">
      <option *ngFor="let item of ref._lov3.refcharges" value="{{item.value}}" >{{item.caption}}</option> 
      </select>                
      </div> 
    </div>

    <div class="form-group">
      <label class="col-md-2"> Charges 4 </label>
      <div class="col-md-4">
      <select [ngModelOptions]="{standalone: true}"  [(ngModel)]="_obj.commRef4" class="form-control input-sm">
        <option *ngFor="let item of ref._lov3.refcharges" value="{{item.value}}" >{{item.caption}}</option> 
      </select>                
      </div> 
    </div>

    <div class="form-group">
      <label class="col-md-2"> Charges 5 </label>
      <div class="col-md-4">
      <select [ngModelOptions]="{standalone: true}"  [(ngModel)]="_obj.commRef5" class="form-control input-sm">
        <option *ngFor="let item of ref._lov3.refcharges" value="{{item.value}}" >{{item.caption}}</option> 
      </select>                
      </div> 
    </div>

    </div>
  </div>
  </div>
  </form>
  </div>
  </div>
</div>
     
  <div id="sessionalert" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <p>{{sessionAlertMsg}}</p>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>

<div [hidden] = "_mflag">
<div class="modal" id="loader"></div>
</div>
    
  `
})
export class MerchantCommMappingSetup{
  // RP Framework 
  subscription: Subscription;
  sub: any;
  _returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
  _obj = this.getDefaultObj();
  _mflag = false;
   _chkloginId = 'false'; chkWaterBill: boolean; _combohide: boolean; _mercbohide: boolean;
  _key = ""; sessionAlertMsg = "";
  _util: ClientUtil = new ClientUtil();

  constructor(private ics: RpIntercomService, private _router: Router, private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences) {
    this.subscription = ics.rpbean$.subscribe(x => { })
    if (!ics.getRole() || ics.getRole() == 0) {
      this._router.navigate(['/login']);
    } else {
      this._mflag = false;
      this.checkSession();
      this.getAllMerchant();
      this.getAllCommRef();
      this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
      this._obj = this.getDefaultObj();
    }
  }
  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let cmd = params['cmd'];
      if (cmd != null && cmd != "" && cmd == "read") {
        let id = params['id'];
        this._key = id;
        this.goReadBySyskey(id);
      }
    });
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  getDefaultObj() {
    jQuery("#mydelete").prop("disabled", true);
    jQuery("#mySave").prop("disabled", false);
    this._combohide = true;
    this._mercbohide = false;
    return { "processingCode": "", "syskey": 0, "recordStatus": 0, "merchantID": "", "kindOfComIssuer": 0, "chkAdvance": false, "t1": "", "commRef1": "-", "commRef2": "-", "commRef3": "-", "commRef4": "-", "commRef5": "-", "code": "", "desc": "", "userID": "", "sessionID": "", "n1": 0, "n2": 0 };

  }

  goReadBySyskey(p) {
    try {
      this._mercbohide = true;
      let url: string = this.ics.cmsurl + 'serviceCMS/getMerchantCommData?syskey=' + p + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
      this.http.doGet(url).subscribe(
        data => {
          if (data.code == '0016') {
            this.sessionAlertMsg = data.desc;
            this.showMessage(data.desc,false);
          }
          else {
            jQuery("#mydelete").prop("disabled", false);
            this._obj = data;
            if (this._obj.t1 != '') {
              this._combohide = false;
              this._obj.chkAdvance = true;
              this.getCodeFromLovdeatils();
            }
            else {
              this._combohide = true;
              this._obj.chkAdvance = false;
            }
            if (this._obj.n2 != 0) {
              this.chkWaterBill = true;
            }
            else {
              this.chkWaterBill = false;
            }
          }
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
        }, () => { });
    } catch (e) {
      alert(e);
    }
  }

  goList() {
    this._router.navigate(['/merchantcommmappinglist']);
  }
  //showMessage() {
  //  jQuery("#sessionalert").modal();
  //  Observable.timer(3000).subscribe(x => {
 //     this.goLogOut();
 //   });
 // }

  goLogOut() {
    jQuery("#sessionalert").modal('hide');
    this._router.navigate(['/login']);
  }

  showMessageAlert(msg) {
    this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
  }


  clearData() {
    this._obj = this.getDefaultObj();
    this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
    this._chkloginId = 'false';
    jQuery("#mydelete").prop("disabled", true);
    jQuery("#mySave").prop("disabled", true);
  }

  goNew() {
    this.clearData();
    jQuery("#mySave").prop("disabled", false);
    jQuery("#mydelete").prop("disabled", true);
  }

  getAllMerchant() {
    this.http.doGet(this.ics.cmsurl + 'serviceAdmLOV/getmerchantidlistdetail?sessionID='+ this.ics._profile.sessionID + '&userID='+ this.ics._profile.userID).subscribe(
      data => {
        this.ref._lov3.ref015 = data.ref015;
        let merchant = [];
        if (this.ref._lov3.ref015 != null && this.ref._lov3.ref015 != undefined) {
          if (!(this.ref._lov3.ref015 instanceof Array)) {
            let m = [];
            m[1] = this.ref._lov3.ref015;
            merchant.push(m[1]);
          }
          for (let j = 0; j < this.ref._lov3.ref015.length; j++) {
            merchant.push(this.ref._lov3.ref015[j]);
          }
        }
        this._obj.merchantID = this.ref._lov3.ref015[0].value;
        this.ref._lov3.ref015 = merchant;
        this._obj.processingCode = this.ref._lov3.ref015[0].processingCode;
      },
      error => {
        if (error._body.type == 'error') {
          alert("Connection Timed Out!");
        }
      }, () => { }
    );
  }


  getAllCommRef() {
    try {
      this._mflag = false;
      this.http.doGet(this.ics.cmsurl + 'serviceAdmLOV/getAllCommRef?sessionID='+ this.ics._profile.sessionID + '&userID='+ this.ics._profile.userID).subscribe(
        data => {
          if (data != null && data != undefined) {
            let Chargescombo = [{ "value": "-", "caption": "-" }];
            for (let i = 0; i < data.refcharges.length; i++) {
              Chargescombo.push({ "value": data.refcharges[i].value, "caption": data.refcharges[i].caption });
            }
            this.ref._lov3.refcharges = Chargescombo;
          }
          else {
            this.ref._lov3.refcharges = [{ "value": "", "caption": "-" }];
          }
          this._mflag = true;
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
          else {

          }
        }, () => { });
    } catch (e) {
      alert(e);
    }
  }

  update(event) {
    if (event.target.checked) {
      this._combohide = false;
      this.getCodeFromLovdeatils();
      this._obj.n1 = 1;
      this._obj.chkAdvance = true;
    }
    else {
      this._combohide = true;
      this._obj.n1 = 0;
      this._obj.chkAdvance = false;
      this._obj.t1 = '';
      this.ref._lov3.reflovdetails = [{ "value": "", "caption": "-" }];
    }
  }

  getCodeFromLovdeatils() {
    try {
      this.http.doGet(this.ics.cmsurl + 'serviceAdmLOV/getCodeFromLovdeatils?merchantId=' + this._obj.merchantID).subscribe(
        data => {
          if (data != null && data != undefined) {
            if (!(data.reflovdetails instanceof Array)) {//Object
              let m = [];
              m[0] = data.reflovdetails;
              this.ref._lov3.reflovdetails = m;
            }
            else {
              this.ref._lov3.reflovdetails = data.reflovdetails;
            }
          }
          else {
            this.ref._lov3.reflovdetails = [{ "value": "", "caption": "-" }];
          }
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
        }, () => { });
    } catch (e) {
      alert(e);
    }
  }

  goSave() {
    if (this._obj.merchantID == '') {
      this.showMessage("Please select Merchant",false);
    }
    else if (this._obj.commRef1 == '' && this._obj.commRef2 == '' && this._obj.commRef3 == '' && this._obj.commRef4 == '' && this._obj.commRef5 == '') {
      this.showMessage("Please select Charges",false);
    }
    else if (this._obj.chkAdvance == true) {
      if (this._obj.t1 == '') {
        this.showMessage("Please select Operator Type",false);
      }
      else {
        this.saveMerchantCommMapping();
      }
    }
    else {
      this.saveMerchantCommMapping();
    }
  }

  saveMerchantCommMapping() {
    try {
      this._mflag = false;
      this._obj.sessionID = this.ics._profile.sessionID;
      this._obj.userID = this.ics._profile.userID;
      let url: string = this.ics.cmsurl + 'serviceCMS/saveMerchantCommRateMapping';
      let json: any = this._obj;
      this.http.doPost(url, json).subscribe(
        data => {
          this._returnResult = data;
          if (this._returnResult.state == 'true') {
            jQuery("#mydelete").prop("disabled", false);
            this.showMessage(data.msgDesc,true);
          } else {
            if (this._returnResult.msgCode == '0016') {
              this.sessionAlertMsg = data.msgDesc;
              this.showMessage(data.msgDesc,false);
            }
            else {
              this.showMessage(data.msgDesc,true);
            }
          }
          this._mflag = true;
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
        }, () => { });
    }

    catch (e) {
      alert(e);
    }
  }

  goDelete() {
    try {
      this._mflag = false;
      this._obj.sessionID = this.ics._profile.sessionID;
      this._obj.userID = this.ics._profile.userID;
      let json: any = this._obj;
      let url: string = this.ics.cmsurl + 'serviceCMS/deleteMerchantCommRateMapping';
      this.http.doPost(url, json).subscribe(
        data => {
          this._returnResult = data;
          if (data.msgCode == '0016') {
            this.sessionAlertMsg = data.msgDesc;
            this.showMessage(data.msgDesc,false);
          }
          else {
            this.showMessageAlert(data.msgDesc);
            if (data.state) {
              this.clearData();
            }
          }
          this._mflag = true;
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
        }, () => { });
    } catch (e) {
      alert(e);
    }

  }

  changeMerchant(event) {
    let options = event.target.options;
    let k = options.selectedIndex;//Get Selected Index
    let value = options[options.selectedIndex].value;//Get Selected Index's Value    

    this._obj.merchantID = value;
    this.getCodeFromLovdeatils();
    for (var i = 0; i < this.ref._lov3.ref015.length; i++) {
      if (this.ref._lov3.ref015[i].value == value) {
        this._obj.merchantID = this.ref._lov3.ref015[i].value;
        this._obj.processingCode = this.ref._lov3.ref015[i].processingCode;

        if (this._obj.processingCode == "090500") {
          this.chkWaterBill = true;
        } else {
          this.chkWaterBill = false;
        }
        break;
      }

    }
  }

  // about session validation
  sessionTimeoutMsg = "";
  _sessionObj = this.getSessionObj();
  getSessionObj() {
    return { "sessionID": "", "userID": "" };
  }

  checkSession() {
    try {
      let url: string = this.ics._apiurl + 'service001/checkSessionTime';

      this._sessionObj.sessionID = this.ics._profile.sessionID;
      this._sessionObj.userID = this.ics._profile.userID;
      let json: any = this._sessionObj;

      this.http.doPost(url, json).subscribe(
        data => {
          if (data != null) {
            if (data.code == "0016") {
              this.showMsgAlert(data.desc);
              this.logout();
            }

            if (data.code == "0014") {
              this.showMsgAlert(data.desc);
            }
          }
        },
        error => {
          if (error._body.type == "error") {
            alert("Connection Timed Out.");
          }
        },
        () => { }
      );
    } catch (e) {
      alert("Invalid URL.");
    }
  }

  showMsgAlert(msg) {
    this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
  }

  logout() {
    this._router.navigate(['/login']);
  }
  showMessage(msg, bool) {
    if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
    if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
    if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "primary", "t3": msg }); }
}
}