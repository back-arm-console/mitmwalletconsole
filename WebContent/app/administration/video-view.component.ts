import { Component, enableProdMode } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { RpHttpService } from '../framework/rp-http.service';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpReferences } from '../framework/rp-references';
import { ClientUtil } from '../util/rp-client.util';
declare var jQuery: any;
enableProdMode();
@Component({
    selector: 'fmr-videoView',
    template: `
    <div class="container col-sm-12 col-xs-12">
    <div class="row clearfix">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
      <form class="form-inline" ngNoForm>
        <legend>Video View</legend>
        <div class="cardview list-height">   
      <div *ngFor="let obj of _obj;let num = index" class="col-md-12">
        <button class="btn btn-sm btn-primary" [disabled]="flagnew" id="new" type="button" (click)="goBack(obj.syskey)">Back</button>
        <h4><a class="uni" (click)="goto(obj.syskey)">{{obj.t1}}</a></h4>
        <div class="uni" style="color:gray;font-style:italic;font-size:11px">{{obj.userName}}</div>
        <div *ngIf="(obj.n10 != 1)" class="form-group">
          <div class="col-md-6">
            <div><!--*ngFor="let img of obj.uploadlist"-->
              <img src="{{setImgUrl(obj.uploadlist[0].name)}}" onError="this.src='./image/image_not_found.png';" alt={{img}} height="240" width="400" />
              
              </div>
          </div>
          <div class="col-md-6">
            <div *ngFor="let img of obj.videoUpload">
              <video id="vid" controls height="240" width="400">
                <canvas id="canvas" width="240" height="400"></canvas>
                <source src="{{videoUrl}}/{{setVideoUrl(img)}}" type="video/mp4" />
              </video>
            </div>
          </div>
        </div>
        <div *ngIf="(obj.n10 == 1)" class="form-group">
          <div *ngFor="let img of obj.uploadlist">
            <img src="{{setImgUrl(obj.uploadlist[0].name)}}" onError="this.src='./image/image_not_found.png';" alt={{img}} height="240" width="400" /><!--src={{img.url}}-->
          </div>
        </div>
        <p class="uni">{{obj.t2}}</p>
        <hr>
      </div>
      </div>
	  </form>
	</div>	  
</div>
</div>
<div [hidden]="_mflag">
  <div id="loader" class="modal"></div>
</div>
  `
})
export class videoViewComponent {
    // RP Framework 
    subscription: Subscription;
    // Application Specific
    profileImageLink: any;
    _alertflag = true;
    _alertmsg = "";
    _totalcount = 1;
    _searchVal = "";
    sub: any;
    _array = [];
    _likearr = [];
    _pager = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 0, "size": 0, "totalcount": 1 };
    _mflag = false;
    count = 0;
    key = 0;
    syskey = 0;
    _time = "";
    _cmtArray = [];
    _replyArray = [];
    _obj = [];
    savevideocommentflag = false;    
    replycommentflag = false;
    videoUrl: any;
    _util: ClientUtil = new ClientUtil();
    _tmpObj = { '_key': 0, '_index': 0 };
    _ansObj = { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": "", "modifiedDate": "", "modifiedTime": "", "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [], "uploadlist": [] };
    _replyObj = { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": "", "modifiedDate": "", "modifiedTime": "", "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [], "videoUpload": [], "uploadlist": [] };
    savecommentflag = false;
    constructor(private ics: RpIntercomService, private _router: Router, private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences) {
        if (!ics.getRole() || ics.getRole() == 0) this._router.navigate(['/login']);
        this._time = this._util.getTodayTime();
        this._ansObj.createdTime = this._time;
        this._ansObj.modifiedTime = this._time;
        this._replyObj.createdTime = this._time;
        this._replyObj.modifiedTime = this._time;
        this.syskey = Number(this.ics._profile.t1);
        this.profileImageLink = this.ics._profileImage1;
        this.videoUrl = this.ics._imageurl;
    }
    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            let cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "new") {
                //  this.goNew();
            } else if (cmd != null && cmd != "" && cmd == "read") {
                let id = params['id'];
                this.goGet(id);
            }
        });
    }
    temp = "";
    goGet(p) {
        this.http.doGet(this.ics.cmsurl + 'serviceVideoAdm/viewAdminVidoe?id=' + p + '&userid=' + this.ics._profile.userID).subscribe(
            response => {
                console.log("my like=" + JSON.stringify(p) + "hello=" + JSON.stringify(response));
                if (response.state) {
                    this._obj = response.data;
                    //this.key = response.data[0].n7;
                    for (let i = 0; i < this._obj.length; i++) {
                        if (this._obj[i].videoUpload > 0) {
                            this.temp = this._obj[i].videoUpload;
                        }
                        else {
                            this.temp = this._obj[i].upload;
                        }
                    }
                }
                this._mflag = true;
            },
            error => { },
            () => { }
        );
    }
    /* search(p) {
        this._mflag = false;
        if (p.end == 0) { p.end = this.ics._profile.n1; }
        if (p.size == 0) { p.size = this.ics._profile.n1; }
        let url: string = this.ics.cmsurl + 'serviceVideoAdm/searchvideoAdminvideo?searchVal=' + this._searchVal;
        let json: any = p;
        this.http.doPost(url, json).subscribe(
            response => {
                if (response != null && response != undefined && response.state) {
                    this._totalcount = response.totalCount;
                    this._array = response.data;

                }
                else {
                    this._array = [];
                    this._totalcount = 1;
                    this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": "Data not found!" });
                }
                this._mflag = true;
            },
            error => { },
            () => { }
        );
    } */
    /* searchVal() {
        this._pager = { "current": 1, "prev": 1, "last": 1, "next": 1, "start": 1, "end": 0, "size": 0, "totalcount": 1 };
        this.search(this._pager);
    } */
    goto(p) {
        this._router.navigate(['/video', 'read', p]);
    }
    goBack(p) {
        this._router.navigate(['/video', 'read', p]);
    }

    goLike(key, type) {
        jQuery("#likeModal").modal();
        this.http.doGet(this.ics.cmsurl + 'serviceEduAdm/searchLike?type=' + type + '&key=' + key).subscribe(
            response => {
                if (response.state && response.data.length > 0) {
                    this._likearr = [];

                    for (let i = 0; i < response.data.length; i++) {
                        this._likearr.push({
                            name: response.data[i].userName,
                            img: response.data[i].photo
                        });
                    }
                    this._mflag = true;
                } else {
                    this._likearr = [];
                }
            },
            error => { },
            () => { }
        );
    }
    /* changedPager(event) {
        let k = event.flag;
        this._pager = event.obj;
        if (k) { this.search(this._pager); }
    } */
    goClickLike(key, index) {
        this.http.doGet(this.ics.cmsurl + 'serviceArticleAdm/clickLikeArticle?key=' + key + '&userSK=' + this.key).subscribe(
            data => {
                if (data.state) {
                    this.goLikeCount(key, index);
                    this._mflag = true;
                }
            },
            error => { },
            () => { }
        );
    }

    ClickLike(key, index) {
        if (this.count == 0 && key == this._obj[index].syskey) {
            this._obj[index].n2 = this._obj[index].n2 + 1;
            this.count = this.count + 1;
            this.goClickLike(key, index);

        } else if (this.count == 1 && key == this._obj[index].syskey) {
            this._obj[index].n2 = this._obj[index].n2 - 1;
            this.count = 0;
            this.goClickLike(key, index);
        }
    }
    goClickUnlike(key, index) {
        this.http.doGet(this.ics.cmsurl + 'serviceArticleAdm/clickUnlikeArticle?key=' + key).subscribe(
            data => {
                if (data.state) console.log("Unlike...");
            },
            error => { },
            () => { }
        );
    }
    goLikeCount(key, index) {
        this.http.doGet(this.ics.cmsurl + 'serviceQuestionAdm/searchLikeCount?key=' + key).subscribe(
            response => {
                if (response.state) {
                    this._obj = response.data;
                    for (let i = 0; i < this._obj.length; i++) {
                        if (this._obj[i].videoUpload > 0) {
                            this._obj[i].videoUpload = this.temp;
                        }
                        else {
                            this._obj[i].videoUpload = this.temp;
                        }
                    }
                }
                this._mflag = true;
            },
            error => { },
            () => { }
        );
    }
    goComment(key, index) {
        console.log(JSON.stringify(key) + " edu get ");
        jQuery("#commentModal").modal();
        this._tmpObj._key = key;
        this._tmpObj._index = index;
        this._cmtArray = [];
        //this.http.doGet(this.ics.cmsurl + 'serviceEduAdm/getComments?id=' + key).subscribe(
        this.http.doGet(this.ics.cmsurl + 'serviceQuestion/getCommentmobile?id=' + key + '&userSK=' + Number(this.ics._profile.t1)).subscribe(
            response => {
                if (response.state) this._cmtArray = response.data;
                this._mflag = true;
            },
            error => { },
            () => { }
        );
    }
    saveComment(cmtObj) {
        this.savecommentflag = true;
        let valid = false;
        if (cmtObj.t2 != undefined && cmtObj.t2 != null && cmtObj.t2 != '') {
            cmtObj.t2 = cmtObj.t2.trim();

            if (cmtObj.t2.length > 0) {
                valid = true;
            }
        }
        this._mflag = false;
        cmtObj.n1 = this._tmpObj._key;
        cmtObj.n5 = Number(this.ics._profile.t1);
        // cmtObj.t12 = '';
        //cmtObj.t13 = '';
        //cmtObj.t3 = 'news';
        cmtObj.t1 = 'answer';
        console.log("UI data : " + JSON.stringify(cmtObj))
        if (valid) {
            //let url: string = this.ics.cmsurl + 'serviceEduAdm/saveComment';
            let url: string = this.ics.cmsurl + 'serviceQuestion/saveAnswer';
            let json: any = cmtObj;
            this.http.doPost(url, json).subscribe(
                response => {
                    this._cmtArray = [];
                    if (response.state) this._cmtArray = response.data;
                    this._ansObj.t2 = "";
                    this._mflag = true;
                    this.savecommentflag = false;
                    this._obj[0].n3 = this._cmtArray[0].n4;
                    console.log(JSON.stringify(this._cmtArray));
                },
                error => {
                    this.showMsgAlert("Can't Saved This Record!", undefined);
                    this._mflag = true;
                    this.savecommentflag = false;
                },
                () => { }
            );
        } else {
            this.showMsgAlert("Please write message.", false);
            this._mflag = true;
            this.savecommentflag = false;
        }
    }

    /* deletevideoreplycomment = false;
    deleteVideoReplyComment(repObj, index) {
        this.deletevideoreplycomment = true;
        let temp = repObj.syskey;
        this._mflag = false;
        this.http.doGet(this.ics.cmsurl + 'serviceQuestion/deleteReplyComment?syskey=' + temp).subscribe(
            response => {
                if (response.state){
                    if(this._cmtArray[index].n11 > 0)                       
                        this._cmtArray[index].n10 -= 1;
                }
                this._mflag = true;
                for (let i = 0; i < this._replyArray.length; i++) {
                    if (temp == this._replyArray[i].syskey) {
                        this._replyArray.splice(i, 1);
                        i--;
                        this.deletevideoreplycomment = false;
                    }
                }
            },
            error => { },
            () => { }
        );
    } */

    showMsgAlert(msg, bool) {
        let type = "";

        if (bool == true) { type = "success"; }
        if (bool == false) { type = "warning"; }
        if (bool == undefined) { type = "danger"; }

        this._alertmsg = msg;
        this._alertflag = false;
        let _snack_style = 'msg-info';
        if (type == "success") _snack_style = 'msg-success';
        else if (type == "warning") _snack_style = 'msg-warning';
        else if (type == "danger") _snack_style = 'msg-danger';
        else if (type == "information") _snack_style = 'msg-info';
        document.getElementById("snackbar1").innerHTML = this._alertmsg;
        let snackbar1 = document.getElementById("snackbar1");
        snackbar1.className = "show " + _snack_style;
        setTimeout(function () { snackbar1.className = snackbar1.className.replace("show", ""); }, 3000);
    }

    replyComment(key, num) {
        this._tmpObj._index = num;
        for (var i = 0; i < this._cmtArray.length; i++) {
            if (num == i) {
                this._cmtArray[i].t13 = 'true';
            }
            else this._cmtArray[i].t13 = 'false';
        }
        //this._cmtArray[num].t13 = 'true';
        this._replyArray = [];
        //this.http.doGet(this.ics.cmsurl + 'serviceEduAdm/getComments?id=' + key).subscribe(
        this.http.doGet(this.ics.cmsurl + 'serviceQuestion/getCommentReplymobile?id=' + key + '&userSK=' + Number(this.ics._profile.t1)).subscribe(

            response => {
                if (response.state) this._replyArray = response.data;
                this._mflag = true;
            },
            error => { },
            () => { }
        );
    }

    editComment(obj, num) {
        this._cmtArray[num].t12 = 'true';
    }
    /* editVideoReplyComment(obj, num) {
        this._replyArray[num].t12 = 'true';
    } */

    /* editReply(replyObj) {
        let valid = false;
        if (replyObj.t2 != undefined && replyObj.t2 != null && replyObj.t2 != '') {
            replyObj.t2 = replyObj.t2.trim();
            if (replyObj.t2.length > 0) {
                valid = true;
            }
        }
        this._mflag = false;
        replyObj.n5 = Number(this.ics._profile.t1);
        replyObj.t1 = 'reply';

        if (valid) {
            let url: string = this.ics.cmsurl + 'serviceQuestion/saveCommentReply';
            let json: any = replyObj;
            this.http.doPost(url, json).subscribe(
                response => {
                    this._replyArray = [];
                    if (response.state) this._replyArray = response.data;
                    this._replyObj.t2 = "";
                    this._mflag = true;
                },
                error => {
                    this.showMsgAlert("Can't Saved This Record!", undefined);
                },
                () => { }
            );
        } else {
            this.showMsgAlert("Please write message.", false);
            this._mflag = true;
        }
    } */

    deleteComment(obj, num) {
        this._mflag = false;
        // let url: string = this.ics.cmsurl + 'serviceEduAdm/deleteComment';
        let json: any = obj;
        //this.http.doPost(url, json).subscribe(
            this.http.doGet(this.ics.cmsurl + 'serviceQuestion/deleteComment?syskey=' + obj.syskey).subscribe(
            response => {
                // this._cmtArray = [];
                // if (response.state) this._cmtArray = response.data;
                // this._mflag = true;

                this._cmtArray = [];
                if (response.state)
                    this._cmtArray = response.data;
                this._ansObj.t2 = "";
                this._mflag = true;
                if (this._cmtArray.length > 0)
                    this._obj[0].n3 = this._cmtArray[0].n4;
                else this._obj[0].n3 = 0;
            },
            error => { },
            () => { }
        );
    }
    setVideoUrl(str) {
        console.log(""+ JSON.stringify(this.ics._imgurl + 'upload/video/' + str))
        return 'upload/video/' + str;
        }
        setImgUrl(str) {
        console.log(""+ JSON.stringify(this.ics._imgurl + 'upload/smallImage/videoImage/' + str))
        return this.ics._imgurl + 'upload/smallImage/videoImage/' + str;
        }
    showMessage(msg, bool) {
        if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
        if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
        if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
    }
    /* saveReply(skey, index) {
        this.replycommentflag = true;
        let valid = false;
        if (this._replyObj.t2 != undefined && this._replyObj.t2 != null && this._replyObj.t2 != '') {
            this._replyObj.t2 = this._replyObj.t2.trim();

            if (this._replyObj.t2.length > 0) {
                valid = true;
            }
        }
        this._mflag = false;
        this._replyObj.n1 = skey;
        //this._replyObj.n3 = -1;
        this._replyObj.n5 = Number(this.ics._profile.t1);
        this._replyObj.t1 = 'reply';

        if (valid) {
            // let url: string = this.ics.cmsurl + 'serviceEduAdm/saveComment';
            let url: string = this.ics.cmsurl + 'serviceQuestion/saveCommentReply';

            let json: any = this._replyObj;
            this.http.doPost(url, json).subscribe(
                response => {
                    this._replyArray = [];
                    if (response.state) this._replyArray = response.data;
                    this._cmtArray[this._tmpObj._index].n3 = this._cmtArray[this._tmpObj._index].n3 + 1;
                    this._replyObj.t2 = "";
                    this._mflag = true;
                    this.replycommentflag = false;
                    this._cmtArray[index].n10 += 1;
                },
                error => {
                    this.showMsgAlert("Can't Saved This Record!", undefined);
                    this.replycommentflag = false;
                },
                () => { }
            );
        } else {
            this.showMsgAlert("Please write message.", false);
            this._mflag = true;
            this.replycommentflag = false;
        }
    } */
}