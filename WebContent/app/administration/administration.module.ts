import { NgModule, CUSTOM_ELEMENTS_SCHEMA }       from '@angular/core';
import { FormsModule }    from '@angular/forms';
import { CommonModule }   from '@angular/common';
import { RpInputModule } from '../framework/rp-input.module';
import { MyDatePickerModule } from 'mydatepicker';
import {TabsModule} from "ngx-tabs";
import {QRCodeComponent} from 'ng2-qrcode';
import { RpHttpService } from '../framework/rp-http.service';
import { AdminPagerModule } from '../util/adminpager.module';
import { PagerModule } from '../util/pager.module';
import { VideoComponent }       from './video.component';
import { VideoList }       from './video-list.component';
import { AdministratorRouting }     from './administration.routing';
import {ContentMenu} from './contentmenu.component';
import {ContentMenuListComponent} from './contentmenu-list.component';
import { EduViewComponent } from './edu-view.component';
import { videoViewComponent }       from './video-view.component';
import { AdvancedSearchModule } from '../util/advancedsearch.module';
import {MultiselectModule} from '../util/multiselect.module';
import { SMSSettingComponent } from './smssettingsetup.component';
import { SMSSettingSetupListComponent } from './SMSSettingSetupList.component';
import { MerchantCommMappingSetup } from './merchantcommmappingsetup.component';
import { MerchantCommMappingSetupList } from './merchantcommmappingsetup-list.component'; 
import { FlexCommRateSetup } from './flexcommratesetup.component';
import { FlexCommRateSetupList } from './flexcommratesetup-list.component'; 
import { MerchantSetup } from './merchant-setup.component';
import { MerchantList } from './merchant-list.component'; 

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RpInputModule,
    AdministratorRouting,
    AdminPagerModule,
    PagerModule,
    MyDatePickerModule,
    TabsModule,
    MultiselectModule,
    AdvancedSearchModule
  ],
  exports : [],
  declarations: [
    QRCodeComponent,    
    VideoComponent,
    VideoList,
    ContentMenu,
    ContentMenuListComponent,
    EduViewComponent,
    videoViewComponent, 
    SMSSettingComponent,
    SMSSettingSetupListComponent,
    MerchantCommMappingSetup,
    MerchantCommMappingSetupList,
    FlexCommRateSetup,
    FlexCommRateSetupList,
    MerchantSetup,
    MerchantList,
  ],
  providers: [
    RpHttpService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class administrationModule {}