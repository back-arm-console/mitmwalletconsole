import { Component, enableProdMode } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { RpHttpService } from '../framework/rp-http.service';
// RP Framework
import { RpIntercomService } from '../framework/rp-intercom.service';
import { ClientUtil } from '../util/rp-client.util';
declare var jQuery: any;
enableProdMode();
@Component({
    selector: 'flexcommratesetup-list',
    template: `
    <div class="container-fluid">
    <form class="form-horizontal"> 
    <!-- Form Name -->
    <legend>Commission List</legend>
		<div class="cardview list-height">
			<div class="row col-md-12">
				<div class="row col-md-3">
					<div class="input-group">
						<span class="input-group-btn">
							<button class="btn btn-sm btn-primary" type="button" (click)="goNew();">New</button>
						</span>
						<input id="textinput" name="textinput" type="text" placeholder="Search" [(ngModel)]="_flexobj.searchText" maxlength="50" class="form-control input-sm" (keyup)="searchKeyup($event)" [ngModelOptions]="{standalone: true}">
						<span class="input-group-btn">
							<button class="btn btn-sm btn-primary" type="button" (click)="Searching()" >
							<span class="glyphicon glyphicon-search"></span>Search
						</button>
						</span> 
					</div>
				</div>
				<div class="pagerright">				
					<adminpager rpPageSizeMax="100" [(rpModel)]="_flexobj.totalCount" (rpChanged)="changedPager($event)" ></adminpager> 
				</div>
            </div>
            <div class="row col-md-12" style="overflow-x:auto">
			  <table class="table table-striped table-condensed table-hover tblborder">
				<thead>
				 <tr>
					<th class="center">Commission Reference No.</th>
					<th>Description</th> 
				</tr>
			  </thead>
			  <tbody>
				<tr *ngFor="let obj of _flexobj.commdetailArr">
					<td class="col-md-3 center"><a (click)="goto(obj.commRef)">{{obj.commRef}}</a></td>
					<td>{{obj.description}}</td>
				</tr>
			  </tbody>
              </table>
              </div>
		</div>
	</form>
  </div> 
  
    <div [hidden] = "_mflag">
        <div class="modal" id="loader"></div>
    </div>
   `
})

export class FlexCommRateSetupList {
    // RP Framework 
    subscription: Subscription;
    // Application Specific

    _col_list = [{ "col_no": "1", "flag": false }, { "col_no": "2", "flag": false }]
    _sorting = { _sort_type: "asc", _sort_col: "1" };
    _mflag = true;
    _util: ClientUtil = new ClientUtil();
    sessionAlertMsg = ""; _recordhide: boolean; _msghide: boolean;    
    _flexobj = {
        "commdetailArr": [{ "syskey": 0, "hkey": "", "zone": "", "commType": "", "fromAmt": "", "toAmt": "", "amount": "", "minAmt": "", "maxAmt": "", "gLorAC": "" }],
        "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0};
    _pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 0 };

    constructor(private ics: RpIntercomService, private _router: Router, private http: RpHttpService) {
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(x => { })
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        } else {
            //jQuery("#loader").modal('hide');
            this._mflag = false;
            this.search();
        }
    }
    // list sorting part
    changeDefault() {
        for (let i = 0; i < this._col_list.length; i++) {
            this._col_list[i].flag = false;
        }
    }
    addSort(e) {
        for (let i = 0; i < this._col_list.length; i++) {
            if (this._col_list[i].col_no == e) {
                let _tmp_flag = this._col_list[i].flag;
                this.changeDefault();
                this._col_list[i].flag = !_tmp_flag;
                this._sorting._sort_type = !_tmp_flag ? "desc" : "asc";
                this._sorting._sort_col = e;
                break;
            }
        }
    }

    changedPager(event) {

        if (this._flexobj.totalCount != 0) {
            this._pgobj = event;
            let current = this._flexobj.currentPage;
            let size = this._flexobj.pageSize;
            this._flexobj.currentPage = this._pgobj.current;
            this._flexobj.pageSize = this._pgobj.size;
            if (this._pgobj.current != current || this._pgobj.size != size) {
                this.search();
            }
        }
    }

    search() {
        try {
            this._mflag = false;
            this.http.doGet(this.ics.cmsurl + 'serviceCMS/getFlexCommRateList?searchVal=' + this._flexobj.searchText + '&pagesize=' + this._flexobj.pageSize + '&currentpage=' + this._flexobj.currentPage + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(
                response => {
                    if (response.msgCode == "0016") {
                        this.sessionAlertMsg = response.msgDesc;
                        this.showMessage(response.msgDesc,false);
                    } else {
                        if (response.totalCount == 0) {
                            this._flexobj = response;
                            this.showMessage("Data not found!",false);
                        }
                        if (response.commdetailArr != null) {
                            if (!(response.commdetailArr instanceof Array)) {
                                let m = [];
                                m[0] = response.commdetailArr;
                                this._flexobj.commdetailArr = m;
                                this._flexobj.totalCount = response.totalCount;
                            }
                            else {
                                this._flexobj = response;
                            }
                        } 
                    }
                    this._mflag = true;
                },
                error => {
                    if (error._body.type == 'error') {
                        alert("Connection Timed Out!");
                    }
                }, () => { });
        } catch (e) {
            alert(e);
        }

    }
    searchKeyup(e: any) {
        if (e.which == 13) { // check enter key
            this._flexobj.currentPage = 1;
            this._msghide = true;
            this._recordhide = false;
            this.search();
        }
    }

    Searching() {
        this._flexobj.currentPage = 1;
        this._msghide = true;
        this._recordhide = false;
        this.search();
    }

    goto(p) {
        this._router.navigate(['/Flex Comm Setup', 'read', p]);
    }

    //showMessage() {
    //    jQuery("#sessionalert").modal();
    //   Observable.timer(3000).subscribe(x => {
    //        this.goLogOut();
    //    });
    //}

    goNew() {
        this._router.navigate(['/Flex Comm Setup', 'new']);
    }

    goLogOut() {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    }
    showMessage(msg, bool) {
        if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
        if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
        if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "primary", "t3": msg }); }
    }
}