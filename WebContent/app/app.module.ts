import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { MyDatePickerModule } from 'mydatepicker';
import { RpRootComponent } from './rp-root.component';
import { routing } from './app.routing';
//import { SystemModule } from './system/rp-system.module';
import { RpInputModule } from './framework/rp-input.module';
import { PagerModule } from './util/pager.module';
import { AdminPagerModule } from './util/adminpager.module';
import { AdvancedSearchModule } from './util/advancedsearch.module';
import { RpLoginComponent } from './rp-login.component';
import { RpHttpService } from './framework/rp-http.service';
import { RpIntercomService } from './framework/rp-intercom.service';
import { RpMenuComponent } from './framework/rp-menu.component';
import { RpInputComponent } from './framework/rp-input.component';
import { RpReferences } from './framework/rp-references';
import {Pager} from './util/pager.component';
import {AdminPager} from './util/adminpager.component';
import {ClientUtil} from './util/rp-client.util';
import {TabsModule} from "ngx-tabs";

import { Frm000Module } from './frm000/frm000.module';
import { administrationModule } from './administration/administration.module';
import { settingsModule } from './settings/settings.module';
import { SystemModule } from './system/system.module';
import { reportModule } from './report/report.module';
import { GeoLocationService } from './framework/GeoLocationService';
import { CookieService } from 'angular2-cookie/core';
import { walletModule } from './wallet/wallet.module';
import {dashBoardModule} from './dashboard/dashboard.module';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,    
    settingsModule,
    RpInputModule,
    PagerModule,
    AdminPagerModule,
    AdvancedSearchModule,
    MyDatePickerModule,
    TabsModule,
    Frm000Module,
    administrationModule,
    SystemModule,
    reportModule,
    walletModule,
    dashBoardModule
    
  ],
  declarations: [
    RpRootComponent,
    RpLoginComponent,
    RpMenuComponent
  ],
  providers: [
    RpHttpService,
    RpIntercomService,
    RpReferences,
    ClientUtil,
    GeoLocationService,
    CookieService,
    { provide: APP_BASE_HREF, useValue : '/' },
    { provide: LocationStrategy, useClass: HashLocationStrategy }
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ],
  bootstrap: [RpRootComponent]
})
export class AppModule {
}
