import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { enableProdMode } from '@angular/core';
import { RpHttpService } from './framework/rp-http.service';
import { RpIntercomService } from './framework/rp-intercom.service';
import { RpReferences } from './framework/rp-references';
import { ClientUtil } from './util/rp-client.util';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { RpBean } from './framework/rp-bean';
import { HostListener } from "@angular/core";
declare var jQuery: any;
enableProdMode();

@Component({
  selector: 'rp-root',
  template: `
  <div (keyup)="docChanges('keyup')" (keydown)="docChanges('keydown')" (click)="docChanges('click')" (DOMMouseScroll)="docChanges('DOMMouseScroll')"
  (mousewheel)="docChanges('mousewheel')" (mousedown)="docChanges('mousedown')" (touchstart)="docChanges('touchstart')" (touchmove)="docChanges('touchmove')"
  (scroll)="docChanges('scroll')" (focus)="docChanges('focus')">

    <rp-menu *ngIf="showmenu"></rp-menu>
    <div class="container col-md-12">
      <div id="snackbar" [hidden]="_alertflag">>{{_alertmsg}}</div>
      <!--<div id="alert" class={{_alerttype}} [hidden]="_alertflag">
        {{_alertmsg}}
      </div>-->
    </div>
    <router-outlet></router-outlet>
    <div id="rootpopup" class="modal fade" role="dialog">
      <div id="rootpopupsize" class="modal-dialog modal-lg">  
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 id="rootpopuptitle" class="modal-title">RP Report ***</h4>
          </div>
          <div id="rootpopupbody" class="modal-body">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn input-sm btn-primary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <div id="timeoutalert" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <p>Your session has expired</p>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>
  `,
  providers: [RpIntercomService, RpReferences, RpHttpService]
})
export class RpRootComponent {
  showmenu: boolean;
  _alertflag = true;
  _alertmsg = "";
  _alerttype = "";
  _util: ClientUtil = new ClientUtil();
  _comboobj = { "value": "", "caption": "" };
  array = [{ "value": "", "caption": "" }];
  constructor(private ics: RpIntercomService, private _router: Router, private http: RpHttpService, private ref: RpReferences, private title: Title) {
    this.showmenu = false;
    ics.rpbean$.subscribe(x => {
      this.showmenu = ics.isMenuBar();
      if (x.t1 !== null && x.t1 == "rp-popup") {
        jQuery("#rootpopupsize").attr('class', 'modal-dialog modal-lg');
        jQuery("#rootpopuptitle").text(x.t2);
        jQuery("#rootpopupbody").load(x.t3);
        jQuery("#rootpopup").modal();
      } else if (x.t1 !== null && x.t1 == "rp-wait") {
        jQuery("#rootpopupsize").attr('class', 'modal-dialog modal-sm');
        jQuery("#rootpopuptitle").text("Please Wait");
        jQuery("#rootpopupbody").text(x.t2);
        jQuery("#rootpopup").modal();
      } else if (x.t1 !== null && x.t1 == "rp-error") {
        jQuery("#rootpopupsize").attr('class', 'modal-dialog modal-sm');
        jQuery("#rootpopuptitle").text("System Exception");
        jQuery("#rootpopupbody").text(x.t2);
        jQuery("#rootpopup").modal();
      } else if (x.t1 !== null && x.t1 == "rp-msg") {
        jQuery("#rootpopupsize").attr('class', 'modal-dialog modal-sm');
        jQuery("#rootpopuptitle").text(x.t2);
        jQuery("#rootpopupbody").text(x.t3);
        jQuery("#rootpopup").modal();
      } else if (x.t1 !== null && x.t1 == "rp-msg-off") {
        jQuery("#rootpopuptitle").text("");
        jQuery("#rootpopupbody").text("");
        jQuery("#rootpopup").modal('hide');
      } /* else if (x.t1 !== null && x.t1 == "rp-alert") {
        this._alerttype = "alert alert-" + x.t2 + " fade in";
        this._alertmsg = x.t3;
        this._alertflag = false;
        setTimeout(() => this._alertflag = true, 3000);
      } */else if (x.t1 !== null && x.t1 == "rp-alert") {
        this._alertmsg = x.t3;
        this._alertflag = false;
        let _snack_style = 'msg-info';
        if (x.t2 == "success") _snack_style = 'msg-success';
        else if (x.t2 == "warning") _snack_style = 'msg-warning';
        else if (x.t2 == "danger") _snack_style = 'msg-danger';
        else if (x.t2 == "information") _snack_style = 'msg-info';
        document.getElementById("snackbar").innerHTML = this._alertmsg;
        let snackbar = document.getElementById("snackbar");
        snackbar.className = "show " + _snack_style;
        setTimeout(function () { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
      }
    });
    this.init();
  }

  init() {
    this.http.doGet('json/config.json?random=' + Math.random()).subscribe(
      data => {
        this.ics._title = data.title;
        this.ics._app = data.app;
        this.ics._appname = data.appname;
        this.title.setTitle(this.ics._title);
        this.ics._apiurl = data.apiurl;
        this.ics._rpturl = data.rpturl;
        this.ics._successPageurl = data.successpageurl;
        this.ics._imglogo = data.imgurl;
        this.ics._imgurl = data.imageurl;
        this.ics._welcometext = data.welcometext;
        this.ics._signintext = data.signintext;
        this.ics._appserviceurl = data.appserviceurl;
        this.ics._sessiontime = data.sessiontime;
        this.ics._profile.userName = data.username;
        this.ics.chaturl = data.chaturl;
        this.ics.chatimageurl = data.chatimageurl;
        this.ics.ticketurl = data.ticketurl;
        this.ics.walleturl = data.walleturl;
        this.ics.cmsurl = data.cmsurl;
        this.ics.icbsrpturl=data.icbsrpturl;
        this.ics._profileImage1 = data.profileImage1;//atn
        this.ics._profileImage2 = data.profileImage2;//atn
        this.ics._imglink = data.imglink;//atn
        this.ics._imageurl = data.imageurl;//atn
      },

      () => { }
    );
    this.http.doGet('json/lov3.json?random=' + Math.random()).subscribe(
      data => {
        this.ref._lov3 = data;
        // this.getmainlist();
      },

      () => { }
    );
  }
  //get lov3 refs for item combo...

  @HostListener('window:unload', ['$event'])
  unloadHandler(event) {
    /*  window.onbeforeunload = function (e) {
       return "Dude, are you sure you want to refresh? Think of the kittens!";} */
    let url: string = this.ics._apiurl + 'service001/signout?sessionID=' + this.ics._profile.sessionID;
    let json: any = this.ics._profile.userID;
    this.http.doPost(url, json).subscribe();
  }

  @HostListener('window:popstate', ['$event'])
  onPopState(event) {
    if (this._router.url == '/login') {
      let url: string = this.ics._apiurl + 'service001/signout?sessionID=' + this.ics._profile.sessionID;
      let json: any = this.ics._profile.userID;
      this.http.doPost(url, json).subscribe();
    }
  }
  docChanges(_doc) {
    if (this.ics._profile.role > 0) {
      let dt = new Date();
      let _time: number = (dt.getHours() * 3600) + (dt.getMinutes() * 60) + dt.getSeconds();
      this.ics._activeTimeout = _time;
      this.ics.sendBean(new RpBean());

    }
  }

  ngOnInit() {
    setInterval(() => this.chkActive(), 10000);
  }

  chkActive() {
    let _activeTime = this.ics._activeTimeout;
    if (this.ics._profile.role > 0 && _activeTime != 0) {
      let dt = new Date();
      let _time: number = (dt.getHours() * 3600) + (dt.getMinutes() * 60) + dt.getSeconds();
      if ((_time - _activeTime) > (parseInt(this.ics._sessiontime) * 60)) {
        jQuery("#timeoutalert").modal();
        Observable.timer(3000).subscribe(x => {
          let url: string = this.ics._apiurl + 'service001/signout?sessionID=' + this.ics._profile.sessionID;
          let json: any = this.ics._profile.userID;
          let _returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "" };
          this.http.doPost(url, json).subscribe(
            data => {
              _returnResult = data;
              if (_returnResult.state) {
                jQuery("#timeoutalert").modal('hide');
                this.ics._profile.role = 0;
                this.ics._activeTimeout = 0;
                this.ics.sendBean(new RpBean());
                this._router.navigate(['/login']);
              }
            },
            error => alert(error),
            () => { }
          );
        });

      }
    }
  }

  getmainlist() {
    this.http.doGet(this.ics._apiurl + 'service001/getMainList').subscribe(
      response => {
        if (response != null && response != undefined) {
          this.ref._lov3.mainmenu = response.data;
        }
        else {
          this.ref._lov3.mainmenu = this.array;
        }
      },

      error => //alert(error),

        () => { }
    );
  }
}
