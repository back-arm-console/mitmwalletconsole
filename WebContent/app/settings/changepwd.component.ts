import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { enableProdMode } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpInputComponent } from '../framework/rp-input.component';
import { RpHttpService } from '../framework/rp-http.service';
import { RpBean } from '../framework/rp-bean';
import { RpReferences } from '../framework/rp-references';
import { ClientUtil } from '../util/rp-client.util';
import { Observable } from 'rxjs/Rx';
declare var jQuery: any;
@Component({
    selector: 'pwd',
    template: ` 
    <div class="container-fluid">
    <div class="row clearfix">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
	<form class="form-horizontal" (ngSubmit)="goChange()">
		<legend>Change Password</legend>
		<div class="cardview list-height">
			<div class="row col-md-12">
				<button type="submit" class="btn btn-sm btn-primary" *ngIf="_chkstr == 'false'" > Change </button>
				<button type="submit" class="btn btn-sm btn-primary" *ngIf="_chkstr == 'true'" disabled> Change </button> 
				<button type="button" class="btn btn-sm btn-primary" (click)="goCancel()">Cancel</button> 
            </div> 

            <div class="row col-md-12">&nbsp;</div>
                <div class="form-group">
			<div [hidden]="messagehide"  class="col-md-8" style="margin-bottom: 10px;">
				<span  class={{msgclass}} >{{resultMsg}}</span>
			</div>
			<div style="color: #3b5998;" class="col-md-12" style="margin-bottom: 10px;">
				<span>* Require Minimum Length {{_pobj.pswminlength}}, Maximum Length {{_pobj.pswmaxlength}}</span><span *ngIf="_pobj.spchar != 0">, Special Character {{_pobj.spchar}}</span><span *ngIf="_pobj.upchar != 0">, Uppercase Character {{_pobj.upchar}}</span><span *ngIf="_pobj.lowerchar != 0">, Lowercase Character {{_pobj.lowerchar}}</span><span *ngIf="_pobj.pswno != 0">, Number {{_pobj.pswno}}</span>.
			</div>  
			
            <div class="col-md-6">
                <div class="form-group">
                    <label for="inputPassword" class="col-md-4">Current Password <font class ="mandatoryfont">*</font></label>
                    <div class="col-md-8">
                        <input type="password" id="inputPassword" class="form-control input-sm" placeholder="Password" required [(ngModel)]="_pwdobj.password" [ngModelOptions]="{standalone: true}" maxlength="20">
                    </div>
                </div> 

                <div class="form-group">
                    <label for="newPassword" class="col-md-4">New Password <font class ="mandatoryfont">*</font></label>
                    <div class="col-md-8">
                        <input type="password" id="newPassword" class="form-control input-sm" placeholder="New Password" required [(ngModel)]="_pwdobj.newpassword" [ngModelOptions]="{standalone: true}"  maxlength="20">
                    </div>
                </div>

                <div class="form-group">
                    <label for="newconfirmPassword" class="col-md-4">Confirm Password <font class ="mandatoryfont">*</font></label>
                    <div class="col-md-8">
                        <input type="password" id="newconfirmPassword" class="form-control input-sm" placeholder="Confirm Password" required [(ngModel)]="_pwdobj.confirmnewpassword" [ngModelOptions]="{standalone: true}" maxlength="20">
                    </div>
                </div>
            </div>
        </div>
        </div>
    </form>
</div>
</div>
</div>

    <div id="alertmodal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <p>{{sessionAlertMsg}}</p>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <div [hidden] = "_mflag">
        <div  id="loader" class="modal" ></div>
    </div>
    `
})

export class pwd {

    _pwdobj = { "password": "", "newpassword": "", "confirmnewpassword": "", "userid": "", "sessionID": "" }
    _resultobj = { "msgCode": "", "msgDesc": "", "keyst": "", state: false };
    _passPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[~`!@#$%^&()_+/':;?,.<>[]|\{}]).{6,20}$/;
    _result: String;
    resultMsg: String;
    _chkstr: String;
    msgclass: String;
    sessionAlertMsg = "";
    subscription: Subscription;
    messagehide: boolean;
    _mflag = false;
    _pobj = this.getDefaultObj();
    constructor(private ics: RpIntercomService, private _util: ClientUtil, private _router: Router, private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences) {
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(x => { })
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }else {
            this._mflag = true;
            this.checkSession();
            this.messagehide = true;
            this.readPswPolicy();
            this._chkstr = "false";
        }
    }

    getDefaultObj() {
        return { "pswminlength": 0, "pswmaxlength": 0, "spchar": 0, "upchar": 0, "lowerchar": 0, "pswno": 0, "msgCode": "", "msgDesc": "", "sessionID": "", "userID": "" };
      }

    readPswPolicy() {
        this._mflag = false;
        let url:string = this.ics._apiurl + 'service001/readPswPolicy?sessionID='+this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID+ '&formID=changepwd';;
        this.http.doGet(url).subscribe(
        data => { 
             if(data.msgCode == '0016')
             {
                 this.sessionAlertMsg = data.msgDesc;
                 this.showMsg(data.msgDesc,false);
             } 
             else{
                this._pobj = data; 
             }
             this._mflag = true;         
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Error!");
          }
        });
      }

    goChange() {
        this._mflag = false;

        if (this._pwdobj.newpassword == this._pwdobj.confirmnewpassword) {
            if (this._pwdobj.newpassword == this._pwdobj.password) {
                this.msgclass = "alert alert-danger";
                this.resultMsg = "New Password should not be the same as current password";
                this.popupMessage(this.resultMsg);
                
                this._mflag = true;
                // this.messagealert();
            } else {
                try {
                    var iv = this._util.getIvs();
                    var dm = this._util.getIvs();
                    var salt = this._util.getIvs();
                    this._pwdobj.userid = this.ics._profile.userID;
                    this._pwdobj.sessionID = this.ics._profile.sessionID;
                    let newpassword = this._util.getEncryptText(iv, salt, dm, this._pwdobj.newpassword);
                    let password = this._util.getEncryptText(iv, salt, dm, this._pwdobj.password);
                    let confirmnewpassword = this._util.getEncryptText(iv, salt, dm, this._pwdobj.confirmnewpassword);
                    let param = {
                        "password": password,
                        "newpassword": newpassword,
                        "confirmnewpassword": confirmnewpassword,
                        "userid": this.ics._profile.userID,
                        "sessionID": this.ics._profile.sessionID,
                        "iv": iv, "dm": dm, "salt": salt
                    }; 
                    let url: string = this.ics._apiurl + 'service001/changePassword';
                    let json: any = param;
                    this.http.doPost(url, json).subscribe(
                        result => {
                            this._resultobj = result;

                            if (this._resultobj.msgCode == "0016") {
                                this.sessionAlertMsg = result.msgDesc;
                                this.showMsg(result.msgDesc,false);
                            } else if (this._resultobj.msgCode == "0014") {
                                this.msgclass = "alert alert-danger";
                                this.resultMsg = this._resultobj.msgDesc;
                                this.showMsg(this.resultMsg,false);

                            } else {
                                this._chkstr = "true";
                                this.msgclass = "alert alert-success";
                                this.showMsg("Please Sign in again.",true);
                                
                                this._router.navigate(['/login']);
                            }

                            this._mflag = true;
                        },
                        error => {
                            if (error._body.type == 'error') {
                                alert("Connection Timed Out!");
                            }
                        },
                        () => { }
                    );
                } catch (e) {
                    alert(e);
                }
            }
        } else {
            this.resultMsg = "New and confirm passwords must be same";
            this.showMsg(this.resultMsg,false);
            // this.messagealert();
        }
    }

    goCancel() {
        this._pwdobj = { "password": "", "newpassword": "", "confirmnewpassword": "", "userid": "", "sessionID": "" }
        this._chkstr = "false";
    }

    popupMessage(msg) {
        this.ics.sendBean({ "t1": "rp-msg", t2: "Password Information", t3: msg });
    }

    passwordValidation(newPass) {
        if (newPass.match(this._passPattern)) {
            return true;
        } else {
            return false;
        }
    }

    showMessage() {
        jQuery("#alertmodal").modal();
        Observable.timer(3000).subscribe(
            x => {
                this.goLogOut();
            }
        );
    }

    goLogOut() {
        jQuery("#alertmodal").modal('hide');
        this._router.navigate(['Login', , { p1: '*' }]);
    }

    messagealert() {
        this.messagehide = false;
        setTimeout(() => this.messagehide = true, 3000);
    }

    /* checkSession() {
        try {
            let url: string = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
            this._resultobj = { "msgCode": "", "msgDesc": "", "keyst": "", state: false };
            this.http.doGet(url).subscribe(
                data => {
                    if (data.msgCode == '0016') {
                        this.sessionAlertMsg = data.msgDesc;
                        this.showMessage();
                    }
                },
                error => {
                    if (error._body.type == 'error') {
                        alert("Connection Error!");
                    }                   
                }, () => { });
        } catch (e) {
            alert(e);
        }

    } */

    // about session validation
    sessionTimeoutMsg = "";
    _sessionObj = this.getSessionObj();
    getSessionObj() {
        return { "sessionID": "", "userID": "" };
    }

    checkSession() {
        try {
            let url: string = this.ics._apiurl + 'service001/checkSessionTime';

            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            let json: any = this._sessionObj;

            this.http.doPost(url, json).subscribe(
                data => {
                    if (data != null) {
                        if (data.code == "0016") {
                            this.showMsgAlert(data.desc);
                            this.logout();
                        }

                        if (data.code == "0014") {
                            this.showMsgAlert(data.desc);
                        }
                    }
                },
                error => {
                    if (error._body.type == "error") {
                        alert("Connection Timed Out.");
                    }
                },
                () => { }
            );
        } catch (e) {
            alert("Invalid URL.");
        }
    }

    showMsgAlert(msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    }

    logout() {
        this._router.navigate(['/login']);
    }
    showMsg(msg, bool) {
        if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
        if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
        if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
      }
}