import { NgModule, CUSTOM_ELEMENTS_SCHEMA }       from '@angular/core';
import { FormsModule }    from '@angular/forms';
import { CommonModule }   from '@angular/common';
import { RpInputModule } from '../framework/rp-input.module';

import {pwd} from './changepwd.component';
import { settingsRouting }     from './settings.routing';
import { RpHttpService } from '../framework/rp-http.service';
import { AdminPagerModule } from '../util/adminpager.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RpInputModule,
    settingsRouting,
    AdminPagerModule
  ],
  exports : [],
  declarations: [
    pwd
  ],
  providers: [
    RpHttpService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class settingsModule {}