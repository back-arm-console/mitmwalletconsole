import { ModuleWithProviders }   from '@angular/core';
import { Routes, RouterModule }  from '@angular/router';
import { pwd }  from './changepwd.component';

const settingsRoutes: Routes = [
  { path: 'change-password',  component: pwd },  
  { path: 'change-password/:cmd', component: pwd }, 
  { path: 'change-password/:cmd/:id', component: pwd },
];

export const settingsRouting: ModuleWithProviders = RouterModule.forChild(settingsRoutes);