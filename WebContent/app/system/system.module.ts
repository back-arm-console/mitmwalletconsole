import { NgModule, CUSTOM_ELEMENTS_SCHEMA }       from '@angular/core';
import { FormsModule }    from '@angular/forms';
import { CommonModule }   from '@angular/common';
import { RpInputModule } from '../framework/rp-input.module';
import { MyDatePickerModule } from 'mydatepicker';
import { RpHttpService } from '../framework/rp-http.service';
import { AdminPagerModule } from '../util/adminpager.module';
import { PagerModule } from '../util/pager.module';
import { SystemRouting }     from './system.routing';

import {UserSetupComponent} from './usersetup.component';
import { UserListComponent } from './user-list.component';
import { PasswordPolicyComponent }  from './passwordpolicysetup.component';
import { ForceSignOutComponent } from './forcesignout.component'; 
import { MenuSetupComponent } from './menusetup.component'; 
import { MenuSetupListComponent } from './menusetup-list.component';
import { RoleSetupComponent } from './rolesetup.component'; 
import {RoleSetupListComponent} from './rolesetup-list.component';
import { VersionHistoryComponent } from './versionhistory.component';
import { VersionHistoryListComponent }  from './versionhistory-list.component';
import { LovSetupComponent }  from './lovsetup.component';
import { LOVSetupListComponent } from './lovsetup-list.component';
import { FAQMenu } from './faq-menu.component';
import { FAQList } from './faq-list.component';
import {LocatorSetupComponent} from './locatorsetup.component';
import {LocatorSetupListComponent} from './locatorsetup-list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RpInputModule,    
    AdminPagerModule,
    PagerModule,
    MyDatePickerModule,
    SystemRouting,       
  ],
  exports : [],
  declarations: [
    UserSetupComponent,
    UserListComponent,
    PasswordPolicyComponent,
    ForceSignOutComponent,    
    MenuSetupComponent,
    MenuSetupListComponent,
    RoleSetupComponent,
    RoleSetupListComponent, 
    VersionHistoryComponent,
    VersionHistoryListComponent,
    LovSetupComponent,
    LOVSetupListComponent,   
    FAQMenu,
    FAQList,
    LocatorSetupComponent,
    LocatorSetupListComponent
  ],
  providers: [
    RpHttpService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SystemModule {}