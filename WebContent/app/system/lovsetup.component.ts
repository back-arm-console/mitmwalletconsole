// RP Framework
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { enableProdMode } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpInputComponent } from '../framework/rp-input.component';
import { RpHttpService } from '../framework/rp-http.service';
import { RpBean } from '../framework/rp-bean';
import { RpReferences } from '../framework/rp-references';
import { ClientUtil } from '../util/rp-client.util';
import { Observable } from 'rxjs/Rx';
enableProdMode();
declare var jQuery: any;
declare var geoloc: any;
@Component({
  selector: 'lovsetup',
  template: `
  <div class="container-fluid">
	<form class="form-horizontal" (ngSubmit) = "goSave()">
		<legend>List of Value</legend>
			<div class="cardview list-height" style="overflow-x: hidden;">          
					<div class="form-group" style="margin-bottom: 10px;"> 
						<div class="col-md-5">
						  <button type="button" class="btn btn-sm btn-primary" (click)="goList();">List</button>
						  <button type="button" class="btn btn-sm btn-primary" (click)="goNew()">New</button>
						  <button type="submit" class="btn btn-sm btn-primary">Save</button>            
						  <button id="mycopybtn" class="btn btn-sm btn-primary" type="button" (click)="goCopy();" disabled>Copy to</button>            
						</div>
					</div>
					
						<div class="col-md-8" style="margin-bottom: 10px;">
							<div class="form-group">
								<rp-input rpType="text" rpClass="col-md-4" rpLabelClass ="col-md-2 control-label" rpLabel="Code" rpPlaceHolder = "TBA"  [(rpModel)]="_lovsetupobj.lovNo" rpReadonly = "true" >TBA</rp-input>         
							</div>         
							<div class="form-group">
								<rp-input rpType="text" rpClass="col-md-4" rpLabelClass ="col-md-2 control-label" rpLabel="Description"  [(rpModel)]="_lovsetupobj.lovDesc2" rpRequired = "true"  ></rp-input>                            
							</div>
						</div>
											
						<div calss = "col-md-12">
								<table class="table table-striped table-condensed table-hover tblborder" >
									<thead>
										<tr>
										  <th class="right">No.</th>
										  <th class="left">Value</th>
										  <th class="left">Description</th>
										  <th class="right">Price</th>
										  <th class="left">&nbsp;</th>
										</tr>
									</thead>
									<tbody>
										<tr *ngFor="let obj of _lovsetupobj.data">
											<td style="vertical-align:middle;text-align:right;" class="col-md-1">{{obj.srno}}</td>
											<td class="col-md-3" class="left">
												<rp-input rpRequired = "true" [(rpModel)]="obj.lovCde" rpType="text"  rpClass = "col-md-0"></rp-input>
											</td>
											<td class="left">
												<rp-input rpRequired = "true" [(rpModel)]="obj.lovDesc1" rpType="text"  rpClass = "col-md-0"></rp-input>
											</td>
                      <td class="col-md-2" class="left">
                        <rp-input rpRequired = "true" [(rpModel)]="obj.price" rpType="number"  rpClass = "col-md-0"></rp-input>
												<!--<input id="numinput"  class="form-control input-sm" type = "number" [(ngModel)]="obj.price" [ngModelOptions]="{standalone: true}" required> -->
											</td>
												<td class="left"><img src="image/remove.png" alt="remove.png" height="20" width="20"  (click)="goRemove(obj)"/></td>
										</tr> 
										<tr>
											<td>             
											   <button class="btn btn-sm btn-primary" type="button" (click)="goAddLovsetup()">Add</button>              
											</td>
										</tr>
									</tbody>
								</table>
						</div>					
      
				<div class="form-group"> 
					<div class="col-md-12">
					  <button type="button" class="btn btn-primary btn-sm" (click)="goList();">List</button> 
					  <button type="button" class="btn btn-primary btn-sm" (click)="goNew()">New</button>
					  <button type="submit" class="btn btn-primary btn-sm">Save</button>
					  <button id="mycopybtn" class="btn btn-primary btn-sm" type="button" (click)="goCopy();" disabled>Copy to</button>
					</div>
				</div>
		</div>
    </form>
</div> 

  <div id="sessionalert" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <p>{{sessionAlertMsg}}</p>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>

  <div [hidden] = "_mflag">
    <div  id="loader" class="modal" ></div>
  </div>
  `
})

export class LovSetupComponent {

  // RP Framework 
  subscription: Subscription;
  sub: any;
  _classname = "";
  _timer = 0;
  messagehide: boolean;
  _mflag = false;
  _returnResult = { "keyResult": 0, "longResult": "", "state": "", "stringResult": "", "userId": "", "msgCode": "", "msgDesc": "" };
  _lovsetupobj = this.getDefaultObj();
  sessionAlertMsg = ""; _key = "";

  constructor(private ics: RpIntercomService, private _router: Router, private l_util: ClientUtil, private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences) {
    // RP Framework 
    this.subscription = ics.rpbean$.subscribe(x => { })

    if (!ics.getRole() || ics.getRole() == 0) {
      this._router.navigate(['/login']);
    } else {
      this._mflag = true;
      this.checkSession();
      this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "", "msgCode": "" };
      this._lovsetupobj.createdDate = this.l_util.getTodayDate();
      this._lovsetupobj.modiDate = this.l_util.getTodayDate();
    }
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(
      params => {
        let cmd = params['cmd'];

        if (cmd != null && cmd != "" && cmd == "read") {
          let id = params['id'];
          this._key = id;
          this.goPostBySysKey(id);
        }
      }
    );
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  getDefaultObj() {
    return { "sysKey": 0, "lovNo": "TBA", "createdDate": "", "modiDate": "", "userID": "User1", "recStatus": "0", "lovDesc2": "", "lovType": "singleselect", "data": [{ "srno": "1", "lovCde": "", "lovDesc1": "", "price": 0.0 }], "messageCode": "", "messageDesc": "" };
  }

  showMessage() {
    jQuery("#sessionalert").modal();
    Observable.timer(3000).subscribe(x => {
      this.goLogOut();
    });
  }
  goLogOut() {
    jQuery("#sessionalert").modal('hide');
    this._router.navigate(['/login']);
  }

  goNew() {
    console.log("In goNew ");
    this._lovsetupobj = this.getDefaultObj();
    this._lovsetupobj.createdDate = this.l_util.getTodayDate();
    this._lovsetupobj.modiDate = this.l_util.getTodayDate();
    this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "", "msgCode": "" };
  }


  goSave() {
    this._mflag = false;
    let url: string = this.ics._apiurl + 'service001/savelovSetup';
    let json: any = this._lovsetupobj;

    this.http.doPost(url, json).subscribe(
      res => {
        if (res != null) {
          this._timer = 3000;

          if (res.messageCode == "0000" || res.messageCode == "0001") {
            this._returnResult.state = "true";
            this.showMsg(res.messageDesc,true);
          } else {
            this._returnResult.state = "false";
            this.showMsg(res.messageDesc,false);
          }

         // this.showMessageAlert(res.messageDesc);
          this._lovsetupobj.lovNo = res.lovNo;
          this._lovsetupobj.sysKey = res.sysKey;
        }

        this._mflag = true;
      }, error => {
        if (error._body.type == 'error') {
          alert("Connection Error!");
        }
      },
      () => { }
    );
  }

  goList() {
    this._router.navigate(['/lovsetuplist']);
  }

  goPostBySysKey(k) {
    let url: string = this.ics._apiurl + 'service001/getLOVbySysKey';
    this._lovsetupobj.sysKey = k;
    let json: any = this._lovsetupobj;
    this.http.doPost(url, json).subscribe(
      res => {
        if (res != null) {
          this._lovsetupobj = res;

          if (!(res.data instanceof Array)) {
            let m = [];
            m[0] = res.data;
            this._lovsetupobj.data = m;
          }

          this.goEnable();
          this._mflag = true;
        }
      }, error => {
        if (error._body.type == 'error') {
          alert("Connection Error!");
        }
      },
      () => { }
    );
  }

  goRemove(p) {
    let index = this._lovsetupobj.data.indexOf(p);
    let length = this._lovsetupobj.data.length;

    if (length < 2) {
      this._lovsetupobj.data[0] = { srno: ("").toString(), lovCde: "", lovDesc1: "", "price": 0.0 };
    } else {
      this._lovsetupobj.data.splice(index, 1);
    }

    for (var i = 0; i < length; i++) {
      let maxsrno = i;
      maxsrno = maxsrno + 1;
      this._lovsetupobj.data[i].srno = (maxsrno).toString();
    }
  }

  goAddLovsetup() {
    if (this._lovsetupobj.data[0].srno == "") {
      this._lovsetupobj.data[0].srno = "1";
    }

    let maxsrno = this._lovsetupobj.data.length;
    maxsrno = maxsrno + 1;
    this._lovsetupobj.data.push({ srno: (maxsrno).toString(), lovCde: "", lovDesc1: "", "price": 0.0 });
  }

  goEnable() {
    jQuery("#mycopybtn").prop("disabled", false);
  }

  showMessageAlert(msg) {
    this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
  }

  goCopy() {
    this._lovsetupobj.lovNo = "TBA";
    this._lovsetupobj.lovDesc2 = "";
  }

  // about session validation
  sessionTimeoutMsg = "";
  _sessionObj = this.getSessionObj();
  getSessionObj() {
    return { "sessionID": "", "userID": "" };
  }

  checkSession() {
    try {
      let url: string = this.ics._apiurl + 'service001/checkSessionTime';

      this._sessionObj.sessionID = this.ics._profile.sessionID;
      this._sessionObj.userID = this.ics._profile.userID;
      let json: any = this._sessionObj;

      this.http.doPost(url, json).subscribe(
        data => {
          if (data != null) {
            if (data.code == "0016") {
              this.showMsgAlert(data.desc);
              this.logout();
            }

            if (data.code == "0014") {
              this.showMsgAlert(data.desc);
            }
          }
        },
        error => {
          if (error._body.type == "error") {
            alert("Connection Timed Out.");
          }
        },
        () => { }
      );
    } catch (e) {
      alert("Invalid URL.");
    }
  }

  showMsgAlert(msg) {
    this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
  }

  logout() {
    this._router.navigate(['/login']);
  }
  showMsg(msg, bool) {
    if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
    if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
    if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
  }

}