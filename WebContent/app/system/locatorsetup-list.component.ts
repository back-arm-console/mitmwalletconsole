import { Component, enableProdMode } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { RpHttpService } from '../framework/rp-http.service';
import { RpReferences } from '../framework/rp-references';
// RP Framework
import { RpIntercomService } from '../framework/rp-intercom.service';
declare var jQuery: any;
enableProdMode();
@Component({
    selector: 'locatorsetup-list',
    template: ` 
    <div class="container col-md-12">
    <form class="form-horizontal"> 
         <legend>Locator List</legend>
			<div class="cardview list-height">
			 <div *ngIf="_flagas">			 
				<div class="row col-md-12">
					<div class="row col-md-3">
						<div  class="input-group">
							<span class="input-group-btn">
								<button class="btn btn-sm btn-primary" type="button" (click)="goNew();">New</button>
							</span> 
							<input id="textinput" name="textinput" type="text"  placeholder="Search" [(ngModel)]="_objs.searchText" (keyup)="searchKeyup($event)" [ngModelOptions]="{standalone: true}" maxlength="30" class="form-control input-sm">
							<span class="input-group-btn">
								<button class="btn btn-sm btn-primary" type="button" (click)="Searching()" >
									<span class="glyphicon glyphicon-search"></span>Search
								</button>
							</span>        
						</div> 
					</div>		
                        <div class="pagerright">
							<adminpager rpPageSizeMax="100" [(rpModel)]="_objs.totalCount" (rpChanged)="changedPager($event)" ></adminpager> 
						</div>
				</div>      
			 </div>  
        
            <!--<div *ngIf="_showListing">-->
            <div class="row col-md-12" style="overflow-x:auto">
			 <table class="table table-striped table-condensed table-hover tblborder" id="tblstyle">
				<thead>
					<tr>
						<th class="center">ID</th>
						<th>Name</th>
						<th>Latitude</th>
						<th>Longitude</th>
						<th>LocationType</th>
						<th>Branch Code</th>
						<th>Address</th>
						<th>Phone 1</th>
						<th>Phone 2</th>    
					</tr>
				</thead>
				<tbody>
					<tr *ngFor="let obj of _objs.data">
						<td class="center"><a (click)="goto(obj.t1)">{{obj.t1}}</a></td>
						<td>{{obj.name}}</td>
						<td>{{obj.latitude}}</td>
						<td>{{obj.longitude}}</td>
						<td>{{obj.locationType}}</td>
						<td>{{obj.branchCode}}</td>
						<td>{{obj.address}}</td>
						<td>{{obj.phone1}}</td>
						<td>{{obj.phone2}}</td>
					</tr>  
				</tbody>
			</table>
            </div>
                <!--<div align="center" *ngIf="_showListing" >
                    Total {{_objs.totalCount}}
                </div>-->
        </div>
        
	</form>
 </div> 
  <!--  <div id="sessionalert" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <p>{{_sessionMsg}}</p>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>-->
 <div [hidden] = "_mflag">
    <div class="modal" id="loader"></div>
   </div>
    `,

})

export class LocatorSetupListComponent {
    _sessionMsg = "";
    _mflag = false;
    subscription: Subscription;
    _msghide: boolean;
    _flagas = true;
    _showListing=false;
    _objs = {
        "data": [{ "createdDate": "", "modifiedDate": "", "latitude": "", "longitude": "", "address": "", "phone1": "", "phone2": "", "branchCode": "", "name": "", "locationType": "", "t1": "" }],
        "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0
    };
    _pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 0 };

    constructor(private ics: RpIntercomService, private _router: Router, private http: RpHttpService,private ref: RpReferences) {
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(x => { })
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        } else {
            this._msghide = true;
            this._mflag = false;
            this.search();
        }
    }


    changedPager(event) {
        if (this._objs.totalCount != 0) {
            this._pgobj = event;
            let current = this._objs.currentPage;
            let size = this._objs.pageSize;
            this._objs.currentPage = this._pgobj.current;
            this._objs.pageSize = this._pgobj.size;
            if (this._pgobj.current != current || this._pgobj.size != size)
                this.search();
        }

    }

    search() {
        try {
            this._mflag = false;
            this.http.doGet(this.ics.cmsurl + 'service001/getATMLocatorList?searchVal=' + this._objs.searchText + '&pagesize=' + this._objs.pageSize + '&currentpage=' + this._objs.currentPage + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(
                response => {
                    
                    this._objs = response;
                    if (response.msgCode == '0016') {
                        this._sessionMsg = response.msgDesc;
                        this.showMessage(response.msgDesc,false);
                    }
                    if (response.data != null && response.data != undefined) {
                        this._showListing=true;
                        if (!(response.data instanceof Array)) {
                            let m = [];
                            m[0] = response.data;
                            this._objs.data = m;
                            this._objs.totalCount = response.totalCount;                            
                        } 
                        for (let i = 0; i < this._objs.data.length; i++) {
                            for (let j = 0; j < this.ref._lov3.refLocType.length; j++) {
                              if (this._objs.data[i].locationType == this.ref._lov3.refLocType[j].value) {
                                this._objs.data[i].locationType = this.ref._lov3.refLocType[j].caption;                                                                
                                break;
                              }
                            }
                          }
                    }else {
                        this._objs = response;
                        if (response.totalCount == 0) {
                            this.showMessage("Data not found!",false);
                        }
                    }

                    this._mflag = true;
                },
                error => {
                    if (error._body.type == 'error') {
                        alert("Connection Timed Out!");
                    }
                },
                () => { }
            );
        } catch (e) {
            alert(e);
        }

    }

    searchKeyup(e: any) {
        if (e.which == 13) { // check enter key
            this._objs.currentPage = 1;
            this._msghide = true;
            this._showListing = false;
            this.search();
        }
    }

    Searching() {
        this._objs.currentPage = 1;
        this._msghide = true;
        this._showListing = false;
        this.search();
    }

    goto(p) {
        this._router.navigate(['/locator-setup', 'read', p]);
    }

  //  showMessage() {
  //     jQuery("#sessionalert").modal();
  //     Observable.timer(3000).subscribe(x => {
  //        this._router.navigate(['Login', , { p1: '*' }]);
  //         jQuery("#sessionalert").modal('hide');
  //   });
  // }

    goNew() {
        this._router.navigate(['locator-setup', , { cmd: "NEW" }]);
    }
    showMessage(msg, bool) {
        if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
        if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
        if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "primary", "t3": msg }); }
    }
}