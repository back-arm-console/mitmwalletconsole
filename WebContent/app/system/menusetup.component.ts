import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { enableProdMode } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpInputComponent } from '../framework/rp-input.component';
import { RpHttpService } from '../framework/rp-http.service';
import { RpBean } from '../framework/rp-bean';
import { RpReferences } from '../framework/rp-references';
import { ClientUtil } from '../util/rp-client.util';
import { Observable } from 'rxjs/Rx';
declare var jQuery: any;
enableProdMode();
@Component({
  selector: 'menu-setup',
  template: `
  <div class="container-fluid">
  <div class="row clearfix">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
	<form class="form-horizontal" (ngSubmit) = "goSave()">
		<legend>Menu</legend>
			<div class="cardview list-height">
				<div class="row col-md-12">  
				  <button class="btn btn-sm btn-primary" type="button" (click)="goList()" >List</button> 
				  <button class="btn btn-sm btn-primary" type="button" (click)="goNew()" >New</button>      
				  <button class="btn btn-sm btn-primary" id="mySave" type="submit" >Save</button>          
				  <button class="btn btn-sm btn-primary" disabled id="mydelete"  type="button" (click)="goDelete();" >Delete</button> 
        </div>
        <div class="row col-md-12">&nbsp;</div>
        <div class="form-group"> 
				    
					<div class="col-md-8">
						<div class="form-group">    
						  <label class="checkbox-inline">    
							<input type="radio" [checked]="parentcheck" id="parentradio"  (click)="checkParents()" name="optionsRadiosinline"   value="option1" checked> 
							Main Menu 
						  </label>  
						  
						  <label class="checkbox-inline">      
							<input type="radio" [checked]="childcheck" id="childradio" (click)="checkChild()"   name="optionsRadiosinline"   value="option2"> 
							Sub Menu    
						  </label>  
						</div>
						<div class="form-group">
						  <label class="col-md-2" [hidden]="rd"> Main Menu <font class="mandatoryfont" >*</font></label>
						  <div [hidden]="rd">
								<rp-input  rpLabelClass="col-md-0" [(rpModel)]="menusys" rpRequired ="true"  rpType="mainmenu" rpClass="col-md-4"></rp-input>
						  </div>              
						</div>        
						  
						<div class="form-group">
							<rp-input  rpType="text" rpClass="col-md-2" rpClass="col-md-4" rpLabelClass ="col-md-2 control-label" rpLabel="Code" [(rpModel)]="_obj.syskey" rpReadonly="true" rpRequired="true"></rp-input>
						</div>

						<div class="form-group">        
							<rp-input  rpType="text" rpClass="col-md-4" rpLabelClass ="col-md-2 control-label" rpLabel="Menu Item" *ngIf="chkmt==false" [(rpModel)]="_obj.t3" rpRequired="true"></rp-input>
							<rp-input  rpType="text" rpClass="col-md-4" rpLabelClass ="col-md-2 control-label" rpLabel="Menu Item" *ngIf="chkmt" [(rpModel)]="_obj.t3" rpReadonly="{{rd}}" rpRequired="true"></rp-input>
						</div>
				  
						<div class="form-group">        
							<rp-input  rpType="text" rpClass="col-md-4" rpLabelClass ="col-md-2 control-label" rpLabel="Description" [(rpModel)]="_obj.t2" rpRequired="true"></rp-input>
						</div>

						<div class="form-group">
							<label class="col-md-2" [hidden]="rd"> Type <font color="#FF0000" >*</font></label>
							<div class="col-md-4" [hidden]="rd">
							  <select [(ngModel)]="_obj.n1" class="form-control input-sm col-md-0" tabindex="2" [ngModelOptions]="{standalone: true}">
								  <option  *ngFor="let item of ref._lov1.refmenu" value="{{item.value}}">{{item.caption}}</option>
							  </select>
							</div> 
						</div> 

					</div>
      </div>
      </div>
    </form>
</div> 
</div>
</div>
      
    <div id="sessionalert" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <p>{{sessionAlertMsg}}</p>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>

<div [hidden] = "_mflag">
  <div  id="loader" class="modal" ></div>
</div>
    
  `
})
export class MenuSetupComponent {
  // RP Framework 
  subscription: Subscription;
  sub: any;
  _menusyskeyrecord: number;
  sessionAlertMsg = "";
  chkmt: boolean; checkparam: boolean; parentcheck: boolean; childcheck: boolean; messagehide: boolean; rd: boolean; rd1: boolean;
  _obj = this.getDefaultObj();
  _before = this._obj;
  _result = { "longResult": "", "msgCode": "", "msgDesc": "", "key": [], "keyResult": 0, "state": "", "stringResult": "" };
  _keyobj = { "_key": "", "isParent": "", "sessionId": "", "userId": "" };
  _key = "";
  menusys: number;
  _output1 = "";
  _mflag = false;

  constructor(private ics: RpIntercomService, private _router: Router, private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences) {
    this.subscription = ics.rpbean$.subscribe(x => { })
    if (!ics.getRole() || ics.getRole() == 0) {
      this._router.navigate(['/login']);
    } else {
      this._mflag = false;
      this.checkSession();
      this.getmainlist();
      this.chkmt = false;
      this._obj = this.getDefaultObj();
    }
  }
  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let cmd = params['cmd'];
      if (cmd != null && cmd != "" && cmd == "read") {
        let id = params['id'];
        this._key = id;
        this.goReadBySyskey(id);
      }
    });
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  getDefaultObj() {
    //Combo BUtton Control
    this.rd = true;
    //Menu Item Control
    this.chkmt = false;
    //Radio Box Control
    this.parentcheck = true;
    this.childcheck = false;

    jQuery("#mydelete").prop("disabled", true);
    jQuery("#mySave").prop("disabled", false);
    return { "syskey": null, "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "t6": "", "n2": 0, "n5": 0,"n1":0, "userId": "", "userName": "", "msgCode": "", "msgDesc": "", "sessionId": "" };

  }

  goReadBySyskey(p) {
    try {
      this.chkmt = true;
      let url: string = this.ics._apiurl + 'service001/getMenuData?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
      this.http.doPost(url, p).subscribe(
        data => {
          if (data.msgCode == '0016') {
            this.sessionAlertMsg = data.msgDesc;
            this.showMsg(data.msgDesc,false);
          }
          else {
            jQuery("#mydelete").prop("disabled", false);
            this._obj = data;
            this.menusys = this._obj.n2;
            if (this._obj.n2 == 0) {
              this.parentcheck = true;
              this.childcheck = false;
            }
            else {
              this.parentcheck = false;
              this.childcheck = true;
              this.getmainlist();
              this.rd = false;
            }
          }
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
        }, () => { });
    } catch (e) {
      alert(e);
    }

  }

  goSave() {
    if ((this.rd == false && this.menusys == 0) || (this.rd == false && this.menusys == undefined)) {
      this._result.msgDesc = "Please Select Main Menu";
      this._result.state = "false";
      this.showMsg(this._result.msgDesc,false);
    } if ((this.rd == false && this._obj.n1 == 0) || (this.rd == false && this._obj.n1 == undefined)) {
      this._result.msgDesc = "Please Select Type";
      this.showMsg(this._result.msgDesc,false);
    }else {
      this._obj.n2 = this.menusys;
      this._obj.n5 = 1;
      this._obj.t4 = "51";
      this._obj.t5 = "100%";
      this._obj.t6 = "800";
      this._obj.t1 = "/anglar2";
      this._obj.userId = this.ics._profile.userID;
      try {
        this._obj.sessionId = this.ics._profile.sessionID;
        let url: string = this.ics._apiurl + 'service001/saveMenu';
        let json: any = this._obj;
        this.http.doPost(url, json).subscribe(
          data => {
            if (data.msgCode == '0016') {
              this.sessionAlertMsg = data.msgDesc;
              this.showMsg(data.msgDesc,false);
            }
            else {
              this._output1 = JSON.stringify(data);
              this._result = data;
              this.messagehide = true;
              this.showMsg(this._result.msgDesc,true);
              this._obj.syskey = this._result.key[1];
              if (this._result.state) {
                this.chkmt = true;
                if (this.rd != false) {//|| this.rd == true
                  this.getmainlist();
                  this._obj.syskey = this._result.key[1];
                  if (this.checkparam != false) {//|| this.checkparam == true
                    this.checkparam = false;
                  }
                  else {
                    jQuery("#mydelete").prop("disabled", true);
                  }
                }
              }
              else {
                this.showMsg(this._result.msgDesc,false);
              }
            }
          },
          error => {
            if (error._body.type == 'error') {
              alert("Connection Timed Out!");
            }
          }, () => { });
      } catch (e) {
        alert(e);
      }
    }
  }

  goList() {
    this._router.navigate(['/menusetup-list']);
  }
  showMessage() {
    jQuery("#sessionalert").modal();
    Observable.timer(3000).subscribe(x => {
      this.goLogOut();
    });
  }

  goLogOut() {
    jQuery("#sessionalert").modal('hide');
    this._router.navigate(['/login']);
  }

  /* checkSession() {
    try {

      let url: string = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
      this.http.doGet(url).subscribe(
        data => {
          if (data.msgCode == '0016') {
            this.sessionAlertMsg = data.msgDesc;
            this.showMessage();
          }
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
          else {

          }
        }, () => { });
    } catch (e) {
      alert(e);
    }
  } */

  getmainlist() {
    try {
      this._mflag = false;
      this.http.doGet(this.ics._apiurl + 'service001/getMainList').subscribe(
        data => {
          if (data != null || data != undefined) {
            this.ref._lov3.mainmenu = data;
          }
          this._mflag = true;
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
        }, () => { });
    } catch (e) {
      alert(e);
    }
  }


  checkChild() {
    this.rd = false;
    //this._obj.syskey = null;
  }

  checkParents() {
    this.rd = true;
    this._obj.n2 = 0;
    //this._obj.syskey = null;
  }

  goDelete() {
    try {
      this._mflag = false;
      let url: string = this.ics._apiurl + 'service001/deleteMenu';
      if (this.rd == true) {
        this._keyobj.isParent = "true";
      }
      else {
        this._keyobj.isParent = "false";
      }
      this._keyobj.sessionId = this.ics._profile.sessionID;
      this._keyobj.userId = this.ics._profile.userID;
      this._keyobj._key = this._key;
      let json: any = this._keyobj;
      this.http.doPost(url, json).subscribe(
        data => {
          if (data.msgCode == '0016') {
            this.sessionAlertMsg = data.msgDesc;
            this.showMsg(data.msgDesc,false);
          }
          else {
            this._output1 = JSON.stringify(data);
            this._result = data;
            this.showMsg(this._result.msgDesc,true);
            if (this._result.state) { //&& this._result.state != "false"
              this.getmainlist();
              if (this.checkparam != false) { //|| this.checkparam == true
                this.goNew();
              }
            }
            else {
              jQuery("#mydelete").prop("disabled", false);
            }
          }
          this._mflag = true;
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
          else {

          }
        }, () => { });
    } catch (e) {
      alert(e);
    }
  }

  showMessageAlert(msg) {
    this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
  }

  clearData() {
    this._obj = this.getDefaultObj();
    jQuery("#mydelete").prop("disabled", true);
    jQuery("#mySave").prop("disabled", false);
  }

  goNew() {
    this.clearData();
    this.rd = true;
    this.chkmt = false;
    jQuery('#parentradio').prop('checked', 'checked');
    jQuery('#childradio').prop('checked', false);
  }

  // about session validation
  sessionTimeoutMsg = "";
  _sessionObj = this.getSessionObj();
  getSessionObj() {
    return { "sessionID": "", "userID": "" };
  }

  checkSession() {
    try {
      let url: string = this.ics._apiurl + 'service001/checkSessionTime';

      this._sessionObj.sessionID = this.ics._profile.sessionID;
      this._sessionObj.userID = this.ics._profile.userID;
      let json: any = this._sessionObj;

      this.http.doPost(url, json).subscribe(
        data => {
          if (data != null) {
            if (data.code == "0016") {
              this.showMsgAlert(data.desc);
              this.logout();
            }

            if (data.code == "0014") {
              this.showMsgAlert(data.desc);
            }
          }
        },
        error => {
          if (error._body.type == "error") {
            alert("Connection Timed Out.");
          }
        },
        () => { }
      );
    } catch (e) {
      alert("Invalid URL.");
    }
  }

  showMsgAlert(msg) {
    this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
  }

  logout() {
    this._router.navigate(['/login']);
  }
  showMsg(msg, bool) {
    if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
    if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
    if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
  }

}