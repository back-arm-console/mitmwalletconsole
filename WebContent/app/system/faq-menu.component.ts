import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpInputComponent } from '../framework/rp-input.component';
import { RpHttpService } from '../framework/rp-http.service';
import { RpBean } from '../framework/rp-bean';
import { RpReferences } from '../framework/rp-references';
import { ClientUtil } from '../util/rp-client.util';
declare var jQuery: any;
declare function capture(fileName: String): any;
declare function save(): any;
declare function convertCanvasToImage(): any;
declare function loadData(): any;
declare function displayImage(fileName: String): any;
declare function isMyanmar(pwd: String): boolean;
declare function isZawgyi(pwd: String): boolean;
declare function ZgtoUni(pwd: String): string;
declare function isUnicode_my(pwd: String): boolean;
declare function UnitoZg(pwd: String): string;
declare function ZgtoUni(pwd: String): string;
declare function systemFont(): string;
declare function wdth(pwd: String): string;
declare function identifyFont(pwd: String): string;
import 'tinymce/tinymce.min';
declare var tinymce: any;
import 'tinymce/themes/modern/theme';
import 'tinymce/plugins/link/plugin.js';
import 'tinymce/plugins/paste/plugin.js';
import 'tinymce/plugins/table/plugin.js';
import 'tinymce/plugins/advlist/plugin.js';
import 'tinymce/plugins/autoresize/plugin.js';
import 'tinymce/plugins/lists/plugin.js';
import 'tinymce/plugins/code/plugin.js';
import 'tinymce/plugins/image/plugin.js';
import 'tinymce/plugins/imagetools/plugin.js';
import { Json } from '@angular/core/src/facade/lang';

@Component({
    selector: 'faqMenu',
    template: `
    <div class="container-fluid">
    <div class="row clearfix">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
	<form class="form-horizontal" ngNoForm>
		<legend>FAQ</legend>
		<div class="cardview list-height">    
            <div class="row col-md-12">
                <button class="btn btn-sm btn-primary"  type="button"   (click)="goList()">List</button> 
                <button class="btn btn-sm btn-primary" [disabled]="flagnew" id="new" type="button" (click)="goNew()">New</button>      
                <button class="btn btn-sm btn-primary" [disabled]="flagsave" id="save" type="button" (click)="goSave()">Save</button> 
                <button class="btn btn-sm btn-primary" [disabled]="flagdelete" id="delete" type="button" (click)="goDelete();" >Delete</button>  
            </div>

            <div class="row col-md-12">&nbsp;</div>
              <div class="form-group">   
        
            <div class="col-md-12">
                <div class="form-group">
                    <label class="col-md-2">Question by English</label>
                    <div class="col-md-4">
                        <textarea  type="text" [(ngModel)]="_faqObj.questionEng" class="form-control input-sm uni" style="height:100px;"></textarea>                       
                    </div>
                        <label class="col-md-2" align="left">Answer by English&nbsp;</label>
                        <div class="col-md-4" > 
                             <textarea  type="text" [(ngModel)]="_faqObj.answerEng" class="form-control input-sm uni" style="height:100px;"> </textarea>
                        </div> 
                   
                </div>
    
                <div class="form-group">
                    <label class="col-md-2">Question by Myanmar</label>
                    <div class="col-md-4">
                        <textarea type="text" [(ngModel)]="_faqObj.questionUni" class="form-control input-sm uni" style="height:100px;"> </textarea>                     
                    </div>                    
                   
                    <label class="col-md-2" align="left">Answer by Myanmar&nbsp;</label>
                        <div class="col-md-4" > 
                              <textarea  type="text" [(ngModel)]="_faqObj.answerUni" class="form-control input-sm uni" style="height:100px;"> </textarea>
                        </div>  
                </div>           
    
                <br>
                <br>
            </div>
			</div>
		</div>
    </form>
</div>
</div>
</div>
    <div [hidden]="_mflag">
    <div  id="loader" class="modal" ></div></div>
`
})

export class FAQMenu {
    obj: any;
    sub: any;
    catch = "";
    message = "";
    _font = "";
    _hide = true;
    _nrcupdatelist = true;
    subscription: Subscription;
    _mflag =true;
    _faqObj=this.getFAQObj();
    flagnew = true;
    flagsave = true;
    flagdelete = true;
    _key="";
  
    constructor(private ics: RpIntercomService, private _router: Router, private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences) {
      this.subscription = ics.rpbean$.subscribe(x => { })
      if (!ics.getRole() || ics.getRole() == 0) {
        this._router.navigate(['/login']);
      } else {    
      this.flagnew = false;
      this.flagsave=false;
      this.flagdelete = true;
      }
    }
    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
          let cmd = params['cmd'];
          if (cmd != null && cmd != "" && cmd == "read") {
            let id = params['id'];
            this.flagdelete = false;
            this._key = id;
            this.goReadBySyskey(id);
          }
        });
      }

      goReadBySyskey(p) {
        try {
          let url: string = this.ics.cmsurl + 'serviceCMS/getFAQbysyskey?id=' + this._key + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
          this.http.doGet(url).subscribe(
            data => {
              this._faqObj = data;
              if (this._faqObj.msgCode == '0016') {
                this.showMessage(this._faqObj.msgDesc,false);
              }
              this._mflag = true;
            },
            error => {
              if (error._body.type == 'error') {
                alert("Connection Error!");
              }
            }, () => { });
        } catch (e) {
          alert(e);
        }
      }

    /* ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
          let cmd = params['cmd'];
          if (cmd != null && cmd != "" && cmd == "new") {
            this.goNew();
          } else if (cmd != null && cmd != "" && cmd == "read") {
            var _List = this.ics.getBean().faqData;
            this._mflag=false;
            this.flagdelete = false;
            this._faqObj.syskey=_List.syskey;
            this._faqObj.questionEng=_List.questionEng;
            this._faqObj.answerEng=_List.answerEng;
            this._faqObj.questionUni=_List.questionUni;
            this._faqObj.answerUni=_List.answerUni;
          }
        });
      } */
  
    getFAQObj() {
      return {
        "srno":0,"syskey": 0, "autokey": 0,"createddate":"","modifiedDate":"","status":0,"userID":"","sessionID":"","questionEng": "", "answerEng": "", "questionUni": "", 
        "answerUni": "","t1": "", "t2": "", "t3": "", "t4": "", "t5": "","n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0,"msgDesc":"","msgCode":"",
    };
    }

    _result = { "longResult": "", "msgDesc": "", "key": [], "keyResult": 0, "state": "", "stringResult": "" };

    goSave() {
            if (this.isValidate()) {
                this._mflag=false;
                this._faqObj.userID=this.ics._profile.userID;
                this._faqObj.sessionID=this.ics._profile.sessionID;
                let url: string = (this.ics.cmsurl+ 'serviceCMS/saveFAQMenu?userid');
                let json: any = this._faqObj;
                this.http.doPost(url, json).subscribe(
                    data => {
                        this._mflag = true;
                        this._result = data;
                        this.showMessage(data.msgDesc, data.state);
                        this._faqObj.syskey = data.longResult[0];
                    },
                    error => {
                        this.showMessage("Can't Saved This Record!", undefined);
                    },
                    () => { }
                );
            }
    }

    goDelete() {
        if (this._faqObj.questionEng != "") {
            this._mflag=false;
            this._faqObj.userID=this.ics._profile.userID;
            this._faqObj.sessionID=this.ics._profile.sessionID;
            let url: string = this.ics.cmsurl + 'serviceCMS/deleteFAQ';
            let json: any = this._faqObj;
            this.http.doPost(url, json).subscribe(
                data => {
                    this._mflag = true;
                    this.showMessage(data.msgDesc, data.state);
                    if (data.state) {
                        this.goNew();
                    }
                },
                error => { },
                () => { }
            );
        } else {
            this.showMessage("No Article to Delete!", undefined);
        }
    }
  
    isValidate() {
      if (this._faqObj.questionEng == "") {
        this.showMessage("Please fill QuestionEng!", false);
        return false;
      }
      if (this._faqObj.answerEng== "") {
        this.showMessage("Please fill AnswerEng!", false);
        return false;
      }
      if (this._faqObj.questionUni== "") {
        this.showMessage("Please fill QuestionUni!", false);
        return false;
      }
      if (this._faqObj.answerUni== "") {
        this.showMessage("Please fill AnswerUni!", false);
        return false;
      }
      
      return true;
    }

    showMessage(msg, bool) {
        if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
        if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
        if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "primary", "t3": msg }); }
    }
    

    goList() {
        this._router.navigate(['/faq-list']);
    }

    goNew() {
        this.flagdelete = true;
        this.flagnew = false;
        this.flagsave=false;
        this._router.navigate(['/faq-menu']);
      }
  
   
}