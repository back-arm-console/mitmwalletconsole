import { Component, enableProdMode } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { Subscription } from 'rxjs/Subscription';
import { RpHttpService } from '../framework/rp-http.service';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpReferences } from '../framework/rp-references';
import { ClientUtil } from '../util/rp-client.util';
declare var jQuery: any;
enableProdMode();
@Component({
  selector: 'passworpolicy-setup',
  template: `
  <div class="container-fluid">
  <div class="row clearfix">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0"> 
	<form class="form-horizontal" class= "form-horizontal" (ngSubmit)="goValidate()">
    <legend>Password Policy</legend>
    
		<div class="cardview list-height">
        <div class="row col-md-12">      
				  <button class="btn btn-sm btn-primary" disabled id="btnupdate"  type="submit">Save</button>
				  <button class="btn btn-sm btn-primary" type="button" (click)="goEdit()">Edit</button>
				</div>
            
      <div class="row col-md-12">&nbsp;</div>         
			<div class="row col-md-6">    
			  <div class="form-group">
        <label class="col-md-3">Minimum Length </label>
        <div class="col-md-4">
        <input min="0" type="number" *ngIf="_editEvent == false" [(ngModel)]='_pswobj.pswminlength' [ngModelOptions]="{standalone: true}" class="form-control input-sm" disabled readonly>
			  <input min="0" type="number" *ngIf="_editEvent == true" [(ngModel)]='_pswobj.pswminlength' [ngModelOptions]="{standalone: true}" class="form-control input-sm">
        </div>			  
			  </div>
        <div class="form-group">
        <label class="col-md-3">Maximum Length</label>
        <div class="col-md-4">
        <input min="0" type="number" *ngIf="_editEvent == false" [(ngModel)]='_pswobj.pswmaxlength' [ngModelOptions]="{standalone: true}" class="form-control input-sm" disabled readonly>
			  <input min="0" type="number" *ngIf="_editEvent == true" [(ngModel)]='_pswobj.pswmaxlength' [ngModelOptions]="{standalone: true}" class="form-control input-sm">
        </div>			
			  </div>    
        <div class="form-group">
        <label class="col-md-3">Special Character</label>
        <div class="col-md-4">
        <input min="0" type="number" *ngIf="_editEvent == false" [(ngModel)]='_pswobj.spchar' [ngModelOptions]="{standalone: true}" class="form-control input-sm" disabled readonly>
			  <input min="0" type="number" *ngIf="_editEvent == true" [(ngModel)]='_pswobj.spchar' [ngModelOptions]="{standalone: true}" class="form-control input-sm">
        </div>
			 
			  </div>
        <div class="form-group">
        <label class="col-md-3">Uppercase Character</label>
        <div class="col-md-4">
        <input min="0" type="number" *ngIf="_editEvent == false" [(ngModel)]='_pswobj.upchar' [ngModelOptions]="{standalone: true}" class="form-control input-sm" disabled readonly>
			  <input min="0" type="number" *ngIf="_editEvent == true" [(ngModel)]='_pswobj.upchar' [ngModelOptions]="{standalone: true}" class="form-control input-sm">
        </div>
			  
			  </div>
        <div class="form-group">
        <label class="col-md-3">Lowercase Character</label>
        <div class="col-md-4">
        <input min="0" type="number" *ngIf="_editEvent == false" [(ngModel)]='_pswobj.lowerchar' [ngModelOptions]="{standalone: true}" class="form-control input-sm" disabled readonly>
			  <input min="0" type="number" *ngIf="_editEvent == true" [(ngModel)]='_pswobj.lowerchar' [ngModelOptions]="{standalone: true}" class="form-control input-sm">
        </div>
			  
			  </div>
        <div class="form-group">
        <label class="col-md-3">Number</label>
        <div class="col-md-4">
        <input min="0" type="number" *ngIf="_editEvent == false" [(ngModel)]='_pswobj.pswno' [ngModelOptions]="{standalone: true}" class="form-control input-sm" disabled readonly>
			  <input min="0" type="number" *ngIf="_editEvent == true" [(ngModel)]='_pswobj.pswno' [ngModelOptions]="{standalone: true}" class="form-control input-sm">
        </div>
			   
			  </div>
      </div> 
      </div>
    </form> 
</div>
</div>
</div>
    
    <div id="sessionalert" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <p>{{sessionAlertMsg}}</p>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>

<div [hidden] = "_mflag">
  <div  id="loader" class="modal" ></div>
</div>
    
  `
})
export class PasswordPolicyComponent {
  // RP Framework 
  subscription: Subscription;
  sub: any;
  _returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };

  msghide: boolean;
  _pswobj = this.getDefaultObj();
  sessionAlertMsg = "";
  _mflag = false; 
  _editEvent: boolean;
  _util: ClientUtil = new ClientUtil();
  constructor(private ics: RpIntercomService, private _router: Router, private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences) {
    this.subscription = ics.rpbean$.subscribe(x => { })
    if (!ics.getRole() || ics.getRole() == 0) {
      this._router.navigate(['/login']);
    } else {
      this._mflag = false;
      this.checkSession();
      this.msghide = true;
      this._pswobj = this.getDefaultObj();
      this._editEvent = false;
    }
  }
  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.goReadBySyskey();
    });
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  getDefaultObj() {
    this._editEvent = false;
    return { "pswminlength": 0, "pswmaxlength": 0, "spchar": 0, "upchar": 0, "lowerchar": 0, "pswno": 0, "msgCode": "", "msgDesc": "", "sessionID": "", "userID": "" };
  }

  goReadBySyskey() {
    this._mflag = false;
    let url:string = this.ics._apiurl + 'service001/readPswPolicy?sessionID='+this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID+ '&formID=pwdpolicy';
    console.log(JSON.stringify(url));
    this.http.doGet(url).subscribe(
    data => { 
         if(data.msgCode == '0016')
         {
             this.sessionAlertMsg = data.msgDesc;
             this.showMessage();
         } 
         else{
            this._pswobj = data; 
         }
         this._mflag = true;         
    },
    error => {
      if (error._body.type == 'error') {
        alert("Connection Error!");
      }
    });
  }

  goList() {
    this._router.navigate(['/ActivateUserList']);
  }
  showMessage() {
    jQuery("#sessionalert").modal();
    Observable.timer(3000).subscribe(x => {
      this.goLogOut();
    });
  }

  goLogOut() {
    jQuery("#sessionalert").modal('hide');
    this._router.navigate(['/login']);
  }

  showMessageAlert(msg) {
    this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
  }

  goEdit()
  {
      this._editEvent = true;
      jQuery("#btnupdate").prop("disabled", false);
  }

  goSave() {
    this._mflag = false;
    let key = localStorage.getItem("key");
    this._pswobj.sessionID = this.ics._profile.sessionID;
    this._pswobj.userID = this.ics._profile.userID;
      let url: string = this.ics._apiurl + 'service001/savePswPolicy';
      let json: any = this._pswobj;
      this.http.doPost(url, json).subscribe(
        data => {
          if (data.msgCode == '0016') {
            this.sessionAlertMsg = data.msgDesc;
            this.showMsg(data.msgDesc, false);
          }
          else if (data.msgCode == '0000') {
            this._editEvent = false;
            jQuery("#btnupdate").prop("disabled", true);
            this.showMsg(data.msgDesc, true);
          }
          else {
            this._editEvent = false;
            this.showMsg(data.msgDesc, false);
          }

          this._mflag = true;
        },
        error => alert(error), () => { });
    
  }

  goValidate() {
    let total = Number(this._pswobj.pswno) + Number(this._pswobj.spchar) + Number(this._pswobj.upchar) + Number(this._pswobj.lowerchar);
    /* if (Number(this._pswobj.pswminlength) < 0 || Number(this._pswobj.pswmaxlength) < 0 || Number(this._pswobj.pswno) < 0 || Number(this._pswobj.spchar) < 0 || Number(this._pswobj.upchar) < 0 || Number(this._pswobj.lowerchar) < 0) {
      this.showMsg("Input data should not be negative number",false);
    } */
    if (Number(this._pswobj.pswminlength) == 0) {
      this.showMsg("Minimum length must be greater than 0",false);
    }
    else if (Number(this._pswobj.pswmaxlength) != 0 && Number(this._pswobj.pswminlength) > Number(this._pswobj.pswmaxlength)) {
      this.showMsg("Minimum length must not be greater than maximum length",false);
    }
    /* else if (Number(this._pswobj.pswminlength) < total) {
      this.showMsg("Minimum length must be greater or equal to " + total,false);
    }
    else if (Number(this._pswobj.pswmaxlength) < total) {
      this.showMsg("Maximum length must be less or equal to " + total,false);
    } */
    else if (this._pswobj.pswminlength > total) {
      this._mflag = true;
      this.showMsg("Password length must be greater than Minimum length", false);
    } 
    else if (this._pswobj.pswmaxlength < total) {
      this._mflag = true;
      this.showMsg("password length must be less than Maximum length", false);
    }
    else
      this.goSave();
  }

  // about session validation
  sessionTimeoutMsg = "";
  _sessionObj = this.getSessionObj();
  getSessionObj() {
    return { "sessionID": "", "userID": "" };
  }

  checkSession() {
    try {
      let url: string = this.ics._apiurl + 'service001/checkSessionTime';

      this._sessionObj.sessionID = this.ics._profile.sessionID;
      this._sessionObj.userID = this.ics._profile.userID;
      let json: any = this._sessionObj;

      this.http.doPost(url, json).subscribe(
        data => {
          if (data != null) {
            if (data.code == "0016") {
              this.showMsgAlert(data.desc);
              this.logout();
            }

            if (data.code == "0014") {
              this.showMsgAlert(data.desc);
            }
          }
        },
        error => {
          if (error._body.type == "error") {
            alert("Connection Timed Out.");
          }
        },
        () => { }
      );
    } catch (e) {
      alert("Invalid URL.");
    }
  }

  showMsgAlert(msg) {
    this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
  }

  logout() {
    this._router.navigate(['/login']);
  }
  showMsg(msg, bool) {
    if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
    if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
    if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
  }
}
