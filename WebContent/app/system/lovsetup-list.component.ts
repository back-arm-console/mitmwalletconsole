import { Component, Input, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { enableProdMode } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
// RP Framework
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpInputComponent } from '../framework/rp-input.component';
import { RpHttpService } from '../framework/rp-http.service';
import { RpBean } from '../framework/rp-bean';
declare var jQuery: any;
// Application Specific
import { AdminPager } from '../util/adminpager.component';
import { ClientUtil } from '../util/rp-client.util';
import { CompilerConfig } from '@angular/compiler';
declare var jQuery: any;
// Application Specific
@Component({
  selector: 'lovsetup-list',
  template: ` 
  <div class="container-fluid">
	<form class="form-horizontal">
		<legend>List of Values</legend>
		<div class="cardview list-height">
			<div class="row col-md-12">
				<div class="row col-md-3">
					<div  class="input-group">
						<span class="input-group-btn input-md">
							<button class="btn btn-sm btn-primary" type="button" (click)="goNew();">New</button>
						</span> 
						<input id="textinput" name="textinput" type="text"  placeholder="Search" [(ngModel)]="_list.searchText" (keyup)="searchKeyup($event)" [ngModelOptions]="{standalone: true}" maxlength="30" class="form-control input-sm">
						<span class="input-group-btn input-md">
							<button class="btn btn-sm btn-primary input-md " type="button" (click)="Searching()" >
								<span class="glyphicon glyphicon-search"></span>Search
							</button>
						</span>        
					</div> 
				</div>		
          <div class="pagerright">
					    <adminpager rpPageSizeMax="100" [(rpModel)]="_list.totalCount" (rpChanged)="changedPager($event)" ></adminpager> 
					</div>
			</div>
					<!--<div *ngIf="_shownull!=false" style="color:#ccc;">No result found!</div> 
          <div *ngIf="_showListing">-->
          <div class="row col-md-12" style="overflow-x:auto">
						<table class="table table-striped table-condensed table-hover tblborder" id="tblstyle"><!--style="width: 750px"-->
						  <thead>
							<tr>
							  <th class="left" >Code</th>
							  <th class="left">Description</th>   
							</tr>
						  </thead>
						  <tbody>
							<tr *ngFor="let obj of  _list.data">
							  <td><a (click)="goto(obj.sysKey)">{{obj.lovNo}}</a></td>
							  <td>{{obj.lovDesc2}}</td>                                      
							</tr> 
						  </tbody>
						</table>
					</div>
					<!--<div align="center" *ngIf="_showListing">
					   Total {{_list.totalCount}}
					</div>-->			
			</div>	
	</form>
</div> 
  <div [hidden] = "_mflag">
    <div  id="loader" class="modal" ></div>
  </div>   
   `
})

export class LOVSetupListComponent {

  subscription: Subscription;
  _col_list = [{ "col_no": "1", "flag": false }, { "col_no": "2", "flag": false }]
  _sorting = { _sort_type: "asc", _sort_col: "1" };
  _mflag = true;
  _util: ClientUtil = new ClientUtil();
  sessionAlertMsg = "";
  _totalCount = 1;
  _shownull=false;
  _showListing = false;
  _msghide: boolean;
  _recordhide: boolean;
  _list = { "data": [{ "sysKey": 0, "lovNo": "", "lovDesc2": "", "lovType": "" }], "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0 };
  _pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 1 };

  constructor(private ics: RpIntercomService, private _router: Router, private http: RpHttpService) {
    // RP Framework
    this.subscription = ics.rpbean$.subscribe(x => { })

    if (!ics.getRole() || ics.getRole() == 0) {
      this._router.navigate(['/login']);
    } else {
      this._mflag = false;
      this.search();
    }
  }

  // list sorting part
  changeDefault() {
    for (let i = 0; i < this._col_list.length; i++) {
      this._col_list[i].flag = false;
    }
  }

  addSort(e) {
    for (let i = 0; i < this._col_list.length; i++) {
      if (this._col_list[i].col_no == e) {
        let _tmp_flag = this._col_list[i].flag;
        this.changeDefault();
        this._col_list[i].flag = !_tmp_flag;
        this._sorting._sort_type = !_tmp_flag ? "desc" : "asc";
        this._sorting._sort_col = e;
        break;
      }
    }
  }

  changedPager(event) {
    if (this._list.totalCount != 0) {
      this._pgobj = event;
      let current = this._list.currentPage;
      let size = this._list.pageSize;
      this._list.currentPage = this._pgobj.current;
      this._list.pageSize = this._pgobj.size;

      if (this._pgobj.current != current || this._pgobj.size != size) {
        this.search();
      }
    }
  }

  search() {
    this._mflag = false;

    try {
      let url: string = this.ics._apiurl + 'service001/lovSetuplist';
      let json: any = this._list;
      console.log(JSON.stringify(url));
      console.log(JSON.stringify(json));
      
      this.http.doPost(url, json).subscribe(
        res => {
          this._mflag = false;
          this._shownull=false;
          if (res.msgCode == '0016') {
            this.sessionAlertMsg = res.msgDesc;
            this.showMessage(res.msgDesc,false);
          } else {
            if (res != null) {
              this._totalCount = res.totalCount;
              this._list = res;

              if (this._totalCount == 0) {
                //this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Data not found!" });
                this.showMessage("Data not found!",false);
              }

              if (res.data != null) {
                if (!(res.data instanceof Array)) {
                  /* let m = [];
                  m[0] = response.commdetailArr;
                  this._flexobj.commdetailArr = m;
                  this._flexobj.totalCount = response.totalCount; */
                  let m = [];
                  m[0] = res.data;
                  this._list.data = m;
                  this._list.totalCount = res.totalCount;
                } else {
                  this._list = res;
                }
              }
              this._showListing = (this._list!= undefined);
            }
            else{
              this._shownull=true;
            }
          }

          this._mflag = true;
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
        },
        () => { }
      );
    } catch (e) {
      alert(e);
    }
  }  

  searchKeyup(e: any) {
    if (e.which == 13) { // check enter key
      this._list.currentPage = 1;
      this._msghide = true;
      this._recordhide = false;
      this.search();
    }
  }

  Searching() {
    this._list.currentPage = 1;
    this._msghide = true;
    this._recordhide = false;
    this.search();
  }

  goto(p) {
    this._router.navigate(['/QuickPayLOVSetup', 'read', p]);
  }

  //showMessage() {
  //  jQuery("#sessionalert").modal();
   // Observable.timer(3000).subscribe(x => {
   //   this.goLogOut();
  //  });
 // }

  goNew() {
    this._router.navigate(['/QuickPayLOVSetup', 'new']);
  }

  goLogOut() {
    jQuery("#sessionalert").modal('hide');
    this._router.navigate(['/login']);
  }
  showMessage(msg, bool) {
    if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
    if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
    if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "primary", "t3": msg }); }
}
  

}