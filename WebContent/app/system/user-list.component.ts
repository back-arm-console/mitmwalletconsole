import { Component, enableProdMode } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { RpHttpService } from '../framework/rp-http.service';
// RP Framework
import { RpIntercomService } from '../framework/rp-intercom.service';
declare var jQuery: any;
enableProdMode();
@Component({
    selector: 'UserList',
    template: ` 
    <div class="container col-md-12 col-sm-12 col-xs-12">
    <form class="form-horizontal"> 
    <!-- Form Name -->
    <legend>User List</legend>
    <div class="cardview list-height">
        <div class="row col-md-12">
            <div class="row col-md-3">
                <div  class="input-group">
                    <span class="input-group-btn"  style="width:20px;">
                        <button class="btn btn-sm btn-primary" type="button" (click)="goNew();">New</button>
                    </span> 
                    <input id="textinput" name="textinput" type="text"  placeholder="Search" [(ngModel)]="_userobj.searchText"  maxlength="50" class="form-control input-sm" (keyup)="searchKeyup($event)" [ngModelOptions]="{standalone: true}">
                    <span class="input-group-btn">
                        <button class="btn btn-sm btn-primary" type="button" (click)="Searching()" >
                            <span class="glyphicon glyphicon-search"></span>Search
                        </button>
                    </span>        
                </div>  
            </div>
            <div class="pagerright">            
                <adminpager rpPageSizeMax="100" [(rpModel)]="_userobj.totalCount" (rpChanged)="changedPager($event)" ></adminpager> 
            </div>
        </div>
         <div *ngIf="_showListing">
         <table class="table table-striped table-condensed table-hover tblborder">
            <thead>
                <tr>
                    <th class="left">ID</th>
                    <th class="left">Name</th>
                    <th class="left">Status</th>          
                </tr>
            </thead>
            <tbody>
                <tr *ngFor="let obj of _userobj.data">
                <td><a (click)="goto(obj.syskey)">{{obj.t1}}</a></td>
                <td>{{obj.username}}</td>
                <td *ngIf="obj.recordStatus == 1 " >Save</td>
                <td *ngIf="obj.recordStatus == 2 && obj.n7 == 0 " >Activate</td>
                <td *ngIf="obj.recordStatus == 21 " >Deactivate</td>
                <td *ngIf="obj.recordStatus == 2 && obj.n7 == 11 " >Lock</td>
                </tr>  
            </tbody>
        </table>
        </div>
    <div align="center" *ngIf="_showListing">
       Total {{_userobj.totalCount}}
  </div>
</div>
</form>
</div> 
<div [hidden] = "_mflag">
<div  id="loader" class="modal" ></div>
</div>
  
         `
})
export class UserListComponent {
    _key = "";
    loader: boolean;
    _mflag = true;
    confirmpwd = "";
    _showListing=false;
    _sessionMsg = "";
    subscription: Subscription;
    _obj = {
        "refkey": "", "refsvytype": "", "hub": "", "division": "",
        "township": "", "branch": "", "branchno": "", "villagename": "", "gpscoordinate": "",
        "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": ""
    };
    _priobj = {
        "data": [{ "refkey": "", "type": "", "hub": "", "division": "", "township": "", "branch": "", "period": "" },
        { "refkey": "", "type": "", "hub": "", "division": "", "township": "", "branch": "", "period": "" }]
    };
    _returnResult = { "keyResult": 0, "longResult": "", "state": "", "stringResult": "", "userId": "", "msgCode": "", "msgDesc": "" };
    _userobj = {
        "data": [{ "syskey": 0, "autokey": 0, "createdDate": "", "modifiedDate": "", "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "parentId": 0, "usersyskey": 0, "t1": "", "username": "", "n2": "" }],
        "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0
    };
    _pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 0 };

    constructor(private ics: RpIntercomService, private _router: Router, private http: RpHttpService) {
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(x => { })
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        } else {
            this.loader = true;
            //_mflag = false;
            this._mflag = false;
            this.search();
        }
    }

    changedPager(event) {
        if (this._userobj.totalCount != 0) {
            this._pgobj = event;
            let current = this._userobj.currentPage;
            let size = this._userobj.pageSize;
            this._userobj.currentPage = this._pgobj.current;
            this._userobj.pageSize = this._pgobj.size;
            if (this._pgobj.current != current || this._pgobj.size != size)
                this.search();
        }

    }

    search() {
        try {
            this._mflag = false;
            this.http.doGet(this.ics._apiurl + 'service001/getUserListing?searchVal=' + encodeURIComponent(this._userobj.searchText) + '&pagesize=' + this._userobj.pageSize + '&currentpage=' + this._userobj.currentPage + '&operation=allbankuser' + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(
                response => {
                    if (response.msgCode == '0016') {
                        this._sessionMsg = response.msgDesc;
                        this.showMessage(response.msgDesc,false);
                        this._mflag  = true;
                    }
                    if (response.data != null && response.data != undefined) {
                        if (!(response.data instanceof Array)) {
                            let m = [];
                            m[0] = response.data;
                            this._userobj.data = m;
                            this._userobj.totalCount = response.totalCount;
                        }
                        else {
                            this._userobj = response;
                            if (response.totalCount == 0) {
                                this.showMessage("Data not found!",false);
                            }
                        }
                        this._showListing = (this._userobj!= undefined);
                    }
                    this._mflag = true;
                },
                error => {
                    if (error._body.type == 'error') {
                        alert("Connection Timed Out!");
                    }
                },
                () => { }
            );
        } catch (e) {
            alert(e);
        }

    }

    //showMessage() {
    //    jQuery("#sessionalert").modal();
    //    Observable.timer(3000).subscribe(x => {
    //       this._router.navigate(['Login', , { p1: '*' }]);
    //        jQuery("#sessionalert").modal('hide');
    //    });
    //}


    Searching() {
        this._userobj.currentPage = 1;
        this.search();
    }

  /*   searchKeyup(e: any) {

        if (e.which == 13) { // check enter key
            this._userobj.currentPage = 1;
            this.search();
        }
    } */

    searchKeyup(e:any){
        if(e.which == 13) {
       this._userobj.currentPage =1;            
       this.search();
      }                   
      }

    goNew() {
        this._router.navigate(['user-setup', , { cmd: "NEW" }]);
    }

    goto(p) {
        this._router.navigate(['/user-setup', 'read', p]);
    }
    showMessage(msg, bool) {
        if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
        if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
        if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "primary", "t3": msg }); }
    }

}