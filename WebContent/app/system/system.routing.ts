import { ModuleWithProviders }   from '@angular/core';
import { Routes, RouterModule }  from '@angular/router';

import {UserSetupComponent} from './usersetup.component';
import { UserListComponent } from './user-list.component';
import { VersionHistoryComponent } from './versionhistory.component';
import { VersionHistoryListComponent }  from './versionhistory-list.component';
import { PasswordPolicyComponent }  from './passwordpolicysetup.component';
import { ForceSignOutComponent } from './forcesignout.component';
import { MenuSetupComponent } from './menusetup.component'; 
import { MenuSetupListComponent } from './menusetup-list.component'; 
import { RoleSetupComponent } from './rolesetup.component'; 
import {RoleSetupListComponent} from './rolesetup-list.component';
import { LovSetupComponent }  from './lovsetup.component';
import { LOVSetupListComponent } from './lovsetup-list.component';
import { FAQMenu } from './faq-menu.component';
import { FAQList } from './faq-list.component';
import {LocatorSetupComponent} from './locatorsetup.component';
import {LocatorSetupListComponent} from './locatorsetup-list.component';

const systemRoutes: Routes = [ 
  { path: 'password-policy',  component: PasswordPolicyComponent }, 
  { path: 'forced-sign-out', component: ForceSignOutComponent },
  { path: 'QuickPayLOVSetup',  component: LovSetupComponent }, 
  { path: 'QuickPayLOVSetup/:cmd', component: LovSetupComponent }, 
  { path: 'QuickPayLOVSetup/:cmd/:id', component: LovSetupComponent }, 
  { path: 'lovsetuplist',  component: LOVSetupListComponent },
  { path: 'version-history', component: VersionHistoryComponent },
  { path: 'version-history/:cmd', component: VersionHistoryComponent }, 
  { path: 'version-history/:cmd/:id', component: VersionHistoryComponent },
  { path: 'version-history-list', component: VersionHistoryListComponent },
  { path: 'user-setup', component: UserSetupComponent}, 
  { path: 'user-setup/:cmd', component: UserSetupComponent}, 
  { path: 'user-setup/:cmd/:id', component: UserSetupComponent}, 
  { path: 'user-list', component: UserListComponent}, 
  { path: 'Menu',  component: MenuSetupComponent },  
  { path: 'Menu/:cmd', component: MenuSetupComponent }, 
  { path: 'Menu/:cmd/:id', component: MenuSetupComponent },
  { path: 'menusetup-list', component: MenuSetupListComponent},  
  { path: 'Role',  component: RoleSetupComponent },  
  { path: 'Role/:cmd', component: RoleSetupComponent }, 
  { path: 'Role/:cmd/:id', component: RoleSetupComponent },
  { path: 'rolesetup-list', component: RoleSetupListComponent},  
  

  { path: 'faq-menu',  component: FAQMenu },  
  { path: 'faq-menu/:cmd', component: FAQMenu }, 
  { path: 'faq-menu/:cmd/:id', component: FAQMenu },
  { path: 'faq-list', component: FAQList},

  { path: 'locator-setup',  component: LocatorSetupComponent },  
  { path: 'locator-setup/:cmd', component: LocatorSetupComponent }, 
  { path: 'locator-setup/:cmd/:id', component: LocatorSetupComponent },
  { path: 'LocatorList', component: LocatorSetupListComponent}, 
  
];

export const SystemRouting: ModuleWithProviders = RouterModule.forChild(systemRoutes);
