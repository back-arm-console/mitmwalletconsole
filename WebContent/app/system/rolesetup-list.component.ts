import { Component, enableProdMode } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { RpHttpService } from '../framework/rp-http.service';
// RP Framework
import { RpIntercomService } from '../framework/rp-intercom.service';
import { ClientUtil } from '../util/rp-client.util';
declare var jQuery: any;
enableProdMode();
@Component({
    selector: 'rolesetup-list',
    template: ` 
    <div class="container-fluid">
	<form class="form-horizontal">
		<legend>Role List</legend>
		<div class="cardview list-height">
			<div class="row col-md-12">
            <div class="row col-md-3">
                <div  class="input-group">
                    <span class="input-group-btn"  style="width:20px;">
                        <button class="btn btn-sm btn-primary" type="button" (click)="goNew();">New</button>
                    </span> 
                    <input id="textinput" name="textinput" type="text"  placeholder="Search" [(ngModel)]="_roleobj.searchText"  maxlength="50" class="form-control input-sm" (keyup)="searchKeyup($event)" [ngModelOptions]="{standalone: true}">
                    <span class="input-group-btn">
                        <button class="btn btn-sm btn-primary" type="button" (click)="Searching()" >
                            <span class="glyphicon glyphicon-search"></span>Search
                        </button>
                    </span>        
                </div>  
            </div>
                <div class="pagerright">
					<adminpager rpPageSizeMax="100" [(rpModel)]="_roleobj.totalCount" (rpChanged)="changedPager($event)" ></adminpager> 
				</div> 
			</div>
			  <div *ngIf="_showListing">
				  <table class="table table-striped table-condensed table-hover tblborder">
					<thead>
					 <tr>
						<th class="left">Code</th>
						<th class="left">Description</th>   
					</tr>
				  </thead>
				  <tbody>
					<tr *ngFor="let obj of _roleobj.data">
						<td><a (click)="goto(obj.syskey)">{{obj.t1}}</a></td>
						<td>{{obj.t2}}</td>
					</tr>  
				  </tbody>
				  </table>
			  </div>
			<div align="center" *ngIf="_showListing">
			   Total {{_roleobj.totalCount}}
			</div>
		</div> 
	</form>
</div>
    <div [hidden] = "_mflag">
        <div  id="loader" class="modal" ></div>
    </div>
   `
})

export class RoleSetupListComponent {
    // RP Framework 
    subscription: Subscription;
    // Application Specific
    _searchVal = ""; // simple search
    _showListing = false;
    _flagas = true; // flag advance search
    _col_list = [{ "col_no": "1", "flag": false }, { "col_no": "2", "flag": false }]
    _sorting = { _sort_type: "asc", _sort_col: "1" };
    _mflag = true;
    _util: ClientUtil = new ClientUtil();
    sessionAlertMsg = ""; _recordhide: boolean; _msghide: boolean;
    _roleobj = { "data": [{ "syskey": 0, "autokey": 0, "createdDate": "", "modifiedDate": "", "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "usersyskey": 0, "t1": "", "t2": "", "t3": "", "n1": 0, "n2": 0, "n3": 0 }], "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0, "msgCode": "", "msgDesc": "" };
    _pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 0 };

    constructor(private ics: RpIntercomService, private _router: Router, private http: RpHttpService) {
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(x => { })
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        } else {
            this._mflag = false;
            this.search();
        }
    }
    // list sorting part
    changeDefault() {
        for (let i = 0; i < this._col_list.length; i++) {
            this._col_list[i].flag = false;
        }
    }
    addSort(e) {
        for (let i = 0; i < this._col_list.length; i++) {
            if (this._col_list[i].col_no == e) {
                let _tmp_flag = this._col_list[i].flag;
                this.changeDefault();
                this._col_list[i].flag = !_tmp_flag;
                this._sorting._sort_type = !_tmp_flag ? "desc" : "asc";
                this._sorting._sort_col = e;
                break;
            }
        }
    }

    changedPager(event) {

        if (this._roleobj.totalCount != 0) {
            this._pgobj = event;
            let current = this._roleobj.currentPage;
            let size = this._roleobj.pageSize;
            this._roleobj.currentPage = this._pgobj.current;
            this._roleobj.pageSize = this._pgobj.size;
            if (this._pgobj.current != current || this._pgobj.size != size) {
                this.search();
            }
        }
    }

    search() {
        try {
            this._mflag = false;
            let url = this.ics._apiurl + 'service001/getRoleList?searchVal=' + encodeURIComponent(this._roleobj.searchText) + '&pagesize=' + this._roleobj.pageSize + '&currentpage=' + this._roleobj.currentPage + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;            
            this.http.doGet(url).subscribe(
                response => {
                    if (response.msgCode == '0016') {
                        this.sessionAlertMsg = response.msgDesc;
                        this.showMessage(response.msgDesc,false);
                    }
                    else {
                        if (response.totalCount == 0) {
                            this._roleobj = response,
                                this.showMessage("Data not found!",false);
                        }
                        else if (response.totalCount == 1) {
                            let m = [];
                            m[0] = response.data;
                            this._roleobj.data = m[0];
                            this._roleobj.totalCount = response.totalCount;
                        }
                        else {
                            this._roleobj = response;
                        }
                        this._showListing = (this._roleobj!= undefined);
                    }
                    this._mflag = true;
                },
                error => {
                    if (error._body.type == 'error') {
                        alert("Connection Timed Out!");
                    }
                    else {

                    }
                }, () => { });
        } catch (e) {
            alert(e);
        }



    }

    searchKeyup(e: any) {

        if (e.which == 13) { // check enter key
            this._roleobj.currentPage = 1;
            this._msghide = true;
            this._recordhide = false;
            this.search();
        }

    }

    Searching() {
        this._roleobj.currentPage = 1;
        this._msghide = true;
        this._recordhide = false;
        this.search();
    }
    goNew() {
        this._router.navigate(['/Role', 'new']);
    }

    goto(p) {
        this._router.navigate(['/Role', 'read', p]);
    }

    //showMessage() {
    //    jQuery("#sessionalert").modal();
    //    Observable.timer(3000).subscribe(x => {
    //        this.goLogOut();
    //    });
    //}

    goLogOut() {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    }
    showMessage(msg, bool) {
        if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
        if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
        if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "primary", "t3": msg }); }
    }
}