import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { enableProdMode } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpInputComponent } from '../framework/rp-input.component';
import { RpHttpService } from '../framework/rp-http.service';
import { RpBean } from '../framework/rp-bean';
import { RpReferences } from '../framework/rp-references';
import { ClientUtil } from '../util/rp-client.util';
import { IMyDpOptions } from 'mydatepicker';
import { Observable } from 'rxjs/Rx';
declare var jQuery: any;
enableProdMode();
@Component({
  selector: 'version-history',
  template: `
  <div class="container-fluid">
  <div class="row clearfix">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
	<form class="form-horizontal" (ngSubmit) = "goSave()">
		<legend>Version History</legend>
		<div class="cardview list-height">
            <div class="row col-md-12">
                <button class="btn btn-sm btn-primary" id="vh_list_btn" type="button" (click)="goList()">List</button>
                <button class="btn btn-sm btn-primary" id="vh_new_btn" type="button" (click)="goNew()">New</button>
                <button class="btn btn-sm btn-primary" id="vh_save_btn" type="submit">{{_btntext}}</button>
                <button class="btn btn-sm btn-primary" disabled id="vh_delete_btn" type="button" (click)="goDelete()">Delete</button>
            </div>

            <div class="row col-md-12">&nbsp;</div>
            <div class="row col-md-12" id="custom-form-alignment-margin">

              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-4">App Code <font color="#FF0000">*</font></label>
                  <div class="col-md-8">
                      <select *ngIf="_key==''" [(ngModel)]="_obj.appcode" class="form-control input-sm col-md-0" [ngModelOptions]="{standalone: true}" required>
                        <option *ngFor="let item of ref._lov3.refAppCode" value="{{item.value}}">{{item.caption}}</option>
                      </select>

                      <select *ngIf="_key!=''" [(ngModel)]="_obj.appcode" class="form-control input-sm col-md-0" [ngModelOptions]="{standalone: true}" disabled>
                        <option *ngFor="let item of ref._lov3.refAppCode" value="{{item.value}}">{{item.caption}}</option>
                      </select>
                  </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4">Version Key </label>
                    <div class="col-md-8">
                        <input disabled readonly type="text" [(ngModel)]="_obj.versionkey" class="form-control input-sm" [ngModelOptions]="{standalone: true}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4">Version <font color="#FF0000">*</font></label>
                    <div class="col-md-8">
                        <input class="form-control input-sm" type="text" [(ngModel)]="_obj.version" required="true" maxlength="20" [ngModelOptions]="{standalone: true}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4">Version Title </label>
                    <div class="col-md-8">
                        <input class="form-control input-sm" type="text" [(ngModel)]="_obj.versiontitle" maxlength="50" [ngModelOptions]="{standalone: true}">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-4">Description </label>
                    <div class="col-md-8">
                        <textarea class="form-control input-sm" rows="3" [(ngModel)]="_obj.description" maxlength="500" [ngModelOptions]="{standalone: true}"></textarea>
                    </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-4">Start Date <font color="#FF0000">*</font></label>
                    <div class="col-md-8">
                      <my-date-picker name="mystartdate" [options]="myDatePickerOptions" [(ngModel)]="_dates.startdate" ngDefaultControl></my-date-picker>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4">Due Date <font color="#FF0000">*</font></label>
                    <div class="col-md-8">
                      <my-date-picker name="myduedate" [options]="myDatePickerOptions" [(ngModel)]="_dates.duedate" ngDefaultControl></my-date-picker>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4">Remark </label>
                    <div class="col-md-8">
                        <textarea id="textarea-custom" class="form-control input-sm" rows="3" [(ngModel)]="_obj.remark" [ngModelOptions]="{standalone: true}"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4">Status <font color="#FF0000">*</font></label>
                    <div class="col-md-8">
                        <select [(ngModel)]="_obj.status" class="form-control input-sm col-md-0" [ngModelOptions]="{standalone: true}" required>
                          <option *ngFor="let item of ref._lov3.refVersionStatusCode" value="{{item.value}}">{{item.caption}}</option>
                        </select>
                    </div>
                </div>
              </div>
            </div>
		  </div>
  </form>
</div>
</div>
</div>

    <div [hidden]="_mflag">
      <div  id="loader" class="modal" ></div>
    </div>
  `
})

export class VersionHistoryComponent {

  _btntext = "";
  _output1 = "";
  _message = "";
  _key = "";
  _versionKey = "";
  _mflag :boolean;

  // to get general methods
  _util: ClientUtil = new ClientUtil();

  // about subscription
  subscription: Subscription;
  sub: any;

  // about date picker
  today = this._util.getTodayDate();
  myDatePickerOptions: IMyDpOptions = this._util.getDatePicker();
  dateobj: { date: { year: number, month: number, day: number } };
  _dates = { "startdate": this.dateobj, "duedate": this.dateobj };

  //about default obj
  _obj = this.getDefaultObj();
  getDefaultObj() {
    return {
      "sessionID": "", "userID": "", "msgCode": "", "msgDesc": "", "state": "",
      "autokey": "", "appcode": "", "versionkey": "", "version": "", "versiontitle": "",
      "description": "", "startdate": "", "duedate": "", "remark": "", "status": "",
      "t1": "", "t2": "", "t3": "", "n1": "", "n2": "", "n3": ""
    };
  }

  constructor(private ics: RpIntercomService, private _router: Router, private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences) {
    this.subscription = ics.rpbean$.subscribe(x => { });

    if (!ics.getRole() || ics.getRole() == 0) {
      this._router.navigate(['/login']);
    } else {
      this.checkSession();

      this._btntext = "Save";
      jQuery("#vh_delete_btn").prop("disabled", true);

      this._obj = this.getDefaultObj();
      this.setTodayDateObj();
      this.setDateObjIntoString();

      this.getAppCodes();
      this.getStatusCodes();
    }
  }

  // about session validation
  sessionTimeoutMsg = "";
  _sessionObj = this.getSessionObj();
  getSessionObj() {
    return { "sessionID": "", "userID": "" };
  }

  checkSession() {
    try {
      let url: string = this.ics._apiurl + 'service001/checkSessionTime';

      this._sessionObj.sessionID = this.ics._profile.sessionID;
      this._sessionObj.userID = this.ics._profile.userID;
      let json: any = this._sessionObj;

      this.http.doPost(url, json).subscribe(
        data => {
          if (data != null) {
            if (data.code == "0016") {
              this.showMsgAlert(data.desc);
              this.logout();
            }

            if (data.code == "0014") {
              this.showMsgAlert(data.desc);
            }
          }
        },
        error => {
          if (error._body.type == "error") {
            alert("Connection Timed Out.");
          }
        },
        () => { }
      );
    } catch (e) {
      alert("Invalid URL.");
    }
  }

  showMsgAlert(msg) {
    this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
  }

  logout() {
    this._router.navigate(['/login']);
  }

  setTodayDateObj() {
    this._dates.startdate = this._util.changestringtodateobject(this.today);
    this._dates.duedate = this._util.changestringtodateobject(this.today);
  }

  setDateObjIntoString() {
    this._obj.startdate = this._util.getDatePickerDate(this._dates.startdate);
    this._obj.duedate = this._util.getDatePickerDate(this._dates.duedate);
  }

  getAppCodes() {
    try {
      let url: string = this.ics.cmsurl + 'serviceAdmLov/getAppCodesForVersionHistory';

      this._sessionObj.sessionID = this.ics._profile.sessionID;
      this._sessionObj.userID = this.ics._profile.userID;
      let json: any = this._sessionObj;

      this.http.doPost(url, json).subscribe(
        data => {
          if (data != null) {
            if (data.msgCode == "0016") {
              this.showMsg(data.msgDesc,false);
              this.logout();
            }

            if (data.msgCode != "0000") {
              this.showMsg(data.msgDesc,false);
            }

            if (data.msgCode == "0000") {
              if (!(data.refAppCode instanceof Array)) {
                let m = [];
                m[0] = data.refAppCode;
                this.ref._lov3.refAppCode = m;
              } else {
                this.ref._lov3.refAppCode = data.refAppCode;
              }
              this._obj.appcode = this.ref._lov3.refAppCode[0].value;
            }
          }
        },
        error => {
          if (error._body.type == "error") {
            alert("Connection Timed Out.");
          }
        },
        () => { }
      );
    } catch (e) {
      alert("Invalid URL.");
    }
  }

  getStatusCodes() {
    try {
      let url: string = this.ics.cmsurl + 'serviceAdmLOV/getStatusCodesForVersionHistory';

      this._sessionObj.sessionID = this.ics._profile.sessionID;
      this._sessionObj.userID = this.ics._profile.userID;
      let json: any = this._sessionObj;

      this.http.doPost(url, json).subscribe(
        data => {
          this._mflag = true;

          if (data != null) {
            if (data.msgCode == "0016") {
              this.showMsg(data.msgDesc,false);
              this.logout();
            }

            if (data.msgCode != "0000") {
              this.showMsg(data.msgDesc,false);
            }

            if (data.msgCode == "0000") {
              if (!(data.refVersionStatusCode instanceof Array)) {
                let m = [];
                m[0] = data.refVersionStatusCode;
                this.ref._lov3.refVersionStatusCode = m;
              } else {
                this.ref._lov3.refVersionStatusCode = data.refVersionStatusCode;
              }
              this._obj.status = this.ref._lov3.refVersionStatusCode[0].value;
            }
          }
        },
        error => {
          if (error._body.type == "error") {
            alert("Connection Timed Out.");
          }
        },
        () => { }
      );
    } catch (e) {
      alert("Invalid URL.");
    }
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(
      params => {
        let cmd = params['cmd'];
        if (cmd != null && cmd != "" && cmd == "read") {
          this._mflag = false;
          //this._btntext = "Update";
          jQuery("#vh_delete_btn").prop("disabled", false);
          let id = params['id'];
          this._key = id;
          this.goReadById(this._key);
        }
      }
    );
  }

  goReadById(_autokeyid) {
    try {
      this._mflag = false;
      let url: string = this.ics.cmsurl + 'service001/getOneVersionHistoryByAutoKey';

      this._obj.sessionID = this.ics._profile.sessionID;
      this._obj.userID = this.ics._profile.userID;
      this._obj.autokey = _autokeyid;
      let json: any = this._obj;

      this.http.doPost(url, json).subscribe(
        data => {
          this._mflag = true;

          let temp = this._obj;

          if (data != null) {
            this._obj = data;

            if (this._obj.msgCode == "0016") {
              this.showMsg(this._obj.msgDesc,false);
              this.logout();
            }

            if (this._obj.msgCode != "0000") {
              this.showMsg(this._obj.msgDesc,false);
              this._obj = temp;
            }

            if (this._obj.msgCode == "0000") {
              jQuery("#vh_delete_btn").prop("disabled", false);
              //this._btntext = "Update";

              this._dates.startdate = this._util.changestringtodateobj(this._obj.startdate);
              this._dates.duedate = this._util.changestringtodateobj(this._obj.duedate);
              this.getAppCodes();
              this.getStatusCodes();
            }
          }
        },
        error => {
          if (error._body.type == "error") {
            alert("Connection Timed Out.");
          }
        },
        () => { }
      );
    } catch (e) {
      alert("Invalid URL.");
    }
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  goSave() {
    this.setDateObjIntoString();

    if (this._obj.appcode == "undefined" || this._obj.appcode.trim() == "") {
      this.showMsg("Blank App Code.",false);
    } else if (this._obj.version == "undefined" || this._obj.version.trim() == "") {
      this.showMsg("Blank Version.",false);
    } else if (this._obj.version.trim().length > 20) {
      this.showMsg("Invalid Version. Its Maximum Character Is 20.",false);
    } else if (this._obj.versiontitle.trim().length > 50) {
      this.showMsg("Invalid Version Title. Its Maximum Character Is 50.",false);
    } else if (this._obj.description.trim().length > 500) {
      this.showMsg("Invalid Description. Its Maximum Character is 500.",false);
    } else if (this._obj.startdate == "undefined" || this._obj.startdate.trim() == "") {
      this.showMsg("Blank Start Date.",false);
    } else if (this._obj.startdate.trim().length != 0 && !/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/.test(this._obj.startdate)) {
      this.showMsg("Invalid Start Date.",false);
    } else if (this._obj.duedate == "undefined" || this._obj.duedate.trim() == "") {
      this.showMsg("Blank Due Date.",false);
    } else if (this._obj.duedate.trim().length != 0 && !/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/.test(this._obj.duedate)) {
      this.showMsg("Invalid Due Date.",false);
    } else if (this._obj.status == "undefined" || this._obj.status.trim() == "") {
      this.showMsg("Blank Status.",false);
    } else {
      this._mflag = false;

      try {
        let url: string = this.ics.cmsurl + 'service001/saveVersionHistory';

        this._obj.sessionID = this.ics._profile.sessionID;
        this._obj.userID = this.ics._profile.userID;
        let json: any = this._obj;

        this.http.doPost(url, json).subscribe(
          data => {
            this._mflag = true;

            let temp: any = this._obj;

            this._output1 = JSON.stringify(data);

            if (data != null) {
              this._obj = data;

              if (this._obj.msgCode == "0016") {
                this.showMsg(this._obj.msgDesc,false);
                this.logout();
              }

              if (this._obj.msgCode != "0000") {
                this.showMsg(this._obj.msgDesc,false);
                this._obj = temp;
              }

              if (this._obj.msgCode == "0000") {
                jQuery("#vh_delete_btn").prop("disabled", false);
                //this._btntext = "Update";

                this._key = this._obj.autokey;
                this._versionKey = this._obj.versionkey;
                this._message = this._obj.msgDesc;

                this._obj = temp;
                this._obj.autokey = this._key;
                this._obj.versionkey = this._versionKey;
                this.showMsg(this._message,true);
              }
            }
          },
          error => {
            if (error._body.type == "error") {
              alert("Connection Timed Out.");
            }
          },
          () => { }
        );
      } catch (e) {
        alert("Invalid URL.");
      }
    }
  }

  goList() {
    this._router.navigate(['/version-history-list']);
  }

  goNew() {    
    this._btntext = "Save";
    jQuery("#vh_delete_btn").prop("disabled", true);
    this.clear();
    this.getAppCodes();
    this.getStatusCodes();
  }

  clear() {
    this._output1 = "";
    this._message = "";
    this._key = "";
    this._versionKey = "";
    this._obj = this.getDefaultObj();
    this.today = this._util.getTodayDate();
    this.setTodayDateObj();
    this.setDateObjIntoString();
    this.sessionTimeoutMsg = "";
    this._sessionObj = this.getSessionObj();
  }

  goDelete() {
    this._mflag = false;
    try {
      let url: string = this.ics.cmsurl + 'service001/deleteVersionHistory';

      this._obj.sessionID = this.ics._profile.sessionID;
      this._obj.userID = this.ics._profile.userID;
      let json: any = this._obj;

      this.http.doPost(url, json).subscribe(
        data => {
          this._mflag = true;

          let temp: any = this._obj;

          this._output1 = JSON.stringify(data);

          if (data != null) {
            this._obj = data;

            if (this._obj.msgCode == "0016") {
              this.showMsg(this._obj.msgDesc,false);
              this.logout();
            }

            if (this._obj.msgCode != "0000") {
              this.showMsg(this._obj.msgDesc,false);
              this._obj = temp;
            }

            if (this._obj.msgCode == "0000") {
              this.showMsg(this._obj.msgDesc,true);
              this.goNew();
            }
          }
        },
        error => {
          if (error._body.type == "error") {
            alert("Connection Timed Out.");
          }
        },
        () => { }
      );
    } catch (e) {
      alert("Invalid URL.");
    }
  }
  showMsg(msg, bool) {
    if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
    if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
    if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
  }
}