import { Component, enableProdMode } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { RpHttpService } from '../framework/rp-http.service';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpReferences } from '../framework/rp-references';
import { ClientUtil } from '../util/rp-client.util';
declare var jQuery: any;
declare function isMyanmar(pwd:String) : boolean;
declare function ZgtoUni(pwd:String) : string;
enableProdMode();

@Component({
    selector: 'FAQList',
    template: `
    
    <div *ngIf="!_divexport" class="container-fluid">
    <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
            <form class="form-horizontal">
                <fieldset>
                    <legend>
                        <div style="float:left;display:inline;font-size:18px;margin-top:5px;margin-left:10px;">
                        FAQ List
                        <button id="btnNew" class="btn btn-sm btn-primary" type="button" style="cursor:pointer;height:34px;" (click)="goNew()" title="New">
                        <i id="lblNew" class="glyphicon glyphicon-file" style="padding-left:2px;"></i> 
                        </button>
                        </div>

                        <div style="float:right;text-align:right;margin-top:0px;margin-bottom:5px;">                                
                            <div style="display:inline-block;padding-right:0px;width:280px;">
                                <div class="input-group"> 
                                    <input class="form-control" type="text" id="isInputSearch" placeholder="Search" autocomplete="off" spellcheck="false" [(ngModel)]="_SearchString" [ngModelOptions]="{standalone: true}" (keyup.enter)="filterSearch()">

                                    <span id="btnSimpleSearch" class="input-group-addon">
                                        <i class="glyphicon glyphicon-search" style="cursor: pointer" (click)="filterSearch()"></i>
                                    </span>
                                </div>
                            </div>

                            <!--<button id="btnToggleSearch" class="btn btn-sm btn-secondary" type="button" style="cursor:pointer;text-align:center;margin-top:-24px;height:34px;" (click)="btnToggleSearch_onClick()" title="Collapse">
                                <i id="lblToggleSearch" class="glyphicon glyphicon-menu-down"></i> 
                            </button>-->

                            <button *ngIf="false" id="btnTogglePagination" type="button" class="btn btn-sm btn-default" style="cursor:pointer;text-align:center;margin-left:25px;margin-top:-24px;width:30px;height:34px;" (click)="btnTogglePagination_onClick()" title="Collapse">
                                <i id="lblTogglePagination" class="glyphicon glyphicon-eject" style="color:#2e8690;margin-left:-4px;transform: rotate(90deg)"></i>
                            </button>

                            <div id="divPagination" style="display:none;margin-left:13px;margin-top:-7px;margin-bottom:5px;">
                                <pager6 id="pgPagination" rpPageSizeMax="100" [(rpModel)]="_ListingDataset.totalCount" (rpChanged)="changedPager($event)" style="font-size:14px;text-align:right;margin-top:-7px;margin-right:10px;"></pager6>
                            </div>
                                                 
                        </div>
                    </legend>
					<div class="cardview list-height">
						<advanced-search id="asAdvancedSearch" [FilterList]="_FilterList" [TypeList]="_TypeList" rpVisibleItems=5 (rpHidden)="renderAdvancedSearch($event)" (rpChanged)="filterAdvancedSearch($event)" style="display:none;"></advanced-search>

						<div *ngIf="_shownull!=false" style="color:#ccc;">No result found!</div>

						<div *ngIf="_showListing" class="form-group">
							<div class="col-md-12" style="overflow-x:auto;">
                            <table class="table table-striped table-condense table-hover tblborder">
                                <thead>
                                    <tr>
                                        <th class="right" style="width:1%" title="UserID">No.</th>
                                        <th class="left" style="width:8%" title="UserName">Question by English</th>
                                        <th class="left" style="width:8%" title="Fatehername">Answer by English</th>
                                        <th class="left" style="width:8%"title="NRC">Question by Myanmar</th>
                                        <th class="left" style="width:10%" title="Date of Birth">Answer by Myanmar</th>
                                    </tr>
                                </thead>
    
                                <tbody>
                                    <tr *ngFor="let obj of _ListingDataset.faqData" >
                                        <td class="right"><a (click)="goto(obj.autokey)">{{obj.autokey}}</a></td>
                                        <td class="textwrap uni" title="({{getFullTitleLabel(obj.questionEng)}})">{{obj.questionEng}}</td>                                   
                                        <td class="textwrap uni" title="({{getFullTitleLabel(obj.answerEng)}})">{{obj.answerEng}}</td>
                                        <td class="textwrap uni" title="({{getFullTitleLabel(obj.questionUni)}})">{{obj.questionUni}}</td>
                                        <td class="textwrap uni" title="({{getFullTitleLabel(obj.answerUni)}})">{{obj.answerUni}}</td>
                                    </tr> 
                                </tbody>
                            </table>
							</div>
						</div>

						<div *ngIf="_showListing" style="text-align:center">Total {{ _ListingDataset.totalCount }}</div>
					</div>
                </fieldset>
            </form>

            <div id="ExcelDownload" style="display: none;width: 0px;height: 0px;"></div>

        </div> 
    </div>
</div>

<div [hidden]="_mflag">
<div  id="loader" class="modal" ></div>
</div>

<div *ngIf='_divexport'>
    <button type="button" class="close" (click)="goClosePrint()" style="margin-top:-20px;">&times;</button>
    <iframe id="frame1" [src]="_printUrl" style="width: 100%;height: 95%;"></iframe> 
</div>
  `
})
export class FAQList {
    // RP Framework 
    subscription: Subscription;
    _shownull = false;
    _OperationMode = "";
    _mflag = false;
    _divexport = false;
    //_printUrl: SafeResourceUrl;
   // this.imageLink = this.ics._imglink + "/upload/image/userProfile/";
    _isLoading = true;
    _SearchString = "";

    _showListing = false;
    _showPagination = false;

    _toggleSearch = true; // true - Simple Search, false - Advanced Search
    _togglePagination = true;

    _TypeList: any;
    _FilterList: any;
    mstatus = 0;

    _font = "";

    _ButtonInfo = { "role": "", "formname": "", "button": "" };

    _FilterDataset =
    {
        filterList:
        [
            { "itemid": "", "caption": "", "fieldname": "", "datatype": "", "condition": "", "t1": "", "t2": "" }
        ],

        "pageNo": 1, "pageSize": 10, "filterSource": 0, "sessionID": this.ics._profile.sessionID, "userID": this.ics._profile.userID, "userName": this.ics._profile.userName
    };

    _ListingDataset =
    {
        faqData:
        [
            {
                "srno":0,"syskey": 0, "autokey": 0,"createddate":"","modifiedDate":"","status":0,"userID":"","questionEng": "", "answerEng": "", "questionUni": "", 
                "answerUni": "","t1": "", "t2": "", "t3": "", "t4": "", "t5": "","n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0,"state": false
                
            }
        ],

        "pageNo": 0, "pageSize": 0, "totalCount": 1, "userID": "", "userName": ""
    };

    _sessionObj = this.getSessionObj();
    getSessionObj() {
        return { "sessionID": this.ics._profile.sessionID, "userID": this.ics._profile.userID };
    }

    constructor(private ics: RpIntercomService, private _router: Router,
        //private sanitizer: DomSanitizer, 
        private http: RpHttpService, private l_util: ClientUtil, private ref: RpReferences) {
        this.mstatus = ics._profile.loginStatus;
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(x => { })
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
    }

    ngOnInit() {
        this.filterSearch();
        this.loadAdvancedSearchData();
    }
    goto(p) {
        console.log("p=" + JSON.stringify(p));
        this.ics.sendBean({"faqData":p});
        this._router.navigate(['/faq-menu', 'read', p]); 
    }
    goNew() {
        this._router.navigate(['/faq-menu', 'new']);
    }

    ngAfterViewChecked() {
        this._isLoading = false;
    }

    loadAdvancedSearchData() {
        this._FilterList =
            [
                { "itemid": "1", "caption": "Search String", "fieldname": "SearchString", "datatype": "simplesearch", "condition": "c", "t1": "", "t2": "", "t3": "" }
            ];
    }


    btnToggleSearch_onClick() {
        this.toggleSearch(!this._toggleSearch);
    }

    btnTogglePagination_onClick() {
        this.togglePagination(!this._togglePagination);
    }

    toggleSearch(aToggleSearch) {
        // Set Flag
        this._toggleSearch = aToggleSearch;

        // Clear Simple Search
        if (!this._toggleSearch) jQuery("#isInputSearch").val("");

        // Enable/Disabled Simple Search Textbox
        jQuery('#isInputSearch').prop('disabled', !this._toggleSearch);

        // Hide Advanced Search
        if (this._toggleSearch) this.ics.send001("CLEAR");

        // Show/Hide Advanced Search    
        //  
        if (this._toggleSearch) jQuery("#asAdvancedSearch").css("display", "none");                                                                             // true     - Simple Search
        else jQuery("#asAdvancedSearch").css("display", "inline-block");                                                                     // false    - Advanced Search

        // Set Icon 
        //  
        if (this._toggleSearch) jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-up").addClass("glyphicon glyphicon-menu-down");                // true     - Simple Search
        else jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-down").addClass("glyphicon glyphicon-menu-up");                // false    - Advanced Search

        // Set Tooltip
        //
        let l_Tooltip = (this._toggleSearch) ? "Expand" : "Collapse";
        jQuery('#btnToggleSearch').attr('title', l_Tooltip);
    }

    togglePagination(aTogglePagination) {
        // Set Flag
        this._togglePagination = aTogglePagination;

        // Show/Hide Pagination
        //
        if (this._showPagination && this._togglePagination) jQuery("#divPagination").css("display", "inline-block");
        else jQuery("#divPagination").css("display", "none");

        // Rotate Icon
        //
        if (!this._togglePagination) jQuery("#lblTogglePagination").css("transform", "rotate(270deg)");
        else jQuery("#lblTogglePagination").css("transform", "rotate(90deg)");

        // Set Icon Position
        if (!this._togglePagination) jQuery("#lblTogglePagination").css({ "margin-left": "-5px", "margin-top": "0px" });
        else jQuery("#lblTogglePagination").css({ "margin-left": "-5px", "margin-top": "-1px" });

        // Set Tooltip
        //
        let l_Tooltip = (this._togglePagination) ? "Collapse" : "Expand";
        jQuery('#btnTogglePagination').attr('title', l_Tooltip);
    }

    btnClose_MouseEnter() {
        jQuery("#btnClose").removeClass('btn btn-sm btn-secondary').addClass('btn btn-sm btn-danger');
    }

    btnClose_MouseLeave() {
        jQuery("#btnClose").removeClass('btn btn-sm btn-danger').addClass('btn btn-sm btn-secondary');
    }

    closePage() {
        this._router.navigate(['000', { cmd: "READ", p1: this.ics._profile.userID }]);
    }

    renderAdvancedSearch(event) {
        this.toggleSearch(!event);
    }

    filterAdvancedSearch(event) {
        this._FilterDataset.filterSource = 1;
        this._FilterDataset.filterList = event;

        if (this._OperationMode != "prepareFilter") {
            if (this._FilterDataset.pageNo == 1) this.filterRecords();
            else this._FilterDataset.pageNo = 1;
        }
    }

    changedPager(event) {
        if (!this._isLoading) {
            let l_objPagination = event.obj;

            this._FilterDataset.pageNo = l_objPagination.current;
            this._FilterDataset.pageSize = l_objPagination.size;

            this.filterRecords();
        }
    }

    filterSearch() {
        if (this._toggleSearch) this.filterCommonSearch();
        else this.ics.send001("FILTER");
    }

    filterCommonSearch() {
        var l_DateRange;
        var l_SearchString = "";

        this._FilterDataset.filterList = [];
        this._FilterDataset.filterSource = 0;

        if (jQuery.trim(this._SearchString) != "") {
            l_SearchString = jQuery.trim(this._SearchString);

            this._FilterDataset.filterList =
                [
                    { "itemid": "1", "caption": "Search String", "fieldname": "SearchString", "datatype": "simplesearch", "condition": "c", "t1": l_SearchString, "t2": "" }
                ];
        }

        if (this._OperationMode != "prepareFilter") {
            if (this._FilterDataset.pageNo == 1) this.filterRecords();
            else this._FilterDataset.pageNo = 1;
        }
    }

    filterRecords() {

        this._mflag = false;
        let l_Data: any = this._FilterDataset;
        let l_ServiceURL: string = this.ics.cmsurl + 'serviceCMS/getFAQList';

        // Show loading animation
        this.http.doPost(l_ServiceURL, l_Data).subscribe
            (
            data => {
                if (data != null) {
                    this._ListingDataset = data;
                    this._shownull = false;
                    // Convert to array for single item
                    if (this._ListingDataset.faqData != undefined) {
                        this._ListingDataset.faqData = this.l_util.convertToArray(this._ListingDataset.faqData);
                    }
                    else {
                        this._shownull = true;
                    }

                    // Show / Hide Listing
                    this._showListing = (this._ListingDataset.faqData != undefined);

                    // Show / Hide Pagination
                    this._showPagination = (this._showListing && (this._ListingDataset.totalCount > 10));

                    // Show / Hide Pagination
                    this.togglePagination(this._showListing);

                    // Hide loading animation
                    this._mflag = true;
                }
            },
            error => {
                // Hide loading animation
                this._mflag = true;

                // Show error message
                // this.ics.sendBean({ t1:"rp-error", t2: "Server connection error." });
            },
            () => { }
            );
    }
    
    downloadFile(aFileName) {
        let l_Url = this.ics._apiurl + 'service001/downloadexcel?filename=' + aFileName;
        jQuery("#ExcelDownload").html("<iframe src=" + l_Url + "></iframe>");
    }

    btnDashboard() {
        this._router.navigate(['/pocDashboard']);
    }
   
    goClosePrint() {
        this._divexport = false;
    }

    showMsg(msg, bool) {
        if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
        if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
        if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
    }

    logout() {
        this._router.navigate(['/login']);
    }

    getFullTitleLabel(str){
        if(str != undefined && str != null){
          if(localStorage.getItem("systemfont")!="uni"){
            if(isMyanmar(str))
              str = ZgtoUni(str);
        }
              return str;
        }else{
            return '';
        }
        
    }

}
