import { Component, enableProdMode } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { Subscription } from 'rxjs/Subscription';
import { RpHttpService } from '../framework/rp-http.service';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpReferences } from '../framework/rp-references';
import { ClientUtil } from '../util/rp-client.util';
declare var jQuery: any;
enableProdMode();
@Component({
  selector: 'sign-out',
  template: `
  <div class="container-fluid">
    <div class="row clearfix">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0"> 
    <form class= "form-horizontal"> 
    <!-- Form Name -->
    <legend>Forced Sign Out</legend>

    <div class="cardview list-height">

    <div class="row  col-md-12">   
      <button class="btn btn-sm btn-primary" disabled id="mylogout"  type="button" (click)="confirmLogout()" >Sign Out</button>
      <button class="btn btn-sm btn-primary" type="button" id="myclear" (click)="goClear()" >Clear</button>
    </div> 
    <div class="row col-md-12">&nbsp;</div>
    <div class="form-group">
    <div class="col-md-8">   
      <div class="form-group">
        <label class="col-md-2"> Login ID <font class="mandatoryfont" >*</font> </label>
        <div class="col-md-4">
              <div class="input-group">
                <input type="text" [(ngModel)]=_obj.t1 [ngModelOptions]="{standalone: true}" required="true"  class="form-control input-sm">
                <span class="input-group-btn">
                  <button class="btn btn-sm btn-primary" type="button" (click)="goCheck()"><i class="glyphicon glyphicon-ok-sign"></i> Check </button>
                </span>
              </div>
        </div>
        <div class="col-md-2">
          <label style="color:green;font-size:20px">&nbsp;{{isvalidate}}</label>   
        </div>
      </div>  
    
      <div class="form-group">
        <rp-input  rpType="text" rpClass="col-md-4" rpLabelClass ="col-md-2 control-label" rpLabel="Name" [(rpModel)]="_obj.name"  rpReadonly="true"></rp-input>    
      </div>     
    </div>   
    
    <div class="col-md-3">
      <div [hidden]="msghide">
        <h3 align="right" style="font-size:25px"><b>{{msg}}</b></h3>
      </div>
  </div>
    
    </div>
    </div>

    </form>
    </div>
    </div>
    </div>

    <div id="logoutconfirm" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
          <p>Do you want to end this user session?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" (click)="goForcedLogout()">Yes</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>

    <div id="sessionalert" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <p>{{sessionAlertMsg}}</p>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>

<div [hidden] = "_mflag">
  <div class="modal" id="loader"></div>
</div>
    
  `
})
export class ForceSignOutComponent {
  // RP Framework 
  subscription: Subscription;

  _returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
  msghide: boolean;
  _obj = this.getDefaultObj();
  sessionAlertMsg = "";
  isvalidate = ""; msg: String;
  _mflag = true;
  _util: ClientUtil = new ClientUtil();
  constructor(private ics: RpIntercomService, private _router: Router, private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences) {
    this.subscription = ics.rpbean$.subscribe(x => { })
    if (!ics.getRole() || ics.getRole() == 0) {
      this._router.navigate(['/login']);
    } else {
      this.checkSession();
      this._obj = this.getDefaultObj();
    }
  }

  getDefaultObj() {
    this.msg = "";
    return { "name": "", "t1": "", "loginstatus": 0 };
  }

  showMessage() {
    jQuery("#sessionalert").modal();
    Observable.timer(3000).subscribe(x => {
      this.goLogOut();
    });
  }

  goLogOut() {
    jQuery("#sessionalert").modal('hide');
    this._router.navigate(['/login']);
  }

  goClear() {
    this.isvalidate = "";
    this._obj = this.getDefaultObj();
    jQuery("#mylogout").prop("disabled", true);
  }

  showMessageAlert(msg) {
    this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
  }

  goCheck() {
    try {
      this._mflag=false;
      this.http.doGet(this.ics._apiurl + 'service001/getUserNameAndStatus?id=' + this._obj.t1 + '&sessionID=' + this.ics._profile.sessionID + '&userIDForSession=' + this.ics._profile.userID).subscribe(//%2B
        data => {
          if (data.msgCode == '0016') {
            this.sessionAlertMsg = data.msgDesc;
            this.showMsg(data.msgDesc,false);
          }
          else {
            if (data.t1 == '') {
              this.isvalidate = "";
              jQuery("#mylogout").prop("disabled", true);
              this.showMsg("Invalid User ID",false);
            }
            else {
              this._obj = data;
              this.isvalidate = "Validate";
              if (this._obj.loginstatus == 1) {
                this.msg = "Active Session";

                if (data.t1 == this.ics._profile.userID) {
                  jQuery("#mylogout").prop("disabled", true);
                  this.showMsg("Please Sign Out",false);
                }
                else {
                  jQuery("#mylogout").prop("disabled", false);
                }
              }
              else {
                this.msg = "Invalid Session";
              }
            }
          }
          this._mflag=true;
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
        }, () => { });
    } catch (e) {
      alert(e);
    }
  }

  confirmLogout() {
    jQuery("#logoutconfirm").modal();
  }

  goForcedLogout() {

    jQuery("#logoutconfirm").modal('hide');
    try {
      this._mflag = false;
      this.http.doGet(this.ics._apiurl + 'service001/forcedlogoutbyId?userId=' + this._obj.t1 + '&sessionID=' + this.ics._profile.sessionID + '&userIDForSession=' + this.ics._profile.userID).subscribe(
        data => {
          if (data.msgCode == '0016') {
            this.sessionAlertMsg = data.msgDesc;
            this.showMsg(data.msgDesc,false);
          }
          else {
          this._mflag = true;
          this._returnResult = data;
          if (this._returnResult.state) {
            this.msg = "Invalid Session";
            this.msghide = false;
            jQuery("#mylogout").prop("disabled", true);
            this.showMsg(data.msgDesc,true);
          }else this.showMsg(data.msgDesc,false);         
        }
        this._mflag = true;
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
        }, () => { });
    } catch (e) {
      alert(e);
    }
  }

  // about session validation
  sessionTimeoutMsg = "";
  _sessionObj = this.getSessionObj();
  getSessionObj() {
    return { "sessionID": "", "userID": "" };
  }

  checkSession() {
    try {
      let url: string = this.ics._apiurl + 'service001/checkSessionTime';

      this._sessionObj.sessionID = this.ics._profile.sessionID;
      this._sessionObj.userID = this.ics._profile.userID;
      let json: any = this._sessionObj;

      this.http.doPost(url, json).subscribe(
        data => {
          if (data != null) {
            if (data.code == "0016") {
              this.showMsgAlert(data.desc);
              this.logout();
            }

            if (data.code == "0014") {
              this.showMsgAlert(data.desc);
            }
          }
        },
        error => {
          if (error._body.type == "error") {
            alert("Connection Timed Out.");
          }
        },
        () => { }
      );
    } catch (e) {
      alert("Invalid URL.");
    }
  }

  showMsgAlert(msg) {
    this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
  }

  logout() {
    this._router.navigate(['/login']);
  }
  showMsg(msg, bool) {
    if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
    if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
    if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
  }
}
