import { Component, enableProdMode } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { Subscription } from 'rxjs/Subscription';
import { RpHttpService } from '../framework/rp-http.service';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpReferences } from '../framework/rp-references';
declare var jQuery: any;
enableProdMode();
@Component({
  selector: 'usersetup',
  template: `
  <div class="container-fluid">
  <div class="row clearfix">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
	<form class="form-horizontal" (ngSubmit) = "goSave()">
		<legend>User</legend>
		<div class="cardview list-height">
                <div class="row col-md-12">  
                    <button class="btn btn-sm btn-primary" type="button" (click)="goList()" >List</button> 
                    <button class="btn btn-sm btn-primary" type="button" (click)="goNew()" >New</button>      
                    <button class="btn btn-sm btn-primary" type="submit">Save</button>          
                    <button class="btn btn-sm btn-primary" disabled id="mydelete"  type="button" (click)="goDelete();" >Delete</button> 
                </div>
                <div class="row col-md-12">&nbsp;</div>
          <div class="form-group">
				
					<div class = "col-md-6">
						<div class="form-group"> 
							<label class="col-md-4">Login ID <font class="mandatoryfont">*</font></label>
								<div class="col-md-8">                  
								  <input *ngIf="_chkloginId == 'true'" class="form-control" type = "text" [(ngModel)]="_obj.t1"  [ngModelOptions]="{standalone: true}"readonly class="form-control input-sm"> 
								  <input *ngIf="_chkloginId != 'true'" class="form-control" type = "text" [(ngModel)]="_obj.t1" [ngModelOptions]="{standalone: true}" required="true" maxlength="20" class="form-control input-sm">                               
								</div>
						</div>
						<div class="form-group"> 
							<label class="col-md-4">Name<font class="mandatoryfont">*</font></label>
								<div class="col-md-8">                  
									<input [(ngModel)]="_obj.name" required type="text" maxlength="50"  class="form-control input-sm" [ngModelOptions]="{standalone: true}" />
								</div>
            </div>
            <div class="form-group"> 
							<label class="col-md-4">Phone No. <font class="mandatoryfont">*</font></label>
								<div class="col-md-8">                  
									<input [(ngModel)]="_obj.t4" required type="text" maxlength="20" (keydown)="restrictSpecialCharacter($event, 101 ,_obj.t4)" pattern="[0-9.,]+"  class="form-control input-sm" [ngModelOptions]="{standalone: true}"/>
								</div>
						</div>						
					</div>
					<div class="col-md-6">
              <div class="form-group"> 
              <label class="col-md-4">Password <font class="mandatoryfont">*</font></label>
                <div class="col-md-8">                  
                  <input [(ngModel)]="_obj.t2" required type="password"  class="form-control input-sm" [ngModelOptions]="{standalone: true}"/>
                </div>
            </div>
            <div class="form-group"> 
              <label class="col-md-4">Confirm Password <font class="mandatoryfont">*</font></label>
                <div class="col-md-8">                  
                  <input [(ngModel)]="confirmpwd" required type="password"  class="form-control input-sm" [ngModelOptions]="{standalone: true}" (keydown)="clearconfirm($event)" maxlength = "20" />
                </div> 
            </div>
						<div class="form-group"> 
							<label class="col-md-4">Email<font class="mandatoryfont">*</font></label>
								<div class="col-md-8">                  
									<input [(ngModel)]="_obj.t3" required type="text"  class="form-control input-sm" [ngModelOptions]="{standalone: true}"/>
								</div>
						</div>
						<!--<div class="form-group"> 
							<div>
								<label class="col-md-4" *ngIf = "isAdmin == true ">Region</label>
									<div class="col-md-8">                  
										<select [(ngModel)]="_obj.t5"   class="form-control input-sm" [ngModelOptions]="{standalone: true}">
											<option *ngFor="let c of ref._lov3.refstate" value="{{c.value}}">{{c.caption}}</option>
										</select>
									</div>
							</div>
						</div>-->
					</div>
				  
				<div class="col-md-12" style="margin-bottom: 10px;">
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#tab1">Role</a></li>
					</ul>
				</div>

					<div class="tab-content">
						<div id="tab1" class="tab-pane fade in active">
						  <div class="col-md-12">
							  <label>
								  <input type="checkbox"  [(ngModel)]="_result" (change)="updateChecked111($event)" [ngModelOptions]="{standalone: true}"> Select All
								<!-- {{_role}} --> </label>    

							<ul  style="list-style:none;">
							  <li *ngFor="let role of _obj.userrolelist">
								<div class="form-group">
									<div *ngIf="role.syskey!=0"> 
									<input type="checkbox"  [(ngModel)]="role.flag" (change)="updateChecked(role.syskey,$event)" [ngModelOptions]="{standalone: true}">             
									{{role.t2}}
									</div>
								</div>    
							  </li>
							</ul>
						</div>
						</div>
					</div>
        </div>
        </div>
            </form>
</div> 
</div>
</div> 

<div id="sessionalert" class="modal fade">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-body">
             <p>{{_sessionMsg}}</p>
        </div>
        <div class="modal-footer">
        </div>
    </div>
</div>
</div>

<div [hidden] = "_mflag">
<div  id="loader" class="modal" ></div>
</div> 

`
})
export class UserSetupComponent {
  catch = ""; sub: any; _output1 = "";
  message = "";
  _key = "";
  state = [{ "code": "", "despEng": "" }];
  city = [{ "code": "", "despEng": "" }];
  _chkloginId = 'false';
  loader: boolean;
  confirmpass = "";
  confirmpwd = "";
  _sessionMsg = "";
  subscription: Subscription;
  isAdmin: boolean;
  //isMerchant: boolean;
  messagehide: boolean;
  _role = "Role";
  _objtest = { "sessionId": "", "msgCode": "", "msgDesc": "", "syskey": 0, "recordStatus": 0, "n4": 0, "t1": "", "t2": "", "t3": "", "t4": "", "name": "", "username": "", "rolesyskey": [], "userrolelist": [{ "syskey": 0, "t2": "", "flag": false }], "t7": "", "t5": "--- Select ---", "t6": "", "n5": 0, "n6": 0, "n7": 0, "n2": 0, "userId": "" };
  _ans = "";
  _bdate: String;
  strongRegex = new RegExp("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%]).{4,20})");
  mediumRegex = new RegExp("^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{5,}$");
  nrcPattern = "(\\d{1,2}/\\w{4,9}\\((N|Naing|n|p|P|naing|Y|y|E|e)\\)\\d{5,6}|(\\w{1,1}/\\w{3,5}|\\w{3,5})\\d{6})";
  _returnResult = { "keyResult": 0, "longResult": "", "state": "", "stringResult": "", "userId": "", "msgCode": "", "msgDesc": "" };
  _obj = this.getDefaultObj();
  _mflag = false;

  constructor(private ics: RpIntercomService, private _router: Router, private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences) {
    this.subscription = ics.rpbean$.subscribe(x => { })
    if (!ics.getRole() || ics.getRole() == 0) {
      this._router.navigate(['/login']);
    } else {
      this._mflag = false;
      this.checkSession();
      jQuery("#mydelete").prop("disabled", true);
      this._obj = this.getDefaultObj();
      this._returnResult = { "keyResult": 0, "longResult": "", "state": "", "stringResult": "", "userId": "", "msgCode": "", "msgDesc": "" };
      this.isAdmin = true;
      //this.isMerchant = false;
      //this.loader = true;
      this.checkSession();
      this.getAllBranch();
      this.getAllMerchant();
      this.ics.confirmUpload(false);
      this.getUserRoleList();
      //this.getStateList();
    }
  }

  pad(s) {
    return (s < 10) ? '0' + s : s;
  }

  getDefaultObj() {
    return { "sessionId": "", "msgCode": "", "msgDesc": "", "syskey": 0, "recordStatus": 0, "n4": 0, "t1": "", "t2": "", "t3": "", "t4": "", "name": "", "username": "", "rolesyskey": [], "userrolelist": [{ "syskey": 0, "t2": "", "flag": false }], "t7": "", "t5": "--- Select ---", "t6": "", "n5": 0, "n6": 0, "n7": 0, "n2": 0, "userId": "" };
  }

  goNew() {
    this._mflag = false;
    this.clearData();
    this._mflag = true;
  }

  clearData() {
    this._chkloginId = 'false';
    this.confirmpwd = "";
    this.message = "";
    this._result = false;
    this._key = "";
    this.rolesyskey = [];
    this._obj.rolesyskey = [];
    this._obj.t1 = "";
    this._obj.t2 = "";
    this._obj.t3 = "";
    this._obj.t5 = "--- Select ---";
    this._obj.t6 = "";
    this._obj.t7 = "";
    this._obj.t4 = "";
    this._obj.recordStatus = 0;

    this._obj.n2 = 0;
    this._obj.n5 = 0;
    this._obj.n6 = 0;
    this._obj.n7 = 0;

    this._obj.userId = "";
    this._obj.name = "";
    this._obj.username = "";
    this._obj.syskey = 0;
    this.messagehide = true;
    this.city = [{ "code": "", "despEng": "" }];
    this._returnResult = { "keyResult": 0, "longResult": "", "state": "", "stringResult": "", "userId": "", "msgCode": "", "msgDesc": "" };

    for (let i = 0; i < this._obj.userrolelist.length; i++) {
      this._obj.userrolelist[i].flag = false;

    }
    jQuery("#mydelete").prop("disabled", true);
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let cmd = params['cmd'];
      if (cmd != null && cmd != "" && cmd == "read") {
        let id = params['id'];
        this._key = id;
        this.goReadBySyskey(id);
      }
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  restrictSpecialCharacter(event, fid, value) {
    if ((event.key >= '0' && event.key <= '9') || event.key == '.' || event.key == ',' || event.key == 'Home' || event.key == 'End' ||
        event.key == 'ArrowLeft' || event.key == 'ArrowRight' || event.key == 'Delete' || event.key == 'Backspace' || event.key == 'Tab' ||
        (event.which == 67 && event.ctrlKey === true) || (event.which == 88 && event.ctrlKey === true) ||
        (event.which == 65 && event.ctrlKey === true) || (event.which == 86 && event.ctrlKey === true)) {
        if (fid == 101) {
            if (/^[\\\"\'\;\:\>\|~`!@#\$%^&*\(\)]$/i.test(event.key)) {
                event.preventDefault();
            }
        }

        if (value.includes(".")) {
            if (/^[\\\"\'\;\:\>\|~`!@#\$%^&*.\(\)]$/i.test(event.key)) {
                event.preventDefault();
            }
        }
    }
    else {
        event.preventDefault();
    }
}

  

  goReadBySyskey(p) {
    try {
      this._chkloginId = 'true';
      let url: string = this.ics._apiurl + 'service001/getBranchUserData?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
      let json: any = this._key;
      this.http.doPost(url, json).subscribe(
        data => {
          this._output1 = JSON.stringify(data);
          this.confirmpwd = data.t2;
          this._obj = data;
          if (this._obj.msgCode == '0016') {
            this._returnResult.msgDesc = "";
            this._sessionMsg = this._obj.msgDesc;
            this.showMessage();
          } else {

            /* if (this._obj.n2 == 3) {
              //this.isMerchant = true;
              this.isAdmin = false;
            }
            else {
              //this.isMerchant = false;
              this.isAdmin = true;
            } */

            for (let i = 0; i < this._obj.userrolelist.length; i++) {
              if (data.userrolelist[i].flag)
                this._obj.userrolelist[i].flag = true;
              else
                this._obj.userrolelist[i].flag = false;

            }

            jQuery("#mydelete").prop("disabled", false);
            this.rolesyskey = [];
            for (let i = 0; i < this._obj.rolesyskey.length; i++) {
              if (this._obj.rolesyskey[i] != 0) {
                this.rolesyskey.push(this._obj.rolesyskey[i]);
              }
            }

            this._obj.rolesyskey = this.rolesyskey;
            if (this._obj.userrolelist[1].syskey == 0) {
              if (this._obj.userrolelist.length - 1 == this.rolesyskey.length) {
                this._result = true;
              }

            } else {
              if (this._obj.userrolelist.length == this.rolesyskey.length) {
                this._result = true;
              }

            }
          }
        },

        () => { }
      );

    } catch (e) {
      alert(e);
    }
  }

  goList() {
    this._router.navigate(['/user-list']);
  }

  getAllMerchant() {
    try {
      this.http.doGet(this.ics._apiurl + 'service001/getAllMerchant').subscribe(
        data => {
          if (data != null && data != undefined) {
            if (!(data.ref019 instanceof Array)) {//Object
              let m = [];
              m[0] = data.ref019;
              this.ref._lov3.ref019 = m;
            }
            else {
              this.ref._lov3.ref019 = data.ref019;
            }
          }
          else {
            this.ref._lov3.ref019 = [{ "value": "", "caption": "Empty" }];
          }
        },
        () => { });
    } catch (e) {
      alert(e);
    }
  }

  getAllBranch() {
    try {
      this._mflag = false;
      this.http.doGet(this.ics._apiurl + 'service001/getAllBranch').subscribe(
        data => {
          if (data != null && data != undefined) {

            let brcombo = [{ "value": "0", "caption": "--- Select ---" }];
            for (let i = 0; i < data.ref018.length; i++) {
              brcombo.push({ "value": data.ref018[i].value, "caption": data.ref018[i].caption });
            }
            this.ref._lov3.ref018 = data.ref018;
            this.ref._lov3.ref018 = brcombo;
          }
          else {
            this.ref._lov3.ref018 = [{ "value": "0", "caption": "--- Select ---" }];
          }
          this._mflag = true;

        },

        () => { }
      );

    } catch (e) {
      alert(e);
    }
  }

  /* changeModule(event) {
    let options = event.target.options;
    let k = options.selectedIndex;//Get Selected Index
    let value = options[options.selectedIndex].value;//Get Selected Index's Value  
    this._obj.n2 = value;

    for (var i = 1; i < this.ref._lov1.ref025.length; i++) {
      if (this.ref._lov1.ref025[i].value == value) {
        this._obj.n2 = this.ref._lov1.ref025[i].value;
      }
    }

    if (this._obj.n2 == 2) {
      this.isAdmin = true;
      this.isMerchant = false;
    } else {
      this.isMerchant = true;
      this.isAdmin = false;
      this.getAllMerchant();

    }
  } */

  updateChecked111(event) {
    if (event.target.checked) {
      for (let i = 0; i < this._obj.userrolelist.length; i++) {
        this._obj.userrolelist[i].flag = true;
        this._result = true;
        if (this._obj.userrolelist[i].syskey != 0) {
          this.rolesyskey[i] = this._obj.userrolelist[i].syskey;
        }
      }

    }
    else {
      //if parentmenu is not check, uncheck all childmenu
      for (let i = 0; i < this._obj.userrolelist.length; i++) {
        this._obj.userrolelist[i].flag = false;
        let indexx = this.rolesyskey.indexOf(this._obj.userrolelist[i].syskey);
        this.rolesyskey.splice(indexx, 1);
        this._result = false;

      }

    }
    this._ans1 = JSON.stringify(this.rolesyskey);
    this._obj.rolesyskey = this.rolesyskey;

  }

  clearconfirm(e: any) {
    this.confirmpass = "";
  }

  keyupconfirm(e: any) {
    if (this._obj.t2 === "" || this._obj.t2 === null) {
      this.message = "";
    }
    else {

      if (this._obj.t2.length >= 6) {
        if (this.mediumRegex.test(this._obj.t2)) {
          this.message = "Medium Password"
          this.catch = "";
        }
        else if (this.strongRegex.test(this._obj.t2)) {
          this.message = "Strong Password";
          this.catch = "";
        }
      }

      else {
        this.message = "Invalid Password";
        this.catch = "aaa";
      }
    }
  }

  _ans1 = "";
  rolesyskey: Array<any> = [];
  _result: boolean;

  updateChecked(value, event) {
    if (event.target.checked) {
      this.rolesyskey.push(value);
    }
    else if (!event.target.checked) {
      let indexx = this.rolesyskey.indexOf(value);
      this.rolesyskey.splice(indexx, 1);
    }
    this._ans = JSON.stringify(this.rolesyskey);

    this._obj.rolesyskey = this.rolesyskey;

    if (this._obj.userrolelist[1].syskey == 0) {


      if (this._obj.userrolelist.length - 1 == this.rolesyskey.length) {

        for (let i = 0; i < this._obj.userrolelist.length - 1; i++) {
          this._obj.userrolelist[i].flag = true;
        }

        this._result = true;
      }

      else {

        this._result = false;
      }
    }
    else {

      if (this._obj.userrolelist.length == this.rolesyskey.length) {

        for (let i = 0; i < this._obj.userrolelist.length; i++) {
          this._obj.userrolelist[i].flag = true;
        }
        this._result = true;
      }
      else {
        this._result = false;
      }
    }

  }
  /* getStateList() {
    try {
      this.http.doGet(this.ics._apiurl + 'service001/getStateListNew?sessionID='+ this.ics._profile.sessionID + '&userID='+ this.ics._profile.userID).subscribe(
        response => {
          if (response.refstate != null && response.refstate != undefined) {
            //this.ref._lov3.refstate[i] = response.refstate[i - 1];
            this.ref._lov3.refstate = [{ "value": "00000000", "caption": "ALL" }];
            for (let i = 1; i < response.refstate.length; i++) {
              if (response.refstate[i].value == "13000000") {
                this.ref._lov3.refstate[1] = response.refstate[i];
              }
              if (response.refstate[i].value == "10000000") {
                this.ref._lov3.refstate[2] = response.refstate[i];
              }
              if (response.refstate[i].value == "00000000") {
                this.ref._lov3.refstate[3] = response.refstate[i];
              }
            }
          } else {
            this.ref._lov3.refstate = [{ "value": "", "caption": "Empty" }];
          }
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Error!");
          }
        }, () => { });
    } catch (e) {
      alert(e);
    }
  } */

  getUserRoleList() {
    try {
      this._mflag = false;
      this.http.doGet(this.ics._apiurl + 'service001/getUserRolelist?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(
        data => {
          if (data != null) {
            this._obj = data;
            for (let i = 0; i < this._obj.userrolelist.length; i++) {
              this._obj.userrolelist[i].flag = false;
            }
          }
          this._mflag = true;
        },

        () => { }
      );
    } catch (e) {
      alert(e);
    }

  }

  validateWhiteSpace(s) {
    if (/\s/.test(s)) {
      return (false);
    }
    return (true);
  }

  goSave() {
    this._mflag = false;
    this._returnResult = { "keyResult": 0, "longResult": "", "state": "", "stringResult": "", "userId": "", "msgCode": "", "msgDesc": "" };
    if (this._obj.t1.length > 0) {
      if (!this.validateWhiteSpace(this._obj.t1)) {
        this._returnResult.state = 'false';
        this._returnResult.msgDesc = "Login ID cannot contains space";
        this._mflag = true;
      }

      if (!/^([A-Za-z0-9]{1,20})$/.test(this._obj.t1)) {
        this._returnResult.state = 'false';
        this._returnResult.msgDesc = "Login ID should not contain special characters";
        this._mflag = true;
      }

      if (this._obj.t1.length > 20) {
        this._returnResult.state = 'false';
        this._returnResult.msgDesc = "Login ID should contain at most twenty characters ";
        this._mflag = true;

      }
    }

    if (this._obj.t2 != this.confirmpwd) {
      //this.confirmpass="Password and Confirm Password do not Match!"
      this._returnResult.state = 'false';
      this._returnResult.msgDesc = "Password and Confirm Password do not Match!";
      this._mflag = true;
    }

    if (this._obj.t2 == this.confirmpwd) {
      if (this._obj.t2.length < 6) {
        this._returnResult.state = 'false';
        this._returnResult.msgDesc = "Password length must be at least six characters";
        this._mflag = true;
      }

    }
    /* if (this._obj.t5 != "00000000" && this._obj.t5 !="13000000" && this._obj.t5 !="10000000") {
      this._returnResult.state = 'false';
      this._returnResult.msgDesc = "Please select region";
      this._mflag = true;
  } */
    else if (this.catch.length > 0) {
      this.message = "Invalid Password,Try Again";
      this._mflag = true;
    }

    if (!/^([0-9]{7,20})$/.test(this._obj.t4)) {
      this._returnResult.state = 'false';
      this._returnResult.msgDesc = "Phone number is invalid";
      this._mflag = true;

    }

    if (this._obj.t3.length > 0) {
      if (!this.validateEmail(this._obj.t3)) {
        this._returnResult.state = 'false';
        this._returnResult.msgDesc = "Email address is invalid";
        this._mflag = true;
      }
    }

    try {
      if (this.confirmpass == '' && this._returnResult.msgDesc == '') {
        this.loader = false;
        // this._mflag = false;
        this._obj.userId = this.ics._profile.userID;
        this._obj.sessionId = this.ics._profile.sessionID;
        let url: string = this.ics._apiurl + 'service001/saveBranchUser';
        let json: any = this._obj;
        this.http.doPost(url, json).subscribe(
          data => {
            this._mflag = true;

            if (data.msgCode == '0016') {
              this._sessionMsg = data.msgDesc;
              this.showMsg(data.msgDesc,false);
            }
            else if (data.msgCode == '0001') {
              this.rolesyskey = [];
              this.showMsg(data.msgDesc,false);
            }
            else {
              if (data.state == true) {
                this._chkloginId = 'true';
                this._obj.syskey = data.keyResult;
                this.message = "";
                this._key = data.keyResult + "";
                jQuery("#mydelete").prop("disabled", false);
                this.showMsg(data.msgDesc,true);
              }else this.showMsg(data.msgDesc,false);
              
              //_mflag = false;
              //this.loader = true;
            }
          },

          () => { }
        );

      } else {
        this.showMsg(this._returnResult.msgDesc,false);
      }
    } catch (e) {
      alert(e);
    }
  }

  goDelete() {
    try {
      this._mflag = false;
      this._obj.userId = this.ics._profile.userID;
      let url: string = this.ics._apiurl + 'service001/deleteUser?sessionID=' + this.ics._profile.sessionID + '&userIDForSession=' + this.ics._profile.userID;
      let json: any =  this._obj;
      this._objtest = this._obj;
      this.http.doPost(url, json).subscribe(
        data => {
          if (data.msgCode == '0016') {
            this._sessionMsg = data.msgDesc;
            this.showMsg(data.msgDesc,false);
          }
          else {

            this._returnResult = data;            
            if (data.state) {
              this._obj = { "sessionId": "", "msgCode": "", "msgDesc": "", "syskey": 0, "recordStatus": 0, "n4": 0, "t1": "", "t2": "", "t3": "", "t4": "", "name": "", "username": "", "rolesyskey": [], "userrolelist": [{ "syskey": 0, "t2": "", "flag": false }], "t7": "", "t5": "--- Select ---", "t6": "", "n5": 0, "n6": 0, "n7": 0, "n2": 0, "userId": "" };
              this.confirmpwd = "";
              this.getUserRoleList();
              jQuery("#mydelete").prop("disabled", true);
              this.showMsg(this._returnResult.msgDesc,true);
            }else this.showMsg(this._returnResult.msgDesc,false);

          }
          this._mflag = true;
        },

        () => { }
      );

    } catch (e) {
      alert(e);
    }
  }


  validateEmail(mail) {
    if (/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(mail)) {
      return (true);
    }
    return (false);
  }

  showMessageAlert(msg) {
    this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
  }

  showMessage() {
    jQuery("#sessionalert").modal();
    Observable.timer(3000).subscribe(x => {
      this._router.navigate(['/login']);
      jQuery("#sessionalert").modal('hide');
    });
  }

  // about session validation
  sessionTimeoutMsg = "";
  _sessionObj = this.getSessionObj();
  getSessionObj() {
    return { "sessionID": "", "userID": "" };
  }

  checkSession() {
    try {
      let url: string = this.ics._apiurl + 'service001/checkSessionTime';

      this._sessionObj.sessionID = this.ics._profile.sessionID;
      this._sessionObj.userID = this.ics._profile.userID;
      let json: any = this._sessionObj;

      this.http.doPost(url, json).subscribe(
        data => {
          if (data != null) {
            if (data.code == "0016") {
              this.showMsgAlert(data.desc);
              this.logout();
            }

            if (data.code == "0014") {
              this.showMsgAlert(data.desc);
            }
          }
        },
        error => {
          if (error._body.type == "error") {
            alert("Connection Timed Out.");
          }
        },
        () => { }
      );
    } catch (e) {
      alert("Invalid URL.");
    }
  }

  showMsgAlert(msg) {
    this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
  }

  logout() {
    this._router.navigate(['/login']);
  }
  showMsg(msg, bool) {
    if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
    if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
    if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
  }
}