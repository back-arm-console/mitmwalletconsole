import { Component, enableProdMode } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { Subscription } from 'rxjs/Subscription';
import { RpHttpService } from '../framework/rp-http.service';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpReferences } from '../framework/rp-references';
declare var jQuery: any;
enableProdMode();
@Component({
  selector: 'role-setup',
  template: `
  <div class="container-fluid">
  <div class="row clearfix">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
	<form class="form-horizontal" (ngSubmit) = "goSave()">
		<legend>Role</legend>
			<div class="cardview list-height">
				<div class="row col-md-12">  
					<button class="btn btn-sm btn-primary" type="button" (click)="goList()" >List</button> 
					<button class="btn btn-sm btn-primary" type="button" (click)="goNew()" >New</button>      
					<button class="btn btn-sm btn-primary" id="mySave" type="submit" >Save</button>          
					<button class="btn btn-sm btn-primary" disabled id="mydelete"  type="button" (click)="goDelete();" >Delete</button> 
        </div>  
        
        <div class="row col-md-12">&nbsp;</div>
        <div class="form-group"> 
				  <div class="col-md-8">             
					<div class="form-group">
					  <label class="col-md-2">Code <font color="mandatoryfont" >*</font></label>
						 <div class="col-md-4">
						   <input  class="form-control input-sm" type = "text" [(ngModel)]="_obj.t1" required="true" *ngIf="_obj.syskey == 0 "  [ngModelOptions]="{standalone: true}"> 
						   <input  class="form-control input-sm" type = "text" [(ngModel)]="_obj.t1" required="true" *ngIf="_obj.syskey != 0 " readonly  [ngModelOptions]="{standalone: true}">     
						 </div>
					  </div>      

					<div class="form-group">        
					  <rp-input  rpType="text" rpClass="col-md-4" rpLabelClass ="col-md-2 control-label" rpLabel="Description" [(rpModel)]="_obj.t2" rpLabelRequired="true" rpRequired="true"></rp-input>
					</div>

					<div class="form-group">
					  <label class="col-md-2">Menu <font class="mandatoryfont">*</font></label> 
					  <div class="col-md-4" > </div>
					</div>
				
				  <div class="col-md-12">
				  <ul  style="list-style:none;">
					<li *ngFor="let parentmenu of _obj.menu, let i=index">
					<div class="form-group">
					
					  <div *ngIf="parentmenu.syskey!=0">
					  <input type="checkbox"  [(ngModel)]=parentmenu.result (change)="getParentValue(i,parentmenu.syskey,$event)" [ngModelOptions]="{standalone: true}">             
					  <!-- <rp-input rpClass="col-md-0"  [(rpModel)]=parentmenu.result rpType="checkbox" (change)="getParentValue(i,parentmenu.syskey,$event)"></rp-input> -->
					  {{parentmenu.t2}} 
					  </div>
					 
					</div>
					<div *ngFor="let pvalue of parentsyskey">
					  <div *ngIf="parentmenu.syskey==pvalue">
					  <ul style="list-style:none;">         
						<li *ngFor="let childmenu of parentmenu.childmenus">
						<div class="form-group"> 
						  <div *ngIf="childmenu.syskey!=0">
						  <!--  <rp-input rpClass="col-md-0" [(rpModel)]=childmenu.result rpType="checkbox" (change)="getChildValue(childmenu.syskey,$event)"></rp-input> -->
						  <input type="checkbox"  [(ngModel)]=childmenu.result (change)="getChildValue(childmenu.syskey,$event)" [ngModelOptions]="{standalone: true}">             
						  {{childmenu.t2}} 
						  </div>                  
						</div>                 
						</li> 
					  </ul>
					  </div>
					  </div>
					</li>  
					</ul>  
				  </div>
          </div>
				</div>
			</div>
    </form>
</div>
</div>
</div>
     
<div [hidden] = "_mflag">
<div  id="loader" class="modal" ></div>
</div> 


  <div id="sessionalert" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body"><p>{{sessionAlertMsg}}</p></div>
        <div class="modal-footer"></div>
      </div>
    </div>
  </div>

 
  `
})
export class RoleSetupComponent {
  // RP Framework 
  subscription: Subscription;
  sub: any;
  _menusyskeyrecord: number;
  sessionAlertMsg = "";
  parentsyskey: Array<any> = [];
  chkparentsyskey: Array<any> = [];
  childsyskey: Array<any> = [];
  _obj = this.getDefaultObj();
  _before = this._obj;
  _result = { "longResult": "", "msgCode": "", "msgDesc": "", "key": [], "keyResult": 0, "state": "", "stringResult": "" };
  _keyobj = { "_key": "", "isParent": "", "sessionId": "" };
  _key = "";  
  menusys: number;  
  _output1 = "";
  _mflag = false;

  constructor(private ics: RpIntercomService, private _router: Router, private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences) {
    this.subscription = ics.rpbean$.subscribe(x => { })
    if (!ics.getRole() || ics.getRole() == 0) {
      this._router.navigate(['/login']);
    } else {
      this._mflag = true;
      this.checkSession();
      this.getRoleMenuList();
      this._obj = this.getDefaultObj();
    }
  }
  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let cmd = params['cmd'];
      if (cmd != null && cmd != "" && cmd == "read") {
        let id = params['id'];
        this._key = id;
        this.goReadBySyskey(id);
      }
    });
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  getDefaultObj() {    

    jQuery("#mydelete").prop("disabled", true);
    jQuery("#mySave").prop("disabled", false);
    return {"syskey": 0,"t1": "","t2": "","n3": "","userId": "","userName": "","msgCode": "","msgDesc": "","sessionId": "","childsyskey": [], "parentsyskey": [],"menu": [{ "syskey": 0, "t2": "", "n2": 0, "result": false, "childmenus": [{ "syskey": 0, "t2": "", "n2": 0, "result": false }] }]};

  }

  goReadBySyskey(p) {
    try {
      this._mflag = false;
      console.log(JSON.stringify(this.ics._profile.sessionID));
      let url: string = this.ics._apiurl + 'service001/getRoleData?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
      this.http.doPost(url, p).subscribe(
        data => {
          if (data.msgCode == '0016') {
            this.sessionAlertMsg = data.msgDesc;
            this.showMsg(data.msgDesc,false);
          }
          else {
            this._obj = data;
            let p = data.menu.length;
            for (let i = 0; i < p; i++) {
              if (data.menu[i].result)
                this._obj.menu[i].result = true;
              else
                this._obj.menu[i].result = false;

              if (data.menu[i].childmenus != undefined && data.menu[i].childmenus != null) {
                let c = data.menu[i].childmenus.length;
                for (let j = 0; j < c; j++) {
                  if (data.menu[i].childmenus[j].result)
                    this._obj.menu[i].childmenus[j].result = true;
                  else
                    this._obj.menu[i].childmenus[j].result = false;
                }
              }
            }
            this.parentsyskey = [];
            this.childsyskey = [];
            this.parentsyskey = this.parentsyskey.concat(this._obj.parentsyskey);
            this.childsyskey = this.childsyskey.concat(this._obj.childsyskey);
            this.chkparentsyskey = this.chkparentsyskey.concat(this._obj.parentsyskey);
            jQuery("#mydelete").prop("disabled", false);
          }
          this._mflag = true;
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
        }, () => { });
    } catch (e) {
      alert(e);
    }
  
  }

  goList() {
    this._router.navigate(['/rolesetup-list']);
  }
  showMessage() {
    jQuery("#sessionalert").modal();
    Observable.timer(3000).subscribe(x => {
      this.goLogOut();
    });
  }

  goLogOut() {
    jQuery("#sessionalert").modal('hide');
    this._router.navigate(['/login']);
  }

  /* checkSession() {
    try {

      let url: string = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
      this.http.doGet(url).subscribe(
        data => {
          if (data.msgCode == '0016') {
            this.sessionAlertMsg = data.msgDesc;
            this.showMessage();
          }
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
          else {

          }
        }, () => { });
    } catch (e) {
      alert(e);
    }
  } */
 
  showMessageAlert(msg) {
    this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
  }

  clearData() {
    
    this._key = "";
    this.parentsyskey = [];
    this.childsyskey = [];
    this._obj.syskey = 0;
    this._obj.t1 = "";
    this._obj.t2 = "";
    this._obj.n3 = "";
    this._obj.childsyskey = [];
    this._obj.parentsyskey = [];
    this._obj.msgCode = "";
    this._obj.msgDesc = "";
    this._obj.sessionId = "";
    let p = this._obj.menu.length;
    for (let i = 0; i < p; i++) {
      this._obj.menu[i].result = false;
      if (this._obj.menu[i].childmenus != undefined && this._obj.menu[i].childmenus != null) {
        let c = this._obj.menu[i].childmenus.length;
        for (let j = 0; j < c; j++) {
          this._obj.menu[i].childmenus[j].result = false;

        }
      }
    }

    jQuery("#mydelete").prop("disabled", true);
    jQuery("#mySave").prop("disabled", false);
  }  

  getRoleMenuList() {
    try {
      this._mflag = false;
      this.http.doGet(this.ics._apiurl + 'service001/getRoleMenuList?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(
        data => {
          if (data.msgCode == '0016') {
            this.sessionAlertMsg = data.msgDesc;
            this.showMsg(data.msgDesc,false);
          }
          else {
            this._obj = data;
            let p = data.menu.length;
            for (let i = 0; i < p; i++) {
              this._obj.menu[i].result = false;
              if (data.menu[i].childmenus != undefined || data.menu[i].childmenus != null) {
                let c = data.menu[i].childmenus.length;
                for (let j = 0; j < c; j++) {
                  this._obj.menu[i].childmenus[j].result = false;
                }
              }
            }

            this._output1 = JSON.stringify(data);
          }
          this._mflag = true;
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
          else {

          }
        }, () => { });
    } catch (e) {
      alert(e);
    }
  }


  getParentValue(indexno, value, event) {

    if (event.target.checked) {
      this.chkparentsyskey.push(value);
      let indexx = this.chkparentsyskey.indexOf(value);
      //if parentmenu is check, check all childmenu
      if (this._obj.menu[indexno].childmenus != undefined) {
        for (let i = 0; i < this._obj.menu[indexno].childmenus.length; i++) {
          this._obj.menu[indexno].childmenus[i].result = true;  
          this.childsyskey.push( this._obj.menu[indexno].childmenus[i].syskey);
          this._obj.childsyskey = this.childsyskey;
          // let indexing = this.childsyskey.indexOf(this._obj.menu[indexno].childmenus[i].syskey);
         
      }
    }
      
      this.parentsyskey.push(value);
    }
    else {

      let indexx = this.parentsyskey.indexOf(value);
      this.parentsyskey.splice(indexx, 1);
      //if parentmenu is not check, uncheck all childmenu
      if (this._obj.menu[indexno].childmenus != undefined) {
        for (let i = 0; i < this._obj.menu[indexno].childmenus.length; i++) {
          this._obj.menu[indexno].childmenus[i].result = false;
          let indexing = this.childsyskey.indexOf(this._obj.menu[indexno].childmenus[i].syskey);
          if (indexing != -1)
            this.childsyskey.splice(indexing, 1);
        }
      }
    }

    this._obj.parentsyskey = this.parentsyskey;

  }

  getChildValue(value, event) {

    if (event.target.checked) {
      this.childsyskey.push(value);
    }
    else {
      let indexx = this.childsyskey.indexOf(value);
      this.childsyskey.splice(indexx, 1);
    }
    this._obj.childsyskey = this.childsyskey;
  }

  goDelete() {
    try {
      this._mflag = false;
      let url: string = this.ics._apiurl + 'service001/deleteRole?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
      let json: any = this._key;
      this.http.doPost(url, json).subscribe(
        data => {
          if (data.msgCode == '0016') {
            this.sessionAlertMsg = data.msgDesc;
            this.showMsg(data.msgDesc,false);
          }
          else {
            if (data.msgDesc == 'Deleted successfully') {
              this.clearData();
              this.showMsg(data.msgDesc,true);
            }else this.showMsg(data.msgDesc,false);            
          }
          this._mflag = true;
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
        }, () => { });
    } catch (e) {
      alert(e);
    }
  }

  goSave() {
    try {
      this._mflag = false;
      this._obj.userId = this.ics._profile.userID;
      this._obj.sessionId = this.ics._profile.sessionID;
      let url: string = this.ics._apiurl + 'service001/saveRole';
      let json: any = this._obj;
      this.http.doPost(url, json).subscribe(
        data => {
          if (data.msgCode == '0016') {
            this.sessionAlertMsg = data.msgDesc;
            this.showMsg(data.msgDesc,false);
          }
          else {
            this._output1 = JSON.stringify(data);
            if (data.msgDesc == 'Saved successfully' || data.msgDesc == 'Updated successfully') {
              this._obj.syskey = data.keyResult;
              this._key = this._obj.syskey + "";
              jQuery("#mydelete").prop("disabled", false);
              this.showMsg(data.msgDesc,true);
            }
            if (data.msgDesc == 'Please select menu') {
              this.parentsyskey = [];
              this.childsyskey = [];
              this.showMsg(data.msgDesc,false);
            }            
          }
          this._mflag = true;
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
        }, () => { });
    } catch (e) {
      alert(e);
    }
  }


  goNew() {
    this.clearData();
  }

  // about session validation
  sessionTimeoutMsg = "";
  _sessionObj = this.getSessionObj();
  getSessionObj() {
    return { "sessionID": "", "userID": "" };
  }

  checkSession() {
    try {
      let url: string = this.ics._apiurl + 'service001/checkSessionTime';

      this._sessionObj.sessionID = this.ics._profile.sessionID;
      this._sessionObj.userID = this.ics._profile.userID;
      let json: any = this._sessionObj;

      this.http.doPost(url, json).subscribe(
        data => {
          if (data != null) {
            if (data.code == "0016") {
              this.showMsg(data.desc, false);
              this.logout();
            }
            if (data.code == "0014") {
              this.showMsg(data.desc, false);
            }
          }
        },
        error => {
          if (error._body.type == "error") {
            alert("Connection Timed Out.");
          }
        },
        () => { }
      );
    } catch (e) {
      alert("Invalid URL.");
    }
  }

  /* showMsgAlert(msg) {
    this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
  } */
  showMsg(msg, bool) {
    if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
    if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
    if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
}

  logout() {
    this._router.navigate(['/login']);
  }
  
}
