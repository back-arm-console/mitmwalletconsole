import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { enableProdMode } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpInputComponent } from '../framework/rp-input.component';
import { RpHttpService } from '../framework/rp-http.service';
import { RpBean } from '../framework/rp-bean';
import { RpReferences } from '../framework/rp-references';
import { ClientUtil } from '../util/rp-client.util';
import { Observable } from 'rxjs/Rx';
declare var jQuery: any;
enableProdMode();
@Component({
  selector: 'locatorsetup',
  template: `
  <div class="container-fluid"> 
  <div class="row clearfix">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0">   
    <form class= "form-horizontal" (ngSubmit)="goSave()"> 
         <!-- Form Name -->
          <legend>Locator</legend>
			<div class="cardview list-height">
				<div class="row col-md-12">  
                    <button class="btn btn-sm btn-primary" type="button" (click)="goList()" >List</button> 
                    <button class="btn btn-sm btn-primary" type="button" (click)="goNew()" >New</button>      
                    <button class="btn btn-sm btn-primary" type="submit">Save</button>          
                    <button class="btn btn-sm btn-primary" disabled id="mydelete"  type="button" (click)="goDelete();" >Delete</button> 
                </div>
                <div class="row col-md-12">&nbsp;</div>
                <div class="form-group"> 
                    <div class = "col-md-6">
                        <div class="form-group">	
                          <label class="col-md-4">ID <font color="#FF0000" >*</font></label>
                          <div class="col-md-8">                  
                              <input [(ngModel)]="_obj.t1" required type="text"  class="form-control input-sm" [ngModelOptions]="{standalone: true}" readonly/>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-4">Name<font color="#FF0000" >*</font></label>
                          <div class="col-md-8">                  
                              <input [(ngModel)]="_obj.name" required type="text"  class="form-control input-sm" [ngModelOptions]="{standalone: true}"/>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-4">Latitude <font color="#FF0000" >*</font></label>
                          <div class="col-md-8">                  
                              <input [(ngModel)]="_obj.latitude" required type="text"  class="form-control input-sm" [ngModelOptions]="{standalone: true}"/>
                          </div>
                        </div>		
                            <div class="form-group">
                          <label class="col-md-4">Location Type <font color="#FF0000" >*</font></label>
                          <div class="col-md-8">                  
                              <select [(ngModel)]="_obj.locationType"  class="form-control input-sm" [ngModelOptions]="{standalone: true}" required>
                                  <option *ngFor="let c of ref._lov3.refLocType" value="{{c.value}}">{{c.value}}</option>
                              </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-4">Address <font color="#FF0000" >*</font></label>
                          <div class="col-md-8">                  
                              <textarea  class="form-control input-sm" rows="3"  [(ngModel)]="_obj.address" [ngModelOptions]="{standalone: true}" required></textarea>
                          </div>
                        </div>
                    </div>
              
                    <div class ="col-md-6">
                        <div class="form-group">
                          <label class="col-md-4">Phone 1 <font color="#FF0000" >*</font></label>
                          <div class="col-md-8">                  
                              <input [(ngModel)]="_obj.phone1" required type="text" (keydown)="restrictSpecialCharacter($event, 101 ,_obj.phone1)" pattern="[0-9.,]+"  class="form-control input-sm" [ngModelOptions]="{standalone: true}"/>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-4">Phone 2 <font color="#FF0000" >*</font></label>
                          <div class="col-md-8">                  
                              <input [(ngModel)]="_obj.phone2" required type="text" (keydown)="restrictSpecialCharacter($event, 101 ,_obj.phone2)" pattern="[0-9.,]+"  class="form-control input-sm" [ngModelOptions]="{standalone: true}"/>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-4">Longitude <font color="#FF0000" >*</font></label>
                          <div class="col-md-8">                  
                            <input [(ngModel)]="_obj.longitude" required type="text"  class="form-control input-sm" [ngModelOptions]="{standalone: true}"/>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-4">Branch Code <font color="#FF0000" >*</font></label>
                          <div class="col-md-8">                    
                              <select [(ngModel)]="_obj.branchCode"  class="form-control input-sm" [ngModelOptions]="{standalone: true}" required>
                                  <option *ngFor="let c of ref._lov3.ref024" value="{{c.value}}">{{c.caption}}</option>
                              </select>
                          </div>
                        </div>	
                    </div>
                    <div align="center" *ngIf="_showListing">
                    Total {{_obj.totalCount}}
                 </div>
        </div>
        </div>
  </form>
</div>
</div>
</div>

<div [hidden] = "_mflag">
  <div class="modal" id="loader"></div>
</div> 
`
})
export class LocatorSetupComponent {
  sub: any;
  _returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "msgCode": "" };
  _output1 = "";
  subscription: Subscription;
  _sessionMsg = "";
  message = "";
  _key = "";
  _obj = this.getDefaultObj();
  confirmpwd = "";
  _mflag = false;
  constructor(private ics: RpIntercomService, private _router: Router, private route: ActivatedRoute, private http: RpHttpService, private ref: RpReferences) {
    this.subscription = ics.rpbean$.subscribe(x => { })
    if (!ics.getRole() || ics.getRole() == 0) {
      this._router.navigate(['/login']);
    } else {
      this._mflag = false;
      this.checkSession();
      this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "msgCode": "" };
      jQuery("#mydelete").prop("disabled", true);
      this.getBranchCode();
      this.getLocationCbo();
    }
  }

  getDefaultObj() {
    return { "sessionID": "", "msgCode": "", "msgDesc": "", "createdDate": "", "modifiedDate": "", "latitude": "", "longitude": "", "address": "", "phone1": "", "phone2": "", "branchCode": "002", "name": "", "locationType": "All", "t1": "", "userid": "", "username": "" };
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let cmd = params['cmd'];
      if (cmd != null && cmd != "" && cmd == "new") {
        this.goNew();
    } else if(cmd != null && cmd != "" && cmd == "read") {
        let id = params['id'];
        this._key = id;
        this.goReadBySyskey(id);
      }
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  goSave() {
    this._mflag = false;
    this._obj.userid = this.ics._profile.userID;
    this._obj.username = this.ics._profile.userName;
    this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "msgCode": "" };
    if (this._obj.phone1.length > 0) {
      if (!/^([0-9]{7,20})$/.test(this._obj.phone1)) {
        this._returnResult.state = 'false';
        this._returnResult.msgDesc = "Phone No1 is invalid.";
        this._mflag = true;
      }
    }
    if (this._obj.phone2.length > 0) {
      if (!/^([0-9]{7,20})$/.test(this._obj.phone2)) {
        this._returnResult.state = 'false';
        this._returnResult.msgDesc = "Phone No2 is invalid.";
        this._mflag = true;
      }
    }
    if (this._obj.phone2.length > 0) {
      if (!/^([0-9]{1,}\.{0,1}[0-9]{1,})$/.test(this._obj.latitude)) {
        this._returnResult.state = 'false';
        this._returnResult.msgDesc = "Invalid Latitude";
        this._mflag = true;
      }
    }
    if (this._obj.longitude.length > 0) {
      if (!/^([0-9]{1,}\.{0,1}[0-9]{1,})$/.test(this._obj.longitude)) {
        this._returnResult.state = 'false';
        this._returnResult.msgDesc = "Invalid Longitude";
        this._mflag = true;
      }
    }
    if (this._returnResult.msgDesc == '') {

      let url: string = this.ics.cmsurl + 'service001/saveATMLocator';
      this._obj.sessionID = this.ics._profile.sessionID;
      this._obj.userid = this.ics._profile.userID;
      let json: any = this._obj;
      this.http.doPost(url, json).subscribe(
        data => {
          this._returnResult = data;
          if (this._returnResult.state) {
            this._obj.t1 = data.userid;
            jQuery("#mydelete").prop("disabled", false);
            this.showMsg(this._returnResult.msgDesc,true);

          } else {
            if (this._returnResult.msgCode == '0016') {
              this._sessionMsg = this._returnResult.msgDesc;
              this._returnResult.msgDesc = "";
              this.showMsg(this._returnResult.msgDesc,false);
            }
            else {
              this.showMsg(this._returnResult.msgDesc,false);
            }
          }
          this._mflag = true;
        },
        error => alert(error),
        () => { }
      );
    }
    else {
      this.showMsg(this._returnResult.msgDesc,false);
    }
  }

  showMessageAlert(msg) {
    this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
  }

  getBranchCode() {
    try {
      this.http.doGet(this.ics.cmsurl + 'serviceAdmLOV/getAllBranchCode').subscribe(
        data => {
          this.ref._lov3.ref024 = data.ref024;
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
        },
        () => { }
      );

    } catch (e) {
      alert(e);
    }
  }

  getLocationCbo() {
    try {
      this._mflag = false;
        let url: string = this.ics.cmsurl + 'serviceAdmLOV/getLovType';
        this._sessionObj.sessionID = this.ics._profile.sessionID;
        this._sessionObj.userID = this.ics._profile.userID;
        this._sessionObj.lovDesc ="Location Type";
        let json: any = this._sessionObj;
        this.http.doPost(url, json).subscribe(
          data => {
              if (data != null) {
                  if (data.msgCode == "0016") {
                      this.showMsg(data.msgDesc, false);
                  }
                  if (data.msgCode == "0000") {
                      if (!(data.lovType instanceof Array)) {
                          let m = [];
                          m[0] = data.lovType;
                          this.ref._lov3.refLocType = m;
                      } else {
                          this.ref._lov3.refLocType = data.lovType;
                      }
                      //this._obj.locationType = this.ref._lov3.refLocType[0].value;
                  }                                    
                  this._mflag = true;
              }           
          },
          
        error => {
          if (error._body.type == 'error') {
            alert("Connection Timed Out!");
          }
        },
        () => { }
      );

    } catch (e) {
      alert(e);
    }
  } 

  goReadBySyskey(p) {
    try {
      this._mflag = false;
      let url: string = this.ics.cmsurl + 'service001/getATMLocatorByID?id=' + this._key + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
      this.http.doGet(url).subscribe(
        data => {
          this._obj = data;
          if (this._obj.msgCode == '0016') {
            this._returnResult.msgDesc = "";
            this._sessionMsg = this._obj.msgDesc;
            this.showMsg(this._obj.msgDesc,false);
          }
          jQuery("#mydelete").prop("disabled", false);
          this._mflag = true;
        },
        error => {
          if (error._body.type == 'error') {
            alert("Connection Error!");
          }
        }, () => { });
    } catch (e) {
      alert(e);
    }
  }

  showMessage() {
    jQuery("#sessionalert").modal();
    Observable.timer(3000).subscribe(x => {
      this._router.navigate(['/login']);
      jQuery("#sessionalert").modal('hide');
    });
  }

  goNew() {
    this._obj = this.getDefaultObj();
    jQuery("#mydelete").prop("disabled", true);
  }

  goList() {
    this._router.navigate(['/LocatorList']);
  }

  restrictSpecialCharacter(event, fid, value) {
    if ((event.key >= '0' && event.key <= '9') || event.key == '.' || event.key == ',' || event.key == 'Home' || event.key == 'End' ||
        event.key == 'ArrowLeft' || event.key == 'ArrowRight' || event.key == 'Delete' || event.key == 'Backspace' || event.key == 'Tab' ||
        (event.which == 67 && event.ctrlKey === true) || (event.which == 88 && event.ctrlKey === true) ||
        (event.which == 65 && event.ctrlKey === true) || (event.which == 86 && event.ctrlKey === true)) {
        if (fid == 101) {
            if (/^[\\\"\'\;\:\>\|~`!@#\$%^&*\(\)]$/i.test(event.key)) {
                event.preventDefault();
            }
        }

        if (value.includes(".")) {
            if (/^[\\\"\'\;\:\>\|~`!@#\$%^&*.\(\)]$/i.test(event.key)) {
                event.preventDefault();
            }
        }
    }
    else {
        event.preventDefault();
    }
}
  goDelete() {
    this._mflag = false;
    this._obj.userid = this.ics._profile.userID;
    this._obj.username = this.ics._profile.userName;
    this._obj.sessionID = this.ics._profile.sessionID;
    let url: string = this.ics.cmsurl + 'service001/deleteATMLocator';
    let json: any = this._obj;
    this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "msgCode": "" };
    this.http.doPost(url, json).subscribe(
      data => {
        this._output1 = JSON.stringify(data);
        this._obj.userid = data.userid;
        this._returnResult = data;

        if (this._returnResult.state) {
          this._obj = { "sessionID": "", "msgCode": "", "msgDesc": "", "createdDate": "", "modifiedDate": "", "latitude": "", "longitude": "", "address": "", "phone1": "", "phone2": "", "branchCode": "", "name": "", "locationType": "All", "t1": "", "userid": "", "username": "" };

          jQuery("#mydelete").prop("disabled", true);
          this.showMsg(this._returnResult.msgDesc,true);
        } else {
          if (this._returnResult.msgCode == '0016') {
            this._sessionMsg = this._returnResult.msgDesc;
            this._returnResult.msgDesc = "";
            this.showMsg(this._returnResult.msgDesc,false);
          }
          else {
            this.showMsg(this._returnResult.msgDesc,false);
          }
        }
        this._mflag = true;
      },
      error => {
        if (error._body.type == 'error') {
            alert("Connection Timed Out!");
        }
    },
      () => { }
    );
    this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "msgCode": "" };
  }

  // about session validation
  sessionTimeoutMsg = "";
  _sessionObj = this.getSessionObj();
  getSessionObj() {
    return { "sessionID": "", "userID": "" ,"lovDesc":""};
  }

  checkSession() {
    try {
      let url: string = this.ics._apiurl + 'service001/checkSessionTime';

      this._sessionObj.sessionID = this.ics._profile.sessionID;
      this._sessionObj.userID = this.ics._profile.userID;
      let json: any = this._sessionObj;

      this.http.doPost(url, json).subscribe(
        data => {
          if (data != null) {
            if (data.code == "0016") {
              this.showMsgAlert(data.desc);
              this.logout();
            }

            if (data.code == "0014") {
              this.showMsgAlert(data.desc);
            }
          }
        },
        error => {
          if (error._body.type == "error") {
            alert("Connection Timed Out.");
          }
        },
        () => { }
      );
    } catch (e) {
      alert("Invalid URL.");
    }
  }

  showMsgAlert(msg) {
    this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
  }

  logout() {
    this._router.navigate(['/login']);
  }
  showMsg(msg, bool) {
    if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
    if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
    if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg }); }
  }
}