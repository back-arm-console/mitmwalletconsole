import { Component, Input, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { enableProdMode } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { RpIntercomService } from '../framework/rp-intercom.service';
import { RpInputComponent } from '../framework/rp-input.component';
import { RpHttpService } from '../framework/rp-http.service';
import { RpReferences } from '../framework/rp-references';
import { RpBean } from '../framework/rp-bean';
import { AdminPager } from '../util/adminpager.component';
import { ClientUtil } from '../util/rp-client.util';
declare var jQuery: any;
enableProdMode();
@Component({
    selector: 'version-history-list',
    template: ` 
    <div class="container col-md-12 col-sm-12 col-xs-12">
    <form class="form-inline">
        <legend>Version History List</legend>
		<div class="cardview list-height">
			
			<div class="row col-md-12">
				<div class="row col-md-4">
					<div  class="input-group">
						<span class="input-group-btn">
							<button class="btn btn-sm btn-primary" type="button" (click)="goNew();">New</button>
						</span> 
						<input id="textinput" name="textinput" type="text"  placeholder="Search" [(ngModel)]="vHobj.searchText" (keyup)="searchKeyup($event)" maxlength="30" class="form-control input-sm">
						<span class="input-group-btn">
							<button class="btn btn-sm btn-primary" type="button" (click)="Searching()" >
								<span class="glyphicon glyphicon-search"></span>Search
							</button>
						</span>        
					</div> 
				</div>		
                    <div class="pagerright">
					    <adminpager rpPageSizeMax="100" [(rpModel)]="vHobj.totalCount" (rpChanged)="changedPager($event)" ></adminpager> 
					</div>
			</div>
			<div class="row col-md-12" style="overflow-x:auto">
			<table class="table table-striped table-condensed table-hover tblborder">
				<thead>
					<tr>
						<th>Version Key</th>
						<th>App Code</th>
						<th>Version</th>
						<th>Version Title</th>
						<th>Description</th>    
						<th>Start Date</th>
						<th>Due Date</th>
						<th>Remark</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					<tr *ngFor="let obj of vHobj.data;let i=index">
						<td><a (click)="goto(obj.autokey)">{{obj.versionkey}}</a></td>
						<ng-container *ngFor="let item of ref._lov3.refAppCode">
							<td *ngIf="item.value==obj.appcode">{{item.caption}}</td>
						</ng-container>
						<td>{{obj.version}}</td>
						<td>{{obj.versiontitle}}</td>
						<td>{{obj.description}}</td>
						<td>{{obj.startdate}}</td>
						<td>{{obj.duedate}}</td>
						<td>{{obj.remark}}</td>
						<ng-container *ngFor="let item of ref._lov3.refVersionStatusCode">
							<td *ngIf="item.value==obj.status">{{item.caption}}</td>
						</ng-container>
					</tr>  
				</tbody>
            </table>
            </div>
            <!--<div align="center" *ngIf="_showListing" >
                Total {{vHobj.totalCount}}
            </div>-->
            
		</div>
    </form>
</div>
    <div [hidden]="_mflag">
      <div  id="loader" class="modal" ></div>
    </div>
    `
})

export class VersionHistoryListComponent {

    subscription: Subscription;
    _mflag :boolean;
    _util = new ClientUtil();
    _showListing=false;
    //about default obj
    vHobj = this.getDefaultObj();
    getDefaultObj() {
        return {
            "data": [{
                "autokey": "", "appcode": "", "versionkey": "", "version": "", "versiontitle": "",
                "description": "", "startdate": "", "duedate": "", "remark": "", "status": ""
            }], "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0,
            "sessionID": "", "userID": "", "msgCode": "", "msgDesc": ""
        };
    }

    _pgobj = {
        "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1,
        "end": 10, "size": 10, "totalcount": 0
    };

    // about session validation
    sessionTimeoutMsg = "";
    _sessionObj = this.getSessionObj();
    getSessionObj() {
        return { "sessionID": "", "userID": "" };
    }

    constructor(private ics: RpIntercomService,private l_util: ClientUtil, private _router: Router, private http: RpHttpService, private ref: RpReferences) {
        this.subscription = ics.rpbean$.subscribe(x => { })

        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        } else {
            this.checkSession();

            this.getAppCodes();
            this.getStatusCodes();

            this.search();
        }
    }

    checkSession() {
        try {
            let url: string = this.ics._apiurl + 'service001/checkSessionTime';

            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            let json: any = this._sessionObj;

            this.http.doPost(url, json).subscribe(
                data => {
                    if (data != null) {
                        if (data.code == "0016") {
                            this.showMsgAlert(data.desc);
                            this.logout();
                        }

                        if (data.code == "0014") {
                            this.showMsgAlert(data.desc);
                        }
                    }
                },
                error => {
                    if (error._body.type == "error") {
                        alert("Connection Timed Out.");
                    }
                },
                () => { }
            );
        } catch (e) {
            alert("Invalid URL.");
        }
    }

    showMsgAlert(msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    }

    logout() {
        this._router.navigate(['/login']);
    }

    getAppCodes() {
        try {
            let url: string = this.ics.cmsurl + 'serviceAdmLOV/getAppCodesForVersionHistory';

            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            let json: any = this._sessionObj;

            this.http.doPost(url, json).subscribe(
                data => {
                    if (data != null) {
                        if (data.msgCode == "0016") {
                            this.showMessage(data.msgDesc,false);
                            this.logout();
                        }

                        if (data.msgCode != "0000") {
                            this.showMessage(data.msgDesc,false);
                        }

                        if (data.msgCode == "0000") {
                            if (!(data.refAppCode instanceof Array)) {
                                let m = [];
                                m[0] = data.refAppCode;
                                this.ref._lov3.refAppCode = m;
                            } else {
                                this.ref._lov3.refAppCode = data.refAppCode;
                            }
                        }
                    }
                },
                error => {
                    if (error._body.type == "error") {
                        alert("Connection Timed Out.");
                    }
                },
                () => { }
            );
        } catch (e) {
            alert("Invalid URL.");
        }
    }

    getStatusCodes() {
        try {
            let url: string = this.ics.cmsurl + 'serviceAdmLOV/getStatusCodesForVersionHistory';

            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            let json: any = this._sessionObj;

            this.http.doPost(url, json).subscribe(
                data => {
                    if (data != null) {
                        if (data.msgCode == "0016") {
                            this.showMessage(data.msgDesc,false);
                            this.logout();
                        }

                        if (data.msgCode != "0000") {
                            this.showMessage(data.msgDesc,false);
                        }

                        if (data.msgCode == "0000") {
                            if (!(data.refVersionStatusCode instanceof Array)) {
                                let m = [];
                                m[0] = data.refVersionStatusCode;
                                this.ref._lov3.refVersionStatusCode = m;
                            } else {
                                this.ref._lov3.refVersionStatusCode = data.refVersionStatusCode;
                            }
                        }
                    }
                },
                error => {
                    if (error._body.type == "error") {
                        alert("Connection Timed Out.");
                    }
                },
                () => { }
            );
        } catch (e) {
            alert("Invalid URL.");
        }
    }

    search() {
        try {
            let url: string = this.ics.cmsurl + 'service001/getAllVersionHistory';

            this.vHobj.sessionID = this.ics._profile.sessionID;
            this.vHobj.userID = this.ics._profile.userID;
            let json: any = this.vHobj;

            this.http.doPost(url, json).subscribe(
                data => {
                    this._mflag = true;                    
                    let temp: any = this.vHobj;
                    if (data != null) {
                        this.vHobj = data;

                        if (this.vHobj.msgCode == "0016") {
                            this.showMessage(this.vHobj.msgDesc,false);
                            this.logout();
                        }

                        if (this.vHobj.msgCode != "0000") {
                            this._showListing=true;
                            this.showMessage(this.vHobj.msgDesc,false);
                            this.vHobj = temp;
                        }

                        if (this.vHobj.msgCode == "0000") {
                            this._showListing=true;
                            if (this.vHobj.totalCount == 0) {
                                this.showMessage(this.vHobj.msgDesc,true);
                            } else {
                                if (!(data.data instanceof Array)) {
                                    let m = [];
                                    m[0] = data.data;
                                    this.vHobj.data = m;
                                }
                            }
                            for (let iIndex = 0; iIndex < this.vHobj.data.length; iIndex++) {                               
                                this.vHobj.data[iIndex].startdate = this._util.changeStringtoDateDDMMYYYY(this.vHobj.data[iIndex].startdate);
                                this.vHobj.data[iIndex].duedate = this._util.changeStringtoDateDDMMYYYY(this.vHobj.data[iIndex].duedate);
                            }
                        }
                    }
                },
                error => {
                    if (error._body.type == "error") {
                        alert("Connection Timed Out.");
                    }
                },
                () => { }
            );
        } catch (e) {
            alert("Invalid URL.");
        }
    }

    goNew() {
        this._router.navigate(['/version-history', , { cmd: "NEW" }]);
    }

    searchKeyup(e: any) {
        if (e.which == 13) {
            this.vHobj.currentPage = 1;
            this.search();
        }
    }

    searching() {
        this.vHobj.currentPage = 1;
        this.search();
    }

    changedPager(event) {
        if (this.vHobj.totalCount != 0) {
            this._pgobj = event;
            let current = this.vHobj.currentPage;
            let size = this.vHobj.pageSize;
            this.vHobj.currentPage = this._pgobj.current;
            this.vHobj.pageSize = this._pgobj.size;

            if (this._pgobj.current != current || this._pgobj.size != size) {
                this.search();
            }
        }
    }

    goto(p) {
        this._router.navigate(['/version-history', 'read', p]);
    }
    showMessage(msg, bool) {
        if (bool == true) { this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg }); }
        if (bool == false) { this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg }); }
        if (bool == undefined) { this.ics.sendBean({ "t1": "rp-alert", "t2": "primary", "t3": msg }); }
    }
}