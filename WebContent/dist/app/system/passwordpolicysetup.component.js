"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var Rx_1 = require('rxjs/Rx');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
core_1.enableProdMode();
var PasswordPolicyComponent = (function () {
    function PasswordPolicyComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
        this._pswobj = this.getDefaultObj();
        this.sessionAlertMsg = "";
        this._mflag = false;
        this._util = new rp_client_util_1.ClientUtil();
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = false;
            this.checkSession();
            this.msghide = true;
            this._pswobj = this.getDefaultObj();
            this._editEvent = false;
        }
    }
    PasswordPolicyComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            _this.goReadBySyskey();
        });
    };
    PasswordPolicyComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    PasswordPolicyComponent.prototype.getDefaultObj = function () {
        this._editEvent = false;
        return { "pswminlength": 0, "pswmaxlength": 0, "spchar": 0, "upchar": 0, "lowerchar": 0, "pswno": 0, "msgCode": "", "msgDesc": "", "sessionID": "", "userID": "" };
    };
    PasswordPolicyComponent.prototype.goReadBySyskey = function () {
        var _this = this;
        this._mflag = false;
        var url = this.ics._apiurl + 'service001/readPswPolicy?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID + '&formID=pwdpolicy';
        console.log(JSON.stringify(url));
        this.http.doGet(url).subscribe(function (data) {
            if (data.msgCode == '0016') {
                _this.sessionAlertMsg = data.msgDesc;
                _this.showMessage();
            }
            else {
                _this._pswobj = data;
            }
            _this._mflag = true;
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Error!");
            }
        });
    };
    PasswordPolicyComponent.prototype.goList = function () {
        this._router.navigate(['/ActivateUserList']);
    };
    PasswordPolicyComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this.goLogOut();
        });
    };
    PasswordPolicyComponent.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    PasswordPolicyComponent.prototype.showMessageAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    PasswordPolicyComponent.prototype.goEdit = function () {
        this._editEvent = true;
        jQuery("#btnupdate").prop("disabled", false);
    };
    PasswordPolicyComponent.prototype.goSave = function () {
        var _this = this;
        this._mflag = false;
        var key = localStorage.getItem("key");
        this._pswobj.sessionID = this.ics._profile.sessionID;
        this._pswobj.userID = this.ics._profile.userID;
        var url = this.ics._apiurl + 'service001/savePswPolicy';
        var json = this._pswobj;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data.msgCode == '0016') {
                _this.sessionAlertMsg = data.msgDesc;
                _this.showMsg(data.msgDesc, false);
            }
            else if (data.msgCode == '0000') {
                _this._editEvent = false;
                jQuery("#btnupdate").prop("disabled", true);
                _this.showMsg(data.msgDesc, true);
            }
            else {
                _this._editEvent = false;
                _this.showMsg(data.msgDesc, false);
            }
            _this._mflag = true;
        }, function (error) { return alert(error); }, function () { });
    };
    PasswordPolicyComponent.prototype.goValidate = function () {
        var total = Number(this._pswobj.pswno) + Number(this._pswobj.spchar) + Number(this._pswobj.upchar) + Number(this._pswobj.lowerchar);
        /* if (Number(this._pswobj.pswminlength) < 0 || Number(this._pswobj.pswmaxlength) < 0 || Number(this._pswobj.pswno) < 0 || Number(this._pswobj.spchar) < 0 || Number(this._pswobj.upchar) < 0 || Number(this._pswobj.lowerchar) < 0) {
          this.showMsg("Input data should not be negative number",false);
        } */
        if (Number(this._pswobj.pswminlength) == 0) {
            this.showMsg("Minimum length must be greater than 0", false);
        }
        else if (Number(this._pswobj.pswmaxlength) != 0 && Number(this._pswobj.pswminlength) > Number(this._pswobj.pswmaxlength)) {
            this.showMsg("Minimum length must not be greater than maximum length", false);
        }
        else if (this._pswobj.pswminlength > total) {
            this._mflag = true;
            this.showMsg("Password length must be greater than Minimum length", false);
        }
        else if (this._pswobj.pswmaxlength < total) {
            this._mflag = true;
            this.showMsg("password length must be less than Maximum length", false);
        }
        else
            this.goSave();
    };
    PasswordPolicyComponent.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    PasswordPolicyComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsgAlert(data.desc);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsgAlert(data.desc);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    PasswordPolicyComponent.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    PasswordPolicyComponent.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    PasswordPolicyComponent.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    PasswordPolicyComponent = __decorate([
        core_1.Component({
            selector: 'passworpolicy-setup',
            template: "\n  <div class=\"container-fluid\">\n  <div class=\"row clearfix\">\n  <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n\t<form class=\"form-horizontal\" class= \"form-horizontal\" (ngSubmit)=\"goValidate()\">\n    <legend>Password Policy</legend>\n    \n\t\t<div class=\"cardview list-height\">\n        <div class=\"row col-md-12\">      \n\t\t\t\t  <button class=\"btn btn-sm btn-primary\" disabled id=\"btnupdate\"  type=\"submit\">Save</button>\n\t\t\t\t  <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goEdit()\">Edit</button>\n\t\t\t\t</div>\n            \n      <div class=\"row col-md-12\">&nbsp;</div>         \n\t\t\t<div class=\"row col-md-6\">    \n\t\t\t  <div class=\"form-group\">\n        <label class=\"col-md-3\">Minimum Length </label>\n        <div class=\"col-md-4\">\n        <input min=\"0\" type=\"number\" *ngIf=\"_editEvent == false\" [(ngModel)]='_pswobj.pswminlength' [ngModelOptions]=\"{standalone: true}\" class=\"form-control input-sm\" disabled readonly>\n\t\t\t  <input min=\"0\" type=\"number\" *ngIf=\"_editEvent == true\" [(ngModel)]='_pswobj.pswminlength' [ngModelOptions]=\"{standalone: true}\" class=\"form-control input-sm\">\n        </div>\t\t\t  \n\t\t\t  </div>\n        <div class=\"form-group\">\n        <label class=\"col-md-3\">Maximum Length</label>\n        <div class=\"col-md-4\">\n        <input min=\"0\" type=\"number\" *ngIf=\"_editEvent == false\" [(ngModel)]='_pswobj.pswmaxlength' [ngModelOptions]=\"{standalone: true}\" class=\"form-control input-sm\" disabled readonly>\n\t\t\t  <input min=\"0\" type=\"number\" *ngIf=\"_editEvent == true\" [(ngModel)]='_pswobj.pswmaxlength' [ngModelOptions]=\"{standalone: true}\" class=\"form-control input-sm\">\n        </div>\t\t\t\n\t\t\t  </div>    \n        <div class=\"form-group\">\n        <label class=\"col-md-3\">Special Character</label>\n        <div class=\"col-md-4\">\n        <input min=\"0\" type=\"number\" *ngIf=\"_editEvent == false\" [(ngModel)]='_pswobj.spchar' [ngModelOptions]=\"{standalone: true}\" class=\"form-control input-sm\" disabled readonly>\n\t\t\t  <input min=\"0\" type=\"number\" *ngIf=\"_editEvent == true\" [(ngModel)]='_pswobj.spchar' [ngModelOptions]=\"{standalone: true}\" class=\"form-control input-sm\">\n        </div>\n\t\t\t \n\t\t\t  </div>\n        <div class=\"form-group\">\n        <label class=\"col-md-3\">Uppercase Character</label>\n        <div class=\"col-md-4\">\n        <input min=\"0\" type=\"number\" *ngIf=\"_editEvent == false\" [(ngModel)]='_pswobj.upchar' [ngModelOptions]=\"{standalone: true}\" class=\"form-control input-sm\" disabled readonly>\n\t\t\t  <input min=\"0\" type=\"number\" *ngIf=\"_editEvent == true\" [(ngModel)]='_pswobj.upchar' [ngModelOptions]=\"{standalone: true}\" class=\"form-control input-sm\">\n        </div>\n\t\t\t  \n\t\t\t  </div>\n        <div class=\"form-group\">\n        <label class=\"col-md-3\">Lowercase Character</label>\n        <div class=\"col-md-4\">\n        <input min=\"0\" type=\"number\" *ngIf=\"_editEvent == false\" [(ngModel)]='_pswobj.lowerchar' [ngModelOptions]=\"{standalone: true}\" class=\"form-control input-sm\" disabled readonly>\n\t\t\t  <input min=\"0\" type=\"number\" *ngIf=\"_editEvent == true\" [(ngModel)]='_pswobj.lowerchar' [ngModelOptions]=\"{standalone: true}\" class=\"form-control input-sm\">\n        </div>\n\t\t\t  \n\t\t\t  </div>\n        <div class=\"form-group\">\n        <label class=\"col-md-3\">Number</label>\n        <div class=\"col-md-4\">\n        <input min=\"0\" type=\"number\" *ngIf=\"_editEvent == false\" [(ngModel)]='_pswobj.pswno' [ngModelOptions]=\"{standalone: true}\" class=\"form-control input-sm\" disabled readonly>\n\t\t\t  <input min=\"0\" type=\"number\" *ngIf=\"_editEvent == true\" [(ngModel)]='_pswobj.pswno' [ngModelOptions]=\"{standalone: true}\" class=\"form-control input-sm\">\n        </div>\n\t\t\t   \n\t\t\t  </div>\n      </div> \n      </div>\n    </form> \n</div>\n</div>\n</div>\n    \n    <div id=\"sessionalert\" class=\"modal fade\">\n      <div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n          <div class=\"modal-body\">\n            <p>{{sessionAlertMsg}}</p>\n          </div>\n          <div class=\"modal-footer\">\n          </div>\n        </div>\n      </div>\n    </div>\n\n<div [hidden] = \"_mflag\">\n  <div  id=\"loader\" class=\"modal\" ></div>\n</div>\n    \n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], PasswordPolicyComponent);
    return PasswordPolicyComponent;
}());
exports.PasswordPolicyComponent = PasswordPolicyComponent;
//# sourceMappingURL=passwordpolicysetup.component.js.map