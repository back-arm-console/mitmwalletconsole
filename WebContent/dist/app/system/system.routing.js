"use strict";
var router_1 = require('@angular/router');
var usersetup_component_1 = require('./usersetup.component');
var user_list_component_1 = require('./user-list.component');
var versionhistory_component_1 = require('./versionhistory.component');
var versionhistory_list_component_1 = require('./versionhistory-list.component');
var passwordpolicysetup_component_1 = require('./passwordpolicysetup.component');
var forcesignout_component_1 = require('./forcesignout.component');
var menusetup_component_1 = require('./menusetup.component');
var menusetup_list_component_1 = require('./menusetup-list.component');
var rolesetup_component_1 = require('./rolesetup.component');
var rolesetup_list_component_1 = require('./rolesetup-list.component');
var lovsetup_component_1 = require('./lovsetup.component');
var lovsetup_list_component_1 = require('./lovsetup-list.component');
var faq_menu_component_1 = require('./faq-menu.component');
var faq_list_component_1 = require('./faq-list.component');
var locatorsetup_component_1 = require('./locatorsetup.component');
var locatorsetup_list_component_1 = require('./locatorsetup-list.component');
var systemRoutes = [
    { path: 'password-policy', component: passwordpolicysetup_component_1.PasswordPolicyComponent },
    { path: 'forced-sign-out', component: forcesignout_component_1.ForceSignOutComponent },
    { path: 'QuickPayLOVSetup', component: lovsetup_component_1.LovSetupComponent },
    { path: 'QuickPayLOVSetup/:cmd', component: lovsetup_component_1.LovSetupComponent },
    { path: 'QuickPayLOVSetup/:cmd/:id', component: lovsetup_component_1.LovSetupComponent },
    { path: 'lovsetuplist', component: lovsetup_list_component_1.LOVSetupListComponent },
    { path: 'version-history', component: versionhistory_component_1.VersionHistoryComponent },
    { path: 'version-history/:cmd', component: versionhistory_component_1.VersionHistoryComponent },
    { path: 'version-history/:cmd/:id', component: versionhistory_component_1.VersionHistoryComponent },
    { path: 'version-history-list', component: versionhistory_list_component_1.VersionHistoryListComponent },
    { path: 'user-setup', component: usersetup_component_1.UserSetupComponent },
    { path: 'user-setup/:cmd', component: usersetup_component_1.UserSetupComponent },
    { path: 'user-setup/:cmd/:id', component: usersetup_component_1.UserSetupComponent },
    { path: 'user-list', component: user_list_component_1.UserListComponent },
    { path: 'Menu', component: menusetup_component_1.MenuSetupComponent },
    { path: 'Menu/:cmd', component: menusetup_component_1.MenuSetupComponent },
    { path: 'Menu/:cmd/:id', component: menusetup_component_1.MenuSetupComponent },
    { path: 'menusetup-list', component: menusetup_list_component_1.MenuSetupListComponent },
    { path: 'Role', component: rolesetup_component_1.RoleSetupComponent },
    { path: 'Role/:cmd', component: rolesetup_component_1.RoleSetupComponent },
    { path: 'Role/:cmd/:id', component: rolesetup_component_1.RoleSetupComponent },
    { path: 'rolesetup-list', component: rolesetup_list_component_1.RoleSetupListComponent },
    { path: 'faq-menu', component: faq_menu_component_1.FAQMenu },
    { path: 'faq-menu/:cmd', component: faq_menu_component_1.FAQMenu },
    { path: 'faq-menu/:cmd/:id', component: faq_menu_component_1.FAQMenu },
    { path: 'faq-list', component: faq_list_component_1.FAQList },
    { path: 'locator-setup', component: locatorsetup_component_1.LocatorSetupComponent },
    { path: 'locator-setup/:cmd', component: locatorsetup_component_1.LocatorSetupComponent },
    { path: 'locator-setup/:cmd/:id', component: locatorsetup_component_1.LocatorSetupComponent },
    { path: 'LocatorList', component: locatorsetup_list_component_1.LocatorSetupListComponent },
];
exports.SystemRouting = router_1.RouterModule.forChild(systemRoutes);
//# sourceMappingURL=system.routing.js.map