"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_http_service_1 = require('../framework/rp-http.service');
// RP Framework
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_client_util_1 = require('../util/rp-client.util');
core_1.enableProdMode();
var RoleSetupListComponent = (function () {
    function RoleSetupListComponent(ics, _router, http) {
        this.ics = ics;
        this._router = _router;
        this.http = http;
        // Application Specific
        this._searchVal = ""; // simple search
        this._showListing = false;
        this._flagas = true; // flag advance search
        this._col_list = [{ "col_no": "1", "flag": false }, { "col_no": "2", "flag": false }];
        this._sorting = { _sort_type: "asc", _sort_col: "1" };
        this._mflag = true;
        this._util = new rp_client_util_1.ClientUtil();
        this.sessionAlertMsg = "";
        this._roleobj = { "data": [{ "syskey": 0, "autokey": 0, "createdDate": "", "modifiedDate": "", "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "usersyskey": 0, "t1": "", "t2": "", "t3": "", "n1": 0, "n2": 0, "n3": 0 }], "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0, "msgCode": "", "msgDesc": "" };
        this._pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 0 };
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = false;
            this.search();
        }
    }
    // list sorting part
    RoleSetupListComponent.prototype.changeDefault = function () {
        for (var i = 0; i < this._col_list.length; i++) {
            this._col_list[i].flag = false;
        }
    };
    RoleSetupListComponent.prototype.addSort = function (e) {
        for (var i = 0; i < this._col_list.length; i++) {
            if (this._col_list[i].col_no == e) {
                var _tmp_flag = this._col_list[i].flag;
                this.changeDefault();
                this._col_list[i].flag = !_tmp_flag;
                this._sorting._sort_type = !_tmp_flag ? "desc" : "asc";
                this._sorting._sort_col = e;
                break;
            }
        }
    };
    RoleSetupListComponent.prototype.changedPager = function (event) {
        if (this._roleobj.totalCount != 0) {
            this._pgobj = event;
            var current = this._roleobj.currentPage;
            var size = this._roleobj.pageSize;
            this._roleobj.currentPage = this._pgobj.current;
            this._roleobj.pageSize = this._pgobj.size;
            if (this._pgobj.current != current || this._pgobj.size != size) {
                this.search();
            }
        }
    };
    RoleSetupListComponent.prototype.search = function () {
        var _this = this;
        try {
            this._mflag = false;
            var url = this.ics._apiurl + 'service001/getRoleList?searchVal=' + encodeURIComponent(this._roleobj.searchText) + '&pagesize=' + this._roleobj.pageSize + '&currentpage=' + this._roleobj.currentPage + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
            this.http.doGet(url).subscribe(function (response) {
                if (response.msgCode == '0016') {
                    _this.sessionAlertMsg = response.msgDesc;
                    _this.showMessage(response.msgDesc, false);
                }
                else {
                    if (response.totalCount == 0) {
                        _this._roleobj = response,
                            _this.showMessage("Data not found!", false);
                    }
                    else if (response.totalCount == 1) {
                        var m = [];
                        m[0] = response.data;
                        _this._roleobj.data = m[0];
                        _this._roleobj.totalCount = response.totalCount;
                    }
                    else {
                        _this._roleobj = response;
                    }
                    _this._showListing = (_this._roleobj != undefined);
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    RoleSetupListComponent.prototype.searchKeyup = function (e) {
        if (e.which == 13) {
            this._roleobj.currentPage = 1;
            this._msghide = true;
            this._recordhide = false;
            this.search();
        }
    };
    RoleSetupListComponent.prototype.Searching = function () {
        this._roleobj.currentPage = 1;
        this._msghide = true;
        this._recordhide = false;
        this.search();
    };
    RoleSetupListComponent.prototype.goNew = function () {
        this._router.navigate(['/Role', 'new']);
    };
    RoleSetupListComponent.prototype.goto = function (p) {
        this._router.navigate(['/Role', 'read', p]);
    };
    //showMessage() {
    //    jQuery("#sessionalert").modal();
    //    Observable.timer(3000).subscribe(x => {
    //        this.goLogOut();
    //    });
    //}
    RoleSetupListComponent.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    RoleSetupListComponent.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "primary", "t3": msg });
        }
    };
    RoleSetupListComponent = __decorate([
        core_1.Component({
            selector: 'rolesetup-list',
            template: " \n    <div class=\"container-fluid\">\n\t<form class=\"form-horizontal\">\n\t\t<legend>Role List</legend>\n\t\t<div class=\"cardview list-height\">\n\t\t\t<div class=\"row col-md-12\">\n            <div class=\"row col-md-3\">\n                <div  class=\"input-group\">\n                    <span class=\"input-group-btn\"  style=\"width:20px;\">\n                        <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goNew();\">New</button>\n                    </span> \n                    <input id=\"textinput\" name=\"textinput\" type=\"text\"  placeholder=\"Search\" [(ngModel)]=\"_roleobj.searchText\"  maxlength=\"50\" class=\"form-control input-sm\" (keyup)=\"searchKeyup($event)\" [ngModelOptions]=\"{standalone: true}\">\n                    <span class=\"input-group-btn\">\n                        <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"Searching()\" >\n                            <span class=\"glyphicon glyphicon-search\"></span>Search\n                        </button>\n                    </span>        \n                </div>  \n            </div>\n                <div class=\"pagerright\">\n\t\t\t\t\t<adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"_roleobj.totalCount\" (rpChanged)=\"changedPager($event)\" ></adminpager> \n\t\t\t\t</div> \n\t\t\t</div>\n\t\t\t  <div *ngIf=\"_showListing\">\n\t\t\t\t  <table class=\"table table-striped table-condensed table-hover tblborder\">\n\t\t\t\t\t<thead>\n\t\t\t\t\t <tr>\n\t\t\t\t\t\t<th class=\"left\">Code</th>\n\t\t\t\t\t\t<th class=\"left\">Description</th>   \n\t\t\t\t\t</tr>\n\t\t\t\t  </thead>\n\t\t\t\t  <tbody>\n\t\t\t\t\t<tr *ngFor=\"let obj of _roleobj.data\">\n\t\t\t\t\t\t<td><a (click)=\"goto(obj.syskey)\">{{obj.t1}}</a></td>\n\t\t\t\t\t\t<td>{{obj.t2}}</td>\n\t\t\t\t\t</tr>  \n\t\t\t\t  </tbody>\n\t\t\t\t  </table>\n\t\t\t  </div>\n\t\t\t<div align=\"center\" *ngIf=\"_showListing\">\n\t\t\t   Total {{_roleobj.totalCount}}\n\t\t\t</div>\n\t\t</div> \n\t</form>\n</div>\n    <div [hidden] = \"_mflag\">\n        <div  id=\"loader\" class=\"modal\" ></div>\n    </div>\n   "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_http_service_1.RpHttpService])
    ], RoleSetupListComponent);
    return RoleSetupListComponent;
}());
exports.RoleSetupListComponent = RoleSetupListComponent;
//# sourceMappingURL=rolesetup-list.component.js.map