"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
// RP Framework
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_client_util_1 = require('../util/rp-client.util');
// Application Specific
var LOVSetupListComponent = (function () {
    function LOVSetupListComponent(ics, _router, http) {
        this.ics = ics;
        this._router = _router;
        this.http = http;
        this._col_list = [{ "col_no": "1", "flag": false }, { "col_no": "2", "flag": false }];
        this._sorting = { _sort_type: "asc", _sort_col: "1" };
        this._mflag = true;
        this._util = new rp_client_util_1.ClientUtil();
        this.sessionAlertMsg = "";
        this._totalCount = 1;
        this._shownull = false;
        this._showListing = false;
        this._list = { "data": [{ "sysKey": 0, "lovNo": "", "lovDesc2": "", "lovType": "" }], "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0 };
        this._pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 1 };
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = false;
            this.search();
        }
    }
    // list sorting part
    LOVSetupListComponent.prototype.changeDefault = function () {
        for (var i = 0; i < this._col_list.length; i++) {
            this._col_list[i].flag = false;
        }
    };
    LOVSetupListComponent.prototype.addSort = function (e) {
        for (var i = 0; i < this._col_list.length; i++) {
            if (this._col_list[i].col_no == e) {
                var _tmp_flag = this._col_list[i].flag;
                this.changeDefault();
                this._col_list[i].flag = !_tmp_flag;
                this._sorting._sort_type = !_tmp_flag ? "desc" : "asc";
                this._sorting._sort_col = e;
                break;
            }
        }
    };
    LOVSetupListComponent.prototype.changedPager = function (event) {
        if (this._list.totalCount != 0) {
            this._pgobj = event;
            var current = this._list.currentPage;
            var size = this._list.pageSize;
            this._list.currentPage = this._pgobj.current;
            this._list.pageSize = this._pgobj.size;
            if (this._pgobj.current != current || this._pgobj.size != size) {
                this.search();
            }
        }
    };
    LOVSetupListComponent.prototype.search = function () {
        var _this = this;
        this._mflag = false;
        try {
            var url = this.ics._apiurl + 'service001/lovSetuplist';
            var json = this._list;
            console.log(JSON.stringify(url));
            console.log(JSON.stringify(json));
            this.http.doPost(url, json).subscribe(function (res) {
                _this._mflag = false;
                _this._shownull = false;
                if (res.msgCode == '0016') {
                    _this.sessionAlertMsg = res.msgDesc;
                    _this.showMessage(res.msgDesc, false);
                }
                else {
                    if (res != null) {
                        _this._totalCount = res.totalCount;
                        _this._list = res;
                        if (_this._totalCount == 0) {
                            //this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Data not found!" });
                            _this.showMessage("Data not found!", false);
                        }
                        if (res.data != null) {
                            if (!(res.data instanceof Array)) {
                                /* let m = [];
                                m[0] = response.commdetailArr;
                                this._flexobj.commdetailArr = m;
                                this._flexobj.totalCount = response.totalCount; */
                                var m = [];
                                m[0] = res.data;
                                _this._list.data = m;
                                _this._list.totalCount = res.totalCount;
                            }
                            else {
                                _this._list = res;
                            }
                        }
                        _this._showListing = (_this._list != undefined);
                    }
                    else {
                        _this._shownull = true;
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    LOVSetupListComponent.prototype.searchKeyup = function (e) {
        if (e.which == 13) {
            this._list.currentPage = 1;
            this._msghide = true;
            this._recordhide = false;
            this.search();
        }
    };
    LOVSetupListComponent.prototype.Searching = function () {
        this._list.currentPage = 1;
        this._msghide = true;
        this._recordhide = false;
        this.search();
    };
    LOVSetupListComponent.prototype.goto = function (p) {
        this._router.navigate(['/QuickPayLOVSetup', 'read', p]);
    };
    //showMessage() {
    //  jQuery("#sessionalert").modal();
    // Observable.timer(3000).subscribe(x => {
    //   this.goLogOut();
    //  });
    // }
    LOVSetupListComponent.prototype.goNew = function () {
        this._router.navigate(['/QuickPayLOVSetup', 'new']);
    };
    LOVSetupListComponent.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    LOVSetupListComponent.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "primary", "t3": msg });
        }
    };
    LOVSetupListComponent = __decorate([
        core_1.Component({
            selector: 'lovsetup-list',
            template: " \n  <div class=\"container-fluid\">\n\t<form class=\"form-horizontal\">\n\t\t<legend>List of Values</legend>\n\t\t<div class=\"cardview list-height\">\n\t\t\t<div class=\"row col-md-12\">\n\t\t\t\t<div class=\"row col-md-3\">\n\t\t\t\t\t<div  class=\"input-group\">\n\t\t\t\t\t\t<span class=\"input-group-btn input-md\">\n\t\t\t\t\t\t\t<button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goNew();\">New</button>\n\t\t\t\t\t\t</span> \n\t\t\t\t\t\t<input id=\"textinput\" name=\"textinput\" type=\"text\"  placeholder=\"Search\" [(ngModel)]=\"_list.searchText\" (keyup)=\"searchKeyup($event)\" [ngModelOptions]=\"{standalone: true}\" maxlength=\"30\" class=\"form-control input-sm\">\n\t\t\t\t\t\t<span class=\"input-group-btn input-md\">\n\t\t\t\t\t\t\t<button class=\"btn btn-sm btn-primary input-md \" type=\"button\" (click)=\"Searching()\" >\n\t\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-search\"></span>Search\n\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t</span>        \n\t\t\t\t\t</div> \n\t\t\t\t</div>\t\t\n          <div class=\"pagerright\">\n\t\t\t\t\t    <adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"_list.totalCount\" (rpChanged)=\"changedPager($event)\" ></adminpager> \n\t\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t\t\t<!--<div *ngIf=\"_shownull!=false\" style=\"color:#ccc;\">No result found!</div> \n          <div *ngIf=\"_showListing\">-->\n          <div class=\"row col-md-12\" style=\"overflow-x:auto\">\n\t\t\t\t\t\t<table class=\"table table-striped table-condensed table-hover tblborder\" id=\"tblstyle\"><!--style=\"width: 750px\"-->\n\t\t\t\t\t\t  <thead>\n\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t  <th class=\"left\" >Code</th>\n\t\t\t\t\t\t\t  <th class=\"left\">Description</th>   \n\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t  </thead>\n\t\t\t\t\t\t  <tbody>\n\t\t\t\t\t\t\t<tr *ngFor=\"let obj of  _list.data\">\n\t\t\t\t\t\t\t  <td><a (click)=\"goto(obj.sysKey)\">{{obj.lovNo}}</a></td>\n\t\t\t\t\t\t\t  <td>{{obj.lovDesc2}}</td>                                      \n\t\t\t\t\t\t\t</tr> \n\t\t\t\t\t\t  </tbody>\n\t\t\t\t\t\t</table>\n\t\t\t\t\t</div>\n\t\t\t\t\t<!--<div align=\"center\" *ngIf=\"_showListing\">\n\t\t\t\t\t   Total {{_list.totalCount}}\n\t\t\t\t\t</div>-->\t\t\t\n\t\t\t</div>\t\n\t</form>\n</div> \n  <div [hidden] = \"_mflag\">\n    <div  id=\"loader\" class=\"modal\" ></div>\n  </div>   \n   "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_http_service_1.RpHttpService])
    ], LOVSetupListComponent);
    return LOVSetupListComponent;
}());
exports.LOVSetupListComponent = LOVSetupListComponent;
//# sourceMappingURL=lovsetup-list.component.js.map