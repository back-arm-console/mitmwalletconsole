"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
core_1.enableProdMode();
var FAQList = (function () {
    function FAQList(ics, _router, 
        //private sanitizer: DomSanitizer, 
        http, l_util, ref) {
        this.ics = ics;
        this._router = _router;
        this.http = http;
        this.l_util = l_util;
        this.ref = ref;
        this._shownull = false;
        this._OperationMode = "";
        this._mflag = false;
        this._divexport = false;
        //_printUrl: SafeResourceUrl;
        // this.imageLink = this.ics._imglink + "/upload/image/userProfile/";
        this._isLoading = true;
        this._SearchString = "";
        this._showListing = false;
        this._showPagination = false;
        this._toggleSearch = true; // true - Simple Search, false - Advanced Search
        this._togglePagination = true;
        this.mstatus = 0;
        this._font = "";
        this._ButtonInfo = { "role": "", "formname": "", "button": "" };
        this._FilterDataset = {
            filterList: [
                { "itemid": "", "caption": "", "fieldname": "", "datatype": "", "condition": "", "t1": "", "t2": "" }
            ],
            "pageNo": 1, "pageSize": 10, "filterSource": 0, "sessionID": this.ics._profile.sessionID, "userID": this.ics._profile.userID, "userName": this.ics._profile.userName
        };
        this._ListingDataset = {
            faqData: [
                {
                    "srno": 0, "syskey": 0, "autokey": 0, "createddate": "", "modifiedDate": "", "status": 0, "userID": "", "questionEng": "", "answerEng": "", "questionUni": "",
                    "answerUni": "", "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "state": false
                }
            ],
            "pageNo": 0, "pageSize": 0, "totalCount": 1, "userID": "", "userName": ""
        };
        this._sessionObj = this.getSessionObj();
        this.mstatus = ics._profile.loginStatus;
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
    }
    FAQList.prototype.getSessionObj = function () {
        return { "sessionID": this.ics._profile.sessionID, "userID": this.ics._profile.userID };
    };
    FAQList.prototype.ngOnInit = function () {
        this.filterSearch();
        this.loadAdvancedSearchData();
    };
    FAQList.prototype.goto = function (p) {
        console.log("p=" + JSON.stringify(p));
        this.ics.sendBean({ "faqData": p });
        this._router.navigate(['/faq-menu', 'read', p]);
    };
    FAQList.prototype.goNew = function () {
        this._router.navigate(['/faq-menu', 'new']);
    };
    FAQList.prototype.ngAfterViewChecked = function () {
        this._isLoading = false;
    };
    FAQList.prototype.loadAdvancedSearchData = function () {
        this._FilterList =
            [
                { "itemid": "1", "caption": "Search String", "fieldname": "SearchString", "datatype": "simplesearch", "condition": "c", "t1": "", "t2": "", "t3": "" }
            ];
    };
    FAQList.prototype.btnToggleSearch_onClick = function () {
        this.toggleSearch(!this._toggleSearch);
    };
    FAQList.prototype.btnTogglePagination_onClick = function () {
        this.togglePagination(!this._togglePagination);
    };
    FAQList.prototype.toggleSearch = function (aToggleSearch) {
        // Set Flag
        this._toggleSearch = aToggleSearch;
        // Clear Simple Search
        if (!this._toggleSearch)
            jQuery("#isInputSearch").val("");
        // Enable/Disabled Simple Search Textbox
        jQuery('#isInputSearch').prop('disabled', !this._toggleSearch);
        // Hide Advanced Search
        if (this._toggleSearch)
            this.ics.send001("CLEAR");
        // Show/Hide Advanced Search    
        //  
        if (this._toggleSearch)
            jQuery("#asAdvancedSearch").css("display", "none"); // true     - Simple Search
        else
            jQuery("#asAdvancedSearch").css("display", "inline-block"); // false    - Advanced Search
        // Set Icon 
        //  
        if (this._toggleSearch)
            jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-up").addClass("glyphicon glyphicon-menu-down"); // true     - Simple Search
        else
            jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-down").addClass("glyphicon glyphicon-menu-up"); // false    - Advanced Search
        // Set Tooltip
        //
        var l_Tooltip = (this._toggleSearch) ? "Expand" : "Collapse";
        jQuery('#btnToggleSearch').attr('title', l_Tooltip);
    };
    FAQList.prototype.togglePagination = function (aTogglePagination) {
        // Set Flag
        this._togglePagination = aTogglePagination;
        // Show/Hide Pagination
        //
        if (this._showPagination && this._togglePagination)
            jQuery("#divPagination").css("display", "inline-block");
        else
            jQuery("#divPagination").css("display", "none");
        // Rotate Icon
        //
        if (!this._togglePagination)
            jQuery("#lblTogglePagination").css("transform", "rotate(270deg)");
        else
            jQuery("#lblTogglePagination").css("transform", "rotate(90deg)");
        // Set Icon Position
        if (!this._togglePagination)
            jQuery("#lblTogglePagination").css({ "margin-left": "-5px", "margin-top": "0px" });
        else
            jQuery("#lblTogglePagination").css({ "margin-left": "-5px", "margin-top": "-1px" });
        // Set Tooltip
        //
        var l_Tooltip = (this._togglePagination) ? "Collapse" : "Expand";
        jQuery('#btnTogglePagination').attr('title', l_Tooltip);
    };
    FAQList.prototype.btnClose_MouseEnter = function () {
        jQuery("#btnClose").removeClass('btn btn-sm btn-secondary').addClass('btn btn-sm btn-danger');
    };
    FAQList.prototype.btnClose_MouseLeave = function () {
        jQuery("#btnClose").removeClass('btn btn-sm btn-danger').addClass('btn btn-sm btn-secondary');
    };
    FAQList.prototype.closePage = function () {
        this._router.navigate(['000', { cmd: "READ", p1: this.ics._profile.userID }]);
    };
    FAQList.prototype.renderAdvancedSearch = function (event) {
        this.toggleSearch(!event);
    };
    FAQList.prototype.filterAdvancedSearch = function (event) {
        this._FilterDataset.filterSource = 1;
        this._FilterDataset.filterList = event;
        if (this._OperationMode != "prepareFilter") {
            if (this._FilterDataset.pageNo == 1)
                this.filterRecords();
            else
                this._FilterDataset.pageNo = 1;
        }
    };
    FAQList.prototype.changedPager = function (event) {
        if (!this._isLoading) {
            var l_objPagination = event.obj;
            this._FilterDataset.pageNo = l_objPagination.current;
            this._FilterDataset.pageSize = l_objPagination.size;
            this.filterRecords();
        }
    };
    FAQList.prototype.filterSearch = function () {
        if (this._toggleSearch)
            this.filterCommonSearch();
        else
            this.ics.send001("FILTER");
    };
    FAQList.prototype.filterCommonSearch = function () {
        var l_DateRange;
        var l_SearchString = "";
        this._FilterDataset.filterList = [];
        this._FilterDataset.filterSource = 0;
        if (jQuery.trim(this._SearchString) != "") {
            l_SearchString = jQuery.trim(this._SearchString);
            this._FilterDataset.filterList =
                [
                    { "itemid": "1", "caption": "Search String", "fieldname": "SearchString", "datatype": "simplesearch", "condition": "c", "t1": l_SearchString, "t2": "" }
                ];
        }
        if (this._OperationMode != "prepareFilter") {
            if (this._FilterDataset.pageNo == 1)
                this.filterRecords();
            else
                this._FilterDataset.pageNo = 1;
        }
    };
    FAQList.prototype.filterRecords = function () {
        var _this = this;
        this._mflag = false;
        var l_Data = this._FilterDataset;
        var l_ServiceURL = this.ics.cmsurl + 'serviceCMS/getFAQList';
        // Show loading animation
        this.http.doPost(l_ServiceURL, l_Data).subscribe(function (data) {
            if (data != null) {
                _this._ListingDataset = data;
                _this._shownull = false;
                // Convert to array for single item
                if (_this._ListingDataset.faqData != undefined) {
                    _this._ListingDataset.faqData = _this.l_util.convertToArray(_this._ListingDataset.faqData);
                }
                else {
                    _this._shownull = true;
                }
                // Show / Hide Listing
                _this._showListing = (_this._ListingDataset.faqData != undefined);
                // Show / Hide Pagination
                _this._showPagination = (_this._showListing && (_this._ListingDataset.totalCount > 10));
                // Show / Hide Pagination
                _this.togglePagination(_this._showListing);
                // Hide loading animation
                _this._mflag = true;
            }
        }, function (error) {
            // Hide loading animation
            _this._mflag = true;
            // Show error message
            // this.ics.sendBean({ t1:"rp-error", t2: "Server connection error." });
        }, function () { });
    };
    FAQList.prototype.downloadFile = function (aFileName) {
        var l_Url = this.ics._apiurl + 'service001/downloadexcel?filename=' + aFileName;
        jQuery("#ExcelDownload").html("<iframe src=" + l_Url + "></iframe>");
    };
    FAQList.prototype.btnDashboard = function () {
        this._router.navigate(['/pocDashboard']);
    };
    FAQList.prototype.goClosePrint = function () {
        this._divexport = false;
    };
    FAQList.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    FAQList.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    FAQList.prototype.getFullTitleLabel = function (str) {
        if (str != undefined && str != null) {
            if (localStorage.getItem("systemfont") != "uni") {
                if (isMyanmar(str))
                    str = ZgtoUni(str);
            }
            return str;
        }
        else {
            return '';
        }
    };
    FAQList = __decorate([
        core_1.Component({
            selector: 'FAQList',
            template: "\n    \n    <div *ngIf=\"!_divexport\" class=\"container-fluid\">\n    <div class=\"row clearfix\">\n        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n            <form class=\"form-horizontal\">\n                <fieldset>\n                    <legend>\n                        <div style=\"float:left;display:inline;font-size:18px;margin-top:5px;margin-left:10px;\">\n                        FAQ List\n                        <button id=\"btnNew\" class=\"btn btn-sm btn-primary\" type=\"button\" style=\"cursor:pointer;height:34px;\" (click)=\"goNew()\" title=\"New\">\n                        <i id=\"lblNew\" class=\"glyphicon glyphicon-file\" style=\"padding-left:2px;\"></i> \n                        </button>\n                        </div>\n\n                        <div style=\"float:right;text-align:right;margin-top:0px;margin-bottom:5px;\">                                \n                            <div style=\"display:inline-block;padding-right:0px;width:280px;\">\n                                <div class=\"input-group\"> \n                                    <input class=\"form-control\" type=\"text\" id=\"isInputSearch\" placeholder=\"Search\" autocomplete=\"off\" spellcheck=\"false\" [(ngModel)]=\"_SearchString\" [ngModelOptions]=\"{standalone: true}\" (keyup.enter)=\"filterSearch()\">\n\n                                    <span id=\"btnSimpleSearch\" class=\"input-group-addon\">\n                                        <i class=\"glyphicon glyphicon-search\" style=\"cursor: pointer\" (click)=\"filterSearch()\"></i>\n                                    </span>\n                                </div>\n                            </div>\n\n                            <!--<button id=\"btnToggleSearch\" class=\"btn btn-sm btn-secondary\" type=\"button\" style=\"cursor:pointer;text-align:center;margin-top:-24px;height:34px;\" (click)=\"btnToggleSearch_onClick()\" title=\"Collapse\">\n                                <i id=\"lblToggleSearch\" class=\"glyphicon glyphicon-menu-down\"></i> \n                            </button>-->\n\n                            <button *ngIf=\"false\" id=\"btnTogglePagination\" type=\"button\" class=\"btn btn-sm btn-default\" style=\"cursor:pointer;text-align:center;margin-left:25px;margin-top:-24px;width:30px;height:34px;\" (click)=\"btnTogglePagination_onClick()\" title=\"Collapse\">\n                                <i id=\"lblTogglePagination\" class=\"glyphicon glyphicon-eject\" style=\"color:#2e8690;margin-left:-4px;transform: rotate(90deg)\"></i>\n                            </button>\n\n                            <div id=\"divPagination\" style=\"display:none;margin-left:13px;margin-top:-7px;margin-bottom:5px;\">\n                                <pager6 id=\"pgPagination\" rpPageSizeMax=\"100\" [(rpModel)]=\"_ListingDataset.totalCount\" (rpChanged)=\"changedPager($event)\" style=\"font-size:14px;text-align:right;margin-top:-7px;margin-right:10px;\"></pager6>\n                            </div>\n                                                 \n                        </div>\n                    </legend>\n\t\t\t\t\t<div class=\"cardview list-height\">\n\t\t\t\t\t\t<advanced-search id=\"asAdvancedSearch\" [FilterList]=\"_FilterList\" [TypeList]=\"_TypeList\" rpVisibleItems=5 (rpHidden)=\"renderAdvancedSearch($event)\" (rpChanged)=\"filterAdvancedSearch($event)\" style=\"display:none;\"></advanced-search>\n\n\t\t\t\t\t\t<div *ngIf=\"_shownull!=false\" style=\"color:#ccc;\">No result found!</div>\n\n\t\t\t\t\t\t<div *ngIf=\"_showListing\" class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-md-12\" style=\"overflow-x:auto;\">\n                            <table class=\"table table-striped table-condense table-hover tblborder\">\n                                <thead>\n                                    <tr>\n                                        <th class=\"right\" style=\"width:1%\" title=\"UserID\">No.</th>\n                                        <th class=\"left\" style=\"width:8%\" title=\"UserName\">Question by English</th>\n                                        <th class=\"left\" style=\"width:8%\" title=\"Fatehername\">Answer by English</th>\n                                        <th class=\"left\" style=\"width:8%\"title=\"NRC\">Question by Myanmar</th>\n                                        <th class=\"left\" style=\"width:10%\" title=\"Date of Birth\">Answer by Myanmar</th>\n                                    </tr>\n                                </thead>\n    \n                                <tbody>\n                                    <tr *ngFor=\"let obj of _ListingDataset.faqData\" >\n                                        <td class=\"right\"><a (click)=\"goto(obj.autokey)\">{{obj.autokey}}</a></td>\n                                        <td class=\"textwrap uni\" title=\"({{getFullTitleLabel(obj.questionEng)}})\">{{obj.questionEng}}</td>                                   \n                                        <td class=\"textwrap uni\" title=\"({{getFullTitleLabel(obj.answerEng)}})\">{{obj.answerEng}}</td>\n                                        <td class=\"textwrap uni\" title=\"({{getFullTitleLabel(obj.questionUni)}})\">{{obj.questionUni}}</td>\n                                        <td class=\"textwrap uni\" title=\"({{getFullTitleLabel(obj.answerUni)}})\">{{obj.answerUni}}</td>\n                                    </tr> \n                                </tbody>\n                            </table>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div *ngIf=\"_showListing\" style=\"text-align:center\">Total {{ _ListingDataset.totalCount }}</div>\n\t\t\t\t\t</div>\n                </fieldset>\n            </form>\n\n            <div id=\"ExcelDownload\" style=\"display: none;width: 0px;height: 0px;\"></div>\n\n        </div> \n    </div>\n</div>\n\n<div [hidden]=\"_mflag\">\n<div  id=\"loader\" class=\"modal\" ></div>\n</div>\n\n<div *ngIf='_divexport'>\n    <button type=\"button\" class=\"close\" (click)=\"goClosePrint()\" style=\"margin-top:-20px;\">&times;</button>\n    <iframe id=\"frame1\" [src]=\"_printUrl\" style=\"width: 100%;height: 95%;\"></iframe> \n</div>\n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_http_service_1.RpHttpService, rp_client_util_1.ClientUtil, rp_references_1.RpReferences])
    ], FAQList);
    return FAQList;
}());
exports.FAQList = FAQList;
//# sourceMappingURL=faq-list.component.js.map