"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
// RP Framework
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_client_util_1 = require('../util/rp-client.util');
core_2.enableProdMode();
var MenuSetupListComponent = (function () {
    function MenuSetupListComponent(ics, _router, http) {
        this.ics = ics;
        this._router = _router;
        this.http = http;
        // Application Specific
        this._searchVal = ""; // simple search
        this._flagas = true; // flag advance search
        this._col_list = [{ "col_no": "1", "flag": false }, { "col_no": "2", "flag": false }];
        this._sorting = { _sort_type: "asc", _sort_col: "1" };
        this._mflag = true;
        this._showListing = false;
        this._util = new rp_client_util_1.ClientUtil();
        this.sessionAlertMsg = "";
        this._userobj = { "data": [{ "syskey": 0, "t1": "", "t2": "", "t5": "", "t6": "", "parentMenu": "" }], "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0, "msgCode": "", "msgDesc": "" };
        this._pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 0 };
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = false;
            this.search();
        }
    }
    // list sorting part
    MenuSetupListComponent.prototype.changeDefault = function () {
        for (var i = 0; i < this._col_list.length; i++) {
            this._col_list[i].flag = false;
        }
    };
    MenuSetupListComponent.prototype.addSort = function (e) {
        for (var i = 0; i < this._col_list.length; i++) {
            if (this._col_list[i].col_no == e) {
                var _tmp_flag = this._col_list[i].flag;
                this.changeDefault();
                this._col_list[i].flag = !_tmp_flag;
                this._sorting._sort_type = !_tmp_flag ? "desc" : "asc";
                this._sorting._sort_col = e;
                break;
            }
        }
    };
    MenuSetupListComponent.prototype.changedPager = function (event) {
        if (this._userobj.totalCount != 0) {
            this._pgobj = event;
            var current = this._userobj.currentPage;
            var size = this._userobj.pageSize;
            this._userobj.currentPage = this._pgobj.current;
            this._userobj.pageSize = this._pgobj.size;
            if (this._pgobj.current != current || this._pgobj.size != size) {
                this.search();
            }
        }
    };
    MenuSetupListComponent.prototype.search = function () {
        var _this = this;
        try {
            this._mflag = false;
            this.http.doGet(this.ics._apiurl + 'service001/getmenulist?searchVal=' + encodeURIComponent(this._userobj.searchText) + '&pagesize=' + this._userobj.pageSize + '&currentpage=' + this._userobj.currentPage + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMessage(data.msgDesc, false);
                }
                else {
                    if (data.totalCount == 1) {
                        var m = [];
                        m[0] = data.data;
                        _this._userobj.data = m[0];
                        _this._userobj.data.splice(1, 1);
                        _this._userobj.totalCount = data.totalCount;
                    }
                    else if (data.totalCount == 0) {
                        _this._userobj = data;
                        _this.showMessage("Data not found!", false);
                    }
                    else {
                        _this._userobj = data;
                    }
                    _this._showListing = (_this._userobj.data != undefined);
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    MenuSetupListComponent.prototype.searchKeyup = function (e) {
        if (e.which == 13) {
            this._userobj.currentPage = 1;
            this._msghide = true;
            this._recordhide = false;
            this.search();
        }
    };
    MenuSetupListComponent.prototype.Searching = function () {
        this._userobj.currentPage = 1;
        this._msghide = true;
        this._recordhide = false;
        this.search();
    };
    MenuSetupListComponent.prototype.goNew = function () {
        this._router.navigate(['/Menu', 'new']);
    };
    MenuSetupListComponent.prototype.goto = function (p) {
        this._router.navigate(['/Menu', 'read', p]);
    };
    //showMessage() {
    //    jQuery("#sessionalert").modal();
    //    Observable.timer(3000).subscribe(x => {
    //        this.goLogOut();
    //    });
    //}
    MenuSetupListComponent.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    MenuSetupListComponent.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "primary", "t3": msg });
        }
    };
    MenuSetupListComponent = __decorate([
        core_1.Component({
            selector: 'menusetup-list',
            template: " \n    <div class=\"container-fluid\">\n\t<form class=\"form-horizontal\">\n\t\t<legend>Menu List</legend>\n\t\t<div class=\"cardview list-height\">\t\t\n\t\t\t<div class=\"row col-md-12\">\n\t\t\t\t<div class=\"row col-md-3\">\n\t\t\t\t\t<div class=\"input-group\">\n\t\t\t\t\t\t<span class=\"input-group-btn\">\n\t\t\t\t\t\t\t<button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goNew();\">New</button>\n\t\t\t\t\t\t</span>\n\t\t\t\t\t\t<input id=\"textinput\" name=\"textinput\" type=\"text\" placeholder=\"Search\" [(ngModel)]=\"_userobj.searchText\" maxlength=\"50\" class=\"form-control input-sm\" (keyup)=\"searchKeyup($event)\" [ngModelOptions]=\"{standalone: true}\">\n\t\t\t\t\t\t<span class=\"input-group-btn\">\n\t\t\t\t\t\t\t<button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"Searching()\" >\n\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-search\"></span>Search\n\t\t\t\t\t\t</button>\n\t\t\t\t\t\t</span> \n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"pagerright\">\t\t\t\t\n\t\t\t\t\t<adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"_userobj.totalCount\" (rpChanged)=\"changedPager($event)\" ></adminpager> \n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div *ngIf=\"_showListing\"> \n\t\t\t\t<table class=\"table table-striped table-condensed table-hover tblborder\">\n\t\t\t\t<thead>\n\t\t\t\t <tr>\n\t\t\t\t\t<th class=\"left\">Code</th>\n\t\t\t\t\t<th class=\"left\">Description</th>\n\t\t\t\t\t<th class=\"left\">Parent Menu </th>      \n\t\t\t\t</tr>\n\t\t\t\t</thead>\n\t\t\t\t<tbody>\n\t\t\t\t<tr *ngFor=\"let obj of _userobj.data\">\n\t\t\t\t\t<td *ngIf=\"obj.syskey != 0\"><a (click)=\"goto(obj.syskey)\">{{obj.syskey}}</a></td>\n\t\t\t\t\t<td>{{obj.t2}}</td>\n\t\t\t\t\t<td>{{obj.parentMenu}}</td>  \n\t\t\t\t</tr>  \n\t\t\t\t</tbody>\n\t\t\t\t</table>\n\t\t\t</div>\n\t\t\t<div align=\"center\" *ngIf=\"_showListing\" >\n\t\t\t\t   Total {{_userobj.totalCount}}\n\t\t\t </div>\n\t\t</div>\n\t</form>\n</div> \n    <div [hidden] = \"_mflag\">\n        <div  id=\"loader\" class=\"modal\" ></div>\n    </div>\n   "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_http_service_1.RpHttpService])
    ], MenuSetupListComponent);
    return MenuSetupListComponent;
}());
exports.MenuSetupListComponent = MenuSetupListComponent;
//# sourceMappingURL=menusetup-list.component.js.map