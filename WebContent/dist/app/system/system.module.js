"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var common_1 = require('@angular/common');
var rp_input_module_1 = require('../framework/rp-input.module');
var mydatepicker_1 = require('mydatepicker');
var rp_http_service_1 = require('../framework/rp-http.service');
var adminpager_module_1 = require('../util/adminpager.module');
var pager_module_1 = require('../util/pager.module');
var system_routing_1 = require('./system.routing');
var usersetup_component_1 = require('./usersetup.component');
var user_list_component_1 = require('./user-list.component');
var passwordpolicysetup_component_1 = require('./passwordpolicysetup.component');
var forcesignout_component_1 = require('./forcesignout.component');
var menusetup_component_1 = require('./menusetup.component');
var menusetup_list_component_1 = require('./menusetup-list.component');
var rolesetup_component_1 = require('./rolesetup.component');
var rolesetup_list_component_1 = require('./rolesetup-list.component');
var versionhistory_component_1 = require('./versionhistory.component');
var versionhistory_list_component_1 = require('./versionhistory-list.component');
var lovsetup_component_1 = require('./lovsetup.component');
var lovsetup_list_component_1 = require('./lovsetup-list.component');
var faq_menu_component_1 = require('./faq-menu.component');
var faq_list_component_1 = require('./faq-list.component');
var locatorsetup_component_1 = require('./locatorsetup.component');
var locatorsetup_list_component_1 = require('./locatorsetup-list.component');
var SystemModule = (function () {
    function SystemModule() {
    }
    SystemModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                forms_1.FormsModule,
                rp_input_module_1.RpInputModule,
                adminpager_module_1.AdminPagerModule,
                pager_module_1.PagerModule,
                mydatepicker_1.MyDatePickerModule,
                system_routing_1.SystemRouting,
            ],
            exports: [],
            declarations: [
                usersetup_component_1.UserSetupComponent,
                user_list_component_1.UserListComponent,
                passwordpolicysetup_component_1.PasswordPolicyComponent,
                forcesignout_component_1.ForceSignOutComponent,
                menusetup_component_1.MenuSetupComponent,
                menusetup_list_component_1.MenuSetupListComponent,
                rolesetup_component_1.RoleSetupComponent,
                rolesetup_list_component_1.RoleSetupListComponent,
                versionhistory_component_1.VersionHistoryComponent,
                versionhistory_list_component_1.VersionHistoryListComponent,
                lovsetup_component_1.LovSetupComponent,
                lovsetup_list_component_1.LOVSetupListComponent,
                faq_menu_component_1.FAQMenu,
                faq_list_component_1.FAQList,
                locatorsetup_component_1.LocatorSetupComponent,
                locatorsetup_list_component_1.LocatorSetupListComponent
            ],
            providers: [
                rp_http_service_1.RpHttpService
            ],
            schemas: [core_1.CUSTOM_ELEMENTS_SCHEMA]
        }), 
        __metadata('design:paramtypes', [])
    ], SystemModule);
    return SystemModule;
}());
exports.SystemModule = SystemModule;
//# sourceMappingURL=system.module.js.map