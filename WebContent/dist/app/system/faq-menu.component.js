"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
require('tinymce/tinymce.min');
require('tinymce/themes/modern/theme');
require('tinymce/plugins/link/plugin.js');
require('tinymce/plugins/paste/plugin.js');
require('tinymce/plugins/table/plugin.js');
require('tinymce/plugins/advlist/plugin.js');
require('tinymce/plugins/autoresize/plugin.js');
require('tinymce/plugins/lists/plugin.js');
require('tinymce/plugins/code/plugin.js');
require('tinymce/plugins/image/plugin.js');
require('tinymce/plugins/imagetools/plugin.js');
var FAQMenu = (function () {
    function FAQMenu(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this.catch = "";
        this.message = "";
        this._font = "";
        this._hide = true;
        this._nrcupdatelist = true;
        this._mflag = true;
        this._faqObj = this.getFAQObj();
        this.flagnew = true;
        this.flagsave = true;
        this.flagdelete = true;
        this._key = "";
        this._result = { "longResult": "", "msgDesc": "", "key": [], "keyResult": 0, "state": "", "stringResult": "" };
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this.flagnew = false;
            this.flagsave = false;
            this.flagdelete = true;
        }
    }
    FAQMenu.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this.flagdelete = false;
                _this._key = id;
                _this.goReadBySyskey(id);
            }
        });
    };
    FAQMenu.prototype.goReadBySyskey = function (p) {
        var _this = this;
        try {
            var url = this.ics.cmsurl + 'serviceCMS/getFAQbysyskey?id=' + this._key + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
            this.http.doGet(url).subscribe(function (data) {
                _this._faqObj = data;
                if (_this._faqObj.msgCode == '0016') {
                    _this.showMessage(_this._faqObj.msgDesc, false);
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Error!");
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    /* ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
          let cmd = params['cmd'];
          if (cmd != null && cmd != "" && cmd == "new") {
            this.goNew();
          } else if (cmd != null && cmd != "" && cmd == "read") {
            var _List = this.ics.getBean().faqData;
            this._mflag=false;
            this.flagdelete = false;
            this._faqObj.syskey=_List.syskey;
            this._faqObj.questionEng=_List.questionEng;
            this._faqObj.answerEng=_List.answerEng;
            this._faqObj.questionUni=_List.questionUni;
            this._faqObj.answerUni=_List.answerUni;
          }
        });
      } */
    FAQMenu.prototype.getFAQObj = function () {
        return {
            "srno": 0, "syskey": 0, "autokey": 0, "createddate": "", "modifiedDate": "", "status": 0, "userID": "", "sessionID": "", "questionEng": "", "answerEng": "", "questionUni": "",
            "answerUni": "", "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "msgDesc": "", "msgCode": "",
        };
    };
    FAQMenu.prototype.goSave = function () {
        var _this = this;
        if (this.isValidate()) {
            this._mflag = false;
            this._faqObj.userID = this.ics._profile.userID;
            this._faqObj.sessionID = this.ics._profile.sessionID;
            var url = (this.ics.cmsurl + 'serviceCMS/saveFAQMenu?userid');
            var json = this._faqObj;
            this.http.doPost(url, json).subscribe(function (data) {
                _this._mflag = true;
                _this._result = data;
                _this.showMessage(data.msgDesc, data.state);
                _this._faqObj.syskey = data.longResult[0];
            }, function (error) {
                _this.showMessage("Can't Saved This Record!", undefined);
            }, function () { });
        }
    };
    FAQMenu.prototype.goDelete = function () {
        var _this = this;
        if (this._faqObj.questionEng != "") {
            this._mflag = false;
            this._faqObj.userID = this.ics._profile.userID;
            this._faqObj.sessionID = this.ics._profile.sessionID;
            var url = this.ics.cmsurl + 'serviceCMS/deleteFAQ';
            var json = this._faqObj;
            this.http.doPost(url, json).subscribe(function (data) {
                _this._mflag = true;
                _this.showMessage(data.msgDesc, data.state);
                if (data.state) {
                    _this.goNew();
                }
            }, function (error) { }, function () { });
        }
        else {
            this.showMessage("No Article to Delete!", undefined);
        }
    };
    FAQMenu.prototype.isValidate = function () {
        if (this._faqObj.questionEng == "") {
            this.showMessage("Please fill QuestionEng!", false);
            return false;
        }
        if (this._faqObj.answerEng == "") {
            this.showMessage("Please fill AnswerEng!", false);
            return false;
        }
        if (this._faqObj.questionUni == "") {
            this.showMessage("Please fill QuestionUni!", false);
            return false;
        }
        if (this._faqObj.answerUni == "") {
            this.showMessage("Please fill AnswerUni!", false);
            return false;
        }
        return true;
    };
    FAQMenu.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "primary", "t3": msg });
        }
    };
    FAQMenu.prototype.goList = function () {
        this._router.navigate(['/faq-list']);
    };
    FAQMenu.prototype.goNew = function () {
        this.flagdelete = true;
        this.flagnew = false;
        this.flagsave = false;
        this._router.navigate(['/faq-menu']);
    };
    FAQMenu = __decorate([
        core_1.Component({
            selector: 'faqMenu',
            template: "\n    <div class=\"container-fluid\">\n    <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n\t<form class=\"form-horizontal\" ngNoForm>\n\t\t<legend>FAQ</legend>\n\t\t<div class=\"cardview list-height\">    \n            <div class=\"row col-md-12\">\n                <button class=\"btn btn-sm btn-primary\"  type=\"button\"   (click)=\"goList()\">List</button> \n                <button class=\"btn btn-sm btn-primary\" [disabled]=\"flagnew\" id=\"new\" type=\"button\" (click)=\"goNew()\">New</button>      \n                <button class=\"btn btn-sm btn-primary\" [disabled]=\"flagsave\" id=\"save\" type=\"button\" (click)=\"goSave()\">Save</button> \n                <button class=\"btn btn-sm btn-primary\" [disabled]=\"flagdelete\" id=\"delete\" type=\"button\" (click)=\"goDelete();\" >Delete</button>  \n            </div>\n\n            <div class=\"row col-md-12\">&nbsp;</div>\n              <div class=\"form-group\">   \n        \n            <div class=\"col-md-12\">\n                <div class=\"form-group\">\n                    <label class=\"col-md-2\">Question by English</label>\n                    <div class=\"col-md-4\">\n                        <textarea  type=\"text\" [(ngModel)]=\"_faqObj.questionEng\" class=\"form-control input-sm uni\" style=\"height:100px;\"></textarea>                       \n                    </div>\n                        <label class=\"col-md-2\" align=\"left\">Answer by English&nbsp;</label>\n                        <div class=\"col-md-4\" > \n                             <textarea  type=\"text\" [(ngModel)]=\"_faqObj.answerEng\" class=\"form-control input-sm uni\" style=\"height:100px;\"> </textarea>\n                        </div> \n                   \n                </div>\n    \n                <div class=\"form-group\">\n                    <label class=\"col-md-2\">Question by Myanmar</label>\n                    <div class=\"col-md-4\">\n                        <textarea type=\"text\" [(ngModel)]=\"_faqObj.questionUni\" class=\"form-control input-sm uni\" style=\"height:100px;\"> </textarea>                     \n                    </div>                    \n                   \n                    <label class=\"col-md-2\" align=\"left\">Answer by Myanmar&nbsp;</label>\n                        <div class=\"col-md-4\" > \n                              <textarea  type=\"text\" [(ngModel)]=\"_faqObj.answerUni\" class=\"form-control input-sm uni\" style=\"height:100px;\"> </textarea>\n                        </div>  \n                </div>           \n    \n                <br>\n                <br>\n            </div>\n\t\t\t</div>\n\t\t</div>\n    </form>\n</div>\n</div>\n</div>\n    <div [hidden]=\"_mflag\">\n    <div  id=\"loader\" class=\"modal\" ></div></div>\n"
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FAQMenu);
    return FAQMenu;
}());
exports.FAQMenu = FAQMenu;
//# sourceMappingURL=faq-menu.component.js.map