"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
core_2.enableProdMode();
var VersionHistoryListComponent = (function () {
    function VersionHistoryListComponent(ics, l_util, _router, http, ref) {
        this.ics = ics;
        this.l_util = l_util;
        this._router = _router;
        this.http = http;
        this.ref = ref;
        this._util = new rp_client_util_1.ClientUtil();
        this._showListing = false;
        //about default obj
        this.vHobj = this.getDefaultObj();
        this._pgobj = {
            "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1,
            "end": 10, "size": 10, "totalcount": 0
        };
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this.checkSession();
            this.getAppCodes();
            this.getStatusCodes();
            this.search();
        }
    }
    VersionHistoryListComponent.prototype.getDefaultObj = function () {
        return {
            "data": [{
                    "autokey": "", "appcode": "", "versionkey": "", "version": "", "versiontitle": "",
                    "description": "", "startdate": "", "duedate": "", "remark": "", "status": ""
                }], "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0,
            "sessionID": "", "userID": "", "msgCode": "", "msgDesc": ""
        };
    };
    VersionHistoryListComponent.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    VersionHistoryListComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsgAlert(data.desc);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsgAlert(data.desc);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    VersionHistoryListComponent.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    VersionHistoryListComponent.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    VersionHistoryListComponent.prototype.getAppCodes = function () {
        var _this = this;
        try {
            var url = this.ics.cmsurl + 'serviceAdmLOV/getAppCodesForVersionHistory';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.msgCode == "0016") {
                        _this.showMessage(data.msgDesc, false);
                        _this.logout();
                    }
                    if (data.msgCode != "0000") {
                        _this.showMessage(data.msgDesc, false);
                    }
                    if (data.msgCode == "0000") {
                        if (!(data.refAppCode instanceof Array)) {
                            var m = [];
                            m[0] = data.refAppCode;
                            _this.ref._lov3.refAppCode = m;
                        }
                        else {
                            _this.ref._lov3.refAppCode = data.refAppCode;
                        }
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    VersionHistoryListComponent.prototype.getStatusCodes = function () {
        var _this = this;
        try {
            var url = this.ics.cmsurl + 'serviceAdmLOV/getStatusCodesForVersionHistory';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.msgCode == "0016") {
                        _this.showMessage(data.msgDesc, false);
                        _this.logout();
                    }
                    if (data.msgCode != "0000") {
                        _this.showMessage(data.msgDesc, false);
                    }
                    if (data.msgCode == "0000") {
                        if (!(data.refVersionStatusCode instanceof Array)) {
                            var m = [];
                            m[0] = data.refVersionStatusCode;
                            _this.ref._lov3.refVersionStatusCode = m;
                        }
                        else {
                            _this.ref._lov3.refVersionStatusCode = data.refVersionStatusCode;
                        }
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    VersionHistoryListComponent.prototype.search = function () {
        var _this = this;
        try {
            var url = this.ics.cmsurl + 'service001/getAllVersionHistory';
            this.vHobj.sessionID = this.ics._profile.sessionID;
            this.vHobj.userID = this.ics._profile.userID;
            var json = this.vHobj;
            this.http.doPost(url, json).subscribe(function (data) {
                _this._mflag = true;
                var temp = _this.vHobj;
                if (data != null) {
                    _this.vHobj = data;
                    if (_this.vHobj.msgCode == "0016") {
                        _this.showMessage(_this.vHobj.msgDesc, false);
                        _this.logout();
                    }
                    if (_this.vHobj.msgCode != "0000") {
                        _this._showListing = true;
                        _this.showMessage(_this.vHobj.msgDesc, false);
                        _this.vHobj = temp;
                    }
                    if (_this.vHobj.msgCode == "0000") {
                        _this._showListing = true;
                        if (_this.vHobj.totalCount == 0) {
                            _this.showMessage(_this.vHobj.msgDesc, true);
                        }
                        else {
                            if (!(data.data instanceof Array)) {
                                var m = [];
                                m[0] = data.data;
                                _this.vHobj.data = m;
                            }
                        }
                        for (var iIndex = 0; iIndex < _this.vHobj.data.length; iIndex++) {
                            _this.vHobj.data[iIndex].startdate = _this._util.changeStringtoDateDDMMYYYY(_this.vHobj.data[iIndex].startdate);
                            _this.vHobj.data[iIndex].duedate = _this._util.changeStringtoDateDDMMYYYY(_this.vHobj.data[iIndex].duedate);
                        }
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    VersionHistoryListComponent.prototype.goNew = function () {
        this._router.navigate(['/version-history', , { cmd: "NEW" }]);
    };
    VersionHistoryListComponent.prototype.searchKeyup = function (e) {
        if (e.which == 13) {
            this.vHobj.currentPage = 1;
            this.search();
        }
    };
    VersionHistoryListComponent.prototype.searching = function () {
        this.vHobj.currentPage = 1;
        this.search();
    };
    VersionHistoryListComponent.prototype.changedPager = function (event) {
        if (this.vHobj.totalCount != 0) {
            this._pgobj = event;
            var current = this.vHobj.currentPage;
            var size = this.vHobj.pageSize;
            this.vHobj.currentPage = this._pgobj.current;
            this.vHobj.pageSize = this._pgobj.size;
            if (this._pgobj.current != current || this._pgobj.size != size) {
                this.search();
            }
        }
    };
    VersionHistoryListComponent.prototype.goto = function (p) {
        this._router.navigate(['/version-history', 'read', p]);
    };
    VersionHistoryListComponent.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "primary", "t3": msg });
        }
    };
    VersionHistoryListComponent = __decorate([
        core_1.Component({
            selector: 'version-history-list',
            template: " \n    <div class=\"container col-md-12 col-sm-12 col-xs-12\">\n    <form class=\"form-inline\">\n        <legend>Version History List</legend>\n\t\t<div class=\"cardview list-height\">\n\t\t\t\n\t\t\t<div class=\"row col-md-12\">\n\t\t\t\t<div class=\"row col-md-4\">\n\t\t\t\t\t<div  class=\"input-group\">\n\t\t\t\t\t\t<span class=\"input-group-btn\">\n\t\t\t\t\t\t\t<button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goNew();\">New</button>\n\t\t\t\t\t\t</span> \n\t\t\t\t\t\t<input id=\"textinput\" name=\"textinput\" type=\"text\"  placeholder=\"Search\" [(ngModel)]=\"vHobj.searchText\" (keyup)=\"searchKeyup($event)\" maxlength=\"30\" class=\"form-control input-sm\">\n\t\t\t\t\t\t<span class=\"input-group-btn\">\n\t\t\t\t\t\t\t<button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"Searching()\" >\n\t\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-search\"></span>Search\n\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t</span>        \n\t\t\t\t\t</div> \n\t\t\t\t</div>\t\t\n                    <div class=\"pagerright\">\n\t\t\t\t\t    <adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"vHobj.totalCount\" (rpChanged)=\"changedPager($event)\" ></adminpager> \n\t\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"row col-md-12\" style=\"overflow-x:auto\">\n\t\t\t<table class=\"table table-striped table-condensed table-hover tblborder\">\n\t\t\t\t<thead>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<th>Version Key</th>\n\t\t\t\t\t\t<th>App Code</th>\n\t\t\t\t\t\t<th>Version</th>\n\t\t\t\t\t\t<th>Version Title</th>\n\t\t\t\t\t\t<th>Description</th>    \n\t\t\t\t\t\t<th>Start Date</th>\n\t\t\t\t\t\t<th>Due Date</th>\n\t\t\t\t\t\t<th>Remark</th>\n\t\t\t\t\t\t<th>Status</th>\n\t\t\t\t\t</tr>\n\t\t\t\t</thead>\n\t\t\t\t<tbody>\n\t\t\t\t\t<tr *ngFor=\"let obj of vHobj.data;let i=index\">\n\t\t\t\t\t\t<td><a (click)=\"goto(obj.autokey)\">{{obj.versionkey}}</a></td>\n\t\t\t\t\t\t<ng-container *ngFor=\"let item of ref._lov3.refAppCode\">\n\t\t\t\t\t\t\t<td *ngIf=\"item.value==obj.appcode\">{{item.caption}}</td>\n\t\t\t\t\t\t</ng-container>\n\t\t\t\t\t\t<td>{{obj.version}}</td>\n\t\t\t\t\t\t<td>{{obj.versiontitle}}</td>\n\t\t\t\t\t\t<td>{{obj.description}}</td>\n\t\t\t\t\t\t<td>{{obj.startdate}}</td>\n\t\t\t\t\t\t<td>{{obj.duedate}}</td>\n\t\t\t\t\t\t<td>{{obj.remark}}</td>\n\t\t\t\t\t\t<ng-container *ngFor=\"let item of ref._lov3.refVersionStatusCode\">\n\t\t\t\t\t\t\t<td *ngIf=\"item.value==obj.status\">{{item.caption}}</td>\n\t\t\t\t\t\t</ng-container>\n\t\t\t\t\t</tr>  \n\t\t\t\t</tbody>\n            </table>\n            </div>\n            <!--<div align=\"center\" *ngIf=\"_showListing\" >\n                Total {{vHobj.totalCount}}\n            </div>-->\n            \n\t\t</div>\n    </form>\n</div>\n    <div [hidden]=\"_mflag\">\n      <div  id=\"loader\" class=\"modal\" ></div>\n    </div>\n    "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, rp_client_util_1.ClientUtil, router_1.Router, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], VersionHistoryListComponent);
    return VersionHistoryListComponent;
}());
exports.VersionHistoryListComponent = VersionHistoryListComponent;
//# sourceMappingURL=versionhistory-list.component.js.map