"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var Rx_1 = require('rxjs/Rx');
core_2.enableProdMode();
var ResetPasswordComponent = (function () {
    function ResetPasswordComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
        this._obj = this.getDefaultObj();
        this.sessionAlertMsg = "";
        this.isvalidate = "";
        this._mflag = false;
        this._util = new rp_client_util_1.ClientUtil();
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = true;
            this.checkSession();
            jQuery("#myreset").prop("disabled", true);
            this._obj = this.getDefaultObj();
        }
    }
    ResetPasswordComponent.prototype.getDefaultObj = function () {
        return { "createdDate": "", "t1": "", "t3": "", "t21": "", "t41": "", "sessionID": "", "userID": "" };
    };
    ResetPasswordComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this.goLogOut();
        });
    };
    ResetPasswordComponent.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    ResetPasswordComponent.prototype.goCheck = function () {
        var _this = this;
        try {
            this._mflag = false;
            var url = this.ics.cmsurl + 'service001/getUserNameAndNrc';
            var json = this._sessionObj;
            //this.http.doGet(this.ics.cmsurl + 'service001/getUserNameAndNrc?id=' + this._obj.t1 + '&sessionID=' + this.ics._profile.sessionID + '&userIDForSession=' + this.ics._profile.userID).subscribe(
            this.http.doPost(url, json).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMsg(data.msgDesc, false);
                }
                else {
                    if (data.t1 == '') {
                        _this.isvalidate = "";
                        jQuery("#myreset").prop("disabled", true);
                        _this.showMsg("Invalid User ID", false);
                    }
                    else {
                        _this._obj = data;
                        _this.isvalidate = "Validate";
                        jQuery("#myreset").prop("disabled", false);
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    ResetPasswordComponent.prototype.goClear = function () {
        this.isvalidate = "";
        this._obj = this.getDefaultObj();
    };
    ResetPasswordComponent.prototype.goReset = function () {
        var _this = this;
        try {
            this._mflag = false;
            //this.http.doGet(this.ics.cmsurl + 'service001/resetPasswordbyId?userId=' + this._obj.t1 + '&adminId=' + this.ics._profile.userID + '&sessionID=' + this.ics._profile.sessionID).subscribe(
            var url = this.ics.cmsurl + 'service001/resetPasswordbyId';
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMsg(data.msgDesc, false);
                }
                else {
                    _this._returnResult.msgDesc = data.msgDesc;
                    _this._returnResult.state = data.state;
                    jQuery("#myreset").prop("disabled", true);
                    _this.showMsg(data.msgDesc, true);
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    ResetPasswordComponent.prototype.showMessageAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    ResetPasswordComponent.prototype.getSessionObj = function () {
        return { "sessionID": this.ics._profile.sessionID, "userID": this.ics._profile.userID, "loginID": "" };
    };
    ResetPasswordComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            //this._sessionObj.sessionID = this.ics._profile.sessionID;
            //this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsgAlert(data.desc);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsgAlert(data.desc);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    ResetPasswordComponent.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    ResetPasswordComponent.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    ResetPasswordComponent.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    ResetPasswordComponent = __decorate([
        core_1.Component({
            selector: 'resetpwd',
            template: "\n  <div class=\"container-fluid\">\n    <div class=\"row clearfix\">\n      <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n        <form class= \"form-horizontal\"> \n          <!-- Form Name -->\n          <legend>Reset Password</legend>\n          <div class=\"cardview list-height\">\n            <div class=\"row col-md-12\">   \n              <button class=\"btn btn-sm btn-primary\" type=\"button\" disabled id=\"myreset\" (click)=\"goReset()\" >Reset</button>\n              <button class=\"btn btn-sm btn-primary\" type=\"button\" id=\"myclear\" (click)=\"goClear()\" >Clear</button>  \n            </div>\n            <div class=\"row col-md-12\">&nbsp;</div>\n              <div class=\"form-group\">\n              <div class=\"col-md-8\">\n              <div class=\"form-group\">\n                <label class=\"col-md-2\"> Login ID <font class=\"mandatoryfont\" >*</font> </label>\n                <div class=\"col-md-4\">\n                      <div class=\"input-group\">\n                        <input type=\"text\" [(ngModel)]=_sessionObj.loginID [ngModelOptions]=\"{standalone: true}\" required=\"true\"  class=\"form-control input-sm\">\n                        <span class=\"input-group-btn\">\n                          <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goCheck()\"><i class=\"glyphicon glyphicon-ok-sign\"></i> Check </button>\n                        </span>\n                      </div>\n                </div>\n                <div class=\"col-md-2\">\n                  <label style=\"color:green;font-size:20px\">&nbsp;{{isvalidate}}</label>   \n                </div>\n              </div>  \n              <div class=\"form-group\">\n                <rp-input [(rpModel)]=\"_obj.t3\" rpRequired =\"true\" rpType=\"text\" rpClass=\"col-md-4\" rpLabelClass =\"col-md-2 control-label\" rpLabel=\"Name\" rpReadonly=\"true\"></rp-input>\n              </div>\n              <div class=\"form-group\">\n                <rp-input  rpType=\"text\" rpClass=\"col-md-4\" rpLabelClass =\"col-md-2 control-label\" rpLabel=\"NRC\" [(rpModel)]=\"_obj.t21\"  rpReadonly=\"true\"></rp-input>\n              </div>\n            </div>\n          </div>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n\n  <div id=\"sessionalert\" class=\"modal fade\">\n    <div class=\"modal-dialog\">\n      <div class=\"modal-content\">\n        <div class=\"modal-body\">\n          <p>{{sessionAlertMsg}}</p>\n        </div>\n        <div class=\"modal-footer\">\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div [hidden] = \"_mflag\">\n    <div class=\"modal\" id=\"loader\"></div>\n  </div>\n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], ResetPasswordComponent);
    return ResetPasswordComponent;
}());
exports.ResetPasswordComponent = ResetPasswordComponent;
//# sourceMappingURL=resetpassword.component.js.map