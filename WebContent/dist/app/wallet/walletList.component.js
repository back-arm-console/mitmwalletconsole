"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
core_1.enableProdMode();
var walletList = (function () {
    function walletList(ics, _router, 
        //private sanitizer: DomSanitizer, 
        http, l_util, ref) {
        this.ics = ics;
        this._router = _router;
        this.http = http;
        this.l_util = l_util;
        this.ref = ref;
        this._shownull = false;
        this._OperationMode = "";
        this._result = { "longResult": "", "msgDesc": "", "key": [], "keyResult": 0, "state": "", "stringResult": "" };
        this.userstatus = "";
        this._mflag = false;
        this._divexport = false;
        //_printUrl: SafeResourceUrl;
        // this.imageLink = this.ics._imglink + "/upload/image/userProfile/";
        this._isLoading = true;
        this._SearchString = "";
        this._showListing = false;
        this._showPagination = false;
        this._toggleSearch = true; // true - Simple Search, false - Advanced Search
        this._togglePagination = true;
        this.mstatus = 0;
        this._ButtonInfo = { "role": "", "formname": "", "button": "" };
        this._FilterDataset = {
            filterList: [
                { "itemid": "", "caption": "", "fieldname": "", "datatype": "", "condition": "", "t1": "", "t2": "" }
            ],
            "pageNo": 1, "pageSize": 10, "filterSource": 0, "sessionID": this.ics._profile.sessionID, "userID": this.ics._profile.userID, "userName": this.ics._profile.userName
        };
        this._ListingDataset = {
            mobileuserData: [
                {
                    "syskey": "", "autokey": "", "t1": "", "t3": "", "t20": "", "t21": "", "t24": "", "t25": "", "createdDate": "", "t16": "", "accNumber": "", "state": false, "t38": "",
                }
            ],
            "pageNo": 0, "pageSize": 0, "totalCount": 1, "userID": "", "userName": ""
        };
        this.muserobj = this.getmuserobj();
        this.mstatus = ics._profile.loginStatus;
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
    }
    walletList.prototype.getmuserobj = function () {
        return { "sessionID": this.ics._profile.sessionID, "userID": this.ics._profile.userID, "syskey": "", "userstatus": "" };
    };
    walletList.prototype.ngOnInit = function () {
        this.filterSearch();
        this.loadAdvancedSearchData();
    };
    walletList.prototype.goto = function (p) {
        console.log("p=" + JSON.stringify(p));
        this.ics.sendBean({ "walletdata": p });
        this._router.navigate(['/walletData', 'read', p]);
    };
    walletList.prototype.goNew = function () {
        this._router.navigate(['/walletData', 'new']);
    };
    walletList.prototype.ngAfterViewChecked = function () {
        this._isLoading = false;
    };
    walletList.prototype.loadAdvancedSearchData = function () {
        // this._TypeList =
        //     {
        //         "lovState": []
        //     };
        //this.loadState(); 
        this._FilterList =
            [
                { "itemid": "1", "caption": "Search String", "fieldname": "SearchString", "datatype": "simplesearch", "condition": "c", "t1": "", "t2": "", "t3": "" },
            ];
    };
    walletList.prototype.btnToggleSearch_onClick = function () {
        this.toggleSearch(!this._toggleSearch);
    };
    walletList.prototype.btnTogglePagination_onClick = function () {
        this.togglePagination(!this._togglePagination);
    };
    walletList.prototype.toggleSearch = function (aToggleSearch) {
        // Set Flag
        this._toggleSearch = aToggleSearch;
        // Clear Simple Search
        if (!this._toggleSearch)
            jQuery("#isInputSearch").val("");
        // Enable/Disabled Simple Search Textbox
        jQuery('#isInputSearch').prop('disabled', !this._toggleSearch);
        // Hide Advanced Search
        if (this._toggleSearch)
            this.ics.send001("CLEAR");
        // Show/Hide Advanced Search    
        //  
        if (this._toggleSearch)
            jQuery("#asAdvancedSearch").css("display", "none"); // true     - Simple Search
        else
            jQuery("#asAdvancedSearch").css("display", "inline-block"); // false    - Advanced Search
        // Set Icon 
        //  
        if (this._toggleSearch)
            jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-up").addClass("glyphicon glyphicon-menu-down"); // true     - Simple Search
        else
            jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-down").addClass("glyphicon glyphicon-menu-up"); // false    - Advanced Search
        // Set Tooltip
        //
        var l_Tooltip = (this._toggleSearch) ? "Expand" : "Collapse";
        jQuery('#btnToggleSearch').attr('title', l_Tooltip);
    };
    walletList.prototype.togglePagination = function (aTogglePagination) {
        // Set Flag
        this._togglePagination = aTogglePagination;
        // Show/Hide Pagination
        //
        if (this._showPagination && this._togglePagination)
            jQuery("#divPagination").css("display", "inline-block");
        else
            jQuery("#divPagination").css("display", "none");
        // Rotate Icon
        //
        if (!this._togglePagination)
            jQuery("#lblTogglePagination").css("transform", "rotate(270deg)");
        else
            jQuery("#lblTogglePagination").css("transform", "rotate(90deg)");
        // Set Icon Position
        if (!this._togglePagination)
            jQuery("#lblTogglePagination").css({ "margin-left": "-5px", "margin-top": "0px" });
        else
            jQuery("#lblTogglePagination").css({ "margin-left": "-5px", "margin-top": "-1px" });
        // Set Tooltip
        //
        var l_Tooltip = (this._togglePagination) ? "Collapse" : "Expand";
        jQuery('#btnTogglePagination').attr('title', l_Tooltip);
    };
    walletList.prototype.btnClose_MouseEnter = function () {
        jQuery("#btnClose").removeClass('btn btn-sm btn-secondary').addClass('btn btn-sm btn-danger');
    };
    walletList.prototype.btnClose_MouseLeave = function () {
        jQuery("#btnClose").removeClass('btn btn-sm btn-danger').addClass('btn btn-sm btn-secondary');
    };
    walletList.prototype.closePage = function () {
        this._router.navigate(['000', { cmd: "READ", p1: this.ics._profile.userID }]);
    };
    walletList.prototype.renderAdvancedSearch = function (event) {
        this.toggleSearch(!event);
    };
    walletList.prototype.filterAdvancedSearch = function (event) {
        this._FilterDataset.filterSource = 1;
        this._FilterDataset.filterList = event;
        if (this._OperationMode != "prepareFilter") {
            if (this._FilterDataset.pageNo == 1)
                this.filterRecords();
            else
                this._FilterDataset.pageNo = 1;
        }
    };
    walletList.prototype.changedPager = function (event) {
        if (!this._isLoading) {
            var l_objPagination = event.obj;
            this._FilterDataset.pageNo = l_objPagination.current;
            this._FilterDataset.pageSize = l_objPagination.size;
            this.filterRecords();
        }
    };
    walletList.prototype.filterSearch = function () {
        if (this._toggleSearch)
            this.filterCommonSearch();
        else
            this.ics.send001("FILTER");
    };
    walletList.prototype.filterCommonSearch = function () {
        var l_DateRange;
        var l_SearchString = "";
        this._FilterDataset.filterList = [];
        this._FilterDataset.filterSource = 0;
        if (jQuery.trim(this._SearchString) != "") {
            l_SearchString = jQuery.trim(this._SearchString);
            this._FilterDataset.filterList =
                [
                    { "itemid": "1", "caption": "Search String", "fieldname": "SearchString", "datatype": "simplesearch", "condition": "c", "t1": l_SearchString, "t2": "" }
                ];
        }
        //l_DateRange = { "itemid": "4", "caption": "Date Range", "fieldname": "DateRange", "datatype": "date", "condition": "eq", "t1": this.l_util.getTodayDate(), "t2": "" };
        //this._FilterDataset.filterList.push(l_DateRange);
        if (this._OperationMode != "prepareFilter") {
            if (this._FilterDataset.pageNo == 1)
                this.filterRecords();
            else
                this._FilterDataset.pageNo = 1;
        }
    };
    walletList.prototype.filterRecords = function () {
        var _this = this;
        this._mflag = false;
        var l_Data = this._FilterDataset;
        //let l_ServiceURL: string = this.ics._apiurl + 'service001/getLoanApprovalList';
        var l_ServiceURL = this.ics.ticketurl + 'serviceTicketAdm/getMobileuser';
        // Show loading animation
        this.http.doPost(l_ServiceURL, l_Data).subscribe(function (data) {
            if (data != null) {
                _this._ListingDataset = data;
                _this._shownull = false;
                // Convert to array for single item
                if (_this._ListingDataset.mobileuserData != undefined) {
                    _this._ListingDataset.mobileuserData = _this.l_util.convertToArray(_this._ListingDataset.mobileuserData);
                }
                else {
                    _this._shownull = true;
                }
                // Show / Hide Listing
                _this._showListing = (_this._ListingDataset.mobileuserData != undefined);
                // Show / Hide Pagination
                _this._showPagination = (_this._showListing && (_this._ListingDataset.totalCount > 10));
                // Show / Hide Pagination
                _this.togglePagination(_this._showListing);
                // Hide loading animation
                _this._mflag = true;
            }
        }, function (error) {
            // Hide loading animation
            _this._mflag = true;
            // Show error message
            // this.ics.sendBean({ t1:"rp-error", t2: "Server connection error." });
        }, function () { });
    };
    walletList.prototype.goApproveRejectbyuser = function (syskey, key) {
        var _this = this;
        this._mflag = false;
        if (key == 0) {
            this.muserobj.userstatus = '3';
        }
        else if (key == 1) {
            this.muserobj.userstatus = '8';
        }
        this.muserobj.syskey = syskey;
        var url = (this.ics.cmsurl + 'serviceCMS/approvedbyuser');
        var json = this.muserobj;
        this.http.doPost(url, json).subscribe(function (data) {
            _this._mflag = true;
            _this._result = data;
            _this.showMsg(data.msgDesc, data.state);
            //syskey= data.longResult[0];
            for (var i = 0; i < _this._ListingDataset.mobileuserData.length; i++) {
                if (_this._ListingDataset.mobileuserData[i].syskey == data.longResult[0]) {
                    _this._ListingDataset.mobileuserData[i].t38 = data.keyst;
                }
            }
        }, function (error) {
            _this.showMsg("Can't Saved This Record!", undefined);
        }, function () { });
    };
    walletList.prototype.downloadFile = function (aFileName) {
        var l_Url = this.ics._apiurl + 'service001/downloadexcel?filename=' + aFileName;
        jQuery("#ExcelDownload").html("<iframe src=" + l_Url + "></iframe>");
    };
    walletList.prototype.btnDashboard = function () {
        this._router.navigate(['/pocDashboard']);
    };
    walletList.prototype.goClosePrint = function () {
        this._divexport = false;
    };
    walletList.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    walletList.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    walletList = __decorate([
        core_1.Component({
            selector: 'mobileUserList',
            template: "\n    \n    <div *ngIf=\"!_divexport\" class=\"container-fluid\">\n    <div class=\"row clearfix\">\n        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n            <form class=\"form-horizontal\">\n                <fieldset>\n                    <legend>\n                        <div style=\"display:inline;\">\n                        Wallet User List\n                        </div>\n                        <div style=\"float:right;text-align:right;\"> \n                            <div style=\"display:inline-block;padding-right:0px;width:280px;\">\n                                <div class=\"input-group\"> \n                                    <input class=\"form-control\" type=\"text\" id=\"isInputSearch\" placeholder=\"Search\" autocomplete=\"off\" spellcheck=\"false\" [(ngModel)]=\"_SearchString\" [ngModelOptions]=\"{standalone: true}\" (keyup.enter)=\"filterSearch()\">\n\n                                    <span id=\"btnSimpleSearch\" class=\"input-group-addon\">\n                                        <i class=\"glyphicon glyphicon-search\" style=\"cursor: pointer\" (click)=\"filterSearch()\"></i>\n                                    </span>\n                                </div>\n                            </div>\n\n                            <!--<button id=\"btnToggleSearch\" class=\"btn btn-sm btn-secondary\" type=\"button\" style=\"cursor:pointer;text-align:center;margin-top:-24px;height:34px;\" (click)=\"btnToggleSearch_onClick()\" title=\"Collapse\">\n                                <i id=\"lblToggleSearch\" class=\"glyphicon glyphicon-menu-down\"></i> \n                            </button>-->\n\n                            <button *ngIf=\"false\" id=\"btnTogglePagination\" type=\"button\" class=\"btn btn-sm btn-default\" style=\"cursor:pointer;text-align:center;margin-left:25px;margin-top:-24px;width:30px;height:34px;\" (click)=\"btnTogglePagination_onClick()\" title=\"Collapse\">\n                                <i id=\"lblTogglePagination\" class=\"glyphicon glyphicon-eject\" style=\"color:#2e8690;margin-left:-4px;transform: rotate(90deg)\"></i>\n                            </button>\n\n                            <div id=\"divPagination\" style=\"display:none;margin-left:13px;margin-top:-7px;margin-bottom:5px;\">\n                                <pager6 id=\"pgPagination\" rpPageSizeMax=\"100\" [(rpModel)]=\"_ListingDataset.totalCount\" (rpChanged)=\"changedPager($event)\" style=\"font-size:14px;text-align:right;margin-top:-7px;margin-right:10px;\"></pager6>\n                            </div>\n\n                            <button id=\"btnClose\" type=\"button\" class=\"btn btn-sm btn-secondary\" (click)=\"closePage()\" (mouseenter)=\"btnClose_MouseEnter()\" (mouseleave)=\"btnClose_MouseLeave()\" title=\"Close\" style=\"cursor:pointer;height:34px;text-align:center;margin-top:-24px;\">\n                                <i id=\"lblClose\" class=\"glyphicon glyphicon-remove\"></i> \n                            </button>                     \n                        </div>                        \n                    </legend>\n\t\t\t\t\t<div class=\"cardview list-height\">\n\t\t\t\t\t\t<advanced-search id=\"asAdvancedSearch\" [FilterList]=\"_FilterList\" [TypeList]=\"_TypeList\" rpVisibleItems=5 (rpHidden)=\"renderAdvancedSearch($event)\" (rpChanged)=\"filterAdvancedSearch($event)\" style=\"display:none;\"></advanced-search>\n\n\t\t\t\t\t\t<div *ngIf=\"_shownull!=false\" style=\"color:#ccc;\">No result found!</div>\n\n\t\t\t\t\t\t<div *ngIf=\"_showListing\" class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-md-12\" style=\"overflow-x:auto;\">\n                            <table class=\"table table-striped table-condense table-hover tblborder\">\n                                <thead>\n                                    <tr>\n                                        <th class=\"left\" style=\"width:1%\" title=\"UserID\">User ID</th>\n                                        <th class=\"left\" style=\"width:7%\" title=\"UserName\">User Name</th>\n                                        <th class=\"left\" style=\"width:7%\" title=\"Fatehername\">Father Name</th>\n                                        <th class=\"left\" style=\"width:7%\"title=\"NRC\">NRC</th>\n                                        <th class=\"left\" style=\"width:5%\" title=\"Date of Birth\">Date Of Birth</th>\n                                        <th class=\"left\" style=\"width:8%\" title=\"Address\">Address</th>\n                                        <th class=\"left\" style=\"width:5%\" title=\"Register Date\">Register Date</th>\n                                        <!--<th style=\"width:8%\"></th>-->\n                                    </tr>\n                                </thead>    \n                                <tbody>\n                                    <tr *ngFor=\"let obj of _ListingDataset.mobileuserData\" >\n                                        <td class=\"left\"><a (click)=\"goto(obj)\">{{obj.t1}}</a></td>\n                                        <td class=\"uni\">{{obj.t3}}</td>                                   \n                                        <td class=\"uni\">{{obj.t20}}</td>\n                                        <td class=\"uni\">{{obj.t21}}</td>\n                                        <td >{{obj.t25}}</td>\n                                        <td >{{obj.t24}}</td>\n                                        <td >{{obj.createdDate}}</td>\n                                        <!--<td><button class=\"btn btn-sm btn-primary\" [disabled]=\"obj.t38=='3'\" type=\"button\" (click)=\"goApproveRejectbyuser(obj.syskey,0)\">Approve</button>&nbsp; \n                                        <button class=\"btn btn-sm btn-primary\" [disabled]=\"obj.t38=='8'\" type=\"button\" (click)=\"goApproveRejectbyuser(obj.syskey,1)\">Reject</button>\n                                        </td>-->                                      \n                                    </tr> \n                                </tbody>\n                            </table>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div *ngIf=\"_showListing\" style=\"text-align:center\">Total {{ _ListingDataset.totalCount }}</div>\n\t\t\t\t\t</div>\n                </fieldset>\n            </form>\n\n            <div id=\"ExcelDownload\" style=\"display: none;width: 0px;height: 0px;\"></div>\n\n        </div> \n    </div>\n</div>\n\n<div [hidden]=\"_mflag\">\n<div  id=\"loader\" class=\"modal\" ></div>\n</div>\n\n<div *ngIf='_divexport'>\n    <button type=\"button\" class=\"close\" (click)=\"goClosePrint()\" style=\"margin-top:-20px;\">&times;</button>\n    <iframe id=\"frame1\" [src]=\"_printUrl\" style=\"width: 100%;height: 95%;\"></iframe> \n</div>\n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_http_service_1.RpHttpService, rp_client_util_1.ClientUtil, rp_references_1.RpReferences])
    ], walletList);
    return walletList;
}());
exports.walletList = walletList;
//# sourceMappingURL=walletList.component.js.map