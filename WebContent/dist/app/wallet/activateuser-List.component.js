"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
// RP Framework
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_client_util_1 = require('../util/rp-client.util');
core_2.enableProdMode();
var FrmActivateUserListComponent = (function () {
    function FrmActivateUserListComponent(ics, _router, http) {
        this.ics = ics;
        this._router = _router;
        this.http = http;
        // Application Specific
        this._searchVal = ""; // simple search
        this._flagas = true; // flag advance search
        this._col_list = [{ "col_no": "1", "flag": false }, { "col_no": "2", "flag": false }];
        this._sorting = { _sort_type: "asc", _sort_col: "1" };
        this._mflag = true;
        this._util = new rp_client_util_1.ClientUtil();
        this.sessionAlertMsg = "";
        this._userobj = {
            "data": [{ "syskey": 0, "userId": "", "userName": "", "recordStatus": 0, "t1": "", "n1": 0 }],
            "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0, "msgCode": "", "msgDesc": ""
        };
        this._pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 0 };
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            //jQuery("#loader").modal('hide');
            this._mflag = false;
            this.search();
        }
    }
    // list sorting part
    FrmActivateUserListComponent.prototype.changeDefault = function () {
        for (var i = 0; i < this._col_list.length; i++) {
            this._col_list[i].flag = false;
        }
    };
    FrmActivateUserListComponent.prototype.addSort = function (e) {
        for (var i = 0; i < this._col_list.length; i++) {
            if (this._col_list[i].col_no == e) {
                var _tmp_flag = this._col_list[i].flag;
                this.changeDefault();
                this._col_list[i].flag = !_tmp_flag;
                this._sorting._sort_type = !_tmp_flag ? "desc" : "asc";
                this._sorting._sort_col = e;
                break;
            }
        }
    };
    FrmActivateUserListComponent.prototype.changedPager = function (event) {
        if (this._userobj.totalCount != 0) {
            this._pgobj = event;
            var current = this._userobj.currentPage;
            var size = this._userobj.pageSize;
            this._userobj.currentPage = this._pgobj.current;
            this._userobj.pageSize = this._pgobj.size;
            if (this._pgobj.current != current || this._pgobj.size != size) {
                this.search();
            }
        }
    };
    FrmActivateUserListComponent.prototype.search = function () {
        var _this = this;
        try {
            this._mflag = false;
            this.http.doGet(this.ics.cmsurl + 'service001/getUserListing?searchVal=' + encodeURIComponent(this._userobj.searchText) + '&pagesize=' + this._userobj.pageSize + '&currentpage=' + this._userobj.currentPage + '&operation=activate&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this.showMessage(data.msgDesc, false);
                }
                else {
                    if (data != null && data != undefined) {
                        _this._pgobj.totalcount = data.totalCount;
                        _this._userobj = data;
                        if (_this._userobj.totalCount == 0) {
                            _this.showMessage("Data not found!", false);
                        }
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    FrmActivateUserListComponent.prototype.searchKeyup = function (e) {
        if (e.which == 13) {
            this._userobj.currentPage = 1;
            this._msghide = true;
            this._recordhide = false;
            this.search();
        }
    };
    FrmActivateUserListComponent.prototype.Searching = function () {
        this._userobj.currentPage = 1;
        this._msghide = true;
        this._recordhide = false;
        this.search();
    };
    FrmActivateUserListComponent.prototype.goto = function (p) {
        this._router.navigate(['/Activate User', 'read', p]);
    };
    FrmActivateUserListComponent.prototype.goBack = function () {
        this._router.navigate(['/Activate User', 'new']);
    };
    //showMessage() {
    //    jQuery("#sessionalert").modal();
    //    Observable.timer(3000).subscribe(x => {
    //        this.goLogOut();
    //    });
    //}
    FrmActivateUserListComponent.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    FrmActivateUserListComponent.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "primary", "t3": msg });
        }
    };
    FrmActivateUserListComponent = __decorate([
        core_1.Component({
            selector: 'activateuser-list',
            template: " \n    <div class=\"container col-md-12 col-sm-12 col-xs-12\">\n    <form class=\"form-horizontal\"> \n    <legend>User List</legend>\n\t\t<div class=\"cardview list-height\"> \n\t\t\t<div class=\"row col-md-12\"><!--*ngIf=\"_flagas\"-->\n\t\t\t\t<div class=\"row col-md-3\">\n\t\t\t\t\t<div  class=\"input-group\">\n\t\t\t\t\t\t<span class=\"input-group-btn\">\n\t\t\t\t\t\t\t<button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goBack();\">Back</button>\n\t\t\t\t\t\t</span> \n\t\t\t\t\t\t<input id=\"textinput\" name=\"textinput\" type=\"text\"  placeholder=\"Search\" [(ngModel)]=\"_userobj.searchText\" (keyup)=\"searchKeyup($event)\" maxlength=\"30\" class=\"form-control input-sm\">\n\t\t\t\t\t\t<span class=\"input-group-btn\">\n\t\t\t\t\t\t\t<button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"Searching()\" >\n\t\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-search\"></span>Search\n\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t</span>        \n\t\t\t\t\t</div> \n\t\t\t\t</div>\t\t\n\t\t\t\t<div class=\"pagerright\">\n\t\t\t\t\t<adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"_userobj.totalCount\" (rpChanged)=\"changedPager($event)\" ></adminpager> \n\t\t\t\t</div>\n\t\t\t</div>\t\t\t\t\t\n\t\t\t  \n\t\t\t  <table class=\"table table-striped table-condensed table-hover tblborder\" id=\"tblstyle\">\n\t\t\t\t<thead>\n\t\t\t\t<tr>\n                    <th style=\"width:6%\">ID </th>\n                    <th style=\"width:10%\">Name </th>      \n                    <th style=\"width:20%\">Status</th>     \n                </tr>\n\t\t\t  </thead>\n              <tbody>\n                  <tr *ngFor=\"let obj of _userobj.data\">\n                      <td><a (click)=\"goto(obj.syskey)\">{{obj.t1}}</a></td>\n                      <td>{{obj.t3}}</td>\n                      <td *ngIf=\"obj.n1 == 11\" >Lock</td>\n                      <td *ngIf=\"obj.recordStatus == 1 \" >Save</td>\n                      <td *ngIf=\"obj.recordStatus == 2 && obj.n1 == 0\" >Activate</td>\n                      <td *ngIf=\"obj.recordStatus == 21 \" >Deactivate</td>\n                  </tr> \n\t\t\t\t<!--<tr *ngFor=\"let obj of _userobj.data\">\n\t\t\t\t\t<td><a (click)=\"goto(obj.syskey)\">{{obj.t1}}</a></td>\n\t\t\t\t\t<td>{{obj.username}}</td>\n\t\t\t\t\t<td *ngIf=\"obj.n7 == 11\" >Lock</td>\n\t\t\t\t\t<td *ngIf=\"obj.recordStatus == 1 \" >Save</td>\n\t\t\t\t\t<td *ngIf=\"obj.recordStatus == 2 && obj.n7 == 0 \" >Activate</td>\n\t\t\t\t\t<td *ngIf=\"obj.recordStatus == 21 \" >Deactivate</td>\n\t\t\t\t</tr>-->  \n\t\t\t  </tbody>\n\t\t\t  </table>\n\t\t</div>\n\t</form>\n</div> \n  \n    <div [hidden] = \"_mflag\">\n        <div class=\"modal\" id=\"loader\"></div>\n    </div>\n   "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_http_service_1.RpHttpService])
    ], FrmActivateUserListComponent);
    return FrmActivateUserListComponent;
}());
exports.FrmActivateUserListComponent = FrmActivateUserListComponent;
//# sourceMappingURL=activateuser-List.component.js.map