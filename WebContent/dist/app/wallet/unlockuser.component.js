"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var Rx_1 = require('rxjs/Rx');
core_2.enableProdMode();
var FrmUnlockUserComponent = (function () {
    function FrmUnlockUserComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
        this._obj = this.getDefaultObj();
        this.sessionAlertMsg = "";
        this._key = "";
        this._mflag = false;
        this._dates = { "date1": "", "date2": "" };
        this._util = new rp_client_util_1.ClientUtil();
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this.checkSession();
            this._mflag = true;
            jQuery("#myunlock").prop("disabled", true);
            this.msghide = true;
            this._obj = this.getDefaultObj();
        }
    }
    FrmUnlockUserComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this._key = id;
                _this.goReadBySyskey(id);
            }
        });
    };
    FrmUnlockUserComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    FrmUnlockUserComponent.prototype.getDefaultObj = function () {
        return { "createdDate": "", "t1": "", "t3": "", "t21": "", "t41": "" };
    };
    FrmUnlockUserComponent.prototype.goReadBySyskey = function (p) {
        var _this = this;
        try {
            this._mflag = false;
            var url = this.ics.cmsurl + 'service001/getUserProfileData?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID + '&operation=unlock';
            var json = p;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMsg(data.msgDesc, false);
                }
                else {
                    _this._obj = data;
                    _this.registeredDate = _this._obj.createdDate;
                    _this._obj.createdDate = _this.registeredDate.substring(0, 4) + "-" + _this.registeredDate.substring(4, 6) + "-" + _this.registeredDate.substring(6, _this.registeredDate.length);
                    jQuery("#myunlock").prop("disabled", false);
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    FrmUnlockUserComponent.prototype.goList = function () {
        this._router.navigate(['/unlockuserlist']);
    };
    FrmUnlockUserComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this.goLogOut();
        });
    };
    FrmUnlockUserComponent.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    FrmUnlockUserComponent.prototype.confirmLock = function () {
        jQuery("#unlockconfirm").modal('show');
    };
    FrmUnlockUserComponent.prototype.showMessageAlert = function (msg) {
        /* if (bool == "true") { this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg }); }
        if (bool == "false") { this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg }); }
        if (bool == "undefined") { this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg }); } */
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmUnlockUserComponent.prototype.messagealert = function () {
        var _this = this;
        this.messagehide = false;
        setTimeout(function () { return _this.messagehide = true; }, 3000);
    };
    FrmUnlockUserComponent.prototype.goUnlock = function () {
        var _this = this;
        try {
            jQuery("#unlockconfirm").modal('hide');
            this._mflag = false;
            var url = this.ics.cmsurl + 'service001/lockunlockUser?k=unlock&userid=' + this.ics._profile.userID + '&sessionID=' + this.ics._profile.sessionID + '&operation=unlock';
            var json = this._key;
            this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
            this.http.doPost(url, json).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMsg(data.msgDesc, false);
                }
                else {
                    _this._returnResult = data;
                    if (_this._returnResult.state) {
                        _this.msg = "Unlocked";
                        _this.msghide = false;
                        jQuery("#myunlock").prop("disabled", true);
                    }
                    _this.showMsg(data.msgDesc, true);
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    FrmUnlockUserComponent.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    FrmUnlockUserComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsgAlert(data.desc);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsgAlert(data.desc);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmUnlockUserComponent.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmUnlockUserComponent.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    FrmUnlockUserComponent.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    FrmUnlockUserComponent = __decorate([
        core_1.Component({
            selector: 'unlockuser',
            template: "\n  <div class=\"container-fluid\">\n  <div class=\"row clearfix\">\n  <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n    <form class= \"form-horizontal\">  \n          <!-- Form Name -->\n          <legend>Unlock User</legend>\n          <div class=\"cardview list-height\">\n\t\t\t\t<div class=\"row col-md-12\">    \n\t\t\t\t<button class=\"btn btn-sm btn-primary\" type=\"button\"  (click)=\"goList()\">List</button>\n\t\t\t\t<button class=\"btn btn-sm btn-primary\" disabled id=\"myunlock\"  type=\"button\"  (click)=\"confirmLock()\">Unlock</button>\n        </div>\t\n        \n        <div class=\"row col-md-12\">&nbsp;</div>\n        <div class=\"form-group\">\n\t\t\t\t<div class=\"col-md-8\">\n\t\t\t\t  <div class=\"form-group\">\n\t\t\t\t\t<rp-input [(rpModel)]=\"_obj.t1\" rpRequired =\"true\" rpType=\"text\" rpClass=\"col-md-4\" rpLabelClass =\"col-md-2 control-label\" rpLabel=\"Login ID\" rpReadonly=\"true\"></rp-input>\n\t\t\t\t  </div>\n\n\t\t\t\t  <div class=\"form-group\">\n\t\t\t\t\t<rp-input [(rpModel)]=\"_obj.t3\" rpRequired =\"true\" rpType=\"text\" rpClass=\"col-md-4\" rpLabelClass =\"col-md-2 control-label\" rpLabel=\"Name\" rpReadonly=\"true\"></rp-input>\n\t\t\t\t  </div>\n\n\t\t\t\t  <div class=\"form-group\">\n\t\t\t\t\t<rp-input  rpType=\"text\" rpClass=\"col-md-4\" rpLabelClass =\"col-md-2 control-label\" rpLabel=\"NRC\" [(rpModel)]=\"_obj.t21\"  rpReadonly=\"true\"></rp-input>\n\t\t\t\t  </div>\n\n\t\t\t\t  <!--<div class=\"form-group\">\n\t\t\t\t\t<rp-input  rpType=\"text\" rpLabel=\"Phone No.\" [(rpModel)]=\"_obj.t4\"  rpReadonly=\"true\"></rp-input>   \n\t\t\t\t  </div>--> \n\t\t\t\t</div> \n\t\t\t\t<div class=\"col-md-2\">\n\t\t\t\t  <div [hidden]=\"msghide\">\n\t\t\t\t\t<h3 align=\"right\"><b>{{msg}}</b></h3>\n\t\t\t\t  </div>\n\t\t\t\t</div>\n      </div>\n      </div>\n        </form>\n      </div>\n    </div>\n  </div>\n\n  <div id=\"unlockconfirm\" class=\"modal fade\">\n    <div class=\"modal-dialog\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n          <h4 class=\"modal-title\">Confirmation</h4>\n        </div>\n        <div class=\"modal-body\">\n          <p>Do you want to unlock?</p>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-primary\" (click)=\"goUnlock()\">Yes</button>\n          <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\">Cancel</button>\n        </div>\n      </div>\n    </div>\n  </div>\n    \n  <div id=\"sessionalert\" class=\"modal fade\">\n    <div class=\"modal-dialog\">\n      <div class=\"modal-content\">\n        <div class=\"modal-body\">\n          <p>{{sessionAlertMsg}}</p>\n        </div>\n        <div class=\"modal-footer\">\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div [hidden] = \"_mflag\">\n    <div class=\"modal\" id=\"loader\"></div>\n  </div>\n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmUnlockUserComponent);
    return FrmUnlockUserComponent;
}());
exports.FrmUnlockUserComponent = FrmUnlockUserComponent;
//# sourceMappingURL=unlockuser.component.js.map