"use strict";
var router_1 = require('@angular/router');
var walletList_component_1 = require('./walletList.component');
var wallet_component_1 = require('./wallet.component');
var activateuser_component_1 = require('./activateuser.component');
var activateuser_List_component_1 = require('./activateuser-List.component');
var deactivateuser_component_1 = require('./deactivateuser.component');
var deactivateuser_List_component_1 = require('./deactivateuser-List.component');
var lockuser_component_1 = require('./lockuser.component');
var lockuser_List_component_1 = require('./lockuser-List.component');
var unlockuser_component_1 = require('./unlockuser.component');
var unlockuser_List_component_1 = require('./unlockuser-List.component');
var resetpassword_component_1 = require('./resetpassword.component');
var pocRoutes = [
    { path: 'walletList', component: walletList_component_1.walletList },
    { path: 'walletData', component: wallet_component_1.WalletComponent },
    { path: 'walletData/:cmd', component: wallet_component_1.WalletComponent },
    { path: 'walletData/:cmd/:id', component: wallet_component_1.WalletComponent },
    { path: 'Activate User', component: activateuser_component_1.FrmActivateUserComponent },
    { path: 'Activate User/:cmd', component: activateuser_component_1.FrmActivateUserComponent },
    { path: 'Activate User/:cmd/:id', component: activateuser_component_1.FrmActivateUserComponent },
    { path: 'ActivateUserList', component: activateuser_List_component_1.FrmActivateUserListComponent },
    { path: 'Deactivate User', component: deactivateuser_component_1.FrmDeactivateUserComponent },
    { path: 'Deactivate User/:cmd', component: deactivateuser_component_1.FrmDeactivateUserComponent },
    { path: 'Deactivate User/:cmd/:id', component: deactivateuser_component_1.FrmDeactivateUserComponent },
    { path: 'deactivateuserlist', component: deactivateuser_List_component_1.FrmDeactivateUserListComponent },
    { path: 'Lock User', component: lockuser_component_1.FrmLockUserComponent },
    { path: 'Lock User/:cmd', component: lockuser_component_1.FrmLockUserComponent },
    { path: 'Lock User/:cmd/:id', component: lockuser_component_1.FrmLockUserComponent },
    { path: 'lockuserlist', component: lockuser_List_component_1.FrmLockUserListComponent },
    { path: 'Unlock User', component: unlockuser_component_1.FrmUnlockUserComponent },
    { path: 'Unlock User/:cmd', component: unlockuser_component_1.FrmUnlockUserComponent },
    { path: 'Unlock User/:cmd/:id', component: unlockuser_component_1.FrmUnlockUserComponent },
    { path: 'unlockuserlist', component: unlockuser_List_component_1.FrmUnlockUserListComponent },
    { path: 'Reset Password', component: resetpassword_component_1.ResetPasswordComponent },
];
exports.walletRouting = router_1.RouterModule.forChild(pocRoutes);
//# sourceMappingURL=wallet.routing.js.map