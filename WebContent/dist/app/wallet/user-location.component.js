"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var GeoLocationService_1 = require('../framework/GeoLocationService');
core_2.enableProdMode();
var userlocationComponet = (function () {
    function userlocationComponet(ics, ref, _router, http, geoLocationService, l_util) {
        this.ics = ics;
        this.ref = ref;
        this._router = _router;
        this.http = http;
        this.geoLocationService = geoLocationService;
        this.l_util = l_util;
        this._mflag = false;
        this.ticketObj = this.getDefaultObj();
        this.markers = [];
        this.marker = [];
        this.address = [];
        this.ticketStatus = "";
        this.ticketRegion = "";
        this.coordinates = {
            latitude: 0,
            longitude: 0
        };
        this.userData = { "list": [{ "autokey": 0, "syskey": 0, "name": "", "nrc": "", "address": "" }] };
        //sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        // _doLoading = false; 
        this.channelName = "";
        this._alertflag = true;
        this._alertmsg = "";
        this._alerttype = "";
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this.checkSession();
            this.ticketStatus = "All";
            this.getAllUserInfo();
        }
    }
    userlocationComponet.prototype.getDefaultObj = function () {
        return {
            "ticketData": [{
                    "autokey": 0, "syskey": 0, "createdDate": "", "modifiedDate": "", "date": "",
                    "time": "", "shootDate": "", "shootTime": "", "userID": "", "n1": 0,
                    "n2": 0, "t1": "", "t2": "", "t3": "", "t4": "",
                    "t5": "", "t6": "", "t7": "", "t8": "", "t9": "",
                    "t10": "", "t11": ""
                }], "state": true, "sessionID": "", "userID": "", "ticketStatus": "",
            "aFromDate": "",
            "aToDate": "",
            "alldate": false
        };
    };
    userlocationComponet.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    userlocationComponet.prototype.ngOnInit = function () {
        var _this = this;
        this.geoLocationService.getPosition().subscribe(function (pos) {
            _this.coordinates = {
                latitude: +(pos.coords.latitude),
                longitude: +(pos.coords.longitude)
            };
        });
    };
    userlocationComponet.prototype.getAllUserInfo = function () {
        var _this = this;
        try {
            this.markers = [];
            this._mflag = false;
            var url = this.ics.cmsurl + 'service001/selectUserInfo';
            var json = this._sessionObj;
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            this.http.doPost(url, json).subscribe(function (data) {
                console.log("data:" + JSON.stringify(data));
                if (data == undefined || data == null && data.length == 0) {
                    _this.showMsg("No User Data", false);
                    _this.userData = null;
                    _this._mflag = true;
                    _this.showNoLatLongMap();
                }
                else {
                    _this.userData = data;
                    if (!(data.list instanceof Array)) {
                        var m = [];
                        m[0] = data.list;
                        _this.userData.list = m;
                    }
                    else {
                        _this.userData.list = data.list;
                    }
                    for (var i = 0; i < _this.userData.list.length; i++) {
                        var info = '';
                        info = "Name: " + _this.userData.list[i].name + "\n" + "Address: " + _this.userData.list[i].nrc;
                        _this.address.push(_this.userData.list[i].nrc);
                    }
                    if (_this.userData.list.length != 0)
                        _this.showUserMap();
                    else
                        _this.showMsg("No User Data", false);
                    _this._mflag = true;
                }
            }, function (error) {
                _this._mflag = true;
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () {
                _this._mflag = true;
            });
        }
        catch (e) {
            this._mflag = true;
            alert("Invalid URL.");
        }
    };
    userlocationComponet.prototype.showUserMap = function () {
        var geocoder;
        var map;
        geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(16.7895532, 96.20043829999997);
        var mapOptions = {
            zoom: 8,
            center: latlng
        };
        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        for (var j = 0; j < this.address.length; j++) {
            geocoder.geocode({ 'address': this.address[j] }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    console.log("geo-location:" + results[0].geometry.location);
                    // map.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location,
                    });
                }
                else if (status === google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                    setTimeout(function () {
                        //codeAddress(zip);
                    }, 250);
                }
                else {
                    alert('Geocode was not successful for the following reason: ' + status);
                }
            });
        }
    };
    userlocationComponet.prototype.showNoLatLongMap = function () {
        var _this = this;
        if (this.coordinates == undefined || this.coordinates.latitude == undefined || this.coordinates.latitude == null
            || this.coordinates.latitude == 0 || this.coordinates.longitude == undefined
            || this.coordinates.longitude == null || this.coordinates.longitude == 0) {
            //Naypyitaw
            this.coordinates.latitude = 19.7450008;
            this.coordinates.longitude = 96.1297226;
        }
        var map = new google.maps.Map(document.getElementById('map-canvas'));
        var infowindow = new google.maps.InfoWindow();
        var latlng = new google.maps.LatLng(this.coordinates.latitude, this.coordinates.longitude);
        map = new google.maps.Map(document.getElementById('map-canvas'), {
            zoom: 8,
            center: latlng
        }, function (error) {
            _this._mflag = true;
        });
    };
    userlocationComponet.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    userlocationComponet.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    userlocationComponet.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    userlocationComponet.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsg(data.desc, false);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsg(data.desc, false);
                    }
                }
            }, function (error) {
                console.log("error=" + JSON.stringify(error));
                // if (error._body.type == "error") {
                // }
                alert("Connection Timed Out.");
                _this._mflag = true;
            }, function () {
                _this._mflag = true;
            });
        }
        catch (e) {
            this._mflag = true;
            alert("Invalid URL.");
        }
    };
    userlocationComponet = __decorate([
        core_1.Component({
            selector: 'ticket-dashboard',
            template: "\n  <div class=\"container col-md-12 col-sm-12 col-xs-12\">\n  <form class=\"form-horizontal\">\n      <legend>   \n          User Location\n      </legend> \n      <div class=\"cardview list-height\">  \n        <div class=\"form-group\">\n            <sebm-google-map [latitude]=\"lat\" [longitude]=\"lng\"></sebm-google-map>\n            <div id=\"map-canvas\" style=\"height:450px\"></div>\n        </div>\n      </div>\n  </form>\n</div>\n\n<div [hidden]=\"_mflag\">\n    <div  id=\"loader\" class=\"modal\" ></div>\n</div>\n"
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, rp_references_1.RpReferences, router_1.Router, rp_http_service_1.RpHttpService, GeoLocationService_1.GeoLocationService, rp_client_util_1.ClientUtil])
    ], userlocationComponet);
    return userlocationComponet;
}());
exports.userlocationComponet = userlocationComponet;
//# sourceMappingURL=user-location.component.js.map