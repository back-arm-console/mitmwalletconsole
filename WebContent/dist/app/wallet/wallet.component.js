"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
core_1.enableProdMode();
var WalletComponent = (function () {
    function WalletComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._util = new rp_client_util_1.ClientUtil();
        this.image = "";
        this._mflag = false;
        this._obj = this.getDefaultObj();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this.imageLink = this.ics._profileImage1;
            this._obj = this.getDefaultObj();
        }
    }
    WalletComponent.prototype.getDefaultObj = function () {
        return {
            "syskey": "", "autokey": "", "t1": "", "t3": "", "t20": "", "t21": "", "t24": "", "t25": "", "createdDate": "", "t16": "", "accNumber": "", "state": false
        };
    };
    WalletComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
                var _List = _this.ics.getBean().walletdata;
                _this._mflag = true;
                _this._obj.t1 = _List.t1;
                _this._obj.t3 = _List.t3;
                _this._obj.t20 = _List.t20;
                _this._obj.t21 = _List.t21;
                _this._obj.t25 = _List.t25;
                _this._obj.t24 = _List.t24;
                _this._obj.createdDate = _List.createdDate;
                _this._obj.t16 = _List.t16;
                _this._obj.accNumber = _List.accNumber;
                _this.image = _this.imageLink + _List.t16;
            }
        });
    };
    WalletComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    WalletComponent.prototype.goList = function () {
        this._router.navigate(['/walletList']);
    };
    WalletComponent.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    WalletComponent.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    WalletComponent = __decorate([
        core_1.Component({
            selector: 'mobileUser',
            template: "\n    <div class=\"container-fluid\">\n    <div class=\"row clearfix\">\n      <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n         <form class=\"form-horizontal\" ngNoForm>\n               <legend>Wallet User</legend>\n                    <div class=\"cardview list-height\">      \n                         <div class=\"row col-md-12\">\n                              <div class=\"col-md-12\" style=\"margin-bottom: 10px;\">\n                                   <button class=\"btn btn-sm btn-primary\" id=\"vh_list_btn\" type=\"button\" (click)=\"goList()\">List</button>\n                              </div>\n                            <div >\n                       <div class=\"row col-md-12\">&nbsp;</div>\n                              <div class=\"form-group\">\n                                <div class=\"row1 col-md-6\">\n                                <div class=\"form-group\">\t\t\t\t\t\t\n\t\t\t\t\t\t                  <label class=\"col-md-4\">User ID </label>\t\t\t\t\t\t\n                                                 <div class=\"col-md-8\">\n                                                       <input disabled readonly type=\"text\" [(ngModel)]=\"_obj.t1\" class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\">\n                                                 </div>\n\t\t\t\t\t\t             </div>\n\t\t\t\t\t\t\n\t\t\t\t\t\t             <div class=\"form-group\">\n\t\t\t\t\t\t                  <label class=\"col-md-4\">User Name</label>\n                                                 <div class=\"col-md-8\">\n                                                      <div class=\"uni\">\n                                                           <input disabled readonly type=\"text\" [(ngModel)]=\"_obj.t3\" class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\">\n                                                      </div> \n                                                  </div>\n\t\t\t\t\t\t             </div>\n                          \n                                </div>\n\n                                <div class=\"row1 col-md-6\">\n                                      <div class=\"form-group\">\t\n                                           <div class=\"row col-md-3\">\n                                                <img src=\"{{image}}\" alt={{img}} style=\"width:70px;height:70px;\"/>\n                                           </div>\n                                      </div>\n                                 </div> \n\t\t\t\t\t\t\t\t\t \n\t\t\t\t\t\t\n\t\t\t\t\t\t             <!--<div class=\"row1 col-md-6\">\n\t\t\t\t\t\t             <div class=\"form-group\">  \n                                          <label class=\"col-md-4\">Address</label>\n                                                 <div class=\"col-md-8\">\n                                                      <div class=\"uni\">\n                                                           <textarea disabled readonly id=\"textarea-custom\" [(ngModel)]=\"_obj.t24\" class=\"form-control\" [ngModelOptions]=\"{standalone: true}\"></textarea>\n                                                      </div>\n                                                  </div>\n\t\t\t\t\t                 </div>\n                                    </div>-->\n                                    <div class=\"row1 col-md-6\">\n                                    <div class=\"form-group\">\t\t\n\t\t\t\t\t\t                  <label class= \"col-md-4\">Father Name</label>\n                                                 <div class= \"col-md-8\">\n                                                      <div class=\"uni\">                  \n                                                           <input disabled readonly type=\"text\" [(ngModel)]=\"_obj.t20\" class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\">\n                                                      </div>\n                                                 </div>\n                                     </div>\n                                     </div>\n\n                                    <div class=\"row1 col-md-6\">\n                                    <div class=\"form-group\">\t\t\t\t\t\t\n\t\t\t\t\t\t                  <label class=\"col-md-4\">NRC </label>\n                                                 <div class=\"col-md-8\">\n                                                      <div class=\"uni\">\n                                                           <input disabled readonly type=\"text\" [(ngModel)]=\"_obj.t21\" class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\">\n                                                      </div>\n                                                 </div>\n                                     </div>\n                                     </div>\n                                    \n                                    <div class=\"row1 col-md-6\">\n\t\t\t\t\t                 <div class=\"form-group\">\t\n\t\t\t\t\t                       <label class=\"col-md-4\">Date Of Birth</label>\n                                                  <div class=\"col-md-8\">\n                                                       <div class=\"uni\">\n                                                             <input disabled readonly type=\"text\" [(ngModel)]=\"_obj.t25\" class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\">\n                                                       </div>\n                                                  </div>\t\n                                     </div>\n                                     </div>\n                                    \n                                    <div class=\"row1 col-md-6\">\n\t\t\t\t\t\t             <div class=\"form-group\">\n                                          <label class=\"col-md-4\">Register date</label>\n                                                 <div class=\"col-md-8\">\n                                                       <input disabled readonly type=\"text\" [(ngModel)]=\"_obj.createdDate\" class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\">\n                                                 </div>\n                                     </div>\n                                     </div>\t\n\n                                    <!-- <div class=\"row1 col-md-6\">\n\t\t\t\t\t                 <div class=\"form-group\">\t\n\t\t\t\t\t                       <label class=\"col-md-4\">Date Of Birth</label>\n                                                  <div class=\"col-md-8\">\n                                                       <div class=\"uni\">\n                                                             <input disabled readonly type=\"text\" [(ngModel)]=\"_obj.t25\" class=\"form-control\" [ngModelOptions]=\"{standalone: true}\">\n                                                       </div>\n                                                  </div>\t\n                                     </div>\n                                     </div>-->\t\n                                     <div class=\"row1 col-md-6\">\n\t\t\t\t\t\t             <div class=\"form-group\">  \n                                          <label class=\"col-md-4\">Address</label>\n                                                 <div class=\"col-md-8\">\n                                                      <div class=\"uni\">\n                                                           <textarea disabled readonly id=\"textarea-custom\" [(ngModel)]=\"_obj.t24\" class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\"></textarea>\n                                                      </div>\n                                                  </div>\n\t\t\t\t\t                 </div>\n                                    </div>\n                                    \n                                    <div class=\"row1 col-md-6\">\n                                     <div class=\"form-group\">\t\t\t\t\t\t\n\t\t\t\t\t\t                  <label class=\"col-md-4\">AccNumber </label>\n                                                 <div class=\"col-md-8\">                             \n                                                       <input disabled readonly type=\"text\" [(ngModel)]=\"_obj.accNumber\" class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\">                            \n                                                 </div>\n                                     </div>\n                                     \n                                     </div>\n                        \n\t\t\t\t\t\t\n\t\t\t\t\t\t        <!--</div>-->\n\t\t\t\t\t\t    </div>\n                     </div>\n               </div>\n               </div>\n          </form>\n      </div>\n     </div>\n</div>\n \n  <div [hidden]=\"_mflag\">\n    <div  id=\"loader\" class=\"modal\" ></div>\n  </div>\n"
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], WalletComponent);
    return WalletComponent;
}());
exports.WalletComponent = WalletComponent;
//# sourceMappingURL=wallet.component.js.map