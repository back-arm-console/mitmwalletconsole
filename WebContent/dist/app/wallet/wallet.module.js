"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var common_1 = require('@angular/common');
var rp_input_module_1 = require('../framework/rp-input.module');
var wallet_routing_1 = require('./wallet.routing');
var rp_http_service_1 = require('../framework/rp-http.service');
var adminpager_module_1 = require('../util/adminpager.module');
var walletList_component_1 = require('./walletList.component');
var pager_module_1 = require('../util/pager.module');
var advancedsearch_module_1 = require('../util/advancedsearch.module');
var mydatepicker_1 = require('mydatepicker');
var wallet_component_1 = require('./wallet.component');
var activateuser_component_1 = require('./activateuser.component');
var activateuser_List_component_1 = require('./activateuser-List.component');
var deactivateuser_component_1 = require('./deactivateuser.component');
var deactivateuser_List_component_1 = require('./deactivateuser-List.component');
var lockuser_component_1 = require('./lockuser.component');
var lockuser_List_component_1 = require('./lockuser-List.component');
var unlockuser_component_1 = require('./unlockuser.component');
var unlockuser_List_component_1 = require('./unlockuser-List.component');
var resetpassword_component_1 = require('./resetpassword.component');
var walletModule = (function () {
    function walletModule() {
    }
    walletModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                forms_1.FormsModule,
                rp_input_module_1.RpInputModule,
                wallet_routing_1.walletRouting,
                adminpager_module_1.AdminPagerModule,
                pager_module_1.PagerModule,
                advancedsearch_module_1.AdvancedSearchModule,
                mydatepicker_1.MyDatePickerModule,
            ],
            exports: [],
            declarations: [
                walletList_component_1.walletList,
                wallet_component_1.WalletComponent,
                activateuser_component_1.FrmActivateUserComponent,
                activateuser_List_component_1.FrmActivateUserListComponent,
                deactivateuser_component_1.FrmDeactivateUserComponent,
                deactivateuser_List_component_1.FrmDeactivateUserListComponent,
                lockuser_component_1.FrmLockUserComponent,
                lockuser_List_component_1.FrmLockUserListComponent,
                unlockuser_component_1.FrmUnlockUserComponent,
                unlockuser_List_component_1.FrmUnlockUserListComponent,
                resetpassword_component_1.ResetPasswordComponent,
            ],
            providers: [
                rp_http_service_1.RpHttpService
            ],
            schemas: [core_1.CUSTOM_ELEMENTS_SCHEMA]
        }), 
        __metadata('design:paramtypes', [])
    ], walletModule);
    return walletModule;
}());
exports.walletModule = walletModule;
//# sourceMappingURL=wallet.module.js.map