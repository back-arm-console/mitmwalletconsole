"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
require('rxjs/add/operator/map');
var Subject_1 = require('rxjs/Subject');
var FrmDashboardService = (function () {
    function FrmDashboardService() {
        this._rpDashboardSource = new Subject_1.Subject();
        this.rpDashboard$ = this._rpDashboardSource.asObservable();
        this._obj = this.getObj();
    }
    FrmDashboardService.prototype.getObj = function () {
        return {
            "caption": "", "value": 0
        };
    };
    FrmDashboardService.prototype.generatePieChart = function (id, d, maintitle, param, psize) {
        var _this = this;
        Highcharts.setOptions({
            chart: {
                style: {
                    fontFamily: 'Pyidaungsu'
                }
            }
        });
        Highcharts.chart(id, {
            chart: { plotBackgroundColor: null, plotBorderWidth: null, plotShadow: false, type: 'pie', marginTop: 1 },
            title: { text: maintitle },
            tooltip: { pointFormat: '</b>{point.percentage:.2f}%</b>' },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    size: psize,
                    events: {
                        click: function (event) {
                            if (param != "") {
                                _this.goClick(event.point.options.val, param);
                            }
                        },
                    },
                    dataLabels: {
                        enabled: true,
                        format: '{point.percentage:.2f}%',
                        style: { color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black' }
                    },
                    showInLegend: true
                }
            },
            credits: { enabled: false },
            series: [{ name: 'Brands', colorByPoint: true, data: d }],
            legend: { align: 'left', layout: 'vertical', verticalAlign: 'left', font: '10px', x: 1, y: 150, }
        });
    };
    FrmDashboardService.prototype.generateLineChart = function (id, d, c, xt, yt, minNumber, title, param, pointerflag, paramJsonArr, paramJsonArrflag) {
        Highcharts.chart(id, {
            chart: { type: 'line' },
            title: { text: title },
            subtitle: { text: '' },
            xAxis: {
                categories: c
            },
            yAxis: {
                title: {
                    text: yt
                }
            },
            credits: { enabled: false },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series: d
        });
    };
    FrmDashboardService.prototype.generateBarChart = function (id, d, c, xt, yt, minNumber, txttitle, param, pointerflag, paramJsonArr, paramJsonArrflag) {
        var _this = this;
        Highcharts.chart(id, {
            chart: { type: 'column' },
            title: { text: txttitle },
            subtitle: { text: '' },
            xAxis: {
                categories: c,
                crosshair: true,
                title: { text: xt }
            },
            yAxis: {
                min: minNumber,
                title: { text: yt }
            },
            credits: { enabled: false },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.2f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    pointWidth: 28,
                    borderWidth: 0,
                    allowPointSelect: pointerflag,
                    cursor: 'pointer',
                    events: {
                        click: function (event) {
                            if (param == "") {
                                _this.goto(event.point.category, param, paramJsonArr, paramJsonArrflag);
                            }
                        },
                    }
                }
            },
            series: d
        });
    };
    FrmDashboardService.prototype.generate3DPieChart = function (id, d, title, psize) {
        Highcharts.chart(id, {
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            title: {
                text: title
            },
            tooltip: {
                pointFormat: ' <b>{point.percentage:.2f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 35,
                    size: psize,
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}'
                    }
                }
            },
            credits: { enabled: false },
            series: [{
                    type: 'pie',
                    data: d
                }]
        });
    };
    FrmDashboardService.prototype.goClick = function (val, param) {
        this._obj.caption = param;
        this._obj.value = val;
        this._rpDashboardSource.next(this._obj);
    };
    FrmDashboardService.prototype.goto = function (val, param, paramArr, paramflag) {
        if (paramflag) {
            for (var i = 0; i < paramArr.length; i++) {
                if (val == paramArr[i].caption) {
                    val = paramArr[i].value;
                    break;
                }
            }
        }
        this._obj.caption = param;
        this._obj.value = val;
        this._rpDashboardSource.next(this._obj);
    };
    FrmDashboardService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], FrmDashboardService);
    return FrmDashboardService;
}());
exports.FrmDashboardService = FrmDashboardService;
//# sourceMappingURL=frmdashboard-service.js.map