"use strict";
var router_1 = require('@angular/router');
var frmDashBoard_component_1 = require('./frmDashBoard.component');
var DashBoardRoutes = [
    { path: 'dash_board', component: frmDashBoard_component_1.frmDashBoard },
    { path: 'dash_board/:cmd', component: frmDashBoard_component_1.frmDashBoard },
    { path: 'dash_board/:cmd/:id', component: frmDashBoard_component_1.frmDashBoard },
];
exports.DashBoardRouting = router_1.RouterModule.forChild(DashBoardRoutes);
//# sourceMappingURL=dashboard.routing.js.map