"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require("@angular/platform-browser");
var router_1 = require('@angular/router');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var dashboard_service_1 = require('../framework/dashboard-service');
core_1.enableProdMode();
var frmDashBoard = (function () {
    function frmDashBoard(el, l_util, renderer, ics, _router, route, http, ref, dashboard, sanitizer) {
        this.el = el;
        this.l_util = l_util;
        this.renderer = renderer;
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this.dashboard = dashboard;
        this.sanitizer = sanitizer;
        this._util = new rp_client_util_1.ClientUtil();
        this.today = this._util.getTodayDate();
        this.myDatePickerOptions = this._util.getDatePicker();
        this._dates = { "fromDate": this.dateobj, "toDate": this.dateobj };
        //for dashboard
        this._mflag = true;
        this.W_t_W = [];
        this.M_P = [];
        this.A_t_W = [];
        this.W_t_A = [];
        this.dataSeries = [];
        this.minBalance = 0;
        this.allTransactionsForFourDays = [];
        //_period : any;
        this.lovstatusType = [{ "value": 1, "caption": "Daily" },
            { "value": 2, "caption": "Weekly" },
            { "value": 3, "caption": "Monthly" }
        ];
        this._period = this.lovstatusType[0].value;
        this.lovTransType = [{ "value": "All", "caption": "All" },
            { "value": "1", "caption": "Wallet to wallet" },
            { "value": "2", "caption": "Merchant payment" },
            { "value": "4", "caption": "Agent to wallet" },
            { "value": "5", "caption": "Wallet to agent" }
        ];
        this.lovMonths = [{ "value": "Jan", "caption": "January" },
            { "value": "Feb", "caption": "February" },
            { "value": "Mar", "caption": "March" },
            { "value": "Apr", "caption": "April" },
            { "value": "May", "caption": "May" },
            { "value": "Jun", "caption": "June" },
            { "value": "Jul", "caption": "July" },
            { "value": "Aug", "caption": "August" },
            { "value": "Sep", "caption": "September" },
            { "value": "Nov", "caption": "November" },
            { "value": "Oct", "caption": "October" },
            { "value": "Dec", "caption": "December" },];
        this.lovYears = [{ "value": "1", "caption": "2019" },
            { "value": "2", "caption": "2020" }];
        this._ButtonInfo = { "role": "", "formname": "", "button": "" };
        this._sessionObj = this.getSessionObj();
        this._obj = this.getDefaultObj();
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        this.gotoBarChart();
        this.WalletToWallet();
        this.WalletToWalletforWeekly();
    }
    frmDashBoard.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    frmDashBoard.prototype.getDefaultObj = function () {
        return {
            "sessionID": "", "userID": "", "fromDate": "", "toDate": "", "fromAccount": "", "alldate": true, "t1": "", "t2": "", "transtype": "All", "condition": "", "period": "",
            "toAccount": "", "totalCount": 0, "currentPage": 1, "pageSize": 10, "fileType": "PDF", "months": "January", "year": "2019"
        };
    };
    frmDashBoard.prototype.setTodayDateObj = function () {
        this._dates.fromDate = this._util.changestringtodateobject(this.today);
    };
    frmDashBoard.prototype.setDateObjIntoString = function () {
        this._obj.fromDate = this._util.getDatePickerDateymd(this._dates.fromDate);
    };
    frmDashBoard.prototype.ngOnInit = function () {
        //this.gotoBarChart();
    };
    frmDashBoard.prototype.formatNumber = function (amt) {
        if (amt != undefined && amt != "0") {
            return this.thousand_sperator(parseFloat(amt).toFixed(2));
        }
        else {
            return "0.00";
        }
    };
    frmDashBoard.prototype.thousand_sperator = function (num) {
        if (num != "" && num != undefined && num != null) {
            num = num.replace(/,/g, "");
        }
        var parts = num.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts; // return 100,000.00
    };
    frmDashBoard.prototype.restrictSpecialCharacter = function (event, fid, value) {
        if ((event.key >= '0' && event.key <= '9') || event.key == '.' || event.key == ',' || event.key == 'Home' || event.key == 'End' ||
            event.key == 'ArrowLeft' || event.key == 'ArrowRight' || event.key == 'Delete' || event.key == 'Backspace' || event.key == 'Tab' ||
            (event.which == 67 && event.ctrlKey === true) || (event.which == 88 && event.ctrlKey === true) ||
            (event.which == 65 && event.ctrlKey === true) || (event.which == 86 && event.ctrlKey === true)) {
            if (fid == 101) {
                if (/^[\\\"\'\;\:\>\|~`!@#\$%^&*\(\)]$/i.test(event.key)) {
                    event.preventDefault();
                }
            }
            if (value.includes(".")) {
                if (/^[\\\"\'\;\:\>\|~`!@#\$%^&*.\(\)]$/i.test(event.key)) {
                    event.preventDefault();
                }
            }
        }
        else {
            event.preventDefault();
        }
    };
    frmDashBoard.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    frmDashBoard.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    frmDashBoard.prototype.changeTo = function (aIndex) {
        this._period = this.lovstatusType[aIndex].value;
        if (this._period == 1) {
            this.dashboard.generateBarChartWTW('newSummary1', '', '', '', '');
            this.WalletToWallet();
        }
        else if (this._period == 2) {
            this.WalletToWalletforWeekly();
        }
    };
    frmDashBoard.prototype.gotoBarChart = function () {
        var _this = this;
        try {
            var p = { "userID": this.ics._profile.userID, "sessionID": this.ics._profile.sessionID };
            var url = this.ics.cmsurl + 'ServiceReportAdm/getDashBoardData?period=' + this._obj.period + '&transType=' + this._obj.transtype;
            var json = p;
            // jQuery("#loader").modal();
            this._mflag = false;
            this.http.doPost(url, json).subscribe(function (data) {
                var d;
                var day1, day2, day3, day4;
                var fourDays;
                var tempObj;
                var allTrans;
                if (data != undefined && data != null && data != '') {
                    if (data.code == '0000') {
                        var m = [];
                        m[0] = data.wldata;
                        data.wldata = m;
                    }
                    d = data.arr;
                    _this.tempAllTransactions = data.wldata;
                    var tempArray = [];
                    if (!Array.isArray(_this.tempAllTransactions)) {
                        tempArray.push(_this.tempAllTransactions);
                        _this.tempAllTransactions = tempArray;
                    }
                    var W_to_WList = [];
                    var M_PaymentList = [];
                    var A_to_WalletList = [];
                    var W_to_AgentList = [];
                    var t1 = 0, t2 = 0, t3 = 0, t4 = 0;
                    var c1 = 0, c2 = 0, c3 = 0, c4 = 0;
                    var d1 = [], d2 = [], d3 = [], d4 = [];
                    for (var i_1 = 0; i_1 < _this.tempAllTransactions.length; i_1++) {
                        if (_this.tempAllTransactions[i_1].transType == 1) {
                            t1 += parseInt(_this.tempAllTransactions[i_1].amount);
                            c1++;
                        }
                        else if (_this.tempAllTransactions[i_1].transType == 2) {
                            t2 += parseInt(_this.tempAllTransactions[i_1].amount);
                            c2++;
                        }
                        else if (_this.tempAllTransactions[i_1].transType == 4) {
                            t3 += parseInt(_this.tempAllTransactions[i_1].amount);
                            c3++;
                        }
                        else if (_this.tempAllTransactions[i_1].transType == 5) {
                            t4 += parseInt(_this.tempAllTransactions[i_1].amount);
                            c4++;
                        }
                    }
                    W_to_WList = [{ count: c1 }, { total: t1 }];
                    M_PaymentList = [{ count: c2 }, { total: t2 }];
                    A_to_WalletList = [{ count: c3 }, { total: t3 }];
                    W_to_AgentList = [{ count: c4 }, { total: t4 }];
                    allTrans = [{ name: "Wallet to Wallet", transType: W_to_WList },
                        { name: "Merchant Payment", transType: M_PaymentList },
                        { name: "Agent to Wallet", transType: A_to_WalletList },
                        { name: "Wallet to Agent", transType: W_to_AgentList }];
                    var allTransactions = [];
                    var index = 0;
                    var dataSeries = [];
                    var color = ["#337AB7", "#FF6384", "#36A2EB", "#FFCE56", "#CC2EFA", "#FF4000", "#0101DF", "#3B0B17", "#8A0829", "#DF013A",
                        "#337AB7", "#FF6384", "#36A2EB", "#FFCE56", "#CC2EFA", "#FF4000", "#0101DF", "#3B0B17", "#8A0829", "#DF013A"];
                    var i = 1;
                    //let tempObj ;
                    for (var j = 0; j < allTrans.length; j++) {
                        allTransactions.push(allTrans[j].name);
                        tempObj = {
                            name: allTrans[j].name,
                            y: allTrans[j].transType[i].total,
                            count: allTrans[j].transType[0].count,
                            color: color[index]
                        };
                        dataSeries.push(tempObj);
                        index++;
                    }
                    for (var i_2 = 0; i_2 < dataSeries.length; i_2++) {
                        if (_this.minBalance > dataSeries[i_2].y) {
                            _this.minBalance = dataSeries[i_2].y;
                        }
                    }
                    _this.dashboard.generateBarChart('newSummary', [{ "name": " ", "data": dataSeries, color: "White" }], allTransactions, '', _this.minBalance);
                    _this._mflag = true;
                }
            }, function (error) {
                // Hide loading animation
                jQuery("#loader").modal('hide');
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    frmDashBoard.prototype.WalletToWallet = function () {
        var _this = this;
        try {
            var p = { "userID": this.ics._profile.userID, "sessionID": this.ics._profile.sessionID };
            var url = this.ics.cmsurl + 'ServiceReportAdm/walletTowallet?period=' + this._period;
            var json = p;
            // jQuery("#loader").modal();
            this._mflag = false;
            this.http.doPost(url, json).subscribe(function (data) {
                var d;
                var day1, day2, day3, day4;
                var fourDays;
                var tempObj;
                var allTrans;
                if (data != undefined && data != null && data != '') {
                    if (data.code == '0000') {
                        var m = [];
                        m[0] = data.wldata;
                        data.wldata = m;
                    }
                    d = data.arr;
                    _this.tempAllTransactions = data.wldata;
                    var tempArray = [];
                    if (!Array.isArray(_this.tempAllTransactions)) {
                        tempArray.push(_this.tempAllTransactions);
                        _this.tempAllTransactions = tempArray;
                    }
                    var W_to_WList = [];
                    var M_PaymentList = [];
                    var A_to_WalletList = [];
                    var W_to_AgentList = [];
                    var t1 = 0, t2 = 0, t3 = 0, t4 = 0;
                    var c1 = 0, c2 = 0, c3 = 0, c4 = 0;
                    var d1 = [], d2 = [], d3 = [], d4 = [];
                    if (_this._period == 1) {
                        for (var i_3 = 0; i_3 < _this.tempAllTransactions.length; i_3++) {
                            if (_this.tempAllTransactions[i_3].transType == 1) {
                                var j = 0;
                                if (_this.tempAllTransactions[i_3].day == d[j++]) {
                                    t1 += parseInt(_this.tempAllTransactions[i_3].amount);
                                    c1++;
                                }
                                else if (_this.tempAllTransactions[i_3].day == d[j++]) {
                                    t2 += parseInt(_this.tempAllTransactions[i_3].amount);
                                    c2++;
                                }
                                else if (_this.tempAllTransactions[i_3].day == d[j++]) {
                                    t3 += parseInt(_this.tempAllTransactions[i_3].amount);
                                    c3++;
                                }
                                else if (_this.tempAllTransactions[i_3].day == d[j++]) {
                                    t4 += parseInt(_this.tempAllTransactions[i_3].amount);
                                    c4++;
                                }
                            }
                        }
                    }
                    else if (_this._period == 2) {
                    }
                    day1 = [{ count: c1 }, { total: t1 }];
                    day2 = [{ count: c2 }, { total: t2 }];
                    day3 = [{ count: c3 }, { total: t3 }];
                    day4 = [{ count: c4 }, { total: t4 }];
                    fourDays = [{ name: "Day1", day: day1 },
                        { name: "Day2", day: day2 },
                        { name: "Day3", day: day3 },
                        { name: "Day4", day: day4 }];
                    var allTransactionsForFourDays = [];
                    var index = 0;
                    var color = ["#337AB7", "#FF6384", "#36A2EB", "#FFCE56", "#CC2EFA", "#FF4000", "#0101DF", "#3B0B17", "#8A0829", "#DF013A",
                        "#337AB7", "#FF6384", "#36A2EB", "#FFCE56", "#CC2EFA", "#FF4000", "#0101DF", "#3B0B17", "#8A0829", "#DF013A"];
                    var i = 1;
                    //let tempObj ;
                    for (var j = 0; j < fourDays.length; j++) {
                        allTransactionsForFourDays.push(fourDays[j].name);
                        tempObj = {
                            name: fourDays[j].name,
                            y: fourDays[j].day[i].total,
                            count: fourDays[j].day[0].count,
                            color: color[index]
                        };
                        _this.dataSeries.push(tempObj);
                        index++;
                    }
                    var minBalance = 0;
                    for (var i_4 = 0; i_4 < _this.dataSeries.length; i_4++) {
                        if (minBalance > _this.dataSeries[i_4].y) {
                            minBalance = _this.dataSeries[i_4].y;
                        }
                    }
                    _this.dashboard.generateBarChartWTW('newSummary1', [{ "name": " ", "data": _this.dataSeries, color: "White" }], allTransactionsForFourDays, '', minBalance);
                    _this._mflag = true;
                }
            }, function (error) {
                // Hide loading animation
                jQuery("#loader").modal('hide');
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    frmDashBoard.prototype.WalletToWalletforWeekly = function () {
        var _this = this;
        try {
            var p = { "userID": this.ics._profile.userID, "sessionID": this.ics._profile.sessionID };
            var url = this.ics.cmsurl + 'ServiceReportAdm/walletTowallet?period=' + this._period;
            var json = p;
            // jQuery("#loader").modal();
            this._mflag = false;
            this.http.doPost(url, json).subscribe(function (data) {
                var week;
                var week1, week2, week3, week4;
                var fourWeeks;
                var tempObj;
                var allTrans;
                if (data != undefined && data != null && data != '') {
                    if (data.code == '0000') {
                        var m = [];
                        m[0] = data.wldata;
                        data.wldata = m;
                    }
                    week = data.arrWeek;
                    _this.tempAllTransactions = data.wldata;
                    var tempArray = [];
                    if (!Array.isArray(_this.tempAllTransactions)) {
                        tempArray.push(_this.tempAllTransactions);
                        _this.tempAllTransactions = tempArray;
                    }
                    var firstWeek = [];
                    var secondWeek = [];
                    var thirdWeek = [];
                    var fourWeek = [];
                    var t1 = 0, t2 = 0, t3 = 0, t4 = 0;
                    var c1 = 0, c2 = 0, c3 = 0, c4 = 0;
                    var d1 = [], d2 = [], d3 = [], d4 = [];
                    week1 = week[0];
                    week2 = week[1];
                    week3 = week[2];
                    week4 = week[3];
                    var week1start = week1[0];
                    var week1End = week1[week1.length - 1];
                    var week2start = week2[0];
                    var week2End = week2[week2.length - 1];
                    var week3start = week3[0];
                    var week3End = week3[week3.length - 1];
                    var week4start = week4[0];
                    var week4End = week4[week4.length - 1];
                    if (_this._period == 2) {
                        for (var j = 0; j < week1.length; j++) {
                            for (var i_5 = 0; i_5 < _this.tempAllTransactions.length; i_5++) {
                                if (_this.tempAllTransactions[i_5].day == week1[j]) {
                                    t1 += parseInt(_this.tempAllTransactions[i_5].amount);
                                    c1++;
                                    _this.tempAllTransactions.splice(i_5, 1);
                                    i_5--;
                                }
                            }
                        }
                        for (var j = 0; j < week2.length; j++) {
                            for (var i_6 = 0; i_6 < _this.tempAllTransactions.length; i_6++) {
                                if (_this.tempAllTransactions[i_6].day == week2[j]) {
                                    t2 += parseInt(_this.tempAllTransactions[i_6].amount);
                                    c2++;
                                    _this.tempAllTransactions.splice(i_6, 1);
                                    i_6--;
                                }
                            }
                        }
                        for (var j = 0; j < week3.length; j++) {
                            for (var i_7 = 0; i_7 < _this.tempAllTransactions.length; i_7++) {
                                if (_this.tempAllTransactions[i_7].day == week3[j]) {
                                    t3 += parseInt(_this.tempAllTransactions[i_7].amount);
                                    c3++;
                                    _this.tempAllTransactions.splice(i_7, 1);
                                    i_7--;
                                }
                            }
                        }
                        for (var j = 0; j < week4.length; j++) {
                            for (var i_8 = 0; i_8 < _this.tempAllTransactions.length; i_8++) {
                                if (_this.tempAllTransactions[i_8].day == week4[j]) {
                                    t4 += parseInt(_this.tempAllTransactions[i_8].amount);
                                    c4++;
                                    _this.tempAllTransactions.splice(i_8, 1);
                                    i_8--;
                                }
                            }
                        }
                    }
                    week1 = [{ count: c1 }, { total: t1 }];
                    week2 = [{ count: c2 }, { total: t2 }];
                    week3 = [{ count: c3 }, { total: t3 }];
                    week4 = [{ count: c4 }, { total: t4 }];
                    fourWeeks = [{ name: "Week1", day: week1 },
                        { name: "Week2", day: week2 },
                        { name: "Week3", day: week3 },
                        { name: "Week4", day: week4 }];
                    var index = 0;
                    var dataSeries = [];
                    var color = ["#337AB7", "#FF6384", "#36A2EB", "#FFCE56", "#CC2EFA", "#FF4000", "#0101DF", "#3B0B17", "#8A0829", "#DF013A",
                        "#337AB7", "#FF6384", "#36A2EB", "#FFCE56", "#CC2EFA", "#FF4000", "#0101DF", "#3B0B17", "#8A0829", "#DF013A"];
                    var i = 1;
                    //let tempObj ;
                    for (var j = 0; j < fourWeeks.length; j++) {
                        _this.allTransactionsForFourDays.push(fourWeeks[j].name);
                        tempObj = {
                            name: fourWeeks[j].name,
                            y: fourWeeks[j].day[i].total,
                            count: fourWeeks[j].day[0].count,
                            color: color[index]
                        };
                        dataSeries.push(tempObj);
                        index++;
                    }
                    var minBalance = 0;
                    for (var i_9 = 0; i_9 < dataSeries.length; i_9++) {
                        if (minBalance > dataSeries[i_9].y) {
                            minBalance = dataSeries[i_9].y;
                        }
                    }
                    _this.dashboard.generateBarChartWTW('newSummary1', [{ "name": " ", "data": dataSeries, color: "White" }], _this.allTransactionsForFourDays, '', minBalance);
                    _this._mflag = true;
                }
            }, function (error) {
                // Hide loading animation
                jQuery("#loader").modal('hide');
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    frmDashBoard = __decorate([
        core_1.Component({
            selector: 'dashboard',
            template: "\n  <div  class=\"container-fluid\">\n  <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n      <form class=\"form-horizontal\" ngNoForm>\n          <legend> <h4 style=\"margin-top: 0px;\"> Dashboard</h4>  </legend> \n    <div class=\"cardview list-height\" style=\"background-color:#ffffff\">                  \n      <div>&nbsp;</div>\n        <div class=\"col-md-12\">\n          <div class=\"col-md-4\" style=\"padding-left: 2px; padding-right: 2px;\">\n            <!-- <div class=\"dbox\"> -->\n          <div> \n            <div [hidden] = \"!_mflag\" style=\"text-align: center;\">All Transactions by Transaction Type</div>  \n            <div>&nbsp;</div>                   \n          <div class=\"body\" id=\"newSummary\"></div>\n        </div>\n      </div>\n   \n      <div class=\"col-md-4\" style=\"padding-left: 2px; padding-right: 2px;\">\n     <!-- <div class=\"col-md-12\"> -->\n        <div class=\"col-md-4\">\n          <select [(ngModel)]=\"_period\"  (change)=\"changeTo($event.target.options.selectedIndex)\"class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\" required>\n            <option *ngFor=\"let c of lovstatusType\" value=\"{{c.value}}\">{{c.caption}}</option>\n          </select>\n        </div>\n      <!--</div> -->\n      \n        <div> \n        \n          <div [hidden] = \"!_mflag\" style=\"text-align: center;\">Wallet to Wallet</div>  \n          <div>&nbsp;</div>                   \n          <div class=\"body\" id=\"newSummary1\"></div>\n        </div>\n      </div>\n    </div>     \n        </div>\n           \n       </form>\n    </div>\n    <div>&nbsp;</div>\n    <!--<div>&nbsp;</div> -->\n  </div>      \n</div> \n<div [hidden] = \"_mflag\">\n<div class=\"modal\" id=\"loader\"></div>\n</div>\t\t\n     "
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef, rp_client_util_1.ClientUtil, core_1.Renderer, rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences, dashboard_service_1.DashboardService, platform_browser_1.DomSanitizer])
    ], frmDashBoard);
    return frmDashBoard;
}());
exports.frmDashBoard = frmDashBoard;
//# sourceMappingURL=frmDashBoard.component.js.map