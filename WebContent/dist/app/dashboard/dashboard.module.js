"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var common_1 = require('@angular/common');
var rp_input_module_1 = require('../framework/rp-input.module');
var mydatepicker_1 = require('mydatepicker');
var rp_http_service_1 = require('../framework/rp-http.service');
var adminpager_module_1 = require('../util/adminpager.module');
var pager_module_1 = require('../util/pager.module');
var advancedsearch_module_1 = require('../util/advancedsearch.module');
var multiselect_module_1 = require('../util/multiselect.module');
var dashboard_routing_1 = require('./dashboard.routing');
var frmDashBoard_component_1 = require('./frmDashBoard.component');
var frmdashboard_service_1 = require('./frmdashboard-service');
var dashboard_service_1 = require('../framework/dashboard-service');
//import { ChartsModule } from 'ng2-charts';
var dashBoardModule = (function () {
    function dashBoardModule() {
    }
    dashBoardModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                forms_1.FormsModule,
                rp_input_module_1.RpInputModule,
                adminpager_module_1.AdminPagerModule,
                pager_module_1.PagerModule,
                mydatepicker_1.MyDatePickerModule,
                multiselect_module_1.MultiselectModule,
                advancedsearch_module_1.AdvancedSearchModule,
                dashboard_routing_1.DashBoardRouting,
            ],
            exports: [],
            declarations: [
                frmDashBoard_component_1.frmDashBoard,
            ],
            providers: [
                rp_http_service_1.RpHttpService,
                frmdashboard_service_1.FrmDashboardService,
                dashboard_service_1.DashboardService
            ],
            schemas: [core_1.CUSTOM_ELEMENTS_SCHEMA]
        }), 
        __metadata('design:paramtypes', [])
    ], dashBoardModule);
    return dashBoardModule;
}());
exports.dashBoardModule = dashBoardModule;
//# sourceMappingURL=dashboard.module.js.map