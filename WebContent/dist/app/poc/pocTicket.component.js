"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
core_2.enableProdMode();
var pocTicket = (function () {
    function pocTicket(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._util = new rp_client_util_1.ClientUtil();
        this.channelName = "";
        this.image = "";
        this.dateTime = "";
        this.ticketStatus = "";
        this._doLoading = false;
        this.sessionTimeoutMsg = "";
        this._obj = this.getDefaultObj();
        this.ticketRequest = { "sessionID": "", "userID": "", "ticketNo": "", "msgCode": "", "msgDesc": "" };
        this.markers = [
            [0, 0, [], ""]
        ];
        this._sessionObj = this.getSessionObj();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this.checkSession();
            this.imageLink = this.ics._imglink + "/uploads/smallImage/";
            this._obj = this.getDefaultObj();
            this.getStateList();
            this.getTicketStatus();
        }
    }
    pocTicket.prototype.getDefaultObj = function () {
        return {
            "code": "", "desc": "", "state": "", "sessionID": "", "autokey": 0,
            "syskey": 0, "createdDate": "", "modifiedDate": "", "userID": "",
            "date": "", "time": "", "shootDate": "", "shootTime": "",
            "t1": "", "t2": "", "t3": "", "t4": "", "t5": "",
            "t6": "", "t7": "", "t8": "", "t9": "", "t10": "",
            "t11": "", "t12": "", "t13": "", "t14": "", "t15": "",
            "t16": "", "t17": "", "t18": "", "t19": "", "t20": "",
            "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0,
            "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0,
            "srno": 0
        };
    };
    pocTicket.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    pocTicket.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
                _this.ticketRequest.ticketNo = params['id'];
                _this.getOneTicket();
            }
        });
    };
    pocTicket.prototype.getOneTicket = function () {
        var _this = this;
        this._doLoading = true;
        var url = this.ics.ticketurl + 'serviceTicketAdm/getOneTicket';
        this.ticketRequest.sessionID = this.ics._profile.sessionID;
        this.ticketRequest.userID = this.ics._profile.userID;
        var json = this.ticketRequest;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data.code == "0016") {
                _this._doLoading = false;
                _this.showMsg(data.desc, false);
                _this.logout();
            }
            else if (data.code == "0000") {
                _this._obj = data;
                _this.channelName = _this._obj.t15;
                _this.image = _this.imageLink + _this._obj.t13;
                _this.dateTime = _this._obj.shootDate + " " + _this._obj.shootTime;
                _this._doLoading = false;
                var abc = _this._obj.t8;
                var latitudeOne = abc.substring(0, abc.indexOf("/"));
                var longitudeOne = abc.substring(abc.indexOf("/") + 1);
                var info = '';
                if (_this._obj.t5 != undefined && _this._obj.t5 != null && _this._obj.t5 != '') {
                    info = "Ticket No: " + _this._obj.t1 + "\n" +
                        "Name: " + _this._obj.t5 + "\n" +
                        "Date: " + _this._obj.shootDate + "\nTime: " + _this._obj.shootTime + "\n" +
                        "Phone No: " + _this._obj.t9;
                }
                _this.markers[0] = [latitudeOne, longitudeOne, [], info];
                var map = new google.maps.Map(document.getElementById('map-canvas'));
                var infowindow = new google.maps.InfoWindow();
                for (var i = 0; i < _this.markers.length; i++) {
                    var latlng = new google.maps.LatLng(_this.markers[i][0], _this.markers[i][1]);
                    map = new google.maps.Map(document.getElementById('map-canvas'), {
                        zoom: 15,
                        center: latlng
                    });
                }
                for (var i = 0; i < _this.markers.length; i++) {
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(_this.markers[i][0], _this.markers[i][1]),
                        map: map,
                        title: _this.markers[i][3],
                        zIndex: _this.markers[i][1]
                    });
                    infowindow = new google.maps.InfoWindow();
                    google.maps.event.addListener(marker, 'click', (function (marker, i) {
                        return function () {
                            //infowindow.setContent("Data:"+MarkerData[i][3]+"<br />"+ MarkerData[i][2]);
                            infowindow.setContent("Data:" + this.markers[i][3]);
                            infowindow.open(map, marker);
                        };
                    })(marker, i));
                }
            }
            else {
                _this._doLoading = false;
                _this.showMsg(data.desc, false);
            }
        }, function (error) {
            _this._doLoading = false;
            console.log("error=" + JSON.stringify(error));
            alert("Connection Timed Out.");
        }, function () { });
    };
    // comment=we need to remove All coz all means all type of status. it will only include in ticket list form.
    pocTicket.prototype.getTicketStatus = function () {
        var _this = this;
        try {
            var url = this.ics.ticketurl + 'serviceTicketAdm/getTicketStatus';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.msgCode == "0016") {
                        _this.showMsg(data.msgDesc, false);
                        _this.logout();
                    }
                    if (data.msgCode != "0000") {
                        _this.showMsg(data.msgDesc, false);
                    }
                    if (data.msgCode == "0000") {
                        if (!(data.refTicketStatus instanceof Array)) {
                            var m = [];
                            m[0] = data.refTicketStatus;
                            _this.ref._lov3.refTicketStatus = m;
                        }
                        else {
                            _this.ref._lov3.refTicketStatus = data.refTicketStatus;
                        }
                        for (var i = 0; i < _this.ref._lov3.refTicketStatus.length; i++) {
                            if (_this.ref._lov3.refTicketStatus[i].value.includes("All")) {
                                _this.ref._lov3.refTicketStatus.splice(i, 1);
                                i--;
                            }
                        }
                    }
                }
                else {
                    alert("Please try again.");
                }
            }, function (error) {
                console.log("error=" + JSON.stringify(error));
                // if (error._body.type == "error") {
                alert("Connection Timed Out.");
                // }
            }, function () {
            });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    pocTicket.prototype.getStateList = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics.ticketurl + 'serviceCMS/getStateListByUserID?type=&userID=' + this.ics._profile.userID).subscribe(function (response) {
                if (response.refstate != null && response.refstate != undefined) {
                    //this.ref._lov3.refstate = [{ "value": "", "caption": "" }];
                    _this.ref._lov3.refstate = [];
                    if (!(response.refstate instanceof Array)) {
                        var m = [];
                        m[0] = response.data;
                        _this.ref._lov3.refstate = m;
                    }
                    else {
                        _this.ref._lov3.refstate = response.refstate;
                    }
                }
                else {
                    _this.ref._lov3.refstate = [{ "value": "", "caption": "Empty" }];
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Error!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    pocTicket.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    pocTicket.prototype.goReply = function () {
        var _this = this;
        var msgparams = {
            t2: this._obj.t20,
            t3: '',
            t5: this._obj.t3,
            t6: this._obj.t4,
            t9: '',
            t11: '',
            n1: "0",
            t12: "android",
            t15: "",
            isGroup: 0,
            n20: this._obj.t6,
            groupName: this._obj.t5,
            t19: ""
        };
        console.log("request sendmsg =" + JSON.stringify(msgparams));
        this.http.doPost(this.ics.chaturl + "serviceChat/sendMessage1", msgparams).subscribe(function (data) {
            if (data.totalCount > 0) {
                _this.showMsg("Sent successfully.", true);
            }
            else {
                _this.showMsg("Send failed.", false);
            }
            console.log("request sendmsg data =" + JSON.stringify(data));
        }, function (error) {
            console.log("request sendmsg error =" + JSON.stringify(error));
            alert("Connection Timed Out.");
        }, function () { });
    };
    pocTicket.prototype.goUpdate = function () {
        var _this = this;
        this._doLoading = true;
        try {
            var url = this.ics.ticketurl + 'serviceTicketAdm/updateMyTicket';
            var alertMsg = "";
            if (alertMsg == "") {
                var json = this._obj;
                this._obj.sessionID = this.ics._profile.sessionID;
                this._obj.userID = this.ics._profile.userID;
                this.http.doPost(url, json).subscribe(function (data) {
                    console.log("data=" + JSON.stringify(data));
                    if (data != null) {
                        if (data.code == "0016") {
                            _this._doLoading = false;
                            _this.showMsg(data.desc, false);
                            _this.logout();
                        }
                        else if (data.code == "0000") {
                            _this._doLoading = false;
                            _this.showMsg(data.desc, true);
                        }
                        else {
                            _this._doLoading = false;
                            _this.showMsg(data.desc, false);
                        }
                    }
                    else {
                        _this._doLoading = false;
                        alert("Please try again.");
                    }
                }, function (error) {
                    _this._doLoading = false;
                    console.log("error=" + JSON.stringify(error));
                    alert("Connection Timed Out.");
                }, function () {
                    _this._doLoading = false;
                });
            }
            else {
                console.log("this._obj=" + JSON.stringify(this._obj));
                this._doLoading = false;
                alert(alertMsg);
            }
        }
        catch (e) {
            console.log("this._obj=" + JSON.stringify(this._obj) + "/r/n/////" + JSON.stringify(e));
            this._doLoading = false;
            alert("Invalid URL.");
        }
    };
    pocTicket.prototype.goList = function () {
        this._router.navigate(['/pocTicketList']);
    };
    /* goDelete() {
      this._doLoading = true;
  
      try {
        let url: string = this.ics.ticketurl + 'serviceTicketAdm/deleteTicket';
  
        this._obj.sessionID = this.ics._profile.sessionID;
        this._obj.userID = this.ics._profile.userID;
        let json: any = this._obj;
  
        this.http.doPost(url, json).subscribe(
          data => {
            if (data != null) {
              this._obj = data;
  
              if (this._obj.code == "0016") {
                this._doLoading = false;
                this.showMsg(this._obj.desc, false);
                this.logout();
              }
              if (this._obj.code != "0000") {
                this._doLoading = false;
                this.showMsg(this._obj.desc, false);
              }
              if (this._obj.code == "0000") {
                this._doLoading = false;
                this.showMsg(this._obj.desc, true);
  
                this._obj = this.getDefaultObj();
  
                jQuery("#vh_delete_btn").prop("disabled", true);
              }
            } else {
              this._doLoading = false;
              alert("Please try again.");
            }
          },
          error => {
            this._doLoading = false;
            console.log("error=" + JSON.stringify(error));
            alert("Connection Timed Out.");
          },
          () => {
            this._doLoading = false;
          }
        );
      } catch (e) {
        this._doLoading = false;
        alert("Invalid URL.");
      }
    } */
    pocTicket.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsg(data.desc, false);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsg(data.desc, false);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    pocTicket.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    pocTicket.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    pocTicket = __decorate([
        core_1.Component({
            selector: 'frm-ticket',
            template: "\n  <div class=\"container col-md-12 col-sm-12 col-xs-12\" style=\"padding: 0px 5%;\">\n  <form class=\"form-horizontal\" ngNoForm>\n      <legend>Ticket</legend>\n      <div class=\"cardview list-height\">      \n          <div class=\"col-md-12\">\n                  <div class=\"col-md-12\" style=\"margin-bottom: 10px;margin-left:-13px;\">\n                      <button class=\"btn btn-sm btn-primary\" id=\"vh_list_btn\" type=\"button\" (click)=\"goList()\">List</button>\n                      <button class=\"btn btn-sm btn-primary\" id=\"vh_save_btn\" type=\"button\" (click)=\"goUpdate()\">Update</button>\n                  </div>\n             \n\t\t\t\t<div class=\"col-md-6\">\t\n\t\t\t\t\t<sebm-google-map [latitude]=\"lat\" [longitude]=\"lng\"></sebm-google-map>\n\t\t\t\t\t<div id=\"map-canvas\" class=\"col-md-12\" style=\"height:50%;left:-14px;border: 1px solid rgb(188, 186, 184);\"></div>\t\t\t\t\t\t\n\t\t\t\t</div>\n\t\t\t\t\n\t\t\t\t <div class=\"col-md-6\">\n              <div class=\"form-group\">\n                    <img src=\"{{image}}\" alt={{img}} style=\"width:100%;height:45%;\"/>\n              </div>\t\t\t\t\n          </div>\n          </div>     \n         \n\t\t   <div class=\"col-md-12\">\n                  <div class=\"col-md-6\">\n                      <div class=\"form-group\">\n                      \n                        <label class=\"row col-md-4\">Status </label>\n                        <div class=\"row col-md-8\">\n                            <select [(ngModel)]=\"_obj.t10\" class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\" required>\n                              <option *ngFor=\"let item of ref._lov3.refTicketStatus\" value=\"{{item.value}}\">{{item.value}}</option>\n                            </select>\n                        </div>\n                      </div>\n                      \n                      <div class=\"form-group\">\t\t\t\t\t\t\t\n                      \n                          <label class=\"row col-md-4\">User </label>\n                          <div class=\"row col-md-8\">\n                            <div class=\"uni\">\n                              <input disabled readonly type=\"text\" [(ngModel)]=\"_obj.t5\" class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\">\n                            </div>\n                          </div>\n                      </div>\n                      \n                      <div class=\"form-group\">\t\n                          <label class= \"row col-md-4\">Region</label>\n                          <div class= \"row col-md-8\">                  \n                              <select disabled readonly [(ngModel)]=\"_obj.t12\"class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\" required>\n                                  <option *ngFor=\"let c of ref._lov3.refstate\" value=\"{{c.value}}\">{{c.caption}}</option>\n                              </select>\n                          </div>\t\t\t\t\t\t\t\t\t\t\t\n                      </div>\n\t\t\t\t\t\t<div class=\"form-group\">\t\t\t\t\t\n                          <label class=\"row col-md-4\">Location </label>\n                          <div class=\"row col-md-8\">\n                              <input disabled readonly type=\"text\" [(ngModel)]=\"_obj.t8\" class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\">\n                          </div>\n\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\n\t\t\t\t\t\t<div class=\"form-group\">\t\n                          <label class=\"row col-md-12\">Description</label>\n                          <div class=\"row col-md-12\">\n                              <textarea disabled readonly id=\"textarea-custom\" class=\"form-control input-sm\" rows=\"3\" style=\"width: 305px;\" [(ngModel)]=\"_obj.t7\" [ngModelOptions]=\"{standalone: true}\"></textarea>\n                          </div>\n\t\t\t\t\t\t</div>\n                  \n                  </div>\n                  \n                  <div class=\"col-md-6\">\n                      <div class=\"form-group\">\n                          \n                        <label class=\"row col-md-4\">Ticket No.</label>\n                          <div class=\"row col-md-8\">\n                              <input disabled readonly type=\"text\" [(ngModel)]=\"_obj.t1\" class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\">\n                          </div>\t\t\t\t\t\t\t\n                      </div>\n                      <div class=\"form-group\">\t\n                          <label class=\"row col-md-4\">Phone No. </label>\n                          <div class=\"row col-md-8\">\n                            <div class=\"uni\">\n                              <input disabled readonly type=\"text\" [(ngModel)]=\"_obj.t9\" class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\">\n                            </div>\n                          </div>\t\t\t\t\t\t\n                      </div>\n                      <div class=\"form-group\">\n                  \n                          <label class=\"row col-md-4\">Channel </label>\n                          <div class=\"row col-md-8\">\n                            <div class=\"uni\">\n                              <input disabled readonly type=\"text\" [(ngModel)]=\"_obj.t15\" class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\">\n                            </div>\n                          </div>\n                      </div>\n                      <div class=\"form-group\">\t\n                          <label class=\"row col-md-4\">Date Time </label>\n                          <div class=\"row col-md-8\">\n                              <input disabled readonly type=\"text\" [(ngModel)]=\"dateTime\" class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\">\n                          </div>\n                      </div>\n\t\t\t\t\t  \n\t\t\t\t\t  <div class=\"form-group\">\t\n                          <label class=\"row col-md-12\">Message</label>\n                          <div class=\"row col-md-12\">\n                              <textarea id=\"textarea-custom\" class=\"form-control input-sm\" rows=\"3\" style=\"width: 305px;\" [(ngModel)]=\"_obj.t20\" [ngModelOptions]=\"{standalone: true}\"></textarea>\n                          </div>\n\t\t\t\t\t\t  <div class=\"uni row col-md-12\">\n                              <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goReply()\" style=\"margin-top: 5px;\">Reply</button>\n                          </div>\n                      </div>\n                  </div>\t\t\t\t\n\t\t\t\t</div>\n      </div>\n  </form>\n</div>\n\n<div [hidden]=\"!_doLoading\">\n  <div  id=\"loader\" class=\"modal\" ></div>\n</div>\n"
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], pocTicket);
    return pocTicket;
}());
exports.pocTicket = pocTicket;
//# sourceMappingURL=pocTicket.component.js.map