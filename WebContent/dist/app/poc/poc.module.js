"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var common_1 = require('@angular/common');
var rp_input_module_1 = require('../framework/rp-input.module');
var poc_routing_1 = require('./poc.routing');
var rp_http_service_1 = require('../framework/rp-http.service');
var adminpager_module_1 = require('../util/adminpager.module');
var pager_module_1 = require('../util/pager.module');
var advancedsearch_module_1 = require('../util/advancedsearch.module');
var mydatepicker_1 = require('mydatepicker');
var pocTicketList_component_1 = require('./pocTicketList.component');
var pocTicket_component_1 = require('./pocTicket.component');
var pocTicketDashboard_component_1 = require('./pocTicketDashboard.component');
var pocModule = (function () {
    function pocModule() {
    }
    pocModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                forms_1.FormsModule,
                rp_input_module_1.RpInputModule,
                poc_routing_1.pocRouting,
                adminpager_module_1.AdminPagerModule,
                pager_module_1.PagerModule,
                advancedsearch_module_1.AdvancedSearchModule,
                mydatepicker_1.MyDatePickerModule
            ],
            exports: [],
            declarations: [
                pocTicketList_component_1.pocTicketList,
                pocTicket_component_1.pocTicket,
                pocTicketDashboard_component_1.PocDashboardComponent
            ],
            providers: [
                rp_http_service_1.RpHttpService
            ],
            schemas: [core_1.CUSTOM_ELEMENTS_SCHEMA]
        }), 
        __metadata('design:paramtypes', [])
    ], pocModule);
    return pocModule;
}());
exports.pocModule = pocModule;
//# sourceMappingURL=poc.module.js.map