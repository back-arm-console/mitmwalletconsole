"use strict";
var router_1 = require('@angular/router');
var pocTicketList_component_1 = require('./pocTicketList.component');
var pocTicket_component_1 = require('./pocTicket.component');
var pocTicketDashboard_component_1 = require('./pocTicketDashboard.component');
var pocRoutes = [
    { path: 'pocTicketdata', component: pocTicket_component_1.pocTicket },
    { path: 'pocTicketdata/:cmd', component: pocTicket_component_1.pocTicket },
    { path: 'pocTicketdata/:cmd/:id', component: pocTicket_component_1.pocTicket },
    { path: 'pocDashboard', component: pocTicketDashboard_component_1.PocDashboardComponent },
    { path: 'pocDashboard/:cmd', component: pocTicketDashboard_component_1.PocDashboardComponent },
    { path: 'pocDashboard/:cmd/:id', component: pocTicketDashboard_component_1.PocDashboardComponent },
    { path: 'pocTicketList', component: pocTicketList_component_1.pocTicketList },
];
exports.pocRouting = router_1.RouterModule.forChild(pocRoutes);
//# sourceMappingURL=poc.routing.js.map