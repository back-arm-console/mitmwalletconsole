"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var Rx_1 = require('rxjs/Rx');
core_2.enableProdMode();
var FrmBranchSetupComponent = (function () {
    function FrmBranchSetupComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this.message = "";
        this._returnResult = { "keyResult": 0, "longResult": "", "state": "", "stringResult": "", "userId": "", "msgCode": "", "msgDesc": "" };
        this._output1 = "";
        this._sessionMsg = "";
        this._obj = this.getDefaultObj();
        this._customer = { "customerID": "", "name": "", "nrc": "", "accountNo": "", "balance": "", "message": "", "glDesp": "", "chkAccount": "false", "chkGL": "false", "other": "false" };
        this._SearchData = { "searchcaption": "", "searchvalue": "1", "search": "0" };
        this._chkloginId = 'false';
        this._key = "";
        this.sessionAlertMsg = "";
        this._util = new rp_client_util_1.ClientUtil();
        this._mflag = false;
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = false;
            this.checkSession();
            this._returnResult = { "keyResult": 0, "longResult": "", "state": "", "stringResult": "", "userId": "", "msgCode": "", "msgDesc": "" };
            jQuery("#mydelete").prop("disabled", true);
            this._obj = this.getDefaultObj();
            this._mflag = true;
        }
    }
    FrmBranchSetupComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this._key = id;
                _this.goReadBySyskey(id);
            }
        });
    };
    FrmBranchSetupComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    FrmBranchSetupComponent.prototype.getDefaultObj = function () {
        return { "sessionID": "", "msgCode": "", "msgDesc": "", "syskey": 0, "branchCode": "", "branchName": "", "address": "", "phone": "", "fax": "", "username": "", "userid": "" };
    };
    FrmBranchSetupComponent.prototype.goto = function (p) {
        this._router.navigate(['Branch Setup', , { cmd: "READ", p1: p }]);
    };
    FrmBranchSetupComponent.prototype.goReadBySyskey = function (p) {
        var _this = this;
        try {
            this._mflag = false;
            this.http.doGet(this.ics._apiurl + 'service001/getBranchSetupDataByID?id=' + this._key + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (data) {
                _this._output1 = JSON.stringify(data);
                _this.confirmpwd = data.t2;
                _this._obj = data;
                if (_this._obj.msgCode == '0016') {
                    _this._returnResult.msgDesc = "";
                    _this._sessionMsg = _this._obj.msgDesc;
                    _this.showMessage();
                }
                jQuery("#mydelete").prop("disabled", false);
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Error!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmBranchSetupComponent.prototype.goNew = function () {
        this._mflag = false;
        this._obj = this.getDefaultObj();
        this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "" };
        jQuery("#mydelete").prop("disabled", true);
        this._mflag = true;
    };
    FrmBranchSetupComponent.prototype.goList = function () {
        this._router.navigate(['/BranchUserList']);
    };
    FrmBranchSetupComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this._router.navigate(['/login']);
            jQuery("#sessionalert").modal('hide');
        });
    };
    /* checkSession() {
      try {
        let url: string = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
        this.http.doGet(url).subscribe(
          data => {
            if (data.msgCode == '0016') {
              this.sessionAlertMsg = data.msgDesc;
              this.showMessage();
            }
          },
          error => {
            if (error._body.type == 'error') {
              alert("Connection Timed Out!");
            }
            else {
  
            }
          }, () => { });
      } catch (e) {
        alert("Invalid URL");
      }
  
    } */
    FrmBranchSetupComponent.prototype.showMessageAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmBranchSetupComponent.prototype.validateWhiteSpace = function (s) {
        if (/\s/.test(s)) {
            return (false);
        }
        return (true);
    };
    FrmBranchSetupComponent.prototype.goSave = function () {
        var _this = this;
        this._mflag = false;
        this._obj.userid = this.ics._profile.userID;
        this._obj.username = this.ics._profile.userName;
        this._returnResult = { "keyResult": 0, "longResult": "", "state": "", "stringResult": "", "userId": "", "msgCode": "", "msgDesc": "" };
        if (this._obj.fax.length > 0) {
            if (!/^([0-9]{6,20})$/.test(this._obj.fax)) {
                this._returnResult.state = 'false';
                this._returnResult.msgDesc = "Invalid Fax No.!";
                this._mflag = true;
            }
        }
        if (this._obj.phone.length > 0) {
            if (!/^([0-9]{7,20})$/.test(this._obj.phone)) {
                this._returnResult.state = 'false';
                this._returnResult.msgDesc = "Phone No. is invalid.";
                this._mflag = true;
            }
        }
        if (this._obj.branchCode.length > 0) {
            if (!/^([0-9]{3,5})$/.test(this._obj.branchCode)) {
                this._returnResult.state = 'false';
                this._returnResult.msgDesc = "Branch Code should be number!";
                this._mflag = true;
            }
            if (!this.validateWhiteSpace(this._obj.branchCode)) {
                this._returnResult.state = 'false';
                this._returnResult.msgDesc = "Branch Code cannot contains space";
                this._mflag = true;
            }
        }
        if (this._returnResult.msgDesc == '') {
            var url = this.ics._apiurl + 'service001/saveBranchSetup';
            this._obj.sessionID = this.ics._profile.sessionID;
            this._obj.userid = this.ics._profile.userID;
            var json = this._obj;
            this.http.doPost(url, json).subscribe(function (data) {
                _this._output1 = JSON.stringify(data);
                _this._returnResult = data;
                if (_this._returnResult.state != 'false') {
                    _this._obj.syskey = _this._returnResult.keyResult;
                    _this.message = "";
                    _this._key = _this._returnResult.keyResult + "";
                    jQuery("#mydelete").prop("disabled", false);
                    _this.showMessageAlert(_this._returnResult.msgDesc);
                }
                else {
                    if (_this._returnResult.msgCode == '0016') {
                        _this._sessionMsg = _this._returnResult.msgDesc;
                        _this._returnResult.msgDesc = "";
                        _this.showMessage();
                    }
                    else {
                        _this.showMessageAlert(_this._returnResult.msgDesc);
                    }
                }
                _this._mflag = true;
            }, function (error) { return alert(error); }, function () { });
        }
        else {
            this.showMessageAlert(this._returnResult.msgDesc);
        }
    };
    FrmBranchSetupComponent.prototype.goDelete = function () {
        var _this = this;
        this._mflag = false;
        this._obj.userid = this.ics._profile.userID;
        this._obj.username = this.ics._profile.userName;
        this._obj.sessionID = this.ics._profile.sessionID;
        this._obj.userid = this.ics._profile.userID;
        var url = this.ics._apiurl + 'service001/deleteBranchSetup?branchCode=' + this._obj.branchCode;
        var json = this._obj;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "" };
        this.http.doPost(url, json).subscribe(function (data) {
            _this._output1 = JSON.stringify(data);
            _this._obj.userid = data.userid;
            _this._returnResult = data;
            if (_this._returnResult.state) {
                _this._obj = { "sessionID": "", "msgCode": "", "msgDesc": "", "syskey": 0, "branchCode": "", "branchName": "", "address": "", "phone": "", "fax": "", "username": "", "userid": "" };
                jQuery("#mydelete").prop("disabled", true);
                _this.showMessageAlert(_this._returnResult.msgDesc);
            }
            else {
                if (_this._returnResult.msgCode == '0016') {
                    _this._sessionMsg = _this._returnResult.msgDesc;
                    _this._returnResult.msgDesc = "";
                    _this.showMessage();
                }
                else {
                    _this.showMessageAlert(_this._returnResult.msgDesc);
                }
            }
            _this._mflag = true;
        }, function (error) { return alert(error); }, function () { });
        this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "" };
    };
    FrmBranchSetupComponent.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    FrmBranchSetupComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsgAlert(data.desc);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsgAlert(data.desc);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmBranchSetupComponent.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmBranchSetupComponent.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    FrmBranchSetupComponent = __decorate([
        core_1.Component({
            selector: 'branchsetup',
            template: "\n  <div class=\"container\">\n     <div class=\"row clearfix\">\n        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n          <form class= \"form-horizontal\" (ngSubmit)=\"goSave()\"> \n         <!-- Form Name -->\n          <legend>Branch Setup</legend>\n\n        <div class=\"row  col-md-12\">  \n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"goList()\" >List</button> \n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"goNew()\" >New</button>      \n            <button class=\"btn btn-primary\" type=\"submit\">Save</button>          \n            <button class=\"btn btn-primary\" disabled id=\"mydelete\"  type=\"button\" (click)=\"goDelete();\" >Delete</button> \n        </div>\n        <div class=\"row col-md-12\">&nbsp;</div>\n        <div class=\"col-md-12\" id=\"custom-form-alignment-margin\">\n        <div class=\"col-md-6\">\n              <div class=\"form-group\">\n                 <label class=\"col-md-4\">Branch Code <font size=\"4\" color=\"#FF0000\" >*</font></label>\n                      <div class=\"col-md-8\">\n                          <input  class=\"form-control\" type = \"text\" [(ngModel)]=\"_obj.branchCode\" required=\"true\" *ngIf=\"_obj.syskey == 0 \" maxlength=\"5\" [ngModelOptions]=\"{standalone: true}\"> \n                          <input  class=\"form-control\" type = \"text\" [(ngModel)]=\"_obj.branchCode\" required=\"true\" *ngIf=\"_obj.syskey != 0 \" readonly maxlength=\"5\" [ngModelOptions]=\"{standalone: true}\">     \n                      </div>\n              </div>      \n         \n              <div class=\"form-group\">\n                  <label class=\"col-md-4\" >Branch Name <font size=\"4\" color=\"#FF0000\" >*</font></label>\n                      <div class=\"col-md-8\">\n                          <input  class=\"form-control\" type = \"text\" [(ngModel)]=\"_obj.branchName\" [ngModelOptions]=\"{standalone: true}\" required=\"true\" maxlength=\"50\">                              \n                      </div>  \n              </div>\n              \n              <div class=\"form-group\">\n                 <rp-input [(rpModel)]=\"_obj.phone\" rpClass=\"col-md-8\" rpLabelClass=\"col-md-4\" rpType=\"text\" rpLabel=\"Phone No.\"></rp-input>\n              </div> \n      \n              <div class=\"form-group\">\n                <rp-input [(rpModel)]=\"_obj.fax\"  rpClass=\"col-md-8\" rpLabelClass=\"col-md-4\" rpType=\"text\" rpLabel=\"Fax\"></rp-input>\n              </div> \n              \n              <div class=\"form-group\">\n                  <label class=\"col-md-4\" > Address <font size=\"4\" color=\"#FF0000\" >*</font></label>\n                    <div class=\"col-md-8\">\n                        <textarea  class=\"form-control\" rows=\"3\"  [(ngModel)]=\"_obj.address\" [ngModelOptions]=\"{standalone: true}\" required></textarea>\n                    </div> \n              </div> \n        </div>\n      </div>\n        </form>\n    </div>\n  </div>\n</div>\n\n  <div id=\"sessionalert\" class=\"modal fade\">\n      <div class=\"modal-dialog\">\n           <div class=\"modal-content\">\n              <div class=\"modal-body\">\n                  <p>{{_sessionMsg}}</p>\n              </div>\n              <div class=\"modal-footer\">\n              </div>\n         </div>\n      </div>\n  </div>\n\n  <div [hidden] = \"_mflag\">\n      <div class=\"modal\" id=\"loader\"></div>\n  </div>\n\n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmBranchSetupComponent);
    return FrmBranchSetupComponent;
}());
exports.FrmBranchSetupComponent = FrmBranchSetupComponent;
//# sourceMappingURL=frmbranchsetup.component.js.map