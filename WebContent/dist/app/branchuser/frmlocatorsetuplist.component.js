"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var Observable_1 = require('rxjs/Observable');
var core_2 = require('@angular/core');
// RP Framework
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
core_2.enableProdMode();
var FrmLocatorSetupListComponent = (function () {
    function FrmLocatorSetupListComponent(ics, _router, http) {
        this.ics = ics;
        this._router = _router;
        this.http = http;
        this._sessionMsg = "";
        this._mflag = false;
        this._flagas = true;
        this._objs = {
            "data": [{ "createdDate": "", "modifiedDate": "", "latitude": "", "longitude": "", "address": "", "phone1": "", "phone2": "", "branchCode": "", "name": "", "locationType": "", "t1": "" }],
            "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0
        };
        this._pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 0 };
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._recordhide = false;
            this._msghide = true;
            //jQuery("#loader").modal('hide');
            this._mflag = false;
            this.search();
        }
    }
    FrmLocatorSetupListComponent.prototype.changedPager = function (event) {
        if (this._objs.totalCount != 0) {
            this._pgobj = event;
            var current = this._objs.currentPage;
            var size = this._objs.pageSize;
            this._objs.currentPage = this._pgobj.current;
            this._objs.pageSize = this._pgobj.size;
            if (this._pgobj.current != current || this._pgobj.size != size)
                this.search();
        }
    };
    FrmLocatorSetupListComponent.prototype.search = function () {
        var _this = this;
        try {
            this._mflag = false;
            this.http.doGet(this.ics._apiurl + 'service001/getATMLocatorList?searchVal=' + this._objs.searchText + '&pagesize=' + this._objs.pageSize + '&currentpage=' + this._objs.currentPage + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (response) {
                _this._objs = response;
                if (response.msgCode == '0016') {
                    _this._sessionMsg = response.msgDesc;
                    _this.showMessage();
                }
                if (response.data != null && response.data != undefined) {
                    if (!(response.data instanceof Array)) {
                        var m = [];
                        m[0] = response.data;
                        _this._objs.data = m;
                        _this._objs.totalCount = response.totalCount;
                    }
                    else {
                        _this._objs = response;
                        if (response.totalCount == 0) {
                            _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Data not found!" });
                        }
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    //   search() {
    //     try {
    //       this.http.doGet(this.ics._apiurl + 'service001/getATMLocatorList?searchVal=' + this._objs.searchText + '&pagesize=' + this._objs.pageSize + '&currentpage=' + this._objs.currentPage + '&sessionID=' + this.ics._profile.sessionID).subscribe(
    //         response => {        
    //           if (response.msgCode == "0016") {
    //             this._sessionMsg = response.msgDesc;
    //             this.showMessage();
    //           } else {
    //             if (response.totalCount == 0) {
    //               this._recordhide = true;
    //               this._msghide = false;
    //             }
    //             if (response.data != null) {
    //               if (!(response.data instanceof Array)) {
    //                 let m = [];
    //                 m[0] = response.data;
    //                 this._objs.data = m;
    //                 this._objs.totalCount = response.totalCount;                        
    //               }
    //               else {
    //                 this._objs = response;
    //               }
    //             }
    //           }
    //         },
    //         error => {
    //           if (error._body.type == 'error') {
    //             alert("Connection Timed Out!");
    //           }          
    //         }, () => { });
    //     } catch (e) {
    //       alert("Invalid URL");
    //     }
    //   }
    FrmLocatorSetupListComponent.prototype.searchKeyup = function (e) {
        if (e.which == 13) {
            this._objs.currentPage = 1;
            this._msghide = true;
            this._recordhide = false;
            this.search();
        }
    };
    FrmLocatorSetupListComponent.prototype.Searching = function () {
        this._objs.currentPage = 1;
        this._msghide = true;
        this._recordhide = false;
        this.search();
    };
    FrmLocatorSetupListComponent.prototype.goto = function (p) {
        this._router.navigate(['/Locator Setup', 'read', p]);
    };
    FrmLocatorSetupListComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Observable_1.Observable.timer(3000).subscribe(function (x) {
            _this._router.navigate(['Login', , { p1: '*' }]);
            jQuery("#sessionalert").modal('hide');
        });
    };
    FrmLocatorSetupListComponent.prototype.goNew = function () {
        this._router.navigate(['Locator Setup', , { cmd: "NEW" }]);
    };
    FrmLocatorSetupListComponent = __decorate([
        core_1.Component({
            selector: 'locatorsetup-list',
            template: " \n<div class=\"container col-md-12\">\n    <form class=\"form-inline\"> \n        <!-- Form Name -->\n         <legend>Locator List</legend>\n         \n         <div *ngIf=\"_flagas\" class=\"input-group\">\n                <span class=\"input-group-btn input-md\"  style=\"width:20px;\">\n                    <button class=\"btn btn-primary\" type=\"button\" (click)=\"goNew();\">New</button>\n                </span> \n                <input id=\"textinput\" name=\"textinput\" type=\"text\"  placeholder=\"Search\" [(ngModel)]=\"_objs.searchText\" (keyup)=\"searchKeyup($event)\" maxlength=\"50\" class=\"form-control input-md\">\n                <span class=\"input-group-btn input-md\">\n                    <button class=\"btn btn-primary input-md\" type=\"button\" (click)=\"Searching()\" >\n                        <span class=\"glyphicon glyphicon-search\"></span>Search\n                    </button>\n                </span>        \n         </div>      \n    </form>      \n        <div style = \"margin-top : 10px\">\n            <adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"_objs.totalCount\" (rpChanged)=\"changedPager($event)\" ></adminpager> \n        </div>\n        \n        <div [hidden]=\"_recordhide\">\n         <table class=\"table table-striped table-condensed table-hover tblborder\" style=\"font-size:14px;\">\n            <thead>\n                <tr>\n                    <th>ATM ID</th>\n                    <th>Name</th>\n                    <th>Latitude</th>\n                    <th>Longitude</th>\n                    <th>LocationType</th>\n                    <th>BranchCode</th>\n                    <th>Address</th>\n                    <th>Phone1</th>\n                    <th>Phone2</th>    \n                </tr>\n            </thead>\n            <tbody>\n                <tr *ngFor=\"let obj of _objs.data\">\n                    <td><a (click)=\"goto(obj.t1)\">{{obj.t1}}</a></td>\n                    <td>{{obj.name}}</td>\n                    <td>{{obj.latitude}}</td>\n                    <td>{{obj.longitude}}</td>\n                    <td *ngIf=\"obj.locationType == 'A' \" >ATM Locator</td>\n                    <td *ngIf=\"obj.locationType == 'B' \" >Branch Locator</td>\n                    <td>{{obj.branchCode}}</td>\n                    <td>{{obj.address}}</td>\n                    <td>{{obj.phone1}}</td>\n                    <td>{{obj.phone2}}</td>\n                </tr>  \n            </tbody>\n        </table>\n        </div>\n </div> \n  <!--  <div id=\"sessionalert\" class=\"modal fade\">\n        <div class=\"modal-dialog\">\n            <div class=\"modal-content\">\n                <div class=\"modal-body\">\n                    <p>{{_sessionMsg}}</p>\n                </div>\n                <div class=\"modal-footer\">\n                </div>\n            </div>\n        </div>\n    </div>-->\n <div [hidden] = \"_mflag\">\n    <div class=\"modal\" id=\"loader\"></div>\n   </div>\n    ",
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_http_service_1.RpHttpService])
    ], FrmLocatorSetupListComponent);
    return FrmLocatorSetupListComponent;
}());
exports.FrmLocatorSetupListComponent = FrmLocatorSetupListComponent;
//# sourceMappingURL=frmlocatorsetuplist.component.js.map