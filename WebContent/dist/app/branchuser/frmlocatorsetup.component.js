"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var Rx_1 = require('rxjs/Rx');
core_2.enableProdMode();
var FrmLocatorSetupComponent = (function () {
    function FrmLocatorSetupComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "msgCode": "" };
        this._output1 = "";
        this._sessionMsg = "";
        this.message = "";
        this._key = "";
        this._obj = this.getDefaultObj();
        this.confirmpwd = "";
        this._mflag = false;
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = false;
            this.checkSession();
            this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "msgCode": "" };
            jQuery("#mydelete").prop("disabled", true);
            this._obj = this.getDefaultObj();
            this.getBranchCode();
            this.getLocationCbo();
        }
    }
    FrmLocatorSetupComponent.prototype.getDefaultObj = function () {
        return { "sessionID": "", "msgCode": "", "msgDesc": "", "createdDate": "", "modifiedDate": "", "latitude": "", "longitude": "", "address": "", "phone1": "", "phone2": "", "branchCode": "", "name": "", "locationType": "", "t1": "", "userid": "", "username": "" };
    };
    FrmLocatorSetupComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this._key = id;
                _this.goReadBySyskey(id);
            }
        });
    };
    FrmLocatorSetupComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    FrmLocatorSetupComponent.prototype.goSave = function () {
        var _this = this;
        this._mflag = false;
        this._obj.userid = this.ics._profile.userID;
        this._obj.username = this.ics._profile.userName;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "msgCode": "" };
        if (this._obj.phone1.length > 0) {
            if (!/^([0-9]{7,20})$/.test(this._obj.phone1)) {
                this._returnResult.state = 'false';
                this._returnResult.msgDesc = "Phone No1 is invalid.";
                this._mflag = true;
            }
        }
        if (this._obj.phone2.length > 0) {
            if (!/^([0-9]{7,20})$/.test(this._obj.phone2)) {
                this._returnResult.state = 'false';
                this._returnResult.msgDesc = "Phone No2 is invalid.";
                this._mflag = true;
            }
        }
        if (this._obj.phone2.length > 0) {
            if (!/^([0-9]{1,}\.{0,1}[0-9]{1,})$/.test(this._obj.latitude)) {
                this._returnResult.state = 'false';
                this._returnResult.msgDesc = "Invalid Latitude";
                this._mflag = true;
            }
        }
        if (this._obj.longitude.length > 0) {
            if (!/^([0-9]{1,}\.{0,1}[0-9]{1,})$/.test(this._obj.longitude)) {
                this._returnResult.state = 'false';
                this._returnResult.msgDesc = "Invalid Longitude";
                this._mflag = true;
            }
        }
        if (this._returnResult.msgDesc == '') {
            var url = this.ics._apiurl + 'service001/saveATMLocator';
            this._obj.sessionID = this.ics._profile.sessionID;
            this._obj.userid = this.ics._profile.userID;
            var json = this._obj;
            this.http.doPost(url, json).subscribe(function (data) {
                _this._returnResult = data;
                if (_this._returnResult.state) {
                    _this._obj.t1 = data.userId;
                    jQuery("#mydelete").prop("disabled", false);
                    _this.showMessageAlert(_this._returnResult.msgDesc);
                }
                else {
                    if (_this._returnResult.msgCode == '0016') {
                        _this._sessionMsg = _this._returnResult.msgDesc;
                        _this._returnResult.msgDesc = "";
                        _this.showMessage();
                    }
                    else {
                        _this.showMessageAlert(_this._returnResult.msgDesc);
                    }
                }
                _this._mflag = true;
            }, function (error) { return alert(error); }, function () { });
        }
        else {
            this.showMessageAlert(this._returnResult.msgDesc);
        }
    };
    FrmLocatorSetupComponent.prototype.showMessageAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmLocatorSetupComponent.prototype.getBranchCode = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics._apiurl + 'service001/getAllBranchCode').subscribe(function (data) {
                _this.ref._lov3.ref024 = data.ref024;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmLocatorSetupComponent.prototype.getLocationCbo = function () {
        var _this = this;
        try {
            this._mflag = false;
            this.http.doGet(this.ics._apiurl + 'service001/getLocationCbo').subscribe(function (data) {
                _this.ref._lov3.refLocType = data.refLocType;
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmLocatorSetupComponent.prototype.goReadBySyskey = function (p) {
        var _this = this;
        try {
            this._mflag = false;
            var url = this.ics._apiurl + 'service001/getATMLocatorByID?id=' + this._key + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
            this.http.doGet(url).subscribe(function (data) {
                _this._obj = data;
                if (_this._obj.msgCode == '0016') {
                    _this._returnResult.msgDesc = "";
                    _this._sessionMsg = _this._obj.msgDesc;
                    _this.showMessage();
                }
                jQuery("#mydelete").prop("disabled", false);
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Error!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmLocatorSetupComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this._router.navigate(['/login']);
            jQuery("#sessionalert").modal('hide');
        });
    };
    FrmLocatorSetupComponent.prototype.goNew = function () {
        this._obj = this.getDefaultObj();
        jQuery("#mydelete").prop("disabled", true);
    };
    FrmLocatorSetupComponent.prototype.goList = function () {
        this._router.navigate(['/LocatorList']);
    };
    /* checkSession() {
      try {
        let url: string = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
        this.http.doGet(url).subscribe(
          data => {
            if (data.msgCode == '0016') {
              this._sessionMsg = data.msgDesc;
              this.showMessage();
            }
          },
          error => {
            if (error._body.type == 'error') {
              alert("Connection Timed Out!");
            }
          }, () => { });
      } catch (e) {
        alert("Invalid URL");
      }
  
    } */
    FrmLocatorSetupComponent.prototype.goDelete = function () {
        var _this = this;
        this._mflag = false;
        this._obj.userid = this.ics._profile.userID;
        this._obj.username = this.ics._profile.userName;
        this._obj.sessionID = this.ics._profile.sessionID;
        var url = this.ics._apiurl + 'service001/deleteATMLocator';
        var json = this._obj;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "msgCode": "" };
        this.http.doPost(url, json).subscribe(function (data) {
            _this._output1 = JSON.stringify(data);
            _this._obj.userid = data.userid;
            _this._returnResult = data;
            if (_this._returnResult.state) {
                _this._obj = { "sessionID": "", "msgCode": "", "msgDesc": "", "createdDate": "", "modifiedDate": "", "latitude": "", "longitude": "", "address": "", "phone1": "", "phone2": "", "branchCode": "", "name": "", "locationType": "", "t1": "", "userid": "", "username": "" };
                jQuery("#mydelete").prop("disabled", true);
                _this.showMessageAlert(_this._returnResult.msgDesc);
            }
            else {
                if (_this._returnResult.msgCode == '0016') {
                    _this._sessionMsg = _this._returnResult.msgDesc;
                    _this._returnResult.msgDesc = "";
                    _this.showMessage();
                }
                else {
                    _this.showMessageAlert(_this._returnResult.msgDesc);
                }
            }
            _this._mflag = true;
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
        }, function () { });
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "msgCode": "" };
    };
    FrmLocatorSetupComponent.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    FrmLocatorSetupComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsgAlert(data.desc);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsgAlert(data.desc);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmLocatorSetupComponent.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmLocatorSetupComponent.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    FrmLocatorSetupComponent = __decorate([
        core_1.Component({
            selector: 'locatorsetup',
            template: "\n<div class=\"container\">\n  <div class=\"row clearfix\">\n     <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n       <form class= \"form-horizontal\" (ngSubmit)=\"goSave()\"> \n         <!-- Form Name -->\n          <legend>Locator Setup</legend>\n\n                <div class=\"row  col-md-12\">  \n                    <button class=\"btn btn-primary\" type=\"button\" (click)=\"goList()\" >List</button> \n                    <button class=\"btn btn-primary\" type=\"button\" (click)=\"goNew()\" >New</button>      \n                    <button class=\"btn btn-primary\" type=\"submit\">Save</button>          \n                    <button class=\"btn btn-primary\" disabled id=\"mydelete\"  type=\"button\" (click)=\"goDelete();\" >Delete</button> \n                </div>\n\n                <div class=\"col-md-12\">&nbsp;</div>\n                    \n                <div class = \"col-md-12\"  id=\"custom-form-alignment-margin\">\n                <div class = \"col-md-6\">\n                  <div class=\"form-group\">\t\n                          <label class=\"col-md-4\">ID <font size=\"4\" color=\"#FF0000\" >*</font></label>\n                              <div class=\"col-md-8\">                  \n                                  <input [(ngModel)]=\"_obj.t1\" required type=\"text\"  class=\"form-control input-md\" [ngModelOptions]=\"{standalone: true}\" readonly/>\n                              </div>\n                  </div>\n                  <div class=\"form-group\">\n                    <label class=\"col-md-4\">Name<font size=\"4\" color=\"#FF0000\" >*</font></label>\n                              <div class=\"col-md-8\">                  \n                                  <input [(ngModel)]=\"_obj.name\" required type=\"text\"  class=\"form-control input-md\" [ngModelOptions]=\"{standalone: true}\"/>\n                              </div>\n                  </div>\n                  <div class=\"form-group\">\n                    <label class=\"col-md-4\">Latitude <font size=\"4\" color=\"#FF0000\" >*</font></label>\n                              <div class=\"col-md-8\">                  \n                                  <input [(ngModel)]=\"_obj.latitude\" required type=\"text\"  class=\"form-control input-md\" [ngModelOptions]=\"{standalone: true}\"/>\n                              </div>\n                  </div>\t\t\n                      <div class=\"form-group\">\n                    <label class=\"col-md-4\">Location Type <font size=\"4\" color=\"#FF0000\" >*</font></label>\n                              <div class=\"col-md-8\">                  \n                        <select [(ngModel)]=\"_obj.locationType\"  class=\"form-control\" [ngModelOptions]=\"{standalone: true}\" required>\n                                      <option *ngFor=\"let c of ref._lov3.refLocType\" value=\"{{c.value}}\">{{c.caption}}</option>\n                                  </select>\n                              </div>\n                  </div>\n                  <div class=\"form-group\">\n                    <label class=\"col-md-4\">Address <font size=\"4\" color=\"#FF0000\" >*</font></label>\n                              <div class=\"col-md-8\">                  \n                                  <textarea  class=\"form-control\" rows=\"3\"  [(ngModel)]=\"_obj.address\" [ngModelOptions]=\"{standalone: true}\" required></textarea>\n                              </div>\n                  </div>\n                </div>\n              \n                <div class =\"col-md-6\">\n                  <div class=\"form-group\">\n                    <label class=\"col-md-4\">Phone 1 <font size=\"4\" color=\"#FF0000\" >*</font></label>\n                              <div class=\"col-md-8\">                  \n                                  <input [(ngModel)]=\"_obj.phone1\" required type=\"text\"  class=\"form-control input-md\" [ngModelOptions]=\"{standalone: true}\"/>\n                              </div>\n                  </div>\n                  <div class=\"form-group\">\n                    <label class=\"col-md-4\">Phone 2 <font size=\"4\" color=\"#FF0000\" >*</font></label>\n                              <div class=\"col-md-8\">                  \n                                  <input [(ngModel)]=\"_obj.phone2\" required type=\"text\"  class=\"form-control input-md\" [ngModelOptions]=\"{standalone: true}\"/>\n                              </div>\n                  </div>\n                  <div class=\"form-group\">\n                    <label class=\"col-md-4\">Longitude <font size=\"4\" color=\"#FF0000\" >*</font></label>\n                              <div class=\"col-md-8\">                  \n                        <input [(ngModel)]=\"_obj.longitude\" required type=\"text\"  class=\"form-control input-md\" [ngModelOptions]=\"{standalone: true}\"/>\n                              </div>\n                  </div>\n                  <div class=\"form-group\">\n                    <label class=\"col-md-4\">Branch Code <font size=\"4\" color=\"#FF0000\" >*</font></label>\n                              <div class=\"col-md-8\">                    \n                                  <select [(ngModel)]=\"_obj.branchCode\"  class=\"form-control\" [ngModelOptions]=\"{standalone: true}\" required>\n                                      <option *ngFor=\"let c of ref._lov3.ref024\" value=\"{{c.value}}\">{{c.caption}}</option>\n                                  </select>\n                              </div>\n                  </div>\t\n                </div>\n              </div>           \n      </form>\n    </div>\n  </div>\n</div>\n\n<div [hidden] = \"_mflag\">\n  <div class=\"modal\" id=\"loader\"></div>\n</div> \n"
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmLocatorSetupComponent);
    return FrmLocatorSetupComponent;
}());
exports.FrmLocatorSetupComponent = FrmLocatorSetupComponent;
//# sourceMappingURL=frmlocatorsetup.component.js.map