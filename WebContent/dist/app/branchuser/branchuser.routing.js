"use strict";
var router_1 = require('@angular/router');
var frmbranchsetup_component_1 = require('./frmbranchsetup.component');
var frmbranchsetuplist_component_1 = require('./frmbranchsetuplist.component');
var frmlocatorsetup_component_1 = require('./frmlocatorsetup.component');
var frmlocatorsetuplist_component_1 = require('./frmlocatorsetuplist.component');
var frmbranchusersetup_component_1 = require('./frmbranchusersetup.component');
var frmbranchusersetuplist_component_1 = require('./frmbranchusersetuplist.component');
var branchuserRoutes = [
    { path: 'Branch Setup', component: frmbranchsetup_component_1.FrmBranchSetupComponent },
    { path: 'Branch Setup/:cmd', component: frmbranchsetup_component_1.FrmBranchSetupComponent },
    { path: 'Branch Setup/:cmd/:id', component: frmbranchsetup_component_1.FrmBranchSetupComponent },
    { path: 'BranchUserList', component: frmbranchsetuplist_component_1.FrmBranchSetupListComponent },
    { path: 'Locator Setup', component: frmlocatorsetup_component_1.FrmLocatorSetupComponent },
    { path: 'Locator Setup/:cmd', component: frmlocatorsetup_component_1.FrmLocatorSetupComponent },
    { path: 'Locator Setup/:cmd/:id', component: frmlocatorsetup_component_1.FrmLocatorSetupComponent },
    { path: 'LocatorList', component: frmlocatorsetuplist_component_1.FrmLocatorSetupListComponent },
    { path: 'Branch User Setup', component: frmbranchusersetup_component_1.FrmBranchUserSetupComponent },
    { path: 'Branch User Setup/:cmd', component: frmbranchusersetup_component_1.FrmBranchUserSetupComponent },
    { path: 'Branch User Setup/:cmd/:id', component: frmbranchusersetup_component_1.FrmBranchUserSetupComponent },
    { path: 'BranchUserSetupList', component: frmbranchusersetuplist_component_1.FrmBranchUserSetupListComponent },
];
exports.branchuserRouting = router_1.RouterModule.forChild(branchuserRoutes);
//# sourceMappingURL=branchuser.routing.js.map