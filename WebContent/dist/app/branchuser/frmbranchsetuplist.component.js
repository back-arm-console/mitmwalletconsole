"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var Observable_1 = require('rxjs/Observable');
var core_2 = require('@angular/core');
// RP Framework
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_client_util_1 = require('../util/rp-client.util');
core_2.enableProdMode();
var FrmBranchSetupListComponent = (function () {
    function FrmBranchSetupListComponent(ics, _router, http) {
        this.ics = ics;
        this._router = _router;
        this.http = http;
        this._sessionMsg = "";
        this._searchVal = ""; // simple search
        this._flagas = true; // flag advance search
        this._output1 = "";
        this._col_list = [{ "col_no": "1", "flag": false }, { "col_no": "2", "flag": false }];
        this._sorting = { _sort_type: "asc", _sort_col: "1" };
        this._mflag = true;
        this._util = new rp_client_util_1.ClientUtil();
        this.sessionAlertMsg = "";
        this._brobj = {
            "data": [{ "syskey": 0, "branchCode": "", "branchName": "", "address": "", "phone": "", "fax": "", "userName": "", "userId": "" }],
            "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0
        };
        this._pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 0 };
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._recordhide = false;
            this._msghide = true;
            //jQuery("#loader").modal('hide');
            this._mflag = true;
            this.search();
        }
    }
    // list sorting part
    FrmBranchSetupListComponent.prototype.changeDefault = function () {
        for (var i = 0; i < this._col_list.length; i++) {
            this._col_list[i].flag = false;
        }
    };
    FrmBranchSetupListComponent.prototype.addSort = function (e) {
        for (var i = 0; i < this._col_list.length; i++) {
            if (this._col_list[i].col_no == e) {
                var _tmp_flag = this._col_list[i].flag;
                this.changeDefault();
                this._col_list[i].flag = !_tmp_flag;
                this._sorting._sort_type = !_tmp_flag ? "desc" : "asc";
                this._sorting._sort_col = e;
                break;
            }
        }
    };
    FrmBranchSetupListComponent.prototype.changedPager = function (event) {
        if (this._brobj.totalCount != 0) {
            this._pgobj = event;
            var current = this._brobj.currentPage;
            var size = this._brobj.pageSize;
            this._brobj.currentPage = this._pgobj.current;
            this._brobj.pageSize = this._pgobj.size;
            if (this._pgobj.current != current || this._pgobj.size != size)
                this.search();
        }
    };
    FrmBranchSetupListComponent.prototype.search = function () {
        var _this = this;
        this._mflag = false;
        this.http.doGet(this.ics._apiurl + 'service001/getBranchSetupList?searchVal=' + this._brobj.searchText + '&pagesize=' + this._brobj.pageSize + '&currentpage=' + this._brobj.currentPage + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (response) {
            if (response.msgCode == "0016") {
                _this._sessionMsg = response.msgDesc;
                _this.showMessage();
            }
            _this._brobj = response;
            if (response.data != null) {
                if (!(response.data instanceof Array)) {
                    var m = [];
                    m[0] = response.data;
                    _this._brobj.data = m;
                    _this._brobj.totalCount = response.totalCount;
                }
                else {
                    _this._brobj = response;
                    if (response.totalCount == 0) {
                        _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Data not found!" });
                    }
                }
            }
            _this._mflag = true;
        }, function (error) { return alert(error); }, function () { });
    };
    FrmBranchSetupListComponent.prototype.searchKeyup = function (e) {
        if (e.which == 13) {
            this._brobj.currentPage = 1;
            this._msghide = true;
            this._recordhide = false;
            this.search();
        }
    };
    FrmBranchSetupListComponent.prototype.Searching = function () {
        this._brobj.currentPage = 1;
        this._msghide = true;
        this._recordhide = false;
        this.search();
    };
    FrmBranchSetupListComponent.prototype.goto = function (p) {
        this._router.navigate(['/Branch Setup', 'read', p]);
    };
    FrmBranchSetupListComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Observable_1.Observable.timer(3000).subscribe(function (x) {
            _this._router.navigate(['Login', , { p1: '*' }]);
            jQuery("#sessionalert").modal('hide');
        });
    };
    FrmBranchSetupListComponent.prototype.goNew = function () {
        this._router.navigate(['Branch Setup', , { cmd: "NEW" }]);
    };
    FrmBranchSetupListComponent = __decorate([
        core_1.Component({
            selector: 'branchsetup-list',
            template: " \n<div class=\"container col-md-12\">\n    <form class=\"form-inline\"> \n        <!-- Form Name -->\n         <legend>Branch Setup List</legend>\n                <div *ngIf=\"_flagas\" class=\"input-group\" >\n                    <span class=\"input-group-btn input-md\"  style=\"width:20px;\">\n                        <button class=\"btn btn-primary\" type=\"button\" (click)=\"goNew();\">New</button>\n                    </span> \n                    <input id=\"textinput\" name=\"textinput\" type=\"text\"  placeholder=\"Search\" [(ngModel)]=\"_brobj.searchText\" (keyup)=\"searchKeyup($event)\" maxlength=\"50\" class=\"form-control input-md\">\n                    <span class=\"input-group-btn input-md\">\n                        <button class=\"btn btn-primary input-md\" type=\"button\" (click)=\"Searching()\" >\n                            <span class=\"glyphicon glyphicon-search\"></span>Search\n                        </button>\n                    </span>        \n                </div>      \n        </form>      \n\n        <div style = \"margin-top : 10px\">\n             <adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"_brobj.totalCount\" (rpChanged)=\"changedPager($event)\" ></adminpager> \n        </div>\n    \n        <div [hidden]=\"_recordhide\">\n         <table class=\"table table-striped table-condensed table-hover tblborder\" style=\"font-size:14px;\">\n            <thead>\n                <tr>\n                    <th>Branch Code</th>\n                    <th>Branch Name</th>\n                    <th>Phone No.</th>\n                    <th>Fax</th>\n                    <th>Address</th>    \n                </tr>\n            </thead>\n            <tbody>\n                <tr *ngFor=\"let obj of _brobj.data\">\n                    <td><a (click)=\"goto(obj.branchCode)\">{{obj.branchCode}} </a></td>      \n                    <td>{{obj.branchName}}</td>\n                    <td>{{obj.phone}}</td>\n                    <td>{{obj.fax}}</td>\n                    <td>{{obj.address}}</td>      \n                </tr>  \n            </tbody>\n        </table>\n        </div>\n </div> \n\n    <div id=\"sessionalert\" class=\"modal fade\">\n        <div class=\"modal-dialog\">\n            <div class=\"modal-content\">\n                <div class=\"modal-body\">\n                    <p>{{_sessionMsg}}</p>\n                </div>\n                <div class=\"modal-footer\">\n                </div>\n            </div>\n        </div>\n    </div>\n  <div [hidden] = \"_mflag\">\n    <div class=\"modal\" id=\"loader\"></div>\n   </div>\n    ",
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_http_service_1.RpHttpService])
    ], FrmBranchSetupListComponent);
    return FrmBranchSetupListComponent;
}());
exports.FrmBranchSetupListComponent = FrmBranchSetupListComponent;
//# sourceMappingURL=frmbranchsetuplist.component.js.map