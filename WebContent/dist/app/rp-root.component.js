"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var core_2 = require('@angular/core');
var rp_http_service_1 = require('./framework/rp-http.service');
var rp_intercom_service_1 = require('./framework/rp-intercom.service');
var rp_references_1 = require('./framework/rp-references');
var rp_client_util_1 = require('./util/rp-client.util');
var router_1 = require('@angular/router');
var Rx_1 = require('rxjs/Rx');
var rp_bean_1 = require('./framework/rp-bean');
var core_3 = require("@angular/core");
core_2.enableProdMode();
var RpRootComponent = (function () {
    function RpRootComponent(ics, _router, http, ref, title) {
        var _this = this;
        this.ics = ics;
        this._router = _router;
        this.http = http;
        this.ref = ref;
        this.title = title;
        this._alertflag = true;
        this._alertmsg = "";
        this._alerttype = "";
        this._util = new rp_client_util_1.ClientUtil();
        this._comboobj = { "value": "", "caption": "" };
        this.array = [{ "value": "", "caption": "" }];
        this.showmenu = false;
        ics.rpbean$.subscribe(function (x) {
            _this.showmenu = ics.isMenuBar();
            if (x.t1 !== null && x.t1 == "rp-popup") {
                jQuery("#rootpopupsize").attr('class', 'modal-dialog modal-lg');
                jQuery("#rootpopuptitle").text(x.t2);
                jQuery("#rootpopupbody").load(x.t3);
                jQuery("#rootpopup").modal();
            }
            else if (x.t1 !== null && x.t1 == "rp-wait") {
                jQuery("#rootpopupsize").attr('class', 'modal-dialog modal-sm');
                jQuery("#rootpopuptitle").text("Please Wait");
                jQuery("#rootpopupbody").text(x.t2);
                jQuery("#rootpopup").modal();
            }
            else if (x.t1 !== null && x.t1 == "rp-error") {
                jQuery("#rootpopupsize").attr('class', 'modal-dialog modal-sm');
                jQuery("#rootpopuptitle").text("System Exception");
                jQuery("#rootpopupbody").text(x.t2);
                jQuery("#rootpopup").modal();
            }
            else if (x.t1 !== null && x.t1 == "rp-msg") {
                jQuery("#rootpopupsize").attr('class', 'modal-dialog modal-sm');
                jQuery("#rootpopuptitle").text(x.t2);
                jQuery("#rootpopupbody").text(x.t3);
                jQuery("#rootpopup").modal();
            }
            else if (x.t1 !== null && x.t1 == "rp-msg-off") {
                jQuery("#rootpopuptitle").text("");
                jQuery("#rootpopupbody").text("");
                jQuery("#rootpopup").modal('hide');
            } /* else if (x.t1 !== null && x.t1 == "rp-alert") {
              this._alerttype = "alert alert-" + x.t2 + " fade in";
              this._alertmsg = x.t3;
              this._alertflag = false;
              setTimeout(() => this._alertflag = true, 3000);
            } */
            else if (x.t1 !== null && x.t1 == "rp-alert") {
                _this._alertmsg = x.t3;
                _this._alertflag = false;
                var _snack_style = 'msg-info';
                if (x.t2 == "success")
                    _snack_style = 'msg-success';
                else if (x.t2 == "warning")
                    _snack_style = 'msg-warning';
                else if (x.t2 == "danger")
                    _snack_style = 'msg-danger';
                else if (x.t2 == "information")
                    _snack_style = 'msg-info';
                document.getElementById("snackbar").innerHTML = _this._alertmsg;
                var snackbar_1 = document.getElementById("snackbar");
                snackbar_1.className = "show " + _snack_style;
                setTimeout(function () { snackbar_1.className = snackbar_1.className.replace("show", ""); }, 3000);
            }
        });
        this.init();
    }
    RpRootComponent.prototype.init = function () {
        var _this = this;
        this.http.doGet('json/config.json?random=' + Math.random()).subscribe(function (data) {
            _this.ics._title = data.title;
            _this.ics._app = data.app;
            _this.ics._appname = data.appname;
            _this.title.setTitle(_this.ics._title);
            _this.ics._apiurl = data.apiurl;
            _this.ics._rpturl = data.rpturl;
            _this.ics._successPageurl = data.successpageurl;
            _this.ics._imglogo = data.imgurl;
            _this.ics._imgurl = data.imageurl;
            _this.ics._welcometext = data.welcometext;
            _this.ics._signintext = data.signintext;
            _this.ics._appserviceurl = data.appserviceurl;
            _this.ics._sessiontime = data.sessiontime;
            _this.ics._profile.userName = data.username;
            _this.ics.chaturl = data.chaturl;
            _this.ics.chatimageurl = data.chatimageurl;
            _this.ics.ticketurl = data.ticketurl;
            _this.ics.walleturl = data.walleturl;
            _this.ics.cmsurl = data.cmsurl;
            _this.ics.icbsrpturl = data.icbsrpturl;
            _this.ics._profileImage1 = data.profileImage1; //atn
            _this.ics._profileImage2 = data.profileImage2; //atn
            _this.ics._imglink = data.imglink; //atn
            _this.ics._imageurl = data.imageurl; //atn
        }, function () { });
        this.http.doGet('json/lov3.json?random=' + Math.random()).subscribe(function (data) {
            _this.ref._lov3 = data;
            // this.getmainlist();
        }, function () { });
    };
    //get lov3 refs for item combo...
    RpRootComponent.prototype.unloadHandler = function (event) {
        /*  window.onbeforeunload = function (e) {
           return "Dude, are you sure you want to refresh? Think of the kittens!";} */
        var url = this.ics._apiurl + 'service001/signout?sessionID=' + this.ics._profile.sessionID;
        var json = this.ics._profile.userID;
        this.http.doPost(url, json).subscribe();
    };
    RpRootComponent.prototype.onPopState = function (event) {
        if (this._router.url == '/login') {
            var url = this.ics._apiurl + 'service001/signout?sessionID=' + this.ics._profile.sessionID;
            var json = this.ics._profile.userID;
            this.http.doPost(url, json).subscribe();
        }
    };
    RpRootComponent.prototype.docChanges = function (_doc) {
        if (this.ics._profile.role > 0) {
            var dt = new Date();
            var _time = (dt.getHours() * 3600) + (dt.getMinutes() * 60) + dt.getSeconds();
            this.ics._activeTimeout = _time;
            this.ics.sendBean(new rp_bean_1.RpBean());
        }
    };
    RpRootComponent.prototype.ngOnInit = function () {
        var _this = this;
        setInterval(function () { return _this.chkActive(); }, 10000);
    };
    RpRootComponent.prototype.chkActive = function () {
        var _this = this;
        var _activeTime = this.ics._activeTimeout;
        if (this.ics._profile.role > 0 && _activeTime != 0) {
            var dt = new Date();
            var _time = (dt.getHours() * 3600) + (dt.getMinutes() * 60) + dt.getSeconds();
            if ((_time - _activeTime) > (parseInt(this.ics._sessiontime) * 60)) {
                jQuery("#timeoutalert").modal();
                Rx_1.Observable.timer(3000).subscribe(function (x) {
                    var url = _this.ics._apiurl + 'service001/signout?sessionID=' + _this.ics._profile.sessionID;
                    var json = _this.ics._profile.userID;
                    var _returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "" };
                    _this.http.doPost(url, json).subscribe(function (data) {
                        _returnResult = data;
                        if (_returnResult.state) {
                            jQuery("#timeoutalert").modal('hide');
                            _this.ics._profile.role = 0;
                            _this.ics._activeTimeout = 0;
                            _this.ics.sendBean(new rp_bean_1.RpBean());
                            _this._router.navigate(['/login']);
                        }
                    }, function (error) { return alert(error); }, function () { });
                });
            }
        }
    };
    RpRootComponent.prototype.getmainlist = function () {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'service001/getMainList').subscribe(function (response) {
            if (response != null && response != undefined) {
                _this.ref._lov3.mainmenu = response.data;
            }
            else {
                _this.ref._lov3.mainmenu = _this.array;
            }
        }, function (error) {
            return function () { };
        });
    };
    __decorate([
        core_3.HostListener('window:unload', ['$event']), 
        __metadata('design:type', Function), 
        __metadata('design:paramtypes', [Object]), 
        __metadata('design:returntype', void 0)
    ], RpRootComponent.prototype, "unloadHandler", null);
    __decorate([
        core_3.HostListener('window:popstate', ['$event']), 
        __metadata('design:type', Function), 
        __metadata('design:paramtypes', [Object]), 
        __metadata('design:returntype', void 0)
    ], RpRootComponent.prototype, "onPopState", null);
    RpRootComponent = __decorate([
        core_1.Component({
            selector: 'rp-root',
            template: "\n  <div (keyup)=\"docChanges('keyup')\" (keydown)=\"docChanges('keydown')\" (click)=\"docChanges('click')\" (DOMMouseScroll)=\"docChanges('DOMMouseScroll')\"\n  (mousewheel)=\"docChanges('mousewheel')\" (mousedown)=\"docChanges('mousedown')\" (touchstart)=\"docChanges('touchstart')\" (touchmove)=\"docChanges('touchmove')\"\n  (scroll)=\"docChanges('scroll')\" (focus)=\"docChanges('focus')\">\n\n    <rp-menu *ngIf=\"showmenu\"></rp-menu>\n    <div class=\"container col-md-12\">\n      <div id=\"snackbar\" [hidden]=\"_alertflag\">>{{_alertmsg}}</div>\n      <!--<div id=\"alert\" class={{_alerttype}} [hidden]=\"_alertflag\">\n        {{_alertmsg}}\n      </div>-->\n    </div>\n    <router-outlet></router-outlet>\n    <div id=\"rootpopup\" class=\"modal fade\" role=\"dialog\">\n      <div id=\"rootpopupsize\" class=\"modal-dialog modal-lg\">  \n        <div class=\"modal-content\">\n          <div class=\"modal-header\">\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n            <h4 id=\"rootpopuptitle\" class=\"modal-title\">RP Report ***</h4>\n          </div>\n          <div id=\"rootpopupbody\" class=\"modal-body\">\n          </div>\n          <div class=\"modal-footer\">\n            <button type=\"button\" class=\"btn input-sm btn-primary\" data-dismiss=\"modal\">Close</button>\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <div id=\"timeoutalert\" class=\"modal fade\">\n      <div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n          <div class=\"modal-body\">\n            <p>Your session has expired</p>\n          </div>\n          <div class=\"modal-footer\">\n          </div>\n        </div>\n      </div>\n    </div>\n  ",
            providers: [rp_intercom_service_1.RpIntercomService, rp_references_1.RpReferences, rp_http_service_1.RpHttpService]
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_http_service_1.RpHttpService, rp_references_1.RpReferences, platform_browser_1.Title])
    ], RpRootComponent);
    return RpRootComponent;
}());
exports.RpRootComponent = RpRootComponent;
//# sourceMappingURL=rp-root.component.js.map