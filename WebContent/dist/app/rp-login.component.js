"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_http_service_1 = require('./framework/rp-http.service');
var rp_intercom_service_1 = require('./framework/rp-intercom.service');
var rp_bean_1 = require('./framework/rp-bean');
var rp_client_util_1 = require('./util/rp-client.util');
core_2.enableProdMode();
var RpLoginComponent = (function () {
    function RpLoginComponent(_util, ics, _router, http) {
        this._util = _util;
        this.ics = ics;
        this._router = _router;
        this.http = http;
        this._mflag = true;
        this.url = "url";
        this.cname = "username";
        this.pname = "password";
        this._signintext = "";
        this.remembercheck = true;
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        this.ics._profile.role = 0;
        this.checkCookie();
        this.ics.sendBean(new rp_bean_1.RpBean());
        ics.confirmUpload(false);
        this.getsignintext();
    }
    RpLoginComponent.prototype.checkCookie = function () {
        var value = this.readCookieValue('username');
        if (value != "" && value != null) {
            this._user = value;
        }
        else {
            this._user = "";
            this.remembercheck = false;
        }
    };
    RpLoginComponent.prototype.goPost = function () {
        var _this = this;
        try {
            //this.ics.sendBean({ t1: "rp-wait", t2: "Signing in ..." });
            this._mflag = false;
            var iv = this._util.getIvs();
            var dm = this._util.getIvs();
            var salt = this._util.getIvs();
            var profile = { "userID": this._user, "password": this._pw, "userType": 2, "iv": iv, "dm": dm, "salt": salt };
            var url = this.ics._apiurl + 'service001/signin';
            profile.password = this._util.getEncryptText(iv, salt, dm, profile.password);
            this.http.doPost(url, profile).subscribe(function (data) {
                console.log("Sign in response data=" + JSON.stringify(data));
                _this.ics.sendBean({ t1: "rp-msg-off" });
                if (data.code == "0000") {
                    _this.authorize(data);
                    _this._mflag = true;
                }
                else {
                    _this._result = data.desc; //"Invalid User ID or Password";
                    _this._mflag = true;
                }
            }, function (error) {
                _this._mflag = true;
                _this.ics.sendBean({ t1: "rp-error", t2: "HTTP Error Type " + error.type });
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    // goPost() {
    //   let url: string = this.ics._apiurl + 'service001/signin';
    //   let profile: any = { "userID": this._user, "password": this._pw, "commandCenter": this._remember };
    //   this.http.doPost(url, profile).subscribe(
    //     data => {
    //       if (this.apiUrl == null && this.apiUrl == undefined) {
    //         this.ics.sendBean({ t1: "rp-msg-off" });
    //       }
    //       if(data.code == "0000"){
    //         this.authorize(data);
    //       }else{
    //          this._result = data.desc //"Invalid User ID or Password";
    //       }
    //       // if (data.role > 0) {
    //       //   this.authorize(data);
    //       // } else {
    //       //   this._result = "Invalid User ID or Password";
    //       // }
    //     },
    //     error => {
    //       this.ics.sendBean({ t1: "rp-error", t2: "HTTP Error Type " + error.type });
    //     },
    //     () => { }
    //   );
    // }
    RpLoginComponent.prototype.authorize = function (data) {
        this.ics._profile = data;
        this.ics._profile.userID = this._user;
        var value = this.readCookieValue('pager');
        if (value != "" && value != null) {
            this.ics._profile.n1 = Number(value);
        }
        else {
            this.ics._profile.n1 = 10;
        }
        this.ics.sendBean(new rp_bean_1.RpBean());
        this._router.navigate(['/000']);
    };
    RpLoginComponent.prototype.setremember = function (cname, _user, pname, _pw, event) {
        if (event.target.checked) {
            if (cname == "" && _pw == "") {
                document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
                document.cookie = "password=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
                document.cookie = "url=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
            }
            else {
                var d = new Date();
                d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
                var expires = "expires=" + d.toDateString();
                document.cookie = cname + "=" + _user + ";" + expires;
                document.cookie = pname + "=" + _pw + ";" + expires;
                document.cookie = this.url + "=" + this.ics._apiurl + ";" + expires;
            }
        }
        else {
            document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
            document.cookie = "password=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
            document.cookie = "url=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
        }
    };
    RpLoginComponent.prototype.readCookieValue = function (name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ')
                c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0)
                return c.substring(nameEQ.length, c.length);
        }
        return null;
    };
    RpLoginComponent.prototype.getsignintext = function () {
        var _this = this;
        this.http.doGet('json/config.json?random=' + Math.random()).subscribe(function (data) {
            _this._signintext = data.signintext;
        }, function (error) { return alert(error); }, function () { });
    };
    RpLoginComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    RpLoginComponent = __decorate([
        core_1.Component({
            selector: 'rp-login',
            template: "\n  <div class=\"container\">\n    <div class=\"row clearfix\">\n        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n            <form class=\"form-horizontal\" (ngSubmit)=\"goPost()\">\n                <div class=\"card card-container\">\n                    <div class=\"row col-md-12\">&nbsp;</div>\n                    <div class=\"row\">\n                        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n                            <h3 style=\"text-align:center;\">{{_signintext}}</h3>\n                        </div>\n                    </div>\n\n                    <div class=\"row col-md-12\">&nbsp;</div>\n\n                    <div style=\"margin-bottom: 30px\" class=\"input-group\">\n                        <span class=\"input-group-addon\">\n                            <i class=\"glyphicon glyphicon-user\"></i>\n                        </span>\n                        <input id=\"nameid\" class=\"form-control\" Required type=\"text\" [(ngModel)]=\"_user\" [ngModelOptions]=\"{standalone: true}\" placeholder=\"User ID\"\n                            autofocus>\n                    </div>\n\n                    <div style=\"margin-bottom: 30px\" class=\"input-group\">\n                        <span class=\"input-group-addon\">\n                            <i class=\"glyphicon glyphicon-lock\"></i>\n                        </span>\n                        <input id=\"itemid\" class=\"form-control\" Required type=\"password\" [(ngModel)]=\"_pw\" [ngModelOptions]=\"{standalone: true}\"\n                            placeholder=\"Password\">\n                    </div>\n\n                    <!-- <div class=\"row\">\n                            <div class=\"col-md-offset-0 col-md-10\"> \n                                <div class=\"checkbox\">\n                                <label><input type=\"checkbox\" value=\"remember-me\" [(ngModel)]=remembercheck [ngModelOptions]=\"{standalone: true}\" (click)=\"setremember(cname,_user,pname,_pw,$event)\" > Remember me</label>\n                                </div>\n                            </div>\n                    </div>-->\n\n                   <div style=\"margin-bottom: 50px\" class=\"form-group\" align=\"center\">\n                        <button class=\"btn input-sm btn-primary btn-block\" style=\"width:91%\" type=\"submit\">Sign In</button>\n                    </div>\n                    <div class=\"row col-md-12\">&nbsp;</div>\n                    <div align=\"center\" style=\"margin-bottom: 50px\">\n                        <span *ngIf=\"_result !=null\" class=\"label\" style=\"color: red;\">{{_result}} </span>\n                    </div>\n                    <div class=\"row col-md-12\">&nbsp;</div>\n                </div>\n            </form>\n        </div>\n    </div>\n  </div>\n  <div [hidden]=\"_mflag\">\n  <div  id=\"loader\" class=\"modal\" ></div>\n</div>\n  "
        }), 
        __metadata('design:paramtypes', [rp_client_util_1.ClientUtil, rp_intercom_service_1.RpIntercomService, router_1.Router, rp_http_service_1.RpHttpService])
    ], RpLoginComponent);
    return RpLoginComponent;
}());
exports.RpLoginComponent = RpLoginComponent;
//# sourceMappingURL=rp-login.component.js.map