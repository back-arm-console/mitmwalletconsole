"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var Rx_1 = require('rxjs/Rx');
core_2.enableProdMode();
var SMSSettingComponent = (function () {
    function SMSSettingComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
        this._obj = this.getDefaultObj();
        this._mflag = false;
        this._chkloginId = 'false';
        this.frommsghide = false;
        this.tomsghide = true;
        this.languageFlat = false;
        this._key = "";
        this.sessionAlertMsg = "";
        this._util = new rp_client_util_1.ClientUtil();
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._formhide = false;
            this.checkSession();
            this.getAllMerchant();
            this.getServiceLov();
            this.getFunctionLov();
            this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
            this._obj = this.getDefaultObj();
        }
    }
    SMSSettingComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this._key = id;
                _this.goReadBySyskey(id);
            }
        });
    };
    SMSSettingComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    SMSSettingComponent.prototype.getDefaultObj = function () {
        jQuery("#mydelete").prop("disabled", true);
        jQuery("#mySave").prop("disabled", false);
        this._onlyfrom = false;
        this._combohide = true;
        this._chkread = false;
        return { "code": "", "desc": "", "id": 0, "syskey": 0, "serviceCode": "", "serviceDesc": "", "funCode": "", "funDesc": "", "site": "", "from": "", "to": "", "both": "", "fromMsg": "", "toMsg": "", "message": "", "merchantID": "", "operatorType": "", "active": "1", "sessionID": "", "userID": "", "language": "", "languageDesc": "" };
    };
    SMSSettingComponent.prototype.goReadBySyskey = function (p) {
        var _this = this;
        try {
            this._formhide = true;
            this._mflag = false;
            this._chkread = true;
            var url = this.ics.cmsurl + 'serviceCMS/getSMSSettingDataById?id=' + p + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
            this.http.doGet(url).subscribe(function (data) {
                if (data.code == '0016') {
                    _this.sessionAlertMsg = data.desc;
                    _this.showMessage();
                }
                else {
                    console.log('Your Service Coode is:' + _this._obj.serviceCode);
                    jQuery("#mydelete").prop("disabled", false);
                    _this._obj = data;
                    console.log('Edit Data: ' + JSON.stringify(_this._obj));
                    if (_this._obj.serviceCode == "1") {
                        _this._onlyfrom = false;
                        _this.frommsghide = false;
                    }
                    else if (_this._obj.serviceCode == "2") {
                        _this._onlyfrom = false;
                        _this.frommsghide = false;
                    }
                    else if (_this._obj.serviceCode == "3") {
                        _this._onlyfrom = true;
                        _this.frommsghide = true;
                    }
                    else {
                        _this._onlyfrom = false;
                        _this.frommsghide = false;
                        _this.tomsghide = false;
                        _this.languageFlat = true;
                    }
                    if (_this._obj.from == "1") {
                        _this._obj.site = "From";
                        _this.frommsghide = false;
                        _this.tomsghide = true;
                    }
                    else if (_this._obj.to == "1") {
                        _this._obj.site = "To";
                        _this.tomsghide = false;
                        _this.frommsghide = true;
                    }
                    else if (_this._obj.both == "1") {
                        _this._obj.site = "Both";
                    }
                    if (_this._obj.funCode == '1' || _this._obj.funCode == '2') {
                        _this._combohide = false;
                    }
                    else {
                        _this._combohide = true;
                    }
                }
                // for(let i=0;i<this.ref._lov3.refservice.length;i++){
                //   this.ref._lov3.refservice[i].code=this._obj.serviceCode;
                // }
                _this._formhide = false;
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    SMSSettingComponent.prototype.goList = function () {
        this._router.navigate(['/smssettinglist']);
    };
    SMSSettingComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this.goLogOut();
        });
    };
    SMSSettingComponent.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    SMSSettingComponent.prototype.showMessageAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    SMSSettingComponent.prototype.clearData = function () {
        this._formhide = false;
        this.frommsghide = false;
        this.tomsghide = true;
        this.languageFlat = false;
        this._obj = this.getDefaultObj();
        this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
        this._chkloginId = 'false';
        jQuery("#mydelete").prop("disabled", true);
        jQuery("#mySave").prop("disabled", false);
    };
    SMSSettingComponent.prototype.goNew = function () {
        this.clearData();
    };
    SMSSettingComponent.prototype.changeFuction = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex; //Get Selected Index
        var value = options[options.selectedIndex].value; //Get Selected Index's Value 
        this._obj.funCode = value;
        if (this._obj.funCode == '1' || this._obj.funCode == '2') {
            this._combohide = false;
        }
        else {
            this._combohide = true;
            this._obj.merchantID = "";
        }
        for (var i = 0; i < this.ref._lov3.reffunction.length; i++) {
            if (this.ref._lov3.reffunction[i].value == value) {
                this._obj.funCode = this.ref._lov3.reffunction[i].value;
                this._obj.funDesc = this.ref._lov3.reffunction[i].caption;
                break;
            }
        }
    };
    SMSSettingComponent.prototype.getFunctionLov = function () {
        var _this = this;
        this.http.doGet(this.ics.cmsurl + 'serviceAdmLOV/getFunctionLov?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (data) {
            _this.ref._lov3.reffunction = data.reffunction;
            var arr = [];
            if (_this.ref._lov3.reffunction != null && _this.ref._lov3.reffunction != undefined) {
                if (!(_this.ref._lov3.reffunction instanceof Array)) {
                    var m = [];
                    m[1] = _this.ref._lov3.reffunction;
                    arr.push(m[1]);
                }
                for (var j = 0; j < _this.ref._lov3.reffunction.length; j++) {
                    arr.push(_this.ref._lov3.reffunction[j]);
                }
            }
            else {
                _this.ref._lov3.reffunction = [{ "value": "", "caption": "-" }];
            }
            _this.ref._lov3.reffunction = arr;
            _this._obj.active = _this.ref._lov1.refActive[0].value;
            _this._mflag = true;
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
            else {
            }
        }, function () { });
    };
    SMSSettingComponent.prototype.changeService = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex; //Get Selected Index
        var value = options[options.selectedIndex].value; //Get Selected Index's Value 
        this._obj.serviceCode = value;
        if (this._obj.serviceCode == "1") {
            this._onlyfrom = false;
            this.frommsghide = false;
            this.tomsghide = true;
        }
        else if (this._obj.serviceCode == "2") {
            this._onlyfrom = false;
            this.frommsghide = false;
            this.tomsghide = true;
        }
        else if (this._obj.serviceCode == "3") {
            this._onlyfrom = true;
            this.frommsghide = true;
            this.tomsghide = true;
        }
        else if (this._obj.serviceCode == "4" && this._obj.site == "Both") {
            this._onlyfrom = false;
            this.frommsghide = false;
            this.tomsghide = false;
            this.languageFlat = true;
        }
        else {
            this._onlyfrom = false;
            this.frommsghide = false;
            this.tomsghide = true;
            this.languageFlat = true;
        }
        for (var i = 0; i < this.ref._lov3.refservice.length; i++) {
            if (this.ref._lov3.refservice[i].value == value) {
                this._obj.serviceCode = this.ref._lov3.refservice[i].value;
                this._obj.serviceDesc = this.ref._lov3.refservice[i].caption;
                break;
            }
        }
    };
    SMSSettingComponent.prototype.changeSite = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex; //Get Selected Index
        var value = options[options.selectedIndex].value; //Get Selected Index's Value 
        this._obj.site = value;
        if (this._obj.site == "From") {
            this.frommsghide = false;
            this.tomsghide = true;
        }
        else if (this._obj.site == "To") {
            this.tomsghide = false;
            this.frommsghide = true;
        }
        else {
            this.frommsghide = false;
            this.tomsghide = false;
        }
    };
    SMSSettingComponent.prototype.getServiceLov = function () {
        var _this = this;
        this.http.doGet(this.ics.cmsurl + 'serviceAdmLOV/getServiceLov?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (data) {
            if (data.refservice != null) {
                _this.ref._lov3.refservice = data.refservice;
                var arr = [];
                if (_this.ref._lov3.refservice != null) {
                    if (!(_this.ref._lov3.refservice instanceof Array)) {
                        var m = [];
                        m[1] = _this.ref._lov3.refservice;
                        arr.push(m[1]);
                    }
                    for (var j = 0; j < _this.ref._lov3.refservice.length; j++) {
                        arr.push(_this.ref._lov3.refservice[j]);
                    }
                }
                _this.ref._lov3.refservice = arr;
            }
            else
                _this.ref._lov3.refservice = [{ "sessionID": "", "userID": "" }];
        }, function (error) {
            if (error._body.type == 'error') {
            }
            else {
            }
        }, function () { });
    };
    SMSSettingComponent.prototype.getAllMerchant = function () {
        var _this = this;
        this.http.doGet(this.ics.cmsurl + 'serviceAdmLOV/getmerchantidlistdetail?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (data) {
            _this.ref._lov3.ref015 = data.ref015;
            if (_this.ref._lov3.ref015 != null && _this.ref._lov3.ref015 != undefined) {
                if (!(_this.ref._lov3.ref015 instanceof Array)) {
                    var m = [];
                    m[0] = _this.ref._lov3.ref015;
                    _this.ref._lov3.ref015 = m;
                }
            }
            else {
                _this.ref._lov3.ref015 = [{ "value": "", "caption": "-" }];
            }
        }, function (error) {
            // if (error._body.type == 'error') {
            //alert("Connection Timed Out!");
            //}
        }, function () { });
    };
    SMSSettingComponent.prototype.goSave = function () {
        if (this._obj.serviceCode == '') {
            this.showMsg("Please select Service", false);
        }
        else if (this._obj.funCode == '') {
            this.showMsg("Please select Function", false);
        }
        else if (this._obj.site == '') {
            this.showMsg("Please select Site", false);
        }
        else if (this._obj.active == '') {
            this.showMsg("Please select Active", false);
        }
        else if (this._combohide == false) {
            if (this._obj.merchantID == '') {
                this.showMsg("Please select Merchant", false);
            }
            else {
                this.saveSMSSetting();
            }
        }
        else {
            this.saveSMSSetting();
        }
    };
    SMSSettingComponent.prototype.saveSMSSetting = function () {
        var _this = this;
        try {
            this._mflag = false;
            if (this._obj.site == "From") {
                this._obj.from = "1";
                this._obj.to = "0";
                this._obj.both = "0";
            }
            else if (this._obj.site == "To") {
                this._obj.from = "0";
                this._obj.to = "1";
                this._obj.both = "0";
            }
            else {
                this._obj.from = "0";
                this._obj.to = "0";
                this._obj.both = "1";
            }
            for (var j = 0; j < this.ref._lov3.reffunction.length; j++) {
                if (this._obj.funCode == this.ref._lov3.reffunction[j].value) {
                    this._obj.funDesc = this.ref._lov3.reffunction[j].caption;
                }
            }
            for (var i = 0; i < this.ref._lov3.refservice.length; i++) {
                if (this._obj.serviceCode == this.ref._lov3.refservice[i].value) {
                    this._obj.serviceDesc = this.ref._lov3.refservice[i].caption;
                }
            }
            for (var k = 0; k < this.ref._lov1.refLanguage.length; k++) {
                if (this._obj.language == this.ref._lov1.refLanguage[k].value) {
                    this._obj.languageDesc = this.ref._lov1.refLanguage[k].caption;
                }
            }
            this._obj.sessionID = this.ics._profile.sessionID;
            this._obj.userID = this.ics._profile.userID;
            var url = this.ics.cmsurl + 'serviceCMS/saveSMSSetting';
            var json = this._obj;
            this.http.doPost(url, json).subscribe(function (data) {
                _this._returnResult = data;
                if (_this._returnResult.state == 'true') {
                    _this._obj.id = _this._returnResult.keyResult;
                    jQuery("#mydelete").prop("disabled", false);
                    _this.showMessageAlert(data.msgDesc);
                }
                else {
                    if (_this._returnResult.msgCode == '0016') {
                        _this.sessionAlertMsg = data.desc;
                        _this.showMsg(data.desc, false);
                    }
                    else {
                        _this.showMsg(data.msgDesc, true);
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    SMSSettingComponent.prototype.goDelete = function () {
        var _this = this;
        try {
            this._mflag = false;
            this._obj.sessionID = this.ics._profile.sessionID;
            this._obj.userID = this.ics._profile.userID;
            var json = this._obj;
            var url = this.ics.cmsurl + 'serviceCMS/deleteSMSSetting';
            this.http.doPost(url, json).subscribe(function (data) {
                _this._returnResult = data;
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.desc;
                    _this.showMsg(data.desc, false);
                }
                else {
                    _this.showMsg(data.msgDesc, true);
                    if (data.state) {
                        _this.clearData();
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    SMSSettingComponent.prototype.getCodeFromLovdeatils = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics.cmsurl + 'serviceCMS/getCodeFromLovdeatils?merchantId=' + this._obj.merchantID).subscribe(function (data) {
                if (data != null && data != undefined) {
                    if (!(data.reflovdetails instanceof Array)) {
                        var m = [];
                        m[0] = data.reflovdetails;
                        _this.ref._lov3.reflovdetails = m;
                    }
                    else {
                        _this.ref._lov3.reflovdetails = data.reflovdetails;
                    }
                }
                else {
                    _this.ref._lov3.reflovdetails = [{ "value": "", "caption": "-" }];
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    SMSSettingComponent.prototype.updateComboData = function (event) {
        this._obj.merchantID = event;
        this.getCodeFromLovdeatils();
    };
    SMSSettingComponent.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    SMSSettingComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsgAlert(data.desc);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsgAlert(data.desc);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    SMSSettingComponent.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    SMSSettingComponent.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    SMSSettingComponent.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    SMSSettingComponent = __decorate([
        core_1.Component({
            selector: 'smssetting-setup',
            template: "\n  <div class=\"container-fluid\">\n  <div class=\"row clearfix\">\n  <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n  <form class=\"form-horizontal\" > \n  <!-- Form Name -->\n  <legend>Message Setting</legend>\n    <div class=\"cardview list-height\">\n      <div class=\"row col-md-12\">  \n        <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goList()\" >List</button> \n        <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goNew()\" >New</button>      \n        <button class=\"btn btn-sm btn-primary\" id=\"mySave\" (click)=\"goSave()\">Save</button>          \n        <button class=\"btn btn-sm btn-primary\" disabled id=\"mydelete\"  type=\"button\" (click)=\"goDelete();\" >Delete</button> \n      </div>\n      <div class=\"row col-md-12\">&nbsp;</div> \n      <div class=\"form-group\">    \n      <div class=\"col-md-8\">   \t\t\t\n        <div class=\"form-group\">\n          <label class=\"col-md-2\">Service</label>\n          <div class=\"col-md-4\"> \n          <select *ngIf =\"_chkread==true\" disabled [(ngModel)]=\"_obj.serviceCode\" [ngModelOptions]=\"{standalone: true}\"  class=\"form-control input-sm\" (change)=\"changeService($event)\"  >\n            <option *ngFor=\"let item of ref._lov3.refservice\" value=\"{{item.value}}\" >{{item.caption}}</option> \n          </select>    \n        \n          <select *ngIf =\"_chkread !=true\"  [(ngModel)]=\"_obj.serviceCode\" [ngModelOptions]=\"{standalone: true}\"  class=\"form-control input-sm\" (change)=\"changeService($event)\"  >\n            <option *ngFor=\"let item of ref._lov3.refservice\" value=\"{{item.value}}\" >{{item.caption}}</option> \n          </select>           \n          </div> \n        </div>\n        <div class=\"form-group\">\n          <label class=\"col-md-2\">Function</label>\n          <div class=\"col-md-4\" > \n          <select *ngIf =\"_chkread==true\" disabled [(ngModel)]=\"_obj.funCode\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control input-sm\" (change)=\"changeFuction($event)\">\n            <option *ngFor=\"let item of ref._lov3.reffunction\" value=\"{{item.value}}\" >{{item.caption}}</option> \n          </select> \n          <select *ngIf =\"_chkread!=true\" [(ngModel)]=\"_obj.funCode\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control input-sm\" (change)=\"changeFuction($event)\">\n            <option *ngFor=\"let item of ref._lov3.reffunction\" value=\"{{item.value}}\" >{{item.caption}}</option> \n          </select> \n          </div>  \n        </div>\n        <div class=\"form-group\">\n          <label class=\"col-md-2\">Which Site</label>\n          <div class=\"col-md-4\" *ngIf=\"_onlyfrom == false\"> \n          <select *ngIf =\"_chkread==true\" disabled [(ngModel)]=\"_obj.site\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control input-sm\">\n            <option *ngFor=\"let item of ref._lov1.refsite\" value=\"{{item.value}}\" >{{item.caption}}</option>\n          </select> \n          <select *ngIf =\"_chkread!=true\"  [(ngModel)]=\"_obj.site\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control input-sm\" (change)=\"changeSite($event)\">\n            <option *ngFor=\"let item of ref._lov1.refsite\" value=\"{{item.value}}\" >{{item.caption}}</option>\n          </select>  \n          </div>\n          <div class=\"col-md-4\"  *ngIf=\"_onlyfrom == true \">\n          <select *ngIf =\"_chkread==true\" disabled  [(ngModel)]=\"_obj.site\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control input-sm\">\n            <option *ngFor=\"let item of ref._lov1.reffrsite\" value=\"{{item.value}}\" >{{item.caption}}</option>                   \n          </select>  \n          <select *ngIf =\"_chkread!=true\"  [(ngModel)]=\"_obj.site\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control input-sm\" (change)=\"changeSite($event)\">\n            <option *ngFor=\"let item of ref._lov1.reffrsite\" value=\"{{item.value}}\" >{{item.caption}}</option>                   \n          </select>                  \n          </div> \n        </div> \n        <div *ngIf=\"_combohide == false\">  \n        <div class=\"form-group\">\n           <label class=\"col-md-2\"> Merchant ID </label>\n           <div class=\"col-md-4\">\n               <select [(ngModel)]=\"_obj.merchantID\" [ngModelOptions]=\"{standalone: true}\"  class=\"form-control input-sm\" (change)=\"updateComboData($event.target.value)\">\n               <option *ngFor=\"let item of ref._lov3.ref015\" value=\"{{item.value}}\" >{{item.caption}}</option> \n              </select> \n           </div>\n           <div class=\"col-md-1\"> \n            {{_obj.merchantID}} \n            </div>\n        </div>\n         </div>\n        <div class=\"form-group\">\n          <label class=\"col-md-2\" >Active</label>\n          <div class=\"col-md-4\">\n          <select [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_obj.active\" class=\"form-control input-sm\">\n            <option *ngFor=\"let item of ref._lov1.refActive\" value=\"{{item.value}}\" >{{item.caption}}</option> \n          </select>                \n          </div> \n        </div>\n        <div class=\"form-group\" *ngIf=\"languageFlat\">\n        <label class=\"col-md-2\" >Language</label>\n        <div class=\"col-md-4\">\n        <select [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_obj.language\" class=\"form-control input-sm\">\n          <option *ngFor=\"let item of ref._lov1.refLanguage\" value=\"{{item.value}}\" >{{item.caption}}</option> \n        </select>                \n        </div> \n      </div>\n        <div class=\"form-group\" [hidden]=\"frommsghide\">\n          <label class=\"col-md-2\"> From Message </label>\n          <div class=\"col-md-4\">\n          <textarea  class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"_obj.fromMsg\" rows=\"7\" maxlength=\"200\"></textarea>                 \n          </div> \n        </div>\n        <div class=\"form-group\" [hidden]=\"tomsghide\">\n          <label class=\"col-md-2\">To Message </label>\n          <div class=\"col-md-4\">\n          <textarea  class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"_obj.toMsg\" rows=\"7\" maxlength=\"200\"></textarea>                 \n          </div> \n        </div>\t\n      </div>\n    </div>\n    </div>\n  </form>\n  </div>\n  </div>\n</div>\n     \n  <div id=\"sessionalert\" class=\"modal fade\">\n    <div class=\"modal-dialog\">\n      <div class=\"modal-content\">\n        <div class=\"modal-body\">\n          <p>{{sessionAlertMsg}}</p>\n        </div>\n        <div class=\"modal-footer\">\n        </div>\n      </div>\n    </div>\n  </div>\n\n<div [hidden] = \"_mflag\">\n<div class=\"modal\" id=\"loader\"></div>\n</div> \n    \n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], SMSSettingComponent);
    return SMSSettingComponent;
}());
exports.SMSSettingComponent = SMSSettingComponent;
//# sourceMappingURL=smssettingsetup.component.js.map