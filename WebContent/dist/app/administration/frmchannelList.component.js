"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var ng2_qrcode_1 = require('ng2-qrcode');
var Rx_1 = require('rxjs/Rx');
core_1.enableProdMode();
var FrmChannelList = (function () {
    function FrmChannelList(ics, ref, _router, http) {
        this.ics = ics;
        this.ref = ref;
        this._router = _router;
        this.http = http;
        this._mflag = true;
        this.nore = 0;
        this.sessionAlertMsg = "";
        this.channelList = { "data": [{ "channelSyskey": 0, "autokey": 0, "channelName": "", "region": "" }], "msgcode": "", "msgdesc": "" };
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.ticketStatus = "";
        this.regionCaption = "";
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this.regionCaption = 'ALL';
            this.getStateList();
            this.getChannelList();
        }
    }
    FrmChannelList.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    /* getStateList() {
        try {
            this.http.doGet(this.ics._apiurl + 'service001/getStateListByUserID?userID='+this.ics._profile.userID).subscribe(
                response => {
                    if (response.refstate != null && response.refstate != undefined) {
                        this.ref._lov3.refstate = [{ "value": "", "caption": "" }];
                        for (let i = 0; i < response.refstate.length; i++) {
                            console.log(JSON.stringify(response.refstate[i]));
                            if (response.refstate[i].value == "13000000") {
                                this.ref._lov3.refstate[i].caption = response.refstate[i].caption;
                              }
                              if (response.refstate[i].value == "10000000") {
                                this.ref._lov3.refstate[i] = response.refstate[i];
                              }
                              if (response.refstate[i].value == "00000000") {
                                this.ref._lov3.refstate[i] = response.refstate[i];
                              }
                        }
                    } else {
                        this.ref._lov3.refstate = [{ "value": "", "caption": "Empty" }];
                    }
                },
                error => {
                    if (error._body.type == 'error') {
                        alert("Connection Error!");
                    }
                }, () => { });
        } catch (e) {
            alert("Invalid URL");
        }
    } */
    FrmChannelList.prototype.getStateList = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics._apiurl + 'service001/getStateListByUserID?userID=' + this.ics._profile.userID).subscribe(function (response) {
                if (response.refstate != null && response.refstate != undefined) {
                    //this.ref._lov3.refstate = [{ "value": "", "caption": "" }];
                    _this.ref._lov3.refstate = [];
                    if (!(response.refstate instanceof Array)) {
                        var m = [];
                        m[0] = response.data;
                        _this.ref._lov3.refstate = m;
                    }
                    else {
                        _this.ref._lov3.refstate = response.refstate;
                    }
                }
                else {
                    _this.ref._lov3.refstate = [{ "value": "", "caption": "Empty" }];
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Error!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmChannelList.prototype.getChannelList = function () {
        var _this = this;
        var json = {
            sessionID: this.ics._profile.sessionID,
            userID: this.ics._profile.userID
        };
        var url = this.ics._apiurl + 'service001/getChannelList';
        try {
            this.http.doPost(url, json).subscribe(function (data) {
                _this.nore = 1;
                if (data.msgCode == "0016") {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMessage();
                }
                else {
                    if (data.channeldata != null) {
                        if (!(data.channeldata instanceof Array)) {
                            var m = [];
                            m[0] = data.channeldata;
                            _this.channelList.data = m;
                        }
                        else {
                            _this.channelList.data = data.channeldata;
                        }
                    }
                    else {
                        _this.channelList.data = [];
                    }
                }
            }, function (error) {
                console.log("signin error=" + error.status);
                //this.nores = 0;            
            });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmChannelList.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this.goLogOut();
        });
    };
    FrmChannelList.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    FrmChannelList.prototype.changeState = function (region) {
        console.log("REgion=" + region);
        if (region == '13000000') {
            this.regionCaption = 'YANGON';
        }
        if (region == '10000000') {
            this.regionCaption = 'MANDALAY';
        }
        if (region == '00000000' || region == '') {
            this.regionCaption = 'ALL';
        }
    };
    FrmChannelList.prototype.goto = function (p) {
        console.log("p=" + JSON.stringify(p));
        this._router.navigate(['/chatlist', 'read', p]);
    };
    FrmChannelList.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmChannelList.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    FrmChannelList = __decorate([
        core_1.Component({
            selector: 'fmr-channelList',
            entryComponents: [ng2_qrcode_1.QRCodeComponent],
            template: "\n    <div class=\"container col-md-12\">\n    <form class=\"form-inline\"> \n        <!-- Form Name -->\n        <legend>Channel</legend>\n        <div class=\"input-group\">\n            <rp-input rpLabelClass=\"col-md-4\" rpClass=\"col-md-8\" \n                      rpLabel=\"Region\" [(rpModel)]=\"channelList.region\" \n                      (change)=\"changeState(channelList.region)\" rpRequired =\"true\" \n                      rpType=\"statecombo\" autofocus>\n            </rp-input>\n        </div>\n        <div class=\"col-md-12\">&nbsp;</div>\n    </form>\n\n    <div class=\"row\">\n        <div class=\"col-md-12\">\n            <div class=\"table-responsive\" *ngIf=\"nore==1\">\n                <table class=\"table table-striped table-condensed table-hover tblborder\" id=\"tblstyle\">\n                    <thead>\n                        <tr>\n                            <th>Channel Key</th>\n                            <th>Channel Name</th>\n                            <th>Region</th>\n                        </tr>\n                    </thead>\n                    <tbody>\n                        <ng-container *ngFor=\"let obj of channelList.data\">\n                            <tr *ngIf=\"obj.region == regionCaption\">\n                                <td><a (click)=\"goto(obj.channelSyskey)\">{{obj.channelSyskey}}</a></td>\n                                <td class=\"uni\">{{obj.channelName}}</td>\n                                <td class=\"uni\">{{obj.region}}</td>\n                            </tr>\n                            <tr *ngIf=\"'ALL' == regionCaption\">\n                                <td><a (click)=\"goto(obj.channelSyskey)\">{{obj.channelSyskey}}</a></td>\n                                <td class=\"uni\">{{obj.channelName}}</td>\n                                <td class=\"uni\">{{obj.region}}</td>\n                            </tr>\n                        </ng-container>\n                    </tbody>\n                </table>\n            </div>    \n        </div>\n    </div>\n</div>\n\n<div [hidden]=\"_mflag\">\n  <div class=\"modal\" id=\"loader\"></div>\n</div>\n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, rp_references_1.RpReferences, router_1.Router, rp_http_service_1.RpHttpService])
    ], FrmChannelList);
    return FrmChannelList;
}());
exports.FrmChannelList = FrmChannelList;
//# sourceMappingURL=frmchannelList.component.js.map