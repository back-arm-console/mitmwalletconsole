"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
require('tinymce/tinymce.min');
require('tinymce/themes/modern/theme');
require('tinymce/plugins/link/plugin.js');
require('tinymce/plugins/paste/plugin.js');
require('tinymce/plugins/table/plugin.js');
require('tinymce/plugins/advlist/plugin.js');
require('tinymce/plugins/autoresize/plugin.js');
require('tinymce/plugins/lists/plugin.js');
require('tinymce/plugins/code/plugin.js');
require('tinymce/plugins/image/plugin.js');
require('tinymce/plugins/imagetools/plugin.js');
var ContentMenu = (function () {
    function ContentMenu(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._file = null;
        this.serial = 0;
        this._viewsk = 0;
        this.content = 0;
        this._fileName = '';
        this._time = "";
        this._str = "";
        this._roleval = "";
        this._image = "";
        this._urllink = "";
        this.alertTime = "";
        this.flagnew = true;
        this.flagsave = true;
        this.flagaca = true;
        this.flagdelete = true;
        this.flagview = true;
        this.flagcanvas = true;
        this._checkcw = false;
        this._checkeditor = false;
        this._checkpubisher = false;
        this._checkadmin = false;
        this.statusval = 0;
        this.stateval = "";
        this.alert = false;
        this._mflag = false;
        this.uploadDataList = [];
        this.uploadDataListResize = [];
        this.uploadData = { "serial": 0, "name": "", "order": "", "url": "", "desc": "" };
        this._obj = this.getDefaultObj();
        this._result = { "longResult": "", "msgDesc": "", "key": [], "keyResult": 0, "state": "", "stringResult": "" };
        this._util = new rp_client_util_1.ClientUtil();
        this.onEditorKeyup = new core_1.EventEmitter();
        this.lovstatusType = [];
        this._sessionObj = this.getSessionObj();
        this.flag = false;
        this.pushcaption = "";
        this.splicearrindex = -1;
        this.splicearrindex1 = -1;
        this.substitueindex = 0;
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        //event
        jQuery("#flagdelete").hide();
        this.flagview = true;
        this._roleval = ics._profile.t1;
        this.flagsave = false;
        this.flagnew = false;
        this._obj.n7 = 5;
        this._datepickerOpts = this.ics._datepickerOpts;
        this._datepickerOpts.format = 'dd/mm/yyyy';
        this._time = this._util.getTodayTime();
        this._obj.createdTime = this._time;
        this._obj.modifiedTime = this._time;
        this.getStatusType();
    }
    ContentMenu.prototype.getDefaultObj = function () {
        return { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": "", "modifiedDate": "", "modifiedTime": "", "userId": "", "sessionId": "", "userName": "", "modifiedUserId": "", "modifiedUserName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "News", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "temp": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [], "videoUpload": [], "uploadlist": [], "resizelist": [] };
    };
    ContentMenu.prototype.ngOnInit = function () {
        var _this = this;
        var urllink1 = this._urllink;
        tinymce.init({
            selector: 'textarea',
            menubar: false,
            statusbar: false,
            content_css: 'https://afeld.github.io/emoji-css/emoji.css',
            toolbar: "bold | alignleft aligncenter alignright alignjustify",
            plugins: ['link', 'modern', 'paste', 'table', 'image', 'imagetools'],
            file_browser_callback: function (field_name, url, type, win) {
                if (type == 'image')
                    jQuery('#upload input').click();
                win.document.getElementById(field_name).value = urllink1;
            },
            setup: function (editor) {
                _this.editor = editor;
                editor.on('init', function () {
                    editor.setContent(_this._obj.t2);
                });
                editor.on('keyup change', function () {
                    var content = editor.getContent();
                    _this._obj.t2 = content;
                    _this.onEditorKeyup.emit(content);
                });
            },
        });
        //
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "new") {
                _this.content = 1;
                _this.goNew();
                _this.content = 0;
            }
            else if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this.goGet(id);
                _this._viewsk = id;
            }
        });
    };
    ContentMenu.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    ContentMenu.prototype.goGet = function (p) {
        var _this = this;
        this.flagview = false;
        this.flagsave = false;
        this.flagdelete = false;
        this._obj.createdTime = this._time;
        this._obj.modifiedTime = this._time;
        this.http.doGet(this.ics.cmsurl + 'serviceContentNew/readBySyskey?key=' + p + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (data) {
            _this._obj = data;
            if (_this._obj != null) {
                _this.editor.setContent(_this._obj.t2);
                if (_this._obj.uploadlist.length > 0) {
                    _this.uploadDataList = _this._obj.uploadlist;
                    _this.serial = _this.uploadDataList[_this.uploadDataList.length - 1].serial;
                    _this._obj.uploadlist = [];
                }
                if (_this._obj.resizelist.length > 0) {
                    _this.uploadDataListResize = _this._obj.resizelist;
                    _this._obj.resizelist = [];
                }
            }
            else
                _this._obj = _this.getDefaultObj();
        }, function (error) { }, function () { });
    };
    ContentMenu.prototype.goNew = function () {
        this.uploadData = { "serial": 0, "name": "", "order": "", "url": "", "desc": "" };
        this._obj = this.getDefaultObj();
        this._result = { "longResult": "", "msgDesc": "", "key": [], "keyResult": 0, "state": "", "stringResult": "" };
        this.serial = 0;
        this._fileName = "";
        this.uploadDataList = [];
        this.flagview = true;
        this.alertTime = "";
        this._time = this._util.getTodayTime();
        this._obj.createdTime = this._time;
        this._obj.modifiedTime = this._time;
        this._datepickerOpts = this.ics._datepickerOpts;
        this._datepickerOpts.format = 'dd/mm/yyyy';
        this._obj.t5 = this.stateval;
        this._obj.n7 = 5;
        this.flagdelete = true;
        this.flagsave = false;
        jQuery("#flagdelete").hide();
        jQuery("#flagsave").show();
        jQuery("#flagview").show();
        jQuery("#flagImageUpload").show();
        jQuery("#imageUpload").val("");
        jQuery("#flagdelete").val("");
        jQuery("#save").prop("disabled", false);
        if (this.content == 0) {
            this.editor.setContent("");
        }
    };
    ContentMenu.prototype.goPost = function () {
        var _this = this;
        this.flagsave = true;
        this._obj.createdTime = this._time;
        this._obj.modifiedTime = this._time;
        this._obj.uploadlist = this.uploadDataList;
        this._obj.resizelist = this.uploadDataListResize;
        this._obj.t3 = "News";
        if (this.isValidate(this._obj)) {
            if (this._obj.syskey != 0) {
                this._obj.modifiedUserId = this.ics._profile.userID;
                this._obj.modifiedUserName = this.ics._profile.userName;
            }
            this._obj.userId = this.ics._profile.userID;
            this._obj.userName = this.ics._profile.userName;
            this._obj.sessionId = this.ics._profile.sessionID;
            var url = this.ics.cmsurl + 'serviceContentNew/goSave';
            this._obj.n5 = Number(this.ics._profile.t1);
            var json = this._obj;
            console.log("Content Save JSON" + JSON.stringify(json));
            this.http.doPost(url, json).subscribe(function (data) {
                _this._result = data;
                if (data.state) {
                    _this.showMessage(data.msgDesc, true);
                    _this.flagsave = false;
                    _this.flagdelete = false;
                    _this._obj.syskey = data.longResult[0];
                }
                else {
                    _this.showMessage(data.msgDesc, false);
                    _this.flagsave = false;
                }
            }, function (error) {
                _this.showMessage("Can't Saved This Record!", undefined);
                _this.flagsave = false;
            }, function () { });
        }
    };
    ContentMenu.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "primary", "t3": msg });
        }
    };
    ContentMenu.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "", "lovDesc": "" };
    };
    ContentMenu.prototype.getStatusType = function () {
        var _this = this;
        try {
            var url = this.ics.cmsurl + 'serviceAdmLOV/getLovType';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            this._sessionObj.lovDesc = "StatusType";
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.msgCode == "0016") {
                        _this.showMessage(data.msgDesc, false);
                    }
                    if (data.msgCode == "0000") {
                        if (!(data.lovType instanceof Array)) {
                            var m = [];
                            m[0] = data.lovType;
                            _this.ref._lov3.StatusType = m;
                        }
                        else {
                            _this.ref._lov3.StatusType = data.lovType;
                        }
                        _this.ref._lov3.StatusType.forEach(function (iItem) {
                            var l_Item = { "value": "", "caption": "" };
                            l_Item['caption'] = iItem.caption;
                            l_Item['value'] = iItem.value;
                            _this.lovstatusType.push(iItem);
                        });
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    ContentMenu.prototype.goDelete = function () {
        var _this = this;
        var url = this.ics.cmsurl + 'serviceContentNew/goDelete';
        this._obj.sessionId = this.ics._profile.sessionID;
        this._obj.userId = this.ics._profile.userID;
        var json = this._obj;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data.state) {
                _this.goNew();
                _this.showMessage(data.msgDesc, true);
            }
            else {
                _this.showMessage(data.msgDesc, false);
            }
        }, function (error) { }, function () { });
    };
    ContentMenu.prototype.goList = function () {
        this._router.navigate(['/contentmenulist']);
    };
    ContentMenu.prototype.goView = function (p) {
        this._router.navigate(['/eduView', 'read', p]);
    };
    ContentMenu.prototype.uploadedFile = function (event, type) {
        var _this = this;
        this.flagcanvas = true;
        if (event.target.files.length == 1) {
            this._fileName = event.target.files[0].name;
            this._file = event.target.files[0];
            var index = this._fileName.lastIndexOf(".");
            var imagename = this._fileName.substring(index);
            imagename = imagename.toLowerCase();
            if (imagename == ".jpg" || imagename == ".jpeg" || imagename == ".png") {
                jQuery("#imagepopup").modal();
                var url = this.ics.cmsurl + 'fileAdm/fleUploadNew?f=upload&fn=' + this._fileName + '&id=' + this.ics._profile.t1 + '&type=0&imgUrl=' + this.ics._imgurl;
                this.http.upload(url, this._file).subscribe(function (data) {
                    jQuery("#imagepopup").modal('hide');
                    if (data.code === 'SUCCESS') {
                        _this.showMessage("Upload Successful", true);
                        var _img = _this._fileName;
                        _this.uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
                        _this.uploadData.name = data.fileName;
                        _this.uploadData.serial = ++_this.serial;
                        _this.uploadData.url = data.url;
                        _this.uploadDataList.push(_this.uploadData);
                        _this.uploadDataListResize.push(data.sfileName);
                        _this._fileName = "";
                    }
                    else {
                        _this.showMessage("Upload Unsuccessful  Please Try Again...", false);
                    }
                }, function (error) { jQuery("#imagepopup").modal('hide'); }, function () { });
            }
            else {
                this.showMessage("Choose Image Associated!", undefined);
            }
        }
        else {
        }
    };
    ContentMenu.prototype.isValidate = function (obj) {
        if (obj.t1 == "") {
            this.showMessage("Please fill Title!", false);
            this.flagsave = false;
            return false;
        }
        else if (obj.t1.length > 500) {
            this.showMessage("Title is exceed limited 500 characters count!", false);
            this.flagsave = false;
            return false;
        }
        else if (obj.t2 == "") {
            this.showMessage("Please fill Content!", false);
            this.flagsave = false;
            return false;
        }
        return true;
    };
    ContentMenu.prototype.setImgUrl = function (str) {
        console.log("" + JSON.stringify(this.ics._imgurl + 'upload/smallImage/contentImage/' + str));
        return this.ics._imgurl + 'upload/smallImage/contentImage/' + str;
    };
    /* goView(p) {
        this._router.navigate(['/eduView', 'read', p]);
    } */
    ContentMenu.prototype.remove = function (index) {
        this.uploadDataList.splice(index, 1);
    };
    ContentMenu.prototype.deleteFile = function (obj) {
        var _this = this;
        this.http.doGet(this.ics.cmsurl + 'fileAdm/fileRemoveContent?fn=' + obj.name + '&url=' + obj.url).subscribe(function (data) {
            if (data.code === 'SUCCESS') {
                var index = _this.uploadDataList.indexOf(obj);
                _this.uploadDataList.splice(index, 1);
                _this.showMessage("Photo Remove!", true);
                jQuery("#imageUpload").val("");
            }
        }, function (error) { }, function () { });
    };
    ContentMenu.prototype.onDragStart = function (event, i, p, subarrname) {
        this.subarr = subarrname;
        this.pushcaption = p.name;
        this.splicearrindex1 = i;
        this.splicearrindex = -1;
    };
    ContentMenu.prototype.allowDrop = function (event) {
        event.preventDefault();
    };
    ContentMenu.prototype.goSubstitute = function (event, p) {
        this.substitueindex = p;
    };
    ContentMenu.prototype.onDrop = function (event, i, p, arrname) {
        if (this.subarr.length > 0) {
            var sub = this.subarr[this.splicearrindex1];
            this.subarr.splice(this.splicearrindex1, 1);
            arrname.splice(this.substitueindex, 0, sub);
        }
    };
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], ContentMenu.prototype, "onEditorKeyup", void 0);
    ContentMenu = __decorate([
        core_1.Component({
            selector: 'contenntmenu',
            template: "\n<div class=\"container-fluid\" >\n    <div class=\"row clearfix\">\n\t\t<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n\t\t\t<form class= \"form-horizontal\" ngNoForm> \n\t\t\t<legend>Content </legend>\n\t\t\t<div class=\"cardview list-height\">  \n\t\t\t\t<div class=\"row col-md-12\" style=\"margin-bottom: 10px;\">\n\t\t\t\t\t<div  style=\"float: left;\"> \n\t\t\t\t\t    <button class=\"btn btn-sm btn-primary\"  type=\"button\"   (click)=\"goList()\">List</button>\n\t\t\t\t\t\t<button class=\"btn btn-sm btn-primary\" [disabled]=\"flagnew\" id=\"flagnew\" type=\"button\" (click)=\"goNew()\">New</button>      \n\t\t\t\t\t\t<button class=\"btn btn-sm btn-primary\" [disabled]=\"flagsave\" type=\"button\" (click)=\"goPost()\">Save</button> \n                        <button class=\"btn btn-sm btn-primary\" [disabled]=\"flagdelete\" id=\"delete\" type=\"button\" (click)=\"goDelete();\" >Delete</button>\t\t\t\t\t   \n                       <!-- <button *ngIf=\"!flagview\" class=\"btn btn-sm btn-primary\"  type=\"button\"  (click)=\"goView(_viewsk);\" >View</button> -->\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t\n\t\t\t\t<div class=\"row col-md-12\" style=\"padding-left: 30px;\">\t\t\t\t\n\t\t\t\t\t<div class=\"form-group\" >\n\t\t\t\t\t\t<label class=\"col-md-2\">Title</label>\n\t\t\t\t\t\t<div class=\"col-md-4\">\n\t\t\t\t\t\t\t<input  type=\"text\" [(ngModel)]=\"_obj.t1\" class=\"form-control input-md uni\"/>\t\t\t\t\n\t\t\t\t\t\t</div>\t\t\n\t\t\t\t\t\t<label class=\"col-md-2\">Status</label>\n\t\t\t\t\t\t<div class=\"col-md-4\">\n\t\t\t\t\t\t\t<select [(ngModel)]=\"_obj.n7\" class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\" required>\n\t\t\t\t\t\t\t<option *ngFor=\"let c of lovstatusType\" value=\"{{c.value}}\">{{c.caption}}</option>\n\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\t\t     \n\t\t\t\t\t\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-md-2\">Content</label>\n\t\t\t\t\t\t<div class=\"col-md-10\">\n\t\t\t\t\t\t<div class=\"uni\">\n\t\t\t\t\t\t<textarea class=\"form-control input-md uni\" [(ngModel)]=\"_obj.t2\" rows=\"10\" ></textarea>\n\t\t\t\t\t\t\t<input name=\"image\" type=\"file\" id=\"upload\" class=\"hidden\" onchange=\"\">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\n\t\t\t\t\t<div class=\"form-group\" id=\"flagImageUpload\">\n\t\t\t\t\t\t<label class=\"col-md-2\">Image Associated (jpeg, png, jpg)</label>\n\t\t\t\t\t\t<div class=\"col-md-4\">\n\t\t\t\t\t\t\t<input type=\"file\" id=\"imageUpload\" class=\"form-control input-md\" (change)=\"uploadedFile($event,_obj.n4)\" placeholder=\"Upload file...\" />\n\t\t\t\t\t\t</div>     \n\t\t\t\t\t</div>\n\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<div *ngFor=\"let img of _obj.videoUpload;let num = index\">\n\t\t\t\t\t\t\t<video id=\"{{img}}\" controls style=\"display:none\" height=\"240\" width=\"400\" type=\"video/mp4\">\n\t\t\t\t\t\t\t\t<source src=\"{{setImgUrl(img)}}\" onError=\"this.src='./image/image_not_found.png';\" type=\"video/mp4\" />  \n\t\t\t\t\t\t\t</video>                                \n\t\t\t\t\t\t<canvas id=\"canvas\" style=\"display:none\" ></canvas> \n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\n\t\t\t\t\t<div class=\"col-md-3 col-sm-4 col-xs-6\" style=\"padding-right: 1px;\" *ngFor=\"let obj of uploadDataList, let i=index\"\n\t\t\t\t\t(dragstart)=\"onDragStart($event, i, obj, uploadDataList)\" draggable =\"true\" (dragover)=\"allowDrop($event)\"\n\t\t\t\t\t(dragover)=\"goSubstitute(event,i)\" (drop)=\"onDrop($event, i, obj, uploadDataList)\">\n\t\t\t\t\t\t<div id=\"img_container\">\n\t\t\t\t\t\t<img src=\"{{setImgUrl(obj.name)}}\" onError=\"this.src='./image/image_not_found.png';\" alt={{obj.name}} title={{obj.name}} style=\"margin-top:20px;width:80%;height:200px;overflow: hidden;object-position: 25% 50%;transition: 1s width, 1s height;border-radius:5px;object-fit:cover;\"/>\n\t\t\t\t\t\t\t<button  class=\"button_over_img\" type=\"button\" title=\"Remove Photo\" (click)=\"deleteFile(obj)\">\n\t\t\t\t\t\t\t\t<span  class=\"glyphicon glyphicon-remove\"></span>\n\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t<textarea class=\"form-control input-md uni\" [(ngModel)]=\"obj.desc\" rows=\"3\" style=\"width:220px;margin-top:10px;\" placeholder=\"Type Image Description\"></textarea>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"form-group\">&nbsp;</div>\n\t\t\t</div>\n\t\t\t</form>\n\t\t</div>\n    </div>\n</div>\n    <!-- processing image modal -->\n    <div id=\"imagepopup\" class=\"modal fade\" role=\"dialog\" style=\" margin-top: 200px; margin-left: 150px;\">\n        <div id=\"imagepopupsize\" class=\"modal-dialog modal-lg\" style=\"width : 240px\">\n            <div class=\"modal-content\">\n                <div id=\"imagepopupbody\" class=\"modal-body\">\n                    <img src=\"image/processing.gif\" style=\"padding-top:30px;\">\n                </div>\n            </div>\n        </div>\n    </div>\n"
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], ContentMenu);
    return ContentMenu;
}());
exports.ContentMenu = ContentMenu;
//# sourceMappingURL=contentmenu.component.js.map