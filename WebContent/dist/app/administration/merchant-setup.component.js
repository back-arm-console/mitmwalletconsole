"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_references_1 = require('../framework/rp-references');
core_1.enableProdMode();
var MerchantSetup = (function () {
    //isGL: boolean;
    function MerchantSetup(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._features = [];
        this._selectref = [];
        this._obj = this.getDefaultObj();
        this._note = "";
        this.confirmpwd = "";
        this._output1 = "";
        this._returnResult = { "keyResult": 0, "longResult": "", "state": "", "stringResult": "", "userId": "", "msgCode": "", "msgDesc": "" };
        //_returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
        this._brCode = { "startBrCode": 0, "endBrCode": 0 };
        this._mID = "";
        this._featurevalue = "";
        this._featurecaption = "";
        this._ordervalue = "";
        this._ordercaption = "";
        this.hasAcc = false;
        //_obj = { "sessionID": "", "msgCode": "", "msgDesc": "", "sysKey": 0, "t1": "TBA", "t3": "", "t4": "", "t5": "", "t6": "", "t11": "", "t12": "", "userName": "", "userId": "", "p1": "AA", "accountNumber": "", "branchname": "", "createdUserID": "", "branchCode": "", "processingCode": "", "chkAccount": "false", "chkGL": "false", "feature": "", "data": [] };
        //_list = { "data": [{ "feature": "", "showfeature": "", "accountNumber": "", "branchCode": "", "order": "" }] };
        //_showlist = { "data": [{ "feature": "", "accountNumber": "", "branchCode": "","order":"" }] };
        this._customer = { "customerID": "", "name": "", "nrc": "", "accountNo": "", "balance": "", "message": "", "glDesp": "", "chkAccount": "false", "chkGL": "false", "other": "false" };
        this._obj1 = { "a1": "admin" };
        this._key = "";
        this._ans = "";
        this.catch = "";
        this.message = "";
        this.isvalidate = "";
        this._classname = "";
        this._mflag = false;
        //uploadDataListResize = [];
        this.uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
        this.myimg = null;
        this._fileName = '';
        this._size = 0;
        this.temp = "";
        this._file = null;
        this.flagImg = false;
        this._userobj = {
            "data": [{ "syskey": 0, "userId": "", "userName": "", "recordStatus": 0, "t1": "", "username": "", "n7": 0, "t7": "" }],
            "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0, "msgCode": "", "msgDesc": ""
        };
        this._pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 0 }; //atn
        /* goAddAll(p) {
          this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "", "msgCode": "" };
      
          if (this.isvalidate == "") {
            this._returnResult.msgDesc = "Please check Account Number/GL again";
            this.showMsg(this._returnResult.msgDesc,false);
            return false;
          }
      
          if (this._obj.accountNumber == '') {
            this._returnResult.msgDesc = "Please Fill Account Number";
            this.showMsg(this._returnResult.msgDesc,false);
            return false;
          }
      
          if (this._obj.feature == '' || this._obj.feature == 'undefined') {
            this._returnResult.msgDesc = "Please Select Feature";
            this.showMsg(this._returnResult.msgDesc,false);
            return false;
          }
      
          if (this._obj.branchCode == '' || this._obj.branchCode == 'undefined') {
            this._returnResult.msgDesc = "Please Select Branch Code";
            this.showMsg(this._returnResult.msgDesc,false);
            return false;
          }
      
          if (this._customer.chkAccount) {
            let accbrcode = this._obj.accountNumber.substring(this._brCode.startBrCode, this._brCode.endBrCode)
      
            if (accbrcode != this._obj.branchCode) {
              this._returnResult.msgDesc = "Invalid Branch Code";
              this.showMsg(this._returnResult.msgDesc,false);
              return false;
            }
          }
      
          if (this._list.data.length > 0) {
            for (let i = 0; i < this._list.data.length; i++) {
              if (this._obj.feature == this._list.data[i].feature) {
                this._returnResult.msgDesc = "Duplicate Feature";
                this.showMsg(this._returnResult.msgDesc,false);
                return false;
              }
            }
          }
      
          if (this._returnResult.msgDesc == '') {
            if (this._obj.feature != "") {
              for (let i = 0; i < this.ref._lov3.refFeature.length; i++) {
                if (this._obj.feature == this.ref._lov3.refFeature[i].value) {
                  this._featurecaption = this.ref._lov3.refFeature[i].caption;
                  break;
                }
              }
            }
      
            if (this._obj.order != "") {
              for (let i = 0; i < this.ref._lov3.refOrder.length; i++) {
                if (this._obj.order == this.ref._lov3.refOrder[i].value) {
                  this._ordercaption = this.ref._lov3.refOrder[i].caption;
                  break;
                }
              }
            }
      
            //this._showlist.data.push({ "feature": this._featurecaption, "accountNumber": this._obj.accountNumber, "branchCode": this._obj.branchCode,"order":this._ordercaption });
            this._list.data.push({ "feature": this._obj.feature, "showfeature": this._featurecaption, "accountNumber": this._obj.accountNumber, "branchCode": "02", "order": "1" });//this._obj.branchCode
            this.isvalidate = '';
            this._obj.accountNumber='';
            this._obj.branchCode='';
            this._obj.feature='';
            this.isGL=false;
      
          } else {
            this.showMsg(this._returnResult.msgDesc,false);
          }
      
        }  */
        /* goRemove(i) {
          let sindex = this._list.data.indexOf(i);
          this._list.data.splice(sindex, 1);
        } */
        /* checkSession() {
          try {
            let url: string = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
            this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "TBA", "msgCode": "" };
      
            this.http.doGet(url).subscribe(
              data => {
                if (data.msgCode == '0016') {
                  this._returnResult.msgDesc = data.msgDesc;
                  this.showMessage();
                }
              },
              error => {
                if (error._body.type == 'error') {
                  alert("Connection Timed Out!");
                }
              },
              () => { }
            );
          } catch (e) {
            alert(e);
          }
        } */
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        // RP Framework 
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = false;
            this.ics.confirmUpload(false);
            this._obj = this.getDefaultObj();
            this.checkSession();
            this.getBrCode();
            this.getAllBranch();
            this.getAllProcessingCode();
            this.getAllFeature();
            this.messagehide = true;
            jQuery("#mydelete").prop("disabled", true);
            jQuery("#mySave").prop("disabled", true);
            this.msghide = true;
        }
    }
    MerchantSetup.prototype.showMessageAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    MerchantSetup.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this._key = id;
                _this.goReadBysysKey(id);
            }
        });
    };
    MerchantSetup.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    MerchantSetup.prototype.setImgUrl = function (str) {
        console.log("" + JSON.stringify(this.ics._imgurl + '/upload/smallImage/merchantImage/' + str));
        return this.ics._imgurl + '/upload/smallImage/merchantImage/' + str;
    };
    MerchantSetup.prototype.uploadedFileImage = function (event) {
        var _this = this;
        this._mflag = true;
        // this.uploadDataListResize = [];
        if (event.target.files.length == 1) {
            /*  if ((this._obj.uploadlist[0] != null)) {
               this.myimg = event.target.files[0];
               jQuery("#sessionalert").modal();
             } else { */
            this._fileName = event.target.files[0].name;
            this._size = event.target.files[0].size;
            if (this._size >= 1048576) {
                this.temp = (this._size / 1048576).toFixed(2) + " MB";
            }
            else if (this._size >= 1024) {
                this.temp = (this._size / 1024).toFixed(2) + " KB";
            }
            else if (this._size > 1) {
                this.temp = this._size + " bytes";
            }
            else if (this._size == 1) {
                this.temp = this._size + " byte";
            }
            else {
                this.temp = "0 bytes";
            }
            this._file = event.target.files[0];
            var index = this._fileName.lastIndexOf(".");
            var imagename = this._fileName.substring(index);
            imagename = imagename.toLowerCase();
            if (imagename == ".jpg" || imagename == ".jpeg" || imagename == ".png") {
                console.log("imgUrl: " + this.ics._imgurl);
                var url = this.ics.cmsurl + 'fileAdm/fileupload?f=upload&fn=' + this._fileName + '&id=' + this.ics._profile.t1 + '&type=11' + '&imgUrl=' + this.ics._imgurl;
                this.http.upload(url, this._file).subscribe(function (data) {
                    if (data.code === 'SUCCESS') {
                        _this.flagImg = true;
                        _this.uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
                        _this.uploadData.name = data.fileName;
                        _this.uploadData.url = data.url;
                        //this._obj.uploadlist[0] = this.uploadData;
                        //this.uploadDataListResize.push(data.sfileName);
                        _this._obj.t13 = _this._fileName;
                        _this._obj.t14 = _this.temp;
                        _this.showMsg("Upload Successful", true);
                    }
                    else {
                        _this.showMsg("Upload Unsuccessful Please Try Again...", false);
                    }
                    _this._mflag = true;
                }, function (error) {
                    if (error._body.type == 'error') {
                        alert("Connection Timed Out!");
                    }
                    else {
                    }
                }, function () { });
            }
            else {
                this._mflag = true;
                jQuery("#imageUpload").val("");
                this.showMsg("Choose Image Associated", false);
            }
        }
    };
    MerchantSetup.prototype.goReadBysysKey = function (p) {
        var _this = this;
        try {
            this._key = p;
            var json = this._key;
            var url = this.ics.cmsurl + 'serviceCMS/getMerchantDataByID?id=' + this._key + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
            this.http.doGet(url).subscribe(function (data) {
                if (data != null) {
                    _this._obj = data;
                    _this._obj.chk = data.t12;
                    for (var i = 0; i < _this.ref._lov3.refFeature.length; i++) {
                        for (var j = 0; j < data.data.length; j++) {
                            if (data.data[j].feature == _this.ref._lov3.refFeature[i].value) {
                                _this.ref._lov3.refFeature[i].value = data.data[j].feature;
                                break;
                            }
                        }
                    }
                }
                _this._output1 = JSON.stringify(data);
                _this.confirmpwd = data.t2;
                if (_this._obj.msgCode == '0016') {
                    _this.showMsg(_this._obj.msgDesc, false);
                }
                _this.hasAcc = true;
                jQuery("#mydelete").prop("disabled", false);
                jQuery("#mySave").prop("disabled", false);
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    MerchantSetup.prototype.getDefaultObj = function () {
        return { "uploadlist": [], "sessionID": "", "msgCode": "", "msgDesc": "", "sysKey": 0, "t1": "TBA", "t3": "", "t4": "", "t5": "", "t6": "", "t11": "", "t12": "", "userName": "", "userId": "", "p1": "AA", "accountNumber": "", "branchname": "", "createdUserID": "", "branchCode": "002", "processingCode": "", "chkAccount": "false", "chkGL": "false", "feature": "Topup", "order": "", "chk": "0", "t13": "", "t14": "" }; // "data": [],
    };
    MerchantSetup.prototype.checkGL = function (event) {
        if (event.target.checked) {
            // this.isGL = true;
            this._obj.accountNumber = '';
            jQuery("#mySave").prop("disabled", true);
            this.isvalidate = "";
        }
        else {
            //this.isGL = false;
            this._obj.accountNumber = '';
            jQuery("#mySave").prop("disabled", true);
            this.isvalidate = "";
        }
    };
    MerchantSetup.prototype.goDelete = function () {
        var _this = this;
        try {
            this._mflag = false;
            var url = this.ics.cmsurl + 'serviceCMS/deleteMerchant';
            this._obj.sessionID = this.ics._profile.sessionID;
            this._obj.createdUserID = this.ics._profile.userID;
            var json = this._obj;
            this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "TBA", "msgCode": "" };
            this.http.doPost(url, json).subscribe(function (data) {
                _this._output1 = JSON.stringify(data);
                _this._obj.userId = data.userId;
                _this._returnResult = data;
                if (_this._returnResult.state) {
                    //this._obj = { "sessionID": "", "msgCode": "", "msgDesc": "", "sysKey": 0, "t1": "TBA", "t3": "", "t4": "", "t5": "", "t6": "", "t11": "", "t12": "", "userName": "", "userId": "", "p1": "AA", "accountNumber": "", "branchname": "", "createdUserID": "", "branchCode": "", "processingCode": "", "chkAccount": "false", "chkGL": "false", "feature": "", "order": "","chk":"0","t13":"","t14":"" };// "data": [],
                    _this._obj = _this.getDefaultObj();
                    jQuery("#mydelete").prop("disabled", true);
                    jQuery("#mySave").prop("disabled", false);
                    _this.isvalidate = "";
                    _this.showMsg(_this._returnResult.msgDesc, true);
                    _this._selectref = [];
                }
                else {
                    if (_this._returnResult.msgCode == '0016') {
                        _this._returnResult.msgDesc = _this._returnResult.msgDesc;
                        _this.showMsg(_this._returnResult.msgDesc, false);
                    }
                    else {
                        _this.showMsg(_this._returnResult.msgDesc, false);
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
            this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "", "msgCode": "" };
        }
        catch (e) {
            alert(e);
        }
    };
    MerchantSetup.prototype.validateEmail = function (mail) {
        if (/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(mail)) {
            return (true);
        }
        return (false);
    };
    MerchantSetup.prototype.goPost = function () {
        var _this = this;
        this._mflag = false;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "", "msgCode": "" };
        if (!/^([0-9]{7,20})$/.test(this._obj.t4)) {
            this._returnResult.msgDesc = "Contact number is invalid";
            this.showMsg(this._returnResult.msgDesc, false);
            this._mflag = true;
            return false;
        }
        if (this._obj.accountNumber == '') {
            this._returnResult.msgDesc = "Please Fill Account Number";
            this.showMsg(this._returnResult.msgDesc, false);
            this._mflag = true;
            return false;
        }
        if (this._obj.feature == '' || this._obj.feature == 'undefined') {
            this._returnResult.msgDesc = "Please Select Feature";
            this.showMsg(this._returnResult.msgDesc, false);
            this._mflag = true;
            return false;
        }
        if (this._obj.branchCode == '' || this._obj.branchCode == 'undefined') {
            this._returnResult.msgDesc = "Please Select Branch Code";
            this.showMsg(this._returnResult.msgDesc, false);
            this._mflag = true;
            return false;
        }
        if (this._obj.t5.length > 0) {
            if (!this.validateEmail(this._obj.t5)) {
                this._returnResult.msgDesc = "Email address is invalid";
                this.showMsg(this._returnResult.msgDesc, false);
                this._mflag = true;
                return false;
            }
        }
        if (this._returnResult.msgDesc == '') {
            try {
                this._obj.createdUserID = this.ics._profile.userID;
                this._obj.sessionID = this.ics._profile.sessionID;
                this._mID = this._obj.userId;
                var url = this.ics.cmsurl + 'serviceCMS/saveMerchant';
                var json = this._obj;
                this.http.doPost(url, json).subscribe(function (data) {
                    _this._output1 = JSON.stringify(data);
                    _this._obj.userId = data.userId;
                    _this._returnResult = data;
                    _this.isvalidate = "";
                    if (!(_this._returnResult.state)) {
                        if (_this._returnResult.msgCode == '0016') {
                            _this.showMsg(_this._returnResult.msgDesc, false);
                        }
                        _this.isvalidate = "";
                        _this._obj.sysKey = _this._returnResult.keyResult;
                        _this._obj.userId = _this._mID;
                        _this.message = "";
                        _this._key = _this._returnResult.keyResult + "";
                    }
                    else {
                        jQuery("#mydelete").prop("disabled", false);
                        _this.showMsg(_this._returnResult.msgDesc, true);
                    }
                    _this._mflag = true;
                }, function (error) {
                    if (error._body.type == 'error') {
                        alert("Connection Timed Out!");
                    }
                }, function () { });
            }
            catch (e) {
                alert(e);
            }
        }
        else {
            this.showMsg(this._returnResult.msgDesc, false);
        }
    };
    MerchantSetup.prototype.goList = function () {
        this._router.navigate(['/Merchantlist']);
    };
    MerchantSetup.prototype.goNew = function () {
        this.clearData();
    };
    MerchantSetup.prototype.clearData = function () {
        this._obj = this.getDefaultObj();
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "", "msgCode": "" };
        jQuery("#mydelete").prop("disabled", true);
        this.isvalidate = "";
        //this.isGL = false;
        jQuery("#mySave").prop("disabled", true);
        this._selectref = [];
    };
    MerchantSetup.prototype.getAllProcessingCode = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics.cmsurl + 'serviceAdmLOV/getAllProcessingCode?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (data) {
                if (data != null && data != undefined) {
                    _this.ref._lov3.refProcessingCode = data.refProcessingCode;
                }
                else {
                    _this.ref._lov3.refProcessingCode = [{ "value": "", "caption": "-" }];
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    MerchantSetup.prototype.getBrCode = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics.cmsurl + 'serviceCMS/getBrCode?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (data) {
                if (data != null) {
                    _this._brCode = data;
                }
                else {
                    _this._brCode = { "startBrCode": 0, "endBrCode": 0 };
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    MerchantSetup.prototype.getAllBranch = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics.cmsurl + 'serviceCMS/getAllBranch').subscribe(function (data) {
                if (data != null && data != undefined) {
                    _this.ref._lov3.ref018 = data.ref018;
                }
                else {
                    _this.ref._lov3.ref018 = [{ "value": "", "caption": "Empty" }];
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    MerchantSetup.prototype.changedPager = function (event) {
        if (this._userobj.totalCount != 0) {
            this._pgobj = event;
            var current = this._userobj.currentPage;
            var size = this._userobj.pageSize;
            this._userobj.currentPage = this._pgobj.current;
            this._userobj.pageSize = this._pgobj.size;
            if (this._pgobj.current != current || this._pgobj.size != size) {
                this.goPopupList();
            }
        }
    };
    MerchantSetup.prototype.goPopupList = function () {
        var _this = this;
        try {
            this._mflag = false;
            this.http.doGet(this.ics.cmsurl + 'service001/showAccountList?searchVal=' + encodeURIComponent(this._userobj.searchText) + '&pagesize=' + this._userobj.pageSize + '&currentpage=' + this._userobj.currentPage + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID + '&chk=' + this._obj.chk).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this.showMsg(data.msgDesc, false);
                }
                else {
                    if (data != null && data != undefined) {
                        _this._pgobj.totalcount = data.totalCount;
                        _this._userobj = data;
                        if (_this._userobj.totalCount == 0) {
                            _this.showMsg("Data not found!", false);
                        }
                    }
                    jQuery("#lu001popup2").modal();
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    MerchantSetup.prototype.gotoWLAccount = function (data) {
        this._obj.accountNumber = data;
        this.hasAcc = true;
        jQuery("#lu001popup2").modal('hide');
    };
    /*  goCheck() {
       this._mflag = false;
       try {
         let p = this._obj.accountNumber;
         let check = '';
         if (this.isGL) {
           check = 'GL';
         } else {
           check = 'Account';
         }
         this.http.doGet(this.ics.cmsurl + 'service001/getCustomerNameandNrcByAccount?account=' + p + '&check=' + check +'&userID='+ this.ics._profile.userID +'&sessionID='+ this.ics._profile.sessionID).subscribe(
           data => {
             if (data.msgCode == '0000') {
               this._customer = data;
               this._obj.chkAccount = this._customer.chkAccount;
               this._obj.chkGL = this._customer.chkGL;
               this._obj.accountNumber = this._obj.accountNumber.trim();
               //this._list.data.push(null);
               if (this._customer.chkAccount) {
                 jQuery("#mySave").prop("disabled", false);
                 jQuery("#lu001popup2").modal();
                 this.isvalidate = "Validate";
               } else if (this._customer.chkGL) {
                 jQuery("#mySave").prop("disabled", false);
                 jQuery("#glpopup").modal();
                 this.isvalidate = "Validate";
               }
             } else {
               this._returnResult.msgDesc = "Invalid Account Number/GL";
               this.isvalidate = "";
               this.showMsg(this._returnResult.msgDesc,false);
               // this.messagealert();
             }
             this._mflag = true;
           },
           error => {
             if (error._body.type == 'error') {
               alert("Connection Timed Out!");
             } else {
             }
           },
           () => { }
         );
       } catch (e) {
         alert(e);
       }
     } */
    MerchantSetup.prototype.getOrderList = function () {
        var _this = this;
        try {
            this._mflag = false;
            this.http.doGet(this.ics.cmsurl + 'serviceAdmLOV/getOrderList').subscribe(function (data) {
                if (data != null && data != undefined) {
                    if (!(data.refOrder instanceof Array)) {
                        var m = [];
                        m[0] = data.refOrder;
                        _this.ref._lov3.refOrder = m;
                    }
                    else {
                        _this.ref._lov3.refOrder = data.refOrder;
                    }
                }
                else {
                    _this.ref._lov3.refOrder = [{ "value": "", "caption": "-" }];
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    MerchantSetup.prototype.getAllFeature = function () {
        var _this = this;
        try {
            this._mflag = false;
            this.http.doGet(this.ics.cmsurl + 'serviceAdmLOV/getAllFeatures').subscribe(function (data) {
                if (data != null && data != undefined) {
                    if (!(data.refFeature instanceof Array)) {
                        var m = [];
                        m[0] = data.refFeature;
                        _this.ref._lov3.refFeature = m;
                    }
                    else {
                        _this.ref._lov3.refFeature = data.refFeature;
                    }
                }
                else {
                    _this.ref._lov3.refFeature = [{ "value": "", "caption": "-" }];
                }
                _this._obj.feature = _this.ref._lov3.refFeature[0].value;
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    MerchantSetup.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    MerchantSetup.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsg(data.desc, false);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsg(data.desc, false);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    MerchantSetup.prototype.radioChecked = function () {
        this._obj.accountNumber = "";
        this._userobj.searchText = "";
    };
    MerchantSetup.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    MerchantSetup.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    MerchantSetup.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    MerchantSetup = __decorate([
        core_1.Component({
            selector: 'merchantprofile',
            template: "\n  <div class=\"container-fluid\">\n  <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n      <form class= \"form-horizontal\" > <!--(ngSubmit)=\"goPost()\"-->\n        <!-- Form Name -->\n        <legend>Merchant</legend>\n\n        <div class=\"cardview list-height\">\n\n        <div class=\"row  col-md-12\">    \n          <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goList()\">List</button>\n          <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goNew()\">New</button>      \n          <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goPost()\">Save</button>\n          <button class=\"btn btn-sm btn-primary\" disabled id=\"mydelete\" type=\"button\" (click)=\"goDelete();\">Delete</button> \n        </div>\n        <div class=\"row col-md-12\">&nbsp;</div>\n        <div class=\"col-md-12\" id=\"custom-form-alignment-margin\">\n          <div  class=\"col-md-6\">\n      <div class=\"form-group\">\n              <rp-input [(rpModel)]=\"_obj.userId\" rpRequired =\"true\" rpType=\"text\" rpLabelRequired='true' rpLabel=\"Merchant ID\" rpReadonly=\"true\" rpClass=\"col-md-8 control-label\" rpLabelClass=\"col-md-4\"></rp-input>\n      </div>\n      <div class=\"form-group\" style=\"margin-top:-7;\">\n              <rp-input rpType=\"text\" rpLabel=\" Company Name\" [(rpModel)]=\"_obj.userName\" rpLabelRequired='true' rpReadonly=\"false\" rpClass=\"col-md-8 control-label\" rpLabelClass=\"col-md-4\" rpRequired=\"true\"></rp-input>\n            </div>      \n      <div class=\"form-group\">\n              <label class=\"col-md-4\" align=\"left\">Feature&nbsp; <font class=\"mandatoryfont\">*</font></label>\n              <div class=\"col-md-8\" > \n                <select [(ngModel)]=\"_obj.feature\"  class=\"form-control col-md-0\" [ngModelOptions]=\"{standalone: true}\">\n                  <option *ngFor=\"let item of ref._lov3.refFeature\" value=\"{{item.value}}\">{{item.caption}}</option>\n                </select> \n              </div> \n      </div>\n      <div class=\"form-group\">\n              <label class=\"col-md-4\" align=\"left\"> Branch Code <font size=\"4\" color=\"#FF0000\">*</font></label>\n              <div class=\"col-md-8\">\n                <select [(ngModel)]=\"_obj.branchCode\" class=\"form-control col-md-0\" [ngModelOptions]=\"{standalone: true}\">\n                  <option *ngFor=\"let item of ref._lov3.ref018\" value=\"{{item.value}}\">{{item.value}}</option> \n                </select>                \n              </div> \n            </div>\n\t\t<div class=\"form-group\">\n        <label class=\"col-md-4\"> Account Number/GL </label>\n        <div class=\"col-md-8\">\n        <div class=\"input-group\">\n          <input type=\"text\" [(ngModel)]='_obj.accountNumber' [ngModelOptions]=\"{standalone: true}\" class=\"form-control col-md-0\" readonly><!--*ngIf=\"isvalidate != ''\"-->\n          <span class=\"input-group-btn\">\n          <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goPopupList()\" style='height:34px'>&equiv;</button>&nbsp;\n          <!--<button class=\"btn btn-sm btn-primary\" id=\"myadd\" style=\"margin-left:5px\" (click)=\"goAddAll()\">Add</button>\t\t\t\t\t\t-->\n          </span>              \n        </div>\n        </div>\t\t\t\t  \n      </div>\n\n      <!--<div class=\"form-group\">\n        <label class=\"col-md-4\" >  &nbsp; &nbsp;&nbsp;</label>\n        <div class=\"col-md-8\">\n          <input type = \"checkbox\" [(ngModel)]=isGL [ngModelOptions]=\"{standalone: true}\" (change)=\"checkGL($event)\"> &nbsp; Is GL \n          <label style=\"color:green;font-size:15px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{isvalidate}}</label>                      \n        </div> \n      </div>-->\n      <div class=\"form-group\">\n      <label class=\"col-md-4\" >  &nbsp; &nbsp;&nbsp;</label>\n        <div class=\"col-md-8\">\n        <label class=\"radio-inline\">\n          <input #m [checked]=\"_obj.chk == m.value\" (click)=\"_obj.chk = m.value\" name=\"chk\" value=\"0\" type=\"radio\" (change)=\"radioChecked()\"> GL\n        </label>\n        <label class=\"radio-inline\">\n          <input  #c [checked]=\"_obj.chk == c.value\" (click)=\"_obj.chk = c.value\" name=\"chk\" value=\"1\" type=\"radio\" (change)=\"radioChecked()\"> Wallet\n        </label>\n        </div>\n      </div>\n      \n      <div class=\"form-group\">\n        <label class=\"col-md-4\" for=\"imageUploadid\"> Image Associated</label>\n          <div class=\"col-md-8\" id=\"imageUploadid\">\n            <input type=\"file\" id=\"imageUpload\"  class=\"file\" (change)=\"uploadedFileImage($event)\"/>\n          \t\t<div class=\"input-group\">\n          \t\t<input type=\"text\" id=\"imageUpload1\" class=\"form-control input-sm\" placeholder=\"No file chosen\" maxlength=\"100\">\n          \t\t<span class=\"input-group-btn\">\n          \t\t<button class=\"browse btn btn-sm btn-primary\" type=\"button\"><i class=\"glyphicon glyphicon-search\"></i> Browse</button>                         \n          \t\t</span>\n          \t\t</div>\n          \t\t<div class=\"col-md-4\">(jpeg, png, jpg) </div> \n          </div>\n      </div>\n      <div class=\"form-group\">\n        <label class=\"col-md-4\"> &nbsp; &nbsp;&nbsp;</label>\n        <div calss=\"col-md-8\" *ngIf=\"_obj.t13!=''\">\t\n          <img src=\"{{setImgUrl(_obj.t13)}}\" onError=\"this.src='./image/image_not_found.png';\" alt={{img}} height=\"240\" width=\"400\"/><!--{{setImgUrl(_obj.uploadlist[0].name)}}-->\t\n        </div>\n      </div>\n          </div>\n             \n          <div  class=\"col-md-6\">\n      <div class=\"form-group\">\n              <rp-input rpType=\"text\" rpLabel=\" Contact Number\" [(rpModel)]=\"_obj.t4\" rpLabelRequired='true' rpReadonly=\"false\" rpClass=\"col-md-8 control-label\" rpLabelClass=\"col-md-4\" rpRequired=\"true\"></rp-input>\n      </div>\n      <div class=\"form-group\" style=\"margin-top:-7;\">\n              <rp-input rpType=\"text\" rpLabel=\"Contact Person\" [(rpModel)]=\"_obj.t3\" rpLabelRequired='true' rpReadonly=\"false\" rpClass=\"col-md-8 control-label\" rpLabelClass=\"col-md-4\" rpRequired=\"true\"></rp-input>\n      </div>\n      <div class=\"form-group\" style=\"margin-top:-7;\">\n              <rp-input rpType=\"text\" rpLabel=\"Email\" [(rpModel)]=\"_obj.t5\" rpLabelRequired='true' rpReadonly=\"false\" rpClass=\"col-md-8 control-label\" rpLabelClass=\"col-md-4\" rpRequired=\"true\"></rp-input>\n            </div>\n      <div class=\"form-group\" style=\"margin-top:-7;\">\n              <rp-input rpType=\"textarea\" rpLabel=\" Address \" [(rpModel)]=\"_obj.t10\" rpReadonly=\"false\" rpClass=\"col-md-8 control-label \" rpLabelClass=\"col-md-4\"></rp-input>  \n            </div>\n      <div class=\"form-group\" style=\"margin-top:-7;\">\n              <rp-input rpType=\"textarea\" rpLabel=\" Remark \" [(rpModel)]=\"_obj.t11\" rpReadonly=\"false\" rpClass=\"col-md-8 control-label \" rpLabelClass=\"col-md-4\"></rp-input>       \n            </div>       \n          </div>\n        </div>\n        <div class=\"col-md-2\">\n          <div [hidden]=\"msghide\">\n            <h3 align=\"right\"><b>{{msg}}</b></h3>\n          </div>\n        </div>\n        </div>\n      </form>\n    </div>\n  </div>\n</div>\n\n<div id=\"lu001popup2\" class=\"modal fade\" role=\"dialog\">\n  <div id=\"lu001popupsize\" class=\"modal-dialog modal-lg\">  \n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n        <h4 id=\"lu001popuptitle\" class=\"modal-title\">Account Info</h4>\n      </div> \n      <div id=\"lu001popupbody\" class=\"modal-body\">\n        <div class=\"col-md-5\" style = \"margin-top : -20px;float: right; !important\">\n\t\t\t  <adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"_userobj.totalCount\" (rpChanged)=\"changedPager($event)\" ></adminpager> \n\t\t\t  </div>\n\t\t\t  <div class=\"col-md-4\" style=\"margin-left: -15px;\">\n            <div class=\"input-group\">\n                <input class=\"form-control input-sm\" type=\"text\" [(ngModel)]=\"_userobj.searchText\" (keyup)=\"searchKeyup($event)\" tabindex=\"3\">\n                <span class=\"input-group-btn input-md\">\n                    <button class=\"btn btn-sm btn-primary input-md\" type=\"button\" ((keyup)=\"searchKeyup($event)\" (click)=\"goPopupList()\"\n                        tabindex=\"4\">\n                        <span class=\"glyphicon glyphicon-search\"></span>Search\n                    </button>\n                </span>\n            </div>\n        </div> \n        <table class=\"table table-striped table-condensed table-hover tblborder\">\n          <thead>\n            <tr>\n              <th class=\"col-md-6\">Phone No.</th>\n              <th class=\"col-md-6\">Name</th>                \n            </tr>\n          </thead>\n          <tbody>\n            <tr *ngFor=\"let obj of  _userobj.data\">\n              <td><a (click)=\"gotoWLAccount(obj.accountNumber)\">{{obj.accountNumber}}</a></td>\n              <td>{{obj.username}}</td> \n            </tr> \n          </tbody>\n        </table>  \n      </div>\n    </div>\n  </div>\n</div>\n\n<div id=\"glpopup\" class=\"modal fade\" role=\"dialog\">\n  <div id=\"lu001popupsize\" class=\"modal-dialog modal-lg\">  \n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n        <h4 id=\"lu001popuptitle\" class=\"modal-title\">GL Info</h4>\n      </div> \n      <div id=\"lu001popupbody\" class=\"modal-body\"> \n        <label> GL Description : {{_customer.glDesp}} </label>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-sm btn-primary\" data-dismiss=\"modal\">Close</button>\n      </div>\n    </div>\n  </div>\n</div>\n  \n<div id=\"sessionalert\" class=\"modal fade\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <div class=\"modal-body\">\n        <p>{{sessionAlertMsg}}</p>\n      </div>\n      <div class=\"modal-footer\">\n      </div>\n    </div>\n  </div>\n</div>\n\n<div [hidden] = \"_mflag\">\n  <div class=\"modal\" id=\"loader\"></div>\n</div>   \n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], MerchantSetup);
    return MerchantSetup;
}());
exports.MerchantSetup = MerchantSetup;
//# sourceMappingURL=merchant-setup.component.js.map