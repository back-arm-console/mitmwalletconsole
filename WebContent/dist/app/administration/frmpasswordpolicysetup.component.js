"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var Rx_1 = require('rxjs/Rx');
core_2.enableProdMode();
var FrmPasswordPolicyComponent = (function () {
    function FrmPasswordPolicyComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
        this._pswobj = this.getDefaultObj();
        this.sessionAlertMsg = "";
        this._mflag = false;
        this._util = new rp_client_util_1.ClientUtil();
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = false;
            this.checkSession();
            this.msghide = true;
            this._pswobj = this.getDefaultObj();
            this._editEvent = false;
        }
    }
    FrmPasswordPolicyComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            _this.goReadBySyskey();
        });
    };
    FrmPasswordPolicyComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    FrmPasswordPolicyComponent.prototype.getDefaultObj = function () {
        this._editEvent = false;
        return { "pswminlength": 0, "pswmaxlength": 0, "spchar": 0, "upchar": 0, "lowerchar": 0, "pswno": 0, "msgCode": "", "msgDesc": "", "sessionID": "", "userID": "" };
    };
    FrmPasswordPolicyComponent.prototype.goReadBySyskey = function () {
        var _this = this;
        this._mflag = false;
        var url = this.ics._apiurl + 'service001/readPswPolicy?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
        this.http.doGet(url).subscribe(function (data) {
            if (data.msgCode == '0016') {
                _this.sessionAlertMsg = data.msgDesc;
                _this.showMessage();
            }
            else {
                _this._pswobj = data;
            }
            _this._mflag = true;
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Error!");
            }
        });
    };
    FrmPasswordPolicyComponent.prototype.goList = function () {
        this._router.navigate(['/ActivateUserList']);
    };
    FrmPasswordPolicyComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this.goLogOut();
        });
    };
    FrmPasswordPolicyComponent.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    /* checkSession() {
      try {
  
        let url: string = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
        this.http.doGet(url).subscribe(
          data => {
            if (data.msgCode == '0016') {
              this.sessionAlertMsg = data.msgDesc;
              this.showMessage();
            }
          },
          error => {
            if (error._body.type == 'error') {
              alert("Connection Timed Out!");
            }
            else {
  
            }
          }, () => { });
      } catch (e) {
        alert("Invalid URL");
      }
    } */
    FrmPasswordPolicyComponent.prototype.showMessageAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmPasswordPolicyComponent.prototype.goEdit = function () {
        this._editEvent = true;
        jQuery("#btnupdate").prop("disabled", false);
    };
    FrmPasswordPolicyComponent.prototype.goSave = function () {
        var _this = this;
        this._mflag = false;
        var key = localStorage.getItem("key");
        this._pswobj.sessionID = this.ics._profile.sessionID;
        this._pswobj.userID = this.ics._profile.userID;
        var url = this.ics._apiurl + 'service001/savePswPolicy';
        var json = this._pswobj;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data.msgCode == '0016') {
                _this.sessionAlertMsg = data.msgDesc;
                _this.showMessage();
            }
            else if (data.msgCode == '0000') {
                _this._editEvent = false;
                jQuery("#btnupdate").prop("disabled", true);
                _this.showMessageAlert(data.msgDesc);
            }
            else {
                _this._editEvent = false;
                _this.showMessageAlert(data.msgDesc);
            }
            _this._mflag = true;
        }, function (error) { return alert(error); }, function () { });
    };
    FrmPasswordPolicyComponent.prototype.goValidate = function () {
        var total = Number(this._pswobj.pswno) + Number(this._pswobj.spchar) + Number(this._pswobj.upchar) + Number(this._pswobj.lowerchar);
        if (Number(this._pswobj.pswminlength) < 0 || Number(this._pswobj.pswmaxlength) < 0 || Number(this._pswobj.pswno) < 0 || Number(this._pswobj.spchar) < 0 || Number(this._pswobj.upchar) < 0 || Number(this._pswobj.lowerchar) < 0) {
            this.showMessageAlert("Input data should not be negative number");
        }
        else if (Number(this._pswobj.pswminlength) == 0) {
            this.showMessageAlert("Minimum length must be greater than 0");
        }
        else if (Number(this._pswobj.pswmaxlength) != 0 && Number(this._pswobj.pswminlength) > Number(this._pswobj.pswmaxlength)) {
            this.showMessageAlert("Minimum length must not be greater than maximum length");
        }
        else if (Number(this._pswobj.pswminlength) < total) {
            this.showMessageAlert("Minimum length must be greater or equal to " + total);
        }
        else
            this.goSave();
    };
    FrmPasswordPolicyComponent.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    FrmPasswordPolicyComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsgAlert(data.desc);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsgAlert(data.desc);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmPasswordPolicyComponent.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmPasswordPolicyComponent.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    FrmPasswordPolicyComponent = __decorate([
        core_1.Component({
            selector: 'passworpolicy-setup',
            template: "\n    <div class=\"container\">\n    <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n    <form class= \"form-horizontal\" (ngSubmit)=\"goValidate()\"> \n    <!-- Form Name -->\n    <legend>Password Policy</legend>\n    <div class=\"row  col-md-12\">    \n      <button class=\"btn btn-primary\" disabled id=\"btnupdate\"  type=\"submit\">Save</button>\n      <button class=\"btn btn-primary\" type=\"button\" (click)=\"goEdit()\">Edit</button>\n    </div>\n\n   <div class=\"form-group\"> \n   <div class=\"row col-md-12\">&nbsp;</div>\n    <div class=\"col-md-10\">    \n      <div class=\"form-group\">\n        <rp-input *ngIf=\"_editEvent == false\" rpLabelRequired=\"true\" [(rpModel)]=\"_pswobj.pswminlength\" rpRequired =\"true\" rpType=\"number\" rpLabel=\"Minimum Length\" rpReadonly=\"true\"></rp-input>\n        <rp-input *ngIf=\"_editEvent == true\" rpLabelRequired=\"true\" [(rpModel)]=\"_pswobj.pswminlength\" rpRequired =\"true\" rpType=\"number\" rpLabel=\"Minimum Length\"></rp-input>\n      </div>\n      <div class=\"form-group\">\n        <rp-input *ngIf=\"_editEvent == false\" rpLabelRequired=\"true\" [(rpModel)]=\"_pswobj.pswmaxlength\" rpRequired =\"true\" rpType=\"number\" rpLabel=\"Maximum Length\" rpReadonly=\"true\"></rp-input>\n        <rp-input *ngIf=\"_editEvent == true\" rpLabelRequired=\"true\" [(rpModel)]=\"_pswobj.pswmaxlength\" rpRequired =\"true\" rpType=\"number\" rpLabel=\"Maximum Length\"></rp-input>\n      </div>    \n      <div class=\"form-group\">\n        <rp-input *ngIf=\"_editEvent == false\" rpLabelRequired=\"true\" [(rpModel)]=\"_pswobj.spchar\" rpRequired =\"true\" rpType=\"number\" rpLabel=\"Special Character\" rpReadonly=\"true\"></rp-input>\n        <rp-input *ngIf=\"_editEvent == true\" rpLabelRequired=\"true\" [(rpModel)]=\"_pswobj.spchar\" rpRequired =\"true\" rpType=\"number\" rpLabel=\"Special Character\"></rp-input>\n      </div>\n      <div class=\"form-group\">\n        <rp-input *ngIf=\"_editEvent == false\" rpLabelRequired=\"true\" [(rpModel)]=\"_pswobj.upchar\" rpRequired =\"true\" rpType=\"number\" rpLabel=\"Uppercase Character\" rpReadonly=\"true\"></rp-input>\n        <rp-input *ngIf=\"_editEvent == true\" rpLabelRequired=\"true\" [(rpModel)]=\"_pswobj.upchar\" rpRequired =\"true\" rpType=\"number\" rpLabel=\"Uppercase Character\"></rp-input>\n      </div>\n      <div class=\"form-group\">\n        <rp-input *ngIf=\"_editEvent == false\" rpLabelRequired=\"true\" [(rpModel)]=\"_pswobj.lowerchar\" rpRequired =\"true\" rpType=\"number\" rpLabel=\"Lowercase Character\" rpReadonly=\"true\"></rp-input>\n        <rp-input *ngIf=\"_editEvent == true\" rpLabelRequired=\"true\" [(rpModel)]=\"_pswobj.lowerchar\" rpRequired =\"true\" rpType=\"number\" rpLabel=\"Lowercase Character\"></rp-input>\n      </div>\n      <div class=\"form-group\">\n        <rp-input *ngIf=\"_editEvent == false\" rpLabelRequired=\"true\" [(rpModel)]=\"_pswobj.pswno\" rpRequired =\"true\" rpType=\"number\" rpLabel=\"Number\" rpReadonly=\"true\"></rp-input>\n        <rp-input *ngIf=\"_editEvent == true\" rpLabelRequired=\"true\" [(rpModel)]=\"_pswobj.pswno\" rpRequired =\"true\" rpType=\"number\" rpLabel=\"Number\"></rp-input>\n      </div>\n    </div>\n    </div> \n\n    </form> \n    </div>\n    </div>\n    </div>\n    \n    <div id=\"sessionalert\" class=\"modal fade\">\n      <div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n          <div class=\"modal-body\">\n            <p>{{sessionAlertMsg}}</p>\n          </div>\n          <div class=\"modal-footer\">\n          </div>\n        </div>\n      </div>\n    </div>\n\n<div [hidden] = \"_mflag\">\n  <div class=\"modal\" id=\"loader\"></div>\n</div>\n    \n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmPasswordPolicyComponent);
    return FrmPasswordPolicyComponent;
}());
exports.FrmPasswordPolicyComponent = FrmPasswordPolicyComponent;
//# sourceMappingURL=frmpasswordpolicysetup.component.js.map