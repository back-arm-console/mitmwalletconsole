"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
// RP Framework
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_client_util_1 = require('../util/rp-client.util');
core_2.enableProdMode();
var MerchantList = (function () {
    function MerchantList(ics, _router, http) {
        this.ics = ics;
        this._router = _router;
        this.http = http;
        // Application Specific
        this._searchVal = ""; // simple search
        this._flagas = true; // flag advance search
        this._col_list = [{ "col_no": "1", "flag": false }, { "col_no": "2", "flag": false }];
        this._sorting = { _sort_type: "asc", _sort_col: "1" };
        this._mflag = true;
        this._util = new rp_client_util_1.ClientUtil();
        this.sessionAlertMsg = "";
        this._merchantobj = {
            "data": [{ "syskey": 0, "t1": "TBA", "t3": "", "t4": "", "t5": "", "t6": "", "t11": "", "t12": "", "userName": "", "userId": "", "accountNumber": "" }],
            "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0
        };
        this._pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 0 };
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            // jQuery("#loader").modal('hide');
            this._mflag = false;
            this.search();
        }
    }
    // list sorting part
    MerchantList.prototype.changeDefault = function () {
        for (var i = 0; i < this._col_list.length; i++) {
            this._col_list[i].flag = false;
        }
    };
    MerchantList.prototype.addSort = function (e) {
        for (var i = 0; i < this._col_list.length; i++) {
            if (this._col_list[i].col_no == e) {
                var _tmp_flag = this._col_list[i].flag;
                this.changeDefault();
                this._col_list[i].flag = !_tmp_flag;
                this._sorting._sort_type = !_tmp_flag ? "desc" : "asc";
                this._sorting._sort_col = e;
                break;
            }
        }
    };
    MerchantList.prototype.changedPager = function (event) {
        if (this._merchantobj.totalCount != 0) {
            this._pgobj = event;
            var current = this._merchantobj.currentPage;
            var size = this._merchantobj.pageSize;
            this._merchantobj.currentPage = this._pgobj.current;
            this._merchantobj.pageSize = this._pgobj.size;
            if (this._pgobj.current != current || this._pgobj.size != size) {
                this.search();
            }
        }
    };
    MerchantList.prototype.search = function () {
        var _this = this;
        try {
            this._mflag = false;
            this.http.doGet(this.ics.cmsurl + 'serviceCMS/getMerchantList?searchVal=' + encodeURIComponent(this._merchantobj.searchText) + '&pagesize=' + this._merchantobj.pageSize + '&currentpage=' + this._merchantobj.currentPage + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMessage(data.msgDesc, false);
                }
                else {
                    if (data != null && data != undefined) {
                        _this._pgobj.totalcount = data.totalCount;
                        _this._merchantobj = data;
                        if (_this._merchantobj.totalCount == 0) {
                            _this.showMessage("Data not found!", false);
                        }
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    MerchantList.prototype.searchKeyup = function (e) {
        if (e.which == 13) {
            this._merchantobj.currentPage = 1;
            this._msghide = true;
            this._recordhide = false;
            this.search();
        }
    };
    MerchantList.prototype.Searching = function () {
        this._merchantobj.currentPage = 1;
        this._msghide = true;
        this._recordhide = false;
        this.search();
    };
    MerchantList.prototype.goto = function (p) {
        this._router.navigate(['/Merchant Profile', 'read', p]);
    };
    //showMessage() {
    //    jQuery("#sessionalert").modal();
    //    Observable.timer(3000).subscribe(x => {
    //        this.goLogOut();
    //    });
    //}
    MerchantList.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    MerchantList.prototype.goNew = function () {
        this._router.navigate(['/Merchant Profile', 'new']);
    };
    MerchantList.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "primary", "t3": msg });
        }
    };
    MerchantList = __decorate([
        core_1.Component({
            selector: 'merchant-list',
            template: " \n    <div class=\"container-fluid\">\n\t<form class=\"form-horizontal\"> \n        <legend>Merchant List</legend>\n\t\t<div class=\"cardview list-height\">\t\t\n\t\t\t<div class=\"row col-md-12\">\n\t\t\t\t<div class=\"row col-md-3\">\n\t\t\t\t\t<div  class=\"input-group\">\n\t\t\t\t\t\t<span class=\"input-group-btn\">\n\t\t\t\t\t\t\t<button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goNew();\">New</button>\n\t\t\t\t\t\t</span> \n\t\t\t\t\t\t<input id=\"textinput\" name=\"textinput\" type=\"text\"  placeholder=\"Search\" [(ngModel)]=\"_merchantobj.searchText\" (keyup)=\"searchKeyup($event)\" [ngModelOptions]=\"{standalone: true}\" maxlength=\"30\" class=\"form-control input-sm\">\n\t\t\t\t\t\t<span class=\"input-group-btn\">\n\t\t\t\t\t\t\t<button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"Searching()\" >\n\t\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-search\"></span>Search\n\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t</span>        \n\t\t\t\t\t</div> \n\t\t\t\t</div>\t\t\n\t\t\t\t  <div class=\"pagerright\">\t\t\t\t  \n\t\t\t\t\t    <adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"_merchantobj.totalCount\" (rpChanged)=\"changedPager($event)\" ></adminpager> \n\t\t\t\t\t</div>\n            </div>\n            <div class=\"row col-md-12\" style=\"overflow-x:auto\">\n\t\t\t<table class=\"table table-striped table-condensed table-hover tblborder\" id=\"tblstyle\">\n\t\t\t\t<thead>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<th class=\"center\">Merchant ID</th>\n\t\t\t\t\t\t<th>Merchant Name</th>\n\t\t\t\t\t\t<th>Contact Person</th>\n\t\t\t\t\t\t<th>Contact No.</th>\n\t\t\t\t\t\t<th>Email</th>\n\t\t\t\t\t\t<th>Address</th>\n\t\t\t\t\t\t<th>Remark</th>   \n\t\t\t\t\t</tr>\n\t\t\t\t</thead>\n\t\t\t\t<tbody>\n\t\t\t\t\t<tr *ngFor=\"let obj of  _merchantobj.data\">\n\t\t\t\t\t\t<td class='center'><a (click)=\"goto(obj.userId)\">{{obj.userId}}</a></td>\n\t\t\t\t\t\t<td>{{obj.userName}}</td>\n\t\t\t\t\t\t<td>{{obj.t3}}</td>\n\t\t\t\t\t\t<td>{{obj.t4}}</td>\n\t\t\t\t\t\t<td>{{obj.t5}}</td>\n\t\t\t\t\t\t<td>{{obj.t10}}</td>\n\t\t\t\t\t\t<td>{{obj.t11}}</td>\n\t\t\t\t\t</tr>  \n\t\t\t\t</tbody>\n            </table>\n            </div>\n\t\t</div>\n\t</form>\n</div> \n  \n    <div [hidden] = \"_mflag\">\n        <div class=\"modal\" id=\"loader\"></div>\n    </div>\n   "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_http_service_1.RpHttpService])
    ], MerchantList);
    return MerchantList;
}());
exports.MerchantList = MerchantList;
//# sourceMappingURL=merchant-list.component.js.map