"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var Rx_1 = require('rxjs/Rx');
var GeoLocationService_1 = require('../framework/GeoLocationService');
var core_3 = require('angular2-cookie/core');
core_2.enableProdMode();
var TicketList = (function () {
    function TicketList(ics, ref, _router, http, geoLocationService, l_util, _cookieService) {
        this.ics = ics;
        this.ref = ref;
        this._router = _router;
        this.http = http;
        this.geoLocationService = geoLocationService;
        this.l_util = l_util;
        this._cookieService = _cookieService;
        this._mflag = false;
        this.ticketObj = this.getDefaultObj();
        this.markers = [];
        this.marker = [];
        //_hide=true;
        this._toggleSearch = true;
        this.ticketRegion = "";
        this._roleval = "";
        this.coordinates = {
            latitude: 0,
            longitude: 0
        };
        this._util = new rp_client_util_1.ClientUtil();
        this.myDatePickerOptions = this._util.getDatePicker();
        this._dates = { "fromDate": this.dateobj, "toDate": this.dateobj };
        this.disableEach = true;
        //disableAll = false;
        this._sessionObj = this.getSessionObj();
        this.today = this._util.getTodayDate();
        this._doLoading = false;
        this._obj = this.getDefaultObjDetail();
        this.image = "";
        this.dateTime = "";
        this.channelName = "";
        this._alertflag = true;
        this._alertmsg = "";
        /* Start TicketList */
        // RP Framework 
        //subscription: Subscription;
        this._shownull = false;
        this._OperationMode = "";
        this._divexport = false;
        this._isLoading = true;
        this._SearchString = "";
        this._showListing = false;
        this._showPagination = false;
        this._togglePagination = true;
        this.mstatus = 0;
        this._ButtonInfo = { "role": "", "formname": "", "button": "" };
        this._FilterDataset = {
            filterList: [
                { "itemid": "", "caption": "", "fieldname": "", "datatype": "", "condition": "", "t1": "", "t2": "" }
            ],
            "pageNo": 1, "pageSize": 10, "filterSource": 0, "sessionID": this.ics._profile.sessionID, "userID": this.ics._profile.userID, "userName": this.ics._profile.userName
        };
        this._ListingDataset = {
            ticketData: [
                {
                    "srno": "", "syskey": "", "autokey": "", "T1": "", "T2": "",
                    "T3": "", "T4": "", "T5": "", "T6": "", "T7": "",
                    "T8": "", "T9": "", "T10": "", "T11": "", "T12": "",
                    "T13": "", "T14": "", "T15": "", "T16": "", "T17": "",
                    "T18": "", "T19": "", "T20": "", "n1": 0, "n2": 0,
                    "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0,
                    "n8": 0, "n9": 0, "n10": 0,
                    "createdDate": "", "modifiedDate": "", "userID": "", "date": "", "time": "",
                    "shootDate": "", "shootTime": "", "sessionID": "", "code": "", "desc": "",
                    "state": false
                }
            ],
            "pageNo": 0, "pageSize": 0, "totalCount": 1, "userID": "", "userName": ""
        };
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        this._roleval = ics._profile.t1;
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this.checkSession();
            this.imageLink = this.ics._imglink + "uploads/smallImage/";
            this._dates.fromDate = this._util.changestringtodateobject(this.today);
            this._dates.toDate = this._util.changestringtodateobject(this.today);
            this.setDates();
            this.getStateList();
            this.getTicketStatus();
        }
    }
    TicketList.prototype.getDefaultObj = function () {
        return {
            "ticketData": [{
                    "autokey": 0, "syskey": 0, "createdDate": "", "modifiedDate": "", "date": "",
                    "time": "", "shootDate": "", "shootTime": "", "userID": "", "n1": 0,
                    "n2": 0, "t1": "", "t2": "", "t3": "", "t4": "",
                    "t5": "", "t6": "", "t7": "", "t8": "", "t9": "",
                    "t10": "", "t11": ""
                }], "sessionID": "", "userID": "",
            "aFromDate": "",
            "aToDate": "",
            "alldate": true,
            "allStatus": true,
            "newStatus": false,
            "wipStatus": false,
            "closedStatus": false,
            "rejectedStatus": false
        };
    };
    TicketList.prototype.getDefaultObjDetail = function () {
        return {
            "sessionID": "", "userID": "", "msgCode": "", "msgDesc": "",
            "autokey": 0, "syskey": 0,
            "createdDate": "", "modifiedDate": "", "userid": "", "shootDate": "", "shootTime": "",
            "date": "", "time": "",
            "t1": "", "t2": "", "t3": "", "t4": "", "t5": "",
            "t6": "", "t7": "", "t8": "", "t9": "", "t10": "",
            "t11": "", "t12": "", "t13": "", "t14": "", "t15": "",
            "t16": "", "t17": "", "t18": "", "t19": "", "t20": "",
            "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0,
            "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0
        };
    };
    TicketList.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    TicketList.prototype.ngOnInit = function () {
        var _this = this;
        this.geoLocationService.getPosition().subscribe(function (pos) {
            _this.coordinates = {
                latitude: +(pos.coords.latitude),
                longitude: +(pos.coords.longitude)
            };
        });
        this.filterSearch();
        this.loadAdvancedSearchData();
    };
    TicketList.prototype.goNew = function () {
        this._router.navigate(['/ticket', 'new']);
    };
    TicketList.prototype.ngAfterViewChecked = function () {
        this._isLoading = false;
    };
    TicketList.prototype.loadAdvancedSearchData = function () {
        this._TypeList =
            {
                "lovState": [],
                "lovStatus": []
            };
        this.loadState(); // Branch
        this.loadStatus(); // Status
        this._FilterList =
            [
                { "itemid": "1", "caption": "Search String", "fieldname": "SearchString", "datatype": "simplesearch", "condition": "c", "t1": "", "t2": "", "t3": "" },
                { "itemid": "2", "caption": "State", "fieldname": "State", "datatype": "lovState", "condition": "", "t1": "", "t2": "", "t3": "" },
                { "itemid": "3", "caption": "Status", "fieldname": "Status", "datatype": "lovStatus", "condition": "", "t1": "", "t2": "", "t3": "" },
                { "itemid": "4", "caption": "Date Range", "fieldname": "DateRange", "datatype": "date", "condition": "bt", "t1": this.l_util.getTodayDate(), "t2": this.l_util.getTodayDate(), "t3": "true" },
            ];
    };
    TicketList.prototype.loadState = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics.ticketurl + 'serviceCMS/getStateListByUserID?type=&userID=' + this.ics._profile.userID).subscribe(function (response) {
                if (response.refstate != null && response.refstate != undefined) {
                    //this.ref._lov3.refstate = [{ "value": "", "caption": "" }];
                    _this.ref._lov3.refstate = [];
                    if (!(response.refstate instanceof Array)) {
                        var m = [];
                        m[0] = response.data;
                        _this.ref._lov3.refstate = m;
                    }
                    else {
                        _this.ref._lov3.refstate = response.refstate;
                    }
                    _this.ref._lov3.refstate.forEach(function (iItem) {
                        var l_Item = { "caption": "", "value": "" };
                        l_Item['caption'] = iItem.caption;
                        l_Item['value'] = iItem.value;
                        _this._TypeList.lovState.push(iItem);
                    });
                }
                else {
                    _this.ref._lov3.refstate = [{ "value": "", "caption": "Empty" }];
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Error!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    TicketList.prototype.loadStatus = function () {
        var _this = this;
        try {
            var url = this.ics.ticketurl + 'serviceTicketAdm/getTicketStatus';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.msgCode == "0016") {
                        _this.showMsg(data.msgDesc, false);
                        _this.logout();
                    }
                    if (data.msgCode == "0000") {
                        if (!(data.refTicketStatus instanceof Array)) {
                            var m = [];
                            m[0] = data.refTicketStatus;
                            _this.ref._lov3.refTicketStatus = m;
                        }
                        else {
                            _this.ref._lov3.refTicketStatus = data.refTicketStatus;
                        }
                        _this.ref._lov3.refTicketStatus.forEach(function (iItem) {
                            var l_Item = { "caption": "", "value": "" };
                            l_Item['caption'] = iItem.caption;
                            l_Item['value'] = iItem.value;
                            _this._TypeList.lovStatus.push(iItem);
                        });
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    TicketList.prototype.btnToggleSearch_onClick = function () {
        this.toggleSearch(!this._toggleSearch);
    };
    TicketList.prototype.btnTogglePagination_onClick = function () {
        this.togglePagination(!this._togglePagination);
    };
    TicketList.prototype.toggleSearch = function (aToggleSearch) {
        // Set Flag
        this._toggleSearch = aToggleSearch;
        // Clear Simple Search
        if (!this._toggleSearch)
            jQuery("#isInputSearch").val("");
        // Enable/Disabled Simple Search Textbox
        jQuery('#isInputSearch').prop('disabled', !this._toggleSearch);
        // Hide Advanced Search
        if (this._toggleSearch)
            this.ics.send001("CLEAR");
        // Show/Hide Advanced Search    
        //  
        if (this._toggleSearch)
            jQuery("#asAdvancedSearch").css("display", "none"); // true     - Simple Search
        else
            jQuery("#asAdvancedSearch").css("display", "inline-block"); // false    - Advanced Search
        // Set Icon 
        //  
        if (this._toggleSearch)
            jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-up").addClass("glyphicon glyphicon-menu-down"); // true     - Simple Search
        else
            jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-down").addClass("glyphicon glyphicon-menu-up"); // false    - Advanced Search
        // Set Tooltip
        //
        var l_Tooltip = (this._toggleSearch) ? "Expand" : "Collapse";
        jQuery('#btnToggleSearch').attr('title', l_Tooltip);
    };
    TicketList.prototype.togglePagination = function (aTogglePagination) {
        // Set Flag
        this._togglePagination = aTogglePagination;
        // Show/Hide Pagination
        //
        if (this._showPagination && this._togglePagination)
            jQuery("#divPagination").css("display", "inline-block");
        else
            jQuery("#divPagination").css("display", "none");
        // Rotate Icon
        //
        if (!this._togglePagination)
            jQuery("#lblTogglePagination").css("transform", "rotate(270deg)");
        else
            jQuery("#lblTogglePagination").css("transform", "rotate(90deg)");
        // Set Icon Position
        if (!this._togglePagination)
            jQuery("#lblTogglePagination").css({ "margin-left": "-5px", "margin-top": "0px" });
        else
            jQuery("#lblTogglePagination").css({ "margin-left": "-5px", "margin-top": "-1px" });
        // Set Tooltip
        //
        var l_Tooltip = (this._togglePagination) ? "Collapse" : "Expand";
        jQuery('#btnTogglePagination').attr('title', l_Tooltip);
    };
    TicketList.prototype.btnClose_MouseEnter = function () {
        jQuery("#btnClose").removeClass('btn btn-sm btn-secondary').addClass('btn btn-sm btn-danger');
    };
    TicketList.prototype.btnClose_MouseLeave = function () {
        jQuery("#btnClose").removeClass('btn btn-sm btn-danger').addClass('btn btn-sm btn-secondary');
    };
    TicketList.prototype.closePage = function () {
        this._router.navigate(['000', { cmd: "READ", p1: this.ics._profile.userID }]);
    };
    TicketList.prototype.renderAdvancedSearch = function (event) {
        this.toggleSearch(!event);
    };
    TicketList.prototype.filterAdvancedSearch = function (event) {
        this._FilterDataset.filterSource = 1;
        this._FilterDataset.filterList = event;
        if (this._OperationMode != "prepareFilter") {
            if (this._FilterDataset.pageNo == 1)
                this.filterRecords();
            else
                this._FilterDataset.pageNo = 1;
        }
    };
    TicketList.prototype.changedPager = function (event) {
        if (!this._isLoading) {
            var l_objPagination = event.obj;
            this._FilterDataset.pageNo = l_objPagination.current;
            this._FilterDataset.pageSize = l_objPagination.size;
            this.filterRecords();
        }
    };
    TicketList.prototype.filterSearch = function () {
        if (this._toggleSearch)
            this.filterCommonSearch();
        else
            this.ics.send001("FILTER");
    };
    TicketList.prototype.filterCommonSearch = function () {
        var l_DateRange;
        var l_SearchString = "";
        this._FilterDataset.filterList = [];
        this._FilterDataset.filterSource = 0;
        if (jQuery.trim(this._SearchString) != "") {
            l_SearchString = jQuery.trim(this._SearchString);
            this._FilterDataset.filterList =
                [
                    { "itemid": "1", "caption": "Search String", "fieldname": "SearchString", "datatype": "simplesearch", "condition": "c", "t1": l_SearchString, "t2": "" }
                ];
        }
        l_DateRange = { "itemid": "5", "caption": "Date Range", "fieldname": "DateRange", "datatype": "date", "condition": "eq", "t1": this.l_util.getTodayDate(), "t2": "" };
        this._FilterDataset.filterList.push(l_DateRange);
        if (this._OperationMode != "prepareFilter") {
            if (this._FilterDataset.pageNo == 1)
                this.filterRecords();
            else
                this._FilterDataset.pageNo = 1;
        }
    };
    TicketList.prototype.filterRecords = function () {
        var _this = this;
        this._mflag = false;
        var l_Data = this._FilterDataset;
        //let l_ServiceURL: string = this.ics._apiurl + 'service001/getLoanApprovalList';
        var l_ServiceURL = this.ics.ticketurl + 'serviceTicketAdm/getTickets';
        // Show loading animation
        this.http.doPost(l_ServiceURL, l_Data).subscribe(function (data) {
            if (data != null) {
                _this._ListingDataset = data;
                _this._shownull = false;
                // Convert to array for single item
                if (_this._ListingDataset.ticketData != undefined) {
                    _this._ListingDataset.ticketData = _this.l_util.convertToArray(_this._ListingDataset.ticketData);
                }
                else {
                    _this._shownull = true;
                }
                // Show / Hide Listing
                _this._showListing = (_this._ListingDataset.ticketData != undefined);
                // Show / Hide Pagination
                _this._showPagination = (_this._showListing && (_this._ListingDataset.totalCount > 10));
                // Show / Hide Pagination
                _this.togglePagination(_this._showListing);
                // Hide loading animation
                _this._mflag = true;
            }
        }, function (error) {
            // Hide loading animation
            _this._mflag = true;
            // Show error message
            // this.ics.sendBean({ t1:"rp-error", t2: "Server connection error." });
        }, function () { });
    };
    TicketList.prototype.getAllTicket = function () {
        var _this = this;
        // this._hide=true;
        this.setDates();
        if (this.ticketObj.aFromDate == "") {
            this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Required From Date" });
        }
        if (this.ticketObj.aToDate == "") {
            this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Required To Date" });
        }
        else if (!/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/.test(this.ticketObj.aFromDate)) {
            this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Invalid From Date!" });
        }
        else if (!/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/.test(this.ticketObj.aToDate)) {
            this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Invalid To Date!" });
        }
        else if (this.ticketObj.aFromDate > this.ticketObj.aToDate) {
            this.ics.sendBean({ "t1": "rp-msg", t2: "Information", t3: "From Date must not exceed To Date!" });
        }
        else {
            this.ticketObj.aFromDate = this.l_util.changeDateFromYYYYDashMMDashDDtoYYYYMMDD(this.ticketObj.aFromDate);
            this.ticketObj.aToDate = this.l_util.changeDateFromYYYYDashMMDashDDtoYYYYMMDD(this.ticketObj.aToDate);
            try {
                this.markers = [];
                //this._mflag = false;
                var url = this.ics.ticketurl + 'serviceTicketAdm/getAllTicket?ticketRegion=' + this.ticketRegion;
                var json = this.ticketObj;
                this.ticketObj.sessionID = this.ics._profile.sessionID;
                this.ticketObj.userID = this.ics._profile.userID;
                this.http.doPost(url, json).subscribe(function (data) {
                    if (data.ticketData == undefined || data.ticketData == null && data.ticketData.length == 0) {
                        _this.showMsg("There is no tickets.", false);
                        _this.ticketObj.ticketData = [];
                        _this.showNoLatLongMap();
                    }
                    else {
                        //this.ticketObj = data;
                        if (!(data.ticketData instanceof Array)) {
                            var m = [];
                            m[0] = data.ticketData;
                            _this.ticketObj.ticketData = m;
                        }
                        else {
                            _this.ticketObj.ticketData = data.ticketData;
                        }
                        for (var i = 0; i < _this.ticketObj.ticketData.length; i++) {
                            if (_this.ticketObj.ticketData[i].t8 != undefined && _this.ticketObj.ticketData[i].t8 != null && _this.ticketObj.ticketData[i].t8 != '') {
                                var def = _this.ticketObj.ticketData[i].t8;
                                var latitudeOne = def.substring(0, def.indexOf("/"));
                                var longitudeOne = def.substring(def.indexOf("/") + 1);
                                var info = '';
                                var ticketNumber = '';
                                if (_this.ticketObj.ticketData[i].t5 != undefined && _this.ticketObj.ticketData[i].t5 != null && _this.ticketObj.ticketData[i].t5 != '') {
                                    info = "Ticket No: " + _this.ticketObj.ticketData[i].t1 + "\n" +
                                        "Name: " + _this.ticketObj.ticketData[i].t5 + "\n" +
                                        "Date: " + _this.ticketObj.ticketData[i].shootDate + "\nTime: " + _this.ticketObj.ticketData[i].shootTime + "\n" +
                                        "Phone No: " + _this.ticketObj.ticketData[i].t9;
                                    ticketNumber = _this.ticketObj.ticketData[i].t1;
                                }
                                _this.markers.push([latitudeOne, longitudeOne, [], info, ticketNumber]);
                            }
                        }
                        if (_this.markers.length != 0) {
                            _this.showMap();
                        }
                        _this._mflag = true;
                    }
                }, function (error) {
                    _this._mflag = true;
                    if (error._body.type == "error") {
                        alert("Connection Timed Out.");
                    }
                }, function () {
                    _this._mflag = true;
                });
            }
            catch (e) {
                this._mflag = true;
                alert("Invalid URL.");
            }
        }
    };
    TicketList.prototype.showMap = function () {
        //this._hide=true;
        if (this.markers.length != 0) {
            var map = new google.maps.Map(document.getElementById('map-canvas'));
            var infowindow = new google.maps.InfoWindow();
            for (var i = 0; i < this.markers.length; i++) {
                var latlng = new google.maps.LatLng(this.markers[i][0], this.markers[i][1]);
                map = new google.maps.Map(document.getElementById('map-canvas'), {
                    zoom: 17,
                    center: latlng
                });
            }
            var _loop_1 = function(i) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(this_1.markers[i][0], this_1.markers[i][1]),
                    map: map,
                    title: this_1.markers[i][3],
                    zIndex: this_1.markers[i][1],
                    ticketNumber: this_1.markers[i][4]
                });
                var that = this_1;
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        that.getOneTicket(marker.ticketNumber);
                    };
                })(marker, i));
            };
            var this_1 = this;
            var marker;
            for (var i = 0; i < this.markers.length; i++) {
                _loop_1(i);
            }
        }
    };
    TicketList.prototype.showNoLatLongMap = function () {
        var _this = this;
        //this._hide=true;
        var cookieLat = this._cookieService.get("latitude");
        var cookieLong = this._cookieService.get("longitude");
        if (cookieLat != undefined && cookieLat != null && cookieLat != '' &&
            cookieLong != undefined && cookieLong != null && cookieLong != '') {
            this.coordinates.latitude = parseFloat(cookieLat);
            this.coordinates.longitude = parseFloat(cookieLong);
        }
        else {
            if (this.coordinates == undefined || this.coordinates.latitude == undefined || this.coordinates.latitude == null
                || this.coordinates.latitude == 0 || this.coordinates.longitude == undefined
                || this.coordinates.longitude == null || this.coordinates.longitude == 0) {
                //Naypyitaw
                this.coordinates.latitude = 19.7450008;
                this.coordinates.longitude = 96.1297226;
            }
        }
        var map = new google.maps.Map(document.getElementById('map-canvas'));
        var infowindow = new google.maps.InfoWindow();
        var latlng = new google.maps.LatLng(this.coordinates.latitude, this.coordinates.longitude);
        map = new google.maps.Map(document.getElementById('map-canvas'), {
            zoom: 15,
            center: latlng
        }, function (error) {
            _this._mflag = true;
        });
    };
    TicketList.prototype.getOneTicket = function (ticketNo) {
        var _this = this;
        //this._hide=true;
        this._doLoading = true;
        try {
            var urlTicket = this.ics.ticketurl + 'serviceTicketAdm/getOneTicket';
            var _objA = {
                "sessionID": this.ics._profile.sessionID,
                "userID": this.ics._profile.userID,
                "ticketNo": ticketNo
            };
            this.http.doPost(urlTicket, _objA).subscribe(function (result) {
                if (result.code == "0016") {
                    _this._doLoading = false;
                    _this.showMsg(result.desc, false);
                    _this.logout();
                }
                else if (result.code == "0000") {
                    _this._obj = result;
                    _this.channelName = _this._obj.t15;
                    _this.image = _this.imageLink + _this._obj.t13;
                    _this.dateTime = _this._obj.shootDate + " " + _this._obj.shootTime;
                    /* channel name end */
                    /* map start */
                    var abc = _this._obj.t8;
                    var latitudeOne = abc.substring(0, abc.indexOf("/"));
                    var longitudeOne = abc.substring(abc.indexOf("/") + 1);
                    var info = '';
                    if (_this._obj.t5 != undefined && _this._obj.t5 != null && _this._obj.t5 != '') {
                        info = "Ticket No. " + _this._obj.t1 + " on " + _this._obj.shootDate;
                    }
                    _this.marker[0] = [latitudeOne, longitudeOne, [], info];
                    var map = new google.maps.Map(document.getElementById('map-canvasTwo'));
                    var infowindow = new google.maps.InfoWindow();
                    for (var i = 0; i < _this.marker.length; i++) {
                        var latlng = new google.maps.LatLng(_this.marker[i][0], _this.marker[i][1]);
                        map = new google.maps.Map(document.getElementById('map-canvasTwo'), {
                            zoom: 15,
                            center: latlng
                        });
                    }
                    for (var i = 0; i < _this.marker.length; i++) {
                        var marker = new google.maps.Marker({
                            position: new google.maps.LatLng(_this.marker[i][0], _this.marker[i][1]),
                            map: map,
                            title: _this.marker[i][3],
                            zIndex: _this.marker[i][1]
                        });
                        infowindow = new google.maps.InfoWindow();
                        google.maps.event.addListener(marker, 'click', (function (marker, i) {
                            return function () {
                                //infowindow.setContent("Data:"+MarkerData[i][3]+"<br />"+ MarkerData[i][2]);
                                infowindow.setContent("Data:" + this.marker[i][3]);
                                infowindow.open(map, marker);
                            };
                        })(marker, i));
                    }
                    /* map end */
                    _this._doLoading = false;
                    jQuery("#popup").modal();
                    _this.setCookie(_this._obj.t8);
                }
                else {
                    _this._doLoading = false;
                    alert("Please try again.");
                    console.log("result else=" + JSON.stringify(result));
                }
            }, function (error) {
                _this._doLoading = false;
                console.log("error=" + JSON.stringify(error));
                alert("Connection Timed Out.");
            }, function () {
                _this._doLoading = false;
            });
        }
        catch (e) {
            this._doLoading = false;
            alert("Invalid URL.");
        }
    };
    TicketList.prototype.goto = function (p) {
        this._router.navigate(['/ticket', 'read', p]);
    };
    TicketList.prototype.goClosed = function () {
        this._obj = { "sessionID": "", "userID": "", "msgCode": "", "msgDesc": "",
            "autokey": 0, "syskey": 0,
            "createdDate": "", "modifiedDate": "", "userid": "", "shootDate": "", "shootTime": "",
            "date": "", "time": "",
            "t1": "", "t2": "", "t3": "", "t4": "", "t5": "",
            "t6": "", "t7": "", "t8": "", "t9": "", "t10": "",
            "t11": "", "t12": "", "t13": "", "t14": "", "t15": "",
            "t16": "", "t17": "", "t18": "", "t19": "", "t20": "",
            "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0,
            "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0 };
        this.dateTime = "";
    };
    TicketList.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    TicketList.prototype.setDates = function () {
        this.ticketObj.aFromDate = this._util.getDatePickerDate(this._dates.fromDate);
        this.ticketObj.aToDate = this._util.getDatePickerDate(this._dates.toDate);
    };
    TicketList.prototype.setCookie = function (latAndLong) {
        var latitude = this._obj.t8.substring(0, this._obj.t8.indexOf("/"));
        var longitude = this._obj.t8.substring(this._obj.t8.indexOf("/") + 1);
        this._cookieService.put("latitude", latitude);
        this._cookieService.put("longitude", longitude);
    };
    TicketList.prototype.getAllDate = function (event) {
        this.ticketObj.alldate = event.target.checked;
        this.getAllTicket();
    };
    TicketList.prototype.getPreferredDate = function () {
        this.setDates();
        this.getAllTicket();
    };
    TicketList.prototype.getAll = function (event) {
        this.ticketObj.allStatus = event.target.checked;
        if (this.ticketObj.allStatus) {
            this.disableEach = true;
            this.getAllTicket();
        }
        else {
            this.disableEach = false;
            if (this.ticketObj.newStatus || this.ticketObj.wipStatus
                || this.ticketObj.closedStatus || this.ticketObj.rejectedStatus) {
                this.getAllTicket();
            }
        }
    };
    TicketList.prototype.getNew = function (event) {
        this.ticketObj.newStatus = event.target.checked;
        this.getAllTicket();
    };
    TicketList.prototype.getWIP = function (event) {
        this.ticketObj.wipStatus = event.target.checked;
        this.getAllTicket();
    };
    TicketList.prototype.getClosed = function (event) {
        this.ticketObj.closedStatus = event.target.checked;
        this.getAllTicket();
    };
    TicketList.prototype.getRejected = function (event) {
        this.ticketObj.rejectedStatus = event.target.checked;
        this.getAllTicket();
    };
    TicketList.prototype.changeRegion = function (options) {
        this.ticketRegion = options[options.selectedIndex].value;
        this.getAllTicket();
    };
    TicketList.prototype.getTicketStatus = function () {
        var _this = this;
        try {
            var url = this.ics.ticketurl + 'serviceTicketAdm/getTicketStatus';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.msgCode == "0016") {
                        _this.showMsg(data.msgDesc, false);
                        _this.logout();
                    }
                    if (data.msgCode != "0000") {
                        _this.showMsg(data.msgDesc, false);
                    }
                    if (data.msgCode == "0000") {
                        if (!(data.refTicketStatus instanceof Array)) {
                            var m = [];
                            m[0] = data.refTicketStatus;
                            _this.ref._lov3.refTicketStatus = m;
                        }
                        else {
                            _this.ref._lov3.refTicketStatus = data.refTicketStatus;
                        }
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    TicketList.prototype.getStateList = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics.ticketurl + 'serviceCMS/getStateListByUserID?type=&userID=' + this.ics._profile.userID).subscribe(function (response) {
                if (response.refstate != null && response.refstate != undefined) {
                    //this.ref._lov3.refstate = [{ "value": "", "caption": "" }];
                    _this.ref._lov3.refstate = [];
                    if (!(response.refstate instanceof Array)) {
                        var m = [];
                        m[0] = response.data;
                        _this.ref._lov3.refstate = m;
                    }
                    else {
                        _this.ref._lov3.refstate = response.refstate;
                    }
                    _this.ticketRegion = _this.ref._lov3.refstate[0].value;
                }
                else {
                    _this.ref._lov3.refstate = [{ "value": "", "caption": "Empty" }];
                }
                _this.getAllTicket();
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Error!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    TicketList.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    TicketList.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this.goLogOut();
        });
    };
    TicketList.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    TicketList.prototype.checkSession = function () {
        var _this = this;
        this._doLoading = true;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsg(data.desc, false);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsg(data.desc, false);
                    }
                }
            }, function (error) {
                console.log("error=" + JSON.stringify(error));
                // if (error._body.type == "error") {
                // }
                alert("Connection Timed Out.");
                _this._doLoading = false;
            }, function () {
                _this._doLoading = false;
            });
        }
        catch (e) {
            this._doLoading = false;
            alert("Invalid URL.");
        }
    };
    TicketList.prototype.goUpdate = function () {
        var _this = this;
        this._doLoading = true;
        try {
            var url = this.ics.ticketurl;
            var alertMsg = "";
            url += 'serviceTicketAdm/updateMyTicket';
            if (alertMsg == "") {
                var json = this._obj;
                this._obj.sessionID = this.ics._profile.sessionID;
                this._obj.userID = this.ics._profile.userID;
                this.http.doPost(url, json).subscribe(function (data) {
                    if (data != null) {
                        if (data.msgCode == "0016") {
                            _this.showCustomMsgAlert(data.msgDesc, false);
                            _this.logout();
                        }
                        if (data.msgCode == "0000") {
                            _this.showCustomMsgAlert(data.msgDesc, true);
                            _this._obj.autokey = data.longResult[0];
                        }
                    }
                    else {
                        _this.showCustomMsgAlert("Please try again.", false);
                    }
                }, function (error) {
                    console.log("error=" + JSON.stringify(error));
                    _this.showCustomMsgAlert("Connection Timed Out.", false);
                    _this._doLoading = false;
                }, function () {
                    _this._doLoading = false;
                });
            }
            else {
                console.log("this._obj=" + JSON.stringify(this._obj));
                this._doLoading = false;
                this.showCustomMsgAlert(alertMsg, false);
            }
        }
        catch (e) {
            console.log("this._obj=" + JSON.stringify(this._obj) + "/r/n/////" + JSON.stringify(e));
            this._doLoading = false;
            this.showCustomMsgAlert("Invalid URL.", false);
        }
    };
    TicketList.prototype.showCustomMsgAlert = function (msg, bool) {
        var type = "";
        if (bool == true) {
            type = "success";
        }
        if (bool == false) {
            type = "warning";
        }
        if (bool == undefined) {
            type = "danger";
        }
        this._alertmsg = msg;
        this._alertflag = false;
        var _snack_style = 'msg-info';
        if (type == "success")
            _snack_style = 'msg-success';
        else if (type == "warning")
            _snack_style = 'msg-warning';
        else if (type == "danger")
            _snack_style = 'msg-danger';
        else if (type == "information")
            _snack_style = 'msg-info';
        document.getElementById("snackbar1").innerHTML = this._alertmsg;
        var snackbar1 = document.getElementById("snackbar1");
        snackbar1.className = "show " + _snack_style;
        setTimeout(function () { snackbar1.className = snackbar1.className.replace("show", ""); }, 3000);
    };
    TicketList.prototype.downloadFile = function (aFileName) {
        var l_Url = this.ics._apiurl + 'service001/downloadexcel?filename=' + aFileName;
        jQuery("#ExcelDownload").html("<iframe src=" + l_Url + "></iframe>");
    };
    TicketList.prototype.btnDashboard = function () {
        this._router.navigate(['/ticketdashboard']);
    };
    TicketList.prototype.goTicketList = function () {
        this._router.navigate(['/ticketList']);
    };
    TicketList.prototype.goClosePrint = function () {
        this._divexport = false;
    };
    TicketList = __decorate([
        core_1.Component({
            selector: 'ticket-list',
            template: "\n  \n  <div class=\"container col-md-12 col-sm-12 col-xs-12\">\n  <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n  \n    <form class=\"form-horizontal\">\n    <legend>\n        <div style=\"float:left;display:inline;font-size:18px;margin-top:7px;margin-bottom:26px;margin-left:5px;\">\n        Tickets\n        <!--<button id=\"btnPrint\" class=\"btn btn-sm btn-primary\" type=\"button\" style=\"cursor:pointer;text-align:center;margin-top:-5px;margin-left:10px;height:34px;\" (click)=\"goTicketList()\" title=\"TicketList\" [disabled]=\"true\">                                    \n            Tickets\n            </button>\n            <button id=\"btnPrint\" class=\"btn btn-sm btn-primary\" type=\"button\" style=\"cursor:pointer;text-align:center;margin-top:-5px;margin-left:10px;height:34px;\" (click)=\"btnDashboard()\" title=\"Dashboard\" >                                    \n            Dashboard\n            </button>-->\n        </div>\n        <!--<div [hidden]=\"_hide\">-->\n        <div style=\"float:right;text-align:right;\">                                \n            <div style=\"display:inline-block;padding-right:0px;width:280px;\">\n                <div class=\"input-group\"> \n                    <input class=\"form-control\" type=\"text\" id=\"isInputSearch\" placeholder=\"Search\" autocomplete=\"off\" spellcheck=\"false\" [(ngModel)]=\"_SearchString\" [ngModelOptions]=\"{standalone: true}\" (keyup.enter)=\"filterSearch()\">\n\n                    <span id=\"btnSimpleSearch\" class=\"input-group-addon\">\n                        <i class=\"glyphicon glyphicon-search\" style=\"cursor: pointer\" (click)=\"filterSearch()\"></i>\n                    </span>\n                </div>\n            </div>\n\n            <button id=\"btnToggleSearch\" class=\"btn btn-sm btn-secondary\" type=\"button\" style=\"cursor:pointer;text-align:center;margin-top:-24px;height:34px;\" (click)=\"btnToggleSearch_onClick()\" title=\"Collapse\">\n                <i id=\"lblToggleSearch\" class=\"glyphicon glyphicon-menu-down\"></i> \n            </button>\n\n            <button *ngIf=\"false\" id=\"btnTogglePagination\" type=\"button\" class=\"btn btn-sm btn-default\" style=\"cursor:pointer;text-align:center;margin-left:25px;margin-top:-24px;width:30px;height:34px;\" (click)=\"btnTogglePagination_onClick()\" title=\"Collapse\">\n                <i id=\"lblTogglePagination\" class=\"glyphicon glyphicon-eject\" style=\"color:#2e8690;margin-left:-4px;transform: rotate(90deg)\"></i>\n            </button>\n\n            <div id=\"divPagination\" style=\"display:none;margin-left:13px;margin-top:-7px;margin-bottom:5px;\">\n                <pager6 id=\"pgPagination\" rpPageSizeMax=\"100\" [(rpModel)]=\"_ListingDataset.totalCount\" (rpChanged)=\"changedPager($event)\" style=\"font-size:14px;text-align:right;margin-top:-7px;margin-right:10px;\"></pager6>\n            </div>\n\n            <button id=\"btnClose\" type=\"button\" class=\"btn btn-sm btn-secondary\" (click)=\"closePage()\" (mouseenter)=\"btnClose_MouseEnter()\" (mouseleave)=\"btnClose_MouseLeave()\" title=\"Close\" style=\"cursor:pointer;height:34px;text-align:center;margin-top:-24px;\">\n                <i id=\"lblClose\" class=\"glyphicon glyphicon-remove\"></i> \n            </button>                     \n        </div>\n        <!--</div>-->\n    </legend>\n    <div class=\"cardview list-height col-md-12\">\n       <advanced-search id=\"asAdvancedSearch\" [FilterList]=\"_FilterList\" [TypeList]=\"_TypeList\" rpVisibleItems=5 (rpHidden)=\"renderAdvancedSearch($event)\" (rpChanged)=\"filterAdvancedSearch($event)\" style=\"display:none;\"></advanced-search>\n            <div id=\"tab\" class=\"btn-group\" data-toggle=\"buttons-radio\">\n              <a href=\"#tickets\" class=\"btn active \" data-toggle=\"tab\" style=\"height: 33px;\">Tickets</a>\n              <!--<a href=\"#dashboard\" class=\"btn \" data-toggle=\"tab\">Dashboard</a>-->\n\n              <button id=\"btnDashboard\" class=\"btn btn-sm \" type=\"button\" (click)=\"btnDashboard()\" title=\"Dashboard\" style=\"margin-bottom: 1px;margin-top: 1px;height: 33px;\">                                    \n              Dashboard\n              </button>\n            </div>\n             \n            <div class=\"tab-content\">\n              <div class=\"tab-pane active\" id=\"tickets\">\n              <div *ngIf=\"_shownull!=false\" style=\"color:#ccc;\">No result found!</div>\n\n\t\t\t\t\t\t<div *ngIf=\"_showListing\" class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-md-12\" style=\"overflow-x:auto;\">\n\t\t\t\t\t\t\t\t<table class=\"table table-striped table-condense table-hover tblborder\">\n\t\t\t\t\t\t\t\t\t<thead>\n\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t<th class=\"left\" style=\"width:6%\" title=\"Ticket No.\">Ticket No.</th>\n\t\t\t\t\t\t\t\t\t\t\t<th class=\"left\" style=\"width:8%\" title=\"Channel\">Channel</th>\n\t\t\t\t\t\t\t\t\t\t\t<th class=\"left\" style=\"width:8%\" title=\"User\">User</th>\n\t\t\t\t\t\t\t\t\t\t\t<th class=\"left\" style=\"width: 25%\"title=\"Message\">Description</th>\n\t\t\t\t\t\t\t\t\t\t\t<th class=\"left\" style=\"width:10%\" title=\"Location\">Location</th>\n\t\t\t\t\t\t\t\t\t\t\t<th class=\"left\" style=\"width:8%\" title=\"Date\">Date</th>\n\t\t\t\t\t\t\t\t\t\t\t<th class=\"left\" style=\"width:8%\" title=\"Time\">Time</th>\n\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t</thead>\n\n\t\t\t\t\t\t\t\t\t<tbody>\n\t\t\t\t\t\t\t\t\t\t<tr *ngFor=\"let obj of _ListingDataset.ticketData\">\n\t\t\t\t\t\t\t\t\t\t\t<td style=\"background-color: yellow; border-left: 1px solid white; border-right: 2px solid white; font-weight: bold;\" *ngIf=\"obj.t10=='WIP'\"><a (click)=\"goto(obj.t1)\">{{obj.t1}}</a></td>\n\t\t\t\t\t\t\t\t\t\t\t<td style=\"background-color: red; border-left: 1px solid white; border-right: 2px solid white; font-weight: bold;\" *ngIf=\"obj.t10=='New'\"><a style=\"color: white;\" (click)=\"goto(obj.t1)\">{{obj.t1}}</a></td>\n\t\t\t\t\t\t\t\t\t\t\t<td style=\"background-color: green; border-left: 1px solid white; border-right: 2px solid white; font-weight: bold;\" *ngIf=\"obj.t10=='Closed'\"><a style=\"color: white;\" (click)=\"goto(obj.t1)\">{{obj.t1}}</a></td>\n\t\t\t\t\t\t\t\t\t\t\t<td style=\"background-color: gray; border-left: 1px solid white; border-right: 2px solid white; font-weight: bold;\" *ngIf=\"obj.t10=='Rejected'\"><a style=\"color: white;\" (click)=\"goto(obj.t1)\">{{obj.t1}}</a></td>\n\t\t\t\t\t\t\t\t\t\t\t<td class=\"uni\">{{obj.t15}}</td>                                   \n\t\t\t\t\t\t\t\t\t\t\t<td class=\"uni\">{{obj.t5}}</td>\n\t\t\t\t\t\t\t\t\t\t\t<td class=\"uni\">{{obj.t7}}</td>\n\t\t\t\t\t\t\t\t\t\t\t<td >{{obj.t8}}</td>\n\t\t\t\t\t\t\t\t\t\t\t<td >{{obj.shootDate}}</td>\n\t\t\t\t\t\t\t\t\t\t\t<td >{{obj.shootTime}}</td>\n\t\t\t\t\t\t\t\t\t\t</tr> \n\t\t\t\t\t\t\t\t\t</tbody>\n\t\t\t\t\t\t\t\t</table>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div *ngIf=\"_showListing\" style=\"text-align:center\">Total {{ _ListingDataset.totalCount }}</div>\n              </div>\n              <div class=\"tab-pane\" id=\"dashboard\" >\n              <div class=\"col-md-12\">\n        <div class=\"form-group\">\n          <div>\n            <sebm-google-map [latitude]=\"lat\" [longitude]=\"lng\"></sebm-google-map>\n            <div id=\"map-canvas\" class=\"col-md-12\" style=\"height:85%; border: 1px solid rgb(188, 186, 184);\"></div>\n          </div>\n        </div>\n      </div>\n            </div>\n            </div>\n    </div>\n    </form>\n\n    <div id=\"popup\" class=\"modal fade\" role=\"dialog\">\n    <div style=\"padding: 20px;height: 90%;width: 90%;padding-bottom: 115px;\" class=\"modal-dialog modal-lg\"><!--style=\"padding: 20px; width: auto;\"-->\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" (click)=\"goClosed()\">&times;</button>\n            <h4 id=\"popuptitle\" class=\"modal-title\">Ticket Details </h4>\n        </div>\n        <div id=\"popupbody\" class=\"modal-body\">\n        <!--this is ticket detail form start -->\n        <div class=\"container\">\n          <div class=\"row clearfix\">\n            <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n              <div id=\"snackbar1\" [hidden]=\"_alertflag\">>{{_alertmsg}}</div>\n              <form class=\"form-horizontal\">\n              <div class=\"col-md-12\">\n          \n            <div class=\"col-md-6\">\t\n                <sebm-google-map [latitude]=\"lat\" [longitude]=\"lng\"></sebm-google-map>\n                <div id=\"map-canvasTwo\" class=\"col-md-12\" style=\"height:75%;left:-14px;border: 1px solid rgb(188, 186, 184);\"></div>\t\t\t\t\t\t\n            </div>\n      \n      \n        <div class=\"col-md-6\">\n            <div class=\"form-group\">\n                  <img src=\"{{image}}\" alt={{img}} style=\"width:100%;height:75%;\"/>\n            </div>\t\t\t\t\n        </div>\n\t\t</div>\n\t\t<div class=\"col-md-12\">\n              <div class=\"col-md-6\">\n                  <div class=\"form-group\">\n                  \n                    <label class=\"row col-md-4\">Status </label>\n                    <div class=\"row col-md-8\">\n                        <select [(ngModel)]=\"_obj.t10\" class=\"form-control col-md-0\" [ngModelOptions]=\"{standalone: true}\" required>\n                          <option *ngFor=\"let item of ref._lov3.refTicketStatus\" value=\"{{item.value}}\">{{item.value}}</option>\n                        </select>\n                    </div>\n                  </div>\n                  \n                  <div class=\"form-group\">\t\t\t\t\t\t\t\n                  \n                      <label class=\"row col-md-4\">User </label>\n                      <div class=\"row col-md-8\">\n                        <div class=\"uni\">\n                          <input disabled readonly type=\"text\" [(ngModel)]=\"_obj.t5\" class=\"form-control\" [ngModelOptions]=\"{standalone: true}\">\n                        </div>\n                      </div>\n                  </div>\n                  \n                  <div class=\"form-group\">\t\n                      <label class= \"row col-md-4\">Region</label>\n                      <div class= \"row col-md-8\">                  \n                          <select disabled readonly [(ngModel)]=\"_obj.t12\"class=\"form-control\" [ngModelOptions]=\"{standalone: true}\" required>\n                              <option *ngFor=\"let c of ref._lov3.refstate\" value=\"{{c.value}}\">{{c.caption}}</option>\n                          </select>\n                      </div>\t\t\t\t\t\t\t\t\t\t\t\n                  </div>\n                    <div class=\"form-group\">\t\t\t\t\t\n                      <label class=\"row col-md-4\">Location </label>\n                      <div class=\"row col-md-8\">\n                          <input disabled readonly type=\"text\" [(ngModel)]=\"_obj.t8\" class=\"form-control\" [ngModelOptions]=\"{standalone: true}\">\n                      </div>\n                    </div>\t\t\t\t\t\t\t\t\t\t\t\n                    \n                    <div class=\"form-group\">\t\n                      <label class=\"row col-md-12\">Description</label>\n                      <div class=\"row col-md-12\">\n                          <textarea disabled readonly id=\"textarea-custom\" class=\"form-control\" rows=\"3\" style=\"width: 305px;\" [(ngModel)]=\"_obj.t7\" [ngModelOptions]=\"{standalone: true}\"></textarea>\n                      </div>\n                    </div>\n              \n              </div>\n              \n              <div class=\"col-md-6\">\n                  <div class=\"form-group\">\n                      \n                    <label class=\"row col-md-4\">Ticket No.</label>\n                      <div class=\"row col-md-8\">\n                          <input disabled readonly type=\"text\" [(ngModel)]=\"_obj.t1\" class=\"form-control\" [ngModelOptions]=\"{standalone: true}\">\n                      </div>\t\t\t\t\t\t\t\n                  </div>\n                  <div class=\"form-group\">\t\n                      <label class=\"row col-md-4\">Phone No. </label>\n                      <div class=\"row col-md-8\">\n                        <div class=\"uni\">\n                          <input disabled readonly type=\"text\" [(ngModel)]=\"_obj.t9\" class=\"form-control\" [ngModelOptions]=\"{standalone: true}\">\n                        </div>\n                      </div>\t\t\t\t\t\t\n                  </div>\n                  <div class=\"form-group\">\n              \n                      <label class=\"row col-md-4\">Channel </label>\n                      <div class=\"row col-md-8\">\n                        <div class=\"uni\">\n                          <input disabled readonly type=\"text\" [(ngModel)]=\"_obj.t15\" class=\"form-control\" [ngModelOptions]=\"{standalone: true}\">\n                        </div>\n                      </div>\n                  </div>\n                  <div class=\"form-group\">\t\n                      <label class=\"row col-md-4\">Date Time </label>\n                      <div class=\"row col-md-8\">\n                          <input disabled readonly type=\"text\" [(ngModel)]=\"dateTime\" class=\"form-control\" [ngModelOptions]=\"{standalone: true}\">\n                      </div>\n                  </div>\n                  \n                  <div class=\"form-group\">\t\n                      <label class=\"row col-md-12\">Message</label>\n                      <div class=\"row col-md-12\">\n                          <textarea id=\"textarea-custom\" class=\"form-control\" rows=\"3\" style=\"width: 305px;\" [(ngModel)]=\"_obj.t20\" [ngModelOptions]=\"{standalone: true}\"></textarea>\n                      </div>\n                      <div class=\"uni row col-md-12\">\n                          <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goReply()\" style=\"margin-top: 5px;\">Reply</button>\n                      </div>\n                  </div>\n              </div>\t\t\t\t\n            </div>\n              </form>\n            </div>\n          </div>\n        </div>\n        <!--this is ticket detail form end -->\n        \n        <div [hidden]=\"!_doLoading\">\n          <div  id=\"loader\" class=\"modal\" ></div>\n        </div>\n\n            <div style=\"\">&nbsp;</div>\n        </div>\n        <div class=\"modal-footer\">\n        <button class=\"btn btn-sm btn-primary\" id=\"vh_save_btn\" type=\"button\" (click)=\"goUpdate()\">Update</button>\n            <button type=\"button\" class=\"btn btn-sm btn-primary\" data-dismiss=\"modal\" (click)=\"goClosed()\">Close</button>\n        </div>\n      </div>\n    </div>\n  </div>\n\n\n  </div>\n  </div>\n  \n  <div [hidden]=\"_mflag\">\n      <div  id=\"loader\" class=\"modal\" ></div>\n  </div>\n"
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, rp_references_1.RpReferences, router_1.Router, rp_http_service_1.RpHttpService, GeoLocationService_1.GeoLocationService, rp_client_util_1.ClientUtil, core_3.CookieService])
    ], TicketList);
    return TicketList;
}());
exports.TicketList = TicketList;
//# sourceMappingURL=ticket-list.component.js.map