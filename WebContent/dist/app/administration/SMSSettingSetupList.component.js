"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
// RP Framework
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_client_util_1 = require('../util/rp-client.util');
core_2.enableProdMode();
var SMSSettingSetupListComponent = (function () {
    function SMSSettingSetupListComponent(ics, _router, http) {
        this.ics = ics;
        this._router = _router;
        this.http = http;
        // Application Specific
        this._searchVal = ""; // simple search
        this._flagas = true; // flag advance search
        this._showListing = false;
        this._col_list = [{ "col_no": "1", "flag": false }, { "col_no": "2", "flag": false }];
        this._sorting = { _sort_type: "asc", _sort_col: "1" };
        this._mflag = true;
        this._util = new rp_client_util_1.ClientUtil();
        this.sessionAlertMsg = "";
        this._smsobj = {
            "data": [{ "code": "", "desc": "", "id": 0, "syskey": 0, "serviceCode": "", "serviceDesc": "", "funCode": "", "funDesc": "", "site": "", "from": "", "to": "", "fromMsg": "", "toMsg": "", "message": "", "merchantID": "", "operatorType": "", "active": "", "sessionID": "", "userID": "" }],
            "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0
        };
        this._pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 0 };
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this.search();
        }
    }
    // list sorting part
    SMSSettingSetupListComponent.prototype.changeDefault = function () {
        for (var i = 0; i < this._col_list.length; i++) {
            this._col_list[i].flag = false;
        }
    };
    SMSSettingSetupListComponent.prototype.addSort = function (e) {
        for (var i = 0; i < this._col_list.length; i++) {
            if (this._col_list[i].col_no == e) {
                var _tmp_flag = this._col_list[i].flag;
                this.changeDefault();
                this._col_list[i].flag = !_tmp_flag;
                this._sorting._sort_type = !_tmp_flag ? "desc" : "asc";
                this._sorting._sort_col = e;
                break;
            }
        }
    };
    SMSSettingSetupListComponent.prototype.changedPager = function (event) {
        if (this._smsobj.totalCount != 0) {
            this._pgobj = event;
            var current = this._smsobj.currentPage;
            var size = this._smsobj.pageSize;
            this._smsobj.currentPage = this._pgobj.current;
            this._smsobj.pageSize = this._pgobj.size;
            if (this._pgobj.current != current || this._pgobj.size != size) {
                this.search();
            }
        }
    };
    SMSSettingSetupListComponent.prototype.search = function () {
        var _this = this;
        try {
            this._mflag = false;
            this.http.doGet(this.ics.cmsurl + 'serviceCMS/getSMSSettingList?searchVal=' + this._smsobj.searchText + '&pagesize=' + this._smsobj.pageSize + '&currentpage=' + this._smsobj.currentPage + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (response) {
                _this._mflag = true;
                if (response.msgCode == "0016") {
                    _this.sessionAlertMsg = response.msgDesc;
                    _this.showMessage(response.msgDesc, false);
                }
                else {
                    if (response.totalCount == 0) {
                        _this._smsobj = response;
                        _this.showMessage("Data not found!", false);
                    }
                    if (response.data != null) {
                        _this._showListing = true;
                        if (!(response.data instanceof Array)) {
                            var m = [];
                            m[0] = response.data;
                            _this._smsobj.data = m;
                            _this._smsobj.totalCount = response.totalCount;
                        }
                        else {
                            _this._smsobj = response;
                        }
                    }
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    SMSSettingSetupListComponent.prototype.searchKeyup = function (e) {
        if (e.which == 13) {
            this._smsobj.currentPage = 1;
            this._msghide = true;
            this._recordhide = false;
            this.search();
        }
    };
    SMSSettingSetupListComponent.prototype.Searching = function () {
        this._smsobj.currentPage = 1;
        this._msghide = true;
        this._recordhide = false;
        this.search();
    };
    SMSSettingSetupListComponent.prototype.goto = function (p) {
        this._router.navigate(['/SMS Setting', 'read', p]);
    };
    // showMessage() {
    //    jQuery("#sessionalert").modal();
    //    Observable.timer(3000).subscribe(x => {
    ///       this.goLogOut();
    //    });
    //}
    SMSSettingSetupListComponent.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    SMSSettingSetupListComponent.prototype.goNew = function () {
        this._router.navigate(['/SMS Setting', 'new']);
    };
    SMSSettingSetupListComponent.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "primary", "t3": msg });
        }
    };
    SMSSettingSetupListComponent = __decorate([
        core_1.Component({
            selector: 'smssettingsetup-list',
            template: " \n    <div class=\"container col-md-12 col-sm-12 col-xs-12\">\n\t<form class=\"form-horizontal\">\n\t\t<legend>Message Setting List</legend>\n\t\t<div class=\"cardview list-height\">  \n\t\t\t<div class=\"row col-md-12\">\n\t\t\t\t<div class=\"row col-md-3\">\n\t\t\t\t\t<div  class=\"input-group\">\n\t\t\t\t\t\t<span class=\"input-group-btn\">\n\t\t\t\t\t\t\t<button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goNew();\">New</button>\n\t\t\t\t\t\t</span> \n\t\t\t\t\t\t<input id=\"textinput\" name=\"textinput\" type=\"text\"  placeholder=\"Search\" [(ngModel)]=\"_smsobj.searchText\" (keyup)=\"searchKeyup($event)\" maxlength=\"30\" class=\"form-control input-sm\">\n\t\t\t\t\t\t<span class=\"input-group-btn\">\n\t\t\t\t\t\t\t<button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"Searching()\" >\n\t\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-search\"></span>Search\n\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t</span>        \n\t\t\t\t\t</div> \n\t\t\t\t</div>\t\t\n                    <div class=\"pagerright\">\n\t\t\t\t\t    <adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"_smsobj.totalCount\" (rpChanged)=\"changedPager($event)\" ></adminpager> \n\t\t\t\t\t</div>\n            </div>\n            <div *ngIf=\"_showListing\">\n    \t\t  <table class=\"table table-striped table-condensed table-hover tblborder\" id=\"tblstyle\">\n    \t\t\t<thead>\n    \t\t\t <tr>\n    \t\t\t\t<th> ID. </th>\n    \t\t\t\t<th>Service </th>\n    \t\t\t\t<th>Function</th>\n    \t\t\t\t<th>Site</th>\n            <th>From Message</th>\n            <th>To Message</th>\n    \t\t\t\t<th class=\"center\">Merchant ID</th>\n    \t\t\t\t<!--<th>Operator</th>-->\n            <th>Active</th>\n            <!--<th>Language</th>-->     \n    \t\t\t</tr>\n    \t\t  </thead>\n    \t\t  <tbody>\n    \t\t\t<tr *ngFor=\"let obj of _smsobj.data\">\n    \t\t\t\t<td><a (click)=\"goto(obj.id)\">{{obj.id}}</a></td>\n    \t\t\t\t<td>{{obj.serviceDesc}}</td>\n    \t\t\t\t<td>{{obj.funDesc}}</td>\n    \t\t\t\t<td *ngIf =\"obj.from == '1' \" >From</td>\n            <td *ngIf =\"obj.to == '1' \" >To</td>\n            <td *ngIf =\"obj.both == '1' \" >Both</td>\n    \t\t\t\t<td>{{obj.fromMsg}} </td>\n    \t\t\t\t<td > {{obj.toMsg}} </td>\n    \t\t\t\t<td class=\"center\">{{obj.merchantID}}</td>\n    \t\t\t\t<!--<td >{{obj.operatorType}}</td>-->\n    \t\t\t\t<td *ngIf =\"obj.active == 0\">No</td>\n            <td *ngIf =\"obj.active == 1\">Yes</td>\n            <!--<td *ngIf=\"obj.language==''\"></td>\n            <td *ngIf=\"obj.language==null\"></td>\n            <td *ngIf =\"obj.language == 1\">English</td>\n            <td *ngIf =\"obj.language == 2\">Myanmar</td>-->\n    \t\t\t</tr>  \n    \t\t  </tbody>\n              </table>\n            </div>\n            <div align=\"center\" *ngIf=\"_showListing\" >\n                Total {{_smsobj.totalCount}}\n            </div>\n\t\t</div> \n\t</form>\n</div>\n    <div [hidden] = \"_mflag\">\n        <div class=\"loader modal\"id=\"loader\"></div>\n    </div>\n   "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_http_service_1.RpHttpService])
    ], SMSSettingSetupListComponent);
    return SMSSettingSetupListComponent;
}());
exports.SMSSettingSetupListComponent = SMSSettingSetupListComponent;
//# sourceMappingURL=SMSSettingSetupList.component.js.map