"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var Rx_1 = require('rxjs/Rx');
var ng2_qrcode_1 = require('ng2-qrcode');
core_2.enableProdMode();
var ChatList = (function () {
    function ChatList(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        //objects
        this.chatList = [];
        this.chatdata = [];
        this.tmpchatdata = [];
        this.userdata = [];
        this.users = [];
        //chatLis2:any=[];
        this.tempchat = [];
        this.chatUser = [];
        this.passData = {};
        this.start = 0;
        this.end = 0;
        this.conversionList = [];
        this.userstatus = 0;
        this.chatstatus = 0;
        this.photos = [];
        this.postMsg = "";
        this.isgroup = 0;
        this.msgList = [];
        this.syskeycss = "";
        this.nore = 0;
        this.channelSyskey = "";
        this.channelmyName = "";
        this.region = "";
        this.uploadDataListResize = [];
        this._fileName = "";
        this._file = null;
        this.flagImg = false;
        this._mflag = false;
        this._img = "";
        this.sessionAlertMsg = "";
        this.userList = [];
        this.uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = true;
            this.profileImageLink = this.ics._profileImage1;
            // this.FromimageLink = this.ics._imglink + "upload/image";
            // this.ToimageLink = this.ics._imglink + "uploads/image";
            this.FromimageLink = this.ics._imglink + "uploads/smallImage";
            this.ToimageLink = this.ics._imglink + "uploads/smallImage";
        }
    }
    ChatList.prototype.goGoogleMap = function (location) {
        var string = location.replace(/\//g, ",");
        window.open('https://www.google.com/maps/search/?api=1&query=' + string, '_system');
    };
    ChatList.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
                _this.channelSyskey = params['channelSyskey'];
                _this.channelmyName = params['channelName'];
                _this.region = params['region'];
                _this.userstatus = 0;
                _this.chatstatus = 0;
                _this.getchatUserList(0);
                _this.getChatContact();
            }
        });
    };
    ChatList.prototype.searchChat = function (ev) {
        var val = ev.target.value;
        if (val == undefined || val.trim() == '') {
            val = '';
        }
        else {
            val = val.trim();
        }
        // if the value is an empty string don't filter the items
        this.tmpchatdata = this.tempchat.filter(function (res) {
            return (res.t2.toLowerCase().indexOf(val.toLowerCase()) > -1 || res.t3.toLowerCase().indexOf(val.toLowerCase()) > -1);
        });
        if (this.tmpchatdata.length == 0) {
            this.chatstatus = 0;
        }
        else {
            this.chatstatus = 1;
            this.chatgroup();
        }
    };
    ChatList.prototype.searchUser = function (ev) {
        var val = ev.target.value;
        if (val == undefined || val.trim() == '') {
            val = '';
        }
        else {
            val = val.trim();
        }
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.userdata = this.userList.data.filter(function (userdata) {
                return (userdata.t3.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
            if (this.userdata.length == 0) {
                this.userstatus = 0;
            }
            else {
                this.userstatus = 1;
            }
        }
        else {
            this.userdata = this.userList.data.filter(function (userList) {
                return (userList.t3.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
            if (this.userdata.length == 0) {
                this.userstatus = 0;
            }
            else {
                this.userstatus = 1;
            }
        }
    };
    ChatList.prototype.goBack = function () {
        this._router.navigate(['/channelList']);
    };
    ChatList.prototype.scrollToBottom = function () {
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
        //window.scrollTo(0, document.body.scrollHeight);
    };
    ChatList.prototype.getchatUserList = function (start) {
        var _this = this;
        this._mflag = false;
        var url = this.ics.chaturl + "serviceChat/searchChatList1?syskey=" + this.channelSyskey + "&role=&domain=DC001&appId=010";
        var json = { start: 0, end: 10 };
        this.http.doPost(url, json).subscribe(function (data) {
            if (data.data != undefined || data.data != null || data.data != '') {
                if (!(data.data instanceof Array)) {
                    var m = [];
                    m[0] = data.data;
                    _this.chatList = m;
                    //this.chatLis2 = m; 
                    _this.tempchat = m;
                }
                else {
                    _this.chatList = data.data;
                    //this.chatLis2=data.data; 
                    _this.tempchat = data.data;
                }
                for (var i = 0; i < data.data.length; i++) {
                    if (data.data[i].t1.includes("YCDC")) {
                        data.data.splice(i, 1);
                        i--;
                    }
                }
                _this.nore = 1;
                if (_this.chatList.length > 0) {
                    _this.viewChat(_this.chatList[0]);
                    _this.getchatUserData();
                }
            }
            else {
                _this.nore = 0;
                _this.chatList = [];
            }
            _this._mflag = true;
        }, function (error) {
            console.log("signin error=" + error.status);
        });
    };
    ChatList.prototype.getChatContact = function () {
        var _this = this;
        var json = {
            sessionID: this.ics._profile.sessionID,
            userID: this.ics._profile.userID
        };
        console.log("request getContact =" + JSON.stringify(json));
        var url = this.ics.cmsurl + 'serviceCMS/getChatContact?channelkey=' + this.channelSyskey;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data.msgCode == "0016") {
                _this.sessionAlertMsg = data.msgDesc;
                _this.showMessage();
            }
            else {
                if (data.contactArr != null) {
                    if (!(data.contactArr instanceof Array)) {
                        var m = [];
                        m[0] = data.contactArr;
                        _this.userList.data = m;
                    }
                    else {
                        _this.userList.data = data.contactArr;
                    }
                }
                else {
                    _this.userList.data = [];
                }
            }
        }, function (error) {
            console.log("signin error=" + error.status);
            //this.nores = 0;            
        });
    };
    ChatList.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this.goLogOut();
        });
    };
    ChatList.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    ChatList.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    ChatList.prototype.viewChat = function (obj) {
        this._mflag = false;
        this.syskey = this.channelSyskey; //1060;this.userDataChat.sKey
        this.passData = obj;
        this.syskeycss = (this.passData.syskey || this.passData.sysKey);
        this.role = (this.passData.syskey || this.passData.sysKey) + "-" + this.syskey;
        this.channelName = this.syskey + "-" + (this.passData.syskey || this.passData.sysKey);
        this.usersyskey = '0';
        //to check
        this.userst = this.syskey;
        this.syskey = (this.passData.syskey || this.passData.sysKey);
        this.start = 0;
        this.end = 0;
        //this.isLoading = true;
        this.getConversion(this.start);
    };
    // {
    //     "groupName":"Thura Aung",
    //     "isGroup":"0",
    //     "n1":"0",
    //     "n20":"",
    //     "t2":"Welcome",
    //     "t3":"",
    //     "t5":"1059",
    //     "t6":"491",
    //     "t9":"",
    //     "t11":"",
    //     "t12":"android",
    //     "t15":"",
    //     "t19":""
    // }
    ChatList.prototype.sendMessagePrepare = function (obj) {
        this.passData.syskey = obj.syskey;
        this.passData.channelkey = "";
        this.passData.t2 = obj.t3;
        this.passData.t19 = "";
    };
    ChatList.prototype.viewUser = function (obj) {
        var abc = [];
        for (var i = 0; i < this.tempchat.length; i++) {
            if (this.tempchat[i].syskey == obj.syskey) {
                abc = this.tempchat[i];
            }
        }
        if (abc.length == 0) {
            this._mflag = true;
            this.passData = obj;
            this.syskeycss = (this.passData.syskey || this.passData.sysKey);
            this.sendMessagePrepare(obj);
            this.msgList = [];
        }
        else {
            this.viewChat(abc);
        }
    };
    ChatList.prototype.getConversion = function (start) {
        var _this = this;
        this._mflag = false;
        this.end = this.end + 10;
        var json = { start: start, end: this.end };
        console.log("channelGroup=" + this.role + "&syskey=" + this.syskey + "&channelSyskey=" + this.passData.channelkey + "&channelName=" + this.channelName);
        var url = this.ics.chaturl + "serviceChat/getConversationForAll?channelGroup="
            + this.role + "&syskey=" + this.syskey + "&channelSyskey="
            + this.passData.channelkey + "&channelName=" + this.channelName;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data.totalCount > 0) {
                var tempArray = [];
                if (!Array.isArray(data.data)) {
                    tempArray.push(data.data);
                    data.data = tempArray;
                }
            }
            else {
                _this.conversionList = [];
            }
            if (data.data.length > 0) {
                _this.end = _this.start + data.data.length;
                _this.conversionList = [];
                _this.conversionList = data.data;
                _this.getGroupData();
            }
            //this.scrollToBottom();
            _this._mflag = true;
        }, function (error) {
            console.log("signin error=" + error.status);
        });
    };
    ChatList.prototype.uploadedFileImage = function (event) {
        var _this = this;
        //this.uploadDataListResize = [];
        if (event.target.files.length == 1) {
            this._fileName = event.target.files[0].name;
            this._file = event.target.files[0];
            var index = this._fileName.lastIndexOf(".");
            var imagename = this._fileName.substring(index);
            imagename = imagename.toLowerCase();
            var url = this.ics.chaturl + 'fileAdm/fileupload?f=uploads&fn=' + this._fileName;
            if (imagename == ".jpg" || imagename == ".jpeg" || imagename == ".png") {
                console.log("imgUrl: " + this.ics._imgurl);
                this.http.upload(url, this._file).subscribe(function (data) {
                    if (data.code === 'SUCCESS') {
                        _this.flagImg = true;
                        console.log("Image: " + JSON.stringify(data));
                        jQuery("#PhotoPopup").modal();
                        //this.popupMessage("Upload Successful!");
                        _this.showMsg("Upload Successful!", true);
                        var img = _this._fileName;
                        _this.uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
                        _this.uploadData.name = data.fileName;
                        _this.uploadData.url = data.filePath;
                        _this._fileName = "";
                    }
                    else {
                        jQuery("#PhotoPopup").modal('hide');
                        // this.popupMessage("Upload Unsuccessful! Please Try Again...");
                        _this.showMsg("Upload Unsuccessful! Please Try Again...", false);
                    }
                }, function (error) { }, function () { });
            }
            else {
                this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": "Choose Image Associated!" });
            }
        }
        else {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "info", "t3": "Upload Image!" });
        }
    };
    ChatList.prototype.onClose = function () {
        this.uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
        this.postMsg = "";
        jQuery("#PhotoPopup").modal('hide');
    };
    ChatList.prototype.sendMsg = function () {
        var _this = this;
        this._mflag = false;
        console.log("seend >>>>");
        this.status = true;
        this.syskey = this.channelSyskey; //1060;
        this.status = true;
        this.scrollToBottom();
        var from, to, sendsys, photo, userID, userName;
        //if (this.postMsg != '' && this.postMsg != null && this.postMsg != undefined) {
        if (this.isgroup == 1) {
            from = this.syskey;
            to = (this.passData.syskey || this.passData.sysKey);
            sendsys = this.syskey;
            this.usersyskey = '0';
        }
        else {
            // one to one
            from = this.syskey;
            to = (this.passData.syskey || this.passData.sysKey);
            console.log("to>> " + to);
        }
        var json = {
            t2: this.postMsg,
            t3: this.uploadData.url,
            t5: from,
            t6: to,
            t9: sendsys,
            t11: '',
            n1: this.usersyskey,
            t12: "android",
            t15: "",
            isGroup: this.isgroup,
            n20: this.passData.channelkey,
            groupName: this.passData.t2,
            t19: this.passData.t19
        };
        console.log("request sendmsg =" + JSON.stringify(json));
        var url = this.ics.chaturl + "serviceChat/sendMessage1";
        this.http.doPost(url, json).subscribe(function (result) {
            console.log("response sendmsg =" + JSON.stringify(result));
            if (result.totalCount != "0") {
                var tempArray = [];
                if (!Array.isArray(result.data)) {
                    tempArray.push(result.data);
                    result.data = tempArray;
                }
                if (result.data.length > 0) {
                    _this.conversionList = result.data;
                    _this.uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
                    _this.msgList = [];
                    _this.postMsg = '';
                    _this.photos = [];
                    _this.getGroupData();
                    //this.nores = 1;
                    _this.end = result.data.length;
                    _this.getConversion(0);
                    _this.scrollToBottom();
                    _this.end = 0;
                }
            }
            else {
                //this.nores = 0;
                _this.conversionList = [];
                //this.showMsgAlert("Send Failed! Please try again.");//atn
                _this.showMsg("Send Failed! Please try again.", false);
            }
            jQuery("#PhotoPopup").modal('hide');
            _this.status = false;
            _this._mflag = true;
        }, function (error) {
            console.log("signin error=" + error.status);
            _this.status = false;
            //this.nores = 0;              
        });
        //}
    };
    ChatList.prototype.getGroupData = function () {
        var group_to_values = this.conversionList.reduce(function (obj, item) {
            obj[item.t7] = obj[item.t7] || [];
            obj[item.t7].push(item);
            return obj;
        }, {});
        var groups = Object.keys(group_to_values).map(function (key) {
            return { t7: key, data: group_to_values[key] };
        });
        for (var i = 0; i < groups.length; i++) {
            groups[i].t7 = this.getTransformDate(groups[i].t7);
            for (var j = 0; j < groups[i].data.length; j++) {
                if (groups[i].data[j].t3.indexOf("''") > -1) {
                    groups[i].data[j].t3 = this.getQuotes(groups[i].data[j].t3);
                }
            }
        }
        this.msgList = groups;
        console.log("List=" + JSON.stringify(this.msgList));
    };
    ChatList.prototype.getQuotes = function (str) {
        if (str.indexOf("''") > -1) {
            var re = /''/gi;
            str = str.replace(re, "'");
        }
        return str;
    };
    ChatList.prototype.getTransformDate = function (date) {
        var tranDate;
        var getdate;
        var month;
        var year;
        var day;
        year = date.slice(0, 4);
        month = date.slice(4, 6);
        day = date.slice(6, 8);
        switch (month) {
            case '01':
                month = 'Jan';
                break;
            case '02':
                month = 'Feb';
                break;
            case '03':
                month = 'Mar';
                break;
            case '04':
                month = 'Apr';
                break;
            case '05':
                month = 'May';
                break;
            case '06':
                month = 'Jun';
                break;
            case '07':
                month = 'Jul';
                break;
            case '08':
                month = 'Aug';
                break;
            case '09':
                month = 'Sep';
                break;
            case '10':
                month = 'Oct';
                break;
            case '11':
                month = 'Nov';
                break;
            case '12':
                month = 'Dec';
                break;
            default:
        }
        tranDate = month + " " + day + ", " + year;
        return tranDate;
    };
    ChatList.prototype.chatgroup = function () {
        var sortedContacts = this.tmpchatdata.sort(function (a, b) {
            if (a.t16 < b.t16)
                return 1;
            if (a.t16 > b.t16)
                return -1;
            return 0;
        });
        var group_to_values = sortedContacts.reduce(function (obj, item) {
            obj[item.t16] = obj[item.t16] || [];
            obj[item.t16].push(item);
            return obj;
        }, {});
        var groups = Object.keys(group_to_values).map(function (key) {
            return { t16: key, data: group_to_values[key] };
        });
        this.chatdata = groups;
    };
    ChatList.prototype.getchatUserData = function () {
        var monthNames = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        for (var i = 0; i < this.chatList.length; i++) {
            var modifiedDateStr = this.chatList[i].modifiedDate;
            // if (this.isToday(modifiedDateStr)) {
            //   this.chatList[i].modifiedDate = time;
            // } else {
            var monthStr = modifiedDateStr.substring(4, 6);
            var monthInt = parseInt(monthStr);
            var month = monthNames[monthInt];
            var dateStr = modifiedDateStr.substring(6);
            var date = parseInt(dateStr);
            this.chatList[i].modifiedDate = date + " " + month;
        }
        var sortedContacts = this.chatList.sort(function (a, b) {
            if (a.t16 < b.t16)
                return 1;
            if (a.t16 > b.t16)
                return -1;
            return 0;
        });
        var group_to_values = sortedContacts.reduce(function (obj, item) {
            obj[item.t16] = obj[item.t16] || [];
            obj[item.t16].push(item);
            return obj;
        }, {});
        var groups = Object.keys(group_to_values).map(function (key) {
            return { t16: key, data: group_to_values[key] };
        });
        this.chatList = groups;
    };
    ChatList.prototype.doRefresh = function (refresher) {
        //this.refresh = "do";
        console.log('Begin async operation', refresher);
        console.log('this.start=' + this.start);
        this.start = 0;
        this.getConversion(this.start);
        setTimeout(function () {
            console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    };
    ChatList.prototype.url = function (string) {
        var re = new RegExp("^(http|https)://", "i");
        return re.test(string);
    };
    ChatList.prototype.searchKeyup = function (e) {
        if (e.which == 13) {
            this.sendMsg();
        }
    };
    ChatList.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    ChatList = __decorate([
        core_1.Component({
            selector: 'ChatList',
            entryComponents: [ng2_qrcode_1.QRCodeComponent],
            template: "\n    <div class=\"container\">\n\t\t<fieldset style=\"padding:.35em .625em .75em;\">\n\t\t\t<legend>\n\t\t\t\t<div style=\"float:left;display:inline;font-size:18px;margin-left:10px;\">\n\t\t\t\t{{channelmyName}} by {{region}}\n\t\t\t\t\t<button id=\"btnPrint\" class=\"btn btn-sm btn-primary\" type=\"button\" style=\"height:30px;\" (click)=\"goBack()\" title=\"back\">                                    \n\t\t\t\t\tBack\n\t\t\t\t\t</button>\n\t\t\t\t</div>\n\t\t\t</legend>\n        </fieldset>\t\t\t\n\t\t\t<div class=\"col-sm-3 chat_sidebar\" *ngIf=\"nore!=0\">\t\t\t\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<tabset>\n\t\t\t\t\t\t<tab title='Chat'>\n\t\t\t\t\t\t<div id=\"custom-search-input\" style=\"background-color:  #fff;\">\n\t\t\t\t\t\t\t<div class=\"input-group col-md-12\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"search-query form-control\" placeholder=\"Search\" (keyup)=\"searchChat($event)\"/>\n\t\t\t\t\t\t\t\t<button class=\"btn btn-danger\" type=\"button\">\n\t\t\t\t\t\t\t\t<span class=\" glyphicon glyphicon-search\"></span>\n\t\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\n\t\t\t\t\t\t</div>\t\t\t\t\n\t\t\t\t\t\t<div class=\"member_list\" style=\"background-color:  #fff;\">\n\t\t\t\t\t\t\t<ul class=\"list-unstyled\">\n                                <div *ngIf=\"chatstatus==0\" >\n                                    <div *ngIf=\"chatList.length > 0\">\n                                    <div *ngFor=\"let cl of chatList\" >\n                                    <li class=\"chat left clearfix\" *ngFor=\"let obj of cl.data\" (click)=\"viewChat(obj)\" [class.doHighLight]=\"syskeycss == (obj.sysKey||obj.syskey)\">\n                                        <span class=\"chat-img pull-left\">\n                                            <img src=\"{{profileImageLink}}{{obj.t18}}\" onError=\"this.src = 'image/user-icon.png'\" >\n                                        </span>\n                                        <div class=\"chat-body clearfix\">\n                                            <div class=\"header_sec\">\n                                                <strong class=\"primary-font uni\">{{obj.t2}}</strong> \n                                                <strong class=\"pull-right\">{{obj.modifiedDate}}</strong>\n                                            </div>\n                                            <div class=\"contact_sec\">\t\t\t\t\t\t\t\t\t\t\t\t\n                                                <p class=\"primary-font uni\" *ngIf=\"obj.t3==''\">\n                                                <span class=\"fa fa-camera\"></span>\n                                                Photo Message</p>\n                                                <p class=\"primary-font uni\" *ngIf=\"obj.t3!=''\">{{obj.t3}}</p> \n                                            </div>\n                                        </div>\n                                    </li>\n                                </div>\n                                </div>\n\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t</div>\n                                <div *ngIf=\"chatstatus==1\">\n                                    <div *ngIf=\"chatdata.length > 0\">\n                                            <div *ngFor=\"let cl of chatdata\">\n                                            <li class=\"chat left clearfix\" *ngFor=\"let obj of cl.data\" (click)=\"viewChat(obj)\" [class.doHighLight]=\"syskeycss == (obj.sysKey||obj.syskey)\">\n                                                <span class=\"chat-img pull-left\">\n                                                    <img src=\"{{profileImageLink}}{{obj.t18}}\" onError=\"this.src = 'image/user-icon.png'\" >\n                                                </span>\n                                                <div class=\"chat-body clearfix\">\n                                                    <div class=\"header_sec\">\n                                                        <strong class=\"primary-font uni\">{{obj.t2}}</strong> \n                                                        <strong class=\"pull-right\">{{obj.modifiedDate}}</strong>\n                                                    </div>\n                                                    <div class=\"contact_sec\">\t\t\t\t\t\t\t\t\t\t\t\t\n                                                        <p class=\"primary-font uni\" *ngIf=\"obj.t3==''\">\n                                                        <span class=\"fa fa-camera\"></span>\n                                                        Photo Message</p>\n                                                        <p class=\"primary-font uni\" *ngIf=\"obj.t3!=''\">{{obj.t3}}</p> \n                                                    </div>\n                                                </div>\n                                            </li>\n                                        </div>\n                                    </div>\n\t\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</tab>\n\t\t\t\t\t\t<tab title='Users'>\n\t\t\t\t\t\t\t<div id=\"custom-search-input\" style=\"background-color:#fff;\">\n\t\t\t\t\t\t\t\t<div class=\"input-group col-md-12\">\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"search-query form-control\" placeholder=\"Search\" (keyup)=\"searchUser($event)\"/>\n\t\t\t\t\t\t\t\t\t<button class=\"btn btn-danger\" type=\"button\">\n\t\t\t\t\t\t\t\t\t<span class=\" glyphicon glyphicon-search\"></span>\n\t\t\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\n\t\t\t\t\t\t\t</div>\t\t\t\t\t\n\t\t\t\t\t\t\t<div class=\"member_list\" style=\"background-color:  #fff;\">\n\t\t\t\t\t\t\t\t<ul class=\"list-unstyled\">\n                                    <div *ngIf=\"userstatus==0\">\n                                        <div>\n                                            <li class=\"chat left clearfix\" *ngFor=\"let obj of userList.data\" (click)=\"viewUser(obj)\" [class.doHighLight]=\"syskeycss == (obj.sysKey||obj.syskey)\">\n                                            <span class=\"chat-img pull-left\">\n                                                <img src=\"{{profileImageLink}}{{obj.t2}}\" onError=\"this.src = 'image/user-icon.png'\" >\n                                            </span>\n                                            <div class=\"chat-body clearfix\">\n                                                <div class=\"header_sec\">\n                                                    <strong class=\"primary-font uni\">{{obj.t3}}</strong> \n                                                </div>\n                                                <div class=\"contact_sec\">\n                                                    <p class=\"primary-font uni\">{{obj.t1}}</p> \n                                                </div>\n                                            </div>\n                                            </li>\t\n                                        </div>\n\t\t\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\n                                    <div *ngIf=\"userstatus==1\">\n                                        <div *ngIf=\"userdata.length > 0\">\n                                            <li class=\"chat left clearfix\" *ngFor=\"let obj of userdata\"  (click)=\"viewUser(obj)\" [class.doHighLight]=\"syskeycss == (obj.sysKey||obj.syskey)\">\n                                            <span class=\"chat-img pull-left\">\n                                                <img src=\"{{profileImageLink}}{{obj.t2}}\" onError=\"this.src = 'image/user-icon.png'\" >\n                                            </span>\n                                            <div class=\"chat-body clearfix\">\n                                                <div class=\"header_sec\">\n                                                    <strong class=\"primary-font uni\">{{obj.t3}}</strong> \n                                                </div>\n                                                <div class=\"contact_sec\">\n                                                    <p class=\"primary-font uni\">{{obj.t1}}</p> \n                                                </div>\n                                            </div>\n                                            </li>\n                                        </div>\n\t\t\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t\t</div>\t\n\t\t\t\t\t\t</tab>\t\t  \n\t\t\t\t\t</tabset>\n\t\t\t\t</div>\t\t\t\n\t\t\t</div>\n\t\t\t<!--chat_sidebar-->\t  \t \n\t\t\t<div class=\"col-sm-9 message_section\" *ngIf=\"nore!=0\" style=\"background-color:#fff\">\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<div class=\"new_message_head\">\n\t\t\t\t\t\t<div class=\"pull-left uni\" style=\"font-size:16px\">{{this.passData.t2}}</div>\n\t\t\t\t\t\t<div class=\"pull-right\">\n\t\t\t\t\t\t\t<button class=\"btn btn-primary\" type=\"submit\" (click)=\"doRefresh($event)\">\n\t\t\t\t\t\t\t<i style=\"font-size:15px\" class=\"fa fa-caret-up\"></i>&nbsp; Refresh</button>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<!--new_message_head-->\n\t\t\t\t\t\t<div class=\"messages messages-img\">\n                            <div style=\"height: 350px;overflow-y: auto;overflow-x: hidden;\">\n                            <div *ngIf=\"msgList.length > 0\">\n                            <div *ngFor=\"let ml of msgList\">\n                            <div align=center style=\"margin-bottom: 0px;\">{{ml.t7}}</div>\n                            <div *ngFor=\"let msg of ml.data\"[class.left]=\" (msg.t5 != userst && msg.n1 != usersyskey) || (msg.t5 == userst && msg.n1 != usersyskey) || (msg.t5 != userst && msg.n1 == usersyskey)\"\n                            [class.right]=\" msg.t5 == userst && msg.n1 == usersyskey\">\n                                <div class=\"item\" *ngIf=\" (msg.t5 != userst && msg.n1 != usersyskey) || (msg.t5 == userst && msg.n1 != usersyskey) || (msg.t5 != userst && msg.n1 == usersyskey)\">\t\t\t\t\t\t\t\t\n                                    <div class=\"image\">\n                                        <div *ngIf=\"msg.t18 != ''\">\n                                            <img src=\"{{profileImageLink}}{{msg.t18}}\" onError=\"this.src = 'image/user-icon.png'\" >\n                                        </div>\n                                        <div *ngIf=\"msg.t18 == ''\">\n                                            <img src=\"image/user-icon.png\" >\n                                        </div>\n                                    </div>                                \n                                    <div class=\"text\">\n                                        <div class=\"heading\">\n                                            <span class=\"uni\" style=\"font-weight: 600;\">{{msg.t10}}</span>\n                                            <span class=\"date\">{{msg.t8}}</span>\n                                        </div>\t\n                                        <p *ngIf=\"msg.t17!='' && msg.t19!=''\"style=\"font-size: 12px;padding-top: 9px;\"><!--style=\"font-size: 0.8rem;\" -->\n                                          <span>{{msg.t17}}</span>\n                                          <span>{{msg.t19}}</span>\n                                        </p>\n                                        <div><img class=\"img-fluid\" src=\"{{FromimageLink}}/{{msg.t3}}\" alt=\"\" *ngIf=\"msg.t3\" style=\"width:40%;height:auto\"></div>\n                                        <p id=\"map\" style=\"font-size: 11px;padding-top:9px;\" (click)=\"goGoogleMap(msg.t16)\" *ngIf=\"msg.t16!=''\"><!--1 -->\n                                          <span style=\"color:#8991f6;cursor: pointer;\">{{msg.t16}}</span>\n                                        </p>\t\t\t\t\t\t\t\t\t\t\n                                        <p class=\"uni\" *ngIf=\"msg.t2\">\t\t\t\t\t\t\t\t\t\t  \n                                          <span *ngIf=\"!url(msg.t2)\">{{msg.t2}}</span>\n                                          <a href=\"{{msg.t2}}\" *ngIf=\"url(msg.t2)\" style='font-size: 11px;'>{{msg.t2}}</a>\n                                        </p>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n                                    </div>\n                                </div>\n                                <div class=\"item in\" *ngIf=\"msg.t5 == userst && msg.n1 == usersyskey\">\n                                    <div class=\"image\">\n                                        <img src=\"image/channelAdmin.png\" alt=\"\">\n                                    </div>\n                                    <div class=\"text\" style=\"text-align: left !important;\">\n                                        <div class=\"heading\">\n                                            <span class=\"uni\" style=\"font-weight: 600;\">{{msg.t10}}</span>\n                                            <span class=\"date\">{{msg.t8}}</span>\n                                        </div>\t\t\t\t\t\t\t\t\t\t\n                                        <div *ngIf=\"msg.t20=='1'\"><!--atn -->\n                                            <qrcode class=\"img-fluid\" [qrdata]=\"'ticketNumber: '+msg.t2\" [size]=\"100\"></qrcode>\n                                        </div>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n                                        <div *ngIf=\"msg.t20!='1' && msg.t3!=''\">\n                                          <img class=\"img-fluid\" src=\"{{ToimageLink}}/{{msg.t3}}\" alt=\"\" style=\"width:40%;height:auto\">\n                                        </div>\t\t\t\t\t\t\t\t\t\t\n                                        <p class=\"uni\" *ngIf=\"msg.t2!=''\">\n                                            <span *ngIf=\"msg.t20=='1'\">Ticket Number : </span>\n                                            <span *ngIf=\"!url(msg.t2)\">{{msg.t2}}</span>\n                                            <a href=\"{{msg.t2}}\" *ngIf=\"url(msg.t2)\" style=\"font-size: 11px;\">{{msg.t2}}</a>\n                                        </p>\t\t\t\t\t\t\t\t\t\t\n                                    </div>\n                                </div>\t\t\t\t\t\t\t\t\n                            </div>\t\t\t\t\t\t\t\t\n                        </div>\n                            </div>\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t<div *ngIf=\"status\">\n\t\t\t\t\t\t\t\t\t<div style=\"color:#808080;\">Sending....</div>\t\t\t\t\t  \n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\n\t\t\t\t\t\t\t<div class=\"panel panel-default push-up-10\">\n\t\t\t\t\t\t\t\t<div class=\"panel-body panel-body-search\">\n\t\t\t\t\t\t\t\t\t<div class=\"input-group\">\n\t\t\t\t\t\t\t\t\t  <div class=\"input-group-btn\">\n\t\t\t\t\t\t\t\t\t\t<label for=\"imageUpload\" class=\"btn btn-default\" style=\"padding-bottom: 8px;padding-top: 8px;\">\n\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-image\"></i>\n\t\t\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t\t\t<input id=\"imageUpload\" type=\"file\" style=\"display: none;\" (change)=\"uploadedFileImage($event)\" accept=\"image/*\"/>\n\t\t\t\t\t\t\t\t\t  </div>\n\t\t\t\t\t\t\t\t\t  <div>\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t\t<textarea type=\"text\" class=\"form-control\" [(ngModel)]=\"postMsg\" rows=\"1\" style=\"resize: none;\" placeholder=\"Your message...\"></textarea>\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t  </div>\t\t\t\t\t\t\t\t  \n\t\t\t\t\t\t\t\t\t  <div class=\"input-group-btn\">\n\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-default\" (click)=\"sendMsg()\">Send</button>\n\t\t\t\t\t\t\t\t\t  </div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t                                             \n\t\t\t\t\t\t</div> \n\t\t\t\t</div>\n\t\t\t</div> <!--message_section-->\n\t\t\t<div id=\"PhotoPopup\" class=\"modal fade\" role=\"dialog\">\n\t\t\t\t<div id=\"popupsize\" class=\"modal-dialog modal-lg\" style=\"width: 560px;\">\n\t\t\t\t\t<div class=\"modal-content\">\n\t\t\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n\t\t\t\t\t\t\t<h4 id=\"popuptitle\" class=\"modal-title\">Photo</h4>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t  <div id=\"popupbody\" class=\"modal-body\">           \t\t\n\t\t\t\t\t\t  <div *ngIf=\"this.uploadData.url!=''\" align=\"center\">\n\t\t\t\t\t\t\t  <img src=\"{{ToimageLink}}/{{this.uploadData.url}}\" alt=\"\" src=\"\" style=\"height:200px;width:300px\">\t\t\t\t\t\t\n\t\t\t\t\t\t  </div><br>\n\t\t\t\t\t\t  <div>\n\t\t\t\t\t\t\t<textarea type=\"text\" [(ngModel)]=\"postMsg\" rows=\"1\" placeholder=\"Your message...\" style=\"width: 80%;padding: 6px 12px;\"></textarea>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t  \n\t\t\t\t\t\t\t<button class=\"btn btn-default\" (click)=\"sendMsg()\" style=\"margin-bottom: 27px;margin-left: -4px;\">Send</button>\n\t\t\t\t\t\t  </div>\n\t\t\t\t\t  </div>\n\t\t\t\t\t\t<div class=\"modal-footer\">\t\t\t\t\t\t\t\n\t\t\t\t\t\t   <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\" (click)=\"onClose()\">Close</button>\n\t\t\t\t\t\t</div>                                      \n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\t\t\n\t\t<div [hidden] = \"_mflag\">\n\t\t\t<div  id=\"loader\" class=\"modal\" ></div>\n\t\t</div>\n</div>\n  ",
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], ChatList);
    return ChatList;
}());
exports.ChatList = ChatList;
//# sourceMappingURL=chat.component.js.map