"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
core_1.enableProdMode();
var FrmTicketList = (function () {
    //subscription: Subscription;
    // _totalcount = 1;
    // _searchVal = "";
    // _util = new ClientUtil();
    // _obj = { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": null, "modifiedDate": "", "modifiedTime": null, "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "Video", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [], "videoUpload": [], "cropdata": null, "agrodata": null, "ferdata": null, "towndata": null, "crop": null, "fert": null, "agro": null, "addTown": null,"modifiedUserId":"","modifiedUserName":"" };
    // _array = [];
    // _pager = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 0, "size": 0, "totalcount": 1, "t1": "","userid":""};
    // _StateType = "";
    // _roleval = "";
    function FrmTicketList(ics, ref, _router, http) {
        // if (!ics.getRole() || ics.getRole() == 0) this._router.navigate(['/login']);
        // this._roleval = ics._profile.t1;
        this.ics = ics;
        this.ref = ref;
        this._router = _router;
        this.http = http;
        this._mflag = false;
        //about default obj
        this.ticketObj = this.getDefaultObj();
        this._pgobj = {
            //previous start
            //"current": 1, "prev": 1, "last": 1, "next": 2, "start": 1,
            "end": 10, "size": 10, "totalcount": 0,
            //previous end
            //new start
            "current": 1,
            "prev": 0,
            "last": 20,
            "next": 11,
            "start": 0,
            "appId": "010",
            "domain": "DC001"
        };
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.ticketStatus = "";
        this.markers = [];
        // this.search();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            //this.checkSession();
            // this.getAppCodes();
            this.ticketStatus = "All";
            this.getTicketStatus();
            this.search();
        }
    }
    FrmTicketList.prototype.changeStatus = function (event) {
        this.search();
    };
    FrmTicketList.prototype.getDefaultObj = function () {
        return {
            "tdata": [
                {
                    "n1": 0,
                    "n2": 0,
                    "t2": "",
                    "t4": "",
                    "t6": "",
                    "t5": "",
                    "t3": "",
                    "t7": "",
                    "t10": "",
                    "t8": "",
                    "t9": "",
                    "t1": "",
                    "status": ""
                }
            ],
            "totalCount": 0,
            "currentPage": 1,
            "pageSize": 10,
            "state": true,
            "searchText": ''
        };
    };
    FrmTicketList.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    FrmTicketList.prototype.search = function () {
        var _this = this;
        try {
            var url = this.ics._ticketlisturl + 'serviceTicket/getTicketList?searchVal=' + this.ticketObj.searchText;
            var json = this._pgobj;
            this.http.doPost(url, json).subscribe(function (data) {
                console.log("data:" + JSON.stringify(data));
                if (data != null) {
                    if (data.totalCount == 0) {
                        _this.showMsgAlert("There is no tickets.");
                    }
                    else {
                        var ticketObjVirtual_1 = data;
                        if (!(data.tdata instanceof Array)) {
                            var m = [];
                            m[0] = data.tdata;
                            ticketObjVirtual_1.tdata = m;
                        }
                        var ticketList = [];
                        for (var i = 0; i < ticketObjVirtual_1.tdata.length; i++) {
                            ticketList[i] = ticketObjVirtual_1.tdata[i].t1;
                            ticketObjVirtual_1.tdata[i].status = "Nothing";
                            var abc = _this.ics._ticketlistimageurl + ticketObjVirtual_1.tdata[i].t10;
                            console.log("abc:" + abc);
                            ticketObjVirtual_1.tdata[i].t10 = _this.ics._ticketlistimageurl + ticketObjVirtual_1.tdata[i].t10;
                            if (ticketObjVirtual_1.tdata[i].t6 != undefined && ticketObjVirtual_1.tdata[i].t6 != null && ticketObjVirtual_1.tdata[i].t6 != '') {
                                var def = ticketObjVirtual_1.tdata[i].t6;
                                var latitudeOne = def.substring(0, def.indexOf("/"));
                                var longitudeOne = def.substring(def.indexOf("/") + 1);
                                // markers = [
                                //     [51.673858, 7.815982, [], "abc"],
                                //     [51.373858, 7.215982, [], "def"],
                                //     [51.723858, 7.895982, [], "ghi"]
                                // ];
                                // this.markers[i][0] = latitudeOne;
                                // this.markers[i][1] = longitudeOne;
                                // this.markers[i][3] = ticketObjVirtual.tdata[i].t9;
                                var info = '';
                                if (ticketObjVirtual_1.tdata[i].t9 != undefined && ticketObjVirtual_1.tdata[i].t9 != null && ticketObjVirtual_1.tdata[i].t9 != '') {
                                    info = "Ticket No. " + ticketObjVirtual_1.tdata[i].t1 + " by " + ticketObjVirtual_1.tdata[i].t9;
                                }
                                _this.markers.push([latitudeOne, longitudeOne, [], info]);
                            }
                        }
                        //start our service
                        try {
                            var urlTicket = _this.ics._apiurl + 'TicketService/getAllTicketList';
                            var _obj = {
                                "sessionID": _this.ics._profile.sessionID,
                                "userID": _this.ics._profile.userID,
                                "ticketList": ticketList
                            };
                            _this.http.doPost(urlTicket, _obj).subscribe(function (result) {
                                _this._mflag = true;
                                if (result != undefined && result != null && result != '') {
                                    if (result.ticketList != undefined && result.ticketList != null && result.ticketList.length > 0) {
                                        for (var j = 0; j < ticketObjVirtual_1.tdata.length; j++) {
                                            for (var i = 0; i < result.ticketList.length; i++) {
                                                //for (let j = 0; j < ticketObjVirtual.tdata.length; j++) {
                                                if (result.ticketList[i].ticketNo == ticketObjVirtual_1.tdata[j].t1) {
                                                    ticketObjVirtual_1.tdata[j].status = result.ticketList[i].code;
                                                }
                                                else {
                                                    if (ticketObjVirtual_1.tdata[j].status == "Nothing") {
                                                        ticketObjVirtual_1.tdata[j].status = "New";
                                                    }
                                                }
                                            }
                                        }
                                        _this.ticketObj = ticketObjVirtual_1;
                                        if (_this.ticketObj.tdata.length == 0) {
                                            _this.showMsgAlert("Data not found!");
                                        }
                                    }
                                    else {
                                        for (var j = 0; j < ticketObjVirtual_1.tdata.length; j++) {
                                            ticketObjVirtual_1.tdata[j].status = "New";
                                        }
                                        _this.ticketObj = ticketObjVirtual_1;
                                        if (_this.ticketObj.tdata.length == 0) {
                                            _this.showMsgAlert("Data not found!");
                                        }
                                    }
                                    console.log("result=" + JSON.stringify(result));
                                }
                                else {
                                    console.log("result else=" + JSON.stringify(result));
                                }
                            }, function (error) {
                                console.log("result else=" + JSON.stringify(error));
                                if (error._body.type == "error") {
                                    alert("Connection Timed Out.");
                                }
                            }, function () { console.log("not data not error="); });
                        }
                        catch (e) {
                            alert("Invalid URL.");
                        }
                    }
                    //}
                    _this._mflag = true;
                }
                else {
                    _this._mflag = true;
                }
            }, function (error) {
                _this._mflag = true;
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    // loadMap() {
    //     var MarkerData = [];
    //     let caption = [];
    //     let obj = [];
    //     let cap;
    //     let ref = this._mapdata.datalst[j].t2.split("-");
    //     obj.push(Number(arr[0]));
    //     obj.push(Number(arr[1]));
    //     for (let v = 0; v < this._mapdata.datalst.length; v++) {
    //         if (_mapdata.datalst[v].datatype == "text") {
    //             let cc = this._mapdata.datalst[v].t2.split("-");
    //             if (cc[1] == ref[1]) {
    //                 cap = this._mapdata.datalst[v].datavalue;
    //                 caption.push(cap);
    //             }
    //         }
    //     }
    //     obj.push(caption);
    //     obj.push(ref[1]);
    // }
    FrmTicketList.prototype.goMap = function () {
        //this._router.navigate(['/map', , { cmd: "maplist", id: this.ticketObj }]);
        var map = new google.maps.Map(document.getElementById('map-canvas'));
        var infowindow = new google.maps.InfoWindow();
        for (var i = 0; i < this.markers.length; i++) {
            var latlng = new google.maps.LatLng(this.markers[i][0], this.markers[i][1]);
            map = new google.maps.Map(document.getElementById('map-canvas'), {
                zoom: 150,
                center: latlng
            });
        }
        for (var i = 0; i < this.markers.length; i++) {
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(this.markers[i][0], this.markers[i][1]),
                map: map,
                title: this.markers[i][3],
                zIndex: this.markers[i][1]
            });
            infowindow = new google.maps.InfoWindow();
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    //infowindow.setContent("Data:"+MarkerData[i][3]+"<br />"+ MarkerData[i][2]);
                    infowindow.setContent("Info: " + marker.title);
                    infowindow.open(map, marker);
                };
            })(marker, i));
        }
        // for (let i = 0; i < this.markers.length; i++) {
        //     var marker = new google.maps.Marker({
        //         position: new google.maps.LatLng(this.markers[i][0], this.markers[i][1]),
        //         map: map,
        //         title: this.markers[i][3],
        //         zIndex: this.markers[i][1]
        //     });
        //     infowindow = new google.maps.InfoWindow();
        //     google.maps.event.addListener(marker, 'click', (function (marker, i) {
        //         return function () {
        //             //infowindow.setContent("Data:"+MarkerData[i][3]+"<br />"+ MarkerData[i][2]);
        //             infowindow.setContent("Data:" + this.markers[i][3]);
        //             infowindow.open(map, marker);
        //         }
        //     })(marker, i));
        // }
        jQuery("#popup").modal();
    };
    FrmTicketList.prototype.searchKeyup = function (e) {
        if (e.which == 13) {
            this.ticketObj.currentPage = 1;
            this.search();
        }
    };
    FrmTicketList.prototype.searching = function () {
        this.ticketObj.currentPage = 1;
        this.search();
    };
    FrmTicketList.prototype.changedPager = function (event) {
        if (this.ticketObj.totalCount != 0) {
            this._pgobj = event;
            var current = this.ticketObj.currentPage;
            var size = this.ticketObj.pageSize;
            this.ticketObj.currentPage = this._pgobj.current;
            this.ticketObj.pageSize = this._pgobj.size;
            if (this._pgobj.current != current || this._pgobj.size != size) {
                this.search();
            }
        }
    };
    FrmTicketList.prototype.goto = function (p) {
        console.log("p=" + JSON.stringify(p));
        this._router.navigate(['/ticket', 'read', p]);
    };
    FrmTicketList.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmTicketList.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    FrmTicketList.prototype.getTicketStatus = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'TicketService/getTicketStatus';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.msgCode == "0016") {
                        _this.showMsgAlert(data.msgDesc);
                        _this.logout();
                    }
                    if (data.msgCode != "0000") {
                        _this.showMsgAlert(data.msgDesc);
                    }
                    if (data.msgCode == "0000") {
                        if (!(data.refTicketStatus instanceof Array)) {
                            var m = [];
                            m[0] = data.refTicketStatus;
                            _this.ref._lov3.refTicketStatus = m;
                        }
                        else {
                            _this.ref._lov3.refTicketStatus = data.refTicketStatus;
                        }
                        _this.ticketStatus = _this.ref._lov3.refTicketStatus[0].value;
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmTicketList = __decorate([
        core_1.Component({
            selector: 'fmr-ticketList',
            template: "\n<div class=\"container\">\n<div class=\"row clearfix\">\n<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n    <form class=\"form-inline\">\n    <!--<div class=\"col-md-12\">-->\n            <legend>Ticket List</legend>\n    <!--</div>-->\n        <!-- <div class=\"input-group\">\n                <button class=\"btn btn-primary\" type=\"button\" (click)=\"goMap()\" id=\"vh_list_btn\">Map View</button>\n                <input id=\"textinput\" name=\"textinput\" type=\"text\" placeholder=\"Search\" [(ngModel)]=\"ticketObj.searchText\" (keyup)=\"searchKeyup($event)\" maxlength=\"50\" class=\"form-control input-md\">\n            <span class=\"input-group-btn input-md\">\n                <button class=\"btn btn-primary input-md\" type=\"button\" (click)=\"searching()\">\n                    <span class=\"glyphicon glyphicon-search\"></span> Search\n                </button>\n            </span> \n        </div>-->\n\n        <div class=\"input-group\" style=\"padding-left: 0px;\">\n            <adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"ticketObj.totalCount\" (rpChanged)=\"changedPager($event)\"></adminpager> \n        </div>\n\n        <div class=\"input-group\" style=\"padding-right: 10px;\">\n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"goMap()\" id=\"vh_list_btn\">Map View</button>\n        </div>\n\n        <div class=\"input-group\">\n            <label class=\"col-md-4\" style=\"padding-left: 0px;\">Status</label>\n            <div class=\"col-md-8\" style=\"padding-left: 0px;\">\n                <select [(ngModel)]=\"ticketStatus\" (change)=\"changeStatus($event)\" class=\"form-control col-md-0\" [ngModelOptions]=\"{standalone: true}\" required>\n                    <option *ngFor=\"let item of ref._lov3.refTicketStatus\" value=\"{{item.value}}\">{{item.value}}</option>\n                </select>\n            </div>\n        </div>\n\n        <div class=\"input-group\" style=\"padding: 25px 5px 25px 0px; float: right;\">\n            <div style=\"height: 10px;width: 10px;background-color: red;display: inline-block;\"></div> New &nbsp;&nbsp;\n            <div style=\"height: 10px;width: 10px;background-color: yellow;display: inline-block;\"></div> Pending &nbsp;&nbsp;\n            <div style=\"height: 10px;width: 10px;background-color: green;display: inline-block;\"></div> Closed &nbsp;&nbsp;\n            <div style=\"height: 10px;width: 10px;background-color: gray;display: inline-block;\"></div> Rejected\n        </div>\n    </form>\n\n    <div class=\"row\">\n        <div class=\"col-md-12\">\n            <div class=\"table-responsive\">\n                <table class=\"table table-striped table-condensed table-hover tblborder\" style=\"font-size: 14px;\">\n                    <thead>\n                        <tr>\n                            <!--<th>Status</th>-->\n                            <th>Ticket No.</th>\n                            <th>Channel</th>\n                            <th>User</th>\n                            <th>Message</th>\n                            <th>Image</th>\n                            <th>Location</th>\n                            <th>Date</th>    \n                            <th>Time</th>\n                        </tr>\n                    </thead>\n                    <tbody *ngIf=\"ticketObj != undefined && ticketObj != null && ticketObj != '' && ticketObj.tdata.length != 0\">\n                        <ng-container *ngFor=\"let obj of ticketObj.tdata\">\n                            <tr *ngIf=\"'All'!=ticketStatus && obj.status==ticketStatus\">\n                            <!--<td style=\"background-color: yellow; border-left: 1px solid white; border-right: 2px solid white;\" *ngIf=\"obj.status=='Pending'\">{{obj.status}}</td>\n                                <td style=\"background-color: red; border-left: 1px solid white; border-right: 2px solid white;\" *ngIf=\"obj.status=='New'\">{{obj.status}}</td>\n                                <td style=\"background-color: green; border-left: 1px solid white; border-right: 2px solid white;\" *ngIf=\"obj.status=='Closed'\">{{obj.status}}</td>\n                                <td style=\"background-color: gray; border-left: 1px solid white; border-right: 2px solid white;\" *ngIf=\"obj.status=='Rejected'\">{{obj.status}}</td>\n                                <td ><a (click)=\"goto(obj)\">{{obj.t1}}</a></td>-->\n\n                                <td style=\"background-color: yellow; border-left: 1px solid white; border-right: 2px solid white; font-weight: bold;\" *ngIf=\"obj.status=='Pending'\"><a style=\"color: white;\" (click)=\"goto(obj)\">{{obj.t1}}</a></td>\n                                <td style=\"background-color: red; border-left: 1px solid white; border-right: 2px solid white; font-weight: bold;\" *ngIf=\"obj.status=='New'\"><a style=\"color: white;\" (click)=\"goto(obj)\">{{obj.t1}}</a></td>\n                                <td style=\"background-color: green; border-left: 1px solid white; border-right: 2px solid white; font-weight: bold;\" *ngIf=\"obj.status=='Closed'\"><a style=\"color: white;\" (click)=\"goto(obj)\">{{obj.t1}}</a></td>\n                                <td style=\"background-color: gray; border-left: 1px solid white; border-right: 2px solid white; font-weight: bold;\" *ngIf=\"obj.status=='Rejected'\"><a style=\"color: white;\" (click)=\"goto(obj)\">{{obj.t1}}</a></td>\n                                <td *ngIf=\"obj.t4 == '1054' || obj.t4 == '1060'\">General</td>\n                                <td *ngIf=\"obj.t4 == '1058' || obj.t4 == '1061'\">Water Pipeline</td>\n                                <td *ngIf=\"obj.t4 == '1059' || obj.t4 == '1062'\">Gabbage Collection</td>\n                                <td class=\"uni\">{{obj.t9}}</td>\n                                <td class=\"uni\">{{obj.t5}}</td>\n                                <td> <img *ngIf=\"obj.t10\" src={{obj.t10}} onError=\"this.src='./image/image_not_found.png';\" height=\"50\" width=\"50\" /></td>\n                                <td>{{obj.t6}}</td>\n                                <td>{{obj.t7}}</td>\n                                <td>{{obj.t8}}</td>\n                            </tr>\n                            <tr *ngIf=\"'All'==ticketStatus\">\n                            <!--<td style=\"background-color: yellow; border-left: 1px solid white; border-right: 2px solid white;\" *ngIf=\"obj.status=='Pending'\">{{obj.status}}</td>\n                                <td style=\"background-color: red; border-left: 1px solid white; border-right: 2px solid white;\" *ngIf=\"obj.status=='New'\">{{obj.status}}</td>\n                                <td style=\"background-color: green; border-left: 1px solid white; border-right: 2px solid white;\" *ngIf=\"obj.status=='Closed'\">{{obj.status}}</td>\n                                <td style=\"background-color: gray; border-left: 1px solid white; border-right: 2px solid white;\" *ngIf=\"obj.status=='Rejected'\">{{obj.status}}</td>\n                                <td><a (click)=\"goto(obj)\">{{obj.t1}}</a></td>-->\n\n                                <td style=\"background-color: yellow; border-left: 1px solid white; border-right: 2px solid white; font-weight: bold;\" *ngIf=\"obj.status=='Pending'\"><a style=\"color: white;\" (click)=\"goto(obj)\">{{obj.t1}}</a></td>\n                                <td style=\"background-color: red; border-left: 1px solid white; border-right: 2px solid white; font-weight: bold;\" *ngIf=\"obj.status=='New'\"><a style=\"color: white;\" (click)=\"goto(obj)\">{{obj.t1}}</a></td>\n                                <td style=\"background-color: green; border-left: 1px solid white; border-right: 2px solid white; font-weight: bold;\" *ngIf=\"obj.status=='Closed'\"><a style=\"color: white;\" (click)=\"goto(obj)\">{{obj.t1}}</a></td>\n                                <td style=\"background-color: gray; border-left: 1px solid white; border-right: 2px solid white; font-weight: bold;\" *ngIf=\"obj.status=='Rejected'\"><a style=\"color: white;\" (click)=\"goto(obj)\">{{obj.t1}}</a></td>\n                                <td *ngIf=\"obj.t4 == '1054' || obj.t4 == '1060'\">General</td>\n                                <td *ngIf=\"obj.t4 == '1058' || obj.t4 == '1061'\">Water Pipeline</td>\n                                <td *ngIf=\"obj.t4 == '1059' || obj.t4 == '1062'\">Gabbage Collection</td>\n                                <td class=\"uni\">{{obj.t9}}</td>\n                                <td class=\"uni\">{{obj.t5}}</td>\n                                <td> <img *ngIf=\"obj.t10\" src={{obj.t10}} onError=\"this.src='./image/image_not_found.png';\" height=\"50\" width=\"50\" /></td>\n                                <td>{{obj.t6}}</td>\n                                <td>{{obj.t7}}</td>\n                                <td>{{obj.t8}}</td>\n                            </tr>  \n                        </ng-container>\n                    </tbody>\n                </table>\n            </div>\n        </div>\n    </div>\n\n    <div id=\"popup\" class=\"modal fade\" role=\"dialog\">\n        <div id=\"popupsize\" class=\"modal-dialog modal-lg\">\n            <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n                <h4 id=\"popuptitle\" class=\"modal-title\">Map</h4>\n            </div>\n            <div id=\"popupbody\" class=\"modal-body\">\n                <div style=\"\">&nbsp;</div>\n                \n                <div class=\"form-group\">\n                    <div class=\"col-md-12\">\n                        <sebm-google-map [latitude]=\"lat\" [longitude]=\"lng\">\n                        </sebm-google-map>\n                        <div id=\"map-canvas\" class=\"col-md-12\" style=\"height:80%\">\n                        </div>\n                    </div>\n                </div>\n\n                <div style=\"\">&nbsp;</div>\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\">Close</button>\n            </div>\n            </div>\n        </div>\n    </div>\n</div>\n</div>\n</div>\n\n<div [hidden]=\"_mflag\">\n  <div class=\"modal\" id=\"loader\"></div>\n</div>\n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, rp_references_1.RpReferences, router_1.Router, rp_http_service_1.RpHttpService])
    ], FrmTicketList);
    return FrmTicketList;
}());
exports.FrmTicketList = FrmTicketList;
//# sourceMappingURL=frmticketList.component.js.map