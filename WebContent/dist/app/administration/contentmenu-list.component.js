"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
core_1.enableProdMode();
var ContentMenuListComponent = (function () {
    function ContentMenuListComponent(ics, _router, 
        //private sanitizer: DomSanitizer, 
        http, l_util, ref) {
        this.ics = ics;
        this._router = _router;
        this.http = http;
        this.l_util = l_util;
        this.ref = ref;
        this._shownull = false;
        this._OperationMode = "";
        this._divexport = false;
        //_printUrl: SafeResourceUrl;
        this._isLoading = true;
        this._SearchString = "";
        this._showListing = false;
        this._showPagination = false;
        this._toggleSearch = true; // true - Simple Search, false - Advanced Search
        this._togglePagination = true;
        this._mflag = false;
        this._ButtonInfo = { "role": "", "formname": "", "button": "" };
        this._FilterDataset = {
            filterList: [
                { "itemid": "", "caption": "", "fieldname": "", "datatype": "", "condition": "", "t1": "", "t2": "" }
            ],
            "pageNo": 1, "pageSize": 10, "filterSource": 0, "sessionID": "", "userID": "", "userName": this.ics._profile.userName
        };
        this._ListingDataset = {
            contentData: [
                {
                    "srno": 0, "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": "", "modifiedDate": "",
                    "modifiedTime": "", "userId": "", "userName": "", "modifiedUserId": "", "modifiedUserName": "",
                    "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "",
                    "t2": "", "t3": "", "t4": "", "t5": "", "t6": "",
                    "t7": "", "t8": "", "t9": "", "t10": "", "t11": "",
                    "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0,
                    "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0,
                    "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "statusdata": "",
                    "upload": null, "answer": null, "videoUpload": null, "comData": null, "ansData": null,
                    "cropdata": null, "agrodata": null, "ferdata": null, "towndata": null, "crop": null,
                    "fert": null, "agro": null, "addTown": null, "uploadlist": null, "resizelist": null,
                    "uploadDatalist": null, "uploadedPhoto": null
                }
            ],
            "pageNo": 0, "pageSize": 10, "totalCount": 1, "userID": "", "userName": ""
        };
        this._sessionObj = this.getSessionObj();
        this.mstatus = 0;
        this._util = new rp_client_util_1.ClientUtil();
        this.mstatus = ics._profile.loginStatus;
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
    }
    ContentMenuListComponent.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "", "lovDesc": "" };
    };
    ContentMenuListComponent.prototype.goto = function (p) {
        console.log("p=" + JSON.stringify(p));
        this._router.navigate(['/contentmenu', 'read', p]);
    };
    ContentMenuListComponent.prototype.ngOnInit = function () {
        this.filterSearch();
        this.loadAdvancedSearchData();
    };
    ContentMenuListComponent.prototype.goNew = function () {
        this._router.navigate(['/contentmenu', 'new']);
    };
    ContentMenuListComponent.prototype.ngAfterViewChecked = function () {
        this._isLoading = false;
    };
    ContentMenuListComponent.prototype.loadAdvancedSearchData = function () {
        this._TypeList =
            {
                "lovStatus": []
            };
        this.loadStatus();
        this._FilterList =
            [
                { "itemid": "1", "caption": "Search String", "fieldname": "SearchString", "datatype": "simplesearch", "condition": "c", "t1": "", "t2": "", "t3": "" },
                { "itemid": "2", "caption": "Status", "fieldname": "Status", "datatype": "lovStatus", "condition": "", "t1": "", "t2": "", "t3": "" },
            ];
    };
    ContentMenuListComponent.prototype.loadStatus = function () {
        var _this = this;
        try {
            var url = this.ics.cmsurl + 'serviceAdmLOV/getLovType';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            this._sessionObj.lovDesc = "StatusType";
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.msgCode == "0016") {
                        _this.showMsg(data.msgDesc, false);
                    }
                    if (data.msgCode == "0000") {
                        if (!(data.lovType instanceof Array)) {
                            var m = [];
                            m[0] = data.lovType;
                            _this.ref._lov3.StatusType = m;
                        }
                        else {
                            _this.ref._lov3.StatusType = data.lovType;
                        }
                        _this.ref._lov3.StatusType.forEach(function (iItem) {
                            var l_Item = { "value": "", "caption": "" };
                            l_Item['caption'] = iItem.caption;
                            l_Item['value'] = iItem.value;
                            _this._TypeList.lovStatus.push(iItem);
                        });
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    ContentMenuListComponent.prototype.btnToggleSearch_onClick = function () {
        this.toggleSearch(!this._toggleSearch);
    };
    ContentMenuListComponent.prototype.btnTogglePagination_onClick = function () {
        this.togglePagination(!this._togglePagination);
    };
    ContentMenuListComponent.prototype.toggleSearch = function (aToggleSearch) {
        // Set Flag
        this._toggleSearch = aToggleSearch;
        // Clear Simple Search
        if (!this._toggleSearch)
            this._SearchString = '';
        // Enable/Disabled Simple Search Textbox
        jQuery('#isInputSearch').prop('disabled', !this._toggleSearch);
        // Hide Advanced Search
        if (this._toggleSearch)
            this.ics.send001("CLEAR");
        // Show/Hide Advanced Search    
        //  
        if (this._toggleSearch)
            jQuery("#asAdvancedSearch").css("display", "none"); // true     - Simple Search
        else
            jQuery("#asAdvancedSearch").css("display", "inline-block"); // false    - Advanced Search
        // Set Icon 
        //  
        if (this._toggleSearch)
            jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-up").addClass("glyphicon glyphicon-menu-down"); // true     - Simple Search
        else
            jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-down").addClass("glyphicon glyphicon-menu-up"); // false    - Advanced Search
        // Set Tooltip
        //
        var l_Tooltip = (this._toggleSearch) ? "Expand" : "Collapse";
        jQuery('#btnToggleSearch').attr('title', l_Tooltip);
    };
    ContentMenuListComponent.prototype.togglePagination = function (aTogglePagination) {
        // Set Flag
        this._togglePagination = aTogglePagination;
        // Show/Hide Pagination
        //
        if (this._showPagination && this._togglePagination)
            jQuery("#divPagination").css("display", "inline-block");
        else
            jQuery("#divPagination").css("display", "none");
        // Rotate Icon
        //
        if (!this._togglePagination)
            jQuery("#lblTogglePagination").css("transform", "rotate(270deg)");
        else
            jQuery("#lblTogglePagination").css("transform", "rotate(90deg)");
        // Set Icon Position
        if (!this._togglePagination)
            jQuery("#lblTogglePagination").css({ "margin-left": "-5px", "margin-top": "0px" });
        else
            jQuery("#lblTogglePagination").css({ "margin-left": "-5px", "margin-top": "-1px" });
        // Set Tooltip
        //
        var l_Tooltip = (this._togglePagination) ? "Collapse" : "Expand";
        jQuery('#btnTogglePagination').attr('title', l_Tooltip);
    };
    ContentMenuListComponent.prototype.btnClose_MouseEnter = function () {
        jQuery("#btnClose").removeClass('btn btn-sm btn-secondary').addClass('btn btn-sm btn-danger');
    };
    ContentMenuListComponent.prototype.btnClose_MouseLeave = function () {
        jQuery("#btnClose").removeClass('btn btn-sm btn-danger').addClass('btn btn-sm btn-secondary');
    };
    ContentMenuListComponent.prototype.closePage = function () {
        this._router.navigate(['000', { cmd: "READ", p1: this.ics._profile.userID }]);
    };
    ContentMenuListComponent.prototype.renderAdvancedSearch = function (event) {
        this.toggleSearch(!event);
    };
    ContentMenuListComponent.prototype.filterAdvancedSearch = function (event) {
        this._FilterDataset.filterSource = 1;
        this._FilterDataset.filterList = event;
        if (this._OperationMode != "prepareFilter") {
            if (this._FilterDataset.pageNo == 1)
                this.filterRecords();
            else
                this._FilterDataset.pageNo = 1;
        }
    };
    ContentMenuListComponent.prototype.changedPager = function (event) {
        if (!this._isLoading) {
            var l_objPagination = event.obj;
            this._FilterDataset.pageNo = l_objPagination.current;
            this._FilterDataset.pageSize = l_objPagination.size;
            this.filterRecords();
        }
    };
    ContentMenuListComponent.prototype.filterSearch = function () {
        if (this._toggleSearch)
            this.filterCommonSearch();
        else
            this.ics.send001("FILTER");
    };
    ContentMenuListComponent.prototype.filterCommonSearch = function () {
        var l_DateRange;
        var l_SearchString = "";
        this._FilterDataset.filterList = [];
        this._FilterDataset.filterSource = 0;
        if (jQuery.trim(this._SearchString) != "") {
            l_SearchString = jQuery.trim(this._SearchString);
            this._FilterDataset.filterList =
                [
                    { "itemid": "1", "caption": "Search String", "fieldname": "SearchString", "datatype": "simplesearch", "condition": "c", "t1": l_SearchString, "t2": "" }
                ];
        }
        l_DateRange = { "itemid": "4", "caption": "Date Range", "fieldname": "DateRange", "datatype": "date", "condition": "eq", "t1": this.l_util.getTodayDate(), "t2": "" };
        this._FilterDataset.filterList.push(l_DateRange);
        if (this._OperationMode != "prepareFilter") {
            if (this._FilterDataset.pageNo == 1)
                this.filterRecords();
            else
                this._FilterDataset.pageNo = 1;
        }
    };
    ContentMenuListComponent.prototype.filterRecords = function () {
        var _this = this;
        this._FilterDataset.sessionID = this.ics._profile.sessionID;
        this._FilterDataset.userID = this.ics._profile.userID;
        var l_Data = this._FilterDataset;
        var l_ServiceURL = this.ics.cmsurl + 'serviceCMS/selectContent';
        // Show loading animation
        this.http.doPost(l_ServiceURL, l_Data).subscribe(function (data) {
            if (data != null) {
                _this._ListingDataset = data;
                _this._shownull = false;
                // Convert to array for single item
                if (_this._ListingDataset.contentData != undefined) {
                    _this._ListingDataset.contentData = _this.l_util.convertToArray(_this._ListingDataset.contentData);
                    // Add thousand separator
                    //
                    for (var k = 0; k < _this._ListingDataset.contentData.length; k++) {
                        _this._ListingDataset.contentData[k].t2.replace(/^\<p\>/, "").replace(/\<\/p\>$/, "");
                        if (_this._ListingDataset.contentData[k].t2.replace("/</?[^>]+(>|$)/g", "").length > 150) {
                            _this._ListingDataset.contentData[k].t2 =
                                _this._ListingDataset.contentData[k].t2.substring(0, 151) + "....";
                        }
                    }
                    for (var i = 0; i < _this._ListingDataset.contentData.length; i++) {
                        _this._ListingDataset.contentData[i].modifiedDate = _this._util.changeDatetoStringDMY(_this._ListingDataset.contentData[i].modifiedDate);
                        for (var j = 0; j < _this._TypeList.lovStatus.length; j++) {
                            if (_this._ListingDataset.contentData[i].n7.toString() == _this._TypeList.lovStatus[j].value) {
                                _this._ListingDataset.contentData[i].statusdata = _this._TypeList.lovStatus[j].caption;
                            }
                        }
                    }
                }
                else {
                    _this._shownull = true;
                }
                // Show / Hide Listing
                _this._showListing = (_this._ListingDataset.contentData != undefined);
                // Show / Hide Pagination
                _this._showPagination = (_this._showListing && (_this._ListingDataset.totalCount > 10));
                // Show / Hide Pagination
                _this.togglePagination(_this._showListing);
                // Hide loading animation
                _this._mflag = true;
            }
        }, function (error) {
            // Hide loading animation
            _this._mflag = true;
        }, function () { });
    };
    ContentMenuListComponent.prototype.btnExport_click = function () {
    };
    ContentMenuListComponent.prototype.downloadFile = function (aFileName) {
    };
    ContentMenuListComponent.prototype.btnPrint_click = function () {
        //this.goPrint();
    };
    ContentMenuListComponent.prototype.goPrint = function () {
    };
    ContentMenuListComponent.prototype.goClosePrint = function () {
        this._divexport = false;
    };
    ContentMenuListComponent.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    ContentMenuListComponent.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    ContentMenuListComponent = __decorate([
        core_1.Component({
            selector: 'frm-contentMenuList',
            template: "\n    <div *ngIf=\"!_divexport\" class=\"container-fluid\">\n    <div class=\"row clearfix\">\n        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n            <form class=\"form-horizontal\">\n                <fieldset>\n                    <legend>\n                        <div style=\"display:inline;\">\n                        Contents\n                        <button id=\"btnNew\" class=\"btn btn-sm btn-primary\" type=\"button\" style=\"cursor:pointer;height:34px;\" (click)=\"goNew()\" title=\"New\">\n                        <i id=\"lblNew\" class=\"glyphicon glyphicon-file\" style=\"padding-left:2px;\"></i> \n                        </button>\n                        </div>\n\n                        <div style=\"float:right;text-align:right;\">                                \n                            <div style=\"display:inline-block;padding-right:0px;width:280px;\">\n                                <div class=\"input-group\"> \n                                    <input class=\"form-control\" type=\"text\" id=\"isInputSearch\" placeholder=\"Search\" autocomplete=\"off\" spellcheck=\"false\" [(ngModel)]=\"_SearchString\" [ngModelOptions]=\"{standalone: true}\" (keyup.enter)=\"filterSearch()\">\n\n                                    <span id=\"btnSimpleSearch\" class=\"input-group-addon\">\n                                        <i class=\"glyphicon glyphicon-search\" style=\"cursor: pointer\" (click)=\"filterSearch()\"></i>\n                                    </span>\n                                </div>\n                            </div>\n\n                            <button id=\"btnToggleSearch\" class=\"btn btn-sm btn-secondary\" type=\"button\" style=\"cursor:pointer;text-align:center;margin-top:-24px;height:34px;\" (click)=\"btnToggleSearch_onClick()\" title=\"Collapse\">\n                                <i id=\"lblToggleSearch\" class=\"glyphicon glyphicon-menu-down\"></i> \n                            </button>\n\n                            <button *ngIf=\"false\" id=\"btnTogglePagination\" type=\"button\" class=\"btn btn-sm btn-default\" style=\"cursor:pointer;text-align:center;margin-left:25px;margin-top:-24px;width:30px;height:34px;\" (click)=\"btnTogglePagination_onClick()\" title=\"Collapse\">\n                                <i id=\"lblTogglePagination\" class=\"glyphicon glyphicon-eject\" style=\"color:#2e8690;margin-left:-4px;transform: rotate(90deg)\"></i>\n                            </button>\n\n                            <div id=\"divPagination\" style=\"display:none;margin-left:13px;margin-top:-7px;margin-bottom:5px;\">\n                                <pager6 id=\"pgPagination\" rpPageSizeMax=\"100\" [(rpModel)]=\"_ListingDataset.totalCount\" (rpChanged)=\"changedPager($event)\" style=\"font-size:14px;text-align:right;margin-top:-7px;margin-right:10px;\"></pager6>\n                            </div>\n\n                            <button id=\"btnClose\" type=\"button\" class=\"btn btn-sm btn-secondary\" (click)=\"closePage()\" (mouseenter)=\"btnClose_MouseEnter()\" (mouseleave)=\"btnClose_MouseLeave()\" title=\"Close\" style=\"cursor:pointer;height:34px;text-align:center;margin-top:-24px;\">\n                                <i id=\"lblClose\" class=\"glyphicon glyphicon-remove\"></i> \n                            </button>                     \n                        </div>\n                    </legend>\n\t\t\t\t\t<div class=\"cardview list-height\">\n\t\t\t\t\t\t<advanced-search id=\"asAdvancedSearch\" [FilterList]=\"_FilterList\" [TypeList]=\"_TypeList\" rpVisibleItems=5 (rpHidden)=\"renderAdvancedSearch($event)\" (rpChanged)=\"filterAdvancedSearch($event)\" style=\"display:none;\"></advanced-search>\n\n\t\t\t\t\t\t<div *ngIf=\"_shownull!=false\" style=\"color:#ccc;\">No result found!</div>\n\n\t\t\t\t\t\t<div *ngIf=\"_showListing\" class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-md-12\" style=\"overflow-x:auto;\">\n\t\t\t\t\t\t\t\t<table class=\"table table-striped table-condense table-hover tblborder\">\n\t\t\t\t\t\t\t\t\t<thead>\n\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t<th class=\"right\" style=\"width:3%\" title=\"No\">No.</th>\n\t\t\t\t\t\t\t\t\t\t\t<th class=\"left\"  style=\"width:15%\"title=\"Title\">Title</th>\n                                            <th class=\"left\" title=\"Content\">Content</th>\n                                            <th class=\"left\"  style=\"width:8%\"title=\"Status\">Status</th>\n\t\t\t\t\t\t\t\t\t\t\t<th class=\"left\"  style=\"width:12%\"title=\"Date\">Date</th>\n\t\t\t\t\t\t\t\t\t\t\t<th class=\"left\"  style=\"width:10%\"title=\"Posted By\">Posted By</th>\n\t\t\t\t\t\t\t\t\t\t\t<th class=\"left\"  style=\"width:10%\"title=\"Modified By\">Modified By</th>\n\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t</thead>\n\n\t\t\t\t\t\t\t\t\t<tbody>\n                                       <tr *ngFor=\"let obj of _ListingDataset.contentData\">\n\t\t\t\t\t\t\t\t\t\t\t<td class=\"right\"><a (click)=\"goto(obj.syskey)\" class=\"uni\">{{obj.srno}}</a></td>\n\t\t\t\t\t\t\t\t\t\t\t<td class=\"textwrap uni\" title=\"{{obj.t1}}\">{{obj.t1}}</td>\n                                            <td class=\"uni\" title=\"{{obj.t2}}\" >{{obj.t2}}</td>\n                                            <td class=\"uni\" >{{obj.statusdata}}</td>\n\t\t\t\t\t\t\t\t\t\t\t<td class=\"uni\" >{{obj.modifiedDate}} &nbsp;&nbsp;   {{obj.modifiedTime}}</td>\n\t\t\t\t\t\t\t\t\t\t\t<td class=\"uni\" >{{obj.userName}}</td>\n\t\t\t\t\t\t\t\t\t\t\t<td class=\"uni\" >{{obj.modifiedUserName}}</td>\n\t\t\t\t\t\t\t\t\t\t</tr> \n\t\t\t\t\t\t\t\t\t</tbody>\n\t\t\t\t\t\t\t\t</table>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div *ngIf=\"_showListing\" style=\"text-align:center\">Total {{ _ListingDataset.totalCount }}</div>\n\t\t\t\t\t</div>\n                </fieldset>\t\t\t\n\t\t\t</form>\n\n            <div id=\"ExcelDownload\" style=\"display: none;width: 0px;height: 0px;\"></div>\n            <!-- <div  id=\"loader\" class=\"modal\" ></div> -->\n            \n\n        </div> \n    </div>\n</div>\n\n<div [hidden] = \"_mflag\">\n    <div  id=\"loader\" class=\"modal\" ></div>\n</div>\n\n<div *ngIf='_divexport'>\n    <button type=\"button\" class=\"close\" (click)=\"goClosePrint()\" style=\"margin-top:-20px;\">&times;</button>\n    <iframe id=\"frame1\" [src]=\"_printUrl\" style=\"width: 100%;height: 95%;\"></iframe> \n</div>\n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_http_service_1.RpHttpService, rp_client_util_1.ClientUtil, rp_references_1.RpReferences])
    ], ContentMenuListComponent);
    return ContentMenuListComponent;
}());
exports.ContentMenuListComponent = ContentMenuListComponent;
//# sourceMappingURL=contentmenu-list.component.js.map