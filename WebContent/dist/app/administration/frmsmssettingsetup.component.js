"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var Rx_1 = require('rxjs/Rx');
core_2.enableProdMode();
var FrmSMSSettingComponent = (function () {
    function FrmSMSSettingComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
        this._obj = this.getDefaultObj();
        this._mflag = false;
        this._chkloginId = 'false';
        this._key = "";
        this.sessionAlertMsg = "";
        this._util = new rp_client_util_1.ClientUtil();
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = false;
            this.checkSession();
            this.getAllMerchant();
            this.getServiceLov();
            this.getFunctionLov();
            this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
            this._obj = this.getDefaultObj();
        }
    }
    FrmSMSSettingComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this._key = id;
                _this.goReadBySyskey(id);
            }
        });
    };
    FrmSMSSettingComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    FrmSMSSettingComponent.prototype.getDefaultObj = function () {
        jQuery("#mydelete").prop("disabled", true);
        jQuery("#mySave").prop("disabled", false);
        this._onlyfrom = false;
        this._combohide = true;
        this._chkread = false;
        return { "code": "", "desc": "", "id": 0, "syskey": 0, "serviceCode": "", "serviceDesc": "", "funCode": "", "funDesc": "", "site": "", "from": "", "to": "", "fromMsg": "", "toMsg": "", "message": "", "merchantID": "", "operatorType": "", "active": "", "sessionID": "", "userID": "" };
    };
    FrmSMSSettingComponent.prototype.goReadBySyskey = function (p) {
        var _this = this;
        try {
            this._mflag = false;
            this._chkread = true;
            var url = this.ics._apiurl + 'service001/getSMSSettingDataById?id=' + p + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
            this.http.doGet(url).subscribe(function (data) {
                if (data.code == '0016') {
                    _this.sessionAlertMsg = data.desc;
                    _this.showMessage();
                }
                else {
                    jQuery("#mydelete").prop("disabled", false);
                    _this._obj = data;
                    if (_this._obj.serviceCode == "1") {
                        _this._onlyfrom = true;
                    }
                    else {
                        _this._onlyfrom = false;
                    }
                    if (_this._obj.from == "0") {
                        _this._obj.site = "To";
                        _this._obj.message = _this._obj.toMsg;
                    }
                    else {
                        _this._obj.site = "From";
                        _this._obj.message = _this._obj.fromMsg;
                    }
                    if (_this._obj.funCode == '1' || _this._obj.funCode == '2') {
                        _this._combohide = false;
                    }
                    else {
                        _this._combohide = true;
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmSMSSettingComponent.prototype.goList = function () {
        this._router.navigate(['/smssettinglist']);
    };
    FrmSMSSettingComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this.goLogOut();
        });
    };
    FrmSMSSettingComponent.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    /* checkSession() {
      try {
        let url: string = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
        this.http.doGet(url).subscribe(
          data => {
            if (data.msgCode == '0016') {
              this.sessionAlertMsg = data.msgDesc;
              this.showMessage();
            }
          },
          error => {
            if (error._body.type == 'error') {
              alert("Connection Timed Out!");
            }
            else {
  
            }
          }, () => { });
      } catch (e) {
        alert("Invalid URL");
      }
  
    } */
    FrmSMSSettingComponent.prototype.showMessageAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmSMSSettingComponent.prototype.clearData = function () {
        this._obj = this.getDefaultObj();
        this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
        this._chkloginId = 'false';
        jQuery("#mydelete").prop("disabled", true);
        jQuery("#mySave").prop("disabled", false);
    };
    FrmSMSSettingComponent.prototype.goNew = function () {
        this.clearData();
    };
    FrmSMSSettingComponent.prototype.changeFuction = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex; //Get Selected Index
        var value = options[options.selectedIndex].value; //Get Selected Index's Value 
        this._obj.funCode = value;
        if (this._obj.funCode == '1' || this._obj.funCode == '2') {
            this._combohide = false;
        }
        else {
            this._combohide = true;
            this._obj.merchantID = "";
        }
        for (var i = 0; i < this.ref._lov3.reffunction.length; i++) {
            if (this.ref._lov3.reffunction[i].value == value) {
                this._obj.funCode = this.ref._lov3.reffunction[i].value;
                this._obj.funDesc = this.ref._lov3.reffunction[i].caption;
                break;
            }
        }
    };
    FrmSMSSettingComponent.prototype.getFunctionLov = function () {
        var _this = this;
        this._mflag = false;
        this.http.doGet(this.ics._apiurl + 'service001/getFunctionLov').subscribe(function (data) {
            _this.ref._lov3.reffunction = data.reffunction;
            var arr = [];
            if (_this.ref._lov3.reffunction != null) {
                if (!(_this.ref._lov3.reffunction instanceof Array)) {
                    var m = [];
                    m[1] = _this.ref._lov3.reffunction;
                    arr.push(m[1]);
                }
                for (var j = 0; j < _this.ref._lov3.reffunction.length; j++) {
                    arr.push(_this.ref._lov3.reffunction[j]);
                }
            }
            _this.ref._lov3.reffunction = arr;
            _this._mflag = true;
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
            else {
            }
        }, function () { });
    };
    FrmSMSSettingComponent.prototype.changeService = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex; //Get Selected Index
        var value = options[options.selectedIndex].value; //Get Selected Index's Value 
        this._obj.serviceCode = value;
        if (this._obj.serviceCode == "1") {
            this._onlyfrom = true;
        }
        else {
            this._onlyfrom = false;
        }
        for (var i = 0; i < this.ref._lov3.refservice.length; i++) {
            if (this.ref._lov3.refservice[i].value == value) {
                this._obj.serviceCode = this.ref._lov3.refservice[i].value;
                this._obj.serviceDesc = this.ref._lov3.refservice[i].caption;
                break;
            }
        }
    };
    FrmSMSSettingComponent.prototype.getServiceLov = function () {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'service001/getServiceLov').subscribe(function (data) {
            _this.ref._lov3.refservice = data.refservice;
            var arr = [];
            if (_this.ref._lov3.refservice != null) {
                if (!(_this.ref._lov3.refservice instanceof Array)) {
                    var m = [];
                    m[1] = _this.ref._lov3.refservice;
                    arr.push(m[1]);
                }
                for (var j = 0; j < _this.ref._lov3.refservice.length; j++) {
                    arr.push(_this.ref._lov3.refservice[j]);
                }
            }
            _this.ref._lov3.refservice = arr;
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
            else {
            }
        }, function () { });
    };
    FrmSMSSettingComponent.prototype.getAllMerchant = function () {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'service001/getmerchantidlistdetail').subscribe(function (data) {
            _this.ref._lov3.ref015 = data.ref015;
            if (_this.ref._lov3.ref015 != null) {
                if (!(_this.ref._lov3.ref015 instanceof Array)) {
                    var m = [];
                    m[0] = _this.ref._lov3.ref015;
                    _this.ref._lov3.ref015 = m;
                }
            }
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
        }, function () { });
    };
    FrmSMSSettingComponent.prototype.goSave = function () {
        if (this._obj.serviceCode == '') {
            this.showMessageAlert("Please select Service");
        }
        else if (this._obj.funCode == '') {
            this.showMessageAlert("Please select Function");
        }
        else if (this._obj.site == '') {
            this.showMessageAlert("Please select Site");
        }
        else if (this._obj.message == '') {
            this.showMessageAlert("Please input Message");
        }
        else if (this._obj.active == '') {
            this.showMessageAlert("Please select Active");
        }
        else if (this._combohide == false) {
            if (this._obj.merchantID == '') {
                this.showMessageAlert("Please select Merchant");
            }
            else {
                this.saveSMSSetting();
            }
        }
        else {
            this.saveSMSSetting();
        }
    };
    FrmSMSSettingComponent.prototype.saveSMSSetting = function () {
        var _this = this;
        try {
            this._mflag = false;
            if (this._obj.site == "From") {
                this._obj.fromMsg = this._obj.message;
                this._obj.from = "1";
                this._obj.to = "0";
            }
            else {
                this._obj.toMsg = this._obj.message;
                this._obj.from = "0";
                this._obj.to = "1";
            }
            this._obj.sessionID = this.ics._profile.sessionID;
            this._obj.userID = this.ics._profile.userID;
            var url = this.ics._apiurl + 'service001/saveSMSSetting';
            var json = this._obj;
            this.http.doPost(url, json).subscribe(function (data) {
                _this._returnResult = data;
                if (_this._returnResult.state == 'true') {
                    _this._obj.id = _this._returnResult.keyResult;
                    jQuery("#mydelete").prop("disabled", false);
                    _this.showMessageAlert(data.msgDesc);
                }
                else {
                    if (_this._returnResult.msgCode == '0016') {
                        _this.sessionAlertMsg = data.desc;
                        _this.showMessage();
                    }
                    else {
                        _this.showMessageAlert(data.msgDesc);
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmSMSSettingComponent.prototype.goDelete = function () {
        var _this = this;
        try {
            this._mflag = false;
            this._obj.sessionID = this.ics._profile.sessionID;
            this._obj.userID = this.ics._profile.userID;
            var json = this._obj;
            var url = this.ics._apiurl + 'service001/deleteSMSSetting';
            this.http.doPost(url, json).subscribe(function (data) {
                _this._returnResult = data;
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.desc;
                    _this.showMessage();
                }
                else {
                    _this.showMessageAlert(data.msgDesc);
                    if (data.state) {
                        _this.clearData();
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmSMSSettingComponent.prototype.getCodeFromLovdeatils = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics._apiurl + 'service001/getCodeFromLovdeatils?merchantId=' + this._obj.merchantID).subscribe(function (data) {
                if (data != null && data != undefined) {
                    if (!(data.reflovdetails instanceof Array)) {
                        var m = [];
                        m[0] = data.reflovdetails;
                        _this.ref._lov3.reflovdetails = m;
                    }
                    else {
                        _this.ref._lov3.reflovdetails = data.reflovdetails;
                    }
                }
                else {
                    _this.ref._lov3.reflovdetails = [{ "value": "", "caption": "-" }];
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmSMSSettingComponent.prototype.updateComboData = function (event) {
        this._obj.merchantID = event;
        this.getCodeFromLovdeatils();
    };
    FrmSMSSettingComponent.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    FrmSMSSettingComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsgAlert(data.desc);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsgAlert(data.desc);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmSMSSettingComponent.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmSMSSettingComponent.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    FrmSMSSettingComponent = __decorate([
        core_1.Component({
            selector: 'smssetting-setup',
            template: "\n    <div class=\"container\">\n    <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n    <form class= \"form-horizontal\" (ngSubmit) = \"goSave()\"> \n    <!-- Form Name -->\n    <legend>SMS Setting </legend>\n    <div class=\"row  col-md-12\">  \n      <button class=\"btn btn-primary\" type=\"button\" (click)=\"goList()\" >List</button> \n      <button class=\"btn btn-primary\" type=\"button\" (click)=\"goNew()\" >New</button>      \n      <button class=\"btn btn-primary\" id=\"mySave\" type=\"submit\" >Save</button>          \n      <button class=\"btn btn-primary\" disabled id=\"mydelete\"  type=\"button\" (click)=\"goDelete();\" >Delete</button> \n    </div>\n    <div class=\"row col-md-12\">&nbsp;</div> \n\n    <div class=\"form-group\">    \n    <div class=\"col-md-10\">\n\n    <div class=\"form-group\">\n      <label class=\"col-md-3\">Service</label>\n      <div class=\"col-md-4\" > \n        <select *ngIf =\"_chkread==true\" disabled [(ngModel)]=_obj.serviceCode [ngModelOptions]=\"{standalone: true}\"  class=\"form-control col-md-0\" (change)=\"changeService($event)\"  >\n          <option *ngFor=\"let item of ref._lov3.refservice\" value=\"{{item.value}}\" >{{item.caption}}</option> \n        </select>    \n    \n        <select *ngIf =\"_chkread !=true\"  [(ngModel)]=_obj.serviceCode [ngModelOptions]=\"{standalone: true}\"  class=\"form-control col-md-0\" (change)=\"changeService($event)\"  >\n          <option *ngFor=\"let item of ref._lov3.refservice\" value=\"{{item.value}}\" >{{item.caption}}</option> \n        </select>           \n      </div> \n    </div>\n\n    <div class=\"form-group\">\n      <label class=\"col-md-3\">Function</label>\n      <div class=\"col-md-4\" > \n        <select *ngIf =\"_chkread==true\" disabled [(ngModel)]=_obj.funCode [ngModelOptions]=\"{standalone: true}\" class=\"form-control col-md-0\" (change)=\"changeFuction($event)\">\n          <option *ngFor=\"let item of ref._lov3.reffunction\" value=\"{{item.value}}\" >{{item.caption}}</option> \n        </select>  \n        \n        <select *ngIf =\"_chkread!=true\" [(ngModel)]=_obj.funCode [ngModelOptions]=\"{standalone: true}\" class=\"form-control col-md-0\" (change)=\"changeFuction($event)\">\n          <option *ngFor=\"let item of ref._lov3.reffunction\" value=\"{{item.value}}\" >{{item.caption}}</option> \n        </select> \n      </div>  \n    </div>\n\n    <div class=\"form-group\">\n      <label class=\"col-md-3\">Which Site</label>\n      <div class=\"col-md-4\" *ngIf=\"_onlyfrom == false\"> \n        <select *ngIf =\"_chkread==true\" disabled [(ngModel)]=_obj.site [ngModelOptions]=\"{standalone: true}\" class=\"form-control col-md-0\">\n          <option *ngFor=\"let item of ref._lov1.refsite\" value=\"{{item.value}}\" >{{item.caption}}</option>\n        </select>    \n        \n        <select *ngIf =\"_chkread!=true\"  [(ngModel)]=_obj.site [ngModelOptions]=\"{standalone: true}\" class=\"form-control col-md-0\">\n          <option *ngFor=\"let item of ref._lov1.refsite\" value=\"{{item.value}}\" >{{item.caption}}</option>\n        </select>  \n      </div>\n\n      <div class=\"col-md-4\"  *ngIf=\"_onlyfrom == true \">\n        <select *ngIf =\"_chkread==true\" disabled  [(ngModel)]=_obj.site [ngModelOptions]=\"{standalone: true}\" class=\"form-control col-md-0\">\n          <option *ngFor=\"let item of ref._lov1.reffrsite\" value=\"{{item.value}}\" >{{item.caption}}</option>                   \n        </select>   \n        \n        <select *ngIf =\"_chkread!=true\"  [(ngModel)]=_obj.site [ngModelOptions]=\"{standalone: true}\" class=\"form-control col-md-0\">\n          <option *ngFor=\"let item of ref._lov1.reffrsite\" value=\"{{item.value}}\" >{{item.caption}}</option>                   \n        </select>                  \n      </div> \n    </div> \n\n    <div *ngIf=\"_combohide == false\">  \n    <div class=\"form-group\">\n         <label class=\"col-md-3\"> Merchant ID </label>\n         <div class=\"col-md-4\">\n               <select [(ngModel)]=_obj.merchantID [ngModelOptions]=\"{standalone: true}\"  class=\"form-control col-md-0\" (change)=\"updateComboData($event.target.value)\">\n               <option *ngFor=\"let item of ref._lov3.ref015\" value=\"{{item.value}}\" >{{item.caption}}</option> \n              </select> \n         </div>\n         <div class=\"col-md-1\"> \n          {{_obj.merchantID}} \n          </div>\n    </div>\n   </div>\n \n    <div class=\"form-group\">\n      <label class=\"col-md-3\" >Active</label>\n      <div class=\"col-md-4\">\n        <select [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=_obj.active class=\"form-control col-md-0\">\n          <option *ngFor=\"let item of ref._lov1.refActive\" value=\"{{item.value}}\" >{{item.caption}}</option> \n        </select>                \n      </div> \n    </div>\n\n    <div class=\"form-group\">\n      <label class=\"col-md-3\"> Message </label>\n      <div class=\"col-md-4\">\n        <textarea  class=\"form-control\" [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"_obj.message\" rows=\"7\" maxlength=\"200\"></textarea>                 \n      </div> \n    </div>\n\n    </div>\n    </div>\n    </form>\n    </div>\n    </div>\n    </div>\n       \n    <div id=\"sessionalert\" class=\"modal fade\">\n      <div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n          <div class=\"modal-body\">\n            <p>{{sessionAlertMsg}}</p>\n          </div>\n          <div class=\"modal-footer\">\n          </div>\n        </div>\n      </div>\n    </div>\n\n<div [hidden] = \"_mflag\">\n  <div class=\"modal\" id=\"loader\"></div>\n</div> \n    \n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmSMSSettingComponent);
    return FrmSMSSettingComponent;
}());
exports.FrmSMSSettingComponent = FrmSMSSettingComponent;
//# sourceMappingURL=frmsmssettingsetup.component.js.map