"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
core_1.enableProdMode();
var FrmVideoList = (function () {
    function FrmVideoList(ics, ref, _router, http) {
        this.ics = ics;
        this.ref = ref;
        this._router = _router;
        this.http = http;
        this._totalcount = 1;
        this.hide = true;
        this._searchVal = "";
        this._util = new rp_client_util_1.ClientUtil();
        this._obj = { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": null, "modifiedDate": "", "modifiedTime": null, "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "Video", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [], "videoUpload": [], "cropdata": null, "agrodata": null, "ferdata": null, "towndata": null, "crop": null, "fert": null, "agro": null, "addTown": null, "modifiedUserId": "", "modifiedUserName": "" };
        this._array = [];
        this._pager = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 0, "size": 0, "totalcount": 1, "t1": "", "userid": "" };
        this._StateType = "";
        this._roleval = "";
        this.mstatus = 0;
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        this._roleval = ics._profile.t1;
        this.mstatus = ics._profile.loginStatus;
        this.hide = true;
        this.search(this._pager);
        //this.getCropComboList();
        this.getStatusList(this.mstatus);
    }
    FrmVideoList.prototype.ChangeStatus = function (options) {
        this._StateType = options[options.selectedIndex].value;
        this.searchVal();
    };
    FrmVideoList.prototype.search = function (p) {
        var _this = this;
        if (p.end == 0) {
            p.end = this.ics._profile.n1;
        }
        if (p.size == 0) {
            p.size = this.ics._profile.n1;
        }
        p.t1 = this._searchVal;
        p.userid = this.ics._profile.userID;
        // let url: string = this.ics._apiurl + 'serviceArticle/searchVideoList?searchVal=' + this._searchVal + '&croptype=' + this._CropType + '&status=' + this.mstatus + '&statetype=' + this._StateType;
        //let url: string = this.ics._apiurl + 'serviceArticle/searchVideoList?searchVal=' + this._searchVal + '&status=' + this.mstatus + '&statetype=' + this._StateType + '&searchtype=' + 'Video';
        if (this._StateType == undefined || this._StateType == null || this._StateType == '') {
            this._StateType = "5";
        }
        var url = this.ics._apiurl + 'serviceArticle/searchVideoList?searchVal=' + this._searchVal + '&status=' + this.mstatus + '&statetype=' + this._StateType;
        var json = p;
        this.http.doPost(url, json).subscribe(function (response) {
            if (response != null && response != undefined && response.state) {
                _this._totalcount = response.totalCount;
                _this.hide = false;
                _this._array = response.data;
                for (var i = 0; i < _this._array.length; i++) {
                    if (_this._array[i].t2.length > 100) {
                        _this._array[i].t2 = _this._array[i].t2.substring(0, 101) + "....";
                    }
                    _this._array[i].modifiedDate = _this._util.changeStringtoDateFromDB(_this._array[i].modifiedDate);
                }
            }
            else {
                _this.hide = true;
                _this._array = [];
                _this._totalcount = 1;
                _this.showMessage("Data not found!", false);
            }
        }, function (error) { }, function () { });
    };
    FrmVideoList.prototype.searchVal = function () {
        this._pager = { "current": 1, "prev": 1, "last": 1, "next": 1, "start": 1, "end": 0, "size": 0, "totalcount": 1, "t1": "", "userid": "" };
        this.search(this._pager);
    };
    FrmVideoList.prototype.goto = function (p) {
        this._router.navigate(['/video', 'read', p]);
    };
    FrmVideoList.prototype.goNew = function () {
        this._router.navigate(['/video', 'new']);
    };
    FrmVideoList.prototype.changedPager = function (event) {
        var k = event.flag;
        this._pager = event.obj;
        if (k) {
            this.search(this._pager);
        }
    };
    FrmVideoList.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    FrmVideoList.prototype.getStatusList = function (mstatus) {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'serviceQuestion/getStatusList?status=' + this.mstatus).subscribe(function (response) {
            if ((response != null || response != undefined) && response.data.length > 0) {
                _this.ref._lov3.statuscombo = _this._util.changeArray(response.data, _this._obj, 1);
                _this._StateType = _this.ref._lov3.statuscombo[3].value;
            }
        }, function (error) { }, function () { });
    };
    FrmVideoList = __decorate([
        core_1.Component({
            selector: 'fmr-videoList',
            template: "\n    <div class=\"container\">\n    <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n        <form class=\"form-inline\">\n        <!--<div class=\"col-md-12\">-->\n                <legend>Videos</legend>\n        <!--</div>-->\n\t\t<div class=\"form-group\">\n            <div class=\"input-group\" style=\"padding-right: 10px;\">\n                <span class=\"input-group-btn input-md\">\n                    <button class=\"btn btn-primary\" type=\"button\" (click)=\"goNew();\">New</button>\n                </span>  \n                <input id=\"textinput\" name=\"textinput\" type=\"text\" placeholder=\"Search\" [(ngModel)]=\"_searchVal\" (keyup.enter)=\"searchVal()\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control input-md\">\n                <span class=\"input-group-btn input-md\">\n                    <button class=\"btn btn-primary input-md\" type=\"button\" (click)=\"searchVal()\" >\n                        <span class=\"glyphicon glyphicon-search\"></span>Search\n                    </button>\n                </span> \n            </div>  \n\t\t\t\t<div class=\"input-group\">\t\t\t\n\t\t\t\t<label class= \"col-md-4 control-label\">Status</label>\n\t\t\t\t\t<div class= \"col-md-8\">                  \n\t\t\t\t\t\t<select [(ngModel)]=\"_StateType\" (change)=\"ChangeStatus($event.target.options)\" class=\"form-control col-md-0\" [ngModelOptions]=\"{standalone: true}\" required>\n                        <option *ngFor=\"let item of ref._lov3.statuscombo\" value=\"{{item.value}}\">{{item.caption}}</option>\n\t\t\t\t\t\t</select>\n\t\t\t\t\t</div>\t\n\t\t\t\t</div>\n\t\t</div>\n            <!--div class=\"input-group\">\n                <label class=\"col-md-4\" style=\"padding-left: 0px;\">Status</label>\n                <div class=\"col-md-8\" style=\"padding-left: 0px;\">\n                    <select [(ngModel)]=\"_StateType\" (change)=\"ChangeStatus($event.target.options)\" class=\"form-control col-md-0\" [ngModelOptions]=\"{standalone: true}\" required>\n                        <option *ngFor=\"let item of ref._lov3.statuscombo\" value=\"{{item.value}}\">{{item.caption}}</option>\n                    </select>\n                </div>\n            </div>-->\n        </form>\n    \n        <pager id=\"pgarticle\" [(rpModel)]=\"_totalcount\" [(rpCurrent)]=\"_pager.current\" (rpChanged)=\"changedPager($event)\"></pager> \n        \n        <div class=\"table-responsive\" *ngIf=\"hide==false\">\n            <table class=\"table table-striped table-condensed table-hover tblborder\" id=\"tblstyle\">\n                <thead>\n                    <tr>\n                        <th>Content</th>\n                        <th>Video</th>\n                        <th>Status</th>\n                        <th>Date</th>\n                        <th>Time</th>\n                        <th>Posted By</th>\n                        <th>Modified By</th>\n                    </tr>\n                </thead>\n                <tbody *ngFor=\"let obj of _array;let num = index\">\n                    <tr>\n                        <td><a (click)=\"goto(obj.syskey)\" class=\"uni\"><p>{{obj.t2}}</p></a></td>\n                        <td *ngIf = \"(obj.n10 == 0)\"><p>{{obj.videoUpload[0]}}</p></td>\n                        <td *ngIf = \"(obj.n10 == 1)\"><p>{{obj.t8}}</p></td>\n                        <td><p class=\"uni\">{{obj.t10}}</td>\n                        <td><p class=\"uni\">{{obj.modifiedDate}}</p></td>\n                        <td><p class=\"uni\">{{obj.modifiedTime}}</p></td>\n                        <td class=\"uni\">{{obj.userName}}</td>\n                        <td class=\"uni\">{{obj.modifiedUserName}}</td>\n                    </tr>\n                </tbody>\n            </table>\n        </div>\n    </div>\n    </div>\n    </div>  \n"
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, rp_references_1.RpReferences, router_1.Router, rp_http_service_1.RpHttpService])
    ], FrmVideoList);
    return FrmVideoList;
}());
exports.FrmVideoList = FrmVideoList;
//# sourceMappingURL=frmvideo-list.component.js.map