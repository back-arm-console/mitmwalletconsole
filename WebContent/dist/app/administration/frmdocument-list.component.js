"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var platform_browser_1 = require("@angular/platform-browser");
core_1.enableProdMode();
var FrmDocumentList = (function () {
    function FrmDocumentList(ics, ref, _router, route, http, sanitizer) {
        this.ics = ics;
        this.ref = ref;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.sanitizer = sanitizer;
        this._totalcount = 1;
        this.hide = true;
        this._searchVal = "";
        this._StatusType = "";
        this._util = new rp_client_util_1.ClientUtil();
        this._obj = { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": null, "modifiedDate": "", "modifiedTime": null, "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "Video", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [], "videoUpload": [], "cropdata": null, "agrodata": null, "ferdata": null, "towndata": null, "crop": null, "fert": null, "agro": null, "addTown": null, "postedby": "", "modifiedby": "" };
        this._array = [];
        this._pager = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 0, "size": 0, "totalcount": 1, "t1": "", "userid": "" };
        this._roleval = "";
        this.mstatus = 0;
        this._mflag = false;
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        this._roleval = ics._profile.t1;
        this.hide = true;
        //this.mstatus = ics._profile.loginStatus; 
        this.getStatusList(this.mstatus);
        this.search(this._pager);
    }
    /* ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
          let cmd = params['cmd']; if (cmd != null && cmd != "" && cmd == "read") {
            this._pager.region= params['id'];
            this.search(this._pager);
          }
        });
      } */
    FrmDocumentList.prototype.getStatusList = function (mstatus) {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'serviceQuestion/getStatusList?status=' + this.mstatus).subscribe(function (response) {
            if ((response != null || response != undefined) && response.data.length > 0) {
                _this.ref._lov3.statuscombo = _this._util.changeArray(response.data, _this._obj, 1);
                _this._StatusType = _this.ref._lov3.statuscombo[3].value;
            }
        }, function (error) { }, function () { });
    };
    FrmDocumentList.prototype.ChangeStatus = function (options) {
        this._StatusType = options[options.selectedIndex].value;
        this.searchVal();
    };
    FrmDocumentList.prototype.searchVal = function () {
        this._pager = { "current": 1, "prev": 1, "last": 1, "next": 1, "start": 1, "end": 0, "size": 0, "totalcount": 1, "t1": "", "userid": "" };
        this.search(this._pager);
    };
    FrmDocumentList.prototype.search = function (p) {
        var _this = this;
        this._mflag = false;
        if (p.end == 0) {
            p.end = this.ics._profile.n1;
        }
        if (p.size == 0) {
            p.size = this.ics._profile.n1;
        }
        p.t1 = this._searchVal;
        p.userid = this.ics._profile.userID;
        /* if (this._StatusType == undefined || this._StatusType == null || this._StatusType == '') {
            this._StatusType = "Publish";
        } */
        var url = this.ics._apiurl + 'serviceVideo/searchDocumentList?searchVal=' + this._searchVal + '&userID=' + this.ics._profile.userID + '&statustype=' + this._StatusType;
        var json = p;
        this.http.doPost(url, json).subscribe(function (response) {
            if (response != null && response != undefined && response.state) {
                _this._totalcount = response.totalCount;
                _this._array = response.data;
                _this.hide = false;
                for (var i = 0; i < _this._array.length; i++) {
                    _this._array[i].modifiedDate = _this._util.changeStringtoDateFromDB(_this._array[i].modifiedDate);
                }
            }
            else {
                _this.hide = true;
                _this._array = [];
                _this._totalcount = 1;
                _this.showMsg("Data not found!", false);
            }
            _this._mflag = true;
        }, function (error) { }, function () { });
    };
    FrmDocumentList.prototype.goto = function (p) {
        this._router.navigate(['/document', 'read', p]);
    };
    FrmDocumentList.prototype.goNew = function () {
        this._router.navigate(['/document', 'new']);
    };
    FrmDocumentList.prototype.changedPager = function (event) {
        var k = event.flag;
        this._pager = event.obj;
        if (k) {
            this.search(this._pager);
        }
    };
    FrmDocumentList.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    FrmDocumentList = __decorate([
        core_1.Component({
            selector: 'fmr-documentList',
            template: "\n    <div class=\"container\">\n    <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n        <form class=\"form-inline\">\n        <!--<div class=\"col-md-12\">-->\n                <legend>Documents</legend>\n        <!--</div>-->\n            <div class=\"input-group\" style=\"padding-right: 10px;\">\n                <span class=\"input-group-btn input-md\">\n                    <button class=\"btn btn-primary\" type=\"button\" (click)=\"goNew();\">New</button>\n                </span>  \n                <input id=\"textinput\" name=\"textinput\" type=\"text\" placeholder=\"Search\" [(ngModel)]=\"_searchVal\" (keyup.enter)=\"searchVal()\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control input-md\">\n                <span class=\"input-group-btn input-md\">\n                    <button class=\"btn btn-primary input-md\" type=\"button\" (click)=\"searchVal()\" >\n                        <span class=\"glyphicon glyphicon-search\"></span>Search\n                    </button>\n                </span> \n            </div> \n            <!--<div class=\"input-group\">\n                <label class=\"col-md-4\" style=\"padding-left: 0px;\">Status</label>\n                <div class=\"col-md-8\" style=\"padding-left: 0px;\">\n                    <select [(ngModel)]=\"_StatusType\" (change)=\"ChangeStatus($event.target.options)\" class=\"form-control col-md-0\" [ngModelOptions]=\"{standalone: true}\" required>\n                        <option *ngFor=\"let item of ref._lov3.statuscombo\" value=\"{{item.value}}\">{{item.caption}}</option>\n                    </select>\n                </div>\n            </div>-->\n        </form>\n    \n        <pager id=\"pgarticle\" [(rpModel)]=\"_totalcount\" [(rpCurrent)]=\"_pager.current\" (rpChanged)=\"changedPager($event)\"></pager> \n        \n        <div class=\"table-responsive\" *ngIf=\"hide==false\">\n            <table class=\"table table-striped table-condensed table-hover tblborder\" id=\"tblstyle\">\n                <thead>\n                    <tr>\n                        <th>ID</th>\n                        <th>Title</th>\n                        <th>File Name</th>\n                        <th>Status</th>\n                        <th>Date</th> \n\t\t\t\t\t\t<th>Time</th> \t\t\t\t\t\t\n                        <th>Posted By</th>\n                        <th>Modified By</th>\n                    </tr>\n                </thead>\n                <tbody *ngFor=\"let obj of _array;let i = index\">\n                    <tr>\n                        <td><a (click)=\"goto(obj.autokey)\" class=\"uni\">{{(i+1)}}</a></td>\n                        <td>{{obj.title}}</td>\n                        <!--<td (click)=\"goDownloadFile(obj)\"><u class=\"uni\">{{obj.fileName}}</u></td>\t-->\n\t\t\t\t\t\t<td><a target=\"_blank\" rel=\"noopener noreferrer\" href=\"{{obj.filePath}}{{obj.fileName}}\">{{obj.fileName}}</a></td>\n                        <td><p class=\"uni\">{{obj.status}}</td>\n                        <td><p class=\"uni\">{{obj.modifiedDate}}</p></td> \n\t\t\t\t\t\t<td><p class=\"uni\">{{obj.modifiedTime}}</p></td> \t\t\t\t\t\t\n                        <td class=\"uni\">{{obj.postedby}}</td>\n                        <td class=\"uni\">{{obj.modifiedby}}</td>\n                    </tr>\n                </tbody>\n            </table>\n        </div>\n    </div>\n    </div>\n    </div>\n    <div [hidden]=\"_mflag\">\n    <div class=\"modal\" id=\"loader\"></div>\n    </div>  \n"
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, rp_references_1.RpReferences, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, platform_browser_1.DomSanitizer])
    ], FrmDocumentList);
    return FrmDocumentList;
}());
exports.FrmDocumentList = FrmDocumentList;
//# sourceMappingURL=frmdocument-list.component.js.map