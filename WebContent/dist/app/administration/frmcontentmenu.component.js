"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
require('tinymce/tinymce.min');
require('tinymce/themes/modern/theme');
require('tinymce/plugins/link/plugin.js');
require('tinymce/plugins/paste/plugin.js');
require('tinymce/plugins/table/plugin.js');
require('tinymce/plugins/advlist/plugin.js');
require('tinymce/plugins/autoresize/plugin.js');
require('tinymce/plugins/lists/plugin.js');
require('tinymce/plugins/code/plugin.js');
require('tinymce/plugins/image/plugin.js');
require('tinymce/plugins/imagetools/plugin.js');
var FrmContentMenu = (function () {
    function FrmContentMenu(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this.flagnew = true;
        this.flagsave = true;
        this.flagaca = true;
        this.flagdelete = true;
        this.falgaccombo = true;
        this.flagview = true;
        this.flagCombo = "true";
        this.dates = { "_date": null };
        // alertenddates = { "_date": null };
        this._file = null;
        this._fileName = '';
        this._util = new rp_client_util_1.ClientUtil();
        this._time = "";
        this._str = "";
        this.flagcanvas = true;
        //_crop = [{ "value": "", "caption": "", "flag": false }];
        this._obj = { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": "", "modifiedDate": "", "modifiedTime": "", "userId": "", "userName": "", "modifiedUserId": "", "modifiedUserName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [], "videoUpload": [], "comData": [], "cropdata": [], "agrodata": [], "ferdata": [], "towndata": [], "crop": null, "fert": null, "agro": null, "addTown": null, "uploadlist": [], "resizelist": [] };
        this.uploadDataList = [];
        this.uploadDataListResize = [];
        this.uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
        this.serial = 0;
        this._viewsk = 0;
        this.ImagePath = "";
        this._noti = { "flag": false };
        // _alert = { "flag": false };
        this.flagAcademic = false;
        this._roleval = "";
        this.onEditorKeyup = new core_1.EventEmitter();
        this._checkcw = false;
        this._checkeditor = false;
        this._checkpubisher = false;
        this._checkadmin = false;
        this._image = "";
        this._urllink = "";
        this.content = 0;
        this._img = "";
        this._result = { "longResult": "", "msgDesc": "", "key": [], "keyResult": 0, "state": "", "stringResult": "" };
        this.flag = false;
        this.pushcaption = "";
        this.splicearrindex = -1;
        this.splicearrindex1 = -1;
        this.substitueindex = 0;
        //alert("status: " + this.ics._profile.loginStatus);
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        this._roleval = ics._profile.t1;
        this._datepickerOpts = this.ics._datepickerOpts;
        this._datepickerOpts.format = 'dd/mm/yyyy';
        this.flagnew = false;
        this.flagsave = false;
        this.flagdelete = true;
        this.falgaccombo = true;
        //this.setBtns();
        this.flagview = true;
        //this.rd = false;
        //this.getCropList();
        //this.getAcademicComboList();
        this.getStateList();
        this._time = this._util.getTodayTime();
        this._obj.createdTime = this._time;
        this._obj.modifiedTime = this._time;
        if (this.ics._profile.loginStatus == 1) {
            this.ref._lov3.postingstate = [];
            this.ref._lov3.postingstate = this.ref._lov3.postingstate0;
            this._checkcw = true;
        }
        if (this.ics._profile.loginStatus == 2) {
            this.ref._lov3.postingstate = [];
            this.ref._lov3.postingstate = this.ref._lov3.postingstate1;
            this._checkeditor = true;
        }
        if (this.ics._profile.loginStatus == 3) {
            //  this.ref._lov3.postingstate = [];
            this.ref._lov3.postingstate = this.ref._lov3.postingstate2;
            this._checkpubisher = true;
        }
        if (this.ics._profile.loginStatus == 4) {
            this.ref._lov3.postingstate = [];
            this.ref._lov3.postingstate = this.ref._lov3.postingstateadmin;
            this._checkadmin = true;
        }
        if (this.ics._profile.loginStatus == 5) {
            this.ref._lov3.postingstate = [];
            this.ref._lov3.postingstate = this.ref._lov3.postingstateadmin;
            this._checkadmin = true;
        }
    }
    FrmContentMenu.prototype.ngOnInit = function () {
        var _this = this;
        var self = this;
        var urllink1 = this._urllink;
        var ics = this.ics;
        var http = this.http;
        var uploadData = this.uploadData;
        var uploadDataList = this.uploadDataList;
        var uploadDataListResize = this.uploadDataList;
        var popupMessage = this.popupMessage;
        var serial = this.serial;
        tinymce.init({
            selector: 'textarea',
            body_class: 'uni',
            menubar: false,
            paste_data_images: true,
            toolbar: "bold italic | alignleft aligncenter alignright alignjustify | image",
            plugins: ['emoticons', 'link', 'modern', 'paste', 'table', 'image', 'imagetools'],
            file_picker_callback: function (callback, value, meta) {
                if (meta.filetype == 'image') {
                    //$('#upload input').click();
                    $('#upload').trigger('click');
                    $('#upload').on('change', function () {
                        var file = this.files[0];
                        var reader = new FileReader();
                        var imgname = "";
                        reader.onload = function (e) {
                            //added from original image uploaded
                            var index = file.name.lastIndexOf(".");
                            var imagename = file.name.substring(index);
                            imagename = imagename.toLowerCase();
                            if (imagename == ".jpg" || imagename == ".jpeg" || imagename == ".png") {
                                var url = ics._apiurl + 'file/fileupload?f=upload&fn=' + file.name + '&id=' + ics._profile.t1 + '&type=' + self._obj.n4 + '&imgUrl=' + ics._imgurl;
                                http.upload(url, file).subscribe(function (data) {
                                    if (data.code === 'SUCCESS') {
                                        imgname = data.fileName;
                                        console.log("image data:" + JSON.stringify(data));
                                        //popupMessage("Upload Successful!!!");
                                        var _img = file.name;
                                        //alert("uploadlsist.url: " + data.url);
                                        uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
                                        uploadData.name = data.fileName;
                                        uploadData.desc = file.name;
                                        uploadData.serial = ++serial;
                                        uploadData.name = data.fileName;
                                        uploadData.url = data.url;
                                        self.uploadDataList.push(uploadData);
                                        self.uploadDataListResize.push(data.sfileName);
                                        value = data.fileName;
                                        ;
                                        self._image = data.fileName;
                                    }
                                    else {
                                        popupMessage("Upload Unsuccessful!!! Please Try Again...");
                                    }
                                }, function (error) { }, function () { });
                            }
                            //
                            callback(reader.result, {
                                alt: file.name
                            });
                        };
                        reader.readAsDataURL(file);
                    });
                }
                //win.document.getElementById(field_name).value = urllink1;
            },
            setup: function (editor) {
                _this.editor = editor;
                editor.on('init', function () {
                    editor.setContent(_this._obj.t2);
                });
                editor.on('keyup change', function () {
                    var content = editor.getContent();
                    _this._obj.t2 = content;
                    _this.onEditorKeyup.emit(content);
                });
            }
        });
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "new") {
                _this.content = 1;
                _this.goNew();
                _this.content = 0;
                _this.rd = true;
            }
            else if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this.goGet(id);
                _this._viewsk = id;
                _this.rd = false;
            }
        });
    };
    // ngOnInit() {
    //     var self =this;
    //     var urllink1 = this._urllink;
    //     var ics = this.ics;
    //     var http = this.http;
    //     var uploadData = this.uploadData;
    //     var uploadDataList = this.uploadDataList;
    //     var uploadDataListResize = this.uploadDataList;
    //     var popupMessage = this.popupMessage;
    //     var serial = this.serial;
    //     tinymce.init({
    //         selector: 'textarea',
    //         menubar: false,
    //         paste_data_images: true,
    //         toolbar: "bold italic | alignleft aligncenter alignright alignjustify | image",
    //         plugins: ['emoticons', 'link', 'modern', 'paste', 'table', 'image', 'imagetools'],
    //         file_picker_callback: function(callback, value, meta) {
    //             if (meta.filetype == 'image') {
    //               $('#upload').trigger('click');
    //               $('#upload').on('change', function() {
    //               var reader = new FileReader();
    //               var file = this.files[0];
    //               var imgname = "";
    //               var index = file.name.lastIndexOf(".");
    //               var imagename = file.name.substring(index);
    //               imagename = imagename.toLowerCase();
    //               if (imagename == ".jpg" || imagename == ".jpeg" || imagename == ".png") {
    //                 jQuery("#upload").val("");
    //                  let  url = ics._apiurl + 'file/fileupload?f=upload&fn=' + file.name + '&id=' + ics._profile.t1 + '&type=' + self._obj.n4 + '&imgUrl=' + ics._imgurl;
    //                   http.upload(url, file).subscribe(
    //                       data => {
    //                           if (data.code === 'SUCCESS') {
    //                               jQuery("#upload").val("");
    //                               imgname = data.fileName;
    //                               console.log("image data:" + JSON.stringify(data));
    //                               let _img = file.name;
    //                               uploadData = { "serial": 0, "name": "", "desc": "", "order": "" , "url" : ""};
    //                               uploadData.name = data.fileName;
    //                               uploadData.desc = file.name;
    //                               uploadData.serial = ++serial;
    //                               uploadData.name = data.fileName;
    //                               uploadData.url = data.url;
    //                               self.uploadDataList.push(uploadData);
    //                               self.uploadDataListResize.push(data.sfileName);
    //                               value =data.fileName;;
    //                               self._image = data.fileName;
    //                           } else {
    //                               popupMessage("Upload Unsuccessful!!! Please Try Again...");
    //                           }
    //                          //jQuery("#upload").val("");
    //                       },
    //                       error => { },
    //                       () => { }
    //                   );
    //                   reader.onload = function(e) {
    //                     callback(reader.result, {
    //                      alt:file.name
    //                    });
    //                 };
    //               }
    //                 reader.readAsDataURL(file);
    //             });
    //             //   $('#upload').on('change', function() {
    //             //     var file = this.files[0];
    //             //     var reader = new FileReader();
    //             //     var imgname = "";
    //             //     reader.onload = function(e) {
    //             //         callback(reader.result, {
    //             //          alt:file.name
    //             //        });
    //             //     };
    //             //     reader.readAsDataURL(file);
    //             //   });
    //             }
    //           },
    //         setup: editor => {
    //             this.editor = editor;
    //             editor.on('init', () => {
    //                 editor.setContent(this._obj.t2);
    //             });
    //             editor.on('keyup change', () => {
    //                 const content = editor.getContent();
    //                 this._obj.t2 = content;
    //                 this.onEditorKeyup.emit(content);
    //             })
    //         }
    //     });
    //     this.sub = this.route.params.subscribe(params => {
    //         let cmd = params['cmd'];
    //         if (cmd != null && cmd != "" && cmd == "new") {
    //             this.content = 1;
    //             this.goNew();
    //             this.content = 0;
    //             this.rd = true;
    //         } else if (cmd != null && cmd != "" && cmd == "read") {
    //             let id = params['id'];
    //             this.goGet(id);
    //             this._viewsk = id;
    //             this.rd = false;
    //         }
    //     });
    // }
    FrmContentMenu.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    FrmContentMenu.prototype.goGet = function (p) {
        var _this = this;
        this.flagview = false;
        this.flagnew = false;
        this.flagsave = false;
        this.flagdelete = false;
        this._obj.createdTime = this._time;
        this._obj.modifiedTime = this._time;
        this.http.doGet(this.ics._apiurl + 'serviceQuestion/readBySyskey?key=' + p).subscribe(function (data) {
            _this._obj = data;
            console.log("Data read=" + JSON.stringify(data.uploadlist));
            // this.alertenddates._date = this._util.setDatePickerDateNew(this._obj.t11);
            _this.editor.setContent(_this._obj.t2);
            if (_this.ics._profile.loginStatus == 1 && _this._obj.n7.toString() == "6") {
                _this.ref._lov3.postingstate = [];
                _this.ref._lov3.postingstate = _this.ref._lov3.readpoststatuecon;
            }
            if (_this.ics._profile.loginStatus == 2 && _this._obj.n7.toString() == "2") {
                _this.ref._lov3.postingstate = [];
                _this.ref._lov3.postingstate = _this.ref._lov3.readpoststatueeditor;
            }
            if (_this.ics._profile.loginStatus == 3 && _this._obj.n7.toString() == "4") {
                _this.ref._lov3.postingstate = [];
                _this.ref._lov3.postingstate = _this.ref._lov3.readpostpublisher;
            }
            if (_this.ics._profile.loginStatus == 4) {
                _this.ref._lov3.postingstate = [];
                _this.ref._lov3.postingstate = _this.ref._lov3.postingstateadmin;
            }
            if (_this.ics._profile.loginStatus == 5) {
                _this.ref._lov3.postingstate = [];
                _this.ref._lov3.postingstate = _this.ref._lov3.postingstateadmin;
            }
            if (_this._obj.t11 != '') {
                _this._noti.flag = true;
            }
            if (_this._obj.uploadlist.length > 0) {
                _this.uploadDataList = _this._obj.uploadlist;
                _this.serial = _this.uploadDataList[_this.uploadDataList.length - 1].serial;
                _this._obj.uploadlist = [];
            }
            if (_this._obj.resizelist.length > 0) {
                _this.uploadDataListResize = _this._obj.resizelist;
                _this._obj.resizelist = [];
            }
        }, function (error) { }, function () { });
    };
    FrmContentMenu.prototype.goNew = function () {
        //this.popupMessage("Please choose type before content");
        if (this.content == 0) {
            this.editor.setContent("");
        }
        jQuery("#imageUpload").val("");
        jQuery("#imageUpload").val("");
        jQuery("#save").prop("disabled", false);
        if (this.ics._profile.t1 == '204202') {
            this.ref._lov3.postingstate = this.ref._lov3.postingstate0;
        }
        if (this.ics._profile.t1 == '204681') {
            this.ref._lov3.postingstate = this.ref._lov3.postingstate1;
        }
        if (this.ics._profile.t1 == '206545') {
            this.ref._lov3.postingstate = this.ref._lov3.postingstate2;
        }
        if (this.ics._profile.t1 == '11384') {
            this.ref._lov3.postingstate = this.ref._lov3.postingstate;
        }
        this.uploadDataList = [];
        this.flagnew = false;
        this.flagsave = false;
        this.flagview = true;
        this.flagdelete = true;
        this._noti.flag = false;
        this.rd = true;
        this._fileName = "";
        var _img = "";
        this.uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
        this._obj = { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": "", "modifiedDate": "", "modifiedTime": "", "userId": "", "userName": "", "modifiedUserId": "", "modifiedUserName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [], "videoUpload": [], "comData": [], "cropdata": [], "agrodata": [], "ferdata": [], "towndata": [], "crop": null, "fert": null, "agro": null, "addTown": null, "uploadlist": [], "resizelist": [] };
        this._time = this._util.getTodayTime();
        this._obj.createdTime = this._time;
        this._obj.modifiedTime = this._time;
        this.serial = 0;
    };
    FrmContentMenu.prototype.goPost = function () {
        var _this = this;
        var imgUrl;
        if ((this._checkcw && this._obj.n7.toString() != "6") || (this._checkeditor && this._obj.n7.toString() != "2") || (this._checkpubisher && this._obj.n7.toString() != "4") || (this._checkadmin)) {
            this._obj.createdTime = this._time;
            this._obj.modifiedTime = this._time;
            this._obj.uploadlist = this.uploadDataList;
            this._obj.resizelist = this.uploadDataListResize;
            imgUrl = this.ics._imgurl;
            if (this.isValidate(this._obj)) {
                if (this._obj.syskey != 0) {
                    this._obj.modifiedUserId = this.ics._profile.userID;
                    this._obj.modifiedUserName = this.ics._profile.userName;
                }
                else {
                    this._obj.userId = this.ics._profile.userID;
                    this._obj.userName = this.ics._profile.userName;
                }
                var url = this.ics._apiurl + 'servicecontentmenu/saveContenMenu?imgurl=' + imgUrl;
                this._obj.n5 = Number(this.ics._profile.t1);
                var json = this._obj;
                this.http.doPost(url, json).subscribe(function (data) {
                    _this._result = data;
                    _this.showMessage(data.msgDesc, data.state);
                    _this._obj.syskey = data.longResult[0];
                    _this.rd = false;
                }, function (error) {
                    _this.showMessage("Can't Saved This Record!!!", undefined);
                }, function () { });
            }
        }
        else {
            this.showMessage("Can't Save This Status!!!", undefined);
        }
    };
    FrmContentMenu.prototype.goDelete = function () {
        var _this = this;
        if (this._obj.t1 != "") {
            var url = this.ics._apiurl + 'serviceQuestion/deleteQuestion';
            var json = this._obj;
            this.http.doPost(url, json).subscribe(function (data) {
                _this.showMessage(data.msgDesc, data.state);
                if (data.state) {
                    _this.goNew();
                }
            }, function (error) { }, function () { });
        }
        else {
            this.showMessage("No Article to Delete!", undefined);
        }
    };
    FrmContentMenu.prototype.goList = function () {
        this._router.navigate(['/contentmenulist']);
    };
    FrmContentMenu.prototype.uploadedFile = function (event, type) {
        var _this = this;
        this.flagcanvas = true;
        if (event.target.files.length == 1) {
            this._fileName = event.target.files[0].name;
            this._file = event.target.files[0];
            var index = this._fileName.lastIndexOf(".");
            var imagename = this._fileName.substring(index);
            imagename = imagename.toLowerCase();
            if (imagename == ".jpg" || imagename == ".jpeg" || imagename == ".png") {
                var url = this.ics._apiurl + 'file/fileupload?f=upload&fn=' + this._fileName + '&id=' + this.ics._profile.t1;
                this.http.upload(url, this._file).subscribe(function (data) {
                    if (data.code === 'SUCCESS') {
                        _this.popupMessage("Upload Successful!!!");
                        alert("ul: " + data.url);
                        var _img = _this._fileName;
                        _this.uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
                        _this.uploadData.name = data.fileName;
                        _this.uploadData.serial = ++_this.serial;
                        _this.uploadData.url = data.url;
                        _this.uploadDataList.push(_this.uploadData);
                        _this.uploadDataListResize.push(data.sfileName);
                        _this._fileName = "";
                    }
                    else {
                        _this.popupMessage("Upload Unsuccessful!!! Please Try Again...");
                    }
                }, function (error) { }, function () { });
            }
            else {
                this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": "Choose Image Associated!" });
            }
        }
        else {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "info", "t3": "Upload File!" });
        }
    };
    FrmContentMenu.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "primary", "t3": msg });
        }
    };
    FrmContentMenu.prototype.isValidate = function (obj) {
        if (obj.t1 == "" && obj.t2 == "") {
            this.showMessage("Please fill Title and Content!!!", false);
            return false;
        }
        else if (obj.t1 == "") {
            this.showMessage("Please fill Title!!!", false);
            return false;
        }
        else if (obj.t2 == "") {
            this.showMessage("Please fill Content!!!", false);
            return false;
        }
        else if (obj.n7 == "") {
            this.showMessage("Choose Status!!!", false);
            return false;
        }
        else if (this._obj.uploadlist.length > 0) {
            for (var i = 0; i < this._obj.uploadlist.length; i++) {
                if (this._obj.uploadlist[i].serial == '' || this._obj.uploadlist[i].serial == 0) {
                    this.showMessage("Invalid Serial No. !!!", false);
                    return false;
                }
            }
        }
        if (this.flagAcademic) {
            if (this._obj.n6 == 0) {
                this.showMessage("Select academic type !!!", false);
                return false;
            }
        }
        return true;
    };
    FrmContentMenu.prototype.setBtns = function () {
        var k = this.ics.getBtns("/contentmenulist");
        if (k != "" && k != undefined) {
            var strs = k.split(",");
            for (var i = 0; i < strs.length; i++) {
                if (strs[i] == "1") {
                    this.flagnew = false;
                }
                if (strs[i] == "2") {
                    this.flagsave = false;
                }
            }
        }
    };
    FrmContentMenu.prototype.setImgUrl = function (str) {
        return 'upload/image/' + str;
    };
    FrmContentMenu.prototype.popupMessage = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", t2: "News Information", t3: msg });
    };
    FrmContentMenu.prototype.changeArray = function (data, obj, num) {
        var arr = [];
        if (data instanceof Array) {
            arr = data;
            return arr;
        }
        else {
            if (num == 0) {
                arr[0] = obj;
                arr[1] = data;
                return arr;
            }
            if (num == 1) {
                arr[0] = data;
                return arr;
            }
        }
    };
    FrmContentMenu.prototype.goView = function (p) {
        this._router.navigate(['/eduView', 'read', p]);
    };
    FrmContentMenu.prototype.remove = function (index) {
        this.uploadDataList.splice(index, 1);
    };
    FrmContentMenu.prototype.valuechange = function (serial, ind) {
        for (var i = 0; i < this.uploadDataList.length; i++) {
            if (this.uploadDataList[i].serial == serial) {
                this.uploadDataList[ind].serial = "";
                this.flag = true;
                this.showMessage("Serial No. Already Exist!!!", false);
            }
        }
        if (!this.flag) {
            this.uploadDataList[ind].serial = serial;
        }
        this.flag = false;
    };
    /* getAcademicComboList() {
        this.http.doGet(this.ics._apiurl + 'servicecontentmenu/getAcademicRef').subscribe(
            response => {
                if ((response != null || response != undefined) && response.data.length > 0) {
                    this.ref._lov3.academicombo = this._util.changeArray(response.data, this._obj, 1);
                }
            },
            error => { },
            () => { }
        );
    } */
    FrmContentMenu.prototype.updateChecked = function (event) {
        if (event.target.checked) {
            this._obj.t11 = "noti";
        }
        else {
            this.showMessage("Please check that you have read and agree to the Terms and Conditions !", undefined);
        }
    };
    FrmContentMenu.prototype.deleteFile = function (obj) {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'file/fileRemove?fn=' + obj.name).subscribe(function (data) {
            console.log(data.code);
            if (data.code === 'SUCCESS') {
                var index = _this.uploadDataList.indexOf(obj);
                _this.uploadDataList.splice(index, 1);
                _this.showMessage('Photo Removed!', true);
            }
        }, function (error) { }, function () { });
    };
    FrmContentMenu.prototype.onDragStart = function (event, i, p, subarrname) {
        this.subarr = subarrname;
        this.pushcaption = p.name;
        this.splicearrindex1 = i;
        this.splicearrindex = -1;
    };
    FrmContentMenu.prototype.allowDrop = function (event) {
        event.preventDefault();
    };
    FrmContentMenu.prototype.goSubstitute = function (event, p) {
        this.substitueindex = p;
    };
    FrmContentMenu.prototype.onDrop = function (event, i, p, arrname) {
        if (this.subarr.length > 0) {
            var sub = this.subarr[this.splicearrindex1];
            var subconditional = [];
            this.subarr.splice(this.splicearrindex1, 1);
            arrname.splice(this.substitueindex, 0, sub);
        }
    };
    /* getStateList() {
        try {
            this.http.doGet(this.ics._apiurl + 'service001/getStateListNew').subscribe(
                response => {
                    if (response.refstate != null && response.refstate != undefined) {
                        this.ref._lov3.refstate = [{ "value": "", "caption": "ALL" }];
                        for (let i = 1; i < response.refstate.length; i++) {
                            this.ref._lov3.refstate[i] = response.refstate[i - 1];
                            //this.ref._lov3.refstate = response.refstate;
                        }
                       
                    }
                    else {
                        this.ref._lov3.refstate = [{ "value": "", "caption": "Empty" }];
                    }
                },
                error => {
                    if (error._body.type == 'error') {
                        alert("Connection Error!");
                    }
                }, () => { });
        } catch (e) {
            alert("Invalid URL");
        }
    } */
    FrmContentMenu.prototype.getStateList = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics._apiurl + 'service001/getStateListByUserID?type=all&userID=' + this.ics._profile.userID).subscribe(function (response) {
                if (response.refstate != null && response.refstate != undefined) {
                    //this.ref._lov3.refstate = [{ "value": "", "caption": "" }];
                    _this.ref._lov3.refstate = [];
                    if (!(response.refstate instanceof Array)) {
                        var m = [];
                        m[0] = response.data;
                        _this.ref._lov3.refstate = m;
                    }
                    else {
                        _this.ref._lov3.refstate = response.refstate;
                    }
                }
                else {
                    _this.ref._lov3.refstate = [{ "value": "", "caption": "Empty" }];
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Error!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], FrmContentMenu.prototype, "onEditorKeyup", void 0);
    FrmContentMenu = __decorate([
        core_1.Component({
            selector: 'frmcontentMenu',
            template: "\n    <div class=\"container\">\n    <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n        <form class= \"form-horizontal\" ngNoForm>\n            <div class=\"col-md-12\">\n                <!--this is currently form-->\n                <legend>Content </legend>\n            </div>\n    \n            <div class=\"col-md-12\">\n                <button class=\"btn btn-primary\"  type=\"button\"   (click)=\"goList()\">List</button> \n                <button class=\"btn btn-primary\" [disabled]=\"flagnew\" id=\"new\" type=\"button\" (click)=\"goNew()\">New</button>      \n                <button class=\"btn btn-primary\" [disabled]=\"flagsave\" id=\"save\" type=\"button\" (click)=\"goPost()\">Save</button> \n                <button class=\"btn btn-primary\" [disabled]=\"flagdelete\" id=\"delete\" type=\"button\" (click)=\"goDelete();\" >Delete</button> \n                <button *ngIf=\"!flagview\" class=\"btn btn-primary\"  type=\"button\"  (click)=\"goView(_viewsk);\" >View</button> \n            </div>\n    \n            <div class=\"col-md-12\">&nbsp;</div>\n    \n            <div class=\"col-md-12\">\n                <div class=\"form-group\">\n                    <label class=\"col-md-2\">Title</label>\n                    <div class=\"col-md-4\">\n                        <input  type=\"text\" [(ngModel)]=\"_obj.t1\" class=\"form-control input-md zawgyi\" style=\"height:50px\"/>\n                        <!--<textarea class=\"form-control\" [(ngModel)]=\"_obj.t1\" rows=\"2\" ></textarea>-->\n                    </div>\n                    <rp-input  [(rpModel)]=\"_obj.n7\" rpRequired =\"true\" rpLabelClass = \"col-md-2\"   rpType=\"postingstate\" rpLabel=\"Status\"  autofocus ></rp-input>\n                </div>\n    \n                <div class=\"form-group\">\n                    <label class=\"col-md-2\">Share Link</label>\n                    <div class=\"col-md-4\">\n                        <input type=\"text\" [(ngModel)]=\"_obj.t13\" class=\"form-control input-md\" style=\"height:50px\"/>\n                        <!-- <textarea class=\"form-control\" [(ngModel)]=\"_obj.t13\" rows=\"2\" ></textarea>-->\n                    </div>\n                    <!-- <rp-input [(rpModel)]=\"_obj.n4\" rpRequired =\"true\" (change)=\"changeAC(_obj.n4)\" rpType=\"contentmenuioniccombo\" rpLabel=\"Type\" autofocus ></rp-input>-->\n                    <rp-input [(rpModel)]=\"_obj.n4\" *ngIf=\"rd\" rpRequired =\"true\" rpType=\"contentmenuioniccombo\" rpLabelClass = \"col-md-2\" rpLabel=\"Type\"></rp-input> \n                    <rp-input [(rpModel)]=\"_obj.n4\" *ngIf=\"!rd\" rpRequired =\"true\" rpType=\"contentmenuioniccombo\" rpLabelClass = \"col-md-2\" rpLabel=\"Type\" rpReadonly=true></rp-input> \n                </div>\n    \n                <!--<div class=\"form-group\">\n                    <label class=\"col-md-2\"></label>\n                    <div class=\"col-md-4\"> \n                        <input type=\"checkbox\"  [(ngModel)]=\"_alert.flag\" (change)=\"updateCheckedAlert($event)\" name=\"check\"  ><label for=\"check\" style=\"padding: 15px; margin-bottom: 20px;border: 7px transparent; border-radius: 4px;\"> Alert</label>\n                    </div>\n                    <label  *ngIf =\"_obj.t4 == 'alert'\" class=\"col-md-2\">Alert End Date</label>\n                    <div  *ngIf =\"_obj.t4 == 'alert'\" class=\"col-md-4\"><datetime [(ngModel)]=\"alertenddates._date\" [datepicker]=\"_datepickerOpts\" [ngModelOptions]=\"{standalone: true}\"></datetime></div>\n                </div>-->\n    \n                <div class=\"form-group\">\n                    <rp-input [(rpModel)]=\"_obj.t5\" rpLabelClass = \"col-md-2\"  (change)=\"changeBrowe(_obj.t5)\" rpRequired =\"true\" rpType=\"statecombo\" rpLabel=\"Region\" ></rp-input>\n                </div>\n    \n                <div class=\"form-group\">\n                    <label class=\"col-md-2\">Content</label>\n                    <div class=\"col-md-10 zawgyi\">\n                            <textarea class=\"form-control input-md\" [(ngModel)]=\"_obj.t2\" rows=\"18\" ></textarea>\n                            <input name=\"image\" type=\"file\" id=\"upload\" class=\"hidden\" onchange=\"\">\n                    </div>\n                </div>\n    \n                <!--<div class=\"form-group\">\n                    <label class=\"col-md-2\">Image Associated (.jpeg, .png, .jpg)</label>\n                    <div class=\"col-md-4\">\n                        <input type=\"file\" id=\"imageUpload\" class=\"form-control input-md\" (change)=\"uploadedFile($event,_obj.n4)\" placeholder=\"Upload file...\" />\n                    </div>\n                </div>-->\n    \n                <!--<div class=\"form-group\">\n                    <div *ngFor=\"let img of _obj.videoUpload;let num = index\">\n                        <video id=\"{{img}}\" controls style=\"display:none\" height=\"240\" width=\"400\" type=\"video/mp4\">\n                            <source src=\"{{setImgUrl(img)}}\" type=\"video/mp4\" />\n                        </video>                     \n                        <canvas id=\"canvas\" style=\"display:none\" ></canvas> \n                    </div>\n                </div>-->\n    \n                <br>\n                <br>\n    \n                <!--<div class=\"col-md-3 col-sm-4 col-xs-6\" style=\"padding-right: 1px;\" *ngFor=\"let obj of uploadDataList, let i=index\"\n                    (dragstart)=\"onDragStart($event, i, obj, uploadDataList)\" draggable =\"true\" (dragover)=\"allowDrop($event)\"\n                    (dragover)=\"goSubstitute(event,i)\" (drop)=\"onDrop($event, i, obj, uploadDataList)\">\n                    <div class=\"upload-img\" id=\"img_container\">-->\n    \n                <!--<img src={{setImgUrl(obj.name)}} alt={{obj.name}} title={{obj.name}} />\n                    <button class=\"button_over_img\" type=\"button\" title=\"Remove Photo\" (click)=\"deleteFile(obj)\">\n                    <span class=\"glyphicon glyphicon-remove\"></span>\n                    </button>\n                    </div>\n                    </div>-->\n            </div>\n        </form>\n    </div>\n    </div>\n    </div>\n"
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmContentMenu);
    return FrmContentMenu;
}());
exports.FrmContentMenu = FrmContentMenu;
//# sourceMappingURL=frmcontentmenu.component.js.map