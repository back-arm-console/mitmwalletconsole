"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
core_2.enableProdMode();
var document = (function () {
    //flagImg = false;
    //addCrop = [];
    // cropobj = { id: "", name: "" };
    function document(el, renderer, ics, _router, route, http, ref) {
        this.el = el;
        this.renderer = renderer;
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._viewsk = 0;
        this._mflag = false;
        this.hide = true;
        this.flagview = true;
        this.flagnew = true;
        this.flagsave = true;
        this.flagdelete = true;
        this.lovftype = [];
        //tableflag=0;
        this.dates = { "_date": null };
        this.uploadDataList = [];
        //uploadDataListResize = [];
        this.uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
        this._img = "";
        this._file = null;
        this._fileName = '';
        this._Time = 0;
        this._util = new rp_client_util_1.ClientUtil();
        this._time = "";
        this.flagcanvas = true;
        this._imageObj = [{ "value": "", "caption": "", "flag": false }];
        this._obj = { "autokey": 0, "createdDate": "", "createdTime": null, "modifiedDate": "", "modifiedTime": null, "title": "", "status": "Draft", "fileSize": "", "fileName": "", "filePath": "", "fileType": "Law", "region": "", "userId": "", "userName": "", "recordStatus": 0, "t1": "", "t2": "00000000", "t3": "", "t4": "", "t5": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "postedby": "", "modifiedby": "", "uploadlist": [] };
        this._sessionObj = this.getSessionObj();
        this._roleval = "";
        this._checkcw = false;
        this._checkeditor = false;
        this._checkpubisher = false;
        this._checkmaster = false;
        this._checkadmin = false;
        this._StateType = "";
        this.stateobj = { id: "", name: "" };
        this.town_obj = { id: "", name: "" };
        this._result = { "longResult": "", "msgDesc": "", "key": [], "keyResult": 0, "state": "", "stringResult": "" };
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        this._roleval = ics._profile.t1;
        this._datepickerOpts = this.ics._datepickerOpts;
        this._datepickerOpts.format = 'dd/mm/yyyy';
        this.flagdelete = true;
        this.flagnew = false;
        this.flagsave = false;
        //this.flagURL = true;
        this.hide = true;
        this.getStateList();
        this.loadFileType();
    }
    document.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    document.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "new") {
                _this.goNew();
            }
            else if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this.goGet(id);
                _this._mflag = true;
                _this._viewsk = id;
            }
        });
    };
    document.prototype.goNew = function () {
        //this.flagvideo = false;
        //this.flagURL = true;
        //this.flagImage = true;
        //this.flagImage1 = false;
        //this.tableflag=0;
        jQuery("#pdfUpload").val("");
        jQuery("#save").prop("disabled", false);
        this.flagview = true;
        this.flagdelete = true;
        this.flagnew = false;
        this.flagsave = false;
        this._obj = { "autokey": 0, "createdDate": "", "createdTime": null, "modifiedDate": "", "modifiedTime": null, "title": "", "status": "Draft", "fileSize": "", "fileName": "", "filePath": "", "fileType": "Law", "region": "ALL", "userId": "", "userName": "", "recordStatus": 0, "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "postedby": "", "modifiedby": "", "uploadlist": [] };
        this._result = { "longResult": "", "msgDesc": "", "key": [], "keyResult": 0, "state": "", "stringResult": "" };
    };
    document.prototype.goDelete = function () {
        var _this = this;
        this._mflag = false;
        if (this._obj.filePath != "") {
            this._obj.modifiedby = this.ics._profile.userID;
            var url = this.ics.cmsurl + 'serviceCMS/deleteDoc';
            var json = this._obj;
            this.http.doPost(url, json).subscribe(function (data) {
                _this.showMsg(data.msgDesc, data.state);
                if (data.state) {
                    _this.goNew();
                    _this._mflag = true;
                }
            }, function (error) { }, function () { });
        }
        else {
            this._mflag = true;
            this.showMsg("No PDF to Delete!", undefined);
        }
    };
    document.prototype.loadFileType = function () {
        var _this = this;
        try {
            var url = this.ics.cmsurl + 'serviceAdmLOV/getFileType';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.msgCode == "0016") {
                        _this.showMsg(data.msgDesc, false);
                    }
                    if (data.msgCode == "0000") {
                        if (!(data.refFileType instanceof Array)) {
                            var m = [];
                            m[0] = data.refFileType;
                            _this.ref._lov3.refFileType = m;
                        }
                        else {
                            _this.ref._lov3.refFileType = data.refFileType;
                        }
                        _this.ref._lov3.refFileType.forEach(function (iItem) {
                            var l_Item = { "caption": "", "value": "" };
                            l_Item['caption'] = iItem.caption;
                            l_Item['value'] = iItem.value;
                            _this.lovftype.push(iItem);
                        });
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    document.prototype.getStateList = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics.cmsurl + 'serviceCMS/getStateListByUserID?type=1&userID=' + this.ics._profile.userID).subscribe(function (response) {
                if (response.refstate != null && response.refstate != undefined) {
                    _this.ref._lov3.refstate = [];
                    if (!(response.refstate instanceof Array)) {
                        var m = [];
                        m[0] = response.refstate;
                        _this.ref._lov3.refstate = m;
                    }
                    else {
                        _this.ref._lov3.refstate = response.refstate;
                    }
                }
                else {
                    _this.ref._lov3.refstate = [{ "value": "", "caption": "Empty" }];
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Error!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    document.prototype.goPost = function () {
        var _this = this;
        this._mflag = false;
        console.log("profile=" + JSON.stringify(this.ics._profile));
        console.log("obj=" + JSON.stringify(this._obj));
        if (this.isValidate(this._obj)) {
            if (this._obj.autokey != 0) {
                this._obj.modifiedby = this.ics._profile.userID;
            }
            else {
                this._obj.postedby = this.ics._profile.userID;
            }
            var url = this.ics.cmsurl + 'serviceCMS/savemyDoc';
            var json = this._obj;
            console.log("json=" + JSON.stringify(json));
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null && data != undefined) {
                    _this._result = data;
                    _this.showMsg(data.msgDesc, data.state);
                    _this._obj.autokey = data.longResult[0];
                    _this._mflag = true;
                }
                else {
                    _this._mflag = true;
                    _this.showMsg("Data Not Found", false);
                }
            }, function (error) {
                _this._mflag = true;
                console.log("data error=");
                _this.showMsg("Can't Saved This Record!", undefined);
            }, function () {
                console.log("data error");
            });
        }
        /* else {
          this.showMsg("Can't Save This Status!", false);
        } */
    };
    document.prototype.uploadedFile = function (event) {
        var _this = this;
        this._mflag = false;
        //this.uploadDataListResize = [];
        if (event.target.files.length == 1) {
            this._fileName = event.target.files[0].name;
            this._file = event.target.files[0];
            var index = this._fileName.lastIndexOf(".");
            var imagename = this._fileName.substring(index);
            imagename = imagename.toLowerCase();
            if (imagename == ".pdf" || imagename == ".doc" || imagename == ".docx" || imagename == ".pptx") {
                console.log("imgUrl: " + this.ics._imgurl);
                var url = this.ics.cmsurl + 'fileAdm/fileupload?f=upload&fn=' + this._fileName + '&id=' + this.ics._profile.t1 + '&type=10' + '&imgUrl=' + this.ics._imgurl;
                this.http.upload(url, this._file).subscribe(function (data) {
                    if (data.code === 'SUCCESS') {
                        //this.flagImg = true;
                        console.log("PDF files: " + JSON.stringify(data));
                        _this.showMsg("Upload Successful!", true);
                        var _img = _this._fileName;
                        _this.uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
                        _this.uploadData.name = data.fileName;
                        _this.uploadData.url = data.url;
                        _this._obj.filePath = _this.uploadData.url.substring(0, _this.uploadData.url.lastIndexOf("/") + 1);
                        _this._obj.fileName = _this.uploadData.url.substring(_this.uploadData.url.lastIndexOf("/") + 1);
                        _this._obj.uploadlist[0] = _this.uploadData;
                        //this.uploadDataListResize.push(data.sfileName);
                        _this._fileName = "";
                        _this._mflag = true;
                    }
                    else {
                        _this._mflag = true;
                        _this.showMsg("Upload Unsuccessful! Please Try Again...", false);
                    }
                }, function (error) { }, function () { });
            }
            else {
                this.showMsg("Choose File Associated!", false);
                this._mflag = true;
            }
        }
        else {
            this.showMsg("Upload Fail!", undefined);
            this._mflag = true;
        }
    };
    document.prototype.isValidate = function (obj) {
        if (obj.title == "") {
            this.showMsg("Please fill Content!", false);
            this._mflag = true;
            return false;
        }
        if (obj.n1 == "") {
            this.showMsg("Choose Status!", false);
            this._mflag = true;
            return false;
        }
        if (obj.fileType == "") {
            this.showMsg("Choose file Type!", false);
            this._mflag = true;
            return false;
        }
        if (obj.t2 == "") {
            this.showMsg("Choose Region!", false);
            this._mflag = true;
            return false;
        }
        return true;
    };
    document.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    document.prototype.goGet = function (p) {
        var _this = this;
        this._mflag = false;
        this.flagview = false;
        this.flagdelete = false;
        this.flagnew = false;
        this.flagsave = false;
        this._obj.createdTime = this._time;
        this._obj.modifiedTime = this._time;
        this.http.doGet(this.ics.cmsurl + 'serviceCMS/DocBysyskey?key=' + p).subscribe(function (data) {
            _this._obj = data;
            console.log("data obj=" + JSON.stringify(data));
            if (_this.ics._profile.loginStatus == 1) {
                _this.ref._lov3.postingstate = [];
                _this.ref._lov3.postingstate = _this.ref._lov3.readpoststatuecon;
            }
            if (_this.ics._profile.loginStatus == 2) {
                _this.ref._lov3.postingstate = [];
                _this.ref._lov3.postingstate = _this.ref._lov3.readpoststatueeditor;
            }
            if (_this.ics._profile.loginStatus == 3) {
                _this.ref._lov3.postingstate = [];
                _this.ref._lov3.postingstate = _this.ref._lov3.readpostpublisher;
            }
            if (_this.ics._profile.loginStatus == 4) {
                _this.ref._lov3.postingstate = [];
                _this.ref._lov3.postingstate = _this.ref._lov3.postingstateadmin;
            }
            if (_this.ics._profile.loginStatus == 5) {
                _this.ref._lov3.postingstate = [];
                _this.ref._lov3.postingstate = _this.ref._lov3.postingstateadmin;
            }
            _this._mflag = true;
            _this.hide = false;
        }, function (error) { }, function () { });
    };
    document.prototype.goList = function () {
        this._router.navigate(['/documentlist']);
    };
    document.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    document = __decorate([
        core_1.Component({
            selector: 'fmrdocument',
            template: "\n  <div class=\"container col-md-12 col-sm-12 col-xs-12\" style=\"padding: 0px 5%;\">\n\t<form class=\"form-horizontal\" ngNoForm>\n\t\t<legend>Document</legend>\n\t\t\t<div class=\"cardview list-height\">\n\t\t\t\t<div class=\"col-md-12\" style=\"margin-bottom: 10px;\">\t\t\t\t  \n\t\t\t\t  <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goList()\">List</button>\n\t\t\t\t  <button class=\"btn btn-sm btn-primary\" [disabled]=\"flagnew\" id=\"new\" type=\"button\" (click)=\"goNew()\">New</button>\n\t\t\t\t  <button class=\"btn btn-sm btn-primary\" [disabled]=\"flagsave\" id=\"save\" type=\"button\" (click)=\"goPost()\">Save</button>\n\t\t\t\t  <button class=\"btn btn-sm btn-primary\" [disabled]=\"flagdelete\" id=\"delete\" type=\"button\" (click)=\"goDelete();\">Delete</button>\n\t\t\t\t</div>\t\t\t\n\t\t\t\t\n\t\t\t\t<div class=\"col-md-10\">\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-md-3\">Title</label>\n\t\t\t\t\t\t\t<div class=\"col-md-4\">\n\t\t\t\t\t\t\t  <input type=\"text\" [(ngModel)]=\"_obj.title\" class=\"form-control input-sm uni\" />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class= \"col-md-3\">Status</label>\n\t\t\t\t\t\t\t  <div class= \"col-md-4\">                  \n\t\t\t\t\t\t\t\t<select [(ngModel)]=\"_obj.n1\"  class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\" required>\n\t\t\t\t\t\t\t\t  <option *ngFor=\"let c of ref._lov1.postingstate\" value=\"{{c.value}}\">{{c.caption}}</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t  </div>\t\t\t\t\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\t\t\t\t\n\t\t\t\t\t\t\t<label class=\"col-md-3\" for=\"filenameid\">File Path(.pdf)</label>\n\t\t\t\t\t\t\t<div class=\"col-md-4\" id=\"filenameid\">\n\t\t\t\t\t\t\t  <input type=\"file\" class=\"file\"(change)=\"uploadedFile($event)\" >\n\t\t\t\t\t\t\t\t<div class=\"input-group\">\n\t\t\t\t\t\t\t\t  <input type=\"text\" id=\"pdfUpload\"  class=\"form-control input-sm\" placeholder=\"No file chosen\">\n\t\t\t\t\t\t\t\t  <span class=\"input-group-btn\">\n\t\t\t\t\t\t\t\t\t<button class=\"browse btn btn-sm btn-primary\" type=\"button\"><i class=\"glyphicon glyphicon-search\"></i> Browse</button>\n\t\t\t\t\t\t\t\t  </span>\n\t\t\t\t\t\t\t\t</div>\t\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-md-3\">File Size</label>\n\t\t\t\t\t\t\t<div class=\"col-md-4\">\n\t\t\t\t\t\t\t\t<input type=\"text\" [(ngModel)]=\"_obj.fileSize\" class=\"form-control input-sm\" />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class= \"col-md-3\">File Type</label>\n\t\t\t\t\t\t\t  <div class= \"col-md-4\">                  \n\t\t\t\t\t\t\t\t<select [(ngModel)]=\"_obj.fileType\"  class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\" required>\n\t\t\t\t\t\t\t\t  <option *ngFor=\"let c of lovftype\" value=\"{{c.caption}}\">{{c.caption}}</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t  </div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class= \"col-md-3\">Region</label>\n\t\t\t\t\t\t\t  <div class= \"col-md-4\">                  \n\t\t\t\t\t\t\t\t<select [(ngModel)]=\"_obj.t2\" class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\" required>\n\t\t\t\t\t\t\t\t  <option *ngFor=\"let c of ref._lov3.refstate\" value=\"{{c.value}}\">{{c.caption}}</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t  </div>\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-md-3\">Keyword</label>\n\t\t\t\t\t\t\t<div class=\"col-md-4\">\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t<input type=\"text\" [(ngModel)]=\"_obj.t1\" class=\"form-control input-sm uni\" />\n\t\t\t\t\t\t\t</div>\t\t\t\n\t\t\t\t\t\t</div> \n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<table *ngIf=\"hide==false\" class=\"table table-striped table-condensed table-hover tblborder\" style=\"width: 525px;margin-left: 14px;\">\n\t\t\t\t\t\t\t\t<thead>\n\t\t\t\t\t\t\t\t  <tr>\n\t\t\t\t\t\t\t\t\t<th>No.</th>\n\t\t\t\t\t\t\t\t\t<th>File Name</th>\n\t\t\t\t\t\t\t\t  </tr>\n\t\t\t\t\t\t\t\t</thead>\n\t\t\t\t\t\t\t\t<tbody>\n\t\t\t\t\t\t\t\t  <tr *ngIf=\"_obj.fileName!=''\">\n\t\t\t\t\t\t\t\t\t<td>1</td>\n\t\t\t\t\t\t\t\t\t<td><a target=\"_blank\" rel=\"noopener noreferrer\" href=\"{{_obj.filePath}}{{_obj.fileName}}\">{{_obj.fileName}}</a></td>\n\t\t\t\t\t\t\t\t  </tr>\n\t\t\t\t\t\t\t\t</tbody>\n\t\t\t\t\t\t\t</table>\n\t\t\t\t\t\t</div>\n\t\t\t\t</div>\t\t\t\t\t\t\n\t\t\t</div>\n\t</form>\n</div>\n<div [hidden]=\"_mflag\">\n<div  id=\"loader\" class=\"modal\" ></div>\n</div>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n  "
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef, core_1.Renderer, rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], document);
    return document;
}());
exports.document = document;
//# sourceMappingURL=document.component.js.map