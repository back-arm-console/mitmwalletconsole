"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var ng2_qrcode_1 = require('ng2-qrcode');
var Rx_1 = require('rxjs/Rx');
core_1.enableProdMode();
var FrmChannelList = (function () {
    function FrmChannelList(ics, ref, _router, http) {
        this.ics = ics;
        this.ref = ref;
        this._router = _router;
        this.http = http;
        this._StateType = "";
        this._mflag = false;
        this.nore = 0;
        this.sessionAlertMsg = "";
        this.channelList = { "data": [{ "channelSyskey": 0, "autokey": 0, "channelName": "", "region": "" }], "msgcode": "", "msgdesc": "" };
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.ticketStatus = "";
        this.regionCaption = "";
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = true;
            //this.regionCaption = 'ALL';
            this.getStateList();
        }
    }
    FrmChannelList.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    FrmChannelList.prototype.getStateList = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics._apiurl + 'service001/getStateListByUserID?type=&userID=' + this.ics._profile.userID).subscribe(function (response) {
                if (response.refstate != null && response.refstate != undefined) {
                    //this.ref._lov3.refstate = [{ "value": "", "caption": "" }];
                    _this.ref._lov3.refstate = [];
                    if (!(response.refstate instanceof Array)) {
                        var m = [];
                        m[0] = response.data;
                        _this.ref._lov3.refstate = m;
                    }
                    else {
                        _this.ref._lov3.refstate = response.refstate;
                    }
                    _this._StateType = _this.ref._lov3.refstate[0].value;
                    _this.getChannelList();
                }
                else {
                    _this.ref._lov3.refstate = [{ "value": "", "caption": "Empty" }];
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Error!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmChannelList.prototype.ChangeState = function (options) {
        this._StateType = options[options.selectedIndex].value;
        this.getChannelList();
    };
    FrmChannelList.prototype.getChannelList = function () {
        var _this = this;
        this._mflag = false;
        var json = {
            sessionID: this.ics._profile.sessionID,
            userID: this.ics._profile.userID
        };
        var url = this.ics._apiurl + 'service001/getChannelList?region=' + this._StateType;
        try {
            this.http.doPost(url, json).subscribe(function (data) {
                _this.nore = 1;
                if (data.msgCode == "0016") {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMessage();
                }
                else {
                    if (data.channeldata != null) {
                        if (!(data.channeldata instanceof Array)) {
                            var m = [];
                            m[0] = data.channeldata;
                            _this.channelList.data = m;
                        }
                        else {
                            _this.channelList.data = data.channeldata;
                        }
                    }
                    else {
                        _this.channelList.data = [];
                    }
                    _this._mflag = true;
                }
            }, function (error) {
                console.log("signin error=" + error.status);
                //this.nores = 0;            
            });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmChannelList.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this.goLogOut();
        });
    };
    FrmChannelList.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    /* changeState(region) {
        console.log("REgion=" + region);

        if (region == '13000000') {
            this.regionCaption = 'YANGON';
        }

        if (region == '10000000') {
            this.regionCaption = 'MANDALAY';
        }

        if (region == '00000000' || region == '') {
            this.regionCaption = 'ALL';
        }
    }  */
    FrmChannelList.prototype.goto = function (p) {
        console.log("p=" + JSON.stringify(p));
        this._router.navigate(['/chatlist', 'read', p]);
    };
    FrmChannelList.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmChannelList.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    FrmChannelList = __decorate([
        core_1.Component({
            selector: 'fmr-channelList',
            entryComponents: [ng2_qrcode_1.QRCodeComponent],
            template: "\n    <div class=\"container\">\n\t<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n\t\t<form class=\"form-horizontal\"> \n\t\t\t<legend>Channels</legend>\n\t\t\t<!-- Form Name -->\n\t\t\t<div class=\"form-group\">\t\t\t\n\t\t\t\t<label class= \"col-md-1 control-label\">Region</label>\n\t\t\t\t\t<div class= \"col-md-2\">                  \n\t\t\t\t\t\t<select [(ngModel)]=\"_StateType\" class=\"form-control\" (change)=\"ChangeState($event.target.options)\" [ngModelOptions]=\"{standalone: true}\" required>\n\t\t\t\t\t\t\t<option *ngFor=\"let c of ref._lov3.refstate\" value=\"{{c.value}}\">{{c.caption}}</option>\n\t\t\t\t\t\t</select>\n\t\t\t\t\t</div>\t\n\t\t\t</div>\n\t\t\t<div class=\"col-md-12\">&nbsp;</div>\t\t\n\t\t\t\t\t<table class=\"table table-striped table-condensed table-hover tblborder\" id=\"tblstyle\" *ngIf=\"nore==1\">\n\t\t\t\t\t\t<thead>\n\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t<th>Channel Key</th>\n\t\t\t\t\t\t\t\t<th>Channel Name</th>\n\t\t\t\t\t\t\t\t<th>Region</th>\n\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t</thead>\n\t\t\t\t\t\t<tbody>\n\t\t\t\t\t\t\t<ng-container *ngFor=\"let obj of channelList.data\">\n\t\t\t\t\t\t\t\t<tr><!--*ngIf=\"obj.region == regionCaption\"-->\n\t\t\t\t\t\t\t\t\t<td><a (click)=\"goto(obj.channelSyskey)\">{{obj.channelSyskey}}</a></td>\n\t\t\t\t\t\t\t\t\t<td class=\"uni\">{{obj.channelName}}</td>\n\t\t\t\t\t\t\t\t\t<td class=\"uni\">{{obj.region}}</td>\n\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t<!--<tr *ngIf=\"'ALL' == regionCaption\">\n\t\t\t\t\t\t\t\t\t<td><a (click)=\"goto(obj.channelSyskey)\">{{obj.channelSyskey}}</a></td>\n\t\t\t\t\t\t\t\t\t<td class=\"uni\">{{obj.channelName}}</td>\n\t\t\t\t\t\t\t\t\t<td class=\"uni\">{{obj.region}}</td>\n\t\t\t\t\t\t\t\t</tr>-->\n\t\t\t\t\t\t\t</ng-container>\n\t\t\t\t\t\t</tbody>\n\t\t\t\t\t</table>\n\t\t</form>\n\t</div>\n</div>\n\n<div [hidden]=\"_mflag\">\n  <div class=\"modal\" id=\"loader\"></div>\n</div>\n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, rp_references_1.RpReferences, router_1.Router, rp_http_service_1.RpHttpService])
    ], FrmChannelList);
    return FrmChannelList;
}());
exports.FrmChannelList = FrmChannelList;
//# sourceMappingURL=frmchannel-list.component.js.map