"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var common_1 = require('@angular/common');
var rp_input_module_1 = require('../framework/rp-input.module');
var mydatepicker_1 = require('mydatepicker');
var ngx_tabs_1 = require("ngx-tabs");
var ng2_qrcode_1 = require('ng2-qrcode');
var rp_http_service_1 = require('../framework/rp-http.service');
var adminpager_module_1 = require('../util/adminpager.module');
var pager_module_1 = require('../util/pager.module');
var video_component_1 = require('./video.component');
var video_list_component_1 = require('./video-list.component');
var administration_routing_1 = require('./administration.routing');
var contentmenu_component_1 = require('./contentmenu.component');
var contentmenu_list_component_1 = require('./contentmenu-list.component');
var edu_view_component_1 = require('./edu-view.component');
var video_view_component_1 = require('./video-view.component');
var advancedsearch_module_1 = require('../util/advancedsearch.module');
var multiselect_module_1 = require('../util/multiselect.module');
var smssettingsetup_component_1 = require('./smssettingsetup.component');
var SMSSettingSetupList_component_1 = require('./SMSSettingSetupList.component');
var merchantcommmappingsetup_component_1 = require('./merchantcommmappingsetup.component');
var merchantcommmappingsetup_list_component_1 = require('./merchantcommmappingsetup-list.component');
var flexcommratesetup_component_1 = require('./flexcommratesetup.component');
var flexcommratesetup_list_component_1 = require('./flexcommratesetup-list.component');
var merchant_setup_component_1 = require('./merchant-setup.component');
var merchant_list_component_1 = require('./merchant-list.component');
var administrationModule = (function () {
    function administrationModule() {
    }
    administrationModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                forms_1.FormsModule,
                rp_input_module_1.RpInputModule,
                administration_routing_1.AdministratorRouting,
                adminpager_module_1.AdminPagerModule,
                pager_module_1.PagerModule,
                mydatepicker_1.MyDatePickerModule,
                ngx_tabs_1.TabsModule,
                multiselect_module_1.MultiselectModule,
                advancedsearch_module_1.AdvancedSearchModule
            ],
            exports: [],
            declarations: [
                ng2_qrcode_1.QRCodeComponent,
                video_component_1.VideoComponent,
                video_list_component_1.VideoList,
                contentmenu_component_1.ContentMenu,
                contentmenu_list_component_1.ContentMenuListComponent,
                edu_view_component_1.EduViewComponent,
                video_view_component_1.videoViewComponent,
                smssettingsetup_component_1.SMSSettingComponent,
                SMSSettingSetupList_component_1.SMSSettingSetupListComponent,
                merchantcommmappingsetup_component_1.MerchantCommMappingSetup,
                merchantcommmappingsetup_list_component_1.MerchantCommMappingSetupList,
                flexcommratesetup_component_1.FlexCommRateSetup,
                flexcommratesetup_list_component_1.FlexCommRateSetupList,
                merchant_setup_component_1.MerchantSetup,
                merchant_list_component_1.MerchantList,
            ],
            providers: [
                rp_http_service_1.RpHttpService
            ],
            schemas: [core_1.CUSTOM_ELEMENTS_SCHEMA]
        }), 
        __metadata('design:paramtypes', [])
    ], administrationModule);
    return administrationModule;
}());
exports.administrationModule = administrationModule;
//# sourceMappingURL=administration.module.js.map