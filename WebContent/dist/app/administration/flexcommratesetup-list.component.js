"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_http_service_1 = require('../framework/rp-http.service');
// RP Framework
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_client_util_1 = require('../util/rp-client.util');
core_1.enableProdMode();
var FlexCommRateSetupList = (function () {
    function FlexCommRateSetupList(ics, _router, http) {
        this.ics = ics;
        this._router = _router;
        this.http = http;
        // Application Specific
        this._col_list = [{ "col_no": "1", "flag": false }, { "col_no": "2", "flag": false }];
        this._sorting = { _sort_type: "asc", _sort_col: "1" };
        this._mflag = true;
        this._util = new rp_client_util_1.ClientUtil();
        this.sessionAlertMsg = "";
        this._flexobj = {
            "commdetailArr": [{ "syskey": 0, "hkey": "", "zone": "", "commType": "", "fromAmt": "", "toAmt": "", "amount": "", "minAmt": "", "maxAmt": "", "gLorAC": "" }],
            "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0 };
        this._pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 0 };
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            //jQuery("#loader").modal('hide');
            this._mflag = false;
            this.search();
        }
    }
    // list sorting part
    FlexCommRateSetupList.prototype.changeDefault = function () {
        for (var i = 0; i < this._col_list.length; i++) {
            this._col_list[i].flag = false;
        }
    };
    FlexCommRateSetupList.prototype.addSort = function (e) {
        for (var i = 0; i < this._col_list.length; i++) {
            if (this._col_list[i].col_no == e) {
                var _tmp_flag = this._col_list[i].flag;
                this.changeDefault();
                this._col_list[i].flag = !_tmp_flag;
                this._sorting._sort_type = !_tmp_flag ? "desc" : "asc";
                this._sorting._sort_col = e;
                break;
            }
        }
    };
    FlexCommRateSetupList.prototype.changedPager = function (event) {
        if (this._flexobj.totalCount != 0) {
            this._pgobj = event;
            var current = this._flexobj.currentPage;
            var size = this._flexobj.pageSize;
            this._flexobj.currentPage = this._pgobj.current;
            this._flexobj.pageSize = this._pgobj.size;
            if (this._pgobj.current != current || this._pgobj.size != size) {
                this.search();
            }
        }
    };
    FlexCommRateSetupList.prototype.search = function () {
        var _this = this;
        try {
            this._mflag = false;
            this.http.doGet(this.ics.cmsurl + 'serviceCMS/getFlexCommRateList?searchVal=' + this._flexobj.searchText + '&pagesize=' + this._flexobj.pageSize + '&currentpage=' + this._flexobj.currentPage + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (response) {
                if (response.msgCode == "0016") {
                    _this.sessionAlertMsg = response.msgDesc;
                    _this.showMessage(response.msgDesc, false);
                }
                else {
                    if (response.totalCount == 0) {
                        _this._flexobj = response;
                        _this.showMessage("Data not found!", false);
                    }
                    if (response.commdetailArr != null) {
                        if (!(response.commdetailArr instanceof Array)) {
                            var m = [];
                            m[0] = response.commdetailArr;
                            _this._flexobj.commdetailArr = m;
                            _this._flexobj.totalCount = response.totalCount;
                        }
                        else {
                            _this._flexobj = response;
                        }
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    FlexCommRateSetupList.prototype.searchKeyup = function (e) {
        if (e.which == 13) {
            this._flexobj.currentPage = 1;
            this._msghide = true;
            this._recordhide = false;
            this.search();
        }
    };
    FlexCommRateSetupList.prototype.Searching = function () {
        this._flexobj.currentPage = 1;
        this._msghide = true;
        this._recordhide = false;
        this.search();
    };
    FlexCommRateSetupList.prototype.goto = function (p) {
        this._router.navigate(['/Flex Comm Setup', 'read', p]);
    };
    //showMessage() {
    //    jQuery("#sessionalert").modal();
    //   Observable.timer(3000).subscribe(x => {
    //        this.goLogOut();
    //    });
    //}
    FlexCommRateSetupList.prototype.goNew = function () {
        this._router.navigate(['/Flex Comm Setup', 'new']);
    };
    FlexCommRateSetupList.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    FlexCommRateSetupList.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "primary", "t3": msg });
        }
    };
    FlexCommRateSetupList = __decorate([
        core_1.Component({
            selector: 'flexcommratesetup-list',
            template: "\n    <div class=\"container-fluid\">\n    <form class=\"form-horizontal\"> \n    <!-- Form Name -->\n    <legend>Commission List</legend>\n\t\t<div class=\"cardview list-height\">\n\t\t\t<div class=\"row col-md-12\">\n\t\t\t\t<div class=\"row col-md-3\">\n\t\t\t\t\t<div class=\"input-group\">\n\t\t\t\t\t\t<span class=\"input-group-btn\">\n\t\t\t\t\t\t\t<button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goNew();\">New</button>\n\t\t\t\t\t\t</span>\n\t\t\t\t\t\t<input id=\"textinput\" name=\"textinput\" type=\"text\" placeholder=\"Search\" [(ngModel)]=\"_flexobj.searchText\" maxlength=\"50\" class=\"form-control input-sm\" (keyup)=\"searchKeyup($event)\" [ngModelOptions]=\"{standalone: true}\">\n\t\t\t\t\t\t<span class=\"input-group-btn\">\n\t\t\t\t\t\t\t<button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"Searching()\" >\n\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-search\"></span>Search\n\t\t\t\t\t\t</button>\n\t\t\t\t\t\t</span> \n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"pagerright\">\t\t\t\t\n\t\t\t\t\t<adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"_flexobj.totalCount\" (rpChanged)=\"changedPager($event)\" ></adminpager> \n\t\t\t\t</div>\n            </div>\n            <div class=\"row col-md-12\" style=\"overflow-x:auto\">\n\t\t\t  <table class=\"table table-striped table-condensed table-hover tblborder\">\n\t\t\t\t<thead>\n\t\t\t\t <tr>\n\t\t\t\t\t<th class=\"center\">Commission Reference No.</th>\n\t\t\t\t\t<th>Description</th> \n\t\t\t\t</tr>\n\t\t\t  </thead>\n\t\t\t  <tbody>\n\t\t\t\t<tr *ngFor=\"let obj of _flexobj.commdetailArr\">\n\t\t\t\t\t<td class=\"col-md-3 center\"><a (click)=\"goto(obj.commRef)\">{{obj.commRef}}</a></td>\n\t\t\t\t\t<td>{{obj.description}}</td>\n\t\t\t\t</tr>\n\t\t\t  </tbody>\n              </table>\n              </div>\n\t\t</div>\n\t</form>\n  </div> \n  \n    <div [hidden] = \"_mflag\">\n        <div class=\"modal\" id=\"loader\"></div>\n    </div>\n   "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_http_service_1.RpHttpService])
    ], FlexCommRateSetupList);
    return FlexCommRateSetupList;
}());
exports.FlexCommRateSetupList = FlexCommRateSetupList;
//# sourceMappingURL=flexcommratesetup-list.component.js.map