"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
core_2.enableProdMode();
var MerchantCommMappingSetup = (function () {
    function MerchantCommMappingSetup(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
        this._obj = this.getDefaultObj();
        this._mflag = false;
        this._chkloginId = 'false';
        this._key = "";
        this.sessionAlertMsg = "";
        this._util = new rp_client_util_1.ClientUtil();
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = false;
            this.checkSession();
            this.getAllMerchant();
            this.getAllCommRef();
            this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
            this._obj = this.getDefaultObj();
        }
    }
    MerchantCommMappingSetup.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this._key = id;
                _this.goReadBySyskey(id);
            }
        });
    };
    MerchantCommMappingSetup.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    MerchantCommMappingSetup.prototype.getDefaultObj = function () {
        jQuery("#mydelete").prop("disabled", true);
        jQuery("#mySave").prop("disabled", false);
        this._combohide = true;
        this._mercbohide = false;
        return { "processingCode": "", "syskey": 0, "recordStatus": 0, "merchantID": "", "kindOfComIssuer": 0, "chkAdvance": false, "t1": "", "commRef1": "-", "commRef2": "-", "commRef3": "-", "commRef4": "-", "commRef5": "-", "code": "", "desc": "", "userID": "", "sessionID": "", "n1": 0, "n2": 0 };
    };
    MerchantCommMappingSetup.prototype.goReadBySyskey = function (p) {
        var _this = this;
        try {
            this._mercbohide = true;
            var url = this.ics.cmsurl + 'serviceCMS/getMerchantCommData?syskey=' + p + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
            this.http.doGet(url).subscribe(function (data) {
                if (data.code == '0016') {
                    _this.sessionAlertMsg = data.desc;
                    _this.showMessage(data.desc, false);
                }
                else {
                    jQuery("#mydelete").prop("disabled", false);
                    _this._obj = data;
                    if (_this._obj.t1 != '') {
                        _this._combohide = false;
                        _this._obj.chkAdvance = true;
                        _this.getCodeFromLovdeatils();
                    }
                    else {
                        _this._combohide = true;
                        _this._obj.chkAdvance = false;
                    }
                    if (_this._obj.n2 != 0) {
                        _this.chkWaterBill = true;
                    }
                    else {
                        _this.chkWaterBill = false;
                    }
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    MerchantCommMappingSetup.prototype.goList = function () {
        this._router.navigate(['/merchantcommmappinglist']);
    };
    //showMessage() {
    //  jQuery("#sessionalert").modal();
    //  Observable.timer(3000).subscribe(x => {
    //     this.goLogOut();
    //   });
    // }
    MerchantCommMappingSetup.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    MerchantCommMappingSetup.prototype.showMessageAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    MerchantCommMappingSetup.prototype.clearData = function () {
        this._obj = this.getDefaultObj();
        this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
        this._chkloginId = 'false';
        jQuery("#mydelete").prop("disabled", true);
        jQuery("#mySave").prop("disabled", true);
    };
    MerchantCommMappingSetup.prototype.goNew = function () {
        this.clearData();
        jQuery("#mySave").prop("disabled", false);
        jQuery("#mydelete").prop("disabled", true);
    };
    MerchantCommMappingSetup.prototype.getAllMerchant = function () {
        var _this = this;
        this.http.doGet(this.ics.cmsurl + 'serviceAdmLOV/getmerchantidlistdetail?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (data) {
            _this.ref._lov3.ref015 = data.ref015;
            var merchant = [];
            if (_this.ref._lov3.ref015 != null && _this.ref._lov3.ref015 != undefined) {
                if (!(_this.ref._lov3.ref015 instanceof Array)) {
                    var m = [];
                    m[1] = _this.ref._lov3.ref015;
                    merchant.push(m[1]);
                }
                for (var j = 0; j < _this.ref._lov3.ref015.length; j++) {
                    merchant.push(_this.ref._lov3.ref015[j]);
                }
            }
            _this._obj.merchantID = _this.ref._lov3.ref015[0].value;
            _this.ref._lov3.ref015 = merchant;
            _this._obj.processingCode = _this.ref._lov3.ref015[0].processingCode;
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
        }, function () { });
    };
    MerchantCommMappingSetup.prototype.getAllCommRef = function () {
        var _this = this;
        try {
            this._mflag = false;
            this.http.doGet(this.ics.cmsurl + 'serviceAdmLOV/getAllCommRef?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (data) {
                if (data != null && data != undefined) {
                    var Chargescombo = [{ "value": "-", "caption": "-" }];
                    for (var i = 0; i < data.refcharges.length; i++) {
                        Chargescombo.push({ "value": data.refcharges[i].value, "caption": data.refcharges[i].caption });
                    }
                    _this.ref._lov3.refcharges = Chargescombo;
                }
                else {
                    _this.ref._lov3.refcharges = [{ "value": "", "caption": "-" }];
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    MerchantCommMappingSetup.prototype.update = function (event) {
        if (event.target.checked) {
            this._combohide = false;
            this.getCodeFromLovdeatils();
            this._obj.n1 = 1;
            this._obj.chkAdvance = true;
        }
        else {
            this._combohide = true;
            this._obj.n1 = 0;
            this._obj.chkAdvance = false;
            this._obj.t1 = '';
            this.ref._lov3.reflovdetails = [{ "value": "", "caption": "-" }];
        }
    };
    MerchantCommMappingSetup.prototype.getCodeFromLovdeatils = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics.cmsurl + 'serviceAdmLOV/getCodeFromLovdeatils?merchantId=' + this._obj.merchantID).subscribe(function (data) {
                if (data != null && data != undefined) {
                    if (!(data.reflovdetails instanceof Array)) {
                        var m = [];
                        m[0] = data.reflovdetails;
                        _this.ref._lov3.reflovdetails = m;
                    }
                    else {
                        _this.ref._lov3.reflovdetails = data.reflovdetails;
                    }
                }
                else {
                    _this.ref._lov3.reflovdetails = [{ "value": "", "caption": "-" }];
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    MerchantCommMappingSetup.prototype.goSave = function () {
        if (this._obj.merchantID == '') {
            this.showMessage("Please select Merchant", false);
        }
        else if (this._obj.commRef1 == '' && this._obj.commRef2 == '' && this._obj.commRef3 == '' && this._obj.commRef4 == '' && this._obj.commRef5 == '') {
            this.showMessage("Please select Charges", false);
        }
        else if (this._obj.chkAdvance == true) {
            if (this._obj.t1 == '') {
                this.showMessage("Please select Operator Type", false);
            }
            else {
                this.saveMerchantCommMapping();
            }
        }
        else {
            this.saveMerchantCommMapping();
        }
    };
    MerchantCommMappingSetup.prototype.saveMerchantCommMapping = function () {
        var _this = this;
        try {
            this._mflag = false;
            this._obj.sessionID = this.ics._profile.sessionID;
            this._obj.userID = this.ics._profile.userID;
            var url = this.ics.cmsurl + 'serviceCMS/saveMerchantCommRateMapping';
            var json = this._obj;
            this.http.doPost(url, json).subscribe(function (data) {
                _this._returnResult = data;
                if (_this._returnResult.state == 'true') {
                    jQuery("#mydelete").prop("disabled", false);
                    _this.showMessage(data.msgDesc, true);
                }
                else {
                    if (_this._returnResult.msgCode == '0016') {
                        _this.sessionAlertMsg = data.msgDesc;
                        _this.showMessage(data.msgDesc, false);
                    }
                    else {
                        _this.showMessage(data.msgDesc, true);
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    MerchantCommMappingSetup.prototype.goDelete = function () {
        var _this = this;
        try {
            this._mflag = false;
            this._obj.sessionID = this.ics._profile.sessionID;
            this._obj.userID = this.ics._profile.userID;
            var json = this._obj;
            var url = this.ics.cmsurl + 'serviceCMS/deleteMerchantCommRateMapping';
            this.http.doPost(url, json).subscribe(function (data) {
                _this._returnResult = data;
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMessage(data.msgDesc, false);
                }
                else {
                    _this.showMessageAlert(data.msgDesc);
                    if (data.state) {
                        _this.clearData();
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    MerchantCommMappingSetup.prototype.changeMerchant = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex; //Get Selected Index
        var value = options[options.selectedIndex].value; //Get Selected Index's Value    
        this._obj.merchantID = value;
        this.getCodeFromLovdeatils();
        for (var i = 0; i < this.ref._lov3.ref015.length; i++) {
            if (this.ref._lov3.ref015[i].value == value) {
                this._obj.merchantID = this.ref._lov3.ref015[i].value;
                this._obj.processingCode = this.ref._lov3.ref015[i].processingCode;
                if (this._obj.processingCode == "090500") {
                    this.chkWaterBill = true;
                }
                else {
                    this.chkWaterBill = false;
                }
                break;
            }
        }
    };
    MerchantCommMappingSetup.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    MerchantCommMappingSetup.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsgAlert(data.desc);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsgAlert(data.desc);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    MerchantCommMappingSetup.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    MerchantCommMappingSetup.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    MerchantCommMappingSetup.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "primary", "t3": msg });
        }
    };
    MerchantCommMappingSetup = __decorate([
        core_1.Component({
            selector: 'merchantcommapping-setup',
            template: "\n  <div class=\"container-fluid\">\n  <div class=\"row clearfix\">\n  <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n  <form class= \"form-horizontal\" (ngSubmit) = \"goSave()\"> \n  <!-- Form Name -->\n  <legend>Merchant Commission </legend>\n  <div class=\"cardview list-height\">\n    <div class=\"row col-md-12\"> \n      <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goList()\" >List</button> \n      <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goNew()\" >New</button>      \n      <button class=\"btn btn-sm btn-primary\" id=\"mySave\" type=\"submit\" >Save</button>          \n      <button class=\"btn btn-sm btn-primary\" disabled id=\"mydelete\"  type=\"button\" (click)=\"goDelete();\" >Delete</button> \n    </div>\n        \n    <div class=\"row col-md-12\">&nbsp;</div>\n    <div class=\"form-group\">\n    <div class=\"col-md-8\">\n    <div class=\"form-group\">\n      <label class=\"col-md-2\">Merchant ID</label>\n      <div class=\"col-md-4\" > \n        <select *ngIf =\"_mercbohide==true\" disabled [(ngModel)]=\"_obj.merchantID\" (change)=\"changeMerchant($event)\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control input-sm\" required>\n        <option *ngFor=\"let item of ref._lov3.ref015\" value=\"{{item.value}}\" >{{item.caption}}</option>\n        </select> \n        \n        <select *ngIf =\"_mercbohide!=true\" [(ngModel)]=\"_obj.merchantID\" (change)=\"changeMerchant($event)\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control input-sm\" required>\n        <option *ngFor=\"let item of ref._lov3.ref015\" value=\"{{item.value}}\" >{{item.caption}}</option>\n        </select> \n      </div>   \n      <div class=\"col-md-1\"> \n      {{_obj.merchantID}} \n      </div>\n    </div>\n\n    <div class=\"form-group\"> \n      <label class=\"col-md-2\" ></label> \n      <div class=\"col-md-4\" >  \n      <label class=\"radio-inline\">\n        <input #m [checked]=\"_obj.kindOfComIssuer == m.value\" (click)=\"_obj.kindOfComIssuer = m.value\" name=\"kindOfComIssuer\" value=\"0\" type=\"radio\"> Merchant\n      </label>\n      <label class=\"radio-inline\">\n        <input  #c [checked]=\"_obj.kindOfComIssuer == c.value\" (click)=\"_obj.kindOfComIssuer = c.value\" name=\"kindOfComIssuer\" value=\"1\" type=\"radio\"> Customer\n      </label>\n      <label class=\"checkbox-inline\" >\n      <input  type=\"checkbox\" [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"_obj.chkAdvance\" (click)=\"update($event)\"> Advanced\n      </label>\n      </div>         \n    </div> \n\n     <div class=\"form-group\" [hidden]=\"_combohide\">\n      <label class=\"col-md-2\" >Operator Type <font class=\"mandatoryfont\">*</font></label>\n      <div class=\"col-md-4\"  >\n      <select [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"_obj.t1\" class=\"form-control input-sm\" id=\"comboadvance\" >\n        <option *ngFor=\"let item of ref._lov3.reflovdetails\" value=\"{{item.value}}\" >{{item.caption}}</option> \n      </select>                \n      </div>      \n      <div class=\"col-md-1\"> \n      {{_obj.merchantID}} \n      </div>\n    </div>  \n\n    <div class=\"form-group\" *ngIf=\"chkWaterBill == true\">\n      <label class=\"col-md-2\"> Peanlty Days <font class=\"mandatoryfont\">*</font></label>\n      <div class=\"col-md-4\">                        \n      <input  class=\"form-control input-sm\" type = \"number\" [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_obj.n2\" required=\"true\" >                              \n      </div>\n    </div> \n   \n    <div class=\"form-group\">\n      <label class=\"col-md-2\" > Charges 1 </label>\n      <div class=\"col-md-4\">\n      <select [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_obj.commRef1\" class=\"form-control input-sm\">\n        <option *ngFor=\"let item of ref._lov3.refcharges\" value=\"{{item.value}}\" >{{item.caption}}</option> \n      </select>                \n      </div> \n    </div>\n\n    <div class=\"form-group\">\n      <label class=\"col-md-2\"> Charges 2 </label>\n      <div class=\"col-md-4\">\n      <select [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_obj.commRef2\" class=\"form-control input-sm\">\n        <option *ngFor=\"let item of ref._lov3.refcharges\" value=\"{{item.value}}\" >{{item.caption}}</option> \n      </select>                \n      </div> \n    </div>\n\n    <div class=\"form-group\">\n    <label class=\"col-md-2\"> Charges 3 </label>\n    <div class=\"col-md-4\">\n      <select [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_obj.commRef3\" class=\"form-control input-sm\">\n      <option *ngFor=\"let item of ref._lov3.refcharges\" value=\"{{item.value}}\" >{{item.caption}}</option> \n      </select>                \n      </div> \n    </div>\n\n    <div class=\"form-group\">\n      <label class=\"col-md-2\"> Charges 4 </label>\n      <div class=\"col-md-4\">\n      <select [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_obj.commRef4\" class=\"form-control input-sm\">\n        <option *ngFor=\"let item of ref._lov3.refcharges\" value=\"{{item.value}}\" >{{item.caption}}</option> \n      </select>                \n      </div> \n    </div>\n\n    <div class=\"form-group\">\n      <label class=\"col-md-2\"> Charges 5 </label>\n      <div class=\"col-md-4\">\n      <select [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_obj.commRef5\" class=\"form-control input-sm\">\n        <option *ngFor=\"let item of ref._lov3.refcharges\" value=\"{{item.value}}\" >{{item.caption}}</option> \n      </select>                \n      </div> \n    </div>\n\n    </div>\n  </div>\n  </div>\n  </form>\n  </div>\n  </div>\n</div>\n     \n  <div id=\"sessionalert\" class=\"modal fade\">\n    <div class=\"modal-dialog\">\n      <div class=\"modal-content\">\n        <div class=\"modal-body\">\n          <p>{{sessionAlertMsg}}</p>\n        </div>\n        <div class=\"modal-footer\">\n        </div>\n      </div>\n    </div>\n  </div>\n\n<div [hidden] = \"_mflag\">\n<div class=\"modal\" id=\"loader\"></div>\n</div>\n    \n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], MerchantCommMappingSetup);
    return MerchantCommMappingSetup;
}());
exports.MerchantCommMappingSetup = MerchantCommMappingSetup;
//# sourceMappingURL=merchantcommmappingsetup.component.js.map