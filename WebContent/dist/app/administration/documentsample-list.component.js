"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var router_1 = require('@angular/router');
var core_1 = require('@angular/core');
var rp_client_util_1 = require('../util/rp-client.util');
var rp_references_1 = require('../framework/rp-references');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var documentsampleList = (function () {
    function documentsampleList(ics, _router, 
        //private sanitizer: DomSanitizer, 
        http, l_util, ref) {
        this.ics = ics;
        this._router = _router;
        this.http = http;
        this.l_util = l_util;
        this.ref = ref;
        this._mflag = false;
        this._OperationMode = "";
        this._divexport = false;
        this._isLoading = true;
        this._SearchString = "";
        this._showListing = false;
        this._showPagination = false;
        this._toggleSearch = true; // true - Simple Search, false - Advanced Search
        this._togglePagination = true;
        this.mstatus = 0;
        this._sessionObj = this.getSessionObj();
        this._ButtonInfo = { "role": "", "formname": "", "button": "" };
        this._FilterDataset = {
            filterList: [
                { "itemid": "", "caption": "", "fieldname": "", "datatype": "", "condition": "", "t1": "", "t2": "" }
            ],
            "pageNo": 1, "pageSize": 10, "filterSource": 0, "sessionID": this.ics._profile.sessionID, "userID": this.ics._profile.userID, "userName": this.ics._profile.userName
        };
        this._ListingDataset = {
            dataList: [
                {
                    "autokey": 0, "fileName": "", "filePath": "", "modifiedDate": "", "modifiedTime": "", "modifiedby": "", "postedby": "", "region": "", "status": "", "title": ""
                }
            ],
            "pageNo": 0, "pageSize": 0, "totalCount": 1, "userID": "", "userName": ""
        };
        this.mstatus = ics._profile.loginStatus;
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
    }
    documentsampleList.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    documentsampleList.prototype.ngOnInit = function () {
        this.filterSearch();
        this.loadAdvancedSearchData();
    };
    documentsampleList.prototype.ngAfterViewChecked = function () {
        this._isLoading = false;
    };
    documentsampleList.prototype.loadAdvancedSearchData = function () {
        this._TypeList =
            {
                "lovState": [],
                "lovStatus": []
            };
        this.loadState();
        this.loadStatus();
        this._FilterList =
            [
                { "itemid": "1", "caption": "Title", "fieldname": "Title", "datatype": "simplesearch", "condition": "c", "t1": "", "t2": "", "t3": "" },
                { "itemid": "2", "caption": "State", "fieldname": "t2", "datatype": "lovState", "condition": "", "t1": "", "t2": "", "t3": "" },
                { "itemid": "3", "caption": "Date Range", "fieldname": "ModifiedDate", "datatype": "date", "condition": "bt", "t1": this.l_util.getTodayDate(), "t2": this.l_util.getTodayDate(), "t3": "true" },
            ];
    };
    documentsampleList.prototype.loadState = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics.ticketurl + 'serviceCMS/getStateListByUserID?type=&userID=' + this.ics._profile.userID).subscribe(function (response) {
                if (response.refstate != null && response.refstate != undefined) {
                    //this.ref._lov3.refstate = [{ "value": "", "caption": "" }];
                    _this.ref._lov3.refstate = [];
                    if (!(response.refstate instanceof Array)) {
                        var m = [];
                        m[0] = response.data;
                        _this.ref._lov3.refstate = m;
                    }
                    else {
                        _this.ref._lov3.refstate = response.refstate;
                    }
                    _this.ref._lov3.refstate.forEach(function (iItem) {
                        var l_Item = { "caption": "", "value": "" };
                        l_Item['caption'] = iItem.caption;
                        l_Item['value'] = iItem.value;
                        _this._TypeList.lovState.push(iItem);
                    });
                }
                else {
                    _this.ref._lov3.refstate = [{ "value": "", "caption": "Empty" }];
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Error!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    documentsampleList.prototype.loadStatus = function () {
        var _this = this;
        try {
            var url = this.ics.ticketurl + 'serviceTicketAdm/getTicketStatus';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.msgCode == "0016") {
                        _this.showMsg(data.msgDesc, false);
                        _this.logout();
                    }
                    if (data.msgCode != "0000") {
                        _this.showMsg(data.msgDesc, false);
                    }
                    if (data.msgCode == "0000") {
                        if (!(data.refTicketStatus instanceof Array)) {
                            var m = [];
                            m[0] = data.refTicketStatus;
                            _this.ref._lov3.refTicketStatus = m;
                        }
                        else {
                            _this.ref._lov3.refTicketStatus = data.refTicketStatus;
                        }
                        _this.ref._lov3.refTicketStatus.forEach(function (iItem) {
                            var l_Item = { "caption": "", "value": "" };
                            l_Item['caption'] = iItem.caption;
                            l_Item['value'] = iItem.value;
                            _this._TypeList.lovStatus.push(iItem);
                        });
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    documentsampleList.prototype.loadbutton = function (aRole, aFormName) {
        var _this = this;
        this._ButtonInfo.role = aRole;
        this._ButtonInfo.formname = aFormName;
        var l_Data = this._ButtonInfo;
        var l_Url = this.ics._apiurl + 'service001/getbuttoninfo';
        this.http.doPost(l_Url, l_Data).subscribe(function (data) {
            jQuery("#butNew").hide();
            _this._ButtonInfo = data;
            var l_ButtonSeries = _this._ButtonInfo.button.split(',');
            if (l_ButtonSeries.length > 0) {
                for (var iIndex = 0; iIndex < l_ButtonSeries.length; iIndex++) {
                    if (l_ButtonSeries[iIndex] == '1')
                        jQuery("#butNew").show();
                }
            }
        }, function (error) {
            _this.ics.sendBean({ t1: "rp-error", t2: "Server connection error." });
        }, function () { });
    };
    documentsampleList.prototype.btnToggleSearch_onClick = function () {
        this.toggleSearch(!this._toggleSearch);
    };
    documentsampleList.prototype.btnTogglePagination_onClick = function () {
        this.togglePagination(!this._togglePagination);
    };
    documentsampleList.prototype.toggleSearch = function (aToggleSearch) {
        // Set Flag
        this._toggleSearch = aToggleSearch;
        // Clear Simple Search
        if (!this._toggleSearch)
            jQuery("#isInputSearch").val("");
        // Enable/Disabled Simple Search Textbox
        jQuery('#isInputSearch').prop('disabled', !this._toggleSearch);
        // Hide Advanced Search
        if (this._toggleSearch)
            this.ics.send001("CLEAR");
        // Show/Hide Advanced Search    
        //  
        if (this._toggleSearch)
            jQuery("#asAdvancedSearch").css("display", "none"); // true     - Simple Search
        else
            jQuery("#asAdvancedSearch").css("display", "inline-block"); // false    - Advanced Search
        // Set Icon 
        //  
        if (this._toggleSearch)
            jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-up").addClass("glyphicon glyphicon-menu-down"); // true     - Simple Search
        else
            jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-down").addClass("glyphicon glyphicon-menu-up"); // false    - Advanced Search
        // Set Tooltip
        //
        var l_Tooltip = (this._toggleSearch) ? "Expand" : "Collapse";
        jQuery('#btnToggleSearch').attr('title', l_Tooltip);
    };
    documentsampleList.prototype.togglePagination = function (aTogglePagination) {
        // Set Flag
        this._togglePagination = aTogglePagination;
        // Show/Hide Pagination
        //
        if (this._showPagination && this._togglePagination)
            jQuery("#divPagination").css("display", "inline-block");
        else
            jQuery("#divPagination").css("display", "none");
        // Rotate Icon
        //
        if (!this._togglePagination)
            jQuery("#lblTogglePagination").css("transform", "rotate(270deg)");
        else
            jQuery("#lblTogglePagination").css("transform", "rotate(90deg)");
        // Set Icon Position
        if (!this._togglePagination)
            jQuery("#lblTogglePagination").css({ "margin-left": "-5px", "margin-top": "0px" });
        else
            jQuery("#lblTogglePagination").css({ "margin-left": "-5px", "margin-top": "-1px" });
        // Set Tooltip
        //
        var l_Tooltip = (this._togglePagination) ? "Collapse" : "Expand";
        jQuery('#btnTogglePagination').attr('title', l_Tooltip);
    };
    documentsampleList.prototype.btnClose_MouseEnter = function () {
        jQuery("#btnClose").removeClass('btn btn-sm btn-secondary').addClass('btn btn-sm btn-danger');
    };
    documentsampleList.prototype.btnClose_MouseLeave = function () {
        jQuery("#btnClose").removeClass('btn btn-sm btn-danger').addClass('btn btn-sm btn-secondary');
    };
    documentsampleList.prototype.closePage = function () {
        this._router.navigate(['000', { cmd: "READ", p1: this.ics._profile.userID }]);
    };
    documentsampleList.prototype.renderAdvancedSearch = function (event) {
        this.toggleSearch(!event);
    };
    documentsampleList.prototype.filterAdvancedSearch = function (event) {
        this._FilterDataset.filterSource = 1;
        this._FilterDataset.filterList = event;
        if (this._OperationMode != "prepareFilter") {
            if (this._FilterDataset.pageNo == 1)
                this.filterRecords();
            else
                this._FilterDataset.pageNo = 1;
        }
    };
    documentsampleList.prototype.changedPager = function (event) {
        if (!this._isLoading) {
            var l_objPagination = event.obj;
            this._FilterDataset.pageNo = l_objPagination.current;
            this._FilterDataset.pageSize = l_objPagination.size;
            this.filterRecords();
        }
    };
    documentsampleList.prototype.filterSearch = function () {
        if (this._toggleSearch)
            this.filterCommonSearch();
        else
            this.ics.send001("FILTER");
    };
    documentsampleList.prototype.filterCommonSearch = function () {
        var l_DateRange;
        var l_SearchString = "";
        this._FilterDataset.filterList = [];
        this._FilterDataset.filterSource = 0;
        if (jQuery.trim(this._SearchString) != "") {
            l_SearchString = jQuery.trim(this._SearchString);
            this._FilterDataset.filterList =
                [
                    { "itemid": "1", "caption": "Title", "fieldname": "Title", "datatype": "simplesearch", "condition": "c", "t1": l_SearchString, "t2": "" }
                ];
        }
        //l_DateRange = { "itemid": "3", "caption": "Modified Date", "fieldname": "ModifiedDate", "datatype": "date", "condition": "eq", "t1": this.l_util.getTodayDate(), "t2": "" };
        //this._FilterDataset.filterList.push(l_DateRange);
        if (this._OperationMode != "prepareFilter") {
            if (this._FilterDataset.pageNo == 1)
                this.filterRecords();
            else
                this._FilterDataset.pageNo = 1;
        }
    };
    documentsampleList.prototype.filterRecords = function () {
        var _this = this;
        var l_Data = this._FilterDataset;
        ///let l_ServiceURL: string = this.ics._apiurl + 'service001/searchDocList';
        var l_ServiceURL = this.ics.cmsurl + 'serviceCMS/searchDocList?userID=' + this.ics._profile.userID;
        // Show loading animation
        this._mflag = false;
        this.http.doPost(l_ServiceURL, l_Data).subscribe(function (data) {
            if (data != null) {
                _this._ListingDataset = data;
                // Convert to array for single item
                if (_this._ListingDataset.dataList != undefined) {
                    _this._ListingDataset.dataList = _this.l_util.convertToArray(_this._ListingDataset.dataList);
                }
                // Show / Hide Listing
                _this._showListing = (_this._ListingDataset.dataList.length != 0);
                // Show / Hide Pagination
                _this._showPagination = (_this._showListing && (_this._ListingDataset.totalCount > 10));
                // Show / Hide Pagination
                _this.togglePagination(_this._showListing);
                // Hide loading animation
                _this._mflag = false;
            }
        }, function (error) {
            // Hide loading animation
            _this._mflag = false;
            // Show error message
            _this.ics.sendBean({ t1: "rp-error", t2: "Server connection error." });
        }, function () { });
    };
    documentsampleList.prototype.btnExport_click = function () {
        var _this = this;
        this._mflag = false;
        // Prepare Parameters
        //
        this._OperationMode = "prepareFilter";
        this.filterSearch();
        var l_Data = this._FilterDataset;
        var l_Url = this.ics._apiurl + 'service001/loanApprovalListDownload';
        this.http.doPost(l_Url, l_Data).subscribe(function (res) {
            if (res != null) {
                var l_FileName = res.msgCode;
                _this.downloadFile(l_FileName);
            }
            _this._OperationMode = "";
            _this._mflag = false;
        }, function (error) {
            _this._OperationMode = "";
            _this._mflag = false;
            _this.ics.sendBean({ t1: "rp-error", t2: "Server connection error." });
        }, function () { });
    };
    documentsampleList.prototype.downloadFile = function (aFileName) {
        var l_Url = this.ics._apiurl + 'service001/downloadexcel?filename=' + aFileName;
        jQuery("#ExcelDownload").html("<iframe src=" + l_Url + "></iframe>");
    };
    documentsampleList.prototype.btnPrint_click = function () {
        this.goPrint();
    };
    documentsampleList.prototype.goPrint = function () {
        var _this = this;
        var l_DataListID = "";
        this._mflag = false;
        // Prepare Parameters
        //
        this._OperationMode = "prepareFilter";
        this.filterSearch();
        var l_Data = this._FilterDataset;
        var l_Url = this.ics._apiurl + 'service001/prepareLoanApproveListing';
        this.http.doPost(l_Url, l_Data).subscribe(function (data) {
            if (data != null && data.state) {
                l_DataListID = data.msgCode;
                _this._divexport = true;
            }
            _this._OperationMode = "";
            _this._mflag = false;
        }, function (error) {
            _this._OperationMode = "";
            _this._mflag = false;
            _this.ics.sendBean({ t1: "rp-error", t2: "Server connection error." });
        }, function () { });
    };
    documentsampleList.prototype.goClosePrint = function () {
        this._divexport = false;
    };
    documentsampleList.prototype.goto = function (p) {
        this._router.navigate(['/document', 'read', p]);
    };
    documentsampleList.prototype.goNew = function () {
        this._router.navigate(['/document', 'new']);
    };
    documentsampleList.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    documentsampleList.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    documentsampleList = __decorate([
        core_1.Component({
            selector: 'frmapprovalloans-report',
            template: "\n        <div class=\"container-fluid\" *ngIf=\"!_divexport\">\n        <div class=\"row clearfix\">\n            <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n                <form class=\"form-horizontal\">\n                <fieldset>\n                    <legend>\n                        <div style=\"float:left;display:inline;font-size:18px;margin-top:-3px;margin-left:10px;\">\n                            Documents\n                            <button id=\"btnNew\" class=\"btn btn-sm btn-primary\" type=\"button\" style=\"cursor:pointer;text-align:center;margin-top:-5px;margin-left:10px;height:34px;\" (click)=\"goNew()\" title=\"New\">\n                            <i id=\"lblNew\" class=\"glyphicon glyphicon-file\" style=\"padding-left:2px;\"></i> \n                            </button>\n\n                            <button id=\"btnPrint\" class=\"btn btn-sm btn-primary\" type=\"button\" style=\"cursor:pointer;text-align:center;margin-top:-5px;margin-left:10px;height:34px;\" (click)=\"btnPrint_click()\" title=\"Print\">\n                                <i id=\"lblPrint\" class=\"glyphicon glyphicon-print\"></i> \n                            </button>\n\n                            <button id=\"btnExport\" class=\"btn btn-sm btn-primary\" type=\"button\" style=\"cursor:pointer;text-align:center;margin-top:-5px;height:34px;\" (click)=\"btnExport_click()\" title=\"Export\">\n                                <i id=\"lblExport\" class=\"glyphicon glyphicon-export\" style=\"padding-left:2px;\"></i> \n                            </button>\n                        </div>\n\n                        <div style=\"float:right;text-align:right;margin-top:-8px;margin-bottom:5px;\">                                \n                            <div style=\"display:inline-block;padding-right:0px;width:280px;\">\n                                <div class=\"input-group\"> \n                                    <input class=\"form-control input-sm\" type=\"text\" id=\"isInputSearch\" placeholder=\"Search\" autocomplete=\"off\" spellcheck=\"false\" [(ngModel)]=\"_SearchString\" [ngModelOptions]=\"{standalone: true}\" (keyup.enter)=\"filterSearch()\">\n\n                                    <span id=\"btnSimpleSearch\" class=\"input-group-addon\">\n                                        <i class=\"glyphicon glyphicon-search\" style=\"cursor: pointer\" (click)=\"filterSearch()\"></i>\n                                    </span>\n                                </div>\n                            </div>\n\n                            <button id=\"btnToggleSearch\" class=\"btn btn-sm btn-secondary\" type=\"button\" style=\"cursor:pointer;text-align:center;margin-top:-24px;height:34px;\" (click)=\"btnToggleSearch_onClick()\" title=\"Collapse\">\n                                <i id=\"lblToggleSearch\" class=\"glyphicon glyphicon-menu-down\"></i> \n                            </button>\n\n                            <button *ngIf=\"false\" id=\"btnTogglePagination\" type=\"button\" class=\"btn btn-sm btn-default\" style=\"cursor:pointer;text-align:center;margin-left:25px;margin-top:-24px;width:30px;height:34px;\" (click)=\"btnTogglePagination_onClick()\" title=\"Collapse\">\n                                <i id=\"lblTogglePagination\" class=\"glyphicon glyphicon-eject\" style=\"color:#2e8690;margin-left:-4px;transform: rotate(90deg)\"></i>\n                            </button>\n\n                            <div id=\"divPagination\" style=\"display:none;margin-left:13px;margin-top:-7px;margin-bottom:5px;\">\n                                <pager6 id=\"pgPagination\" rpPageSizeMax=\"100\" [(rpModel)]=\"_ListingDataset.totalCount\" (rpChanged)=\"changedPager($event)\" style=\"font-size:14px;text-align:right;margin-top:-7px;margin-right:10px;\"></pager6>\n                            </div>\n\n                            <button id=\"btnClose\" type=\"button\" class=\"btn btn-sm btn-secondary\" (click)=\"closePage()\" (mouseenter)=\"btnClose_MouseEnter()\" (mouseleave)=\"btnClose_MouseLeave()\" title=\"Close\" style=\"cursor:pointer;height:34px;text-align:center;margin-top:-24px;\">\n                                <i id=\"lblClose\" class=\"glyphicon glyphicon-remove\"></i> \n                            </button>                     \n                        </div>\n                    </legend>\n\n                    <advanced-search id=\"asAdvancedSearch\" [FilterList]=\"_FilterList\" [TypeList]=\"_TypeList\" rpVisibleItems=5 (rpHidden)=\"renderAdvancedSearch($event)\" (rpChanged)=\"filterAdvancedSearch($event)\" style=\"display:none;\"></advanced-search>\n\n                    <div *ngIf=\"!_showListing\" style=\"color:#ccc;\">No result found!</div>\n\n                    <div *ngIf=\"_showListing\" class=\"form-group\">\n                        <div class=\"col-md-12\" style=\"overflow-x:auto;\">\n                            <table class=\"table table-striped table-condense table-hover tblborder table-bordered\">\n                                <thead>\n                                    <tr>\n                                        <th style=\"width: 120px;text-align:right;\">NO.</th>\n                                        <th style=\"min-width: 100px;text-align:left;\">Title</th>\n                                        <th style=\"min-width: 100px;text-align:left\">File Name</th>\n                                        <th style=\"width: 120px;text-align:left\">Status</th>\n                                        <th style=\"width: 120px;text-align:left\">Date</th> \n                                        <th style=\"width: 120px;text-align:left\">Time</th> \t\t\t\t\t\t\n                                        <th style=\"width: 120px;text-align:left\">Posted By</th>\n                                        <th style=\"width: 120px;text-align:left\">Modified By</th>\n                                    </tr>\n                                </thead>\n                                <tbody>\n                                    <tr *ngFor=\"let obj of _ListingDataset.dataList; let i = index\" (click)=\"goto(obj.autokey)\" style=\"cursor:pointer;\">\n                                        <td style=\"width: 120px;text-align:right;\">{{(i+1)}}</td>\n                                        <td style=\"width: 40px;\">{{obj.title}}</td>\n                                        <td style=\"width: 40px;\"><a target=\"_blank\" rel=\"noopener noreferrer\" href=\"{{obj.filePath}}{{obj.fileName}}\">{{obj.fileName}}</a></td>\n                                        <td style=\"width: 120px;\">{{obj.status}}</td>\n                                        <td style=\"width: 120px;\">{{obj.modifiedDate}}</td> \n                                        <td style=\"width: 120px;\">{{obj.modifiedTime}}</td> \t\t\t\t\t\t\n                                        <td style=\"width: 120px;\">{{obj.postedby}}</td>\n                                        <td style=\"width: 120px;\">{{obj.modifiedby}}</td>\n                                    </tr>\n                                </tbody>\n                            </table>\n                        </div>\n                    </div>\n\n                    <div *ngIf=\"_showListing\" style=\"text-align:center\">Total {{ _ListingDataset.totalCount }}</div>\n                </fieldset>\n            </form>\n\n            <div id=\"ExcelDownload\" style=\"display: none;width: 0px;height: 0px;\"></div>\n            <div [hidden]=\"!_doLoading\">            \n          </div>\n        </div> \n    </div>\n<div *ngIf='_divexport'>\n        <button type=\"button\" class=\"close\" (click)=\"goClosePrint()\" style=\"margin-top:-20px;\">&times;</button>\n        <iframe id=\"frame1\" [src]=\"_printUrl\" style=\"width: 100%;height: 95%;\"></iframe> \n</div>\n<div [hidden]=\"_mflag\">\n<div  id=\"loader\" class=\"modal\" ></div>\n</div>\n    ",
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_http_service_1.RpHttpService, rp_client_util_1.ClientUtil, rp_references_1.RpReferences])
    ], documentsampleList);
    return documentsampleList;
}());
exports.documentsampleList = documentsampleList;
//# sourceMappingURL=documentsample-list.component.js.map