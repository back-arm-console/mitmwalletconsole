"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
core_2.enableProdMode();
var FrmVersionHistoryListComponent = (function () {
    function FrmVersionHistoryListComponent(ics, _router, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.http = http;
        this.ref = ref;
        this._mflag = false;
        //about default obj
        this.vHobj = this.getDefaultObj();
        this._pgobj = {
            "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1,
            "end": 10, "size": 10, "totalcount": 0
        };
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this.checkSession();
            this.getAppCodes();
            this.getStatusCodes();
            this.search();
        }
    }
    FrmVersionHistoryListComponent.prototype.getDefaultObj = function () {
        return {
            "data": [{
                    "autokey": "", "appcode": "", "versionkey": "", "version": "", "versiontitle": "",
                    "description": "", "startdate": "", "duedate": "", "remark": "", "status": ""
                }], "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0,
            "sessionID": "", "userID": "", "msgCode": "", "msgDesc": ""
        };
    };
    FrmVersionHistoryListComponent.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    FrmVersionHistoryListComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsgAlert(data.desc);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsgAlert(data.desc);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmVersionHistoryListComponent.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmVersionHistoryListComponent.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    FrmVersionHistoryListComponent.prototype.getAppCodes = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/getAppCodesForVersionHistory';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.msgCode == "0016") {
                        _this.showMsgAlert(data.msgDesc);
                        _this.logout();
                    }
                    if (data.msgCode != "0000") {
                        _this.showMsgAlert(data.msgDesc);
                    }
                    if (data.msgCode == "0000") {
                        if (!(data.refAppCode instanceof Array)) {
                            var m = [];
                            m[0] = data.refAppCode;
                            _this.ref._lov3.refAppCode = m;
                        }
                        else {
                            _this.ref._lov3.refAppCode = data.refAppCode;
                        }
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmVersionHistoryListComponent.prototype.getStatusCodes = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/getStatusCodesForVersionHistory';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.msgCode == "0016") {
                        _this.showMsgAlert(data.msgDesc);
                        _this.logout();
                    }
                    if (data.msgCode != "0000") {
                        _this.showMsgAlert(data.msgDesc);
                    }
                    if (data.msgCode == "0000") {
                        if (!(data.refVersionStatusCode instanceof Array)) {
                            var m = [];
                            m[0] = data.refVersionStatusCode;
                            _this.ref._lov3.refVersionStatusCode = m;
                        }
                        else {
                            _this.ref._lov3.refVersionStatusCode = data.refVersionStatusCode;
                        }
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmVersionHistoryListComponent.prototype.search = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/getAllVersionHistory';
            this.vHobj.sessionID = this.ics._profile.sessionID;
            this.vHobj.userID = this.ics._profile.userID;
            var json = this.vHobj;
            this.http.doPost(url, json).subscribe(function (data) {
                _this._mflag = true;
                var temp = _this.vHobj;
                if (data != null) {
                    _this.vHobj = data;
                    if (_this.vHobj.msgCode == "0016") {
                        _this.showMsgAlert(_this.vHobj.msgDesc);
                        _this.logout();
                    }
                    if (_this.vHobj.msgCode != "0000") {
                        _this.showMsgAlert(_this.vHobj.msgDesc);
                        _this.vHobj = temp;
                    }
                    if (_this.vHobj.msgCode == "0000") {
                        if (_this.vHobj.totalCount == 0) {
                            _this.showMsgAlert(_this.vHobj.msgDesc);
                        }
                        else {
                            if (!(data.data instanceof Array)) {
                                var m = [];
                                m[0] = data.data;
                                _this.vHobj.data = m;
                            }
                        }
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmVersionHistoryListComponent.prototype.goNew = function () {
        this._router.navigate(['/version-history', , { cmd: "NEW" }]);
    };
    FrmVersionHistoryListComponent.prototype.searchKeyup = function (e) {
        if (e.which == 13) {
            this.vHobj.currentPage = 1;
            this.search();
        }
    };
    FrmVersionHistoryListComponent.prototype.searching = function () {
        this.vHobj.currentPage = 1;
        this.search();
    };
    FrmVersionHistoryListComponent.prototype.changedPager = function (event) {
        if (this.vHobj.totalCount != 0) {
            this._pgobj = event;
            var current = this.vHobj.currentPage;
            var size = this.vHobj.pageSize;
            this.vHobj.currentPage = this._pgobj.current;
            this.vHobj.pageSize = this._pgobj.size;
            if (this._pgobj.current != current || this._pgobj.size != size) {
                this.search();
            }
        }
    };
    FrmVersionHistoryListComponent.prototype.goto = function (p) {
        this._router.navigate(['/version-history', 'read', p]);
    };
    FrmVersionHistoryListComponent = __decorate([
        core_1.Component({
            selector: 'version-history-list',
            template: " \n    <div class=\"container col-md-12\">\n        <form class=\"form-inline\"> \n            <!-- Form Name -->\n            <legend>Version History List</legend>\n            <div class=\"input-group\">\n                <span class=\"input-group-btn input-md\" style=\"width: 20px;\">\n                    <button class=\"btn btn-primary\" type=\"button\" (click)=\"goNew()\">New</button>\n                </span> \n                <input id=\"textinput\" name=\"textinput\" type=\"text\" placeholder=\"Search\" [(ngModel)]=\"vHobj.searchText\" (keyup)=\"searchKeyup($event)\" maxlength=\"50\" class=\"form-control input-md\">\n                <span class=\"input-group-btn input-md\">\n                    <button class=\"btn btn-primary input-md\" type=\"button\" (click)=\"searching()\">\n                        <span class=\"glyphicon glyphicon-search\"></span>Search\n                    </button>\n                </span>        \n            </div>      \n        </form>      \n\n        <div>\n            <adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"vHobj.totalCount\" (rpChanged)=\"changedPager($event)\"></adminpager> \n        </div>\n\n        <div>\n            <table class=\"table table-striped table-condensed table-hover tblborder\" style=\"font-size: 14px;\">\n                <thead>\n                    <tr>\n                        <th>No.</th>\n                        <th>Version Key</th>\n                        <th>App Code</th>\n                        <th>Version</th>\n                        <th>Version Title</th>\n                        <th>Description</th>    \n                        <th>Start Date</th>\n                        <th>Due Date</th>\n                        <th>Remark</th>\n                        <th>Status</th>\n                    </tr>\n                </thead>\n                <tbody>\n                    <tr *ngFor=\"let obj of vHobj.data\">\n                        <td><a (click)=\"goto(obj.autokey)\">{{obj.autokey}}</a></td>\n                        <td>{{obj.versionkey}}</td>\n                        <ng-container *ngFor=\"let item of ref._lov3.refAppCode\">\n                            <td *ngIf=\"item.value==obj.appcode\">{{item.caption}}</td>\n                        </ng-container>\n                        <td>{{obj.version}}</td>\n                        <td>{{obj.versiontitle}}</td>\n                        <td>{{obj.description}}</td>\n                        <td>{{obj.startdate}}</td>\n                        <td>{{obj.duedate}}</td>\n                        <td>{{obj.remark}}</td>\n                        <ng-container *ngFor=\"let item of ref._lov3.refVersionStatusCode\">\n                            <td *ngIf=\"item.value==obj.status\">{{item.caption}}</td>\n                        </ng-container>\n                    </tr>  \n                </tbody>\n            </table>\n        </div>\n    </div>\n\n    <div [hidden]=\"_mflag\">\n      <div class=\"modal\" id=\"loader\"></div>\n    </div>\n    "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmVersionHistoryListComponent);
    return FrmVersionHistoryListComponent;
}());
exports.FrmVersionHistoryListComponent = FrmVersionHistoryListComponent;
//# sourceMappingURL=frmversionhistorylist.component.js.map