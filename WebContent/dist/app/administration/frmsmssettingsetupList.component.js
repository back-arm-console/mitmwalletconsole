"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var Observable_1 = require('rxjs/Observable');
var core_2 = require('@angular/core');
// RP Framework
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_client_util_1 = require('../util/rp-client.util');
core_2.enableProdMode();
var FrmSMSSettingSetupListComponent = (function () {
    function FrmSMSSettingSetupListComponent(ics, _router, http) {
        this.ics = ics;
        this._router = _router;
        this.http = http;
        // Application Specific
        this._searchVal = ""; // simple search
        this._flagas = true; // flag advance search
        this._col_list = [{ "col_no": "1", "flag": false }, { "col_no": "2", "flag": false }];
        this._sorting = { _sort_type: "asc", _sort_col: "1" };
        this._mflag = true;
        this._util = new rp_client_util_1.ClientUtil();
        this.sessionAlertMsg = "";
        this._smsobj = {
            "data": [{ "code": "", "desc": "", "id": 0, "syskey": 0, "serviceCode": "", "serviceDesc": "", "funCode": "", "funDesc": "", "site": "", "from": "", "to": "", "fromMsg": "", "toMsg": "", "message": "", "merchantID": "", "operatorType": "", "active": "", "sessionID": "", "userID": "" }],
            "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0
        };
        this._pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 0 };
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this.search();
        }
    }
    // list sorting part
    FrmSMSSettingSetupListComponent.prototype.changeDefault = function () {
        for (var i = 0; i < this._col_list.length; i++) {
            this._col_list[i].flag = false;
        }
    };
    FrmSMSSettingSetupListComponent.prototype.addSort = function (e) {
        for (var i = 0; i < this._col_list.length; i++) {
            if (this._col_list[i].col_no == e) {
                var _tmp_flag = this._col_list[i].flag;
                this.changeDefault();
                this._col_list[i].flag = !_tmp_flag;
                this._sorting._sort_type = !_tmp_flag ? "desc" : "asc";
                this._sorting._sort_col = e;
                break;
            }
        }
    };
    FrmSMSSettingSetupListComponent.prototype.changedPager = function (event) {
        if (this._smsobj.totalCount != 0) {
            this._pgobj = event;
            var current = this._smsobj.currentPage;
            var size = this._smsobj.pageSize;
            this._smsobj.currentPage = this._pgobj.current;
            this._smsobj.pageSize = this._pgobj.size;
            if (this._pgobj.current != current || this._pgobj.size != size) {
                this.search();
            }
        }
    };
    FrmSMSSettingSetupListComponent.prototype.search = function () {
        var _this = this;
        try {
            this._mflag = false;
            this.http.doGet(this.ics._apiurl + 'service001/getSMSSettingList?searchVal=' + this._smsobj.searchText + '&pagesize=' + this._smsobj.pageSize + '&currentpage=' + this._smsobj.currentPage + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (response) {
                _this._mflag = true;
                if (response.msgCode == "0016") {
                    _this.sessionAlertMsg = response.msgDesc;
                    _this.showMessage();
                }
                else {
                    if (response.totalCount == 0) {
                        _this._smsobj = response;
                        _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Data not found!" });
                    }
                    if (response.data != null) {
                        if (!(response.data instanceof Array)) {
                            var m = [];
                            m[0] = response.data;
                            _this._smsobj.data = m;
                            _this._smsobj.totalCount = response.totalCount;
                        }
                        else {
                            _this._smsobj = response;
                        }
                    }
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmSMSSettingSetupListComponent.prototype.searchKeyup = function (e) {
        if (e.which == 13) {
            this._smsobj.currentPage = 1;
            this._msghide = true;
            this._recordhide = false;
            this.search();
        }
    };
    FrmSMSSettingSetupListComponent.prototype.Searching = function () {
        this._smsobj.currentPage = 1;
        this._msghide = true;
        this._recordhide = false;
        this.search();
    };
    FrmSMSSettingSetupListComponent.prototype.goto = function (p) {
        this._router.navigate(['/SMS Setting', 'read', p]);
    };
    FrmSMSSettingSetupListComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Observable_1.Observable.timer(3000).subscribe(function (x) {
            _this.goLogOut();
        });
    };
    FrmSMSSettingSetupListComponent.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    FrmSMSSettingSetupListComponent.prototype.goNew = function () {
        this._router.navigate(['/SMS Setting', 'new']);
    };
    FrmSMSSettingSetupListComponent = __decorate([
        core_1.Component({
            selector: 'smssettingsetup-list',
            template: " \n  <div class=\"container col-md-12\">\n    <form class=\"form-inline\"> \n    \n    <!-- Form Name -->\n    <legend>SMS Setting List</legend>\n      <div class=\"input-group\">\n        <span class=\"input-group-btn input-md\">\n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"goNew();\">New</button>\n        </span>\n        <input id=\"textinput\" name=\"textinput\" type=\"text\" placeholder=\"Search\" [(ngModel)]=\"_smsobj.searchText\" (keyup)=\"searchKeyup($event)\" maxlength=\"50\" class=\"form-control input-md\">\n        <span class=\"input-group-btn input-md\">\n\t        <button class=\"btn btn-primary input-md\" type=\"button\" (click)=\"Searching()\" >\n\t          <span class=\"glyphicon glyphicon-search\"></span>Search\n\t        </button>\n\t      </span>        \n\t    </div>      \n    </form>\n    \n   <div style = \"margin-top : 10px\">\n   <adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"_smsobj.totalCount\" (rpChanged)=\"changedPager($event)\" ></adminpager> \n  </div> \n  <table class=\"table table-striped table-condensed table-hover tblborder\" style=\"font-size:14px;\">\n    <thead>\n     <tr>\n        <th> ID. </th>\n        <th>Service </th>\n        <th>Function</th>\n        <th>Site</th>\n        <th>Message</th>\n        <th>Merchant ID</th>\n        <th>Operator</th>\n        <th>Active</th>    \n    </tr>\n  </thead>\n  <tbody>\n    <tr *ngFor=\"let obj of _smsobj.data\">\n        <td><a (click)=\"goto(obj.id)\">{{obj.id}}</a></td>\n        <td>{{obj.serviceDesc}}</td>\n        <td>{{obj.funDesc}}</td>\n        <td *ngIf =\"obj.from == '0' \" >To</td>\n        <td *ngIf =\"obj.to == '0' \" >From</td>\n        <td *ngIf =\"obj.from == 0\" > {{obj.toMsg}} </td>\n        <td *ngIf =\"obj.to == 0\" > {{obj.fromMsg}} </td>\n        <td>{{obj.merchantID}}</td>\n        <td >{{obj.operatorType}}</td>\n        <td *ngIf =\"obj.active == 0\">No</td>\n        <td *ngIf =\"obj.active == 1\">Yes</td>\n    </tr>  \n  </tbody>\n  </table>\n  </div> \n  \n    <div [hidden] = \"_mflag\">\n        <div class=\"loader modal\"id=\"loader\"></div>\n    </div>\n   "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_http_service_1.RpHttpService])
    ], FrmSMSSettingSetupListComponent);
    return FrmSMSSettingSetupListComponent;
}());
exports.FrmSMSSettingSetupListComponent = FrmSMSSettingSetupListComponent;
//# sourceMappingURL=frmsmssettingsetupList.component.js.map