"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var FrmQnAListComponent = (function () {
    function FrmQnAListComponent(ics, route, ref, _router, http) {
        this.ics = ics;
        this.route = route;
        this.ref = ref;
        this._router = _router;
        this.http = http;
        // Application Specific
        this._totalcount = 1;
        this._searchVal = "";
        this._CropType = "";
        this._StateType = "";
        this._que = "";
        this._time = "";
        this.key = 0;
        this._array = [];
        this._pager = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 0, "size": 0, "totalcount": 1, "t1": "", "changestatus": 0 };
        this._image = [];
        this._cmtArray = [];
        this._ansArray = [];
        this._replyArray = [];
        this._tmpObj = { '_key': 0, '_index': 0 };
        this._util = new rp_client_util_1.ClientUtil();
        this._ansObj = { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": "", "modifiedDate": "", "modifiedTime": "", "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [] };
        this._replyObj = { "syskey": 0, "autokey": 0, "createdDate": "", "modifiedDate": "", "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [] };
        this._obj = { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": "", "modifiedDate": "", "modifiedTime": "", "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "question", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [], "videoUpload": [], "comData": [], "cropdata": [], "agrodata": [], "ferdata": [], "towndata": [], "crop": null, "fert": null, "agro": null, "addTown": null };
        this._roleval = "";
        this.mstatus = 0;
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        this.mstatus = ics._profile.loginStatus;
        this.search(this._pager);
        this._time = this._util.getTodayTime();
        this._ansObj.createdTime = this._time;
        this._ansObj.modifiedTime = this._time;
        this.key = Number(this.ics._profile.t1);
        this.getCropComboList();
        this.getStatusList(this.mstatus);
    }
    FrmQnAListComponent.prototype.search = function (p) {
        var _this = this;
        if (p.end == 0) {
            p.end = this.ics._profile.n1;
        }
        if (p.size == 0) {
            p.size = this.ics._profile.n1;
        }
        p.t1 = this._searchVal;
        var url = this.ics._apiurl + 'serviceQuestion/searchQuesList?searchVal=' + this._searchVal + '&status=' + this.mstatus + '&statetype=' + this._StateType + '&searchtype=' + 'question';
        var json = p;
        this.http.doPost(url, json).subscribe(function (response) {
            if (response != null && response != undefined && response.state) {
                _this._totalcount = response.totalCount;
                _this._array = response.data;
                for (var i = 0; i < _this._array.length; i++) {
                    if (_this._array[i].t2.length > 100) {
                        _this._array[i].t2 = _this._array[i].t2.substring(0, 101) + "....";
                    }
                    _this._array[i].modifiedDate = _this._util.changeStringtoDateFromDB(_this._array[i].modifiedDate);
                    if (!(_this._array[i].upload.length > 0)) {
                        _this._array[i].t9 = 0;
                    }
                    else {
                        _this._array[i].t9 = 1;
                    }
                }
                _this.mstatus = 0;
            }
            else {
                _this._array = [];
                _this._totalcount = 1;
                _this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": "Data not found!" });
            }
        }, function (error) { }, function () { });
    };
    FrmQnAListComponent.prototype.searchVal = function () {
        this._pager = { "current": 1, "prev": 1, "last": 1, "next": 1, "start": 1, "end": 0, "size": 0, "totalcount": 1, "t1": "", "changestatus": 0 };
        this.search(this._pager);
    };
    FrmQnAListComponent.prototype.goto = function (p) {
        this._router.navigate(['/question', 'read', p]);
    };
    FrmQnAListComponent.prototype.goNew = function () {
        this._router.navigate(['/question', 'new']);
    };
    FrmQnAListComponent.prototype.goView = function (p) {
        this._router.navigate(['/questionView', 'read', p]);
    };
    FrmQnAListComponent.prototype.changedPager = function (event) {
        var k = event.flag;
        this._pager = event.obj;
        if (k) {
            this.search(this._pager);
        }
    };
    FrmQnAListComponent.prototype.goClickLike = function (key, index) {
        this._array[index].n2 = this._array[index].n2 + 1;
        this.http.doGet(this.ics._apiurl + 'serviceQuestion/clickLikeQuestion?key=' + key).subscribe(function (data) {
            if (data.state)
                console.log("Like...");
        }, function (error) { }, function () { });
    };
    FrmQnAListComponent.prototype.goClickUnlike = function (key, index) {
        this._array[index].n4 = this._array[index].n4 + 1;
        this.http.doGet(this.ics._apiurl + 'serviceQuestion/clickUnlikeQuestion?key=' + key).subscribe(function (data) {
            if (data.state)
                console.log("Unlike...");
        }, function (error) { }, function () { });
    };
    FrmQnAListComponent.prototype.goAnswer = function (key, index) {
        var _this = this;
        jQuery("#commentModal").modal();
        this._tmpObj._key = key;
        this._tmpObj._index = index;
        this._cmtArray = [];
        this.http.doGet(this.ics._apiurl + 'serviceQuestion/getAnswers?id=' + key).subscribe(function (response) {
            if (response.state)
                _this._cmtArray = response.data;
        }, function (error) { }, function () { });
    };
    FrmQnAListComponent.prototype.saveAns = function (cmtObj) {
        var _this = this;
        cmtObj.n1 = this._tmpObj._key;
        cmtObj.n5 = Number(this.ics._profile.t1);
        cmtObj.t12 = '';
        cmtObj.t13 = '';
        if (cmtObj.t2 != '') {
            var url = this.ics._apiurl + 'serviceQuestion/saveAns';
            var json = cmtObj;
            this.http.doPost(url, json).subscribe(function (response) {
                _this._cmtArray = [];
                if (response.state)
                    _this._cmtArray = response.data;
                _this._ansObj.t2 = "";
            }, function (error) {
                _this.showMessage("Can't Saved This Record!!!", undefined);
            }, function () { });
        }
    };
    FrmQnAListComponent.prototype.goAns = function (key, index, t2) {
        var _this = this;
        this._que = t2;
        jQuery("#answerModal").modal();
        this._tmpObj._key = key;
        this._tmpObj._index = index;
        this._cmtArray = [];
        this.http.doGet(this.ics._apiurl + 'serviceQuestion/getAns?id=' + key).subscribe(function (response) {
            if (response.state)
                _this._cmtArray = response.data;
        }, function (error) { }, function () { });
    };
    FrmQnAListComponent.prototype.saveAnswer = function (cmtObj) {
        var _this = this;
        cmtObj.n1 = this._tmpObj._key;
        cmtObj.n5 = Number(this.ics._profile.t1);
        cmtObj.t12 = '';
        cmtObj.t13 = '';
        var url = this.ics._apiurl + 'serviceQuestion/saveAnswer';
        var json = cmtObj;
        this.http.doPost(url, json).subscribe(function (response) {
            _this._cmtArray = [];
            if (response.state)
                _this._cmtArray = response.data;
            _this._array[_this._tmpObj._index].n3 = _this._array[_this._tmpObj._index].n3 + 1;
            _this._ansObj.t2 = "";
        }, function (error) {
            _this.showMessage("Can't Saved This Record!!!", undefined);
        }, function () { });
    };
    FrmQnAListComponent.prototype.replyAnswer = function (key, num) {
        var _this = this;
        this._tmpObj._index = num;
        this._cmtArray[num].t13 = 'true';
        this._replyArray = [];
        this.http.doGet(this.ics._apiurl + 'serviceQuestion/getAnswers?id=' + key).subscribe(function (response) {
            if (response.state)
                _this._replyArray = response.data;
        }, function (error) { }, function () { });
    };
    FrmQnAListComponent.prototype.saveReply = function (skey) {
        var _this = this;
        this._replyObj.n1 = skey;
        this._replyObj.n3 = -1;
        this._replyObj.n5 = Number(this.ics._profile.t1);
        var url = this.ics._apiurl + 'serviceQuestion/saveAnswer';
        var json = this._replyObj;
        this.http.doPost(url, json).subscribe(function (response) {
            _this._replyArray = [];
            if (response.state)
                _this._replyArray = response.data;
            _this._cmtArray[_this._tmpObj._index].n3 = _this._cmtArray[_this._tmpObj._index].n3 + 1;
            _this._replyObj.t2 = "";
        }, function (error) {
            _this.showMessage("Can't Saved This Record!!!", undefined);
        }, function () { });
    };
    FrmQnAListComponent.prototype.editAnswer = function (obj, num) {
        this._cmtArray[num].t12 = 'true';
    };
    FrmQnAListComponent.prototype.deleteAnswer = function (obj, num) {
        var _this = this;
        var url = this.ics._apiurl + 'serviceQuestion/deleteAnswer';
        var json = obj;
        this.http.doPost(url, json).subscribe(function (response) {
            _this.showMessage(response.msgDesc, response.state);
            _this._cmtArray = [];
            if (response.state)
                _this._cmtArray = response.data;
        }, function (error) { }, function () { });
    };
    FrmQnAListComponent.prototype.deleteAns = function (obj, num) {
        var _this = this;
        var url = this.ics._apiurl + 'serviceQuestion/deleteAns';
        var json = obj;
        this.http.doPost(url, json).subscribe(function (response) {
            _this._cmtArray = [];
            if (response.state)
                _this._cmtArray = response.data;
        }, function (error) { }, function () { });
    };
    FrmQnAListComponent.prototype.setImgUrl = function (str) {
        return 'upload/image/' + str;
    };
    FrmQnAListComponent.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    // goImage(key, index) {
    //     jQuery("#commentModal").modal();
    //     this._tmpObj._key = key;
    //     this._tmpObj._index = index;
    //     this._image = [];
    //     this.http.doGet(this.ics._apiurl + 'serviceQuestion/getImage?id=' + key).subscribe(
    //         response => {
    //             if (response.state) {
    //                 this._image = response.uploads;
    //             }
    //         },
    //         error => { },
    //         () => { }
    //     );
    // }
    FrmQnAListComponent.prototype.getCropComboList = function () {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'serviceCrop/getSenderList').subscribe(function (response) {
            if ((response != null || response != undefined) && response.data.length > 0) {
                _this.ref._lov3.cropcombo = _this._util.changeArray(response.data, _this._obj, 1);
            }
        }, function (error) { }, function () { });
    };
    FrmQnAListComponent.prototype.getStatusList = function (mstatus) {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'serviceQuestion/getStatusList?status=' + this.mstatus).subscribe(function (response) {
            if ((response != null || response != undefined) && response.data.length > 0) {
                _this.ref._lov3.statuscombo = _this._util.changeArray(response.data, _this._obj, 1);
                _this._StateType = _this.ref._lov3.statuscombo[3].value;
            }
        }, function (error) { }, function () { });
    };
    FrmQnAListComponent = __decorate([
        core_1.Component({
            selector: 'fmr-QnAList',
            template: "\n    <div class=\"container\">\n    <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n    <!-- Form Open--> \n    <form class=\"form-inline\" > \n    <legend>Question List</legend>\n    <div class=\"input-group\">\n    <span class=\"input-group-btn input-md\">\n        <button class=\"btn btn-danger\" type=\"button\" (click)=\"goNew();\">New</button>\n    </span>  \n        <input id=\"textinput\" name=\"textinput\" type=\"text\" placeholder=\"Search\" [(ngModel)]=\"_searchVal\" (keyup.enter)=\"searchVal()\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control input-md\">\n    <span class=\"input-group-btn input-md\">\n\t     <button class=\"btn btn-danger input-md\" type=\"button\" (click)=\"searchVal()\" >\n\t <span class=\"glyphicon glyphicon-search\"></span>Search\n\t     </button>\n     </span>\n     </div>\n      \n    <div class=\"form-group\">\n    <label class=\"col-md-4\">Status</label>\n    <div class=\"col-md-4\">\n       <rp-input [(rpModel)]=\"_StateType\" (change)=\"searchVal($event)\" rpRequired =\"true\" rpType=\"statuscombo\"  autofocus></rp-input>\n     </div>  \n     </div>     \n    </form>\n    <!-- Form Close--> \n\n    <pager id=\"pgQuestion\" [(rpModel)]=\"_totalcount\" [(rpCurrent)]=\"_pager.current\" (rpChanged)=\"changedPager($event)\"></pager> \n    <!--Table Responsive Open-->\n    <div class=\"table-responsive\">\n    <table class=\"table table-striped table-condensed table-hover tblborder\" style=\"font-size:14px;\">\n      <thead>\n            <tr>\n                <th width=\"30%\">Question</th>\n                <th width=\"10%\">Image</th>\n                <th width=\"10%\">Status</th>\n                <th width=\"15%\">Date</th>\n                <th width=\"10%\">Time</th>\n                <th width=\"15%\">Post by</th>\n                <th width=\"10%\"></th>\n            </tr>\n        </thead>\n        <tbody *ngFor=\"let obj of _array;let num = index\">\n          <tr>\n               <td><a (click)=\"goto(obj.syskey)\" class=\"unicode\"><p>{{obj.t2}}</p></a></td>\n               <td *ngIf =\"obj.t9==0\"><p></p></td>\n               <td *ngIf =\"obj.t9==1\"> <img src={{setImgUrl(obj.upload[0])}}  height=\"50\" width=\"50\" /></td>\n               <td><p  class=\"unicode\">{{obj.t10}}</td>\n               <td><p  class=\"unicode\">{{obj.modifiedDate}}</p></td>                    \n                <td><p  class=\"unicode\">{{obj.modifiedTime}}</p></td>\n                <td><span style=\"color:gray;font-style:italic;font-size:11px\">{{obj.userName}}</span></td>\n                <td><button class=\"btn btn-danger\" id=\"save\" type=\"button\" (click)=\"goAns(obj.syskey,num,obj.t2)\">Answer</button> </td>\n            </tr>\n    </tbody>\n    </table>\n    </div>\n <!--Table Responsive Close-->\n\n    </div>\n    </div>\n    </div>\n\n    <!-- Modal -->\n    <div id=\"commentModal\" class=\"modal fade\" role=\"dialog\">\n    <div class=\"modal-dialog modal-md\">  \n        <div class=\"modal-content\">\n        <div class=\"modal-header\">\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>           \n            <div class=\"form-group col-md-12\">\n                        <h4 align=\"center\" class=\"modal-title\">Image</h4>\n            </div>\n        </div>\n        <div id=\"jbody\" class=\"modal-body\" >\n            <div style=\"overflow: hidden; position: relative;\">                \n                <div class=\"col-xs-7 col-sm-7 col-md-7 col-lg-7  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n                    <div class=\"form-group col-md-12\">\n                        <label class=\"col-md-6\"></label>\n                        <div class=\"col-md-6\" *ngFor=\"let img of _image\">\n                            <img src={{setImgUrl(img)}}  alt={{img}} height=\"140\" width=\"200\" align=\"middle\" />\n                        </div> \n                        <div class=\"col-md-0\"></div>\n                    </div>                 \n                   \n                </div>\n            </div>\n        </div>\n        </div>\n    </div>\n    </div>\n\n\n     <!-- Modal -->\n    <div id=\"answerModal\" class=\"modal fade\" role=\"dialog\">\n    <div class=\"modal-dialog modal-lg\">  \n        <div class=\"modal-content\">\n        <div class=\"modal-header\">\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n            <h4 class=\"modal-title\">Question & Answer</h4>\n        </div>\n        <div id=\"jbody\" class=\"modal-body\" >\n            <div style=\"overflow: hidden; position: relative;\">                \n                <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n                    <div class=\"form-group col-md-12\">\n                         <label class=\"col-md-2\">Question :</label>\n                             <div class=\"col-md-8\">                             \n                                <label >{{_que}}</label>\n                             </div>\n                    </div>   \n                    <div *ngFor=\"let obj of _cmtArray;let num2 = index\" class=\"col-md-12\">\n                        <div *ngIf=\"obj.t12!='true'\">                           \n                             <label class=\"col-md-2\">Answer {{num2+1}} :</label>\n                                 <div class=\"col-md-8\">                             \n                                    <p >{{obj.t2}}</p>\n                                </div>\n                                <br>\n                        </div>\n\n                        <div *ngIf=\"obj.t12=='true'\"> \n                             <label class=\"col-md-2\">Answer : </label>\n                            <div class=\"col-md-7\">\n                                <textarea type=\"text\" [(ngModel)]=\"obj.t2\" class=\"form-control input-md\" rows=\"3\" ></textarea>\n                           </div>\n                           <br>\n                        </div>\n\n                        <button *ngIf=\"obj.t12=='true'\" class=\"btn btn-link\" type=\"button\" (click)=\"saveAns(obj)\">Save</button> \n                        <button *ngIf=\"obj.t12!='true' && obj.n5 == key\" class=\"btn btn-link\" type=\"button\" (click)=\"editAnswer(obj,num2)\">Edit</button>\n                        <button class=\"btn btn-link\" type=\"button\" (click)=\"deleteAns(obj,num2)\">Delete</button> \n                        <hr>\n                    </div>\n\n                    <div  class=\"form-group col-md-12\">\n                       <label class=\"col-md-2\">Answer :</label>\n                        <div class=\"col-md-8\">                             \n                            <textarea type=\"text\" [(ngModel)]=\"_ansObj.t2\" class=\"form-control input-md\" rows=\"3\" ></textarea>\n                        </div>\n                        <div class=\"col-md-2\"> \n                            <button class=\"btn btn-danger\" type=\"button\" (click)=\"saveAns(_ansObj)\">Save</button>  \n                        </div>\n                    </div> \n            </div>\n        </div>\n        </div>\n    </div>\n    </div>\n\n\n    \n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.ActivatedRoute, rp_references_1.RpReferences, router_1.Router, rp_http_service_1.RpHttpService])
    ], FrmQnAListComponent);
    return FrmQnAListComponent;
}());
exports.FrmQnAListComponent = FrmQnAListComponent;
//# sourceMappingURL=frmQnAList.component.js.map