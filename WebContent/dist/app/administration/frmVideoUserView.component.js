"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_client_util_1 = require('../util/rp-client.util');
core_1.enableProdMode();
var FrmVideoUserViewComponent = (function () {
    function FrmVideoUserViewComponent(ics, _router, http) {
        this.ics = ics;
        this._router = _router;
        this.http = http;
        // Application Specific
        this._totalcount = 1;
        this._searchVal = "";
        this.count = 0;
        this.key = 0;
        this._time = "";
        this.syskey = 0;
        this._array = [];
        this._pager = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 0, "size": 0, "totalcount": 1 };
        this._obj = [];
        this._cmtArray = [];
        this._replyArray = [];
        this._likearr = [];
        this.mobile = "";
        this._util = new rp_client_util_1.ClientUtil();
        this._tmpObj = { '_key': 0, '_index': 0 };
        this._ansObj = { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": "", "modifiedDate": "", "modifiedTime": "", "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [], "videoUpload": [] };
        this._replyObj = { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": "", "modifiedDate": "", "modifiedTime": "", "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [], "videoUpload": [] };
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        this.search(this._pager);
        this._time = this._util.getTodayTime();
        this._ansObj.createdTime = this._time;
        this._ansObj.modifiedTime = this._time;
        this._replyObj.createdTime = this._time;
        this._replyObj.modifiedTime = this._time;
        console.log(this._time + " today");
        this.syskey = Number(this.ics._profile.t1);
        this.mobile = this.ics._profile.userID;
        console.log(this.mobile + " mobile");
    }
    FrmVideoUserViewComponent.prototype.search = function (p) {
        var _this = this;
        if (p.end == 0) {
            p.end = this.ics._profile.n1;
        }
        if (p.size == 0) {
            p.size = this.ics._profile.n1;
        }
        var url = this.ics._apiurl + 'serviceArticle/searchVideoUserList?searchVal=' + this._searchVal + '&mobile=' + this.ics._profile.userID;
        var json = p;
        this.http.doPost(url, json).subscribe(function (response) {
            console.log(JSON.stringify(response) + " List");
            if (response != null && response != undefined && response.state) {
                _this._totalcount = response.totalCount;
                _this._array = response.data;
                for (var i = 0; i < _this._array.length; i++) {
                    for (var j = 0; j < _this._array[i].videoUpload.length; j++) {
                        _this._ansObj.videoUpload.push(_this._array[i].videoUpload[j]);
                    }
                }
            }
            else {
                _this._array = [];
                _this._totalcount = 1;
                _this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": "Data not found!" });
            }
        }, function (error) { }, function () { });
    };
    FrmVideoUserViewComponent.prototype.searchVal = function () {
        this._pager = { "current": 1, "prev": 1, "last": 1, "next": 1, "start": 1, "end": 0, "size": 0, "totalcount": 1 };
        this.search(this._pager);
    };
    FrmVideoUserViewComponent.prototype.goto = function (p) {
        this._router.navigate(['/article', 'read', p]);
    };
    FrmVideoUserViewComponent.prototype.goNew = function () {
        this._router.navigate(['/article', 'new']);
    };
    FrmVideoUserViewComponent.prototype.changedPager = function (event) {
        var k = event.flag;
        this._pager = event.obj;
        if (k) {
            this.search(this._pager);
        }
    };
    /*goClickLike(key, index) {
        this._array[index].n2 = this._array[index].n2 + 1;
        this.http.doGet(this.ics._apiurl + 'serviceArticle/clickLikeArticle?key=' + key).subscribe(
            data => {
                if (data.state) console.log("Like...");
            },
            error => { },
            () => { }
        );
    }*/
    FrmVideoUserViewComponent.prototype.goClickLike = function (key, index) {
        var _this = this;
        //this._obj[index].n2 = this._obj[index].n2 + 1;
        this.http.doGet(this.ics._apiurl + 'serviceVideo/clickLikeVideo?key=' + key + '&userSK=' + this.syskey).subscribe(function (data) {
            console.log(JSON.stringify(data) + "Like...");
            if (data.state) {
                console.log("Like...");
                _this.goLikeCount(key, index);
            }
        }, function (error) { }, function () { });
    };
    FrmVideoUserViewComponent.prototype.goClickUnlike = function (key, index) {
        this._array[index].n4 = this._array[index].n4 + 1;
        this.http.doGet(this.ics._apiurl + 'serviceArticle/clickUnlikeArticle?key=' + key).subscribe(function (data) {
            if (data.state)
                console.log("Unlike...");
        }, function (error) { }, function () { });
    };
    FrmVideoUserViewComponent.prototype.ClickLike = function (key, index) {
        console.log(JSON.stringify(key) + " key **");
        for (var i = 0; i < this._array.length; i++) {
            if (this.count == 0 && key == this._array[index].syskey) {
                this._array[index].n2 = this._array[index].n2 + 1;
                this.count = this.count + 1;
                this.goClickLike(key, index);
            }
            else if (this.count == 1 && key == this._array[index].syskey) {
                this._array[index].n2 = this._array[index].n2 - 1;
                this.count = 0;
                this.goClickLike(key, index);
            }
        }
        console.log(this._array[index].n2 + " && vidKey  " + key + " " + this.count);
    };
    FrmVideoUserViewComponent.prototype.goLikeCount = function (key, index) {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'serviceVideo/searchLikeCount?key=' + key).subscribe(function (response) {
            //console.log(JSON.stringify(response.state) + "  " + JSON.stringify(response));
            if (response.state)
                _this._obj = response.data;
            console.log("articleLike..." + _this._obj);
        }, function (error) { }, function () { });
    };
    FrmVideoUserViewComponent.prototype.goLike = function (key, type) {
        var _this = this;
        console.log(JSON.stringify(type) + " type");
        jQuery("#likeModal").modal();
        this.http.doGet(this.ics._apiurl + 'serviceVideo/searchLike?type=' + type + '&key=' + key).subscribe(function (response) {
            console.log(JSON.stringify(response) + " like user %%" + JSON.stringify(response.state));
            if (response.state)
                _this._likearr = response.data;
            console.log(JSON.stringify(_this._likearr) + " %%");
        }, function (error) { }, function () { });
    };
    FrmVideoUserViewComponent.prototype.goComment = function (key, index) {
        var _this = this;
        jQuery("#commentModal").modal();
        this._tmpObj._key = key;
        this._tmpObj._index = index;
        this._cmtArray = [];
        this.http.doGet(this.ics._apiurl + 'serviceVideo/getComments?id=' + key).subscribe(function (response) {
            if (response.state)
                _this._cmtArray = response.data;
        }, function (error) { }, function () { });
    };
    FrmVideoUserViewComponent.prototype.saveComment = function (cmtObj) {
        var _this = this;
        cmtObj.n1 = this._tmpObj._key;
        cmtObj.n5 = Number(this.ics._profile.t1);
        cmtObj.t12 = '';
        cmtObj.t13 = '';
        //console.log(JSON.stringify(cmtObj.videoUplad) + " vdo** ");
        for (var i = 0; i < cmtObj.videoUpload.length; i++) {
            console.log(JSON.stringify(cmtObj.videoUpload[i] + " vdo** "));
        }
        var url = this.ics._apiurl + 'serviceVideo/saveComments';
        var json = cmtObj;
        this.http.doPost(url, json).subscribe(function (response) {
            _this._cmtArray = [];
            if (response.state)
                _this._cmtArray = response.data;
            _this._array[_this._tmpObj._index].n3 = _this._array[_this._tmpObj._index].n3 + 1;
            _this._ansObj.t2 = "";
        }, function (error) {
            _this.showMessage("Can't Saved This Record!!!", undefined);
        }, function () { });
    };
    FrmVideoUserViewComponent.prototype.replyComment = function (key, num) {
        var _this = this;
        this._tmpObj._index = num;
        this._cmtArray[num].t13 = 'true';
        this._replyArray = [];
        this.http.doGet(this.ics._apiurl + 'serviceVideo/getComments?id=' + key).subscribe(function (response) {
            if (response.state)
                _this._replyArray = response.data;
        }, function (error) { }, function () { });
    };
    FrmVideoUserViewComponent.prototype.saveReply = function (skey) {
        var _this = this;
        this._replyObj.n1 = skey;
        this._replyObj.n3 = -1;
        this._replyObj.n5 = Number(this.ics._profile.t1);
        var url = this.ics._apiurl + 'serviceVideo/saveComment';
        var json = this._replyObj;
        this.http.doPost(url, json).subscribe(function (response) {
            _this._replyArray = [];
            if (response.state)
                _this._replyArray = response.data;
            _this._cmtArray[_this._tmpObj._index].n3 = _this._cmtArray[_this._tmpObj._index].n3 + 1;
            _this._replyObj.t2 = "";
        }, function (error) {
            _this.showMessage("Can't Saved This Record!!!", undefined);
        }, function () { });
    };
    FrmVideoUserViewComponent.prototype.editComment = function (obj, num) {
        this._cmtArray[num].t12 = 'true';
    };
    FrmVideoUserViewComponent.prototype.deleteComment = function (obj, num) {
        var _this = this;
        var url = this.ics._apiurl + 'serviceVideo/deleteComment';
        var json = obj;
        this.http.doPost(url, json).subscribe(function (response) {
            //this.showMessage(response.msgDesc, response.state);
            _this._cmtArray = [];
            if (response.state)
                _this._cmtArray = response.data;
        }, function (error) { }, function () { });
    };
    FrmVideoUserViewComponent.prototype.setImgUrl = function (str) {
        return 'upload/video/' + str;
    };
    FrmVideoUserViewComponent.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    FrmVideoUserViewComponent = __decorate([
        core_1.Component({
            selector: 'fmr-videoUserView',
            template: "\n    <div class=\"container\">\n    <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n    <form class=\"form-inline\" ngNoForm> \n    <legend>Video </legend>\n   <!-- <div class=\"input-group\">\n        <span class=\"input-group-btn input-md\">\n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"goNew();\">New</button>\n        </span>  \n        <input id=\"textinput\" name=\"textinput\" type=\"text\" placeholder=\"Search\" [(ngModel)]=\"_searchVal\" (keyup.enter)=\"searchVal()\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control input-md\">\n        <span class=\"input-group-btn input-md\">\n\t        <button class=\"btn btn-primary input-md\" type=\"button\" (click)=\"searchVal()\" >\n\t            <span class=\"glyphicon glyphicon-search\"></span>Search\n\t        </button>\n\t    </span>        \n\t</div> -->     \n    </form>\n    <pager id=\"pgarticle\" [(rpModel)]=\"_totalcount\" [(rpCurrent)]=\"_pager.current\" (rpChanged)=\"changedPager($event)\"></pager> \n    \n        <div  *ngFor=\"let obj of _array;let num = index\" class=\"col-md-12\">\n            <h4><a (click)=\"goto(obj.syskey)\">{{obj.t1}}</a></h4>\n            <span style=\"color:gray;font-style:italic;font-size:11px\">{{obj.userName}}</span>\n            <div class=\"form-group\">\n                <div *ngFor=\"let img of obj.videoUpload\">\n                   <!-- <img src={{setImgUrl(img)}} alt={{img}} height=\"240\" width=\"400\" /> -->\n                   <video controls  height=\"240\" width=\"400\">\n                        <source src=\"{{setImgUrl(img)}}\" type=\"video/mp4\" />\n               \n                    </video>\n                </div>\n            </div>\n            <p>{{obj.t2}}</p>\n            <table><tr>\n            <!-- <td><button class=\"btn btn-link\" type=\"button\" (click)=\"goClickLike(obj.syskey,num)\"><span class=\"glyphicon glyphicon-thumbs-up\"></span></button><span *ngIf=\"obj.n2!=0\">{{obj.n2}}</span></td> --> \n            <td><button class=\"btn btn-link\" type=\"button\" (click)=\"ClickLike(obj.syskey,num)\"><span class=\"glyphicon glyphicon-thumbs-up\"></span></button><span *ngIf=\"obj.n2!=0\"><a (click)=\"goLike(obj.syskey,obj.t3)\">{{obj.n2}}</a></span></td>\n            <!-- <td><button class=\"btn btn-link\" type=\"button\" (click)=\"goClickUnlike(obj.syskey,num)\"><span class=\"glyphicon glyphicon-thumbs-down\"></span></button><span *ngIf=\"obj.n4!=0\">{{obj.n4}}</span></td>-->\n            <td><button class=\"btn btn-link\" type=\"button\" (click)=\"goComment(obj.syskey,num)\"><span class=\"glyphicon glyphicon-comment\"></span></button><span *ngIf=\"obj.n3 > 0 \">{{obj.n3}}</span></td> \n            \n            </tr></table>\n            <hr>\n        </div>\n\n    </div>\n    </div>\n    </div>\n\n    <!-- Modal -->\n    <div id=\"commentModal\" class=\"modal fade\" role=\"dialog\">\n    <div class=\"modal-dialog modal-lg\">  \n        <div class=\"modal-content\">\n        <div class=\"modal-header\">\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n            <h4 class=\"modal-title\">Comments</h4>\n        </div>\n        <div id=\"jbody\" class=\"modal-body\" >\n            <div style=\"overflow: hidden; position: relative;\">                \n                <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n                    <div *ngFor=\"let obj of _cmtArray;let num2 = index\" class=\"col-md-12\">\n                        <div *ngIf=\"obj.t12!='true'\">\n                            <!-- <h4>{{obj.userName}}</h4>\n                            <p>{{obj.t2}}</p> -->\n\n                            <label class=\"col-md-2\"><img style='height: 50px; width: 50px;'  src='image/farmerpng.png'  ></label>\n                                <div class=\"col-md-7\" >\n                                     <h4>{{obj.userName}}</h4> \n                                        \n                                     <p>{{obj.t2}}</p>\n                                </div> \n                                <br>\n                        </div>\n\n\n                        <div *ngIf=\"obj.t12=='true'\">\n                            <!-- <h4>{{obj.userName}}</h4> -->\n                            <label class=\"col-md-2\"><img style='height: 50px; width: 50px;'  src='image/farmerpng.png'  ></label>\n                            <div class=\"col-md-7\">\n                                <textarea type=\"text\" [(ngModel)]=\"obj.t2\" class=\"form-control input-md\" rows=\"3\" ></textarea>\n                           </div>\n                           <br>\n                        </div>\n                        <button *ngIf=\"obj.t12=='true'\" class=\"btn btn-link\" type=\"button\" (click)=\"saveComment(obj)\">Save</button> \n                        <button *ngIf=\"obj.t12!='true' && obj.n5 == syskey\" class=\"btn btn-link\" type=\"button\" (click)=\"editComment(obj,num2)\">Edit</button>\n                        <button class=\"btn btn-link\" type=\"button\" (click)=\"replyComment(obj.syskey,num2)\">Reply</button>\n                        <button class=\"btn btn-link\" type=\"button\" (click)=\"deleteComment(obj,num2)\">Delete</button>\n                        <div *ngIf=\"obj.t13=='true'\">\n                        <div *ngFor=\"let replyObj of _replyArray\">\n                            <!-- <h5>{{replyObj.userName}}</h5>\n                            <p>{{replyObj.t2}}</p> -->\n\n                            <div class=\"form-group col-md-12\">\n                                     <label class=\"col-md-2\"></label>\n                                    <label class=\"col-md-2\"><img style='height: 50px; width: 50px;'  src='image/farmerpng.png'  ></label>\n                                    <div class=\"col-md-8\" >\n                                        <h4>{{replyObj.userName}}</h4>                         \n                                        <p>{{replyObj.t2}}</p>\n                                    </div>\n                                      \n                            </div> \n                        </div>\n\n                        <div class=\"form-group col-md-12\">\n                            <!-- <label class=\"col-md-2\">{{ics._profile.userName}}</label>-->\n                             <label class=\"col-md-2\"></label>\n                            <div class=\"col-md-8\">\n                                <textarea type=\"text\" [(ngModel)]=\"_replyObj.t2\" class=\"form-control input-md\" rows=\"3\" ></textarea>\n                            </div>\n                            <div class=\"col-md-2\"> \n                                <button class=\"btn btn-primary\" type=\"button\" (click)=\"saveReply(obj.syskey)\">Reply</button>  \n                            </div>\n                        </div>\n                        </div>\n                        <hr>\n                    </div>\n                    <div class=\"form-group col-md-12\">\n                        <!-- <label class=\"col-md-2\">{{ics._profile.userName}}</label> -->\n                        <label class=\"col-md-2\"><img style='height: 50px; width: 50px;'  src='image/farmerpng.png'  ></label>\n                        <div class=\"col-md-8\">\n                            <textarea type=\"text\" [(ngModel)]=\"_ansObj.t2\" class=\"form-control input-md\" rows=\"3\" ></textarea>\n                        </div>\n                        <div class=\"col-md-2\"> \n                            <button class=\"btn btn-primary\" type=\"button\" (click)=\"saveComment(_ansObj)\">Save</button>  \n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"modal-footer\">\n            <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\" (click)=\"clearReply()\">Close</button>\n        </div>\n        </div>\n    </div>\n    </div>\n\n     <!-- Modal -->\n    <div id=\"likeModal\" class=\"modal fade\" role=\"dialog\">\n    <div class=\"modal-dialog modal-md\">  \n        <div class=\"modal-content\">\n        <div class=\"modal-header\">\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>           \n            <div class=\"form-group col-md-12\">\n                <label class=\"col-md-4\"></label>\n                    <div class=\"col-md-6\" >\n                        <h4 class=\"modal-title\">People who like this!</h4>\n                    </div> \n                    <div class=\"col-md-2\"></div>\n            </div>\n        </div>\n        <div id=\"jbody\" class=\"modal-body\" >\n            <div style=\"overflow: hidden; position: relative;\">                \n                <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n                    \n                    <div class=\"form-group col-md-12\">\n                        <!-- <label class=\"col-md-6\"></label> -->\n                        <div class=\"col-md-12\" *ngFor=\"let obj of _likearr\">\n                             <label class=\"col-md-12\">{{obj.t2}}</label>\n                             \n                        </div> \n                        <div class=\"col-md-0\"></div>\n                    </div>                 \n                   \n                </div>\n            </div>\n        </div>\n        <div class=\"modal-footer\">\n            <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\" (click)=\"clearReply()\">Close</button>\n        </div>\n        </div>\n    </div>\n    </div>\n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_http_service_1.RpHttpService])
    ], FrmVideoUserViewComponent);
    return FrmVideoUserViewComponent;
}());
exports.FrmVideoUserViewComponent = FrmVideoUserViewComponent;
//# sourceMappingURL=frmVideoUserView.component.js.map