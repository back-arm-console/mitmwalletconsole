"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var FrmScheduleEntryComponent = (function () {
    function FrmScheduleEntryComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._returnResult = { "keyResult": 0, "longResult": "", "state": "", "stringResult": "", "userId": "", "msgCode": "", "msgDesc": "" };
        this._excelobj = {
            "arr": [{
                    "lnNo": "", "merchantID": "", "scheduleName": "", "meterID": "", "survey": "", "billNo": "", "lastUnit": 0, "thisUnit": 0, "totalUnit": 0, "rate": 0, "conservationFee": 0,
                    "amount": 0, "lastDate": "", "status": "", "remark": "", "errdesc": "", "messagedesc": ""
                }], "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0
        };
        this._pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 0 };
        this._sessionMsg = "";
        this._obj = { "syskey": 0, "schedulename": "" };
        this._resultobj = { "msgCode": "", "msgDesc": "", "key": "", "keyLong": 0 };
        this._key = "";
        this._sheetname = "";
        this._excelfilename = "";
        this._importUrl = "about:blank";
        this._mflag = true;
        this._excelhide = true;
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
    }
    FrmScheduleEntryComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "new") {
                _this.goClear();
            }
        });
    };
    FrmScheduleEntryComponent.prototype.goList = function () {
        //this._router.navigate(['Schedule Entry List', , { cmd: "LIST" }]);
        this._router.navigate(['/Schedule Entry List']);
    };
    FrmScheduleEntryComponent.prototype.clearData = function () {
        this._obj = { "syskey": 0, "schedulename": "" };
        this._resultobj = { "msgCode": "", "msgDesc": "", "key": "", "keyLong": 0 };
        this._key = "";
        jQuery("#myBtnDelete").prop("disabled", true);
        this._excelfilename = "";
        this._excelhide = true;
        this._excelobj = {
            "arr": [{
                    "lnNo": "", "merchantID": "", "scheduleName": "", "meterID": "", "survey": "", "billNo": "", "lastUnit": 0, "thisUnit": 0, "totalUnit": 0, "rate": 0, "conservationFee": 0,
                    "amount": 0, "lastDate": "", "status": "", "remark": "", "errdesc": "", "messagedesc": ""
                }], "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0
        };
    };
    FrmScheduleEntryComponent.prototype.popupMessage = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", t2: "Schedule Entry", t3: msg });
    };
    FrmScheduleEntryComponent.prototype.goEnable = function () {
        jQuery("#myBtnDelete").prop("disabled", false);
    };
    FrmScheduleEntryComponent.prototype.goReadOnly = function () {
        var Code = document.getElementById('schedulename');
        Code.readOnly = true;
    };
    FrmScheduleEntryComponent.prototype.goReadOnlyFalse = function () {
        var Code = document.getElementById('schedulename');
        Code.readOnly = false;
    };
    FrmScheduleEntryComponent.prototype.FileChange = function (event) {
        if (event.target.files.length > 0) {
            this._excelfilename = event.target.files[0].name;
            var url = this.ics._apiurl + 'service001/uploadExcelFile?fn=' + this._excelfilename;
            var fd = new FormData();
            var xhr = new XMLHttpRequest();
            for (var i = 0; i < event.target.files.length; i++) {
                fd.append("uploads[]", event.target.files[i], event.target.files[i].name);
            }
            xhr.open('POST', url, true);
            xhr.send(fd);
            this.getSheetName();
        }
        else {
            var temp = this._obj.schedulename;
            this.goClear();
            this._obj.schedulename = temp;
        }
    };
    FrmScheduleEntryComponent.prototype.goPreview = function () {
        var _this = this;
        this._mflag = false;
        if (this._excelfilename != "" && this._excelfilename != null) {
            var url = this.ics._apiurl + 'service001/readExcel?fn=' + this._excelfilename + '&sn=' + this._sheetname;
            var json = this._excelobj;
            this.http.doPost(url, json).subscribe(function (data) {
                _this._mflag = true;
                if (data.msgCode == '0014') {
                    _this._excelhide = true;
                    _this.popupMessage(data.msgDesc);
                }
                else {
                    _this._excelobj = data;
                    if (data != null) {
                        if (!(data.arr instanceof Array)) {
                            var m = [];
                            m[0] = data.arr;
                            _this._excelobj.arr = m;
                        }
                        else {
                            _this._excelobj = data;
                        }
                        _this._excelhide = false;
                    }
                }
            }, function (error) { return alert(error); }, function () { });
        }
        else {
            this._mflag = true;
            this.popupMessage("No file is selected to preview.");
        }
    };
    FrmScheduleEntryComponent.prototype.goImport = function () {
        var _this = this;
        this._mflag = false;
        if (this._excelfilename != "" && this._excelfilename != null) {
            var url = this.ics._apiurl + 'service001/importExcel?fn=' + this._excelfilename + '&sn=' + this._sheetname + '&userId=' + this.ics._profile.userID + '&scheduleName=' + this._obj.schedulename;
            var json = this._obj;
            this.http.doPost(url, json).subscribe(function (data) {
                _this._mflag = true;
                _this._returnResult = data;
                // this.messagealert();
                _this.popupMessage(_this._returnResult.msgDesc);
            }, function (error) { return alert(error); }, function () { });
            this.goReadOnlyFalse();
        }
        else {
            this._mflag = true;
            this.popupMessage("No file is selected to import.");
        }
    };
    FrmScheduleEntryComponent.prototype.messagealert = function () {
        var _this = this;
        this.messagehide = false;
        setTimeout(function () { return _this.messagehide = true; }, 3000);
    };
    FrmScheduleEntryComponent.prototype.goClear = function () {
        jQuery("#filename").val('');
        this._excelfilename = "";
        this._obj.schedulename = "";
        jQuery("#refsheetname").val('');
        this.ref._lov3.refsheetname = [{ "value": "", "caption": "" }];
        this._excelhide = true;
        this._excelobj = {
            "arr": [{
                    "lnNo": "", "merchantID": "", "scheduleName": "", "meterID": "", "survey": "", "billNo": "", "lastUnit": 0, "thisUnit": 0, "totalUnit": 0, "rate": 0, "conservationFee": 0,
                    "amount": 0, "lastDate": "", "status": "", "remark": "", "errdesc": "", "messagedesc": ""
                }], "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0
        };
    };
    FrmScheduleEntryComponent.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmScheduleEntryComponent.prototype.getSheetName = function () {
        var _this = this;
        this._mflag = false;
        var url = this.ics._apiurl + 'service001/getSheetName?fn=' + this._excelfilename;
        var json = null;
        this.http.doPost(url, json).subscribe(function (data) {
            _this._mflag = true;
            if (data != null) {
                if (data.msgCode == '0014') {
                    _this.showMsgAlert(data.msgDesc);
                }
                else {
                    if (!(data.refsheetname instanceof Array)) {
                        var m = [];
                        m[0] = data.refsheetname;
                        _this.ref._lov3.refsheetname = m;
                    }
                    else {
                        _this.ref._lov3.refsheetname = data.refsheetname;
                    }
                    _this._sheetname = _this.ref._lov3.refsheetname[0].value;
                }
            }
        }, function (error) { return alert(error); }, function () { });
    };
    FrmScheduleEntryComponent.prototype.goDownload = function () {
        //to download a file
        jQuery("#downloadAFile").html("<iframe src=" + this.ics._apiurl + "service001/downloadExcelTemplate></iframe>");
    };
    FrmScheduleEntryComponent.prototype.changedPager = function (event) {
        if (this._excelobj.totalCount != 0) {
            this._pgobj = event;
            var current = this._excelobj.currentPage;
            var size = this._excelobj.pageSize;
            this._excelobj.currentPage = this._pgobj.current;
            this._excelobj.pageSize = this._pgobj.size;
            if (this._pgobj.current != current || this._pgobj.size != size) {
                this.goPreview();
            }
        }
    };
    FrmScheduleEntryComponent = __decorate([
        core_1.Component({
            selector: 'FrmWaterBill',
            template: "\n    <div class=\"container\">\n        <div class=\"row clearfix\"> \n            <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n                <form class=\"form-horizontal\" (ngSubmit)=\"goImport()\" >\n                    <legend>Schedule Entry Import</legend>\n                    <div class=\"row  col-md-12\"> \n                        <button type=\"button\" class=\"btn btn-primary\" (click)=\"goList()\">List</button> \n                        <button type=\"button\" class=\"btn btn-primary\" (click)=\"goClear()\">Clear</button> \n                        <button type=\"button\" class=\"btn btn-primary\" (click)=\"goPreview()\">Preview</button> \n                        <button type=\"submit\" class=\"btn btn-primary\">Import</button>  \n                        <button type=\"button\" class=\"btn btn-primary\" (click)=\"goDownload()\">Download Template</button>                 \n                    </div>\n                    <div class=\"row col-md-12\">&nbsp;</div>\n                    <div class=\"row col-md-12\" id=\"custom-form-alignment-margin\">\n                        <div class=\"col-md-6\">\n                            <div class=\"form-group\">\n                                <rp-input rpClass=\"col-md-8\" rpLabelClass=\"col-md-4\" rpLabelRequired='true' rpType=\"text\" rpId=\"schedulename\" rpLabel=\"Schedule Name\" [(rpModel)]=\"_obj.schedulename\" rpRequired=\"true\"></rp-input>\n                            </div> \n\n                            <div class=\"form-group\">\n                                <label class=\"col-md-4 control-label\" for=\"filenameid\" style=\"margin: 5px 0px 5px 0px; text-align: left;\"> File Name <font size=\"4\" color=\"#FF0000\">*</font></label>\n                                <div class=\"col-md-8\" id=\"filenameid\">\n                                    <input type=\"file\" name=\"img[]\" class=\"file\" accept=\".xls\" (change)=\"FileChange($event)\" >\n                                    <div class=\"input-group\">\n                                        <input type=\"text\" id=\"filename\" class=\"form-control\" disabled placeholder=\"No file chosen\">\n                                        <span class=\"input-group-btn\">\n                                            <button class=\"browse btn btn-primary\" type=\"button\"><i class=\"glyphicon glyphicon-search\"></i> Browse</button>\n                                        </span>\n                                    </div>\n                                </div>\n                            </div>\n\n                            <div class=\"form-group\"> \n                                <rp-input rpClass=\"col-md-8\" rpLabelClass=\"col-md-4\" rpLabelRequired='true' rpType=\"refsheetname\" rpId=\"refsheetname\" rpLabel=\"Sheet Name\" [(rpModel)]=\"_sheetname\" rpRequired=\"true\"></rp-input>\n                            </div>\n                        </div>\n                    </div>\n                </form>\n            </div>\n        </div>\n    </div>\n\n    <div [hidden]=\"_excelhide\">\n        <div class=\"container\">\n            <div class=\"row clearfix\"> \n                <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n                    <adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"_excelobj.totalCount\" (rpChanged)=\"changedPager($event)\"></adminpager>\n                    <table class=\"table table-striped table-condense table-hover tblborder\" style=\"font-size: 14px;\">\n                        <thead>\n                            <tr>\n                            <th> LnNo </th>\n                            <th> Merchant ID </th>\n                            <th> Meter ID </th>\n                            <th> Bill No </th>\n                            <th> Last Unit </th>\n                            <th> This Unit </th>\n                            <th> Total Unit </th>\n                            <th> Rate </th>\n                            <th> Conservation Fee </th>\n                            <th> Amount </th>\n                            <th> LastDate </th>\n                            <th> Status </th>\n                            <th> Remark </th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                            <tr *ngFor=\"let obj of _excelobj.arr\">\n                            <td>{{obj.lnNo}}</td>\n                            <td>{{obj.merchantID}}</td>\n                            <td>{{obj.meterID}}</td>\n                            <td>{{obj.billNo}}</td>\n                            <td>{{obj.lastUnit}}</td>\n                            <td>{{obj.thisUnit}}</td>\n                            <td>{{obj.totalUnit}}</td>\n                            <td>{{obj.rate}}</td>\n                            <td>{{obj.conservationFee}}</td>\n                            <td>{{obj.amount}}</td>\n                            <td>{{obj.lastDate}}</td>\n                            <td *ngIf = \"obj.status == 0\">Active</td>\n                            <td *ngIf = \"obj.status != 0\">Active</td>\n                            <td>{{obj.remark}}</td>\n                            </tr> \n                        </tbody>                        \n                    </table>   \n                </div>\n            </div>\n        </div>\n    </div>\n\n    <!-- to download a file -->\n    <div id=\"downloadAFile\" style=\"display:none; width:0px; height:0px;\"></div>\n\n    <div [hidden] = \"_mflag\">\n        <div class=\"modal\" id=\"loader\"></div>\n    </div>                    \n    ",
            styles: ["\n    .file {\n        visibility: hidden;\n        position: absolute;\n      }\n  "],
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmScheduleEntryComponent);
    return FrmScheduleEntryComponent;
}());
exports.FrmScheduleEntryComponent = FrmScheduleEntryComponent;
//# sourceMappingURL=frmscheduleentry.component.js.map