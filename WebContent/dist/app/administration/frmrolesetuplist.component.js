"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var Observable_1 = require('rxjs/Observable');
var core_2 = require('@angular/core');
// RP Framework
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_client_util_1 = require('../util/rp-client.util');
core_2.enableProdMode();
var FrmRoleSetupListComponent = (function () {
    function FrmRoleSetupListComponent(ics, _router, http) {
        this.ics = ics;
        this._router = _router;
        this.http = http;
        // Application Specific
        this._searchVal = ""; // simple search
        this._flagas = true; // flag advance search
        this._col_list = [{ "col_no": "1", "flag": false }, { "col_no": "2", "flag": false }];
        this._sorting = { _sort_type: "asc", _sort_col: "1" };
        this._mflag = true;
        this._util = new rp_client_util_1.ClientUtil();
        this.sessionAlertMsg = "";
        this._roleobj = { "data": [{ "syskey": 0, "autokey": 0, "createdDate": "", "modifiedDate": "", "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "usersyskey": 0, "t1": "", "t2": "", "t3": "", "n1": 0, "n2": 0, "n3": 0 }], "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0, "msgCode": "", "msgDesc": "" };
        this._pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 0 };
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = false;
            this.search();
        }
    }
    // list sorting part
    FrmRoleSetupListComponent.prototype.changeDefault = function () {
        for (var i = 0; i < this._col_list.length; i++) {
            this._col_list[i].flag = false;
        }
    };
    FrmRoleSetupListComponent.prototype.addSort = function (e) {
        for (var i = 0; i < this._col_list.length; i++) {
            if (this._col_list[i].col_no == e) {
                var _tmp_flag = this._col_list[i].flag;
                this.changeDefault();
                this._col_list[i].flag = !_tmp_flag;
                this._sorting._sort_type = !_tmp_flag ? "desc" : "asc";
                this._sorting._sort_col = e;
                break;
            }
        }
    };
    FrmRoleSetupListComponent.prototype.changedPager = function (event) {
        if (this._roleobj.totalCount != 0) {
            this._pgobj = event;
            var current = this._roleobj.currentPage;
            var size = this._roleobj.pageSize;
            this._roleobj.currentPage = this._pgobj.current;
            this._roleobj.pageSize = this._pgobj.size;
            if (this._pgobj.current != current || this._pgobj.size != size) {
                this.search();
            }
        }
    };
    FrmRoleSetupListComponent.prototype.search = function () {
        var _this = this;
        try {
            this._mflag = false;
            this.http.doGet(this.ics._apiurl + 'service001/getRoleList?searchVal=' + encodeURIComponent(this._roleobj.searchText) + '&pagesize=' + this._roleobj.pageSize + '&currentpage=' + this._roleobj.currentPage + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (response) {
                if (response.msgCode == '0016') {
                    _this.sessionAlertMsg = response.msgDesc;
                    _this.showMessage();
                }
                else {
                    if (response.totalCount == 0) {
                        _this._roleobj = response,
                            _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Data not found!" });
                    }
                    else if (response.totalCount == 1) {
                        var m = [];
                        m[0] = response.data;
                        _this._roleobj.data = m[0];
                        _this._roleobj.totalCount = response.totalCount;
                    }
                    else {
                        _this._roleobj = response;
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmRoleSetupListComponent.prototype.searchKeyup = function (e) {
        if (e.which == 13) {
            this._roleobj.currentPage = 1;
            this._msghide = true;
            this._recordhide = false;
            this.search();
        }
    };
    FrmRoleSetupListComponent.prototype.Searching = function () {
        this._roleobj.currentPage = 1;
        this._msghide = true;
        this._recordhide = false;
        this.search();
    };
    FrmRoleSetupListComponent.prototype.goNew = function () {
        this._router.navigate(['/Role', 'new']);
    };
    FrmRoleSetupListComponent.prototype.goto = function (p) {
        this._router.navigate(['/Role', 'read', p]);
    };
    FrmRoleSetupListComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Observable_1.Observable.timer(3000).subscribe(function (x) {
            _this.goLogOut();
        });
    };
    FrmRoleSetupListComponent.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    FrmRoleSetupListComponent = __decorate([
        core_1.Component({
            selector: 'rolesetup-list',
            template: " \n  <div class=\"container col-md-12\">\n    <form class=\"form-inline\"> \n    \n    <!-- Form Name -->\n    <legend>Role List</legend>\n      <div class=\"input-group\">\n        <span class=\"input-group-btn input-md\">\n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"goNew();\">New</button>\n        </span>\n        <input id=\"textinput\" name=\"textinput\" type=\"text\" placeholder=\"Search\" [(ngModel)]=\"_roleobj.searchText\" (keyup)=\"searchKeyup($event)\" maxlength=\"50\" class=\"form-control input-md\">\n        <span class=\"input-group-btn input-md\">\n\t        <button class=\"btn btn-primary input-md\" type=\"button\" (click)=\"Searching()\" >\n\t          <span class=\"glyphicon glyphicon-search\"></span>Search\n\t        </button>\n\t      </span>        \n\t    </div>      \n    </form>\n    \n   <div style = \"margin-top : 10px\">\n   <adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"_roleobj.totalCount\" (rpChanged)=\"changedPager($event)\" ></adminpager> \n  </div> \n  <table class=\"table table-striped table-condensed table-hover tblborder\" style=\"font-size:14px;\">\n    <thead>\n     <tr>\n        <th>Code</th>\n        <th>Description</th>   \n    </tr>\n  </thead>\n  <tbody>\n    <tr *ngFor=\"let obj of _roleobj.data\">\n        <td><a (click)=\"goto(obj.syskey)\">{{obj.t1}}</a></td>\n        <td>{{obj.t2}}</td>\n    </tr>  \n  </tbody>\n  </table>\n  </div> \n  \n    <div [hidden] = \"_mflag\">\n        <div class=\"modal\" id=\"loader\"></div>\n    </div>\n   "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_http_service_1.RpHttpService])
    ], FrmRoleSetupListComponent);
    return FrmRoleSetupListComponent;
}());
exports.FrmRoleSetupListComponent = FrmRoleSetupListComponent;
//# sourceMappingURL=frmrolesetuplist.component.js.map