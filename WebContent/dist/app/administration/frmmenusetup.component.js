"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var Rx_1 = require('rxjs/Rx');
core_2.enableProdMode();
var FrmMenuSetupComponent = (function () {
    function FrmMenuSetupComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this.sessionAlertMsg = "";
        this._obj = this.getDefaultObj();
        this._before = this._obj;
        this._result = { "longResult": "", "msgCode": "", "msgDesc": "", "key": [], "keyResult": 0, "state": "", "stringResult": "" };
        this._keyobj = { "_key": "", "isParent": "", "sessionId": "", "userId": "" };
        this._key = "";
        this._output1 = "";
        this._mflag = false;
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = false;
            this.checkSession();
            this.getmainlist();
            this.chkmt = false;
            this._obj = this.getDefaultObj();
        }
    }
    FrmMenuSetupComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this._key = id;
                _this.goReadBySyskey(id);
            }
        });
    };
    FrmMenuSetupComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    FrmMenuSetupComponent.prototype.getDefaultObj = function () {
        //Combo BUtton Control
        this.rd = true;
        //Menu Item Control
        this.chkmt = false;
        //Radio Box Control
        this.parentcheck = true;
        this.childcheck = false;
        jQuery("#mydelete").prop("disabled", true);
        jQuery("#mySave").prop("disabled", false);
        return { "syskey": null, "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "t6": "", "n2": 0, "n5": 0, "n1": 0, "userId": "", "userName": "", "msgCode": "", "msgDesc": "", "sessionId": "" };
    };
    FrmMenuSetupComponent.prototype.goReadBySyskey = function (p) {
        var _this = this;
        try {
            this.chkmt = true;
            var url = this.ics._apiurl + 'service001/getMenuData?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
            this.http.doPost(url, p).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMessage();
                }
                else {
                    jQuery("#mydelete").prop("disabled", false);
                    _this._obj = data;
                    _this.menusys = _this._obj.n2;
                    if (_this._obj.n2 == 0) {
                        _this.parentcheck = true;
                        _this.childcheck = false;
                    }
                    else {
                        _this.parentcheck = false;
                        _this.childcheck = true;
                        _this.getmainlist();
                        _this.rd = false;
                    }
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmMenuSetupComponent.prototype.goSave = function () {
        var _this = this;
        if ((this.rd == false && this.menusys == 0) || (this.rd == false && this.menusys == undefined)) {
            this._result.msgDesc = "Please Select Main Menu";
            this._result.state = "false";
            this.showMessageAlert(this._result.msgDesc);
        }
        if ((this.rd == false && this._obj.n1 == 0) || (this.rd == false && this._obj.n1 == undefined)) {
            this._result.msgDesc = "Please Select Type";
            this.showMessageAlert(this._result.msgDesc);
        }
        else {
            this._obj.n2 = this.menusys;
            this._obj.n5 = 1;
            this._obj.t4 = "51";
            this._obj.t5 = "100%";
            this._obj.t6 = "800";
            this._obj.t1 = "/anglar2";
            this._obj.userId = this.ics._profile.userID;
            try {
                this._obj.sessionId = this.ics._profile.sessionID;
                var url = this.ics._apiurl + 'service001/saveMenu';
                var json = this._obj;
                this.http.doPost(url, json).subscribe(function (data) {
                    if (data.msgCode == '0016') {
                        _this.sessionAlertMsg = data.msgDesc;
                        _this.showMessage();
                    }
                    else {
                        _this._output1 = JSON.stringify(data);
                        _this._result = data;
                        _this.messagehide = true;
                        _this.showMessageAlert(_this._result.msgDesc);
                        _this._obj.syskey = _this._result.key[1];
                        if (_this._result.state) {
                            _this.chkmt = true;
                            if (_this.rd != false) {
                                _this.getmainlist();
                                _this._obj.syskey = _this._result.key[1];
                                if (_this.checkparam != false) {
                                    _this.checkparam = false;
                                }
                                else {
                                    jQuery("#mydelete").prop("disabled", true);
                                }
                            }
                        }
                        else {
                            _this.showMessageAlert(_this._result.msgDesc);
                        }
                    }
                }, function (error) {
                    if (error._body.type == 'error') {
                        alert("Connection Timed Out!");
                    }
                }, function () { });
            }
            catch (e) {
                alert("Invalid URL");
            }
        }
    };
    FrmMenuSetupComponent.prototype.goList = function () {
        this._router.navigate(['/menusetuplist']);
    };
    FrmMenuSetupComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this.goLogOut();
        });
    };
    FrmMenuSetupComponent.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    /* checkSession() {
      try {
  
        let url: string = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
        this.http.doGet(url).subscribe(
          data => {
            if (data.msgCode == '0016') {
              this.sessionAlertMsg = data.msgDesc;
              this.showMessage();
            }
          },
          error => {
            if (error._body.type == 'error') {
              alert("Connection Timed Out!");
            }
            else {
  
            }
          }, () => { });
      } catch (e) {
        alert("Invalid URL");
      }
    } */
    FrmMenuSetupComponent.prototype.getmainlist = function () {
        var _this = this;
        try {
            this._mflag = false;
            this.http.doGet(this.ics._apiurl + 'service001/getMainList').subscribe(function (data) {
                if (data != null || data != undefined) {
                    _this.ref._lov3.mainmenu = data;
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmMenuSetupComponent.prototype.checkChild = function () {
        this.rd = false;
        this._obj.syskey = null;
    };
    FrmMenuSetupComponent.prototype.checkParents = function () {
        this.rd = true;
        this._obj.n2 = 0;
        this._obj.syskey = null;
    };
    FrmMenuSetupComponent.prototype.goDelete = function () {
        var _this = this;
        try {
            this._mflag = false;
            var url = this.ics._apiurl + 'service001/deleteMenu';
            if (this.rd == true) {
                this._keyobj.isParent = "true";
            }
            else {
                this._keyobj.isParent = "false";
            }
            this._keyobj.sessionId = this.ics._profile.sessionID;
            this._keyobj.userId = this.ics._profile.userID;
            this._keyobj._key = this._key;
            var json = this._keyobj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMessage();
                }
                else {
                    _this._output1 = JSON.stringify(data);
                    _this._result = data;
                    _this.showMessageAlert(_this._result.msgDesc);
                    if (_this._result.state) {
                        _this.getmainlist();
                        if (_this.checkparam != false) {
                            _this.goNew();
                        }
                    }
                    else {
                        jQuery("#mydelete").prop("disabled", false);
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmMenuSetupComponent.prototype.showMessageAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmMenuSetupComponent.prototype.clearData = function () {
        this._obj = this.getDefaultObj();
        jQuery("#mydelete").prop("disabled", true);
        jQuery("#mySave").prop("disabled", false);
    };
    FrmMenuSetupComponent.prototype.goNew = function () {
        this.clearData();
        this.rd = true;
        this.chkmt = false;
        jQuery('#parentradio').prop('checked', 'checked');
        jQuery('#childradio').prop('checked', false);
    };
    FrmMenuSetupComponent.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    FrmMenuSetupComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsgAlert(data.desc);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsgAlert(data.desc);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmMenuSetupComponent.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmMenuSetupComponent.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    FrmMenuSetupComponent = __decorate([
        core_1.Component({
            selector: 'menu-setup',
            template: "\n    <div class=\"container\">\n    <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n    <form class= \"form-horizontal\" (ngSubmit) = \"goSave()\"> \n    <!-- Form Name -->\n    <legend>Menu </legend>\n    <div class=\"row  col-md-12\">  \n      <button class=\"btn btn-primary\" type=\"button\" (click)=\"goList()\" >List</button> \n      <button class=\"btn btn-primary\" type=\"button\" (click)=\"goNew()\" >New</button>      \n      <button class=\"btn btn-primary\" id=\"mySave\" type=\"submit\" >Save</button>          \n      <button class=\"btn btn-primary\" disabled id=\"mydelete\"  type=\"button\" (click)=\"goDelete();\" >Delete</button> \n    </div>\n    <div class=\"row col-md-12\">&nbsp;</div> \n\n    <div class=\"form-group\">    \n    <div class=\"col-md-10\">\n\n    <div class=\"form-group\">    \n      <label class=\"checkbox-inline\">    \n        <input type=\"radio\" [checked]=\"parentcheck\" id=\"parentradio\"  (click)=\"checkParents()\" name=\"optionsRadiosinline\"   value=\"option1\" checked> \n        Main Menu \n      </label>  \n      \n      <label class=\"checkbox-inline\">      \n        <input type=\"radio\" [checked]=\"childcheck\" id=\"childradio\" (click)=\"checkChild()\"   name=\"optionsRadiosinline\"   value=\"option2\"> \n        Sub Menu    \n      </label>  \n    </div>\n\n\n    <div class=\"form-group\">\n      <label class=\"col-md-3\" [hidden]=\"rd\"> Main Menu <font class=\"mandatoryfont\" >*</font></label>\n      <div [hidden]=\"rd\">\n            <rp-input  rpLabelClass=\"col-md-0\" [(rpModel)]=\"menusys\" rpRequired =\"true\"  rpType=\"mainmenu\" rpClass=\"col-md-4\"></rp-input>\n      </div>              \n    </div>        \n          \n  <div class=\"form-group\">\n        <rp-input  rpType=\"text\" rpClass=\"col-md-2\" rpLabel=\"Code\" [(rpModel)]=\"_obj.syskey\" rpReadonly=\"true\" rpRequired=\"true\"></rp-input>\n  </div>\n\n  <div class=\"form-group\">        \n    <rp-input  rpType=\"text\" rpLabel=\"Menu Item\" *ngIf=\"chkmt==false\" [(rpModel)]=\"_obj.t3\" rpRequired=\"true\"></rp-input>\n    <rp-input  rpType=\"text\" rpLabel=\"Menu Item\" *ngIf=\"chkmt\" [(rpModel)]=\"_obj.t3\" rpReadonly=\"{{rd}}\" rpRequired=\"true\"></rp-input>\n  </div>\n  \n  <div class=\"form-group\">        \n    <rp-input  rpType=\"text\" rpLabel=\"Description\" [(rpModel)]=\"_obj.t2\" rpRequired=\"true\"></rp-input>\n  </div>\n\n  <div class=\"form-group\">\n      <label class=\"col-md-3\" [hidden]=\"rd\"> Type <font size=\"4\" color=\"#FF0000\" >*</font></label>\n      <div class=\"col-md-4\" [hidden]=\"rd\">\n          <select [(ngModel)]=\"_obj.n1\" class=\"form-control col-md-0\" tabindex=\"2\" [ngModelOptions]=\"{standalone: true}\">\n              <option  *ngFor=\"let item of ref._lov1.refmenu\" value=\"{{item.value}}\">{{item.caption}}</option>\n          </select>\n     </div> \n  </div> \n\n    </div>\n    </div>\n\n    </form>\n    </div>\n    </div>\n    </div>\n       \n    <div id=\"sessionalert\" class=\"modal fade\">\n      <div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n          <div class=\"modal-body\">\n            <p>{{sessionAlertMsg}}</p>\n          </div>\n          <div class=\"modal-footer\">\n          </div>\n        </div>\n      </div>\n    </div>\n\n<div [hidden] = \"_mflag\">\n  <div class=\"modal\" id=\"loader\"></div>\n</div>\n    \n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmMenuSetupComponent);
    return FrmMenuSetupComponent;
}());
exports.FrmMenuSetupComponent = FrmMenuSetupComponent;
//# sourceMappingURL=frmmenusetup.component.js.map