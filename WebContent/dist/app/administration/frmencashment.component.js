"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var Rx_1 = require('rxjs/Rx');
core_2.enableProdMode();
var FrmEncashmentComponent = (function () {
    function FrmEncashmentComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._output1 = "";
        this.sessionAlertMsg = "";
        this._remittedDate = "";
        this._reqDate = "";
        this._key = "";
        this._mflag = true;
        this._util = new rp_client_util_1.ClientUtil();
        this.myDatePickerOptions = this._util.getDatePicker();
        this._dates = { "reqDate": this.dateobj, "remittedDate": this.dateobj };
        this.today = this._util.getTodayDate();
        this._accinfo = {
            "accArr": [{ "srno": "0", "branchCode": "", "name": "", "nrc": "", "accNumber": "", "currencyCode": "", "currentBalance": "", "lastTransDate": "", "productType": "", "accountType": "", "zone": "", "status": "" }],
            "glArr": [{ "srno": "0", "accNo": "", "accdesc": "", "subGroup": "", "subGroupDesc": "", "accTypeDescription": "" }]
        };
        this._SearchData = { "searchcaption": "", "searchvalue": "1", "search": "0" };
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "loginID": "", "phNo": "", "parentID": "" };
        this._returnAppResult = { "bankRefNumber": "", "bankTaxRefNumber": "", "cardExpire": "", "code": "", "desc": "Agent transfered successfully", "transactionDate": "" };
        this._encashobj = {
            "syskey": 0, "userID": "", "sessionID": "", "fromAccount": "", "fromName": "",
            "toAccount": "", "toName": "", "amount": "", "bankCharges": 0, "narrative": "",
            "refNo": "", "draweeName": "", "draweeNRC": "", "draweePhone": "",
            "payeeName": "", "payeeNRC": "", "payeePhone": "", "reqDate": "", "remittedDate": "", "field1": "3", "field2": "E",
            "branchCode": "", "accountNRC": "", "accountPhone": "",
        };
        this.strongRegex = new RegExp("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%]).{4,20})");
        this.mediumRegex = new RegExp("^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{5,}$");
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._recordhide = false;
            this._msghide = true;
            this._mflag = false;
            this.checkSession();
            this._dates.remittedDate = this._util.changestringtodateobject(this.today);
            this._dates.reqDate = this._util.changestringtodateobject(this.today);
            this.ics.confirmUpload(false);
            this.getAllBranch();
            this._mflag = true;
        }
    }
    FrmEncashmentComponent.prototype.showMessageAlert = function (msg) {
        this._mflag = true;
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmEncashmentComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this._key = id;
                _this.goReadBySyskey(id);
            }
        });
    };
    FrmEncashmentComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    FrmEncashmentComponent.prototype.goReadBySyskey = function (p) {
        var _this = this;
        try {
            this._mflag = false;
            this._key = p;
            this.http.doGet(this.ics._apiurl + 'service001/getAgentTransferDataByRefNo?id=' + this._key + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMessage();
                }
                else {
                    _this._dates.reqDate = _this._util.changestringtodateobject(_this.today);
                    _this._encashobj = data,
                        _this._dates.remittedDate = _this._util.changestringtodateobject(_this._encashobj.remittedDate);
                    _this._encashobj.field1 = "3";
                    _this._encashobj.field2 = "E";
                    _this._encashobj.amount = _this.formatMoney(_this._encashobj.amount);
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Error!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmEncashmentComponent.prototype.formatMoney = function (n) {
        return (+n).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
    };
    FrmEncashmentComponent.prototype.date = function () {
        this._encashobj.remittedDate = this._util.getDatePickerDate(this._dates.remittedDate);
        this._encashobj.reqDate = this._util.getDatePickerDate(this._dates.reqDate);
    };
    FrmEncashmentComponent.prototype.validation = function () {
        if (this._encashobj.refNo == "" || this._encashobj.refNo == null) {
            this.showMessageAlert("Ref No. is Required");
            return false;
        }
        if (this._encashobj.toAccount == "" || this._encashobj.toAccount == null) {
            if (this._encashobj.field1 == '2') {
                this.showMessageAlert("Account is Required");
            }
            else if (this._encashobj.field1 == '3') {
                this.showMessageAlert("GL Account is Required");
            }
            return false;
        }
        if (this._encashobj.toName == "" || this._encashobj.toName == null) {
            this.showMessageAlert("Name is Required");
            return false;
        }
        if (this._encashobj.branchCode == "" || this._encashobj.branchCode == null) {
            this.showMessageAlert("Please Select Branch Code");
            return false;
        }
        if (this._encashobj.accountNRC.length > 0) {
            if (!/^(([1-9]|1[0-9])\/[A-Za-z]+\([N|n]\)[0-9]{6})$/.test(this._encashobj.accountNRC)) {
                this.showMessageAlert("Invalid NRC No.");
                return false;
            }
        }
        if (this._encashobj.accountPhone.length > 0) {
            if (!/^([+0-9]{7,20})$/.test(this._encashobj.accountPhone)) {
                this.showMessageAlert("Phone No. is invalid.");
                return false;
            }
        }
        return true;
    };
    FrmEncashmentComponent.prototype.goPost = function () {
        var _this = this;
        this._mflag = false;
        this.date();
        this._returnAppResult = { "bankRefNumber": "", "bankTaxRefNumber": "", "cardExpire": "", "code": "", "desc": "", "transactionDate": "" };
        if (this.validation()) {
            try {
                this._encashobj.userID = this.ics._profile.userID;
                this._encashobj.sessionID = this.ics._profile.sessionID;
                this._encashobj.amount = this._encashobj.amount.replace(",", "");
                var url = this.ics._appserviceurl + 'service001/goAgentTransferAdmin';
                var json = this._encashobj;
                this.http.doPost(url, json).subscribe(function (data) {
                    if (data.code == '0016') {
                        _this.sessionAlertMsg = data.desc;
                        _this.showMessage();
                    }
                    else {
                        _this._output1 = JSON.stringify(data);
                        _this._returnAppResult = data;
                        _this.showMessageAlert(_this._returnAppResult.desc);
                    }
                    _this._mflag = true;
                }, function (error) {
                    if (error._body.type == 'error') {
                        alert("Connection Error!");
                    }
                    _this._mflag = true;
                }, function () { });
            }
            catch (e) {
                alert("Invalid URL");
            }
        }
    };
    FrmEncashmentComponent.prototype.clearAccountInformation = function (field) {
        if (this._encashobj.field1 != field) {
            this._encashobj.toName = "";
            this._encashobj.toAccount = "";
            this._encashobj.accountNRC = "";
            this._encashobj.accountPhone = "";
            this._encashobj.branchCode = "";
        }
    };
    FrmEncashmentComponent.prototype.goList = function () {
        this._router.navigate(['/EncashmentList']);
    };
    FrmEncashmentComponent.prototype.goClear = function () {
        this.clearData();
    };
    FrmEncashmentComponent.prototype.clearData = function () {
        this._encashobj.userID = "";
        this._encashobj.fromAccount = "";
        this._encashobj.fromName = "";
        this._encashobj.toAccount = "";
        this._encashobj.toName = "";
        this._encashobj.bankCharges = 0;
        this._encashobj.narrative = "";
        this._encashobj.refNo = "";
        this._encashobj.draweeName = "";
        this._encashobj.draweeNRC = "";
        this._encashobj.draweePhone = "";
        this._encashobj.payeeName = "";
        this._encashobj.payeeNRC = "";
        this._encashobj.payeePhone = "";
        this._dates.remittedDate = this._util.changestringtodateobject(this.today);
        this._dates.reqDate = this._util.changestringtodateobject(this.today);
        this._encashobj.amount = "";
        this._encashobj.field1 = "3";
        this._encashobj.field2 = "E";
        this._encashobj.branchCode = "";
    };
    FrmEncashmentComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this._router.navigate(['Login', , { p1: '*' }]);
            jQuery("#sessionalert").modal('hide');
        });
    };
    /* checkSession() {
        try {
            let url: string = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
            this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "loginID": "", "phNo": "", "parentID": "" };
            this.http.doGet(url).subscribe(
                data => {
                    if (data.msgCode == '0016') {
                        this._returnResult.msgDesc = data.msgDesc;
                        this.showMessage();
                    }
                },
                error => {
                    if (error._body.type == 'error') {
                        alert("Connection Timed Out!");
                    }
                    else {

                    }
                }, () => { });
        } catch (e) {
            alert("Invalid URL");
        }

    } */
    //refaccount  refgl
    FrmEncashmentComponent.prototype.lookuppopup = function () {
        this._accinfo.accArr = [];
        this._SearchData = { "searchcaption": "", "searchvalue": "1", "search": "0" };
        this.hideselect = true;
        jQuery("#popup").modal();
    };
    FrmEncashmentComponent.prototype.goSearch = function (searchCaption, searchValue, search) {
        var _this = this;
        console.log("you click goSearch");
        this._mflag = false;
        this._accinfo.accArr = [];
        if (searchCaption == '' || searchValue == '' || search == '') {
            this._returnResult.msgDesc = "Please enter data to search..";
            this.showMessageAlert(this._returnResult.msgDesc);
        }
        else {
            try {
                var method = "";
                if (this._encashobj.field1 == '2') {
                    method = "checkAccount";
                }
                else if (this._encashobj.field1 == '3') {
                    method = "checkGL";
                }
                this.http.doGet(this.ics._apiurl + 'service001/' + method + '?searchCaption=' + searchCaption + '&searchValue=' + searchValue + '&searchWay=' + search).subscribe(function (data) {
                    var _selected;
                    if (_this._encashobj.field1 == '2') {
                        if (data.accArr != null) {
                            _this.hideselect = false;
                            if (!(data.accArr instanceof Array)) {
                                var m = [];
                                m[0] = data.accArr;
                                _this._accinfo.accArr = m;
                            }
                            else {
                                _this._accinfo = data;
                                _selected = _this._accinfo.accArr;
                            }
                        }
                        else {
                            _this._accinfo.accArr = [];
                            _this.showMessageAlert("Data not found");
                        }
                    }
                    else if (_this._encashobj.field1 == '3') {
                        _this._encashobj.branchCode = "";
                        if (data.glArr != null) {
                            _this.hideselect = false;
                            if (!(data.glArr instanceof Array)) {
                                var m = [];
                                m[0] = data.glArr;
                                _this._accinfo.glArr = m;
                            }
                            else {
                                _this._accinfo = data;
                                _selected = _this._accinfo.glArr;
                            }
                        }
                        else {
                            _this._accinfo.glArr = [];
                            _this.showMessageAlert("Data not found");
                        }
                    }
                    _this._mflag = true;
                }, function (error) {
                    if (error._body.type == 'error') {
                        alert("Connection Error!");
                    }
                    _this._mflag = true;
                }, function () { });
            }
            catch (e) {
                alert("Invalid URL");
                this._mflag = true;
            }
        }
    };
    FrmEncashmentComponent.prototype.gotobindAccount = function (arr) {
        if (this._encashobj.field1 == '2') {
            this._encashobj.toAccount = arr.accNumber;
            this._encashobj.toName = arr.name;
            this._encashobj.branchCode = arr.branchCode;
            this._encashobj.accountNRC = "";
            this._encashobj.accountPhone = "";
        }
        else if (this._encashobj.field1 == '3') {
            this._encashobj.toAccount = arr.accNo;
            this._encashobj.toName = arr.accdesc;
            this._encashobj.accountNRC = "";
            this._encashobj.accountPhone = "";
        }
        jQuery("#popup").modal('hide');
    };
    FrmEncashmentComponent.prototype.searchKeyup1 = function (e) {
        if (e.which == 13) {
            if (!(this._SearchData.searchcaption.length == 0)) {
                this.goSearch(this._SearchData.searchcaption, this._SearchData.searchvalue, this._SearchData.search);
            }
            else {
                this._returnResult.msgDesc = "Please enter data to search..";
                this.showMessageAlert(this._returnResult.msgDesc);
            }
        }
    };
    FrmEncashmentComponent.prototype.getAllBranch = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics._apiurl + 'service001/getAllBranch').subscribe(function (data) {
                if (data != null && data != undefined) {
                    _this.ref._lov3.ref018 = data.ref018;
                }
                else {
                    _this.ref._lov3.ref018 = [{ "value": "", "caption": "Empty" }];
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmEncashmentComponent.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    FrmEncashmentComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsgAlert(data.desc);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsgAlert(data.desc);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmEncashmentComponent.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmEncashmentComponent.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    FrmEncashmentComponent = __decorate([
        core_1.Component({
            selector: 'frmencashment',
            template: "\n<div class=\"container\">\n    <div class=\"row clearfix\">\n        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n            <form class= \"form-horizontal\"> \n                <!-- Form Name -->\n                <legend>Encash</legend>\n                <div class=\"row  col-md-12\">    \n                    <button class=\"btn btn-primary\" type=\"button\"  (click)=\"goList()\">List</button>\n                    <button class=\"btn btn-primary\" type=\"button\" (click)=\"goClear()\" > Clear </button>      \n                    <button class=\"btn btn-primary\" type=\"button\" (click)=\"goPost()\">Post</button>\n                </div>\n                <div class=\"form-group\">    \n                    <div class=\"col-md-12\" id=\"encash-align\">\n                        <div class=\"row col-md-12\">&nbsp;</div>\n                        <div  class=\"col-md-6\">\n                            <div class=\"form-group\">\n                                <rp-input [(rpModel)]=\"_encashobj.refNo\" rpRequired =\"true\" rpType=\"text\" rpLabelRequired='true' rpLabel=\" Ref No.\" rpReadonly=\"true\" rpClass=\"col-md-8 control-label\" rpLabelClass=\"col-md-4 \"  ></rp-input>\n                            </div>\n\n                            <div class=\"form-group\">\n                                <label class=\"col-md-4\" > Request Date <font class=\"mandatoryfont\" >*</font></label>\n                                <div class=\"col-md-8\">\n                                    <my-date-picker name=\"mydate\"  [options]=\"myDatePickerOptions\" [(ngModel)]=\"_dates.reqDate\" required=\"true\" [ngModelOptions]=\"{standalone: true}\" ngDefaultControl></my-date-picker>\n                                </div>\n                            </div> \n\n                            <div class=\"form-group\">\n                                <rp-input  rpType=\"text\"  rpLabel=\"Amount\" [(rpModel)]=\"_encashobj.amount\" rpLabelRequired='true' rpReadonly=\"true\" rpClass=\"col-md-8 control-label\" rpLabelClass=\"col-md-4 \" rpRequired=\"true\"  ></rp-input>\n                            </div>\n                        </div> \n        \n                        <div class=\"col-md-6\">\n                            <div class=\"form-group\">\n                                <rp-input  rpType=\"text\" rpLabel=\" From Agent\" [(rpModel)]=\"_encashobj.fromName\" rpLabelRequired='true' rpReadonly=\"true\" rpClass=\"col-md-8 control-label\" rpLabelClass=\"col-md-4 \" rpRequired=\"true\"  ></rp-input>\n                            </div>\n\n                            <div class=\"form-group\">\n                                <label class=\"col-md-4\" > Remitted Date <font class=\"mandatoryfont\" >*</font></label>\n                                <div class=\"col-md-8\">\n                                    <my-date-picker name=\"mydate\"  [options]=\"myDatePickerOptions\" [(ngModel)]=\"_dates.remittedDate\" required=\"true\" [ngModelOptions]=\"{standalone: true}\" ngDefaultControl></my-date-picker>\n                                </div>\n                            </div> \n                        </div>\n                    </div>\n\n                    <div class=\"col-md-12\" id=\"encash-align\">\n                        <div class=\"col-md-6\">\n                            <legend class=\"encash-legend\">Sender Information</legend>\n                            <div class=\"form-group\">\n                                <rp-input [(rpModel)]=\"_encashobj.draweeName\" rpRequired =\"true\" rpType=\"text\" rpLabelRequired='true' rpLabel=\" Name \" rpReadonly=\"true\" rpClass=\"col-md-8 control-label\" rpLabelClass=\"col-md-4 \"  ></rp-input>\n                            </div>\n                            <div class=\"form-group\">\n                                <rp-input [(rpModel)]=\"_encashobj.draweeNRC\" rpRequired =\"true\" rpType=\"text\" rpLabelRequired='true' rpLabel=\" NRC \" rpReadonly=\"true\" rpClass=\"col-md-8 control-label\" rpLabelClass=\"col-md-4 \"  ></rp-input>\n                            </div>\n                            <div class=\"form-group\">\n                                <rp-input [(rpModel)]=\"_encashobj.draweePhone\" rpRequired =\"true\" rpType=\"text\" rpLabelRequired='true' rpLabel=\" Phone No.\" rpReadonly=\"true\" rpClass=\"col-md-8 control-label\" rpLabelClass=\"col-md-4 \"  ></rp-input>\n                            </div>\n                        </div>\n\n                        <div class=\"col-md-6\">\n                            <legend class=\"encash-legend\">Receiver Information</legend>\n                            <div class=\"form-group\">\n                                <rp-input [(rpModel)]=\"_encashobj.payeeName\" rpRequired =\"true\" rpType=\"text\" rpLabelRequired='true' rpLabel=\"Name \" rpReadonly=\"true\" rpClass=\"col-md-8 control-label\" rpLabelClass=\"col-md-4 \"  ></rp-input>\n                            </div>\n                            <div class=\"form-group\">\n                                <rp-input [(rpModel)]=\"_encashobj.payeeNRC\" rpRequired =\"true\" rpType=\"text\" rpLabelRequired='true' rpLabel=\" NRC \" rpReadonly=\"true\" rpClass=\"col-md-8 control-label\" rpLabelClass=\"col-md-4 \"  ></rp-input>\n                            </div>\n                            <div class=\"form-group\">\n                                <rp-input [(rpModel)]=\"_encashobj.payeePhone\" rpRequired =\"true\" rpType=\"text\" rpLabelRequired='true' rpLabel=\" Phone No.\" rpReadonly=\"true\" rpClass=\"col-md-8 control-label\" rpLabelClass=\"col-md-4 \" ></rp-input>\n                            </div>\n                        </div>\n                    </div>\n\n                    <div class=\"col-md-12\" id=\"encash-align\">\n                        <div class=\"col-md-6\">\n                            <legend class=\"encash-legend\">Type</legend>\n                            <div class=\"col-md-12\">\n                                <div class=\"form-group\">\n                                    <div class=\"col-md-6 col-md-offset-4\">\n                                        <label class=\"radio-inline\"><input #c [checked]=\"_encashobj.field1 == c.value\" (click)=\"clearAccountInformation(3)\" (click)=\"_encashobj.field1 = c.value\" type=\"radio\" name=\"_encashobj.field1\" value=\"3\" checked >By GL </label>\n                                        <label class=\"radio-inline\"><input #ac [checked]=\"_encashobj.field1 == ac.value\" (click)=\"clearAccountInformation(2)\" (click)=\"_encashobj.field1 = ac.value\" type=\"radio\" name=\"_encashobj.field1\" value=\"2\" >By Account </label>\n                                    </div>\n                                </div>\n\n                                <div class=\"form-group\"> \n                                    <label class=\"col-md-4\" *ngIf=\"_encashobj.field1 == 3\"> GL <font class=\"mandatoryfont\" >*</font></label>\n                                    <label class=\"col-md-4\" *ngIf=\"_encashobj.field1 == 2\"> Account <font class=\"mandatoryfont\" >*</font></label>\n                                    <div class=\"col-md-8\"  style=\"padding-left:16px !important;\">   \n                                        <div class=\"input-group\">\n                                            <input  class=\"form-control\" required=\"true\" type = \"text\" *ngIf=\"_encashobj.field1 == 3\" [(ngModel)]=\"_encashobj.toAccount\" [ngModelOptions]=\"{standalone: true}\" readonly> \n                                            <input  class=\"form-control\" required=\"true\" type = \"text\" *ngIf=\"_encashobj.field1 == 2\" [(ngModel)]=\"_encashobj.toAccount\" [ngModelOptions]=\"{standalone: true}\" readonly> \n                                            <span class=\"input-group-btn\"><button class=\"btn btn-primary\" type=\"button\" (click)=\"lookuppopup();\" >&equiv;</button></span>\n                                        </div>\n                                    </div>\n                                </div>  \n\n                                <div class=\"form-group\">  \n                                    <rp-input *ngIf=\"_encashobj.field1 == 2\" [(rpModel)]=\"_encashobj.branchCode\" rpReadonly=\"true\" rpRequired =\"true\" rpType=\"text\" rpLabelRequired='true' rpLabel=\"Branch Code \"  rpClass=\"col-md-8 control-label\" rpLabelClass=\"col-md-4 \" ></rp-input>  \n                                    \n                                    <div class=\"form-group\" *ngIf=\"_encashobj.field1 == 3\">\n                                        <label class=\"col-md-4\" style=\"text-align: left;padding-left: 28px\"> Branch Code <font size=\"4\" color=\"#FF0000\" >*</font></label>\n                                        <div class=\"col-md-8\">\n                                            <select [(ngModel)]=\"_encashobj.branchCode\" style=\"width:95%;margin-left:6px\" class=\"form-control col-md-0\" [ngModelOptions]=\"{standalone: true}\">\n                                                <option *ngFor=\"let item of ref._lov3.ref018\" value=\"{{item.value}}\" >{{item.caption}}</option> \n                                            </select>                \n                                        </div> \n                                    </div> \n                                </div>\n                            </div>\n                        </div>\n\n                        <div  class=\"col-md-6\">\n                            <legend class=\"encash-legend\">Account Information</legend>\n                            <div class=\"form-group\">\n                                <rp-input [(rpModel)]=\"_encashobj.toName\" rpRequired =\"true\" rpType=\"text\" rpLabelRequired='true' rpLabel=\"Name \"  rpClass=\"col-md-8 control-label\" rpLabelClass=\"col-md-4 \" ></rp-input>\n                            </div>\n                            <div class=\"form-group\">\n                                <rp-input [(rpModel)]=\"_encashobj.accountNRC\"  rpType=\"text\"  rpLabel=\" NRC \"  rpClass=\"col-md-8 control-label\" rpLabelClass=\"col-md-4 \" ></rp-input>\n                            </div>\n                            <div class=\"form-group\">\n                                <rp-input [(rpModel)]=\"_encashobj.accountPhone\"  rpType=\"text\"  rpLabel=\" Phone No.\"  rpClass=\"col-md-8 control-label\" rpLabelClass=\"col-md-4\"></rp-input>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </form>\n        </div>\n    </div>\n</div>\n\n\n<div id=\"popup\" class=\"modal fade\" role=\"dialog\">\n    <div id=\"popupsize\" class=\"modal-dialog modal-lg\">  \n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n                <h4 id=\"popuptitle\" class=\"modal-title\" *ngIf=\"_encashobj.field1 == 3\">GL Info Criteria</h4>\n                <h4 id=\"popuptitle\" class=\"modal-title\" *ngIf=\"_encashobj.field1 == 2\">Account Info Criteria</h4>\n            </div> \n\n            <div id=\"popupbody\" class=\"modal-body\">\n                <div class=\"form-group\"> \n                    <div class=\"col-md-3\">\n                        <div class=\"form-group\"> \n                            <select *ngIf=\"_encashobj.field1 == 3\" [(ngModel)]=_SearchData.searchvalue class=\"form-control col-md-0\" tabindex=\"1\">\n                                 <option  *ngFor=\"let item of ref._lov1.refgl\" value=\"{{item.value}}\">{{item.caption}}</option>\n                            </select>\n                            <select *ngIf=\"_encashobj.field1 == 2\" [(ngModel)]=_SearchData.searchvalue class=\"form-control col-md-0\" tabindex=\"1\">\n                                <option *ngFor=\"let item of ref._lov1.refaccount\" value=\"{{item.value}}\">{{item.caption}}</option>\n                            </select>\n                        </div>\n                    </div> \n                    \n                    <div class=\"col-md-3\">\n                        <div class=\"form-group\"> \n                            <select [(ngModel)]=_SearchData.search class=\"form-control col-md-0\" tabindex=\"2\">\n                                <option  *ngFor=\"let item of ref._lov1.ref023\" value=\"{{item.value}}\">{{item.caption}}</option>\n                            </select>\n                        </div>\n                    </div> \n            \n                    <div class=\"col-md-4\">\n                        <div class=\"input-group\"> \n                                <input class=\"form-control\" type=\"text\" [(ngModel)]=\"_SearchData.searchcaption\" (keyup)=\"searchKeyup1($event)\" tabindex=\"3\"> \n                                <span class=\"input-group-btn input-md\">\n                                    <button class=\"btn btn-primary input-md\" type=\"button\" ((keyup)=\"searchKeyup1($event)\" (click)=\"goSearch(_SearchData.searchcaption,_SearchData.searchvalue,_SearchData.search)\"  tabindex=\"4\">\n                                        <span class=\"glyphicon glyphicon-search\"></span>Search\n                                    </button>\n                                </span> \n                        </div>\n                    </div>\n                </div>           \n\n                <div class=\"form-group\" [hidden]=\"hideselect\">  \n                        <table *ngIf=\"_encashobj.field1 == 2\" class=\"table table-striped table-condensed table-hover tblborder\" style=\"font-size:14px;\">\n                            <thead>\n                                <tr>\n                                    <th>Sr No.</th>\n                                    <th>Account Number</th>\n                                    <th>Name</th> \n                                    <th>NRC</th>\n                                    <th>Currency</th>             \n                                </tr>\n                            </thead>\n                            <tbody>\n                                <tr *ngFor=\"let obj of _accinfo.accArr,let i=index\">\n                                    <td class=\"srno\">{{i+1}}</td>\n                                    <td class=\"account\"> <a (click)=\"gotobindAccount(obj)\">{{obj.accNumber}}</a></td>\n                                    <td>{{obj.name}}</td>\n                                    <td>{{obj.nrc}}</td> \n                                    <td>{{obj.currencyCode}}</td> \n                                </tr> \n                            </tbody>\n                        </table> \n\n                        <table *ngIf=\"_encashobj.field1 == 3\" class=\"table table-striped table-condensed table-hover tblborder\" style=\"font-size:14px;\">\n                            <thead>\n                                <tr>\n                                    <th>Sr No.</th>\n                                    <th>Account Number</th>\n                                    <th>Account Description</th> \n                                    <th>SubGroup</th>\n                                    <th>SubGroup Description</th> \n                                    <th>Account Type Description</th>             \n                                </tr>\n                            </thead>\n                            <tbody>\n                                <tr *ngFor=\"let obj of _accinfo.glArr,let i=index\">\n                                    <td>{{i+1}}</td>\n                                    <td> <a (click)=\"gotobindAccount(obj)\">{{obj.accNo}}</a></td>\n                                    <td>{{obj.accdesc}}</td>\n                                    <td>{{obj.subGroup}}</td> \n                                    <td>{{obj.subGroupDesc}}</td>\n                                    <td>{{obj.accTypeDescription}}</td> \n                                </tr> \n                            </tbody>\n                        </table>    \n                </div> \n                <div style = \"width : 500px; height: 20px\">&nbsp;</div>\n            </div>\n\n            <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\" (click)=\"_mflag=true\">Close</button>\n            </div>\n        </div>\n    </div>\n</div>\n  \n<div [hidden] = \"_mflag\">\n    <div class=\"loader modal\"id=\"loader\" ></div>\n</div> \n      \n    "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmEncashmentComponent);
    return FrmEncashmentComponent;
}());
exports.FrmEncashmentComponent = FrmEncashmentComponent;
//# sourceMappingURL=frmencashment.component.js.map