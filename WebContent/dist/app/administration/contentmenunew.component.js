"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
require('tinymce/tinymce.min');
require('tinymce/themes/modern/theme');
require('tinymce/plugins/link/plugin.js');
require('tinymce/plugins/paste/plugin.js');
require('tinymce/plugins/table/plugin.js');
require('tinymce/plugins/advlist/plugin.js');
require('tinymce/plugins/autoresize/plugin.js');
require('tinymce/plugins/lists/plugin.js');
require('tinymce/plugins/code/plugin.js');
require('tinymce/plugins/image/plugin.js');
require('tinymce/plugins/imagetools/plugin.js');
var contentmenunew = (function () {
    function contentmenunew(ics, _router, route, http, ref, l_util) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this.l_util = l_util;
        this._mflag = false;
        this.flagnew = true;
        this.flagsave = true;
        this.flagaca = true;
        this.flagdelete = true;
        this.falgaccombo = true;
        this.flagview = true;
        this.flagCombo = "true";
        this.dates = { "_date": null };
        this._appCodeCombo = [];
        this._appCodeData = "";
        //for multielect
        this._region = [];
        //Region MultiSelect //
        this.selectSettings2 = {
            enableSearch: true,
        };
        this.texts = {
            defaultTitle: 'Select Region'
        };
        this.selectedregion = [];
        this._regioncode = "";
        this._obj1 = { id: 0, name: '', value: '' };
        // alertenddates = { "_date": null };
        this._file = null;
        this._fileName = '';
        this._util = new rp_client_util_1.ClientUtil();
        this._time = "";
        this._str = "";
        this.flagcanvas = true;
        //_crop = [{ "value": "", "caption": "", "flag": false }];
        this._obj = { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": "", "modifiedDate": "", "modifiedTime": "", "userId": "", "userName": "", "modifiedUserId": "", "modifiedUserName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [], "videoUpload": [], "comData": [], "cropdata": [], "agrodata": [], "ferdata": [], "towndata": [], "crop": null, "fert": null, "agro": null, "addTown": null, "uploadlist": [], "resizelist": [] };
        this.uploadDataList = [];
        this.uploadDataListResize = [];
        this.uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
        this.serial = 0;
        this._viewsk = 0;
        this.ImagePath = "";
        this._noti = { "flag": false };
        // _alert = { "flag": false };
        this.flagAcademic = false;
        this._roleval = "";
        this.onEditorKeyup = new core_1.EventEmitter();
        this._checkcw = false;
        this._checkeditor = false;
        this._checkpubisher = false;
        this._checkadmin = false;
        this._image = "";
        this._urllink = "";
        this.content = 0;
        this._img = "";
        this._result = { "longResult": "", "msgDesc": "", "key": [], "keyResult": 0, "state": "", "stringResult": "" };
        this.flag = false;
        this.pushcaption = "";
        this.splicearrindex = -1;
        this.splicearrindex1 = -1;
        this.substitueindex = 0;
        //alert("status: " + this.ics._profile.loginStatus);
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        this._roleval = ics._profile.t1;
        this._datepickerOpts = this.ics._datepickerOpts;
        this._datepickerOpts.format = 'dd/mm/yyyy';
        this.flagnew = false;
        this.flagsave = false;
        this.flagdelete = true;
        this.falgaccombo = true;
        //this.setBtns();
        this.flagview = true;
        //this.rd = false;
        //this.getCropList();
        //this.getAcademicComboList();
        this.getStateList();
        this._time = this._util.getTodayTime();
        this._obj.createdTime = this._time;
        this._obj.modifiedTime = this._time;
        if (this.ics._profile.loginStatus == 1) {
            this.ref._lov3.postingstate = [];
            this.ref._lov3.postingstate = this.ref._lov3.postingstate0;
            this._checkcw = true;
        }
        if (this.ics._profile.loginStatus == 2) {
            this.ref._lov3.postingstate = [];
            this.ref._lov3.postingstate = this.ref._lov3.postingstate1;
            this._checkeditor = true;
        }
        if (this.ics._profile.loginStatus == 3) {
            //  this.ref._lov3.postingstate = [];
            this.ref._lov3.postingstate = this.ref._lov3.postingstate2;
            this._checkpubisher = true;
        }
        if (this.ics._profile.loginStatus == 4) {
            this.ref._lov3.postingstate = [];
            this.ref._lov3.postingstate = this.ref._lov3.postingstateadmin;
            this._checkadmin = true;
        }
        if (this.ics._profile.loginStatus == 5) {
            this.ref._lov3.postingstate = [];
            this.ref._lov3.postingstate = this.ref._lov3.postingstateadmin;
            this._checkadmin = true;
        }
    }
    contentmenunew.prototype.ngOnInit = function () {
        var _this = this;
        var self = this;
        var urllink1 = this._urllink;
        var ics = this.ics;
        var http = this.http;
        var uploadData = this.uploadData;
        var uploadDataList = this.uploadDataList;
        var uploadDataListResize = this.uploadDataList;
        var showMessage = this.showMessage;
        var serial = this.serial;
        tinymce.init({
            selector: 'textarea',
            body_class: 'uni',
            menubar: false,
            paste_data_images: true,
            toolbar: "bold italic | alignleft aligncenter alignright alignjustify | image",
            plugins: ['emoticons', 'link', 'modern', 'paste', 'table', 'image', 'imagetools'],
            /* file_browser_callback: function (field_name, url, type, win) {
                if (type == 'image') jQuery('#upload input').click();
                win.document.getElementById(field_name).value = urllink1;
            }, */
            setup: function (editor) {
                _this.editor = editor;
                editor.on('init', function () {
                    editor.setContent(_this._obj.t2);
                });
                editor.on('keyup change', function () {
                    var content = editor.getContent();
                    _this._obj.t2 = content;
                    _this.onEditorKeyup.emit(content);
                });
            }
        });
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "new" && _this.ics.getBean != null) {
                var parameterlst = _this.ics.getBean().parameterlst;
                _this.content = 1;
                _this.goNew();
                _this.content = 0;
            }
            else if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this.goGet(id);
                _this._viewsk = id;
            }
        });
    };
    contentmenunew.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    contentmenunew.prototype.goGet = function (p) {
        var _this = this;
        this.flagview = false;
        this.flagnew = false;
        this.flagsave = false;
        this.flagdelete = false;
        this._obj.createdTime = this._time;
        this._obj.modifiedTime = this._time;
        this.http.doGet(this.ics.cmsurl + 'serviceCMS/readBySyskey?key=' + p).subscribe(function (data) {
            _this._obj = data;
            console.log("Data read=" + JSON.stringify(data.uploadlist));
            // this.alertenddates._date = this._util.setDatePickerDateNew(this._obj.t11);
            _this.editor.setContent(_this._obj.t2);
            if (_this.ics._profile.loginStatus == 1 && _this._obj.n7.toString() == "6") {
                _this.ref._lov3.postingstate = [];
                _this.ref._lov3.postingstate = _this.ref._lov3.readpoststatuecon;
            }
            if (_this.ics._profile.loginStatus == 2 && _this._obj.n7.toString() == "2") {
                _this.ref._lov3.postingstate = [];
                _this.ref._lov3.postingstate = _this.ref._lov3.readpoststatueeditor;
            }
            if (_this.ics._profile.loginStatus == 3 && _this._obj.n7.toString() == "4") {
                _this.ref._lov3.postingstate = [];
                _this.ref._lov3.postingstate = _this.ref._lov3.readpostpublisher;
            }
            if (_this.ics._profile.loginStatus == 4) {
                _this.ref._lov3.postingstate = [];
                _this.ref._lov3.postingstate = _this.ref._lov3.postingstateadmin;
            }
            if (_this.ics._profile.loginStatus == 5) {
                _this.ref._lov3.postingstate = [];
                _this.ref._lov3.postingstate = _this.ref._lov3.postingstateadmin;
            }
            if (_this._obj.t11 != '') {
                _this._noti.flag = true;
            }
            if (_this._obj.uploadlist.length > 0) {
                _this.uploadDataList = _this._obj.uploadlist;
                _this.serial = _this.uploadDataList[_this.uploadDataList.length - 1].serial;
                _this._obj.uploadlist = [];
            }
            if (_this._obj.resizelist.length > 0) {
                _this.uploadDataListResize = _this._obj.resizelist;
                _this._obj.resizelist = [];
            }
        }, function (error) { }, function () { });
    };
    contentmenunew.prototype.goNew = function () {
        if (this.content == 0) {
            this.editor.setContent("");
        }
        jQuery("#imageUpload").val("");
        jQuery("#imageUpload").val("");
        jQuery("#save").prop("disabled", false);
        if (this.ics._profile.t1 == '204202') {
            this.ref._lov3.postingstate = this.ref._lov3.postingstate0;
        }
        if (this.ics._profile.t1 == '204681') {
            this.ref._lov3.postingstate = this.ref._lov3.postingstate1;
        }
        if (this.ics._profile.t1 == '206545') {
            this.ref._lov3.postingstate = this.ref._lov3.postingstate2;
        }
        if (this.ics._profile.t1 == '11384') {
            this.ref._lov3.postingstate = this.ref._lov3.postingstate;
        }
        this.uploadDataList = [];
        this.flagnew = false;
        this.flagsave = false;
        this.flagview = true;
        this.flagdelete = true;
        this._noti.flag = false;
        //this.rd = true;
        this._fileName = "";
        var _img = "";
        this.uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
        this._obj = { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": "", "modifiedDate": "", "modifiedTime": "", "userId": "", "userName": "", "modifiedUserId": "", "modifiedUserName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [], "videoUpload": [], "comData": [], "cropdata": [], "agrodata": [], "ferdata": [], "towndata": [], "crop": null, "fert": null, "agro": null, "addTown": null, "uploadlist": [], "resizelist": [] };
        this._time = this._util.getTodayTime();
        this._obj.createdTime = this._time;
        this._obj.modifiedTime = this._time;
        this.serial = 0;
    };
    contentmenunew.prototype.goPost = function () {
        var _this = this;
        var imgUrl;
        if ((this._checkcw && this._obj.n7.toString() != "6") || (this._checkeditor && this._obj.n7.toString() != "2") || (this._checkpubisher && this._obj.n7.toString() != "4") || (this._checkadmin)) {
            this._obj.createdTime = this._time;
            this._obj.modifiedTime = this._time;
            this._obj.uploadlist = this.uploadDataList;
            this._obj.resizelist = this.uploadDataListResize;
            imgUrl = this.ics._imgurl;
            if (this.isValidate(this._obj)) {
                if (this._obj.syskey != 0) {
                    this._obj.modifiedUserId = this.ics._profile.userID;
                    this._obj.modifiedUserName = this.ics._profile.userName;
                }
                else {
                    this._obj.userId = this.ics._profile.userID;
                    this._obj.userName = this.ics._profile.userName;
                }
                var url = this.ics.cmsurl + 'serviceCMS/saveContenMenu?imgurl=' + imgUrl;
                this._obj.n5 = Number(this.ics._profile.t1);
                var json = this._obj;
                this.http.doPost(url, json).subscribe(function (data) {
                    _this._result = data;
                    _this.showMessage(data.msgDesc, data.state);
                    _this._obj.syskey = data.longResult[0];
                    //this.rd = false;
                }, function (error) {
                    _this.showMessage("Can't Saved This Record!", undefined);
                }, function () { });
            }
        }
        else {
            this.showMessage("Can't Save This Status!", undefined);
        }
    };
    contentmenunew.prototype.goDelete = function () {
        var _this = this;
        if (this._obj.t1 != "") {
            var url = this.ics.cmsurl + 'serviceQuestionAdm/deleteQuestion';
            var json = this._obj;
            this.http.doPost(url, json).subscribe(function (data) {
                _this.showMessage(data.msgDesc, data.state);
                if (data.state) {
                    _this.goNew();
                }
            }, function (error) { }, function () { });
        }
        else {
            this.showMessage("No Article to Delete!", undefined);
        }
    };
    contentmenunew.prototype.goList = function () {
        this._router.navigate(['/contentmenulist']);
    };
    contentmenunew.prototype.uploadedFile = function (event) {
        var _this = this;
        this.flagcanvas = true;
        if (event.target.files.length == 1) {
            this._fileName = event.target.files[0].name;
            this._file = event.target.files[0];
            var index = this._fileName.lastIndexOf(".");
            var imagename = this._fileName.substring(index);
            imagename = imagename.toLowerCase();
            if (imagename == ".jpg" || imagename == ".jpeg" || imagename == ".png") {
                var url = this.ics.cmsurl + 'fileAdm/fileupload?f=upload&fn=' + this._fileName + '&id=' + this.ics._profile.t1 + '&type=' + this._obj.n4 + '&imgUrl=' + this.ics._imgurl;
                this.http.upload(url, this._file).subscribe(function (data) {
                    if (data.code === 'SUCCESS') {
                        _this.showMessage("Upload Successful!", true);
                        alert("url: " + data.url);
                        var _img = _this._fileName;
                        _this.uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
                        _this.uploadData.name = data.fileName;
                        _this.uploadData.serial = ++_this.serial;
                        _this.uploadData.url = data.url;
                        _this.uploadDataList.push(_this.uploadData);
                        _this.uploadDataListResize.push(data.sfileName);
                        _this._fileName = "";
                    }
                    else {
                        _this.showMessage("Upload Unsuccessful! Please Try Again...", false);
                    }
                }, function (error) { }, function () { });
            }
            else {
                this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": "Choose Image Associated!" });
            }
        }
        else {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "info", "t3": "Upload File!" });
        }
    };
    contentmenunew.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "primary", "t3": msg });
        }
    };
    contentmenunew.prototype.isValidate = function (obj) {
        if (obj.t1 == "" && obj.t2 == "") {
            this.showMessage("Please fill Title and Content!", false);
            return false;
        }
        else if (obj.t1 == "") {
            this.showMessage("Please fill Title!", false);
            return false;
        }
        else if (obj.t2 == "") {
            this.showMessage("Please fill Content!", false);
            return false;
        }
        else if (obj.t5 == "") {
            this.showMessage("Choose Region!", false);
            return false;
        }
        else if (obj.n7 == "") {
            this.showMessage("Choose Status!", false);
            return false;
        }
        else if (this._obj.uploadlist.length > 0) {
            for (var i = 0; i < this._obj.uploadlist.length; i++) {
                if (this._obj.uploadlist[i].serial == '' || this._obj.uploadlist[i].serial == 0) {
                    this.showMessage("Invalid Serial No. !", false);
                    return false;
                }
            }
        }
        if (this.flagAcademic) {
            if (this._obj.n6 == 0) {
                this.showMessage("Select academic type !", false);
                return false;
            }
        }
        return true;
    };
    contentmenunew.prototype.setBtns = function () {
        var k = this.ics.getBtns("/contentmenulist");
        if (k != "" && k != undefined) {
            var strs = k.split(",");
            for (var i = 0; i < strs.length; i++) {
                if (strs[i] == "1") {
                    this.flagnew = false;
                }
                if (strs[i] == "2") {
                    this.flagsave = false;
                }
            }
        }
    };
    contentmenunew.prototype.setImgUrl = function (str) {
        return 'upload/image/' + str;
    };
    contentmenunew.prototype.changeArray = function (data, obj, num) {
        var arr = [];
        if (data instanceof Array) {
            arr = data;
            return arr;
        }
        else {
            if (num == 0) {
                arr[0] = obj;
                arr[1] = data;
                return arr;
            }
            if (num == 1) {
                arr[0] = data;
                return arr;
            }
        }
    };
    contentmenunew.prototype.goView = function (p) {
        this._router.navigate(['/eduView', 'read', p]);
    };
    contentmenunew.prototype.remove = function (index) {
        this.uploadDataList.splice(index, 1);
    };
    contentmenunew.prototype.valuechange = function (serial, ind) {
        for (var i = 0; i < this.uploadDataList.length; i++) {
            if (this.uploadDataList[i].serial == serial) {
                this.uploadDataList[ind].serial = "";
                this.flag = true;
                this.showMessage("Serial No. Already Exist!", false);
            }
        }
        if (!this.flag) {
            this.uploadDataList[ind].serial = serial;
        }
        this.flag = false;
    };
    /* getAcademicComboList() {
        this.http.doGet(this.ics._apiurl + 'servicecontentmenu/getAcademicRef').subscribe(
            response => {
                if ((response != null || response != undefined) && response.data.length > 0) {
                    this.ref._lov3.academicombo = this._util.changeArray(response.data, this._obj, 1);
                }
            },
            error => { },
            () => { }
        );
    } */
    contentmenunew.prototype.updateChecked = function (event) {
        if (event.target.checked) {
            this._obj.t11 = "noti";
        }
        else {
            this.showMessage("Please check that you have read and agree to the Terms and Conditions !", undefined);
        }
    };
    contentmenunew.prototype.deleteFile = function (obj) {
        var _this = this;
        this.http.doGet(this.ics.cmsurl + 'fileAdm/fileRemove?fn=' + obj.name + '&type=').subscribe(function (data) {
            _this._mflag = true;
            console.log(data.code);
            if (data.code === 'SUCCESS') {
                var index = _this.uploadDataList.indexOf(obj);
                _this.uploadDataList.splice(index, 1);
                _this.showMessage('Photo Removed!', true);
            }
        }, function (error) { }, function () { });
    };
    contentmenunew.prototype.onDragStart = function (event, i, p, subarrname) {
        this.subarr = subarrname;
        this.pushcaption = p.name;
        this.splicearrindex1 = i;
        this.splicearrindex = -1;
    };
    contentmenunew.prototype.allowDrop = function (event) {
        event.preventDefault();
    };
    contentmenunew.prototype.goSubstitute = function (event, p) {
        this.substitueindex = p;
    };
    contentmenunew.prototype.onDrop = function (event, i, p, arrname) {
        if (this.subarr.length > 0) {
            var sub = this.subarr[this.splicearrindex1];
            var subconditional = [];
            this.subarr.splice(this.splicearrindex1, 1);
            arrname.splice(this.substitueindex, 0, sub);
        }
    };
    contentmenunew.prototype.getStateList = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics.cmsurl + 'serviceCMS/getStateListByUserID?type=1&userID=' + this.ics._profile.userID).subscribe(function (response) {
                if (response.refstate != null && response.refstate != undefined) {
                    //this.ref._lov3.refstate = [{ "value": "", "caption": "" }];
                    _this.ref._lov3.refstate = [];
                    if (!(response.refstate instanceof Array)) {
                        var m = [];
                        m[0] = response.data;
                        _this.ref._lov3.refstate = m;
                    }
                    else {
                        _this.ref._lov3.refstate = response.refstate;
                    }
                }
                else {
                    _this.ref._lov3.refstate = [{ "value": "", "caption": "Empty" }];
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Error!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], contentmenunew.prototype, "onEditorKeyup", void 0);
    contentmenunew = __decorate([
        core_1.Component({
            selector: 'contentMenunew',
            template: "\n    <div class=\"container col-md-12 col-sm-12 col-xs-12\" style=\"padding: 0px 5%;\">\n\t<form class=\"form-horizontal\" ngNoForm>\n\t\t<legend>Content</legend>\n\t\t<!--<div class=\"cardview list-height\">    -->\n            <div class=\"col-md-12\" style=\"margin-bottom: 10px;\">\n                <button class=\"btn btn-sm btn-primary\"  type=\"button\"   (click)=\"goList()\">List</button> \n                <button class=\"btn btn-sm btn-primary\" [disabled]=\"flagnew\" id=\"new\" type=\"button\" (click)=\"goNew()\">New</button>      \n                <button class=\"btn btn-sm btn-primary\" [disabled]=\"flagsave\" id=\"save\" type=\"button\" (click)=\"goPost()\">Save</button> \n                <button class=\"btn btn-sm btn-primary\" [disabled]=\"flagdelete\" id=\"delete\" type=\"button\" (click)=\"goDelete();\" >Delete</button> \n                <button *ngIf=\"!flagview\" class=\"btn btn-sm btn-primary\"  type=\"button\"  (click)=\"goView(_viewsk);\" >View</button> \n            </div>\n            <div class=\"col-md-12\">\n                <div class=\"form-group\">\n                    <label class=\"col-md-2\">Title</label>\n                    <div class=\"col-md-4\">\n                        <input  type=\"text\" [(ngModel)]=\"_obj.t1\" class=\"form-control input-sm zawgyi\"/>                       \n                    </div>\n                    <rp-input  [(rpModel)]=\"_obj.n7\" rpRequired =\"true\" rpLabelClass = \"col-md-2\"   rpType=\"postingstate\" rpLabel=\"Status\"  autofocus ></rp-input>\n                </div>\n    \n                <div class=\"form-group\">\n                    <label class=\"col-md-2\">Share Link</label>\n                    <div class=\"col-md-4\">\n                        <input type=\"text\" [(ngModel)]=\"_obj.t13\" class=\"form-control input-sm\"/>                       \n                    </div>                    \n                    <rp-input [(rpModel)]=\"_obj.n4\" rpRequired =\"true\" rpType=\"contentmenuioniccombo\" rpLabelClass = \"col-md-2\" rpLabel=\"Type\"></rp-input>                  \n                </div>\n    \n                <div class=\"form-group\">\n                    <label class=\"col-md-2\">Content</label>\n                    <div class=\"col-md-10 zawgyi\">\n                            <textarea class=\"form-control input-sm\" [(ngModel)]=\"_obj.t2\" rows=\"12\" ></textarea>\n                            <input name=\"image\" type=\"file\" id=\"upload\" class=\"hidden\" onchange=\"\">\n                    </div>\n                </div>\n\t\t\t\t<div class=\"form-group\" id=\"flagImageUpload\">\n\t\t\t\t\t<label class=\"col-md-2\">Image Associated (.jpeg, .png, .jpg)</label>\n\t\t\t\t\t<div class=\"col-md-4\">\n\t\t\t\t\t\t<input type=\"file\" id=\"imageUpload\" class=\"form-control input-md\" (change)=\"uploadedFile($event)\" placeholder=\"Upload file...\" />\n\t\t\t\t\t</div>     \n\t\t\t\t</div>\t\t\t\t\n            </div>\n    \n\t\t\t<div class=\"col-md-3 col-sm-4 col-xs-6\" style=\"padding-right: 1px;\" *ngFor=\"let obj of uploadDataList, let i=index\"\n\t\t\t(dragstart)=\"onDragStart($event, i, obj, uploadDataList)\" draggable =\"true\" (dragover)=\"allowDrop($event)\"\n\t\t\t(dragover)=\"goSubstitute(event,i)\" (drop)=\"onDrop($event, i, obj, uploadDataList)\">\n\t\t\t\t<div id=\"img_container\">\n\t\t\t\t<img src={{(obj.url)}} alt={{obj.name}} title={{obj.name}} style=\"margin-top:20px;width:80%;height:200px;overflow: hidden;object-position: 25% 50%;transition: 1s width, 1s height;border-radius:5px;object-fit:cover;\"/>\n\t\t\t\t\t<button class=\"button_over_img\" type=\"button\" title=\"Remove Photo\" (click)=\"deleteFile(obj)\">\n\t\t\t\t\t\t<span class=\"glyphicon glyphicon-remove\"></span>\n\t\t\t\t\t</button>\t\t\t\t\n\t\t\t\t<input  type=\"text\" [(ngModel)]=\"obj.imgdesc\" class=\"form-control input-md zawgyi\" style=\"margin-top:10px;width:220px;\"/>\n\t\t\t\t</div>\n\t\t\t</div>\n\n        <div class=\"form-group\">&nbsp;</div>\n\t\t<!--</div>-->\n    </form>\n</div>\n    <div [hidden]=\"_mflag\">\n    <div  id=\"loader\" class=\"modal\" ></div></div>\n"
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences, rp_client_util_1.ClientUtil])
    ], contentmenunew);
    return contentmenunew;
}());
exports.contentmenunew = contentmenunew;
//# sourceMappingURL=contentmenunew.component.js.map