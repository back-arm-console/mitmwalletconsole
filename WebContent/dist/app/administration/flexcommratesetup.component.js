"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
core_2.enableProdMode();
var FlexCommRateSetup = (function () {
    function FlexCommRateSetup(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
        this._list = { "commdetailArr": [{ "syskey": 0, "hkey": 0, "zone": "", "commType": "", "fromAmt": "", "toAmt": "", "amount": "", "minAmt": "", "maxAmt": "", "gLorAC": "" }], "mainType": "", "commRef": "TBA", "msgCode": "", "msgDesc": "", "createdUserID": "", "sessionID": "", "description": "" };
        this._showlist = { "commdetailArr": [{ "syskey": 0, "hkey": "", "zone": "", "commType": "", "fromAmt": "", "toAmt": "", "amount": "", "minAmt": "", "maxAmt": "", "gLorAC": "" }], "mainType": "", "commRef": "TBA", "msgCode": "", "msgDesc": "", "createdUserID": "", "sessionID": "", "description": "" };
        this._obj = this.getDefaultObj();
        this._mflag = false;
        this._key = "";
        this.hKey = 0;
        this.sessionAlertMsg = "";
        this._zonecaption = "";
        this._util = new rp_client_util_1.ClientUtil();
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = false;
            this.checkSession();
            this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
            this._obj = this.getDefaultObj();
            this.getZone();
        }
    }
    FlexCommRateSetup.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this._key = id;
                _this.goReadBySyskey(id);
            }
        });
    };
    FlexCommRateSetup.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    FlexCommRateSetup.prototype.goRemove = function (i) {
        var index = this._showlist.commdetailArr.indexOf(i);
        this._showlist.commdetailArr.splice(index, 1);
        this._list.commdetailArr.splice(index, 1);
        // this.hKey = 0;
    };
    FlexCommRateSetup.prototype.goSave = function () {
        var _this = this;
        this._mflag = false;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "msgCode": "" };
        if (this._list.commdetailArr.length < 1) {
            this._returnResult.state = 'false';
            this._returnResult.msgDesc = "No data to save";
            this._mflag = true;
        }
        if (this._list.description == '') {
            this._returnResult.state = "false";
            this._returnResult.msgDesc = "Blank Description";
            this._mflag = true;
        }
        if (this._returnResult.msgDesc == '') {
            try {
                this._list.createdUserID = this.ics._profile.userID;
                this._list.sessionID = this.ics._profile.sessionID;
                this._list.commdetailArr[0].hkey = this.hKey;
                console.log("commdetail arr hkey is here : " + JSON.stringify(this._list.commdetailArr[0].hkey));
                var url = this.ics.cmsurl + 'serviceCMS/saveFlexCommRate';
                var json = this._list;
                this.http.doPost(url, json).subscribe(function (data) {
                    _this._obj = data;
                    _this._list.commRef = _this._obj.commRef;
                    _this._list.commdetailArr[0].hkey = _this._obj.hkey;
                    _this.hKey = _this._obj.hkey;
                    _this._returnResult.msgDesc = _this._obj.msgDesc;
                    _this._returnResult.state = _this._obj.state;
                    if (_this._obj.state == 'false') {
                        if (_this._obj.msgCode == '0016') {
                            _this.showMessage(_this._obj.msgDesc, false);
                        }
                    }
                    else {
                        jQuery("#mydelete").prop("disabled", false);
                    }
                    _this.showMessage(_this._returnResult.msgDesc, true);
                    _this._mflag = true;
                }, function (error) {
                    if (error._body.type == 'error') {
                        alert("Connection Timed Out!");
                    }
                }, function () { });
            }
            catch (e) {
                alert(e);
            }
        }
        else {
            this.showMessage(this._returnResult.msgDesc, false);
        }
    };
    FlexCommRateSetup.prototype.getDefaultObj = function () {
        this.isMainType = true;
        this.chkMainType = false;
        this.isPercentage = true;
        this._list = { "commdetailArr": [{ "syskey": 0, "hkey": 0, "zone": "", "commType": "", "fromAmt": "", "toAmt": "", "amount": "", "minAmt": "", "maxAmt": "", "gLorAC": "" }], "mainType": "", "commRef": "TBA", "msgCode": "", "msgDesc": "", "createdUserID": "", "sessionID": "", "description": "" };
        this._list.commdetailArr = [];
        this._showlist.commdetailArr = [];
        //this.goRemove(0);
        jQuery("#mydelete").prop("disabled", true);
        jQuery("#mySave").prop("disabled", false);
        return { "createdUserID": "", "sessionID": "", "hkey": 0, "msgCode": "", "msgDesc": "", "syskey": 0, "commRef": "TBA", "description": "", "mainType": "", "zone": "Same City", "amount": "", "fromAmt": "", "toAmt": "", "minAmt": "", "maxAmt": "", "commType": "Fix", "state": "" };
    };
    FlexCommRateSetup.prototype.goReadBySyskey = function (p) {
        var _this = this;
        try {
            this._mflag = false;
            var url = this.ics.cmsurl + 'serviceCMS/getFlexCommRateDataByID?id=' + p + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
            this.http.doGet(url).subscribe(function (data) {
                if (data != null) {
                    if (data.commdetailArr != null) {
                        if (!(data.commdetailArr instanceof Array)) {
                            var m = [];
                            m[0] = data.commdetailArr;
                            console.log("mdata is here : " + JSON.stringify(m[0]));
                            _this._list.commdetailArr = m;
                            console.log("mdata is here : " + JSON.stringify(_this._list.commdetailArr));
                            _this._list.commRef = data.commRef;
                            _this._list.description = data.description;
                            _this._list.mainType = data.mainType;
                            for (var i = 0; i < _this.ref._lov1.ref026.length; i++) {
                                if (data.commdetailArr.zone == _this.ref._lov1.ref026[i].value) {
                                    _this._zonecaption = _this.ref._lov1.ref026[i].caption;
                                    _this._showlist.commdetailArr.push({ "syskey": 0, "hkey": "", "zone": _this._zonecaption, "commType": data.commdetailArr.commType, "fromAmt": String(Number.parseFloat(data.commdetailArr.fromAmt).toFixed(2)), "toAmt": String(Number.parseFloat(data.commdetailArr.toAmt).toFixed(2)), "amount": String(Number.parseFloat(data.commdetailArr.amount).toFixed(2)), "minAmt": String(Number.parseFloat(data.commdetailArr.minAmt).toFixed(2)), "maxAmt": String(Number.parseFloat(data.commdetailArr.maxAmt).toFixed(2)), "gLorAC": "" });
                                    break;
                                }
                            }
                        }
                        else {
                            _this._list = data;
                            for (var j = 0; j < data.commdetailArr.length; j++) {
                                for (var i = 0; i < _this.ref._lov1.ref026.length; i++) {
                                    if (data.commdetailArr[j].zone == _this.ref._lov1.ref026[i].value) {
                                        _this._zonecaption = _this.ref._lov1.ref026[i].caption;
                                        _this._obj.zone = _this.ref._lov1.ref026[i].value;
                                        _this._showlist.commdetailArr.push({ "syskey": 0, "hkey": "", "zone": _this._zonecaption, "commType": data.commdetailArr[j].commType, "fromAmt": String(Number.parseFloat(data.commdetailArr[j].fromAmt).toFixed(2)), "toAmt": String(Number.parseFloat(data.commdetailArr[j].toAmt).toFixed(2)), "amount": String(Number.parseFloat(data.commdetailArr[j].amount).toFixed(2)), "minAmt": String(Number.parseFloat(data.commdetailArr[j].minAmt).toFixed(2)), "maxAmt": String(Number.parseFloat(data.commdetailArr[j].maxAmt).toFixed(2)), "gLorAC": "" });
                                        break;
                                    }
                                }
                            }
                        }
                        _this.hKey = _this._list.commdetailArr[0].hkey;
                        if (_this._list.mainType == "0") {
                            _this.chkMainType = false;
                            _this.isMainType = true;
                        }
                        else {
                            _this.chkMainType = true;
                            _this.isMainType = false;
                        }
                    }
                }
                if (_this._list.msgCode == '0016') {
                    _this._returnResult.msgDesc = _this._list.msgDesc;
                    _this.showMessage(_this._list.msgDesc, false);
                }
                jQuery("#mydelete").prop("disabled", false);
                jQuery("#mySave").prop("disabled", false);
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    FlexCommRateSetup.prototype.goList = function () {
        this._router.navigate(['/flexcommratelist']);
    };
    // showMessage() {
    // jQuery("#sessionalert").modal();
    //  Observable.timer(3000).subscribe(x => {
    //    this.goLogOut();
    //   });
    // }
    FlexCommRateSetup.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    /* checkSession() {
      try {
    
        let url: string = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
        this.http.doGet(url).subscribe(
          data => {
            if (data.msgCode == '0016') {
              this.sessionAlertMsg = data.msgDesc;
              this.showMessage();
            }
          },
          error => {
            if (error._body.type == 'error') {
              alert("Connection Timed Out!");
            }
          }, () => { });
      } catch (e) {
        alert(e);
      }
    
    } */
    FlexCommRateSetup.prototype.showMessageAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FlexCommRateSetup.prototype.clearData = function () {
        this._obj = this.getDefaultObj();
        this.hKey = 0;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
        jQuery("#mySave").prop("disabled", false);
        jQuery("#mydelete").prop("disabled", true);
    };
    FlexCommRateSetup.prototype.goNew = function () {
        this.clearData();
    };
    FlexCommRateSetup.prototype.changeCommissionType = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex; //Get Selected Index
        var value = options[options.selectedIndex].value; //Get Selected Index's Value  
        this._obj.commType = value;
        if (this._obj.commType == '1') {
            this.isPercentage = false;
        }
        else {
            this.isPercentage = true;
            this._obj.commType = this.ref._lov1.ref027[0].value;
        }
        for (var i = 1; i < this.ref._lov1.ref027.length; i++) {
            if (this.ref._lov1.ref027[i].value == value) {
                this._obj.commType = this.ref._lov1.ref027[i].value;
            }
        }
        // this._obj.commType = this.ref._lov1.ref027[0].value;
    };
    FlexCommRateSetup.prototype.goAddAll = function (p) {
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "msgCode": "" };
        if (this._obj.amount == '') {
            this._returnResult.state = "false";
            this._returnResult.msgDesc = "Blank Commission Amount";
        }
        else if (!/^([0-9.]{1,50})$/.test(this._obj.amount)) {
            this._returnResult.state = 'false';
            this._returnResult.msgDesc = "Amount should be number";
        }
        else if (this._obj.zone == '') {
            this._returnResult.state = "false";
            this._returnResult.msgDesc = "Blank Zone";
        }
        if (this.isMainType == false) {
            if (this._obj.fromAmt == '') {
                this._returnResult.state = "false";
                this._returnResult.msgDesc = "Blank From Amount";
            }
            else if (!/^([0-9.]{1,50})$/.test(this._obj.fromAmt)) {
                this._returnResult.state = 'false';
                this._returnResult.msgDesc = "From Amount should be number";
            }
            if (this._obj.toAmt == '') {
                this._returnResult.state = "false";
                this._returnResult.msgDesc = "Blank To Amount";
            }
            else if (!/^([0-9.]{1,50})$/.test(this._obj.toAmt)) {
                this._returnResult.state = 'false';
                this._returnResult.msgDesc = "To Amount should be number";
            }
        }
        if (this.isPercentage == false) {
            if (this._obj.minAmt == '') {
                this._returnResult.state = "false";
                this._returnResult.msgDesc = "Blank Minimum Amount";
            }
            else if (!/^([0-9.]{1,50})$/.test(this._obj.minAmt)) {
                this._returnResult.state = 'false';
                this._returnResult.msgDesc = "Minimum Amount should be number";
            }
            if (this._obj.maxAmt == '') {
                this._returnResult.state = "false";
                this._returnResult.msgDesc = "Blank Maximum Amount";
            }
            else if (!/^([0-9.]{1,50})$/.test(this._obj.maxAmt)) {
                this._returnResult.state = 'false';
                this._returnResult.msgDesc = "Maximum Amount should be number";
            }
        }
        if (this._obj.commType == '') {
            this._returnResult.state = 'false';
            this._returnResult.msgDesc = "Blank Commission Type.";
        }
        if (this._returnResult.msgDesc == '') {
            this._list.commdetailArr.push({ "syskey": 0, "hkey": 0, "zone": this._obj.zone, "commType": this._obj.commType, "fromAmt": this._obj.fromAmt, "toAmt": this._obj.toAmt, "amount": this._obj.amount, "minAmt": this._obj.minAmt, "maxAmt": this._obj.maxAmt, "gLorAC": "" });
            this._showlist.commdetailArr.push({ "syskey": 0, "hkey": "", "zone": this._zonecaption, "commType": this._obj.commType, "fromAmt": this._obj.fromAmt, "toAmt": this._obj.toAmt, "amount": this._obj.amount, "minAmt": this._obj.minAmt, "maxAmt": this._obj.maxAmt, "gLorAC": "" });
        }
        else {
            this.showMessage(this._returnResult.msgDesc, false);
        }
    };
    FlexCommRateSetup.prototype.getZone = function () {
        var _this = this;
        try {
            this._mflag = false;
            this.http.doGet(this.ics.cmsurl + 'serviceAdmLOV/getZone').subscribe(function (data) {
                _this.ref._lov1.ref026 = data.ref026;
                _this._mflag = true;
                _this._obj.zone = _this.ref._lov1.ref026[0].value;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
        }
    };
    FlexCommRateSetup.prototype.changeZone = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex; //Get Selected Index
        var value = options[options.selectedIndex].value; //Get Selected Index's Value  
        this._obj.zone = value;
        this._zonecaption = options[options.selectedIndex].text;
        for (var i = 1; i < this.ref._lov1.ref026.length; i++) {
            if (this.ref._lov1.ref026[i].value == value) {
                this._obj.zone = this.ref._lov1.ref026[i].value;
            }
        }
    };
    FlexCommRateSetup.prototype.checkMainType = function (event) {
        if (event.target.checked) {
            this.isMainType = false;
            this._list.mainType = "1";
        }
        else {
            this.isMainType = true;
            this._list.mainType = "0";
        }
    };
    FlexCommRateSetup.prototype.goDelete = function () {
        var _this = this;
        try {
            this._mflag = false;
            this._list.sessionID = this.ics._profile.sessionID;
            this._list.createdUserID = this.ics._profile.userID;
            var url = this.ics.cmsurl + 'serviceCMS/deleteFlexCommRate';
            var json = this._list;
            this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "msgCode": "" };
            this.http.doPost(url, json).subscribe(function (data) {
                _this._returnResult = data;
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMessage(data.msgDesc, false);
                }
                else {
                    _this.showMessage(data.msgDesc, true);
                    if (data.state) {
                        jQuery("#mydelete").prop("disabled", true);
                        jQuery("#mySave").prop("disabled", false);
                        _this._obj = { "createdUserID": "", "sessionID": "", "msgCode": "", "hkey": 0, "msgDesc": "", "syskey": 0, "commRef": "TBA", "description": "", "mainType": "", "zone": "", "amount": "", "fromAmt": "", "toAmt": "", "minAmt": "", "maxAmt": "", "commType": "", "state": "" };
                        _this._list = {
                            "commdetailArr": [{ "syskey": 0, "hkey": 0, "zone": "", "commType": "", "fromAmt": "", "toAmt": "", "amount": "", "minAmt": "", "maxAmt": "", "gLorAC": "" }], "mainType": "", "commRef": "TBA", "msgCode": "", "msgDesc": "", "createdUserID": "", "sessionID": "",
                            "description": ""
                        };
                        _this._showlist = { "commdetailArr": [{ "syskey": 0, "hkey": "", "zone": "", "commType": "", "fromAmt": "", "toAmt": "", "amount": "", "minAmt": "", "maxAmt": "", "gLorAC": "" }], "mainType": "", "commRef": "TBA", "msgCode": "", "msgDesc": "", "createdUserID": "", "sessionID": "", "description": "" };
                        _this.goRemove(0);
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
            this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "msgCode": "" };
        }
        catch (e) {
            alert(e);
        }
    };
    FlexCommRateSetup.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    FlexCommRateSetup.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMessage(data.desc, false);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMessage(data.desc, false);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FlexCommRateSetup.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FlexCommRateSetup.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    FlexCommRateSetup.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    FlexCommRateSetup.prototype.restrictSpecialCharacter = function (event, fid, value) {
        if ((event.key >= '0' && event.key <= '9') || event.key == '.' || event.key == ',' || event.key == 'Home' || event.key == 'End' ||
            event.key == 'ArrowLeft' || event.key == 'ArrowRight' || event.key == 'Delete' || event.key == 'Backspace' || event.key == 'Tab' ||
            (event.which == 67 && event.ctrlKey === true) || (event.which == 88 && event.ctrlKey === true) ||
            (event.which == 65 && event.ctrlKey === true) || (event.which == 86 && event.ctrlKey === true)) {
            if (fid == 101) {
                if (/^[\\\"\'\;\:\>\|~`!@#\$%^&*\(\)]$/i.test(event.key)) {
                    event.preventDefault();
                }
            }
            if (value.includes(".")) {
                if (/^[\\\"\'\;\:\>\|~`!@#\$%^&*.\(\)]$/i.test(event.key)) {
                    event.preventDefault();
                }
            }
        }
        else {
            event.preventDefault();
        }
    };
    FlexCommRateSetup = __decorate([
        core_1.Component({
            selector: 'flexcommrate-setup',
            template: "\n  <div class=\"container-fluid\">\n  <div class=\"row clearfix\">\n  <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n  <form class= \"form-horizontal\" (ngSubmit) = \"goSave()\"> \n  <!-- Form Name -->\n  <legend>Commission</legend>\n  <div class=\"cardview list-height\">\n  <div class=\"row col-md-12\">  \n    <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goList()\" >List</button> \n    <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goNew()\" >New</button>      \n    <button class=\"btn btn-sm btn-primary\" id=\"mySave\" type=\"submit\" >Save</button>          \n    <button class=\"btn btn-sm btn-primary\" disabled id=\"mydelete\"  type=\"button\" (click)=\"goDelete();\" >Delete</button> \n  </div>\n    \n  <div class=\"row col-md-12\">&nbsp;</div>\n  <div class=\"form-group\">\n  <div class=\"col-md-10\">\n\n  <div class=\"form-group\">\n    <rp-input [(rpModel)]=\"_list.commRef\" rpType=\"text\" rpClass=\"col-md-4\" rpLabelClass =\"col-md-2 control-label\" rpLabel=\"Commission Reference\" rpReadonly=\"true\"></rp-input>\n  </div>\n\n  <div class=\"form-group\">\n    <rp-input [(rpModel)]=\"_list.description\"rpType=\"text\" rpClass=\"col-md-4\" rpLabelClass =\"col-md-2 control-label\" rpLabel=\"Description \" rpLabelRequired=\"true\"></rp-input>\n  </div>\n\n  <div class=\"form-group\" >          \n    <label class=\"col-md-2\" > Main Type <font class=\"mandatoryfont\">*</font></label> \n    <div class=\"col-md-4\">\n      <input type = \"checkbox\" [(ngModel)]=\"chkMainType\" (change)=\"checkMainType($event)\" [ngModelOptions]=\"{standalone: true}\"> &nbsp; Is Tier                   \n    </div>\n  </div> \n\n  <div class=\"form-group\">\n    <label class=\"col-md-2\"> Zone <font class=\"mandatoryfont\">*</font> </label>\n    <div class=\"col-md-4\">\n    <select [ngModelOptions]=\"{standalone: true}\"  (change)=\"changeZone($event)\"  [(ngModel)]=\"_obj.zone\" class=\"form-control input-sm\">\n      <option *ngFor=\"let item of ref._lov1.ref026\" value=\"{{item.value}}\" >{{item.caption}}</option> \n    </select>                \n    </div> \n  </div>\n\n  <div class=\"form-group\">\n  <label class=\"col-md-2\"> Commission Type <font class=\"mandatoryfont\">*</font> </label>\n  <div class=\"col-md-4\">\n    <select [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_obj.commType\" (change)=\"changeCommissionType($event)\"  class=\"form-control input-sm\">\n    <option *ngFor=\"let item of ref._lov1.ref027\" value=\"{{item.value}}\" >{{item.caption}}</option> \n    </select>                \n    </div> \n  </div>\n  \n  <div *ngIf=\"isMainType == false\">\n    <div class=\"form-group\">\n    <rp-input [(rpModel)]=\"_obj.fromAmt\" rpLabel=\"From Amount \" rpType=\"text\" rpClass=\"col-md-4\"  (keydown)=\"restrictSpecialCharacter($event, 101 ,_obj.fromAmt)\" pattern=\"[0-9.,]+\" rpLabelClass =\"col-md-2 control-label\" rpLabelRequired=\"true\"></rp-input>\n    </div>\n\n    <div class=\"form-group\">\n    <rp-input [(rpModel)]=\"_obj.toAmt\" rpLabel=\"To Amount \" rpType=\"text\" rpClass=\"col-md-4\" (keydown)=\"restrictSpecialCharacter($event, 101 ,_obj.toAmt)\" pattern=\"[0-9.,]+\" rpLabelClass =\"col-md-2 control-label\" rpLabelRequired=\"true\"></rp-input>\n    </div>\n  </div>\n\n  <div class=\"form-group\">\n    <rp-input [(rpModel)]=\"_obj.amount\" rpLabel=\"Commission Amount \" rpType=\"text\" rpClass=\"col-md-4\"  (keydown)=\"restrictSpecialCharacter($event, 101 ,_obj.amount)\" pattern=\"[0-9.,]+\" rpLabelClass =\"col-md-2 control-label\" rpLabelRequired=\"true\"></rp-input>\n  </div>\n\n  <div *ngIf=\"isPercentage == false\">\n    <div class=\"form-group\">\n    <rp-input [(rpModel)]=\"_obj.minAmt\" rpLabel=\"Minimum Amount \" rpType=\"text\" rpClass=\"col-md-4\"  (keydown)=\"restrictSpecialCharacter($event, 101 ,_obj.minAmt)\" pattern=\"[0-9.,]+\" rpLabelClass =\"col-md-2 control-label\" rpLabelRequired=\"true\"></rp-input>\n    </div>\n\n    <div class=\"form-group\">\n    <rp-input [(rpModel)]=\"_obj.maxAmt\" rpLabel=\"Maximum Amount \" rpType=\"text\" rpClass=\"col-md-4\"  (keydown)=\"restrictSpecialCharacter($event, 101 ,_obj.maxAmt)\" pattern=\"[0-9.,]+\" rpLabelClass =\"col-md-2 control-label\" rpLabelRequired=\"true\"></rp-input>\n    </div>\n  </div>\n\n  <div class=\"form-group\" style=\"padding-left:15px\"> \n    <form> <button class=\"btn btn-sm btn-primary\" id=\"myadd\" (click)=\"goAddAll()\" >Add</button> </form>  \n  </div> \n  </div>\n    \n  <!--Start Table-->\n  <div class=\"row col-md-12\" style=\"overflow-x:auto; padding-left:30px\">\n    <table class=\"table table-striped table-condensed table-hover tblborder\">\n    <thead>\n      <tr>\n      <th>Sr No.</th>\n      <th>Zone</th>\n      <th>Commission Type</th>\n      <th style=\"text-align:right;\">From Amount</th>\n      <th style=\"text-align:right;\">To Amount</th>\n      <th style=\"text-align:right;\">Commission Amount</th>\n      <th style=\"text-align:right;\">Minimum Amount </th>\n      <th style=\"text-align:right;\">Maximum Amount </th>  \n      <th>&nbsp; </th> \n      </tr>\n    </thead>                \n    <tbody>\n      <tr *ngFor = \"let obj of _showlist.commdetailArr,let i=index\">                  \n      <td class = \"col-md-1\">  {{i+1 }}</td>\n      <td class = \"col-md-2\" > {{obj.zone}} </td>                    \n      <td class = \"col-md-2\" *ngIf=\"obj.commType == 0\"> Fix </td>\n      <td class = \"col-md-2\" *ngIf=\"obj.commType == 1\"> Percentage </td>                    \n      <td class = \"col-md-2 right\" > {{obj.fromAmt}}</td>\n      <td class = \"col-md-2 right\" > {{obj.toAmt}} </td>\n      <td class = \"col-md-2 right\" > {{obj.amount}} </td>  \n      <td class = \"col-md-2 right\" > {{obj.minAmt}} </td>\n      <td class = \"col-md-2 right\" > {{obj.maxAmt}} </td>  \n      <td> <img src=\"image/remove.png\" alt=\"remove.png\" height=\"20\" width=\"20\"  (click)=\"goRemove(obj)\"/></td>\n      </tr>\n    </tbody>\n    </table> \n    </div>\n  <!--End Table--> \n  </div>\n  </div>\n  </form>\n  </div>\n  </div>\n  </div>\n     \n  <div id=\"sessionalert\" class=\"modal fade\">\n    <div class=\"modal-dialog\">\n      <div class=\"modal-content\">\n        <div class=\"modal-body\">\n          <p>{{sessionAlertMsg}}</p>\n        </div>\n        <div class=\"modal-footer\">\n        </div>\n      </div>\n    </div>\n  </div>\n\n<div [hidden] = \"_mflag\">\n<div class=\"modal\" id=\"loader\"></div>\n</div> \n    \n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FlexCommRateSetup);
    return FlexCommRateSetup;
}());
exports.FlexCommRateSetup = FlexCommRateSetup;
//# sourceMappingURL=flexcommratesetup.component.js.map