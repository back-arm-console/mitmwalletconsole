"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var Rx_1 = require('rxjs/Rx');
core_1.enableProdMode();
var FrmTicketList = (function () {
    function FrmTicketList(ics, ref, _router, http) {
        this.ics = ics;
        this.ref = ref;
        this._router = _router;
        this.http = http;
        this._mflag = false;
        this.imgdata = "";
        this.hide = true;
        this._channelSyskey = "";
        this.ticketObj = this.getDefaultObj();
        this.finalObj = [];
        this.sessionAlertMsg = "";
        this.ticketStatus = "";
        this._roleval = "";
        //_pgobj = {"end": 10, "size": 10, "totalcount": 0,"current": 1,"prev": 0,"last": 20,"next": 11,"start": 0,};
        this._pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 0 };
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.markers = [];
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        this._roleval = ics._profile.t1;
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this.hide = true;
            this.ticketStatus = "All";
            this.imageLink = this.ics._imageurl;
            this.getTicketStatus();
            this.search();
        }
    }
    FrmTicketList.prototype.getDefaultObj = function () {
        return { "ticketData": [{ "autokey": 0, "syskey": 0, "createdDate": "", "modifiedDate": "", "date": "", "time": "", "shootDate": "", "shootTime": "", "userID": "", "n1": 0, "n2": 0, "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "",
                    "t11": "" }], "totalCount": 0, "currentPage": 1, "pageSize": 10, "state": true, "sessionID": "", "userID": "", "ticketStatus": "", "searchText": "" };
    };
    FrmTicketList.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    FrmTicketList.prototype.searchKeyup = function (e) {
        if (e.which == 13) {
            this.ticketObj.currentPage = 1;
            this.search();
        }
    };
    FrmTicketList.prototype.searching = function () {
        this.ticketObj.currentPage = 1;
        this.search();
    };
    FrmTicketList.prototype.ChangeStatus = function (options) {
        this.ticketStatus = options[options.selectedIndex].value;
        this.ticketObj.currentPage = 1;
        this.search();
    };
    FrmTicketList.prototype.search = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'TicketService/getTickets?ticketStatus=' + this.ticketStatus;
            var json = this.ticketObj;
            this.ticketObj.sessionID = this.ics._profile.sessionID;
            this.ticketObj.userID = this.ics._profile.userID;
            this.http.doPost(url, json).subscribe(function (data) {
                console.log("data:" + JSON.stringify(data));
                if (data != null) {
                    if (data.totalCount == 0) {
                        _this.showMsg("There is no tickets.", false);
                        _this.ticketObj.ticketData = [];
                    }
                    else {
                        _this.hide = false;
                        _this.ticketObj = data;
                        _this._pgobj.totalcount = data.totalCount;
                        if (!(data.ticketData instanceof Array)) {
                            var m = [];
                            m[0] = data.ticketData;
                            _this.ticketObj.ticketData = m;
                        }
                        else {
                            _this.ticketObj.ticketData = data.ticketData;
                        }
                        for (var i = 0; i < _this.ticketObj.ticketData.length; i++) {
                            if (_this.ticketObj.ticketData[i].t8 != undefined && _this.ticketObj.ticketData[i].t8 != null && _this.ticketObj.ticketData[i].t8 != '') {
                                var def = _this.ticketObj.ticketData[i].t8;
                                var latitudeOne = def.substring(0, def.indexOf("/"));
                                var longitudeOne = def.substring(def.indexOf("/") + 1);
                                var info = '';
                                if (_this.ticketObj.ticketData[i].t5 != undefined && _this.ticketObj.ticketData[i].t5 != null && _this.ticketObj.ticketData[i].t5 != '') {
                                    info = "Ticket No. " + _this.ticketObj.ticketData[i].t1 + " by " + _this.ticketObj.ticketData[i].t5;
                                }
                                _this.markers.push([latitudeOne, longitudeOne, [], info]);
                            }
                        }
                    }
                    _this._mflag = true;
                }
                else {
                    _this.hide = true;
                    _this._mflag = true;
                }
            }, function (error) {
                _this._mflag = true;
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmTicketList.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this.goLogOut();
        });
    };
    FrmTicketList.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    // loadMap() {
    //     var MarkerData = [];
    //     let caption = [];
    //     let obj = [];
    //     let cap;
    //     let ref = this._mapdata.datalst[j].t2.split("-");
    //     obj.push(Number(arr[0]));
    //     obj.push(Number(arr[1]));
    //     for (let v = 0; v < this._mapdata.datalst.length; v++) {
    //         if (_mapdata.datalst[v].datatype == "text") {
    //             let cc = this._mapdata.datalst[v].t2.split("-");
    //             if (cc[1] == ref[1]) {
    //                 cap = this._mapdata.datalst[v].datavalue;
    //                 caption.push(cap);
    //             }
    //         }
    //     }
    //     obj.push(caption);
    //     obj.push(ref[1]);
    // }
    FrmTicketList.prototype.goMap = function () {
        //this._router.navigate(['/map', , { cmd: "maplist", id: this.ticketObj }]);
        var map = new google.maps.Map(document.getElementById('map-canvas'));
        var infowindow = new google.maps.InfoWindow();
        for (var i = 0; i < this.markers.length; i++) {
            var latlng = new google.maps.LatLng(this.markers[i][0], this.markers[i][1]);
            map = new google.maps.Map(document.getElementById('map-canvas'), {
                zoom: 15,
                center: latlng
            });
        }
        for (var i = 0; i < this.markers.length; i++) {
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(this.markers[i][0], this.markers[i][1]),
                map: map,
                title: this.markers[i][3],
                zIndex: this.markers[i][1]
            });
            infowindow = new google.maps.InfoWindow();
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    //infowindow.setContent("Data:"+MarkerData[i][3]+"<br />"+ MarkerData[i][2]);
                    infowindow.setContent("Info: " + marker.title);
                    infowindow.open(map, marker);
                };
            })(marker, i));
        }
        // for (let i = 0; i < this.markers.length; i++) {
        //     var marker = new google.maps.Marker({
        //         position: new google.maps.LatLng(this.markers[i][0], this.markers[i][1]),
        //         map: map,
        //         title: this.markers[i][3],
        //         zIndex: this.markers[i][1]
        //     });
        //     infowindow = new google.maps.InfoWindow();
        //     google.maps.event.addListener(marker, 'click', (function (marker, i) {
        //         return function () {
        //             //infowindow.setContent("Data:"+MarkerData[i][3]+"<br />"+ MarkerData[i][2]);
        //             infowindow.setContent("Data:" + this.markers[i][3]);
        //             infowindow.open(map, marker);
        //         }
        //     })(marker, i));
        // }
        jQuery("#popup").modal();
    };
    FrmTicketList.prototype.changedPager = function (event) {
        if (this.ticketObj.totalCount != 0) {
            this._pgobj = event;
            var current = this.ticketObj.currentPage;
            var size = this.ticketObj.pageSize;
            this.ticketObj.currentPage = this._pgobj.current;
            this.ticketObj.pageSize = this._pgobj.size;
            if (this._pgobj.current != current || this._pgobj.size != size) {
                this.search();
            }
        }
    };
    FrmTicketList.prototype.goto = function (p) {
        console.log("p=" + JSON.stringify(p));
        this._router.navigate(['/ticket', 'read', p]);
    };
    FrmTicketList.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    FrmTicketList.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    FrmTicketList.prototype.getTicketStatus = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'TicketService/getTicketStatus';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.msgCode == "0016") {
                        _this.showMsg(data.msgDesc, false);
                        _this.logout();
                    }
                    if (data.msgCode != "0000") {
                        _this.showMsg(data.msgDesc, false);
                    }
                    if (data.msgCode == "0000") {
                        if (!(data.refTicketStatus instanceof Array)) {
                            var m = [];
                            m[0] = data.refTicketStatus;
                            _this.ref._lov3.refTicketStatus = m;
                        }
                        else {
                            _this.ref._lov3.refTicketStatus = data.refTicketStatus;
                        }
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmTicketList = __decorate([
        core_1.Component({
            selector: 'fmr-ticketList',
            template: "\n    <div class=\"container\">\n    <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\t\n        <form class=\"form-inline\">\n            <legend>Tickets</legend>\n            <div class=\"input-group\" style=\"padding-left: 0px;\">\n                <adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"ticketObj.totalCount\" (rpChanged)=\"changedPager($event)\"></adminpager>                                 \n            </div>\n    \n            <div class=\"input-group\" style=\"padding-right: 10px;\">\n                <button class=\"btn btn-primary\" type=\"button\" (click)=\"goMap()\" id=\"vh_list_btn\">Map View</button>\n            </div>\n    \n            <div class=\"input-group\">\n                <label class=\"col-md-4\" style=\"padding-left: 0px;\">Status</label>\n                <div class=\"col-md-8\" style=\"padding-left: 0px;\">\n                    <select [(ngModel)]=\"ticketStatus\" (change)=\"ChangeStatus($event.target.options)\" class=\"form-control col-md-0\" [ngModelOptions]=\"{standalone: true}\" required>\n                        <option *ngFor=\"let item of ref._lov3.refTicketStatus\" value=\"{{item.value}}\">{{item.value}}</option>\n                    </select>\n                </div>\n            </div>\n    \n            <div class=\"input-group\" style=\"padding: 25px 5px 25px 0px; float: right;\">\n                <div style=\"height: 10px;width: 10px;background-color: red;display: inline-block;\"></div> New &nbsp;&nbsp;\n                <div style=\"height: 10px;width: 10px;background-color: yellow;display: inline-block;\"></div> Pending &nbsp;&nbsp;\n                <div style=\"height: 10px;width: 10px;background-color: green;display: inline-block;\"></div> Closed &nbsp;&nbsp;\n                <div style=\"height: 10px;width: 10px;background-color: gray;display: inline-block;\"></div> Rejected\n            </div>\n        </form>\n    \n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"table-responsive\" *ngIf=\"hide==false\">\n                    <table class=\"table table-striped table-condensed table-hover tblborder\" id=\"tblstyle\">\n                        <thead>\n                            <tr>\n                                <!--<th>Status</th>-->\n                                <th>Ticket No.</th>\n                                <th>Channel</th>\n                                <th>User</th>\n                                <th>Message</th>\n                                <th class=\"center\">Image</th>\n                                <th>Location</th>\n                                <th>Date</th>    \n                                <th>Time</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                            <ng-container *ngFor=\"let obj of ticketObj.ticketData\">\n                                <tr>\n                                    <td style=\"background-color: yellow; border-left: 1px solid white; border-right: 2px solid white; font-weight: bold;\" *ngIf=\"obj.t10=='Pending'\"><a style=\"color: white;\" (click)=\"goto(obj)\">{{obj.t1}}</a></td>\n                                    <td style=\"background-color: red; border-left: 1px solid white; border-right: 2px solid white; font-weight: bold;\" *ngIf=\"obj.t10=='New'\"><a style=\"color: white;\" (click)=\"goto(obj)\">{{obj.t1}}</a></td>\n                                    <td style=\"background-color: green; border-left: 1px solid white; border-right: 2px solid white; font-weight: bold;\" *ngIf=\"obj.t10=='Closed'\"><a style=\"color: white;\" (click)=\"goto(obj)\">{{obj.t1}}</a></td>\n                                    <td style=\"background-color: gray; border-left: 1px solid white; border-right: 2px solid white; font-weight: bold;\" *ngIf=\"obj.t10=='Rejected'\"><a style=\"color: white;\" (click)=\"goto(obj)\">{{obj.t1}}</a></td>\n                                    <td *ngIf=\"obj.t4 == '1054' || obj.t4 == '1060'\">General</td>\n                                    <td *ngIf=\"obj.t4 == '1058' || obj.t4 == '1061'\">Water Pipeline</td>\n                                    <td *ngIf=\"obj.t4 == '1059' || obj.t4 == '1062'\">Gabbage Collection</td>                                    \n                                    <td class=\"uni\">{{obj.t5}}</td>\n                                    <td class=\"uni\">{{obj.t7}}</td>\n                                    <td class=\"center\"> <img *ngIf=\"obj.t13\" src=\"{{imageLink}}{{obj.t14}}{{obj.t13}}\" onError=\"this.src='./image/image_not_found.png';\" height=\"50\" width=\"50\" /></td>\n                                    <td>{{obj.t8}}</td>\n                                    <td>{{obj.date}}</td>\n                                    <td>{{obj.time}}</td>\n                                </tr>                                \n                            </ng-container>\n                        </tbody>\n                    </table>\n                </div>\n            </div>\n        </div>\n    \n        <div id=\"popup\" class=\"modal fade\" role=\"dialog\">\n            <div id=\"popupsize\" class=\"modal-dialog modal-lg\">\n                <div class=\"modal-content\">\n                <div class=\"modal-header\">\n                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n                    <h4 id=\"popuptitle\" class=\"modal-title\">Map</h4>\n                </div>\n                <div id=\"popupbody\" class=\"modal-body\">\n                    <div style=\"\">&nbsp;</div>\n                    \n                    <div class=\"form-group\">\n                        <div class=\"col-md-12\">\n                            <sebm-google-map [latitude]=\"lat\" [longitude]=\"lng\">\n                            </sebm-google-map>\n                            <div id=\"map-canvas\" class=\"col-md-12\" style=\"height:80%\">\n                            </div>\n                        </div>\n                    </div>\n    \n                    <div style=\"\">&nbsp;</div>\n                </div>\n                <div class=\"modal-footer\">\n                    <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\">Close</button>\n                </div>\n                </div>\n            </div>\n        </div>\n    </div>\n    </div>\n    </div>\n    \n    <div [hidden]=\"_mflag\">\n      <div class=\"modal\" id=\"loader\"></div>\n    </div>\n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, rp_references_1.RpReferences, router_1.Router, rp_http_service_1.RpHttpService])
    ], FrmTicketList);
    return FrmTicketList;
}());
exports.FrmTicketList = FrmTicketList;
//# sourceMappingURL=frmticket-list.component.js.map