"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
core_2.enableProdMode();
var FrmTicketComponent = (function () {
    function FrmTicketComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._util = new rp_client_util_1.ClientUtil();
        this.channelName = "";
        this.ticketStatus = "";
        this._doLoading = false;
        this.sessionTimeoutMsg = "";
        this._obj = this.getDefaultObj();
        this._sessionObj = this.getSessionObj();
        this.markers = [
            [0, 0, [], ""]
        ];
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this.checkSession();
            this.imageLink = this.ics._imageurl;
            this._obj = this.getDefaultObj();
            this.getStateList();
            this.getTicketStatus();
        }
    }
    /* getDefaultObj() {
      return {
        "sessionID": "", "userID": "", "msgCode": "", "msgDesc": "", "state": "","id":0,
        "n1": 0, "n2": 0, "t2": "", "t4": "", "t6": "", "t5": "", "t3": "", "t7": "", "t10": "",
        "t8": "", "t9": "", "t1": "", "status": "", "t20": "", "t11": "", "t12": "", "t13": "",
        "t14": "", "t15": "", "t16": "", "t17": "", "t18": "", "t19": "", "n3": 0, "n4": 0, "n5": 0,
        "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "n14": 0, "n15": 0,
        "n16": 0, "n17": 0, "n18": 0, "n19": 0, "n20": 0, "autoKey": 0, "syskey": 0, "createdDate": "",
        "modifiedDate": "", "code": "", "ticketNo": "", "senderKey": "", "senderName": "", "message": "", "location": "",
        "shortDate": "", "shortTime": "", "image": "", "channelKey": "", "receiverKey": ""
      };
    }  */
    FrmTicketComponent.prototype.getDefaultObj = function () {
        return {
            "sessionID": "", "userID": "", "msgCode": "", "msgDesc": "", "state": "", "autokey": 0,
            "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "",
            "t11": "", "t12": "", "t13": "", "t14": "", "t15": "", "t16": "", "t17": "", "t18": "", "t19": "", "t20": "",
            "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "date": "", "time": ""
        };
    };
    FrmTicketComponent.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    FrmTicketComponent.prototype.goReply = function () {
        var _this = this;
        var msgparams = {
            t2: this._obj.t20,
            t3: '',
            t5: this._obj.t3,
            t6: this._obj.t4,
            t9: '',
            t11: '',
            n1: "0",
            t12: "android",
            t15: "",
            isGroup: 0,
            n20: this._obj.t6,
            groupName: this._obj.t5,
            t19: ""
        };
        console.log("request sendmsg =" + JSON.stringify(msgparams));
        this.http.doPost(this.ics._ticketlisturl + "serviceChat/sendMessage", msgparams).subscribe(function (data) {
            if (data.totalCount > 0) {
                _this.showMsg("Sent successfully.", true);
            }
            else {
                _this.showMsg("Send fail.", false);
            }
            console.log("request sendmsg data =" + JSON.stringify(data));
        }, function (error) {
            console.log("request sendmsg error =" + JSON.stringify(error));
            alert("Connection Timed Out.");
        }, function () { });
    };
    FrmTicketComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._doLoading = true;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
                _this._obj.autokey = params['autokey'];
                _this._obj.n2 = params['n2'];
                _this._obj.t1 = params['t1'];
                _this._obj.t2 = params['t2'];
                _this._obj.t3 = params['t3'];
                _this._obj.t4 = params['t4'];
                _this._obj.t5 = params['t5'];
                _this._obj.t6 = params['t6'];
                _this._obj.t7 = params['t7'];
                _this._obj.t8 = params['t8'];
                _this._obj.t9 = params['t9'];
                _this._obj.t10 = params['t10'];
                _this._obj.t11 = params['t11'];
                _this._obj.t12 = params['t12'];
                _this._obj.t13 = params['t13'];
                _this._obj.date = params['date'];
                _this._obj.time = params['time'];
                _this.ticketStatus = _this._obj.t10;
                if (_this._obj.t4 == '1054' || _this._obj.t4 == '1060') {
                    _this.channelName = "General";
                }
                if (_this._obj.t4 == '1058' || _this._obj.t4 == '1061') {
                    _this.channelName = "Water Pipeline";
                }
                if (_this._obj.t4 == '1059' || _this._obj.t4 == '1062') {
                    _this.channelName = "Gabbage Collection";
                }
                var abc = _this._obj.t8;
                var latitudeOne = abc.substring(0, abc.indexOf("/"));
                var longitudeOne = abc.substring(abc.indexOf("/") + 1);
                var info = '';
                if (_this._obj.t5 != undefined && _this._obj.t5 != null && _this._obj.t5 != '') {
                    info = "Ticket No. " + _this._obj.t1 + " by " + _this._obj.t5;
                }
                _this.markers[0] = [latitudeOne, longitudeOne, [], info];
                var map = new google.maps.Map(document.getElementById('map-canvas'));
                var infowindow = new google.maps.InfoWindow();
                for (var i = 0; i < _this.markers.length; i++) {
                    var latlng = new google.maps.LatLng(_this.markers[i][0], _this.markers[i][1]);
                    map = new google.maps.Map(document.getElementById('map-canvas'), {
                        zoom: 15,
                        center: latlng
                    });
                }
                for (var i = 0; i < _this.markers.length; i++) {
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(_this.markers[i][0], _this.markers[i][1]),
                        map: map,
                        title: _this.markers[i][3],
                        zIndex: _this.markers[i][1]
                    });
                    infowindow = new google.maps.InfoWindow();
                    google.maps.event.addListener(marker, 'click', (function (marker, i) {
                        return function () {
                            //infowindow.setContent("Data:"+MarkerData[i][3]+"<br />"+ MarkerData[i][2]);
                            infowindow.setContent("Data:" + this.markers[i][3]);
                            infowindow.open(map, marker);
                        };
                    })(marker, i));
                }
            }
        });
    };
    // comment=we need to remove All coz all means all type of status. it will only include in ticket list form.
    FrmTicketComponent.prototype.getTicketStatus = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'TicketService/getTicketStatus';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.msgCode == "0016") {
                        _this.showMsg(data.msgDesc, false);
                        _this.logout();
                    }
                    if (data.msgCode != "0000") {
                        _this.showMsg(data.msgDesc, false);
                    }
                    if (data.msgCode == "0000") {
                        if (!(data.refTicketStatus instanceof Array)) {
                            var m = [];
                            m[0] = data.refTicketStatus;
                            _this.ref._lov3.refTicketStatus = m;
                        }
                        else {
                            _this.ref._lov3.refTicketStatus = data.refTicketStatus;
                        }
                        for (var i = 0; i < _this.ref._lov3.refTicketStatus.length; i++) {
                            if (_this.ref._lov3.refTicketStatus[i].value.includes("All")) {
                                _this.ref._lov3.refTicketStatus.splice(i, 1);
                                i--;
                            }
                        }
                    }
                }
                else {
                    alert("Please try again.");
                }
            }, function (error) {
                console.log("error=" + JSON.stringify(error));
                // if (error._body.type == "error") {
                alert("Connection Timed Out.");
                // }
            }, function () {
            });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmTicketComponent.prototype.getOneTicket = function (this_obj) {
        var _this = this;
        try {
            var urlTicket = this.ics._apiurl + 'TicketService/getOneTicket';
            var ticketList = [];
            ticketList[0] = this_obj.t1;
            var _objA = {
                "sessionID": this.ics._profile.sessionID,
                "userID": this.ics._profile.userID,
                "ticketList": ticketList
            };
            this.http.doPost(urlTicket, _objA).subscribe(function (result) {
                console.log("result=" + JSON.stringify(result));
                if (result != undefined && result != null && result != '') {
                    if (result.msgCode != undefined && result.msgCode != null && result.msgCode == "0000") {
                    }
                    else if (result.msgCode == "0009") {
                    }
                    else {
                        _this.showMsg(result.msgDesc, false);
                    }
                }
                else {
                    alert("Please try again.");
                    console.log("result else=" + JSON.stringify(result));
                }
            }, function (error) {
                console.log("error=" + JSON.stringify(error));
                // if (error._body.type == "error") {
                // }
                alert("Connection Timed Out.");
                _this._doLoading = false;
            }, function () {
                _this._doLoading = false;
            });
        }
        catch (e) {
            this._doLoading = false;
            alert("Invalid URL.");
        }
    };
    FrmTicketComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    FrmTicketComponent.prototype.goUpdate = function () {
        var _this = this;
        this._doLoading = true;
        try {
            var url = this.ics._apiurl;
            var alertMsg = "";
            url += 'TicketService/updateMyTicket';
            if (alertMsg == "") {
                var json = this._obj;
                this._obj.sessionID = this.ics._profile.sessionID;
                this._obj.userID = this.ics._profile.userID;
                this.http.doPost(url, json).subscribe(function (data) {
                    console.log("data=" + JSON.stringify(data));
                    if (data != null) {
                        if (data.msgCode == "0016") {
                            _this.showMsg(data.msgDesc, false);
                            _this.logout();
                        }
                        if (data.msgCode == "0000") {
                            _this.showMsg(data.msgDesc, true);
                            _this._obj.autokey = data.longResult[0];
                        }
                    }
                    else {
                        alert("Please try again.");
                    }
                }, function (error) {
                    console.log("error=" + JSON.stringify(error));
                    alert("Connection Timed Out.");
                    _this._doLoading = false;
                }, function () {
                    _this._doLoading = false;
                });
            }
            else {
                console.log("this._obj=" + JSON.stringify(this._obj));
                this._doLoading = false;
                alert(alertMsg);
            }
        }
        catch (e) {
            console.log("this._obj=" + JSON.stringify(this._obj) + "/r/n/////" + JSON.stringify(e));
            this._doLoading = false;
            alert("Invalid URL.");
        }
    };
    FrmTicketComponent.prototype.goList = function () {
        this._router.navigate(['/ticketList']);
    };
    FrmTicketComponent.prototype.goDelete = function () {
        var _this = this;
        this._doLoading = true;
        try {
            var url = this.ics._apiurl + 'TicketService/deleteTicket';
            this._obj.sessionID = this.ics._profile.sessionID;
            this._obj.userID = this.ics._profile.userID;
            var json = this._obj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (_this._obj.msgCode == "0016") {
                        _this.showMsg(_this._obj.msgDesc, false);
                        _this.logout();
                    }
                    if (_this._obj.msgCode != "0000") {
                        _this.showMsg(_this._obj.msgDesc, false);
                    }
                    if (_this._obj.msgCode == "0000") {
                        var temp = _this._obj;
                        _this.showMsg(_this._obj.msgDesc, true);
                        _this._obj = _this.getDefaultObj();
                        _this._obj.n1 = temp.n1;
                        _this._obj.n2 = temp.n2;
                        _this._obj.t1 = temp.t1;
                        _this._obj.t2 = temp.t2;
                        _this._obj.t3 = temp.t3;
                        _this._obj.t4 = temp.t4;
                        _this._obj.t5 = temp.t5;
                        _this._obj.t6 = temp.t6;
                        _this._obj.t7 = temp.t7;
                        _this._obj.t8 = temp.t8;
                        _this._obj.t9 = temp.t9;
                        _this._obj.t10 = temp.t10;
                        _this._obj.t12 = temp.status;
                        _this.ticketStatus = _this._obj.t12;
                    }
                }
                else {
                    alert("Please try again.");
                }
            }, function (error) {
                console.log("error=" + JSON.stringify(error));
                alert("Connection Timed Out.");
                _this._doLoading = false;
            }, function () {
                _this._doLoading = false;
            });
        }
        catch (e) {
            this._doLoading = false;
            alert("Invalid URL.");
        }
    };
    FrmTicketComponent.prototype.getStateList = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics._apiurl + 'service001/getStateListByUserID?type=&userID=' + this.ics._profile.userID).subscribe(function (response) {
                if (response.refstate != null && response.refstate != undefined) {
                    //this.ref._lov3.refstate = [{ "value": "", "caption": "" }];
                    _this.ref._lov3.refstate = [];
                    if (!(response.refstate instanceof Array)) {
                        var m = [];
                        m[0] = response.data;
                        _this.ref._lov3.refstate = m;
                    }
                    else {
                        _this.ref._lov3.refstate = response.refstate;
                    }
                }
                else {
                    _this.ref._lov3.refstate = [{ "value": "", "caption": "Empty" }];
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Error!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmTicketComponent.prototype.checkSession = function () {
        var _this = this;
        this._doLoading = true;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsg(data.desc, false);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsg(data.desc, false);
                    }
                }
            }, function (error) {
                console.log("error=" + JSON.stringify(error));
                // if (error._body.type == "error") {
                // }
                alert("Connection Timed Out.");
                _this._doLoading = false;
            }, function () {
                _this._doLoading = false;
            });
        }
        catch (e) {
            this._doLoading = false;
            alert("Invalid URL.");
        }
    };
    FrmTicketComponent.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    /* showMsgAlert(msg) {
      this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    } */
    FrmTicketComponent.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    FrmTicketComponent = __decorate([
        core_1.Component({
            selector: 'ticket',
            template: "\n  <div class=\"container\">\n  <div class=\"row clearfix\">\n  <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n    <form class=\"form-horizontal\">\n      <div class=\"col-md-12\">\n        <!--this is currently form-->\n        <legend>Ticket </legend>\n      </div>\n  \n      <div class=\"col-md-12\">\n          <button class=\"btn btn-primary\" id=\"vh_list_btn\" type=\"button\" (click)=\"goList()\">List</button>\n          <button class=\"btn btn-primary\" id=\"vh_save_btn\" type=\"button\" (click)=\"goUpdate()\">Update</button>\n          <!--<button class=\"btn btn-primary\" disabled id=\"vh_delete_btn\" type=\"button\" (click)=\"goDelete()\">Delete</button>-->\n      </div>\n  \n      <div class=\"col-md-12\">&nbsp;</div>\n      \n      <div class=\"col-md-6\">\n        <div class=\"form-group\">\n          <label class=\"col-md-4\">Status </label>\n          <div class=\"col-md-8\">\n              <select [(ngModel)]=\"_obj.t10\" class=\"form-control col-md-0\" [ngModelOptions]=\"{standalone: true}\" required>\n                <option *ngFor=\"let item of ref._lov3.refTicketStatus\" value=\"{{item.value}}\">{{item.value}}</option>\n              </select>\n          </div>\n        </div>\n  \n        <div class=\"form-group\">\n            <label class=\"col-md-4\">Ticket No.</label>\n            <div class=\"col-md-8\">\n                <input disabled readonly type=\"text\" [(ngModel)]=\"_obj.t1\" class=\"form-control\" [ngModelOptions]=\"{standalone: true}\">\n            </div>\n        </div>\n  \n        <div class=\"form-group\">\n            <label class=\"col-md-4\">Channel </label>\n            <div class=\"col-md-8\">\n              <div class=\"uni\">\n                <input disabled readonly type=\"text\" [(ngModel)]=\"channelName\" class=\"form-control\" [ngModelOptions]=\"{standalone: true}\">\n              </div>\n            </div>\n        </div>\n  \n        <div class=\"form-group\">\n            <label class=\"col-md-4\">User </label>\n            <div class=\"col-md-8\">\n              <div class=\"uni\">\n                <input disabled readonly type=\"text\" [(ngModel)]=\"_obj.t5\" class=\"form-control\" [ngModelOptions]=\"{standalone: true}\">\n              </div>\n            </div>\n        </div>\n  \n        <div class=\"form-group\">\n            <label class=\"col-md-4\">Message </label>\n            <div class=\"col-md-8\">\n              <div class=\"uni\">\n                <textarea disabled readonly id=\"textarea-custom\" class=\"form-control\" rows=\"3\" [(ngModel)]=\"_obj.t7\" [ngModelOptions]=\"{standalone: true}\"></textarea>\n              </div>\n            </div>\n        </div>\n  \n        <div class=\"form-group\">\n            <label class=\"col-md-4\">Location </label>\n            <div class=\"col-md-8\">\n                <input disabled readonly type=\"text\" [(ngModel)]=\"_obj.t8\" class=\"form-control\" [ngModelOptions]=\"{standalone: true}\">\n            </div>\n        </div>\n      \n      <div class=\"form-group\">\n        <label class= \"col-md-4\">Region</label>\n        <div class= \"col-md-8\">                  \n          <select disabled readonly [(ngModel)]=\"_obj.t12\"class=\"form-control\" [ngModelOptions]=\"{standalone: true}\" required>\n            <option *ngFor=\"let c of ref._lov3.refstate\" value=\"{{c.value}}\">{{c.caption}}</option>\n          </select>\n        </div>\t\t\t\t\t\t           \n        </div>\n  \n        <div class=\"form-group\">\n            <label class=\"col-md-4\">Date </label>\n            <div class=\"col-md-8\">\n                <input disabled readonly type=\"text\" [(ngModel)]=\"_obj.date\" class=\"form-control\" [ngModelOptions]=\"{standalone: true}\">\n            </div>\n        </div>\n  \n        <div class=\"form-group\">\n            <label class=\"col-md-4\">Time </label>\n            <div class=\"col-md-8\">\n                <input disabled readonly type=\"text\" [(ngModel)]=\"_obj.time\" class=\"form-control\" [ngModelOptions]=\"{standalone: true}\">\n            </div>\n        </div>\n            \n        <div class=\"form-group\">\n            <label class=\"col-md-4\">Image </label>\n            <div class=\"col-md-8\">\n              <img src=\"{{imageLink}}{{_obj.t14}}{{_obj.t13}}\" onError=\"this.src='./image/image_not_found.png';\" alt={{img}} width=\"100%\" />\n            </div>\n        </div>\n      </div>\n      \n      <div class=\"col-md-6\">\n        <div class=\"form-group\">\n            <label class=\"col-md-4\">Reply Message </label>\n            <div class=\"col-md-8\">\n              <div class=\"uni\">\n                <textarea id=\"textarea-custom\" class=\"form-control\" rows=\"3\" [(ngModel)]=\"_obj.t20\" [ngModelOptions]=\"{standalone: true}\"></textarea>\n              </div>\n            </div>\n        </div>\n  \n        <div class=\"form-group\">\n          <div class=\"col-md-4\"></div>\n          <div class=\"col-md-8\">\n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"goReply()\">Send</button>\n          </div>\n        </div>\n  \n        <div class=\"form-group\">\n          <div class=\"col-md-12\">\n            <sebm-google-map [latitude]=\"lat\" [longitude]=\"lng\"></sebm-google-map>\n            <div id=\"map-canvas\" class=\"col-md-12\" style=\"height:80%\"></div>\n          </div>\n        </div>\n      </div>\n    </form>\n  </div>\n  </div>\n  </div>\n  \n  <div [hidden]=\"!_doLoading\">\n    <div class=\"modal\" id=\"loader\"></div>\n  </div>\n"
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmTicketComponent);
    return FrmTicketComponent;
}());
exports.FrmTicketComponent = FrmTicketComponent;
//# sourceMappingURL=frmticket.component.js.map