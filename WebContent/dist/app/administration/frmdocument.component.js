"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
core_2.enableProdMode();
var Frmdocument = (function () {
    //flagImg = false;
    //addCrop = [];
    // cropobj = { id: "", name: "" };
    function Frmdocument(el, renderer, ics, _router, route, http, ref) {
        this.el = el;
        this.renderer = renderer;
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._viewsk = 0;
        this._mflag = false;
        this.flagview = true;
        this.flagvideo = false;
        this.flagURL = true;
        this.flagImage = true;
        this.flagImage1 = false;
        this.flagnew = true;
        this.flagsave = true;
        this.flagdelete = true;
        //tableflag=0;
        this.dates = { "_date": null };
        this.uploadDataList = [];
        //uploadDataListResize = [];
        this.uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
        this._img = "";
        this._file = null;
        this._fileName = '';
        this._Time = 0;
        this._util = new rp_client_util_1.ClientUtil();
        this._time = "";
        this.flagcanvas = true;
        this._imageObj = [{ "value": "", "caption": "", "flag": false }];
        this._obj = { "autokey": 0, "createdDate": "", "createdTime": null, "modifiedDate": "", "modifiedTime": null, "title": "", "status": "Draft", "fileSize": "", "fileName": "", "filePath": "", "fileType": "Law", "region": "", "userId": "", "userName": "", "recordStatus": 0, "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "postedby": "", "modifiedby": "", "uploadlist": [] };
        this._sessionObj = this.getSessionObj();
        this._roleval = "";
        this._checkcw = false;
        this._checkeditor = false;
        this._checkpubisher = false;
        this._checkmaster = false;
        this._checkadmin = false;
        this._StateType = "";
        this.stateobj = { id: "", name: "" };
        this.town_obj = { id: "", name: "" };
        this._result = { "longResult": "", "msgDesc": "", "key": [], "keyResult": 0, "state": "", "stringResult": "" };
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        this._roleval = ics._profile.t1;
        this._datepickerOpts = this.ics._datepickerOpts;
        this._datepickerOpts.format = 'dd/mm/yyyy';
        this.flagdelete = true;
        this.flagnew = false;
        this.flagsave = false;
        this.flagview = true;
        this.flagURL = true;
        this.flagImage = true;
        this.flagImage1 = false;
        this.flagvideo = false;
        this._mflag = true;
        this.getStateList();
        //this._time = this._util.getTodayTime();
        //this._obj.createdTime = this._time;
        //this._obj.modifiedTime = this._time;
        if (this.ics._profile.loginStatus == 1) {
            this.ref._lov3.postingstate = [];
            this.ref._lov3.postingstate = this.ref._lov3.postingstate0;
            this._checkcw = true;
        }
        if (this.ics._profile.loginStatus == 2) {
            this.ref._lov3.postingstate = [];
            this.ref._lov3.postingstate = this.ref._lov3.postingstate1;
            this._checkeditor = true;
        }
        if (this.ics._profile.loginStatus == 3) {
            this.ref._lov3.postingstate = [];
            this.ref._lov3.postingstate = this.ref._lov3.postingstate2;
            this._checkpubisher = true;
        }
        if (this.ics._profile.loginStatus == 4) {
            this.ref._lov3.postingstate = this.ref._lov3.postingstate;
            this._checkmaster = true;
        }
        if (this.ics._profile.loginStatus == 5) {
            this.ref._lov3.postingstate = this.ref._lov3.postingstate;
            this._checkadmin = true;
        }
    }
    Frmdocument.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    Frmdocument.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "new") {
                _this.goNew();
            }
            else if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this.goGet(id);
                _this._viewsk = id;
            }
        });
    };
    Frmdocument.prototype.goNew = function () {
        this.flagvideo = false;
        this.flagURL = true;
        this.flagImage = true;
        this.flagImage1 = false;
        //this.tableflag=0;
        jQuery("#pdfUpload").val("");
        jQuery("#save").prop("disabled", false);
        this.flagview = true;
        this.flagdelete = true;
        this.flagnew = false;
        this.flagsave = false;
        this._obj = { "autokey": 0, "createdDate": "", "createdTime": null, "modifiedDate": "", "modifiedTime": null, "title": "", "status": "Draft", "fileSize": "", "fileName": "", "filePath": "", "fileType": "Law", "region": "ALL", "userId": "", "userName": "", "recordStatus": 0, "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "postedby": "", "modifiedby": "", "uploadlist": [] };
        this._result = { "longResult": "", "msgDesc": "", "key": [], "keyResult": 0, "state": "", "stringResult": "" };
    };
    Frmdocument.prototype.goDelete = function () {
        var _this = this;
        this._mflag = false;
        if (this._obj.filePath != "") {
            this._obj.modifiedby = this.ics._profile.userID;
            var url = this.ics._apiurl + 'serviceVideo/deleteDoc';
            var json = this._obj;
            this.http.doPost(url, json).subscribe(function (data) {
                _this.showMessage(data.msgDesc, data.state);
                if (data.state) {
                    _this.goNew();
                    _this._mflag = true;
                }
            }, function (error) { }, function () { });
        }
        else {
            this._mflag = true;
            this.showMessage("No PDF to Delete!", undefined);
        }
    };
    Frmdocument.prototype.getStateList = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics._apiurl + 'service001/getStateListByUserID?type=all&userID=' + this.ics._profile.userID).subscribe(function (response) {
                if (response.refstate != null && response.refstate != undefined) {
                    _this.ref._lov3.refstate = [];
                    if (!(response.refstate instanceof Array)) {
                        var m = [];
                        m[0] = response.refstate;
                        _this.ref._lov3.refstate = m;
                    }
                    else {
                        _this.ref._lov3.refstate = response.refstate;
                    }
                }
                else {
                    _this.ref._lov3.refstate = [{ "value": "", "caption": "Empty" }];
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Error!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    Frmdocument.prototype.goPost = function () {
        var _this = this;
        this._mflag = false;
        console.log("profile=" + JSON.stringify(this.ics._profile));
        console.log("obj=" + JSON.stringify(this._obj));
        if ((this._checkcw && this._obj.status.toString() != "6") || (this._checkeditor && this._obj.status.toString() != "2") || (this._checkpubisher && this._obj.status.toString() != "4") || (this._checkmaster) || (this._checkadmin)) {
            //this._obj.createdTime = this._time;
            //this._obj.modifiedTime = this._time;
            if (this.isValidate(this._obj)) {
                if (this._obj.autokey != 0) {
                    this._obj.modifiedby = this.ics._profile.userID;
                }
                else {
                    this._obj.postedby = this.ics._profile.userID;
                }
                var url = this.ics._apiurl + 'serviceVideo/savemyDoc';
                var json = this._obj;
                console.log("json=" + JSON.stringify(json));
                this.http.doPost(url, json).subscribe(function (data) {
                    if (data != null && data != undefined) {
                        _this._result = data;
                        _this.showMessage(data.msgDesc, data.state);
                        _this._obj.autokey = data.longResult[0];
                        _this._mflag = true;
                    }
                    else {
                        _this._mflag = true;
                        _this.showMessage("Data Not Found", false);
                    }
                }, function (error) {
                    _this._mflag = true;
                    console.log("data error=");
                    _this.showMessage("Can't Saved This Record!!!", undefined);
                }, function () {
                    console.log("data error");
                });
            }
        }
        else {
            this.showMessage("Can't Save This Status!!!", undefined);
        }
    };
    Frmdocument.prototype.uploadedFile = function (event) {
        var _this = this;
        this._mflag = false;
        //this.uploadDataListResize = [];
        if (event.target.files.length == 1) {
            this._fileName = event.target.files[0].name;
            this._file = event.target.files[0];
            var index = this._fileName.lastIndexOf(".");
            var imagename = this._fileName.substring(index);
            imagename = imagename.toLowerCase();
            if (imagename == ".pdf" || imagename == ".doc" || imagename == ".docx" || imagename == ".pptx") {
                console.log("imgUrl: " + this.ics._imgurl);
                var url = this.ics._apiurl + 'file/fileupload?f=upload&fn=' + this._fileName + '&id=' + this.ics._profile.t1 + '&type=10' + '&imgUrl=' + this.ics._imgurl;
                this.http.upload(url, this._file).subscribe(function (data) {
                    if (data.code === 'SUCCESS') {
                        //this.flagImg = true;
                        console.log("PDF files: " + JSON.stringify(data));
                        _this.showMessage("Upload Successful!!!", true);
                        var _img = _this._fileName;
                        _this.uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
                        _this.uploadData.name = data.fileName;
                        _this.uploadData.url = data.url;
                        _this._obj.filePath = _this.uploadData.url.substring(0, _this.uploadData.url.lastIndexOf("/") + 1);
                        _this._obj.fileName = _this.uploadData.url.substring(_this.uploadData.url.lastIndexOf("/") + 1);
                        _this._obj.uploadlist[0] = _this.uploadData;
                        //this.uploadDataListResize.push(data.sfileName);
                        _this._fileName = "";
                        _this._mflag = true;
                    }
                    else {
                        _this._mflag = true;
                        _this.showMessage("Upload Unsuccessful!!! Please Try Again...", false);
                    }
                }, function (error) { }, function () { });
            }
            else {
                this.showMessage("Choose File Associated!", false);
                this._mflag = true;
            }
        }
        else {
            this.showMessage("Upload Fail!", undefined);
            this._mflag = true;
        }
    };
    Frmdocument.prototype.isValidate = function (obj) {
        if (obj.title == "") {
            this.showMessage("Please fill Content!!!", false);
            this._mflag = true;
            return false;
        }
        if (obj.status == "") {
            this.showMessage("Choose Status!!!", false);
            this._mflag = true;
            return false;
        }
        if (obj.fileType == "") {
            this.showMessage("Choose file Type!!!", false);
            this._mflag = true;
            return false;
        }
        if (obj.t2 == "") {
            this.showMessage("Choose Region!!!", false);
            this._mflag = true;
            return false;
        }
        return true;
    };
    Frmdocument.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    Frmdocument.prototype.goGet = function (p) {
        var _this = this;
        this._mflag = false;
        this.flagview = false;
        this.flagdelete = false;
        this.flagnew = false;
        this.flagsave = false;
        this._obj.createdTime = this._time;
        this._obj.modifiedTime = this._time;
        this.http.doGet(this.ics._apiurl + 'serviceVideo/DocBysyskey?key=' + p).subscribe(function (data) {
            //let url: string = this.ics._apiurl + 'serviceVideo/DocBysyskey?key=' + p;
            // this._sessionObj.sessionID=this.ics._profile.sessionID;
            // this._sessionObj.userID=this.ics._profile.userID;
            //let json: any = this._sessionObj;
            //this.http.doPost(url, json).subscribe(
            // data => {
            _this._obj = data;
            console.log("data obj=" + JSON.stringify(data));
            if (_this.ics._profile.loginStatus == 1) {
                _this.ref._lov3.postingstate = [];
                _this.ref._lov3.postingstate = _this.ref._lov3.readpoststatuecon;
            }
            if (_this.ics._profile.loginStatus == 2) {
                _this.ref._lov3.postingstate = [];
                _this.ref._lov3.postingstate = _this.ref._lov3.readpoststatueeditor;
            }
            if (_this.ics._profile.loginStatus == 3) {
                _this.ref._lov3.postingstate = [];
                _this.ref._lov3.postingstate = _this.ref._lov3.readpostpublisher;
            }
            if (_this.ics._profile.loginStatus == 4) {
                _this.ref._lov3.postingstate = [];
                _this.ref._lov3.postingstate = _this.ref._lov3.postingstateadmin;
            }
            if (_this.ics._profile.loginStatus == 5) {
                _this.ref._lov3.postingstate = [];
                _this.ref._lov3.postingstate = _this.ref._lov3.postingstateadmin;
            }
            _this._mflag = true;
        }, function (error) { }, function () { });
    };
    Frmdocument.prototype.goList = function () {
        this._router.navigate(['/documentlist']);
    };
    Frmdocument.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    Frmdocument = __decorate([
        core_1.Component({
            selector: 'fmrdocument',
            template: "\n  <div class=\"container\">\n  <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n    <form class=\"form-horizontal\" ngNoForm>\n        <legend>Document</legend>\n        <div class=\"row  col-md-12\">\n          <div style=\"float: left;\">\n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"goList()\">List</button>\n            <button class=\"btn btn-primary\" [disabled]=\"flagnew\" id=\"new\" type=\"button\" (click)=\"goNew()\">New</button>\n            <button class=\"btn btn-primary\" [disabled]=\"flagsave\" id=\"save\" type=\"button\" (click)=\"goPost()\">Save</button>\n            <button class=\"btn btn-primary\" [disabled]=\"flagdelete\" id=\"delete\" type=\"button\" (click)=\"goDelete();\">Delete</button>\n          </div>\n        </div>\n        <div class=\"row col-md-12\">&nbsp;</div>\n\t\t\t<div class=\"form-group\">\n\t\t\t\t<label class=\"col-md-2\">Title</label>\n\t\t\t\t<div class=\"col-md-4\">\n\t\t\t\t\t<input type=\"text\" [(ngModel)]=\"_obj.title\" class=\"form-control input-md uni\" />\n\t\t\t\t</div>\n\t\t\t\t<label class= \"col-md-2\">Status</label>\n\t\t\t\t\t<div class= \"col-md-4\">                  \n\t\t\t\t\t\t<select [(ngModel)]=\"_obj.status\"  class=\"form-control\" [ngModelOptions]=\"{standalone: true}\" required>\n\t\t\t\t\t\t\t<option *ngFor=\"let c of ref._lov1.postingstate\" value=\"{{c.caption}}\">{{c.caption}}</option>\n\t\t\t\t\t\t</select>\n\t\t\t\t\t</div>\n\t\t\t\t<!--<rp-input rpLabelClass = \"col-md-2\" [(rpModel)]=\"_obj.status\" rpRequired=\"true\" rpType=\"postingstate\" rpLabel=\"Status\" autofocus></rp-input>-->\n\t\t\t</div> \n\t\t\t<div class=\"form-group\">\n\t\t\t  <!--<label class=\"col-md-2\">File Path(.pdf)</label>\n\t\t\t  <div class=\"col-md-4\">\n\t\t\t\t<input id=\"pdfUpload\" class=\"form-control input-md\" type=\"file\" (change)=\"uploadedFile($event)\">\n\t\t\t  </div>-->\n\t\t\t\t\n\t\t\t\t<label class=\"col-md-2\" for=\"filenameid\">File Path(.pdf)</label>\n\t\t\t\t<div class=\"col-md-4\" id=\"filenameid\">\n\t\t\t\t\t<input type=\"file\" class=\"file\"(change)=\"uploadedFile($event)\" >\n\t\t\t\t\t\t<div class=\"input-group\">\n\t\t\t\t\t\t\t<input type=\"text\" id=\"pdfUpload\"  class=\"form-control\" placeholder=\"No file chosen\">\n\t\t\t\t\t\t\t<span class=\"input-group-btn\">\n\t\t\t\t\t\t\t\t<button class=\"browse btn btn-primary\" type=\"button\"><i class=\"glyphicon glyphicon-search\"></i> Browse</button>\n\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t</div>\t\n\t\t\t\t</div>\n\t\t\t\t\n\t\t\t  <label class=\"col-md-2\">File Size</label>\n\t\t\t  <div class=\"col-md-4\">\n\t\t\t\t<input type=\"text\" [(ngModel)]=\"_obj.fileSize\" class=\"form-control input-md\" />\n\t\t\t  </div>\n\t\t\t</div>\n\t\t\t<div class=\"form-group\">\n\t\t\t\t<label class= \"col-md-2\">File Type</label>\n\t\t\t\t\t<div class= \"col-md-4\">                  \n\t\t\t\t\t\t<select [(ngModel)]=\"_obj.fileType\"  class=\"form-control\" [ngModelOptions]=\"{standalone: true}\" required>\n\t\t\t\t\t\t\t<option *ngFor=\"let c of ref._lov1.filetype\" value=\"{{c.caption}}\">{{c.caption}}</option>\n\t\t\t\t\t\t</select>\n\t\t\t\t\t</div>\t\n\t\t\t\t<label class= \"col-md-2\">Region</label>\n\t\t\t\t\t<div class= \"col-md-4\">                  \n\t\t\t\t\t\t<select [(ngModel)]=\"_obj.t2\"class=\"form-control\" [ngModelOptions]=\"{standalone: true}\" required>\n\t\t\t\t\t\t\t<option *ngFor=\"let c of ref._lov3.refstate\" value=\"{{c.value}}\">{{c.caption}}</option>\n\t\t\t\t\t\t</select>\n\t\t\t\t\t</div>\t\t\t\t\t\n\t\t\t\t<!--<rp-input rpLabelClass = \"col-md-2\" [(rpModel)] =\"_obj.t2\" rpRequired=\"true\" rpType=\"statecombo\" rpLabel=\"Region\"></rp-input>-->\n\t\t\t</div>\n\t\t\t<div class=\"form-group\">\n\t\t\t\t<label class=\"col-md-2\">Keyword</label>\n\t\t\t\t<div class=\"col-md-4\">\n\t\t\t\t\t<input type=\"text\" [(ngModel)]=\"_obj.t1\" class=\"form-control input-md uni\" />\n\t\t\t\t</div>\t\t\t\n\t\t\t</div> \n\t\t\n\t\t<div class=\"row col-md-6\">\n            <table class=\"table table-striped table-condensed table-hover tblborder\" id=\"tblstyle\">\n                <thead>\n                    <tr>\n                        <th>No.</th>\n\t\t\t\t\t\t<th>File Name</th>\n                    </tr>\n                </thead>\n                <tbody><!--*ngFor=\"let obj of _obj;let i = index\"-->\n                    <tr *ngIf=\"_obj.fileName!=''\">\n                        <td>1</td>\n\t\t\t\t\t\t<td><a target=\"_blank\" rel=\"noopener noreferrer\" href=\"{{_obj.filePath}}{{_obj.fileName}}\">{{_obj.fileName}}</a></td>\n                    </tr>\n                </tbody>\n            </table>\n        </div>\n    </form>\n    </div>\n  </div>\n</div>\n<div [hidden]=\"_mflag\">\n<div class=\"modal\" id=\"loader\"></div>\n</div>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n  "
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef, core_1.Renderer, rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], Frmdocument);
    return Frmdocument;
}());
exports.Frmdocument = Frmdocument;
//# sourceMappingURL=frmdocument.component.js.map