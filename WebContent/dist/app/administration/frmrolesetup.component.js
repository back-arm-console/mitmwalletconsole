"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var Rx_1 = require('rxjs/Rx');
core_2.enableProdMode();
var FrmRoleSetupComponent = (function () {
    function FrmRoleSetupComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this.sessionAlertMsg = "";
        this.parentsyskey = [];
        this.chkparentsyskey = [];
        this.childsyskey = [];
        this._obj = this.getDefaultObj();
        this._before = this._obj;
        this._result = { "longResult": "", "msgCode": "", "msgDesc": "", "key": [], "keyResult": 0, "state": "", "stringResult": "" };
        this._keyobj = { "_key": "", "isParent": "", "sessionId": "" };
        this._key = "";
        this._output1 = "";
        this._mflag = false;
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = true;
            this.checkSession();
            this.getRoleMenuList();
            this._obj = this.getDefaultObj();
        }
    }
    FrmRoleSetupComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this._key = id;
                _this.goReadBySyskey(id);
            }
        });
    };
    FrmRoleSetupComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    FrmRoleSetupComponent.prototype.getDefaultObj = function () {
        jQuery("#mydelete").prop("disabled", true);
        jQuery("#mySave").prop("disabled", false);
        return { "syskey": 0, "t1": "", "t2": "", "n3": "", "userId": "", "userName": "", "msgCode": "", "msgDesc": "", "sessionId": "", "childsyskey": [], "parentsyskey": [], "menu": [{ "syskey": 0, "t2": "", "n2": 0, "result": false, "childmenus": [{ "syskey": 0, "t2": "", "n2": 0, "result": false }] }] };
    };
    FrmRoleSetupComponent.prototype.goReadBySyskey = function (p) {
        var _this = this;
        try {
            this._mflag = false;
            var url = this.ics._apiurl + 'service001/getRoleData?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
            this.http.doPost(url, p).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMessage();
                }
                else {
                    _this._obj = data;
                    var p_1 = data.menu.length;
                    for (var i = 0; i < p_1; i++) {
                        if (data.menu[i].result)
                            _this._obj.menu[i].result = true;
                        else
                            _this._obj.menu[i].result = false;
                        if (data.menu[i].childmenus != undefined && data.menu[i].childmenus != null) {
                            var c = data.menu[i].childmenus.length;
                            for (var j = 0; j < c; j++) {
                                if (data.menu[i].childmenus[j].result)
                                    _this._obj.menu[i].childmenus[j].result = true;
                                else
                                    _this._obj.menu[i].childmenus[j].result = false;
                            }
                        }
                    }
                    _this.parentsyskey = [];
                    _this.childsyskey = [];
                    _this.parentsyskey = _this.parentsyskey.concat(_this._obj.parentsyskey);
                    _this.childsyskey = _this.childsyskey.concat(_this._obj.childsyskey);
                    _this.chkparentsyskey = _this.chkparentsyskey.concat(_this._obj.parentsyskey);
                    jQuery("#mydelete").prop("disabled", false);
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmRoleSetupComponent.prototype.goList = function () {
        this._router.navigate(['/rolesetuplist']);
    };
    FrmRoleSetupComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this.goLogOut();
        });
    };
    FrmRoleSetupComponent.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    /* checkSession() {
      try {
  
        let url: string = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
        this.http.doGet(url).subscribe(
          data => {
            if (data.msgCode == '0016') {
              this.sessionAlertMsg = data.msgDesc;
              this.showMessage();
            }
          },
          error => {
            if (error._body.type == 'error') {
              alert("Connection Timed Out!");
            }
            else {
  
            }
          }, () => { });
      } catch (e) {
        alert("Invalid URL");
      }
    } */
    FrmRoleSetupComponent.prototype.showMessageAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmRoleSetupComponent.prototype.clearData = function () {
        this._key = "";
        this.parentsyskey = [];
        this.childsyskey = [];
        this._obj.syskey = 0;
        this._obj.t1 = "";
        this._obj.t2 = "";
        this._obj.n3 = "";
        this._obj.childsyskey = [];
        this._obj.parentsyskey = [];
        this._obj.msgCode = "";
        this._obj.msgDesc = "";
        this._obj.sessionId = "";
        var p = this._obj.menu.length;
        for (var i = 0; i < p; i++) {
            this._obj.menu[i].result = false;
            if (this._obj.menu[i].childmenus != undefined && this._obj.menu[i].childmenus != null) {
                var c = this._obj.menu[i].childmenus.length;
                for (var j = 0; j < c; j++) {
                    this._obj.menu[i].childmenus[j].result = false;
                }
            }
        }
        jQuery("#mydelete").prop("disabled", true);
        jQuery("#mySave").prop("disabled", false);
    };
    FrmRoleSetupComponent.prototype.changeModule = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex; //Get Selected Index
        var value = options[options.selectedIndex].value; //Get Selected Index's Value  
        if (value == "ALL" || value == "") {
            this._obj.n3 = "ALL";
        }
        else {
            this._obj.n3 = value;
            for (var i = 1; i < this.ref._lov1.ref025.length; i++) {
                if (this.ref._lov1.ref025[i].value == value) {
                    this._obj.n3 = this.ref._lov1.ref025[i].value;
                }
            }
        }
    };
    FrmRoleSetupComponent.prototype.getRoleMenuList = function () {
        var _this = this;
        try {
            this._mflag = false;
            this.http.doGet(this.ics._apiurl + 'service001/getRoleMenuList?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMessage();
                }
                else {
                    _this._obj = data;
                    var p = data.menu.length;
                    for (var i = 0; i < p; i++) {
                        _this._obj.menu[i].result = false;
                        if (data.menu[i].childmenus != undefined || data.menu[i].childmenus != null) {
                            var c = data.menu[i].childmenus.length;
                            for (var j = 0; j < c; j++) {
                                _this._obj.menu[i].childmenus[j].result = false;
                            }
                        }
                    }
                    _this._output1 = JSON.stringify(data);
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmRoleSetupComponent.prototype.getParentValue = function (indexno, value, event) {
        if (event.target.checked) {
            this.chkparentsyskey.push(value);
            var indexx = this.chkparentsyskey.indexOf(value);
            //if parentmenu is check, check all childmenu
            if (this._obj.menu[indexno].childmenus != undefined) {
                for (var i = 0; i < this._obj.menu[indexno].childmenus.length; i++) {
                    this._obj.menu[indexno].childmenus[i].result = true;
                    this.childsyskey.push(this._obj.menu[indexno].childmenus[i].syskey);
                    this._obj.childsyskey = this.childsyskey;
                }
            }
            this.parentsyskey.push(value);
        }
        else {
            var indexx = this.parentsyskey.indexOf(value);
            this.parentsyskey.splice(indexx, 1);
            //if parentmenu is not check, uncheck all childmenu
            if (this._obj.menu[indexno].childmenus != undefined) {
                for (var i = 0; i < this._obj.menu[indexno].childmenus.length; i++) {
                    this._obj.menu[indexno].childmenus[i].result = false;
                    var indexing = this.childsyskey.indexOf(this._obj.menu[indexno].childmenus[i].syskey);
                    if (indexing != -1)
                        this.childsyskey.splice(indexing, 1);
                }
            }
        }
        this._obj.parentsyskey = this.parentsyskey;
    };
    FrmRoleSetupComponent.prototype.getChildValue = function (value, event) {
        if (event.target.checked) {
            this.childsyskey.push(value);
        }
        else {
            var indexx = this.childsyskey.indexOf(value);
            this.childsyskey.splice(indexx, 1);
        }
        this._obj.childsyskey = this.childsyskey;
    };
    FrmRoleSetupComponent.prototype.goDelete = function () {
        var _this = this;
        try {
            this._mflag = false;
            var url = this.ics._apiurl + 'service001/deleteRole?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
            var json = this._key;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMessage();
                }
                else {
                    if (data.msgDesc == 'Deleted successfully') {
                        _this.clearData();
                    }
                    _this.showMessageAlert(data.msgDesc);
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmRoleSetupComponent.prototype.goSave = function () {
        var _this = this;
        try {
            this._mflag = false;
            this._obj.userId = this.ics._profile.userID;
            this._obj.sessionId = this.ics._profile.sessionID;
            var url = this.ics._apiurl + 'service001/saveRole';
            var json = this._obj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMessage();
                }
                else {
                    _this._output1 = JSON.stringify(data);
                    if (data.msgDesc == 'Saved successfully' || data.msgDesc == 'Updated successfully') {
                        _this._obj.syskey = data.keyResult;
                        _this._key = _this._obj.syskey + "";
                        jQuery("#mydelete").prop("disabled", false);
                    }
                    if (data.msgDesc == 'Please select menu') {
                        _this.parentsyskey = [];
                        _this.childsyskey = [];
                    }
                    _this.showMessageAlert(data.msgDesc);
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmRoleSetupComponent.prototype.goNew = function () {
        this.clearData();
    };
    FrmRoleSetupComponent.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    FrmRoleSetupComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsgAlert(data.desc);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsgAlert(data.desc);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmRoleSetupComponent.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmRoleSetupComponent.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    FrmRoleSetupComponent = __decorate([
        core_1.Component({
            selector: 'role-setup',
            template: "\n  <div class=\"container\">\n  <div class=\"row clearfix\">\n  <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n    <form class= \"form-horizontal\" (ngSubmit) = \"goSave()\"> \n  <!-- Form Name -->\n      <legend>Role </legend>\n        <div class=\"row  col-md-12\">  \n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"goList()\" >List</button> \n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"goNew()\" >New</button>      \n            <button class=\"btn btn-primary\" id=\"mySave\" type=\"submit\" >Save</button>          \n            <button class=\"btn btn-primary\" disabled id=\"mydelete\"  type=\"button\" (click)=\"goDelete();\" >Delete</button> \n        </div>\n        \n        <div class=\"row col-md-12\">&nbsp;</div> \n\n        <div class=\"form-group\">    \n          <div class=\"col-md-10\">\n            <div class=\"form-group\">\n              <label class=\"col-md-3\" > Module <font class=\"mandatoryfont\">*</font> </label>\n              <div class=\"col-md-4\" > \n                <select [(ngModel)]=_obj.n3 (change)=\"changeModule($event)\"  [ngModelOptions]=\"{standalone: true}\" class=\"form-control col-md-0\" required >\n                <option *ngFor=\"let item of ref._lov1.ref025\" value=\"{{item.value}}\" >{{item.caption}}</option> \n                </select> \n              </div>    \n            </div>\n \n            <div class=\"form-group\">\n              <label class=\"col-md-3\">Code <font size=\"4\" color=\"#FF0000\" >*</font></label>\n                 <div class=\"col-md-4\">\n                   <input  class=\"form-control\" type = \"text\" [(ngModel)]=\"_obj.t1\" required=\"true\" *ngIf=\"_obj.syskey == 0 \"  [ngModelOptions]=\"{standalone: true}\"> \n                   <input  class=\"form-control\" type = \"text\" [(ngModel)]=\"_obj.t1\" required=\"true\" *ngIf=\"_obj.syskey != 0 \" readonly  [ngModelOptions]=\"{standalone: true}\">     \n                 </div>\n              </div>      \n\n            <div class=\"form-group\">        \n              <rp-input  rpType=\"text\" rpLabel=\"Description\" [(rpModel)]=\"_obj.t2\" rpLabelRequired=\"true\" rpRequired=\"true\"></rp-input>\n            </div>\n\n            <div class=\"form-group\">\n              <label class=\"col-md-3\">Menu <font class=\"mandatoryfont\">*</font></label> \n              <div class=\"col-md-4\" > </div>\n            </div>\n\n          <div class=\"form-group\">  \n          <div class=\"col-md-12\">\n          <ul  style=\"list-style:none;\">\n            <li *ngFor=\"let parentmenu of _obj.menu, let i=index\">\n            <div class=\"form-group\">\n            \n              <div *ngIf=\"parentmenu.syskey!=0\">\n              <input type=\"checkbox\"  [(ngModel)]=parentmenu.result (change)=\"getParentValue(i,parentmenu.syskey,$event)\" [ngModelOptions]=\"{standalone: true}\">             \n              <!-- <rp-input rpClass=\"col-md-0\"  [(rpModel)]=parentmenu.result rpType=\"checkbox\" (change)=\"getParentValue(i,parentmenu.syskey,$event)\"></rp-input> -->\n              {{parentmenu.t2}} \n              </div>\n             \n            </div>\n            <div *ngFor=\"let pvalue of parentsyskey\">\n              <div *ngIf=\"parentmenu.syskey==pvalue\">\n              <ul style=\"list-style:none;\">         \n                <li *ngFor=\"let childmenu of parentmenu.childmenus\">\n                <div class=\"form-group\"> \n                  <div *ngIf=\"childmenu.syskey!=0\">\n                  <!--  <rp-input rpClass=\"col-md-0\" [(rpModel)]=childmenu.result rpType=\"checkbox\" (change)=\"getChildValue(childmenu.syskey,$event)\"></rp-input> -->\n                  <input type=\"checkbox\"  [(ngModel)]=childmenu.result (change)=\"getChildValue(childmenu.syskey,$event)\" [ngModelOptions]=\"{standalone: true}\">             \n                  {{childmenu.t2}} \n                  </div>                  \n                </div>                 \n                </li> \n              </ul>\n              </div>\n              </div>\n            </li>  \n            </ul>  \n          </div>\n          </div>\n\n        </div>\n      </div>\n    </form>\n  </div>\n  </div>\n</div>\n     \n<div [hidden] = \"_mflag\">\n<div class=\"modal\" id=\"loader\"></div>\n</div> \n\n\n  <div id=\"sessionalert\" class=\"modal fade\">\n    <div class=\"modal-dialog\">\n      <div class=\"modal-content\">\n        <div class=\"modal-body\"><p>{{sessionAlertMsg}}</p></div>\n        <div class=\"modal-footer\"></div>\n      </div>\n    </div>\n  </div>\n\n \n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmRoleSetupComponent);
    return FrmRoleSetupComponent;
}());
exports.FrmRoleSetupComponent = FrmRoleSetupComponent;
//# sourceMappingURL=frmrolesetup.component.js.map