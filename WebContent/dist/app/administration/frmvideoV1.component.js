"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
core_2.enableProdMode();
var FrmVideoComponent = (function () {
    //addCrop = [];
    // cropobj = { id: "", name: "" };
    function FrmVideoComponent(el, renderer, ics, _router, route, http, ref) {
        this.el = el;
        this.renderer = renderer;
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._viewsk = 0;
        this.flagview = true;
        this.flagvideo = false;
        this.flagURL = true;
        this.flagImage = true;
        this.flagImage1 = false;
        this.flagnew = true;
        this.flagsave = true;
        this.flagdelete = true;
        this.dates = { "_date": null };
        this.uploadDataList = [];
        this.uploadDataListResize = [];
        this.uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
        this._img = "";
        this._file = null;
        this._fileName = '';
        this._Time = 0;
        this._util = new rp_client_util_1.ClientUtil();
        this._time = "";
        this.flagcanvas = true;
        this._imageObj = [{ "value": "", "caption": "", "flag": false }];
        //_crop = [{ "value": "", "caption": "", "flag": false }];
        this._obj = { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": null, "modifiedDate": "", "modifiedTime": null, "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "Video", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [], "videoUpload": [], "cropdata": null, "agrodata": null, "ferdata": null, "towndata": null, "crop": null, "fert": null, "agro": null, "addTown": null, "resizelist": [], "modifiedUserId": "", "modifiedUserName": "", "uploadlist": [] };
        this._roleval = "";
        this._checkcw = false;
        this._checkeditor = false;
        this._checkpubisher = false;
        this._checkmaster = false;
        this._checkadmin = false;
        this.flagImg = false;
        this.stateobj = { id: "", name: "" };
        this.town_obj = { id: "", name: "" };
        this._result = { "longResult": "", "msgDesc": "", "key": [], "keyResult": 0, "state": "", "stringResult": "" };
        // getCropList() {
        //     this.http.doGet(this.ics._apiurl + 'serviceCrop/getSenderList').subscribe(
        //         response => {
        //             if (response != null && response != undefined) {
        //                 this._crop = response.data;
        //             }
        //         },
        //         error => { },
        //         () => { }
        //     );
        // }
        this.obj = { id: "", name: "" };
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        this._roleval = ics._profile.t1;
        this._datepickerOpts = this.ics._datepickerOpts;
        this._datepickerOpts.format = 'dd/mm/yyyy';
        this.flagdelete = true;
        this.flagnew = false;
        this.flagsave = false;
        this.flagview = true;
        this.flagURL = true;
        this.flagImage = true;
        this.flagImage1 = false;
        this.flagvideo = false;
        //this.flagImg = true;
        // this.setBtns();
        // this.getCropList();
        this.getStateList();
        this._time = this._util.getTodayTime();
        this._obj.createdTime = this._time;
        this._obj.modifiedTime = this._time;
        if (this.ics._profile.loginStatus == 1) {
            this.ref._lov3.postingstate = [];
            this.ref._lov3.postingstate = this.ref._lov3.postingstate0;
            this._checkcw = true;
        }
        if (this.ics._profile.loginStatus == 2) {
            this.ref._lov3.postingstate = [];
            this.ref._lov3.postingstate = this.ref._lov3.postingstate1;
            this._checkeditor = true;
        }
        if (this.ics._profile.loginStatus == 3) {
            this.ref._lov3.postingstate = [];
            this.ref._lov3.postingstate = this.ref._lov3.postingstate2;
            this._checkpubisher = true;
        }
        if (this.ics._profile.loginStatus == 4) {
            this.ref._lov3.postingstate = this.ref._lov3.postingstate;
            this._checkmaster = true;
        }
        if (this.ics._profile.loginStatus == 5) {
            this.ref._lov3.postingstate = this.ref._lov3.postingstate;
            this._checkadmin = true;
        }
    }
    FrmVideoComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "new") {
                _this.goNew();
            }
            else if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this.goGet(id);
                _this._viewsk = id;
            }
        });
    };
    FrmVideoComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    FrmVideoComponent.prototype.goGet = function (p) {
        var _this = this;
        this.flagview = false;
        this.flagdelete = false;
        this.flagnew = false;
        this.flagsave = false;
        this._obj.createdTime = this._time;
        this._obj.modifiedTime = this._time;
        //this.http.doGet(this.ics._apiurl + 'serviceVideo/readVideoBySyskey?key=' + p).subscribe(
        this.http.doGet(this.ics._apiurl + 'serviceQuestion/readBySyskey?key=' + p).subscribe(function (data) {
            _this._obj = data;
            console.log("data obj=" + JSON.stringify(data));
            if (_this.ics._profile.loginStatus == 1) {
                _this.ref._lov3.postingstate = [];
                _this.ref._lov3.postingstate = _this.ref._lov3.readpoststatuecon;
            }
            if (_this.ics._profile.loginStatus == 2) {
                _this.ref._lov3.postingstate = [];
                _this.ref._lov3.postingstate = _this.ref._lov3.readpoststatueeditor;
            }
            if (_this.ics._profile.loginStatus == 3) {
                _this.ref._lov3.postingstate = [];
                _this.ref._lov3.postingstate = _this.ref._lov3.readpostpublisher;
            }
            if (_this.ics._profile.loginStatus == 4) {
                _this.ref._lov3.postingstate = [];
                _this.ref._lov3.postingstate = _this.ref._lov3.postingstateadmin;
            }
            if (_this.ics._profile.loginStatus == 5) {
                _this.ref._lov3.postingstate = [];
                _this.ref._lov3.postingstate = _this.ref._lov3.postingstateadmin;
            }
            if (_this._obj.n10 == 0) {
                _this.flagvideo = false;
                _this.flagURL = true;
                _this.flagImage = true;
                _this.flagImage1 = false;
            }
            if (_this._obj.n10 == 1 || _this._obj.n10 == 2) {
                //if (this._obj.n10 == 1) {
                _this.flagURL = false;
                _this.flagvideo = true;
                _this.flagImage = false;
                _this.flagImage1 = true;
            }
            for (var i = 0; i < _this._obj.upload.length; i++) {
                var index = _this._obj.upload[i].lastIndexOf(".");
                var fe = _this._obj.upload[i].substring(index);
            }
            if (_this._obj.resizelist.length > 0) {
                _this.uploadDataListResize = _this._obj.resizelist;
                _this._obj.resizelist = [];
            }
            if (_this._obj.uploadlist[0].name != "") {
                _this.flagImg = true;
            }
            else {
                _this.flagImg = false;
            }
            // for (let i = 0; i < this._crop.length; i++) {
            //     for (let j = 0; j < this._obj.cropdata.length; j++) {
            //         if (this._obj.cropdata[j] == this._crop[i].caption) {
            //             this._crop[i].flag = true;
            //             this.goAddCrop(this._crop[i].value, this._crop[i].caption);
            //             break;
            //         }
            //         else {
            //             this._crop[i].flag = false;
            //         }
            //     }
            // }
        }, function (error) { }, function () { });
    };
    FrmVideoComponent.prototype.goNew = function () {
        this.flagvideo = false;
        this.flagURL = true;
        this.flagImage = true;
        this.flagImage1 = false;
        jQuery("#uploadVideo").val("");
        jQuery("#imageUpload").val("");
        jQuery("#save").prop("disabled", false);
        this.flagview = true;
        this.flagdelete = true;
        this.flagnew = false;
        this.flagsave = false;
        this._obj.videoUpload = [];
        this._obj = { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": null, "modifiedDate": "", "modifiedTime": null, "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "Video", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [], "videoUpload": [], "cropdata": null, "agrodata": null, "ferdata": null, "towndata": null, "crop": null, "fert": null, "agro": null, "addTown": null, "resizelist": [], "modifiedUserId": "", "modifiedUserName": "", "uploadlist": [] };
        // for (let i = 0; i < this._crop.length; i++) {
        //     this._crop[i].flag = false;
        // }
        // this.addCrop = [];
    };
    FrmVideoComponent.prototype.goPost = function () {
        var _this = this;
        console.log("profile=" + JSON.stringify(this.ics._profile));
        console.log("obj=" + JSON.stringify(this._obj));
        if ((this._checkcw && this._obj.n7.toString() != "6") || (this._checkeditor && this._obj.n7.toString() != "2") || (this._checkpubisher && this._obj.n7.toString() != "4") || (this._checkmaster) || (this._checkadmin)) {
            // for (let j = 0; j < this._obj.videoUpload.length; j++) {
            //     this._img = capture(this._obj.videoUpload[j]);
            //     this._obj.upload.push(this._img);
            //     this.flagcanvas = false;
            //     this._img = "";
            // }
            this._obj.createdTime = this._time;
            this._obj.modifiedTime = this._time;
            this._obj.resizelist = this.uploadDataListResize;
            //this._obj.crop = this.addCrop;
            if (this.isValidate(this._obj)) {
                if (this._obj.syskey != 0) {
                    //alert("modified!!!");
                    this._obj.modifiedUserId = this.ics._profile.userID;
                    this._obj.modifiedUserName = this.ics._profile.userName;
                }
                else {
                    //alert("not modified!!!");
                    this._obj.userId = this.ics._profile.userID;
                    this._obj.userName = this.ics._profile.userName;
                }
                var url = this.ics._apiurl + 'serviceVideo/saveVideo';
                this._obj.n5 = Number(this.ics._profile.t1);
                var json = this._obj;
                console.log("json=" + JSON.stringify(json));
                if (isMyanmar(json.t2)) {
                    if (identifyFont(json.t2) == "zawgyi") {
                        json.t2 = ZgtoUni(json.t2);
                    }
                }
                this.http.doPost(url, json).subscribe(function (data) {
                    console.log("data=" + JSON.stringify(data));
                    _this._result = data;
                    _this.showMessage(data.msgDesc, data.state);
                    _this._obj.syskey = data.longResult[0];
                }, function (error) {
                    console.log("data error=");
                    _this.showMessage("Can't Saved This Record!!!", undefined);
                }, function () {
                    console.log("data error");
                    //this.showMessage("Something wrong in service",undefined);
                });
            }
        }
        else {
            this.showMessage("Can't Save This Status!!!", undefined);
        }
    };
    FrmVideoComponent.prototype.goDelete = function () {
        var _this = this;
        if (this._obj.t2 != "") {
            //  let url: string = this.ics._apiurl + 'serviceVideo/deleteVideo';
            var url = this.ics._apiurl + 'serviceQuestion/deleteQuestion';
            var json = this._obj;
            this.http.doPost(url, json).subscribe(function (data) {
                _this.showMessage(data.msgDesc, data.state);
                if (data.state) {
                    _this.goNew();
                }
            }, function (error) { }, function () { });
        }
        else {
            this.showMessage("No Article to Delete!", undefined);
        }
    };
    FrmVideoComponent.prototype.goList = function () {
        this._router.navigate(['/videoList']);
    };
    FrmVideoComponent.prototype.uploadedFile = function (event) {
        var _this = this;
        this.flagcanvas = true;
        if (event.target.files.length == 1) {
            this._fileName = event.target.files[0].name;
            this._file = event.target.files[0];
            var index = this._fileName.lastIndexOf(".");
            var videoname = this._fileName.substring(index);
            videoname = videoname.toLowerCase();
            if (videoname == ".mp4" || videoname == ".flv" || videoname == ".webm" || videoname == ".3gp" || videoname == ".3gp2" || videoname == ".mpeg4" || videoname == ".mpeg" || videoname == ".wmv" || videoname == ".avi") {
                jQuery("#imagepopup").modal();
                var url = this.ics._apiurl + 'file/fileupload?f=upload&fn=' + this._fileName + '&id=' + this.ics._profile.t1 + '&type=10&imgUrl=' + this.ics._imgurl; // "http://localhost:8080/DigitalConsole/"
                this.http.upload(url, this._file).subscribe(function (data) {
                    jQuery("#imagepopup").modal('hide');
                    if (data.code === 'SUCCESS') {
                        _this.flagImg = true;
                        _this.popupMessage("Upload Successful!!!");
                        _this._obj.videoUpload[0] = data.fileName;
                        _this._fileName = "";
                    }
                    else {
                        _this.popupMessage("Upload Unsuccessful!!! Please Try Again...");
                    }
                }, function (error) { }, function () { });
            }
            else {
                this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": "Choose video Associated!" });
            }
        }
        else {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "info", "t3": "Upload File!" });
        }
    };
    FrmVideoComponent.prototype.uploadedFileImage = function (event) {
        var _this = this;
        this.uploadDataListResize = [];
        if (event.target.files.length == 1) {
            this._fileName = event.target.files[0].name;
            this._file = event.target.files[0];
            var index = this._fileName.lastIndexOf(".");
            var imagename = this._fileName.substring(index);
            imagename = imagename.toLowerCase();
            if (imagename == ".jpg" || imagename == ".jpeg" || imagename == ".png") {
                console.log("imgUrl: " + this.ics._imgurl);
                var url = this.ics._apiurl + 'file/fileupload?f=upload&fn=' + this._fileName + '&id=' + this.ics._profile.t1 + '&type=10' + '&imgUrl=' + this.ics._imgurl;
                this.http.upload(url, this._file).subscribe(function (data) {
                    if (data.code === 'SUCCESS') {
                        _this.flagImg = true;
                        console.log("videoImage: " + JSON.stringify(data));
                        _this.popupMessage("Upload Successful!!!");
                        var _img = _this._fileName;
                        _this.uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
                        _this.uploadData.name = data.fileName;
                        _this.uploadData.url = data.url;
                        //  this.uploadDataList.push(this.uploadData);
                        //this._obj.uploadlist.push( this.uploadData);
                        _this._obj.uploadlist[0] = _this.uploadData;
                        _this.uploadDataListResize.push(data.sfileName);
                        _this._fileName = "";
                    }
                    else {
                        _this.popupMessage("Upload Unsuccessful!!! Please Try Again...");
                    }
                }, function (error) { }, function () { });
            }
            else {
                this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": "Choose Image Associated!" });
            }
        }
        else {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "info", "t3": "Upload File!" });
        }
    };
    FrmVideoComponent.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    FrmVideoComponent.prototype.isValidate = function (obj) {
        if (obj.t2 == "") {
            this.showMessage("Please fill Content!!!", false);
            return false;
        }
        if (obj.n10 == 0 && obj.upload)
            if (!this._util.validateLanguage(obj.t2) && !this._util.validateEng(obj.t2)) {
                this.showMessage("Please change Myanmar3 Font at Content !!!", false);
                return false;
            }
        if (obj.t8 == "" && (obj.n10 == 1 || obj.n10 == 2)) {
            //        if (obj.t8 == "" && obj.n10 == 1) {
            this.showMessage("Please fill URL Link!!!", false);
            return false;
        }
        // if ((obj.upload == null || obj.upload.length == 0) && obj.n10 == 1) {
        //     this.showMessage("Please fill Associaed Image!!!", false);
        //     return false;
        if ((obj.videoUpload == null || obj.videoUpload.length == 0) && obj.n10 == 0) {
            this.showMessage("Choose Associated Video!!!", false);
            return false;
        }
        if (obj.n7 == "") {
            this.showMessage("Choose Status!!!", false);
            return false;
        }
        return true;
    };
    FrmVideoComponent.prototype.setBtns = function () {
        var k = this.ics.getBtns("/videoList");
        if (k != "" && k != undefined) {
            var strs = k.split(",");
            for (var i = 0; i < strs.length; i++) {
                if (strs[i] == "1") {
                    this.flagnew = false;
                }
                if (strs[i] == "2") {
                    this.flagsave = false;
                }
            }
        }
    };
    FrmVideoComponent.prototype.setImgUrl = function (img) {
        var abc = this.ics._imgurl + '/upload/video/' + img;
        console.log("abc=" + abc);
        return abc;
    };
    FrmVideoComponent.prototype.popupMessage = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", t2: "Video Information", t3: msg });
    };
    FrmVideoComponent.prototype.popupMessage1 = function (msg) {
        this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": "Upload Successfully!" });
    };
    FrmVideoComponent.prototype.toggleVideo = function (event) {
        this.videoplayer.nativeElement.play();
    };
    // goRemoveChekallCrop(p) {
    //     for (var i = 0; i < this._crop.length; i++) {
    //         this._crop[i].flag = false;
    //     }
    //     this.addCrop = [];
    // }
    // goAddChekAllCrop() {
    //     for (var i = 0; i < this._crop.length; i++) {
    //         this._crop[i].flag = true;
    //         this.cropobj = { id: "", name: "" };
    //         this.cropobj.id = this._crop[i].value;
    //         this.cropobj.name = this._crop[i].caption;
    //         this.addCrop.push(this.cropobj);
    //     }
    // }
    // checkboxCrop(event, p1, p2) {
    //     if (event.target.checked) {
    //         if (p2 == "All") {
    //             this.goAddChekAllCrop();
    //         }
    //         else {
    //             this.goAddCrop(p1, p2);
    //         }
    //     } else if (!event.target.checked) {
    //         if (p2 == "All") {
    //             this.goRemoveChekallCrop(p1)
    //         }
    //         else {
    //             this.goRemoveCrop(p1);
    //         }
    //     }
    // }
    // goRemoveCrop(p) {
    //     var temp = [], j = 0;
    //     for (var i = 0; i < this.addCrop.length; i++) {
    //         if (p == this.addCrop[i].id) {
    //             this.addCrop[i].id = "";
    //             this.addCrop[i].name = "";
    //         }
    //     }
    //     for (var i = 0; i < this.addCrop.length; i++) {
    //         if (this.addCrop[i].id != "") {
    //             this.cropobj = { id: "", name: "" };
    //             this.cropobj.id = this.addCrop[i].id;
    //             this.cropobj.name = this.addCrop[i].name;
    //             temp[j] = this.cropobj; j++;
    //         }
    //     }
    //     this.addCrop = temp;
    // }
    // goAddCrop(p1, p2) {
    //     this.cropobj = { id: "", name: "" };
    //     this.cropobj.id = p1;
    //     this.cropobj.name = p2;
    //     this.addCrop.push(this.cropobj);
    // }
    FrmVideoComponent.prototype.goView = function (p) {
        this._router.navigate(['/videoAdminView', 'read', p]);
    };
    FrmVideoComponent.prototype.goRemove = function (obj, num) {
        this._fileName = "";
        var img = obj;
        this._obj.videoUpload = [];
    };
    FrmVideoComponent.prototype.changeBrowe = function (p) {
        if (p == 0) {
            this.flagvideo = false;
            this.flagURL = true;
            this.flagImage = true;
            this.flagImage1 = false;
        }
        else {
            this.flagvideo = true;
            this.flagURL = false;
            this.flagImage = false;
            this.flagImage1 = true;
        }
    };
    FrmVideoComponent.prototype.deleteFile = function (obj) {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'file/fileRemove?fn=' + obj).subscribe(function (data) {
            console.log(data.code);
            if (data.code === 'SUCCESS') {
                _this._obj.n10 = 0;
                var index = _this.uploadDataList.indexOf(obj);
                _this.uploadDataList.splice(index, 1);
                _this.showMessage('Photo Removed!', true);
            }
        }, function (error) { }, function () { });
    };
    /*  try {
          this.http.doGet(this.ics._apiurl + 'service001/getStateListNew').subscribe(
            response => {
              if (response != null && response != undefined) {
                this.ref._lov3.refstate = response.refstate;
              }
              else {
                this.ref._lov3.refstate = [{ "value": "", "caption": "Empty" }];
              }
            },
            error => {
              if (error._body.type == 'error') {
                alert("Connection Error!");
              }
            }, () => { });
        } catch (e) {
          alert("Invalid URL");
        } */
    FrmVideoComponent.prototype.getStateList = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics._apiurl + 'service001/getStateListNew').subscribe(function (response) {
                if (response.refstate != null && response.refstate != undefined) {
                    _this.ref._lov3.refstate = [{ "value": "", "caption": "ALL" }];
                    for (var i = 1; i < response.refstate.length; i++) {
                        _this.ref._lov3.refstate[i] = response.refstate[i - 1];
                    }
                }
                else {
                    _this.ref._lov3.refstate = [{ "value": "", "caption": "Empty" }];
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Error!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmVideoComponent = __decorate([
        core_1.Component({
            selector: 'fmr-video',
            template: "\n    <div class=\"container\">\n    <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n    <form class= \"form-horizontal\" ngNoForm> \n        <legend>Video</legend>\n        <div class=\"row  col-md-12\"> \n        <div  style=\"float: left;\"> \n            <button class=\"btn btn-primary\" [disabled]=\"flagnew\" id=\"new\" type=\"button\" (click)=\"goNew()\">New</button>      \n            <button class=\"btn btn-primary\" [disabled]=\"flagsave\" id=\"save\" type=\"button\" (click)=\"goPost()\">Save</button> \n            <button class=\"btn btn-primary\" [disabled]=\"flagdelete\" id=\"delete\" type=\"button\" (click)=\"goDelete();\" >Delete</button> \n            <button class=\"btn btn-primary\"  type=\"button\"   (click)=\"goList()\">List</button> \n        </div>\n        <div style=\"float: left; padding-left : 3px;\" [hidden]=\"flagview\"> \n            <button class=\"btn btn-primary\"  type=\"button\"  (click)=\"goView(_viewsk);\" >View</button> \n        </div> \n        </div>\n        <div class=\"row col-md-12\">&nbsp;</div>\n        <div class=\"form-group\">\n            <label class=\"col-md-2\">Title</label>\n            <div class=\"col-md-4\">\n                <input type=\"text\" [(ngModel)]=\"_obj.t1\" class=\"form-control input-md\"/>\n            </div>\n            <rp-input [(rpModel)]=\"_obj.t3\" rpLabelClass = \"col-md-2\"   rpRequired =\"true\" rpType=\"videoType\" rpLabel=\"Type\"  rpReadonly=\"true\"></rp-input> \n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-md-2\">Content</label>\n            <div class=\"col-md-4\">\n                <textarea type=\"text\" [(ngModel)]=\"_obj.t2\" class=\"form-control input-md\" rows=\"6\" ></textarea>\n            </div>\n                <!--<div class=\"col-md-6\">&nbsp;</div>-->\n                <label class=\"col-md-2\">video Duration Time</label>\n                <div class=\"col-md-4\">\n                <input type=\"text\" [(ngModel)]=\"_obj.t7\" class=\"form-control input-md\"/>\n                </div>\n                <div class=\"col-md-6\">&nbsp;</div>\n                <rp-input rpLabelClass = \"col-md-2\" [(rpModel)]=\"_obj.n7\" rpRequired =\"true\"   rpType=\"postingstate\" rpLabel=\"Status\"  autofocus ></rp-input> \n        </div>\n\n\n\n        <div class=\"form-group\" >\n            <label class=\"col-md-2\">File Size</label>\n            <div class=\"col-md-4\">\n                <input type=\"text\" [(ngModel)]=\"_obj.t6\" class=\"form-control input-md\"/>\n            </div>\n            <label class=\"col-md-2\"  [hidden]=\"flagImage1\">Image Associated (.jpeg, .png, .jpg)</label>\n            <div class=\"col-md-4\" [hidden]=\"flagImage1\">\n                <input type=\"file\" id=\"imageUpload\" class=\"form-control input-md\" (change)=\"uploadedFileImage($event)\" placeholder=\"Upload file...\" />\n            </div>\n            <label class=\"col-md-2\"  [hidden]=\"flagImage\">Image Associated (.jpeg, .png, .jpg)</label>\n            <div class=\"col-md-4\" [hidden]=\"flagImage\">\n                <input type=\"file\" id=\"imageUpload\" class=\"form-control input-md\" (change)=\"uploadedFileImage($event)\" placeholder=\"Upload file...\" />\n            </div>  \n        </div>\n\n        <div class=\"form-group\">\n            <rp-input [(rpModel)]=\"_obj.n10\" rpLabelClass = \"col-md-2\"  (change)=\"changeBrowe(_obj.n10)\" rpRequired =\"true\" rpType=\"videostatus\" rpLabel=\"Status Type\"  ></rp-input>\n            <label class=\"col-md-2\" [hidden]=\"flagvideo\">Video Associated (.mp4)</label>\n            <div class=\"col-md-4\" [hidden]=\"flagvideo\">\n                <input type=\"file\" id=\"uploadVideo\" class=\"form-control input-md\" (change)=\"uploadedFile($event)\" placeholder=\"Upload file...\" />\n            </div>\n        </div>\n\n        <div class=\"form-group\">\n            <rp-input [(rpModel)]=\"_obj.t5\" rpLabelClass = \"col-md-2\"  (change)=\"changeBrowe(_obj.t5)\" rpRequired =\"true\" rpType=\"statecombo\" rpLabel=\"Region\" ></rp-input>\n        </div>\n\n        <div class=\"form-group\" >\n            <label class=\"col-md-2\" [hidden]=\"flagURL\">URL Link</label>\n            <div class=\"col-md-4\" [hidden]=\"flagURL\">\n                <input type=\"text\" [(ngModel)]=\"_obj.t8\" class=\"form-control input-md\"/>\n            </div>\n        </div>\n\n<!-- <div *ngIf = \"(_obj.n10 == 1)\" class=\"form-group\"> -->\n        <div *ngIf = \"(_obj.n10 == 1) || (_obj.n10 == 2)\" class=\"form-group\">\n            <div *ngFor=\"let img of _obj.uploadlist\">\n                <img src={{img.url}} onError=\"this.src='./image/image_not_found.png';\" alt={{img}} height=\"240\" width=\"400\" />\n            </div>\n        </div>\n\n        <div>\n            <div class=\"col-md-6\" *ngFor=\"let img of _obj.videoUpload;let num = index\">  \n                <video id=\"{{img}}\" controls  height=\"240\" width=\"400\" type=\"video/mp4\">\n                    <source src=\"{{setImgUrl(img)}}\" type=\"video/mp4\" />\n                </video>                     \n                    <canvas id=\"canvas\" style=\"display:none\" ></canvas>  \n            </div>\n            \n            <div class=\"col-md-6\" *ngIf = \"_obj.n10 == 0 && flagImg\" >\n                <div *ngFor=\"let img of _obj.uploadlist\">\n                    <img src={{img.url}} onError=\"this.src='./image/image_not_found.png';\" alt={{img}} height=\"240\" width=\"400\" />\n                </div>\n            </div>\n        </div>\n\n    </form>\n    </div>\n    </div>\n    </div>\n\n    <!-- processing image modal -->\n    <div id=\"imagepopup\" class=\"modal fade\" role=\"dialog\" style=\" margin-top: 200px; margin-left: 150px;\">\n        <div id=\"imagepopupsize\" class=\"modal-dialog modal-lg\" style=\"width : 240px\">\n            <div class=\"modal-content\">\n                <div id=\"imagepopupbody\" class=\"modal-body\">\n                    <img src=\"image/processing.gif\" style=\"padding-top:30px;\">\n                </div>\n            </div>\n        </div>\n    </div>\n  "
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef, core_1.Renderer, rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmVideoComponent);
    return FrmVideoComponent;
}());
exports.FrmVideoComponent = FrmVideoComponent;
//# sourceMappingURL=frmvideoV1.component.js.map