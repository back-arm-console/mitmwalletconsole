"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var platform_browser_1 = require("@angular/platform-browser");
core_1.enableProdMode();
var FrmDocumentList = (function () {
    function FrmDocumentList(ics, ref, _router, http, sanitizer) {
        this.ics = ics;
        this.ref = ref;
        this._router = _router;
        this.http = http;
        this.sanitizer = sanitizer;
        this._totalcount = 1;
        this._searchVal = "";
        this._util = new rp_client_util_1.ClientUtil();
        this._obj = { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": null, "modifiedDate": "", "modifiedTime": null, "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "Video", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [], "videoUpload": [], "cropdata": null, "agrodata": null, "ferdata": null, "towndata": null, "crop": null, "fert": null, "agro": null, "addTown": null, "modifiedUserId": "", "modifiedUserName": "" };
        this._array = [];
        this._pager = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 0, "size": 0, "totalcount": 1, "t1": "", "userid": "" };
        this._roleval = "";
        this.mstatus = 0;
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        this._roleval = ics._profile.t1;
        //this.mstatus = ics._profile.loginStatus;
        this.search(this._pager);
    }
    FrmDocumentList.prototype.search = function (p) {
        var _this = this;
        if (p.end == 0) {
            p.end = this.ics._profile.n1;
        }
        if (p.size == 0) {
            p.size = this.ics._profile.n1;
        }
        p.t1 = this._searchVal;
        p.userid = this.ics._profile.userID;
        var url = this.ics._apiurl + 'serviceVideo/searchDocumentList?searchVal=' + this._searchVal;
        var json = p;
        this.http.doPost(url, json).subscribe(function (response) {
            if (response != null && response != undefined && response.state) {
                _this._totalcount = response.totalCount;
                _this._array = response.data;
                for (var i = 0; i < _this._array.length; i++) {
                    _this._array[i].modifiedDate = _this._util.changeStringtoDateFromDB(_this._array[i].modifiedDate);
                }
            }
            else {
                _this._array = [];
                _this._totalcount = 1;
                _this.showMsgAlert("Data not found!");
            }
        }, function (error) { }, function () { });
    };
    /*  goDownloadFile(p){
         var loginuser = this.ics._profile.userID+"~"+this.ics._profile.userName;
         var downloadOK = false;
         // public
         if(this._mydocData.n7 == 0){
           // download
           downloadOK = true;
         }
         // private
         else if(this._mydocData.n7 == 1){
           if(loginuser == this._mydocData.t20){//admin
             //download
             downloadOK = true;
           }
         }
         // private shared
         else if(this._mydocData.n7 == 2){
           if(loginuser == this._mydocData.t20){
             downloadOK = true;
           }
           else{
             for(var i=0; i<this._v5personlist.length; i++){
               if(this.ics._profile.userID == this._v5personlist[i].empID){
                 // download
                 downloadOK = true;
                 break;
               }
             }
           }
         }
         if(downloadOK){
           var user = this._mydocData.t20.split("~");
           this.downloadUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.ics._apiurl + 'serviceVideo/downloadFile?fileName='+p+'&status='
                              +this._mydocData.n7.toString()+"&empid="+user[0]+"&domain="+"");
         }
         else{
             this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": "You can't download this file!" });
           //pms.showNotification('top', 'right', "You can't download this file!", 'danger');
         }
       } */
    FrmDocumentList.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmDocumentList.prototype.searchVal = function () {
        this._pager = { "current": 1, "prev": 1, "last": 1, "next": 1, "start": 1, "end": 0, "size": 0, "totalcount": 1, "t1": "", "userid": "" };
        this.search(this._pager);
    };
    FrmDocumentList.prototype.goto = function (p) {
        this._router.navigate(['/document', 'read', p]);
    };
    FrmDocumentList.prototype.goNew = function () {
        this._router.navigate(['/document', 'new']);
    };
    FrmDocumentList.prototype.changedPager = function (event) {
        var k = event.flag;
        this._pager = event.obj;
        if (k) {
            this.search(this._pager);
        }
    };
    FrmDocumentList.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    FrmDocumentList = __decorate([
        core_1.Component({
            selector: 'fmr-documentList',
            template: "\n    <div class=\"container\">\n    <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n        <form class=\"form-inline\">\n        <!--<div class=\"col-md-12\">-->\n                <legend>Document List</legend>\n        <!--</div>-->\n            <div class=\"input-group\" style=\"padding-right: 10px;\">\n                <span class=\"input-group-btn input-md\">\n                    <button class=\"btn btn-primary\" type=\"button\" (click)=\"goNew();\">New</button>\n                </span>  \n                <input id=\"textinput\" name=\"textinput\" type=\"text\" placeholder=\"Search\" [(ngModel)]=\"_searchVal\" (keyup.enter)=\"searchVal()\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control input-md\">\n                <span class=\"input-group-btn input-md\">\n                    <button class=\"btn btn-primary input-md\" type=\"button\" (click)=\"searchVal()\" >\n                        <span class=\"glyphicon glyphicon-search\"></span>Search\n                    </button>\n                </span> \n            </div> \n        </form>\n    \n        <pager id=\"pgarticle\" [(rpModel)]=\"_totalcount\" [(rpCurrent)]=\"_pager.current\" (rpChanged)=\"changedPager($event)\"></pager> \n        \n        <div class=\"table-responsive\">\n            <table class=\"table table-striped table-condensed table-hover tblborder\" style=\"font-size:14px;\" >\n                <thead>\n                    <tr>\n                        <th width=\"20%\">ID</th>\n                        <th width=\"20%\">Title</th>\n                        <th width=\"30%\">File Name</th>\n                        <th width=\"10%\">Status</th>\n                        <th width=\"20%\">Date</th> \n\t\t\t\t\t\t<th width=\"20%\">Time</th> \t\t\t\t\t\t\n                        <th width=\"10%\">Posted By</th>\n                        <th width=\"10%\">Modified By</th>\n                    </tr>\n                </thead>\n                <tbody *ngFor=\"let obj of _array;let i = index\">\n                    <tr>\n                        <td><a (click)=\"goto(obj.autokey)\" class=\"uni\">{{(i+1)}}</a></td>\n                        <td>{{obj.title}}</td>\n                        <!--<td (click)=\"goDownloadFile(obj)\"><u class=\"uni\">{{obj.fileName}}</u></td>\t-->\n\t\t\t\t\t\t<td><a target=\"_blank\" rel=\"noopener noreferrer\" href=\"{{obj.filePath}}{{obj.fileName}}\">{{obj.fileName}}</a></td>\n                        <td><p class=\"uni\">{{obj.status}}</td>\n                        <td><p class=\"uni\">{{obj.modifiedDate}}</p></td> \n\t\t\t\t\t\t<td><p class=\"uni\">{{obj.modifiedTime}}</p></td> \t\t\t\t\t\t\n                        <td class=\"uni\"><span style=\"color:gray;font-style:italic;font-size:11px\">{{obj.userName}}</span></td>\n                        <td class=\"uni\"><span style=\"color:gray;font-style:italic;font-size:11px\">{{obj.modifiedUserId}}</span></td>\n                    </tr>\n                </tbody>\n            </table>\n        </div>\n    </div>\n    </div>\n    </div>  \n"
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, rp_references_1.RpReferences, router_1.Router, rp_http_service_1.RpHttpService, platform_browser_1.DomSanitizer])
    ], FrmDocumentList);
    return FrmDocumentList;
}());
exports.FrmDocumentList = FrmDocumentList;
//# sourceMappingURL=frmdocumentList.component.js.map