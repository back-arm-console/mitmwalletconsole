"use strict";
var router_1 = require('@angular/router');
var video_component_1 = require('./video.component');
var video_list_component_1 = require('./video-list.component');
var video_view_component_1 = require('./video-view.component');
var contentmenu_component_1 = require('./contentmenu.component');
var contentmenu_list_component_1 = require('./contentmenu-list.component');
var edu_view_component_1 = require('./edu-view.component');
var smssettingsetup_component_1 = require('./smssettingsetup.component');
var SMSSettingSetupList_component_1 = require('./SMSSettingSetupList.component');
var merchantcommmappingsetup_component_1 = require('./merchantcommmappingsetup.component');
var merchantcommmappingsetup_list_component_1 = require('./merchantcommmappingsetup-list.component');
var flexcommratesetup_component_1 = require('./flexcommratesetup.component');
var flexcommratesetup_list_component_1 = require('./flexcommratesetup-list.component');
var merchant_setup_component_1 = require('./merchant-setup.component');
var merchant_list_component_1 = require('./merchant-list.component');
var AdministratorRoutes = [
    { path: 'contentmenu', component: contentmenu_component_1.ContentMenu },
    { path: 'contentmenu/:cmd', component: contentmenu_component_1.ContentMenu },
    { path: 'contentmenu/:cmd/:id', component: contentmenu_component_1.ContentMenu },
    { path: 'contentmenulist', component: contentmenu_list_component_1.ContentMenuListComponent },
    { path: 'contentmenulist/:cmd', component: contentmenu_list_component_1.ContentMenuListComponent },
    { path: 'contentmenulist/:cmd/:id', component: contentmenu_list_component_1.ContentMenuListComponent },
    { path: 'eduView/:cmd/:id', component: edu_view_component_1.EduViewComponent },
    { path: 'videomenu', component: video_component_1.VideoComponent },
    { path: 'videomenu/:cmd', component: video_component_1.VideoComponent },
    { path: 'videomenu/:cmd/:id', component: video_component_1.VideoComponent },
    { path: 'videoList', component: video_list_component_1.VideoList },
    { path: 'videoAdminView', component: video_view_component_1.videoViewComponent },
    { path: 'videoAdminView/:cmd/:id', component: video_view_component_1.videoViewComponent },
    { path: 'SMS Setting', component: smssettingsetup_component_1.SMSSettingComponent },
    { path: 'SMS Setting/:cmd', component: smssettingsetup_component_1.SMSSettingComponent },
    { path: 'SMS Setting/:cmd/:id', component: smssettingsetup_component_1.SMSSettingComponent },
    { path: 'smssettinglist', component: SMSSettingSetupList_component_1.SMSSettingSetupListComponent },
    { path: 'Merchant Comm Mapping', component: merchantcommmappingsetup_component_1.MerchantCommMappingSetup },
    { path: 'Merchant Comm Mapping/:cmd', component: merchantcommmappingsetup_component_1.MerchantCommMappingSetup },
    { path: 'Merchant Comm Mapping/:cmd/:id', component: merchantcommmappingsetup_component_1.MerchantCommMappingSetup },
    { path: 'merchantcommmappinglist', component: merchantcommmappingsetup_list_component_1.MerchantCommMappingSetupList },
    { path: 'Flex Comm Setup', component: flexcommratesetup_component_1.FlexCommRateSetup },
    { path: 'Flex Comm Setup/:cmd', component: flexcommratesetup_component_1.FlexCommRateSetup },
    { path: 'Flex Comm Setup/:cmd/:id', component: flexcommratesetup_component_1.FlexCommRateSetup },
    { path: 'flexcommratelist', component: flexcommratesetup_list_component_1.FlexCommRateSetupList },
    { path: 'Merchant Profile', component: merchant_setup_component_1.MerchantSetup },
    { path: 'Merchant Profile/:cmd', component: merchant_setup_component_1.MerchantSetup },
    { path: 'Merchant Profile/:cmd/:id', component: merchant_setup_component_1.MerchantSetup },
    { path: 'Merchantlist', component: merchant_list_component_1.MerchantList },
];
exports.AdministratorRouting = router_1.RouterModule.forChild(AdministratorRoutes);
//# sourceMappingURL=administration.routing.js.map