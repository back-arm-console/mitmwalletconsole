"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
core_1.enableProdMode();
var DocumentList = (function () {
    function DocumentList(ics, _router, 
        //private sanitizer: DomSanitizer, 
        http, l_util, ref) {
        this.ics = ics;
        this._router = _router;
        this.http = http;
        this.l_util = l_util;
        this.ref = ref;
        this._mflag = false;
        this._OperationMode = "";
        this._shownull = false;
        this._divexport = false;
        //_printUrl: SafeResourceUrl;
        this._isLoading = true;
        this._SearchString = "";
        this._showListing = false;
        this._showPagination = false;
        this._toggleSearch = true; // true - Simple Search, false - Advanced Search
        this._togglePagination = true;
        this._ButtonInfo = { "role": "", "formname": "", "button": "" };
        this._FilterDataset = {
            filterList: [
                { "itemid": "", "caption": "", "fieldname": "", "datatype": "", "condition": "", "t1": "", "t2": "" }
            ],
            "pageNo": 1, "pageSize": 10, "filterSource": 0, "sessionID": this.ics._profile.sessionID, "userID": this.ics._profile.userID, "userName": this.ics._profile.userName
        };
        this._ListingDataset = {
            documentData: [
                {
                    "srno": "", "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": null, "modifiedDate": "", "title": "", "fileType": "",
                    "modifiedTime": null, "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0,
                    "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "Video", "t4": "", "t5": "",
                    "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0,
                    "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0,
                    "n12": 0, "n13": 0, "upload": [], "videoUpload": [], "cropdata": null, "agrodata": null,
                    "ferdata": null, "towndata": null, "crop": null, "fert": null, "agro": null, "addTown": null,
                    "postedby": "", "modifiedby": ""
                }
            ],
            "pageNo": 0, "pageSize": 0, "totalCount": 1, "userID": "", "userName": ""
        };
        this._sessionObj = this.getSessionObj();
        this.mstatus = 0;
        this._util = new rp_client_util_1.ClientUtil();
        this.mstatus = ics._profile.loginStatus;
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
    }
    DocumentList.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    DocumentList.prototype.goto = function (p) {
        this._router.navigate(['/document', 'read', p]);
    };
    DocumentList.prototype.ngOnInit = function () {
        this.filterSearch();
        this.loadAdvancedSearchData();
    };
    DocumentList.prototype.goNew = function () {
        this._router.navigate(['/document', 'new']);
    };
    DocumentList.prototype.ngAfterViewChecked = function () {
        this._isLoading = false;
    };
    DocumentList.prototype.loadAdvancedSearchData = function () {
        this._TypeList =
            {
                "lovState": [],
                "lovStatus": []
            };
        this.loadState(); // Branch
        //this.loadPromotionType();       // Promotion Type
        this.loadStatus(); // Status
        this._FilterList =
            [
                { "itemid": "1", "caption": "Search String", "fieldname": "SearchString", "datatype": "simplesearch", "condition": "c", "t1": "", "t2": "", "t3": "" },
                { "itemid": "2", "caption": "State", "fieldname": "State", "datatype": "lovState", "condition": "", "t1": "", "t2": "", "t3": "" },
                { "itemid": "3", "caption": "Status", "fieldname": "Status", "datatype": "lovStatus", "condition": "", "t1": "", "t2": "", "t3": "" },
            ];
    };
    DocumentList.prototype.loadState = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics.ticketurl + 'serviceCMS/getStateListByUserID?type=1&userID=' + this.ics._profile.userID).subscribe(function (response) {
                if (response.refstate != null && response.refstate != undefined) {
                    //this.ref._lov3.refstate = [{ "value": "", "caption": "" }];
                    _this.ref._lov3.refstate = [];
                    if (!(response.refstate instanceof Array)) {
                        var m = [];
                        m[0] = response.data;
                        _this.ref._lov3.refstate = m;
                    }
                    else {
                        _this.ref._lov3.refstate = response.refstate;
                    }
                    _this.ref._lov3.refstate.forEach(function (iItem) {
                        var l_Item = { "caption": "", "value": "" };
                        l_Item['caption'] = iItem.caption;
                        l_Item['value'] = iItem.value;
                        _this._TypeList.lovState.push(iItem);
                    });
                }
                else {
                    _this.ref._lov3.refstate = [{ "value": "", "caption": "Empty" }];
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Error!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    DocumentList.prototype.loadStatus = function () {
        var _this = this;
        try {
            var url = this.ics.ticketurl + 'serviceCMS/getStatusList?status=' + this.mstatus;
            this.http.doGet(url).subscribe(function (response) {
                if (response != undefined && response != null && response.data != undefined && response.data != null) {
                    if (!(response.data instanceof Array)) {
                        var m = [];
                        m[0] = response.data;
                        _this.ref._lov3.statuscombo = m;
                    }
                    else {
                        _this.ref._lov3.statuscombo = response.data;
                    }
                    _this.ref._lov3.statuscombo.forEach(function (iItem) {
                        var l_Item = { "caption": "", "value": "" };
                        l_Item['caption'] = iItem.caption;
                        l_Item['value'] = iItem.value;
                        _this._TypeList.lovStatus.push(iItem);
                    });
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    DocumentList.prototype.btnToggleSearch_onClick = function () {
        this.toggleSearch(!this._toggleSearch);
    };
    DocumentList.prototype.btnTogglePagination_onClick = function () {
        this.togglePagination(!this._togglePagination);
    };
    DocumentList.prototype.toggleSearch = function (aToggleSearch) {
        // Set Flag
        this._toggleSearch = aToggleSearch;
        // Clear Simple Search
        if (!this._toggleSearch)
            jQuery("#isInputSearch").val("");
        // Enable/Disabled Simple Search Textbox
        jQuery('#isInputSearch').prop('disabled', !this._toggleSearch);
        // Hide Advanced Search
        if (this._toggleSearch)
            this.ics.send001("CLEAR");
        // Show/Hide Advanced Search    
        //  
        if (this._toggleSearch)
            jQuery("#asAdvancedSearch").css("display", "none"); // true     - Simple Search
        else
            jQuery("#asAdvancedSearch").css("display", "inline-block"); // false    - Advanced Search
        // Set Icon 
        //  
        if (this._toggleSearch)
            jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-up").addClass("glyphicon glyphicon-menu-down"); // true     - Simple Search
        else
            jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-down").addClass("glyphicon glyphicon-menu-up"); // false    - Advanced Search
        // Set Tooltip
        //
        var l_Tooltip = (this._toggleSearch) ? "Expand" : "Collapse";
        jQuery('#btnToggleSearch').attr('title', l_Tooltip);
    };
    DocumentList.prototype.togglePagination = function (aTogglePagination) {
        // Set Flag
        this._togglePagination = aTogglePagination;
        // Show/Hide Pagination
        //
        if (this._showPagination && this._togglePagination)
            jQuery("#divPagination").css("display", "inline-block");
        else
            jQuery("#divPagination").css("display", "none");
        // Rotate Icon
        //
        if (!this._togglePagination)
            jQuery("#lblTogglePagination").css("transform", "rotate(270deg)");
        else
            jQuery("#lblTogglePagination").css("transform", "rotate(90deg)");
        // Set Icon Position
        if (!this._togglePagination)
            jQuery("#lblTogglePagination").css({ "margin-left": "-5px", "margin-top": "0px" });
        else
            jQuery("#lblTogglePagination").css({ "margin-left": "-5px", "margin-top": "-1px" });
        // Set Tooltip
        //
        var l_Tooltip = (this._togglePagination) ? "Collapse" : "Expand";
        jQuery('#btnTogglePagination').attr('title', l_Tooltip);
    };
    DocumentList.prototype.btnClose_MouseEnter = function () {
        jQuery("#btnClose").removeClass('btn btn-md btn-secondary').addClass('btn btn-md btn-danger');
    };
    DocumentList.prototype.btnClose_MouseLeave = function () {
        jQuery("#btnClose").removeClass('btn btn-md btn-danger').addClass('btn btn-md btn-secondary');
    };
    DocumentList.prototype.closePage = function () {
        this._router.navigate(['000', { cmd: "READ", p1: this.ics._profile.userID }]);
    };
    DocumentList.prototype.renderAdvancedSearch = function (event) {
        this.toggleSearch(!event);
    };
    DocumentList.prototype.filterAdvancedSearch = function (event) {
        this._FilterDataset.filterSource = 1;
        this._FilterDataset.filterList = event;
        if (this._OperationMode != "prepareFilter") {
            if (this._FilterDataset.pageNo == 1)
                this.filterRecords();
            else
                this._FilterDataset.pageNo = 1;
        }
    };
    DocumentList.prototype.changedPager = function (event) {
        if (!this._isLoading) {
            var l_objPagination = event.obj;
            this._FilterDataset.pageNo = l_objPagination.current;
            this._FilterDataset.pageSize = l_objPagination.size;
            this.filterRecords();
        }
    };
    DocumentList.prototype.filterSearch = function () {
        if (this._toggleSearch)
            this.filterCommonSearch();
        else
            this.ics.send001("FILTER");
    };
    DocumentList.prototype.filterCommonSearch = function () {
        var l_DateRange;
        var l_SearchString = "";
        this._FilterDataset.filterList = [];
        this._FilterDataset.filterSource = 0;
        if (jQuery.trim(this._SearchString) != "") {
            l_SearchString = jQuery.trim(this._SearchString);
            this._FilterDataset.filterList =
                [
                    { "itemid": "1", "caption": "Search String", "fieldname": "SearchString", "datatype": "simplesearch", "condition": "c", "t1": l_SearchString, "t2": "" }
                ];
        }
        l_DateRange = { "itemid": "4", "caption": "Date Range", "fieldname": "DateRange", "datatype": "date", "condition": "eq", "t1": this.l_util.getTodayDate(), "t2": "" };
        this._FilterDataset.filterList.push(l_DateRange);
        if (this._OperationMode != "prepareFilter") {
            if (this._FilterDataset.pageNo == 1)
                this.filterRecords();
            else
                this._FilterDataset.pageNo = 1;
        }
    };
    DocumentList.prototype.filterRecords = function () {
        var _this = this;
        var l_Data = this._FilterDataset;
        var l_ServiceURL = this.ics.cmsurl + 'serviceCMS/getDocumentlist';
        // Show loading animation
        this._mflag = false;
        this.http.doPost(l_ServiceURL, l_Data).subscribe(function (data) {
            if (data != null) {
                _this._ListingDataset = data;
                _this._shownull = false;
                // Convert to array for single item
                if (_this._ListingDataset.documentData != undefined) {
                    _this._ListingDataset.documentData = _this.l_util.convertToArray(_this._ListingDataset.documentData);
                    for (var iIndex = 0; iIndex < _this._ListingDataset.documentData.length; iIndex++) {
                        if (_this._ListingDataset.documentData[iIndex].title.length > 75) {
                            _this._ListingDataset.documentData[iIndex].title = _this._ListingDataset.documentData[iIndex].title.substring(0, 74) + "...";
                        }
                        _this._ListingDataset.documentData[iIndex].modifiedDate = _this._util.changeDatetoStringDMY(_this._ListingDataset.documentData[iIndex].modifiedDate);
                    }
                }
                else {
                    _this._shownull = true;
                }
                // Show / Hide Listing
                _this._showListing = (_this._ListingDataset.documentData != undefined);
                // Show / Hide Pagination
                _this._showPagination = (_this._showListing && (_this._ListingDataset.totalCount > 10));
                // Show / Hide Pagination
                _this.togglePagination(_this._showListing);
                // Hide loading animation
                _this._mflag = true;
            }
        }, function (error) {
            // Hide loading animation
            _this._mflag = true;
            // Show error message
            // this.ics.sendBean({ t1: "rp-error", t2: "Server connection error." });
        }, function () { });
    };
    DocumentList.prototype.btnExport_click = function () {
        // this._mflag = false; 
        // // Prepare Parameters
        // //
        // this._OperationMode = "prepareFilter";
        // this.filterSearch();
        // let l_Data: any = this._FilterDataset;
        // let l_Url = this.ics._apiurl + 'service001/loanApprovalListDownload';
        // this.http.doPost(l_Url, l_Data).subscribe
        // (
        //     res =>
        //     { 
        //         if(res!=null)
        //         {
        //             let l_FileName = res.msgCode;
        //             this.downloadFile(l_FileName);
        //         }
        //         this._OperationMode = "";
        //             this._mflag = false; 
        //     },
        //     error => 
        //     {   
        //         this._OperationMode = "";
        //         this._mflag = false;
        //         this.ics.sendBean({ t1 : "rp-error", t2: "Server connection error." });
        //     },
        //     () => { }
        // );
    };
    DocumentList.prototype.downloadFile = function (aFileName) {
        var l_Url = this.ics._apiurl + 'service001/downloadexcel?filename=' + aFileName;
        jQuery("#ExcelDownload").html("<iframe src=" + l_Url + "></iframe>");
    };
    DocumentList.prototype.btnPrint_click = function () {
        //this.goPrint();
    };
    DocumentList.prototype.goPrint = function () {
    };
    DocumentList.prototype.goClosePrint = function () {
        this._divexport = false;
    };
    DocumentList.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    DocumentList.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    DocumentList = __decorate([
        core_1.Component({
            selector: 'DocumentList',
            template: "\n    \n    <div *ngIf=\"!_divexport\" class=\"container-fluid\">\n    <div class=\"row clearfix\">\n        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n            <form class=\"form-horizontal\">\n                <fieldset>\n                    <legend>\n                        <div style=\"display:inline;\">\n                        Documents\n                        <button id=\"btnNew\" class=\"btn btn-sm btn-primary\" type=\"button\" style=\"cursor:pointer;height:34px;\" (click)=\"goNew()\" title=\"New\">\n                        <i id=\"lblNew\" class=\"glyphicon glyphicon-file\" style=\"padding-left:2px;\"></i> \n                        </button>                                \n                        </div>\n\n                        <div style=\"float:right;text-align:right;\">                                \n                        <div style=\"display:inline-block;padding-right:0px;width:280px;\">\n                            <div class=\"input-group\"> \n                                <input class=\"form-control\" type=\"text\" id=\"isInputSearch\" placeholder=\"Search\" autocomplete=\"off\" spellcheck=\"false\" [(ngModel)]=\"_SearchString\" [ngModelOptions]=\"{standalone: true}\" (keyup.enter)=\"filterSearch()\">\n\n                                <span id=\"btnSimpleSearch\" class=\"input-group-addon\">\n                                    <i class=\"glyphicon glyphicon-search\" style=\"cursor: pointer\" (click)=\"filterSearch()\"></i>\n                                </span>\n                            </div>\n                        </div>\n\n                            <button id=\"btnToggleSearch\" class=\"btn btn-md btn-secondary\" type=\"button\" style=\"cursor:pointer;text-align:center;margin-top:-24px;height:34px;\" (click)=\"btnToggleSearch_onClick()\" title=\"Collapse\">\n                                <i id=\"lblToggleSearch\" class=\"glyphicon glyphicon-menu-down\"></i> \n                            </button>\n\n                            <button *ngIf=\"false\" id=\"btnTogglePagination\" type=\"button\" class=\"btn btn-md btn-default\" style=\"cursor:pointer;text-align:center;margin-left:25px;margin-top:-24px;width:30px;height:34px;\" (click)=\"btnTogglePagination_onClick()\" title=\"Collapse\">\n                                <i id=\"lblTogglePagination\" class=\"glyphicon glyphicon-eject\" style=\"color:#2e8690;margin-left:-4px;transform: rotate(90deg)\"></i>\n                            </button>\n\n                            <div id=\"divPagination\" style=\"display:none;margin-left:13px;margin-top:-7px;margin-bottom:5px;\">\n                                <pager6 id=\"pgPagination\" rpPageSizeMax=\"100\" [(rpModel)]=\"_ListingDataset.totalCount\" (rpChanged)=\"changedPager($event)\" style=\"font-size:14px;text-align:right;margin-top:-7px;margin-right:10px;\"></pager6>\n                            </div>\n\n                            <button id=\"btnClose\" type=\"button\" class=\"btn btn-md btn-secondary\" (click)=\"closePage()\" (mouseenter)=\"btnClose_MouseEnter()\" (mouseleave)=\"btnClose_MouseLeave()\" title=\"Close\" style=\"cursor:pointer;height:34px;text-align:center;margin-top:-24px;\">\n                                <i id=\"lblClose\" class=\"glyphicon glyphicon-remove\"></i> \n                            </button>                     \n                        </div>\n                    </legend>\n\t\t\t\t\t<div class=\"cardview list-height\">\n\t\t\t\t\t\t<advanced-search id=\"asAdvancedSearch\" [FilterList]=\"_FilterList\" [TypeList]=\"_TypeList\" rpVisibleItems=3 (rpHidden)=\"renderAdvancedSearch($event)\" (rpChanged)=\"filterAdvancedSearch($event)\" style=\"display:none;\"></advanced-search>\n\n\t\t\t\t\t\t<div *ngIf=\"_shownull!=false\" style=\"color:#ccc;\">No result found!</div>\n\n\t\t\t\t\t\t<div *ngIf=\"_showListing\" class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-md-12\" style=\"overflow-x:auto;\">\n\t\t\t\t\t\t\t\t<table class=\"table table-striped table-condense table-hover tblborder\">\n\t\t\t\t\t\t\t\t\t<thead>\n\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t<th class=\"right\" style=\"width:6%\">No.</th>\n\t\t\t\t\t\t\t\t\t\t<th class=\"left\" style=\"width:15%\">Title</th>\n                                        <th class=\"left\" style=\"width:20%\">File Name</th>\n                                        <th class=\"left\" style=\"width:6%\">File Type</th>\n\t\t\t\t\t\t\t\t\t\t<th class=\"left\" style=\"width:10%\">Status</th>\n\t\t\t\t\t\t\t\t\t\t<th class=\"left\" style=\"width:8%\">Date</th> \n\t\t\t\t\t\t\t\t\t\t<th class=\"left\" style=\"width:8%\">Time</th> \t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t\t<th class=\"left\" style=\"width:10%\">Posted By</th>\n\t\t\t\t\t\t\t\t\t\t<th class=\"left\" style=\"width:10%\">Modified By</th>\n\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t</thead>\n\n\t\t\t\t\t\t\t\t\t<tbody>\n\t\t\t\t\t\t\t\t\t\t<tr *ngFor=\"let obj of _ListingDataset.documentData; let i=index\">\n\t\t\t\t\t\t\t\t\t\t<td class=\" right\"><a (click)=\"goto(obj.autokey)\">{{obj.srno}}</a></td>\n\t\t\t\t\t\t\t\t\t\t<td class=\"uni\">{{obj.title}}</td>                                    \n\t\t\t\t\t\t\t\t\t\t<td class=\"uni\"><a target=\"_blank\" rel=\"noopener noreferrer\" href=\"{{obj.filePath}}{{obj.fileName}}\">{{obj.fileName}}</a></td>\n                                        <td>{{obj.fileType}}</td>\n                                        <td>{{obj.status}}</td>\n\t\t\t\t\t\t\t\t\t\t<td>{{obj.modifiedDate}}</td> \n\t\t\t\t\t\t\t\t\t\t<td>{{obj.modifiedTime}}</td> \t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t\t<td class=\"uni\">{{obj.postedby}}</td>\n\t\t\t\t\t\t\t\t\t\t<td class=\"uni\">{{obj.modifiedby}}</td>\n\t\t\t\t\t\t\t\t\t\t</tr> \n\t\t\t\t\t\t\t\t\t</tbody>\n\t\t\t\t\t\t\t\t</table>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div *ngIf=\"_showListing\" style=\"text-align:center\">Total {{ _ListingDataset.totalCount }}</div>\n\t\t\t\t\t</div>\n\t\t\t\t</fieldset>\n            </form>\n\n            <div id=\"ExcelDownload\" style=\"display: none;width: 0px;height: 0px;\"></div>\n            \n        </div> \n    </div>\n</div>\n\n<div *ngIf='_divexport'>\n    <button type=\"button\" class=\"close\" (click)=\"goClosePrint()\" style=\"margin-top:-20px;\">&times;</button>\n    <iframe id=\"frame1\" [src]=\"_printUrl\" style=\"width: 100%;height: 95%;\"></iframe> \n</div>\n<div [hidden]=\"_mflag\">\n<div  id=\"loader\" class=\"modal\" ></div>\n</div>\n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_http_service_1.RpHttpService, rp_client_util_1.ClientUtil, rp_references_1.RpReferences])
    ], DocumentList);
    return DocumentList;
}());
exports.DocumentList = DocumentList;
//# sourceMappingURL=document-list.component.js.map