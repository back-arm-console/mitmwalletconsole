"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var FrmQnAComponent = (function () {
    function FrmQnAComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this.flagview = true;
        this.flagnew = true;
        this.flagsave = true;
        this.flagdelete = true;
        this._shown = true;
        this.dates = { "_date": null };
        this._file = null;
        this._time = "";
        this._str = "";
        this._fileName = '';
        this._viewsk = 0;
        this._crop = [{ "value": "", "caption": "", "flag": false }];
        this._util = new rp_client_util_1.ClientUtil();
        this._noti = { "flag": false };
        this._obj = { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": "", "modifiedDate": "", "modifiedTime": "", "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "question", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [], "videoUpload": [], "comData": [], "cropdata": [], "agrodata": [], "ferdata": [], "towndata": [], "crop": null, "fert": null, "agro": null, "addTown": null };
        this._roleval = "";
        this._checkcw = false;
        this._checkeditor = false;
        this._checkpubisher = false;
        this._checkadmin = false;
        this.cropobj = { id: "", name: "" };
        this.addCrop = [];
        this.crop = [{ "value": "", "caption": "", "flag": false }];
        this.count = 0;
        this.flagQuestion = true;
        this.flagLike = true;
        this._replyArray = [];
        this._image = [];
        this.key = 0;
        this._likearr = [];
        this._array = [];
        this._totalcount = 1;
        this._searchVal = "";
        this._answer = [{ "value": "", "caption": "" }];
        this._tmpObj = { '_key': 0, '_index': 0 };
        this.view = 0;
        this._result = { "longResult": "", "msgDesc": "", "key": [], "keyResult": 0, "state": "", "stringResult": "" };
        this.viewObj = [];
        this._ansObj = { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": "", "modifiedDate": "", "modifiedTime": "", "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [] };
        this._replyObj = { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": "", "modifiedDate": "", "modifiedTime": "", "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [] };
        this._cmtArray = [];
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        this._roleval = ics._profile.t1;
        this._datepickerOpts = this.ics._datepickerOpts;
        this._datepickerOpts.format = 'dd/mm/yyyy';
        this.flagdelete = true;
        this.flagview = true;
        this.setBtns();
        this.getCropList();
        this._time = this._util.getTodayTime();
        this._obj.createdTime = this._time;
        this._obj.modifiedTime = this._time;
        if (this.ics._profile.loginStatus == 1) {
            this.ref._lov3.postingstate = [];
            this.ref._lov3.postingstate = this.ref._lov3.postingstate0;
            this._checkcw = true;
        }
        if (this.ics._profile.loginStatus == 2) {
            this.ref._lov3.postingstate = [];
            this.ref._lov3.postingstate = this.ref._lov3.postingstate1;
            this._checkeditor = true;
        }
        if (this.ics._profile.loginStatus == 3) {
            this.ref._lov3.postingstate = [];
            this.ref._lov3.postingstate = this.ref._lov3.postingstate2;
            this._checkpubisher = true;
        }
        if (this.ics._profile.loginStatus == 4) {
            this.ref._lov3.postingstate = this.ref._lov3.postingstate;
            this._checkadmin = true;
        }
        if (this.ics._profile.loginStatus == 5) {
            this.ref._lov3.postingstate = this.ref._lov3.postingstate;
            this._checkadmin = true;
        }
    }
    FrmQnAComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "new") {
                _this.goNew();
            }
            else if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this.goGet(id);
                _this._viewsk = id;
            }
        });
    };
    FrmQnAComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    FrmQnAComponent.prototype.setImgUrl = function (str) {
        return 'upload/image/' + str;
    };
    FrmQnAComponent.prototype.goGet = function (p) {
        var _this = this;
        this.view = 1;
        this.flagview = false;
        this.http.doGet(this.ics._apiurl + 'serviceQuestion/readBySyskey?key=' + p).subscribe(function (data) {
            _this._obj = data;
            if (_this.ics._profile.loginStatus == 1 && _this._obj.n7.toString() == "6") {
                _this.ref._lov3.postingstate = [];
                _this.ref._lov3.postingstate = _this.ref._lov3.readpoststatuecon;
            }
            if (_this.ics._profile.loginStatus == 2 && _this._obj.n7.toString() == "2") {
                _this.ref._lov3.postingstate = [];
                _this.ref._lov3.postingstate = _this.ref._lov3.readpoststatueeditor;
            }
            if (_this.ics._profile.loginStatus == 3 && _this._obj.n7.toString() == "4") {
                _this.ref._lov3.postingstate = [];
                _this.ref._lov3.postingstate = _this.ref._lov3.readpostpublisher;
            }
            if (_this.ics._profile.loginStatus == 4) {
                _this.ref._lov3.postingstate = _this.ref._lov3.postingstate;
            }
            if (_this.ics._profile.loginStatus == 5) {
                _this.ref._lov3.postingstate = _this.ref._lov3.postingstate;
            }
            if (_this._obj.t5 != '') {
                _this._noti.flag = true;
            }
            _this.flagdelete = false;
            for (var i = 0; i < _this._obj.upload.length; i++) {
                var index = _this._obj.upload[i].lastIndexOf(".");
                var fe = _this._obj.upload[i].substring(index);
            }
            for (var i = 0; i < _this._crop.length; i++) {
                for (var j = 0; j < _this._obj.cropdata.length; j++) {
                    if (_this._obj.cropdata[j] == _this._crop[i].caption) {
                        _this._crop[i].flag = true;
                        _this.goAddCrop(_this._crop[i].value, _this._crop[i].caption);
                        break;
                    }
                    else {
                        _this._crop[i].flag = false;
                    }
                }
            }
            _this.flagdelete = false;
            _this.http.doGet(_this.ics._apiurl + 'serviceQuestion/viewByID?id=' + p).subscribe(function (response) {
                if (response.state) {
                    _this.viewObj = response.data;
                    _this.key = response.data[0].n7;
                }
            }, function (error) { }, function () { });
        }, function (error) { }, function () { });
    };
    FrmQnAComponent.prototype.goNew = function () {
        this.view = 0;
        jQuery("#imageUpload").val("");
        jQuery("#save").prop("disabled", false);
        this.flagview = true;
        this.flagdelete = true;
        this._noti.flag = false;
        this._obj = { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": "", "modifiedDate": "", "modifiedTime": "", "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "question", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [], "videoUpload": [], "comData": [], "cropdata": [], "agrodata": [], "ferdata": [], "towndata": [], "crop": null, "fert": null, "agro": null, "addTown": null };
        for (var i = 0; i < this._crop.length; i++) {
            this._crop[i].flag = false;
        }
        this.addCrop = [];
    };
    FrmQnAComponent.prototype.goPost = function () {
        var _this = this;
        if ((this._checkcw && this._obj.n7.toString() != "6") || (this._checkeditor && this._obj.n7.toString() != "2") || (this._checkpubisher && this._obj.n7.toString() != "4") || (this._checkadmin)) {
            this._obj.createdTime = this._time;
            this._obj.modifiedTime = this._time;
            this._obj.crop = this.addCrop;
            if (this.isValidate(this._obj)) {
                var url = this.ics._apiurl + 'serviceQuestion/saveQuestion';
                this._obj.n5 = Number(this.ics._profile.t1);
                var json = this._obj;
                if (isMyanmar(json.t2)) {
                    if (identifyFont(json.t2) == "zawgyi") {
                        json.t2 = ZgtoUni(json.t2);
                    }
                }
                this.http.doPost(url, json).subscribe(function (data) {
                    _this._result = data;
                    _this.showMessage(data.msgDesc, data.state);
                    _this._obj.syskey = data.longResult[0];
                }, function (error) {
                    _this.showMessage("Can't Saved This Record!!!", undefined);
                }, function () { });
            }
        }
        else {
            this.showMessage("Can't Save This Status!!!", undefined);
        }
    };
    FrmQnAComponent.prototype.goDelete = function () {
        var _this = this;
        var url = this.ics._apiurl + 'serviceQuestion/deleteQuestion';
        var json = this._obj;
        this.http.doPost(url, json).subscribe(function (data) {
            _this.showMessage(data.msgDesc, data.state);
            if (data.state) {
                _this.goNew();
            }
        }, function (error) { }, function () { });
    };
    FrmQnAComponent.prototype.goList = function () {
        this._router.navigate(['/questionList']);
    };
    FrmQnAComponent.prototype.uploadedFile = function (event) {
        var _this = this;
        if (event.target.files.length == 1) {
            this._fileName = event.target.files[0].name;
            this._file = event.target.files[0];
            var index = this._fileName.lastIndexOf(".");
            var imagename = this._fileName.substring(index);
            imagename = imagename.toLowerCase();
            if (imagename == ".jpg" || imagename == ".jpeg" || imagename == ".png") {
                var url = this.ics._apiurl + 'file/fileupload?f=upload&fn=' + this._fileName + '&id=' + this.ics._profile.t1;
                this.http.upload(url, this._file).subscribe(function (data) {
                    if (data.code === 'SUCCESS') {
                        _this.popupMessage("Upload Successful!!!");
                        var _img = _this._fileName;
                        _this._obj.upload.push(data.fileName);
                        _this._obj.t13 = data.sfileName;
                        _this._fileName = "";
                    }
                    else {
                        _this.popupMessage("Upload Unsuccessful!!! Please Try Again...");
                    }
                }, function (error) { }, function () { });
            }
            else {
                this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": "Choose Image Associated!" });
            }
        }
        else {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "info", "t3": "Upload File!" });
        }
    };
    FrmQnAComponent.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    FrmQnAComponent.prototype.popupMessage = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", t2: "Question And Answer Information", t3: msg });
    };
    FrmQnAComponent.prototype.isValidate = function (obj) {
        if (obj.t2 == "") {
            this.showMessage("Please fill the Question!!!", false);
            return false;
        }
        else if (this.addCrop == null || this.addCrop.length == 0) {
            this.showMessage("Choose the crop list !!!", false);
            return false;
        }
        else if (obj.n7 == "") {
            this.showMessage("Choose Status!!!", false);
            return false;
        }
        else
            return true;
    };
    FrmQnAComponent.prototype.setBtns = function () {
        var k = this.ics.getBtns("/questionList");
        if (k != "" && k != undefined) {
            var strs = k.split(",");
            for (var i = 0; i < strs.length; i++) {
                if (strs[i] == "1") {
                    this.flagnew = false;
                }
                if (strs[i] == "2") {
                    this.flagsave = false;
                }
            }
        }
    };
    FrmQnAComponent.prototype.getCropList = function () {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'serviceCrop/getSenderList').subscribe(function (response) {
            if (response != null && response != undefined) {
                _this._crop = response.data;
            }
            else {
            }
        }, function (error) { }, function () { });
    };
    FrmQnAComponent.prototype.checkboxCrop = function (event, p1, p2) {
        if (event.target.checked) {
            if (p2 == "All") {
                this.goAddChekAllCrop();
            }
            else {
                this.goAddCrop(p1, p2);
            }
        }
        else if (!event.target.checked) {
            if (p2 == "All") {
                this.goRemoveChekallCrop(p1);
            }
            else {
                this.goRemoveCrop(p1);
            }
        }
    };
    FrmQnAComponent.prototype.goAddCrop = function (p1, p2) {
        this.cropobj = { id: "", name: "" };
        this.cropobj.id = p1;
        this.cropobj.name = p2;
        this.addCrop.push(this.cropobj);
    };
    FrmQnAComponent.prototype.goAddChekAllCrop = function () {
        for (var i = 0; i < this._crop.length; i++) {
            this._crop[i].flag = true;
            this.cropobj = { id: "", name: "" };
            this.cropobj.id = this._crop[i].value;
            this.cropobj.name = this._crop[i].caption;
            this.addCrop.push(this.cropobj);
        }
    };
    FrmQnAComponent.prototype.updateChecked = function (event) {
        if (event.target.checked) {
            this._obj.t5 = "noti";
        }
        else {
            this.showMessage("Please check that you have read and agree to the Terms and Conditions !", undefined);
        }
    };
    FrmQnAComponent.prototype.goRemoveCrop = function (p) {
        var temp = [], j = 0;
        for (var i = 0; i < this.addCrop.length; i++) {
            if (p == this.addCrop[i].id) {
                this.addCrop[i].id = "";
                this.addCrop[i].name = "";
            }
        }
        for (var i = 0; i < this.addCrop.length; i++) {
            if (this.addCrop[i].id != "") {
                this.cropobj = { id: "", name: "" };
                this.cropobj.id = this.addCrop[i].id;
                this.cropobj.name = this.addCrop[i].name;
                temp[j] = this.cropobj;
                j++;
            }
        }
        this.addCrop = temp;
    };
    FrmQnAComponent.prototype.goRemoveChekallCrop = function (p) {
        for (var i = 0; i < this._crop.length; i++) {
            this._crop[i].flag = false;
        }
        this.addCrop = [];
    };
    FrmQnAComponent.prototype.goView = function (p) {
        this._router.navigate(['/questionView', 'read', p]);
    };
    //tta
    FrmQnAComponent.prototype.goLike = function (key) {
        var _this = this;
        jQuery("#likeModal").modal();
        this.http.doGet(this.ics._apiurl + 'serviceQuestion/searchLike?key=' + key).subscribe(function (response) {
            if (response.state)
                _this._likearr = response.data;
        }, function (error) { }, function () { });
    };
    FrmQnAComponent.prototype.goClickLike = function (key, index) {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'serviceQuestion/clickLikeQuestion?key=' + key + '&userSK=' + this.key).subscribe(function (data) {
            if (data.state) {
                _this.goLikeCount(key, index);
            }
        }, function (error) { }, function () { });
    };
    FrmQnAComponent.prototype.goLikeCount = function (key, index) {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'serviceQuestion/searchLikeCount?key=' + key).subscribe(function (response) {
            if (response.state)
                _this.viewObj = response.data;
        }, function (error) { }, function () { });
    };
    FrmQnAComponent.prototype.goClickUnlike = function (key, index) {
        this.http.doGet(this.ics._apiurl + 'serviceQuestion/clickUnlikeQuestion?key=' + key).subscribe(function (data) {
            if (data.state)
                console.log("Unlike...");
        }, function (error) { }, function () { });
    };
    FrmQnAComponent.prototype.goAnswer = function (key, index) {
        var _this = this;
        jQuery("#commentModal").modal();
        this._tmpObj._key = key;
        this._tmpObj._index = index;
        this._cmtArray = [];
        this.http.doGet(this.ics._apiurl + 'serviceQuestion/getAnswers?id=' + key).subscribe(function (response) {
            if (response.state)
                _this._cmtArray = response.data;
        }, function (error) { }, function () { });
    };
    FrmQnAComponent.prototype.saveAnswer = function (cmtObj) {
        var _this = this;
        cmtObj.n1 = this._tmpObj._key;
        cmtObj.n5 = Number(this.ics._profile.t1);
        cmtObj.t12 = '';
        cmtObj.t13 = '';
        if (cmtObj.t2 != '') {
            var url = this.ics._apiurl + 'serviceQuestion/saveAnswer';
            var json = cmtObj;
            this.http.doPost(url, json).subscribe(function (response) {
                _this._cmtArray = [];
                if (response.state)
                    _this._cmtArray = response.data;
                _this._ansObj.t2 = "";
            }, function (error) {
                _this.showMessage("Can't Saved This Record!!!", undefined);
            }, function () { });
        }
    };
    FrmQnAComponent.prototype.replyAnswer = function (key, num) {
        var _this = this;
        this._tmpObj._index = num;
        this._cmtArray[num].t13 = 'true';
        this._replyArray = [];
        this.http.doGet(this.ics._apiurl + 'serviceQuestion/getAnswers?id=' + key).subscribe(function (response) {
            if (response.state)
                _this._replyArray = response.data;
        }, function (error) { }, function () { });
    };
    FrmQnAComponent.prototype.saveReply = function (skey) {
        var _this = this;
        this._replyObj.n1 = skey;
        this._replyObj.n3 = -1;
        this._replyObj.n5 = Number(this.ics._profile.t1);
        if (this._replyObj.t2 != '') {
            var url = this.ics._apiurl + 'serviceQuestion/saveAnswer';
            var json = this._replyObj;
            this.http.doPost(url, json).subscribe(function (response) {
                _this._replyArray = [];
                if (response.state)
                    _this._replyArray = response.data;
                _this._cmtArray[_this._tmpObj._index].n3 = _this._cmtArray[_this._tmpObj._index].n3 + 1;
                _this._replyObj.t2 = "";
            }, function (error) {
                _this.showMessage("Can't Saved This Record!!!", undefined);
            }, function () { });
        }
    };
    FrmQnAComponent.prototype.editAnswer = function (obj, num) {
        this._cmtArray[num].t12 = 'true';
    };
    FrmQnAComponent.prototype.deleteAnswer = function (obj, num) {
        var _this = this;
        var url = this.ics._apiurl + 'serviceQuestion/deleteAnswer';
        var json = obj;
        this.http.doPost(url, json).subscribe(function (response) {
            _this._cmtArray = [];
            if (response.state)
                _this._cmtArray = response.data;
        }, function (error) { }, function () { });
    };
    FrmQnAComponent.prototype.convertToAns = function (obj, num) {
        var _this = this;
        obj.n5 = Number(this.ics._profile.t1);
        var url = this.ics._apiurl + 'serviceQuestion/convertToAns';
        var json = obj;
        this.http.doPost(url, json).subscribe(function (response) {
            _this._cmtArray = [];
            if (response.state) {
                _this._cmtArray = response.data;
            }
        }, function (error) { }, function () { });
    };
    FrmQnAComponent = __decorate([
        core_1.Component({
            selector: 'fmr-qna',
            template: "\n    <div class=\"container\">\n    <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n    <form class= \"form-horizontal\" ngNoForm> \n    <legend>Question</legend>\n        <div class=\"row  col-md-12\"> \n            <div  style=\"float: left;\"> \n            <button class=\"btn btn-danger\" [disabled]=\"flagnew\" id=\"new\" type=\"button\" (click)=\"goNew()\">New</button>      \n            <button class=\"btn btn-danger\" [disabled]=\"flagsave\" id=\"save\" type=\"button\" (click)=\"goPost()\">Save</button> \n            <button class=\"btn btn-danger\" [disabled]=\"flagdelete\" id=\"delete\" type=\"button\" (click)=\"goDelete();\" >Delete</button> \n            <button class=\"btn btn-danger\"  type=\"button\"   (click)=\"goList()\">List</button> \n            </div>\n        </div>\n\n        <div class=\"row col-md-12\">&nbsp;</div>\n        <div class=\"form-group\">\n        <label class=\"col-md-2\">Question</label>\n            <div class=\"col-md-4\">\n                <textarea type=\"text\" [(ngModel)]=\"_obj.t2\" class=\"form-control input-md zawgyi\" rows=\"7\" ></textarea>\n            </div>\n            \n        <label class=\"col-md-2\">Crop  <span style=\"color:red\">*</span> </label>\n        <div class=\"col-md-4\" >\n            <div style=\"height:100px;width:360px;border:1px solid #ccc;overflow:auto;\">\n                <div *ngFor=\"let obj of _crop\">\n                    <input type=\"checkbox\" [(ngModel)]=\"obj.flag\"  (change)=\"checkboxCrop($event,obj.value,obj.caption)\" name=\"rdodiv\" value=\"{{obj.value}}\" >{{obj.caption}} \n                </div>\n            </div>\n        </div>\n        </div>\n\n        <div class=\"form-group\">\n        <label class=\"col-md-2\">Image Associated (.jpeg, .png, .jpg)</label>\n        <div class=\"col-md-4\">\n             <input type=\"file\" id=\"imageUpload\" class=\"form-control input-md\" (change)=\"uploadedFile($event)\" placeholder=\"Upload file...\" />\n        </div>\n            <rp-input  [(rpModel)]=\"_obj.n7\" rpRequired =\"true\"   rpType=\"postingstate\" rpLabel=\"Status\"  autofocus ></rp-input>\n\n        <label class=\"col-md-6\"></label>\n        <div class=\"col-md-4\"> \n            <input type=\"checkbox\"  [(ngModel)]=\"_noti.flag\" (change)=\"updateChecked($event)\" name=\"check\"  ><label for=\"check\" >   Notification</label>\n               \n        </div>  \n        </div>\n\n    <div *ngIf=\"view==0\">\n        <div class=\"form-group\">\n            <div *ngFor=\"let img of _obj.upload\">\n                <img src={{setImgUrl(img)}} alt={{img}} height=\"240\" width=\"400\" />\n            </div>\n        </div>\n    </div>\n    </form>\n\n    <div *ngIf=\"view==1\">\n        <div  *ngFor=\"let obj of viewObj;let num = index\" class=\"col-md-12\">\n            <div class=\"form-group\">\n               <div *ngFor=\"let img of obj.upload\">\n                   <img src={{setImgUrl(img)}} alt={{img}} height=\"240\" width=\"400\" />\n               </div>\n            </div>\n\n            <table>\n                <tr>\n                   <td><button class=\"btn btn-link\" type=\"button\" (click)=\"goClickLike(obj.syskey,num)\" ><span class=\"glyphicon glyphicon-thumbs-up\"></span></button><span *ngIf=\"obj.n2!=0\"><a (click)=\"goLike(obj.syskey)\">{{obj.n2}}</a></span></td>\n                   <td><button class=\"btn btn-link\" type=\"button\" (click)=\"goAnswer(obj.syskey,num)\"><span class=\"glyphicon glyphicon-comment\"></span></button><span *ngIf=\"obj.n3!=0\">{{obj.n3}}</span></td> \n                </tr>\n            </table>\n            <hr>\n        </div> \n        </div>\n    </div>\n    </div>\n    </div>\n\n    <!-- Modal -->\n    <div id=\"commentModal\" class=\"modal fade\" role=\"dialog\">\n    <div class=\"modal-dialog modal-lg\">  \n        <div class=\"modal-content\">\n        <div class=\"modal-header\">\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n            <h4 class=\"modal-title\">Comments</h4>\n        </div>\n        <div id=\"jbody\" class=\"modal-body\" >\n            <div style=\"overflow: hidden; position: relative;\">                \n                <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n                    <div *ngFor=\"let obj of _cmtArray;let num2 = index\" class=\"col-md-12\">\n                        <div *ngIf=\"obj.t12!='true'\">\n                            <label class=\"col-md-2\"><img style='height: 50px; width: 50px;'  src='image/farmerpng.png'  ></label>\n                                <div class=\"col-md-7\" >\n                                     <h4>{{obj.userName}}</h4>                                         \n                                     <p>{{obj.t2}}</p>\n                                </div> \n                                <br>\n                        </div>\n                        <div *ngIf=\"obj.t12=='true'\"> \n                            <label class=\"col-md-2\"><img style='height: 50px; width: 50px;'  src='image/farmerpng.png'  ></label>\n                            <div class=\"col-md-7\">\n                                <textarea type=\"text\" [(ngModel)]=\"obj.t2\" class=\"form-control input-md\" rows=\"3\" ></textarea>\n                            </div>\n                            <br>\n                        </div>\n                        <button *ngIf=\"obj.t12=='true'\" class=\"btn btn-link\" type=\"button\" (click)=\"saveAnswer(obj)\">Save</button> \n                        <button *ngIf=\"obj.t12!='true' && obj.n5 == key\" class=\"btn btn-link\" type=\"button\" (click)=\"editAnswer(obj,num2)\">Edit</button>\n                        <button class=\"btn btn-link\" type=\"button\" (click)=\"replyAnswer(obj.syskey,num2)\">Reply</button>\n                        <button class=\"btn btn-link\" type=\"button\" (click)=\"deleteAnswer(obj,num2)\">Delete</button>\n                        <button *ngIf=\"obj.n2==0\" class=\"btn btn-link\" type=\"button\" (click)=\"convertToAns(obj,num2)\"><span class=\"glyphicon glyphicon-circle-arrow-right\"></span>Convert To Answer</button>\n                \n                        <div *ngIf=\"obj.t13=='true'\">\n                            <div *ngFor=\"let replyObj of _replyArray\">\n                                <div class=\"form-group col-md-12\">\n                                     <label class=\"col-md-2\"></label>\n                                    <label class=\"col-md-2\"><img style='height: 50px; width: 50px;'  src='image/farmerpng.png'  ></label>\n                                    <div class=\"col-md-8\" >\n                                        <h4>{{replyObj.userName}}</h4>                         \n                                        <p>{{replyObj.t2}}</p>\n                                    </div>\n                                      \n                                </div> \n                            </div>\n                         <div class=\"form-group col-md-12\">\n                              <label class=\"col-md-2\"></label>\n                            <div class=\"col-md-8\">                            \n                                <textarea type=\"text\" [(ngModel)]=\"_replyObj.t2\" class=\"form-control input-md\" rows=\"3\" ></textarea>\n                            </div>\n                            <div class=\"col-md-2\"> \n                                <button class=\"btn btn-danger\" type=\"button\" (click)=\"saveReply(obj.syskey)\">Reply</button>  \n                            </div>\n                        </div>\n                        </div>\n                        <hr>\n                    </div>\n                    <div class=\"form-group col-md-12\">\n                        <label class=\"col-md-2\"><img style='height: 50px; width: 50px;'  src='image/farmerpng.png'  ></label>\n                        <div class=\"col-md-8\">                             \n                            <textarea type=\"text\" [(ngModel)]=\"_ansObj.t2\" class=\"form-control input-md\" rows=\"3\" ></textarea>\n                        </div>\n                        <div class=\"col-md-2\"> \n                            <button class=\"btn btn-danger\" type=\"button\" (click)=\"saveAnswer(_ansObj)\">Save</button>  \n                        </div>\n                    </div> \n                </div>\n            </div>\n        </div>\n        </div>\n    </div>\n    </div>\n\n     <!-- Modal -->\n    <div id=\"likeModal\" class=\"modal fade\" role=\"dialog\">\n    <div class=\"modal-dialog modal-md\">  \n        <div class=\"modal-content\">\n        <div class=\"modal-header\">\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>           \n            <div class=\"form-group col-md-12\">\n                <label class=\"col-md-4\"></label>\n                    <div class=\"col-md-6\" >\n                        <h4 class=\"modal-title\">People who like this!</h4>\n                    </div> \n                    <div class=\"col-md-2\"></div>\n            </div>\n        </div>\n        <div id=\"jbody\" class=\"modal-body\" >\n            <div style=\"overflow: hidden; position: relative;\">                \n                <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n                    \n                    <div class=\"form-group col-md-12\">\n                        <div class=\"col-md-12\" *ngFor=\"let obj of _likearr\">\n                             <label class=\"col-md-12\">{{obj.t2}}</label>\n                             \n                        </div> \n                        <div class=\"col-md-0\"></div>\n                    </div>                 \n                   \n                </div>\n            </div>\n        </div>\n        </div>\n    </div>\n    </div>\n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmQnAComponent);
    return FrmQnAComponent;
}());
exports.FrmQnAComponent = FrmQnAComponent;
//# sourceMappingURL=frmQnA.component.js.map