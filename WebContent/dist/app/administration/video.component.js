"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
core_2.enableProdMode();
var VideoComponent = (function () {
    function VideoComponent(el, renderer, ics, _router, route, http, ref) {
        this.el = el;
        this.renderer = renderer;
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this.td = true;
        this._viewsk = "";
        this.alert = false;
        this.stateval = "";
        this.statusval = 0;
        this.flagview = true;
        this.flagvideo = false;
        this.flagURL = true;
        this.flagImage = false;
        //myimg=[];
        this.flagnew = true;
        this.flagsave = true;
        this.flagdelete = true;
        this.dates = { "_date": null };
        this.uploadDataList = [];
        this.uploadDataListResize = [];
        this.uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
        this._sessionObj = this.getSessionObj();
        this.lovdomainType = [];
        this._img = "";
        this._file = null;
        this.myimg = null;
        this._fileName = '';
        this._Time = 0;
        this._util = new rp_client_util_1.ClientUtil();
        this._time = "";
        this.flagcanvas = true;
        this._imageObj = [{ "value": "", "caption": "", "flag": false }];
        this.alertTime = "";
        //_crop = [{ "value": "", "caption": "", "flag": false }];
        //_obj = { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": null, "modifiedDate": "", "modifiedTime": null, "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "Video", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [], "videoUpload": [], "cropdata": null, "agrodata": null, "ferdata": null, "towndata": null, "crop": null, "fert": null, "agro": null, "addTown": null, "resizelist": [], "modifiedUserId": "", "modifiedUserName": "", "uploadlist": [] };
        this._obj = this.getDefaultObj();
        this._roleval = "";
        this._checkcw = false;
        this._checkeditor = false;
        this._checkpubisher = false;
        this._checkmaster = false;
        this._checkadmin = false;
        this.flagImg = false;
        this.lovstatusType = [];
        this._size = 0;
        this.temp = "";
        this.tests = "";
        this.noti = { "flag": false };
        this.alertDate = { "_date": null };
        this.stateobj = { id: "", name: "" };
        this.town_obj = { id: "", name: "" };
        this._result = { "longResult": "", "msgDesc": "", "key": [], "keyResult": 0, "state": "", "stringResult": "" };
        this.test = "";
        this.obj = { id: "", name: "" };
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        this._roleval = ics._profile.t1;
        this._datepickerOpts = this.ics._datepickerOpts;
        this._datepickerOpts.format = 'dd/mm/yyyy';
        this.alertDate._date = new Date();
        this.flagdelete = true;
        this.flagnew = false;
        this.flagsave = false;
        this.flagview = true;
        this.flagURL = true;
        this.alert = true;
        this.flagImage = false;
        this.flagvideo = false;
        this._obj.n7 = 5;
        this.getStatusType();
        this._time = this._util.getTodayTime();
        this._obj.createdTime = this._time;
        this._obj.modifiedTime = this._time;
        this.videoUrl = this.ics._imgurl;
    }
    VideoComponent.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "", "lovDesc": "" };
    };
    VideoComponent.prototype.getDefaultObj = function () {
        return { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": "", "modifiedDate": "", "modifiedTime": "", "alertDate": "", "alertTime": "", "sessionId": "", "userId": "", "userName": "", "modifiedUserId": "", "modifiedUserName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "Video", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "temp": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [], "videoUpload": [], "comData": [], "cropdata": [], "agrodata": [], "ferdata": [], "towndata": [], "uploadlist": [], "resizelist": [] };
    };
    VideoComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "new") {
                _this.goNew();
            }
            else if (cmd != null && cmd != "" && cmd == "read") {
                var idsys = params['id'];
                _this.goGet(idsys);
                _this._viewsk = idsys;
            }
        });
    };
    VideoComponent.prototype.updateCheckedNoti = function (event) {
        if (event.target.checked) {
            this._obj.n9 = 1;
        }
        else {
            this._obj.n9 = 0;
        }
    };
    VideoComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    VideoComponent.prototype.goGet = function (p) {
        var _this = this;
        this.td = false;
        this._mflag = false;
        this.flagview = false;
        this.flagdelete = false;
        this.flagnew = false;
        this.flagsave = false;
        this._obj.createdTime = this._time;
        this._obj.modifiedTime = this._time;
        this.http.doGet(this.ics.cmsurl + 'serviceVideoAdm/readBySyskeyNew?key=' + p + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (data) {
            _this._obj = data;
            if (_this._obj != null) {
                _this.alertTime = _this._obj.alertTime;
                console.log("data obj=" + JSON.stringify(data));
                if (_this._obj.n10 == 0) {
                    _this.flagvideo = false;
                    _this.flagURL = true;
                    _this.flagImage = false;
                }
                if (_this._obj.n10 == 1 || _this._obj.n10 == 2) {
                    _this.flagURL = false;
                    _this.flagvideo = true;
                    _this.flagImage = false;
                }
                for (var i = 0; i < _this._obj.upload.length; i++) {
                    var index = _this._obj.upload[i].lastIndexOf(".");
                    var fe = _this._obj.upload[i].substring(index);
                }
                if (_this._obj.resizelist.length > 0) {
                    _this.uploadDataListResize = _this._obj.resizelist;
                    _this._obj.resizelist = [];
                }
                if (_this._obj.n9 == 1) {
                    _this.noti.flag = true;
                }
                if (_this._obj.uploadlist[0].name != "") {
                    _this.flagImg = true;
                    jQuery("#uploadVideo1").val(_this._obj.videoUpload[0]);
                    jQuery("#imageUpload1").val(_this._obj.uploadlist[0].name);
                }
                else {
                    _this.flagImg = false;
                }
            }
            else
                _this._obj = _this.getDefaultObj();
            _this._mflag = true;
        }, function (error) { }, function () { });
    };
    VideoComponent.prototype.goNew = function () {
        this.td = true;
        this._datepickerOpts = this.ics._datepickerOpts;
        this._datepickerOpts.format = 'dd/mm/yyyy';
        this.alertDate._date = new Date();
        this._time = this._util.getTodayTime();
        this.flagvideo = false;
        this.flagURL = true;
        //this.flagImage = true;
        this.flagImage = false;
        jQuery("#uploadVideo1").val("");
        jQuery("#imageUpload1").val("");
        jQuery("#save").prop("disabled", false);
        this.flagview = true;
        this.flagdelete = true;
        this.flagnew = false;
        this.flagsave = false;
        this.flagImg = false;
        this.uploadDataList = [];
        this._obj = this.getDefaultObj();
        this.test = "";
        this.tests = "";
        this._fileName = "";
        this._obj.t5 = this.stateval;
        this._obj.n7 = 5;
        this.alertTime = "";
    };
    VideoComponent.prototype.goPost = function () {
        var _this = this;
        this.flagsave = true;
        console.log("profile=" + JSON.stringify(this.ics._profile));
        console.log("obj=" + JSON.stringify(this._obj));
        this._obj.createdTime = this._time;
        this._obj.modifiedTime = this._time;
        this._obj.resizelist = this.uploadDataListResize;
        this._obj.alertTime = this.alertTime;
        if (this.isValidate(this._obj)) {
            if (this._obj.syskey != 0) {
                this._obj.modifiedUserId = this.ics._profile.userID;
                this._obj.modifiedUserName = this.ics._profile.userName;
            }
            this._obj.userId = this.ics._profile.userID;
            this._obj.userName = this.ics._profile.userName;
            this._obj.sessionId = this.ics._profile.sessionID;
            var url = this.ics.cmsurl + 'serviceContentNew/goSave';
            this._obj.n5 = Number(this.ics._profile.t1);
            this._obj.t3 = "Video";
            var json = this._obj;
            console.log("json=" + JSON.stringify(json));
            if (isMyanmar(json.t2)) {
                if (identifyFont(json.t2) == "zawgyi") {
                    json.t2 = ZgtoUni(json.t2);
                }
            }
            this.http.doPost(url, json).subscribe(function (data) {
                _this._result = data;
                if (data.state) {
                    _this.showMessage(data.msgDesc, true);
                    _this.flagsave = false;
                    _this.flagdelete = false;
                    _this._obj.syskey = data.longResult[0];
                }
                else {
                    _this.showMessage(data.msgDesc, false);
                    _this.flagsave = false;
                }
            }, function (error) {
                _this._mflag = true;
                console.log("data error=");
                _this.flagsave = false;
                _this.showMessage("Can't Saved This Record", undefined);
            }, function () {
                _this.flagsave = false;
                console.log("data error");
            });
        }
    };
    VideoComponent.prototype.goDelete = function () {
        var _this = this;
        this._mflag = false;
        if (this._obj.t2 != "") {
            this._obj.sessionId = this.ics._profile.sessionID;
            this._obj.userId = this.ics._profile.userID;
            var url = this.ics.cmsurl + 'serviceContentNew/goDelete';
            var json = this._obj;
            this.http.doPost(url, json).subscribe(function (data) {
                _this._mflag = true;
                _this.showMessage(data.msgDesc, data.state);
                if (data.state) {
                    _this.goNew();
                }
            }, function (error) { }, function () { });
        }
        else {
            this._mflag = true;
            this.showMessage("No Article to Delete", undefined);
        }
    };
    VideoComponent.prototype.goList = function () {
        this._router.navigate(['/videoList']);
    };
    VideoComponent.prototype.goOK = function () {
        var _this = this;
        jQuery("#sessionalert1").modal('hide');
        this.flagcanvas = true;
        this.flagvideo = false;
        this._fileName = this.myimg.name;
        this._file = this.myimg;
        var index = this._fileName.lastIndexOf(".");
        var videoname = this._fileName.substring(index);
        videoname = videoname.toLowerCase();
        if (videoname == ".mp4" || videoname == ".flv" || videoname == ".webm" || videoname == ".3gp" || videoname == ".3gp2" || videoname == ".mpeg4" || videoname == ".mpeg" || videoname == ".wmv" || videoname == ".avi") {
            jQuery("#imagepopup").modal();
            var url = this.ics.cmsurl + 'fileAdm/fileupload?f=upload&fn=' + this._fileName + '&id=' + this.ics._profile.t1 + '&type=10&imgUrl=' + this.ics._imgurl;
            this.http.upload(url, this._file).subscribe(function (data) {
                jQuery("#imagepopup").modal('hide');
                if (data.code === 'SUCCESS') {
                    //this.flagImg = true;
                    _this.showMessage("Upload Successful", true);
                    _this._obj.videoUpload[0] = data.fileName;
                    //this.gettime();
                    _this.tests = _this._fileName;
                }
                else {
                    _this.showMessage("Upload Unsuccessful Please Try Again...", false);
                }
            }, function (error) { }, function () { });
        }
        else {
            jQuery("#uploadVideo1").val("");
            this._obj.videoUpload = []; //atn
            this.showMessage("Choose video Associated", false);
        }
        this.tests = "";
    };
    VideoComponent.prototype.uploadedFile = function (event) {
        var _this = this;
        this.flagcanvas = true;
        this.flagvideo = false;
        if (event.target.files.length == 1) {
            if ((this._obj.videoUpload[0] != null)) {
                this.myimg = event.target.files[0];
                jQuery("#sessionalert1").modal();
            }
            else {
                this._fileName = event.target.files[0].name;
                this._file = event.target.files[0];
                var index = this._fileName.lastIndexOf(".");
                var videoname = this._fileName.substring(index);
                videoname = videoname.toLowerCase();
                if (videoname == ".mp4" || videoname == ".flv" || videoname == ".webm" || videoname == ".3gp" || videoname == ".3gp2" || videoname == ".mpeg4" || videoname == ".mpeg" || videoname == ".wmv" || videoname == ".avi") {
                    jQuery("#imagepopup").modal();
                    var url = this.ics.cmsurl + 'fileAdm/fileupload?f=upload&fn=' + this._fileName + '&id=' + this.ics._profile.t1 + '&type=10&imgUrl=' + this.ics._imgurl;
                    this.http.upload(url, this._file).subscribe(function (data) {
                        jQuery("#imagepopup").modal('hide');
                        if (data.code === 'SUCCESS') {
                            //this.flagImg = true;
                            _this.showMessage("Upload Successful", true);
                            _this.uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
                            _this._obj.videoUpload[0] = data.fileName;
                            _this.tests = _this._fileName;
                        }
                        else {
                            _this.showMessage("Upload Unsuccessful Please Try Again...", false);
                        }
                    }, function (error) { }, function () { });
                }
                else {
                    jQuery("#uploadVideo").val("");
                    this.showMessage("Choose video Associated", false);
                }
            }
        }
        /* else {
          this._fileName=this.tests;
          this.showMessage("Upload Fail", undefined);
        } */
    };
    VideoComponent.prototype.goCancle = function () {
        jQuery("#sessionalert1").modal('hide');
        jQuery("#uploadVideo1").val(this.tests);
    };
    VideoComponent.prototype.goNo = function () {
        jQuery("#sessionalert").modal('hide');
        jQuery("#imageUpload1").val(this.test);
    };
    VideoComponent.prototype.goYes = function () {
        var _this = this;
        jQuery("#sessionalert").modal('hide');
        this._fileName = this.myimg.name;
        this._size = this.myimg.size;
        if (this._size >= 1048576) {
            this.temp = (this._size / 1048576).toFixed(2) + " MB";
        }
        else if (this._size >= 1024) {
            this.temp = (this._size / 1024).toFixed(2) + " KB";
        }
        else if (this._size > 1) {
            this.temp = this._size + " bytes";
        }
        else if (this._size == 1) {
            this.temp = this._size + " byte";
        }
        else {
            this.temp = "0 bytes";
        }
        this._file = this.myimg;
        var index = this._fileName.lastIndexOf(".");
        var imagename = this._fileName.substring(index);
        imagename = imagename.toLowerCase();
        if (imagename == ".jpg" || imagename == ".jpeg" || imagename == ".png") {
            console.log("imgUrl: " + this.ics._imgurl);
            var url = this.ics.cmsurl + 'fileAdm/fileupload?f=upload&fn=' + this._fileName + '&id=' + this.ics._profile.t1 + '&type=10' + '&imgUrl=' + this.ics._imgurl;
            this.http.upload(url, this._file).subscribe(function (data) {
                if (data.code === 'SUCCESS') {
                    _this.flagImg = true;
                    console.log("videoImage: " + JSON.stringify(data));
                    var _img = _this._fileName;
                    _this.uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
                    _this.uploadData.name = data.fileName;
                    _this.uploadData.url = data.url;
                    _this._obj.uploadlist[0] = _this.uploadData;
                    _this.uploadDataListResize.push(data.sfileName);
                    _this.test = _this._fileName;
                    _this._obj.t6 = _this.temp;
                    _this.showMessage("Upload Successful", true);
                }
                else {
                    _this.showMessage("Upload Unsuccessful Please Try Again...", false);
                }
                _this._mflag = true;
            }, function (error) { }, function () { });
        }
        else {
            this._mflag = true;
            jQuery("#imageUpload1").val("");
            this._obj.uploadlist = [];
            this.showMessage("Choose Image Associated", false);
        }
        this.test = "";
    };
    VideoComponent.prototype.uploadedFileImage = function (event) {
        var _this = this;
        this._mflag = true;
        this.uploadDataListResize = [];
        if (event.target.files.length == 1) {
            //if(this.flagImg == true){
            if ((this._obj.uploadlist[0] != null)) {
                this.myimg = event.target.files[0];
                jQuery("#sessionalert").modal();
            }
            else {
                this._fileName = event.target.files[0].name;
                this._size = event.target.files[0].size;
                if (this._size >= 1048576) {
                    this.temp = (this._size / 1048576).toFixed(2) + " MB";
                }
                else if (this._size >= 1024) {
                    this.temp = (this._size / 1024).toFixed(2) + " KB";
                }
                else if (this._size > 1) {
                    this.temp = this._size + " bytes";
                }
                else if (this._size == 1) {
                    this.temp = this._size + " byte";
                }
                else {
                    this.temp = "0 bytes";
                }
                this._file = event.target.files[0];
                var index = this._fileName.lastIndexOf(".");
                var imagename = this._fileName.substring(index);
                imagename = imagename.toLowerCase();
                if (imagename == ".jpg" || imagename == ".jpeg" || imagename == ".png") {
                    console.log("imgUrl: " + this.ics._imgurl);
                    var url = this.ics.cmsurl + 'fileAdm/fileupload?f=upload&fn=' + this._fileName + '&id=' + this.ics._profile.t1 + '&type=10' + '&imgUrl=' + this.ics._imgurl;
                    this.http.upload(url, this._file).subscribe(function (data) {
                        if (data.code === 'SUCCESS') {
                            _this.flagImg = true;
                            console.log("videoImage: " + JSON.stringify(data));
                            var _img = _this._fileName;
                            _this.uploadData = { "serial": 0, "name": "", "desc": "", "order": "", "url": "" };
                            _this.uploadData.name = data.fileName;
                            _this.uploadData.url = data.url;
                            _this._obj.uploadlist[0] = _this.uploadData;
                            _this.uploadDataListResize.push(data.sfileName);
                            _this.test = _this._fileName;
                            _this._obj.t6 = _this.temp;
                            _this.showMessage("Upload Successful", true);
                        }
                        else {
                            _this.showMessage("Upload Unsuccessful Please Try Again...", false);
                        }
                        _this._mflag = true;
                    }, function (error) { }, function () { });
                }
                else {
                    this._mflag = true;
                    jQuery("#imageUpload").val("");
                    this.showMessage("Choose Image Associated", false);
                }
            }
        } /*  else {
            jQuery("#imageUpload").val(this.test);
            this.showMessage("Upload Fail", undefined);
          } */
    };
    VideoComponent.prototype.getStatusType = function () {
        var _this = this;
        try {
            var url = this.ics.cmsurl + 'serviceAdmLOV/getLovType';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            this._sessionObj.lovDesc = "StatusType";
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.msgCode == "0016") {
                        _this.showMessage(data.msgDesc, false);
                    }
                    if (data.msgCode == "0000") {
                        if (!(data.lovType instanceof Array)) {
                            var m = [];
                            m[0] = data.lovType;
                            _this.ref._lov3.StatusType = m;
                        }
                        else {
                            _this.ref._lov3.StatusType = data.lovType;
                        }
                        _this.ref._lov3.StatusType.forEach(function (iItem) {
                            var l_Item = { "value": "", "caption": "" };
                            l_Item['caption'] = iItem.caption;
                            l_Item['value'] = iItem.value;
                            _this.lovstatusType.push(iItem);
                        });
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    VideoComponent.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "primary", "t3": msg });
        }
    };
    VideoComponent.prototype.isValidate = function (obj) {
        if (obj.t1 == "" && obj.t2 == "") {
            this.showMessage("Please fill Title and Content", false);
            this.flagsave = false;
            return false;
        }
        if (obj.t1 == "") {
            this.showMessage("Please fill Title", false);
            this.flagsave = false;
            return false;
        }
        if (obj.t2 == "") {
            this.showMessage("Please fill Content", false);
            this.flagsave = false;
            return false;
        }
        //if (obj.n10 == 0 && obj.upload)
        if (!this._util.validateLanguage(obj.t2) && !this._util.validateEng(obj.t2)) {
            this.showMessage("Please change Myanmar3 Font at Content ", false);
            this.flagsave = false;
            return false;
        }
        if (obj.t8 == "" && (obj.n10 == 1 || obj.n10 == 2)) {
            this.showMessage("Please fill URL Link", false);
            this.flagsave = false;
            return false;
        }
        if ((obj.videoUpload == null || obj.videoUpload.length == 0) && obj.n10 == 0) {
            this.showMessage("Choose Associated Video", false);
            this.flagsave = false;
            return false;
        }
        if (obj.n7 == "") {
            this.showMessage("Choose Status", false);
            this.flagsave = false;
            return false;
        }
        return true;
    };
    VideoComponent.prototype.setBtns = function () {
        var k = this.ics.getBtns("/videoList");
        if (k != "" && k != undefined) {
            var strs = k.split(",");
            for (var i = 0; i < strs.length; i++) {
                if (strs[i] == "1") {
                    this.flagnew = false;
                }
                if (strs[i] == "2") {
                    this.flagsave = false;
                }
            }
        }
    };
    VideoComponent.prototype.setVideoUrl = function (str) {
        console.log("" + JSON.stringify(this.ics._imgurl + 'upload/video/' + str));
        return 'upload/video/' + str;
    };
    VideoComponent.prototype.setImgUrl = function (str) {
        console.log("" + JSON.stringify(this.ics._imgurl + '/upload/smallImage/videoImage/' + str));
        return this.ics._imgurl + '/upload/smallImage/videoImage/' + str;
    };
    VideoComponent.prototype.toggleVideo = function (event) {
        this.videoplayer.nativeElement.play();
    };
    VideoComponent.prototype.onMetadata = function (e, video) {
        console.log("Video duration...........");
        console.log('duration: ' + video.duration);
        this._obj.t7 = this.videoDurationForm(video.duration);
    };
    /* videoDurationForm(p : number){
      let hr: number;
      let hrs:String;
      let min: number;
      let mins: String;
      let sec: number;
      let secs: String;
      p = p/1000;
      sec = p%60;
      min = p/60;
      //min = min%60;
      hr = min/60;
      //sec = Math.ceil(sec);
      sec = Math.floor(sec);
      secs = this.count(sec);
      min = Math.floor(min);
      mins = this.count(min);
      hr = Math.floor(hr);
      hrs = this.count(hr);
      console.log(hrs + ":" + mins + ":" + secs);
      return hrs + ":" + mins + ":" + secs;
      
    } */
    VideoComponent.prototype.videoDurationForm = function (p) {
        var hr;
        var hrs;
        var min;
        var mins;
        var sec;
        var secs;
        hr = Math.floor(p / 3600);
        hrs = this.count(hr);
        p %= 3600;
        min = Math.floor(p / 60);
        mins = this.count(min);
        sec = p % 60;
        sec = Math.floor(sec);
        secs = this.count(sec);
        console.log(hrs + ":" + mins + ":" + secs);
        return hrs + ":" + mins + ":" + secs;
    };
    VideoComponent.prototype.count = function (p) {
        if (p < 10) {
            p = '0' + p;
        }
        return p;
    };
    VideoComponent.prototype.goView = function (p) {
        this._router.navigate(['/videoAdminView', 'read', p]);
    };
    VideoComponent.prototype.goRemove = function (obj, num) {
        this._fileName = "";
        var img = obj;
        this._obj.videoUpload = [];
    };
    VideoComponent.prototype.changeBrowe = function (p) {
        if (p == 0) {
            this.flagvideo = false;
            this.flagURL = true;
            //this.flagImage = true;
            this.flagImage = false;
        }
        else {
            this.flagvideo = true;
            this.flagURL = false;
            //this.flagImage = false;
            this.flagImage = false;
            this._obj.videoUpload = [];
            jQuery("#uploadVideo1").val('');
        }
    };
    VideoComponent.prototype.deleteFile = function (obj) {
        var _this = this;
        this.http.doGet(this.ics.cmsurl + 'fileAdm/fileRemove?fn=' + obj).subscribe(function (data) {
            console.log(data.code);
            if (data.code === 'SUCCESS') {
                _this._obj.n10 = 0;
                var index = _this.uploadDataList.indexOf(obj);
                _this.uploadDataList.splice(index, 1);
                _this.showMessage('Photo Removed!', true);
            }
        }, function (error) { }, function () { });
    };
    __decorate([
        core_1.ViewChild('myFileInput'), 
        __metadata('design:type', core_1.ElementRef)
    ], VideoComponent.prototype, "myInputVariable", void 0);
    VideoComponent = __decorate([
        core_1.Component({
            selector: 'fmr-video',
            template: "\n  <div class=\"container-fluid col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n\t<form class=\"form-horizontal\" ngNoForm>\n\t\t<legend>Video</legend>\n\t\t\t<div class=\"cardview list-height\">  \n\t\t\t\t<div class=\"col-md-12\" style=\"margin-bottom: 10px;\">\n\t\t\t\t  <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goList()\">List</button>\n\t\t\t\t  <button class=\"btn btn-sm btn-primary\" [disabled]=\"flagnew\" id=\"new\" type=\"button\" (click)=\"goNew()\">New</button>\n\t\t\t\t  <button class=\"btn btn-sm btn-primary\" [disabled]=\"flagsave\" id=\"save\" type=\"button\" (click)=\"goPost()\">Save</button>\n\t\t\t\t  <button class=\"btn btn-sm btn-primary\" [disabled]=\"flagdelete\" id=\"delete\" type=\"button\" (click)=\"goDelete();\">Delete</button>\n\t\t\t\t  <!--<button *ngIf=\"!flagview\" class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goView(_viewsk);\">View</button>-->\n\t\t\t\t</div>\n  \n\t\t\t\t<div class=\"col-md-12\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t<label class=\"col-md-2\">Title</label>\n\t\t\t\t\t\t<div class=\"col-md-4\">\n\t\t\t\t\t\t<input type=\"text\" [(ngModel)]=\"_obj.t1\" class=\"form-control input-sm uni\" maxlength=\"255\"/>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t<label class=\"col-md-2\">Status</label>\n\t\t\t\t\t\t<div class=\"col-md-4\">\n\t\t\t\t\t\t\t<select [(ngModel)]=\"_obj.n7\"  class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\" required>\n\t\t\t\t\t\t\t<option *ngFor=\"let c of lovstatusType\" value=\"{{c.value}}\">{{c.caption}}</option>\n\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>   \n\t\t\t\t\t\t  \n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-md-2\">Content</label>\n\t\t\t\t\t\t<div class=\"col-md-4 uni\">\n\t\t\t\t\t\t<textarea type=\"text\" *ngIf=\"td\" [(ngModel)]=\"_obj.t2\" class=\"form-control input-md uni\" rows=\"6\"></textarea>\n\t\t\t\t\t\t<textarea type=\"text\" *ngIf=\"!td\" [(ngModel)]=\"_obj.t2\" class=\"form-control input-md uni\" rows=\"6\"></textarea>\t\t\t\t\t\t\t\t\t\t\n            </div>\t\n            <div style=\"margin-bottom: 38px !important;\">\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t<rp-input rpLabelClass = \"col-md-2\" [(rpModel)]=\"_obj.n10\" (change)=\"changeBrowe(_obj.n10)\" rpRequired=\"true\" rpType=\"videostatus\" rpLabel=\"Type\"></rp-input>\n            </div>\n            <div style=\"margin-bottom: 76px !important;\">\n              <label class=\"col-md-2\">File Size</label>\n\t\t\t\t\t\t\t<div class=\"col-md-4\">\n\t\t\t\t\t\t\t<input type=\"text\" [(ngModel)]=\"_obj.t6\" class=\"form-control input-sm\" maxlength=\"50\" disabled />\n\t\t\t\t\t\t\t</div>\n            </div>\n            <div>\n              <label class=\"col-md-2\">Video Duration Time</label>\n\t\t\t\t\t\t\t<div class=\"col-md-4\">\n\t\t\t\t\t\t\t  <input type=\"text\" [(ngModel)]=\"_obj.t7\" class=\"form-control input-sm\" maxlength=\"50\" [disabled]='_obj.n10==0'/>\n              </div>\n            </div>  \n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"form-group\"> \n\t\t\t\t\t\t<label class=\"col-md-2\" [hidden]=\"flagImage\" for=\"imageUploadid\">Image Associated </label>\n\t\t\t\t\t\t\t<div class=\"col-md-4\" [hidden]=\"flagImage\" id=\"imageUploadid\">\n\t\t\t\t\t\t\t<input type=\"file\" id=\"imageUpload\"  class=\"file\" (change)=\"uploadedFileImage($event)\"/><!--#myFileInput-->\n\t\t\t\t\t\t\t<div class=\"input-group\">\n\t\t\t\t\t\t\t<input type=\"text\" id=\"imageUpload1\" class=\"form-control input-sm\" placeholder=\"No file chosen\" maxlength=\"100\">\n\t\t\t\t\t\t\t<span class=\"input-group-btn\">\n\t\t\t\t\t\t\t<button class=\"browse btn btn-sm btn-primary\" type=\"button\"><i class=\"glyphicon glyphicon-search\"></i> Browse</button>                         \n\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"col-md-4\">(jpeg, png, jpg) </div>                  \n\t\t\t\t\t\t\t</div> \n\t\t\t\t\t\t\t\n\t\t\t\t\t\t<label class=\"col-md-2\" [hidden]=\"flagURL\">URL Link</label>\n\t\t\t\t\t\t\t<div class=\"col-md-4\" [hidden]=\"flagURL\">\n\t\t\t\t\t\t\t<input type=\"text\" [(ngModel)]=\"_obj.t8\" class=\"form-control input-sm\" />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\n\t\t\t\t\t\t<label class=\"col-md-2\" [hidden]=\"flagvideo\" for=\"uploadVideoid\">Video Associated </label>\n\t\t\t\t\t\t\t<div class=\"col-md-4\" [hidden]=\"flagvideo\" id=\"uploadVideoid\">\n\t\t\t\t\t\t\t  <input type=\"file\" id=\"uploadVideo\" class=\"file\"  (change)=\"uploadedFile($event)\" placeholder=\"Upload video...\"/>\n\t\t\t\t\t\t\t<div class=\"input-group\">\n\t\t\t\t\t\t\t<input type=\"text\"  id=\"uploadVideo1\" class=\"form-control input-sm\" placeholder=\"No file chosen\"  maxlength=\"100\">\n\t\t\t\t\t\t\t<span class=\"input-group-btn\">\n\t\t\t\t\t\t\t<button class=\"browse btn btn-sm btn-primary\" type=\"button\"><i class=\"glyphicon glyphicon-search\"></i> Browse</button>\n\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-md-4\" *ngIf='flagvideo==false'>(mp4)</div> \n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t</div>\n           \t\t\t  \t\t\t\t\t  \t\t\t  \t\t\t\t\t\t\n\t\t\t\t</div>\n       \n\t\t\t\t<div class=\"col-md-6\" *ngIf=\"_obj.uploadlist.length!=0\">\n\t\t\t\t\t<div class=\"col-md-4\"></div>\n\t\t\t\t\t<div calss=\"col-md-8\">\n\t\t\t\t\t\t<div><!--*ngFor=\"let img of _obj.uploadlist\" -->\n\t\t\t\t\t\t  <img src=\"{{setImgUrl(_obj.uploadlist[0].name)}}\" onError=\"this.src='./image/image_not_found.png';\" alt={{img}} height=\"240\" width=\"400\"/><!--{{setImgUrl(_obj.uploadlist[0].name)}}-->\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-md-6\" *ngIf=\"_obj.videoUpload.length!=0\"></div>\n\t\t\t\t<div class=\"col-md-6\" *ngIf=\"_obj.videoUpload.length!=0\">\n\t\t\t\t\t<div class=\"col-md-4\"></div>\n\t\t\t\t\t<div calss=\"col-md-8\">\n\t\t\t\t\t\t<div *ngIf='flagURL==true'>\n\t\t\t\t\t\t\t<div *ngFor=\"let img of _obj.videoUpload;let num = index\" >\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t<video #videoPlayer id=\"{{img}}\" type=\"video/mp4\" (loadedmetadata)=\"onMetadata($event, videoPlayer)\" controls height=\"240\" width=\"400\">\n\t\t\t\t\t\t\t\t<source src=\"{{videoUrl}}/{{setVideoUrl(img)}}\" type=\"video/mp4\" />\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t</video>\n\t\t\t\t\t\t\t\t<canvas id=\"canvas\" style=\"display:none\"></canvas>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t</div>\t\n    </form>\n</div>\n<div id=\"sessionalert\" class=\"modal\" tabindex=\"-1\" role=\"dialog\">\n<div class=\"modal-dialog modal-sm\" id=\"rootpopupsize\">\n  <div class=\"modal-content\">\n  <div class=\"modal-header\">\n  \n  <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\" (click)=\"goNo()\">\n    <span aria-hidden=\"true\">&times;</span>\n  </button>\n</div>\n    <div class=\"modal-body\">\n      <p>Do you want to Overwrite?</p>\n    </div>\n    <div class=\"modal-footer\">\n      <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goYes()\">OK</button>\n      <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goNo()\">Cancle</button>\n    </div>\n  </div>\n</div>\n</div>\n<div id=\"sessionalert1\" class=\"modal\" tabindex=\"-1\" role=\"dialog\">\n<div class=\"modal-dialog modal-sm\" id=\"rootpopupsize\">\n  <div class=\"modal-content\">\n  <div class=\"modal-header\">\n  \n  <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\" (click)=\"goCancle()\">\n    <span aria-hidden=\"true\">&times;</span>\n  </button>\n</div>\n    <div class=\"modal-body\">\n      <p>Do you want to Overwrite?</p>\n    </div>\n    <div class=\"modal-footer\">\n      <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goOK()\">OK</button>\n      <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goCancle()\">Cancle</button>\n    </div>\n  </div>\n</div>\n</div>\n  <!-- processing image modal -->\n  <div id=\"imagepopup\" class=\"modal fade\" role=\"dialog\" style=\" margin-top: 200px; margin-left: 150px;\">\n    <div id=\"imagepopupsize\" class=\"modal-dialog modal-lg\" style=\"width : 240px\">\n      <div class=\"modal-content\">\n        <div id=\"imagepopupbody\" class=\"modal-body\">\n          <img src=\"image/processing.gif\" style=\"padding-top:30px;\">\n        </div>\n      </div>\n    </div>\n  </div>\n  <div [hidden]=\"_mflag\">\n  <div  id=\"loader\" class=\"modal\" ></div>\n"
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef, core_1.Renderer, rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], VideoComponent);
    return VideoComponent;
}());
exports.VideoComponent = VideoComponent;
//# sourceMappingURL=video.component.js.map