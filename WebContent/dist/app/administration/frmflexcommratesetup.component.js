"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var Rx_1 = require('rxjs/Rx');
core_2.enableProdMode();
var FrmFlexCommRateSetupComponent = (function () {
    function FrmFlexCommRateSetupComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
        this._list = { "commdetailArr": [{ "syskey": 0, "hkey": 0, "zone": "", "commType": "", "fromAmt": "", "toAmt": "", "amount": "", "minAmt": "", "maxAmt": "", "gLorAC": "" }], "mainType": "", "commRef": "TBA", "msgCode": "", "msgDesc": "", "createdUserID": "", "sessionID": "", "description": "" };
        this._showlist = { "commdetailArr": [{ "syskey": 0, "hkey": "", "zone": "", "commType": "", "fromAmt": "", "toAmt": "", "amount": "", "minAmt": "", "maxAmt": "", "gLorAC": "" }], "mainType": "", "commRef": "TBA", "msgCode": "", "msgDesc": "", "createdUserID": "", "sessionID": "", "description": "" };
        this._obj = this.getDefaultObj();
        this._mflag = false;
        this._key = "";
        this.hKey = 0;
        this.sessionAlertMsg = "";
        this._zonecaption = "";
        this._util = new rp_client_util_1.ClientUtil();
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = false;
            this.checkSession();
            this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
            this._obj = this.getDefaultObj();
            this.getZone();
        }
    }
    FrmFlexCommRateSetupComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this._key = id;
                _this.goReadBySyskey(id);
            }
        });
    };
    FrmFlexCommRateSetupComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    FrmFlexCommRateSetupComponent.prototype.goSave = function () {
        var _this = this;
        this._mflag = false;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "msgCode": "" };
        if (this._list.commdetailArr.length < 1) {
            this._returnResult.state = 'false';
            this._returnResult.msgDesc = "No data to save";
            this._mflag = true;
        }
        if (this._list.description == '') {
            this._returnResult.state = "false";
            this._returnResult.msgDesc = "Blank Description";
            this._mflag = true;
        }
        if (this._returnResult.msgDesc == '') {
            try {
                this._list.createdUserID = this.ics._profile.userID;
                this._list.sessionID = this.ics._profile.sessionID;
                this._list.commdetailArr[0].hkey = this.hKey;
                var url = this.ics._apiurl + 'service001/saveFlexCommRate';
                var json = this._list;
                this.http.doPost(url, json).subscribe(function (data) {
                    _this._obj = data;
                    _this._list.commRef = _this._obj.commRef;
                    _this._list.commdetailArr[0].hkey = _this._obj.hkey;
                    _this.hKey = _this._obj.hkey;
                    _this._returnResult.msgDesc = _this._obj.msgDesc;
                    _this._returnResult.state = _this._obj.state;
                    if (_this._obj.state == 'false') {
                        if (_this._obj.msgCode == '0016') {
                            _this.showMessage();
                        }
                    }
                    else {
                        jQuery("#mydelete").prop("disabled", false);
                    }
                    _this.showMessageAlert(_this._returnResult.msgDesc);
                    _this._mflag = true;
                }, function (error) {
                    if (error._body.type == 'error') {
                        alert("Connection Timed Out!");
                    }
                }, function () { });
            }
            catch (e) {
                alert("Invalid URL");
            }
        }
        else {
            this.showMessageAlert(this._returnResult.msgDesc);
        }
    };
    FrmFlexCommRateSetupComponent.prototype.goRemove = function (i) {
        var index = this._showlist.commdetailArr.indexOf(i);
        this._list.commdetailArr.splice(index, 1);
        this._showlist.commdetailArr.splice(index, 1);
    };
    FrmFlexCommRateSetupComponent.prototype.getDefaultObj = function () {
        this.isMainType = true;
        this.chkMainType = false;
        this.isPercentage = true;
        this._list = { "commdetailArr": [{ "syskey": 0, "hkey": 0, "zone": "", "commType": "", "fromAmt": "", "toAmt": "", "amount": "", "minAmt": "", "maxAmt": "", "gLorAC": "" }], "mainType": "", "commRef": "TBA", "msgCode": "", "msgDesc": "", "createdUserID": "", "sessionID": "", "description": "" };
        this._list.commdetailArr = [];
        this._showlist.commdetailArr = [];
        this.goRemove(0);
        jQuery("#mydelete").prop("disabled", true);
        jQuery("#mySave").prop("disabled", false);
        return { "createdUserID": "", "sessionID": "", "hkey": 0, "msgCode": "", "msgDesc": "", "syskey": 0, "commRef": "TBA", "description": "", "mainType": "", "zone": "", "amount": "", "fromAmt": "", "toAmt": "", "minAmt": "", "maxAmt": "", "commType": "", "state": "" };
    };
    FrmFlexCommRateSetupComponent.prototype.goReadBySyskey = function (p) {
        var _this = this;
        try {
            this._mflag = false;
            var url = this.ics._apiurl + 'service001/getFlexCommRateDataByID?id=' + p + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
            this.http.doGet(url).subscribe(function (data) {
                if (data != null) {
                    if (data.commdetailArr != null) {
                        if (!(data.commdetailArr instanceof Array)) {
                            var m = [];
                            m[0] = data.commdetailArr;
                            _this._list.commdetailArr = m;
                            _this._list.commRef = data.commRef;
                            _this._list.description = data.description;
                            _this._list.mainType = data.mainType;
                            for (var i = 0; i < _this.ref._lov1.ref026.length; i++) {
                                if (data.commdetailArr.zone == _this.ref._lov1.ref026[i].value) {
                                    _this._zonecaption = _this.ref._lov1.ref026[i].caption;
                                    _this._showlist.commdetailArr.push({ "syskey": 0, "hkey": "", "zone": _this._zonecaption, "commType": data.commdetailArr.commType, "fromAmt": String(Number.parseFloat(data.commdetailArr.fromAmt).toFixed(2)), "toAmt": String(Number.parseFloat(data.commdetailArr.toAmt).toFixed(2)), "amount": String(Number.parseFloat(data.commdetailArr.amount).toFixed(2)), "minAmt": String(Number.parseFloat(data.commdetailArr.minAmt).toFixed(2)), "maxAmt": String(Number.parseFloat(data.commdetailArr.maxAmt).toFixed(2)), "gLorAC": "" });
                                    break;
                                }
                            }
                        }
                        else {
                            _this._list = data;
                            for (var j = 0; j < data.commdetailArr.length; j++) {
                                for (var i = 0; i < _this.ref._lov1.ref026.length; i++) {
                                    if (data.commdetailArr[j].zone == _this.ref._lov1.ref026[i].value) {
                                        _this._zonecaption = _this.ref._lov1.ref026[i].caption;
                                        _this._obj.zone = _this.ref._lov1.ref026[i].value;
                                        _this._showlist.commdetailArr.push({ "syskey": 0, "hkey": "", "zone": _this._zonecaption, "commType": data.commdetailArr[j].commType, "fromAmt": String(Number.parseFloat(data.commdetailArr[j].fromAmt).toFixed(2)), "toAmt": String(Number.parseFloat(data.commdetailArr[j].toAmt).toFixed(2)), "amount": String(Number.parseFloat(data.commdetailArr[j].amount).toFixed(2)), "minAmt": String(Number.parseFloat(data.commdetailArr[j].minAmt).toFixed(2)), "maxAmt": String(Number.parseFloat(data.commdetailArr[j].maxAmt).toFixed(2)), "gLorAC": "" });
                                        break;
                                    }
                                }
                            }
                        }
                        _this.hKey = _this._list.commdetailArr[0].hkey;
                        if (_this._list.mainType == "0") {
                            _this.chkMainType = false;
                            _this.isMainType = true;
                        }
                        else {
                            _this.chkMainType = true;
                            _this.isMainType = false;
                        }
                    }
                }
                if (_this._list.msgCode == '0016') {
                    _this._returnResult.msgDesc = _this._list.msgDesc;
                    _this.showMessage();
                }
                jQuery("#mydelete").prop("disabled", false);
                jQuery("#mySave").prop("disabled", false);
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmFlexCommRateSetupComponent.prototype.goList = function () {
        this._router.navigate(['/flexcommratelist']);
    };
    FrmFlexCommRateSetupComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this.goLogOut();
        });
    };
    FrmFlexCommRateSetupComponent.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    /* checkSession() {
      try {
    
        let url: string = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
        this.http.doGet(url).subscribe(
          data => {
            if (data.msgCode == '0016') {
              this.sessionAlertMsg = data.msgDesc;
              this.showMessage();
            }
          },
          error => {
            if (error._body.type == 'error') {
              alert("Connection Timed Out!");
            }
          }, () => { });
      } catch (e) {
        alert("Invalid URL");
      }
    
    } */
    FrmFlexCommRateSetupComponent.prototype.showMessageAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmFlexCommRateSetupComponent.prototype.clearData = function () {
        this._obj = this.getDefaultObj();
        this.hKey = 0;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
        jQuery("#mySave").prop("disabled", false);
        jQuery("#mydelete").prop("disabled", true);
    };
    FrmFlexCommRateSetupComponent.prototype.goNew = function () {
        this.clearData();
    };
    FrmFlexCommRateSetupComponent.prototype.changeCommissionType = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex; //Get Selected Index
        var value = options[options.selectedIndex].value; //Get Selected Index's Value  
        this._obj.commType = value;
        if (this._obj.commType == '1') {
            this.isPercentage = false;
        }
        else {
            this.isPercentage = true;
        }
        for (var i = 1; i < this.ref._lov1.ref027.length; i++) {
            if (this.ref._lov1.ref027[i].value == value) {
                this._obj.commType = this.ref._lov1.ref027[i].value;
            }
        }
    };
    FrmFlexCommRateSetupComponent.prototype.goAddAll = function (p) {
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "msgCode": "" };
        if (this._obj.amount == '') {
            this._returnResult.state = "false";
            this._returnResult.msgDesc = "Blank Commission Amount";
        }
        else if (!/^([0-9.]{1,50})$/.test(this._obj.amount)) {
            this._returnResult.state = 'false';
            this._returnResult.msgDesc = "Amount should be number";
        }
        else if (this._obj.zone == '') {
            this._returnResult.state = "false";
            this._returnResult.msgDesc = "Blank Zone";
        }
        if (this.isMainType == false) {
            if (this._obj.fromAmt == '') {
                this._returnResult.state = "false";
                this._returnResult.msgDesc = "Blank From Amount";
            }
            else if (!/^([0-9.]{1,50})$/.test(this._obj.fromAmt)) {
                this._returnResult.state = 'false';
                this._returnResult.msgDesc = "From Amount should be number";
            }
            if (this._obj.toAmt == '') {
                this._returnResult.state = "false";
                this._returnResult.msgDesc = "Blank To Amount";
            }
            else if (!/^([0-9.]{1,50})$/.test(this._obj.toAmt)) {
                this._returnResult.state = 'false';
                this._returnResult.msgDesc = "To Amount should be number";
            }
        }
        if (this.isPercentage == false) {
            if (this._obj.minAmt == '') {
                this._returnResult.state = "false";
                this._returnResult.msgDesc = "Blank Minimum Amount";
            }
            else if (!/^([0-9.]{1,50})$/.test(this._obj.minAmt)) {
                this._returnResult.state = 'false';
                this._returnResult.msgDesc = "Minimum Amount should be number";
            }
            if (this._obj.maxAmt == '') {
                this._returnResult.state = "false";
                this._returnResult.msgDesc = "Blank Maximum Amount";
            }
            else if (!/^([0-9.]{1,50})$/.test(this._obj.maxAmt)) {
                this._returnResult.state = 'false';
                this._returnResult.msgDesc = "Maximum Amount should be number";
            }
        }
        if (this._returnResult.msgDesc == '') {
            this._list.commdetailArr.push({ "syskey": 0, "hkey": 0, "zone": this._obj.zone, "commType": this._obj.commType, "fromAmt": this._obj.fromAmt, "toAmt": this._obj.toAmt, "amount": this._obj.amount, "minAmt": this._obj.minAmt, "maxAmt": this._obj.maxAmt, "gLorAC": "" });
            this._showlist.commdetailArr.push({ "syskey": 0, "hkey": "", "zone": this._zonecaption, "commType": this._obj.commType, "fromAmt": this._obj.fromAmt, "toAmt": this._obj.toAmt, "amount": this._obj.amount, "minAmt": this._obj.minAmt, "maxAmt": this._obj.maxAmt, "gLorAC": "" });
        }
        else {
            this.showMessageAlert(this._returnResult.msgDesc);
        }
    };
    FrmFlexCommRateSetupComponent.prototype.getZone = function () {
        var _this = this;
        try {
            this._mflag = false;
            this.http.doGet(this.ics._apiurl + 'service001/getZone').subscribe(function (data) {
                _this.ref._lov1.ref026 = data.ref026;
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmFlexCommRateSetupComponent.prototype.changeZone = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex; //Get Selected Index
        var value = options[options.selectedIndex].value; //Get Selected Index's Value  
        this._obj.zone = value;
        this._zonecaption = options[options.selectedIndex].text;
        for (var i = 1; i < this.ref._lov1.ref026.length; i++) {
            if (this.ref._lov1.ref026[i].value == value) {
                this._obj.zone = this.ref._lov1.ref026[i].value;
            }
        }
    };
    FrmFlexCommRateSetupComponent.prototype.checkMainType = function (event) {
        if (event.target.checked) {
            this.isMainType = false;
            this._list.mainType = "1";
        }
        else {
            this.isMainType = true;
            this._list.mainType = "0";
        }
    };
    FrmFlexCommRateSetupComponent.prototype.goDelete = function () {
        var _this = this;
        try {
            this._mflag = false;
            this._list.sessionID = this.ics._profile.sessionID;
            this._list.createdUserID = this.ics._profile.userID;
            var url = this.ics._apiurl + 'service001/deleteFlexCommRate';
            var json = this._list;
            this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "msgCode": "" };
            this.http.doPost(url, json).subscribe(function (data) {
                _this._returnResult = data;
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMessage();
                }
                else {
                    _this.showMessageAlert(data.msgDesc);
                    if (data.state) {
                        jQuery("#mydelete").prop("disabled", true);
                        jQuery("#mySave").prop("disabled", false);
                        _this._obj = { "createdUserID": "", "sessionID": "", "msgCode": "", "hkey": 0, "msgDesc": "", "syskey": 0, "commRef": "TBA", "description": "", "mainType": "", "zone": "", "amount": "", "fromAmt": "", "toAmt": "", "minAmt": "", "maxAmt": "", "commType": "", "state": "" };
                        _this._list = {
                            "commdetailArr": [{ "syskey": 0, "hkey": 0, "zone": "", "commType": "", "fromAmt": "", "toAmt": "", "amount": "", "minAmt": "", "maxAmt": "", "gLorAC": "" }], "mainType": "", "commRef": "TBA", "msgCode": "", "msgDesc": "", "createdUserID": "", "sessionID": "",
                            "description": ""
                        };
                        _this._showlist = { "commdetailArr": [{ "syskey": 0, "hkey": "", "zone": "", "commType": "", "fromAmt": "", "toAmt": "", "amount": "", "minAmt": "", "maxAmt": "", "gLorAC": "" }], "mainType": "", "commRef": "TBA", "msgCode": "", "msgDesc": "", "createdUserID": "", "sessionID": "", "description": "" };
                        _this.goRemove(0);
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
            this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "msgCode": "" };
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmFlexCommRateSetupComponent.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    FrmFlexCommRateSetupComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsgAlert(data.desc);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsgAlert(data.desc);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmFlexCommRateSetupComponent.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmFlexCommRateSetupComponent.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    FrmFlexCommRateSetupComponent = __decorate([
        core_1.Component({
            selector: 'flexcommrate-setup',
            template: "\n    <div class=\"container\">\n    <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n    <form class= \"form-horizontal\" (ngSubmit) = \"goSave()\"> \n    <!-- Form Name -->\n    <legend>Flex Comm Rate</legend>\n    <div class=\"row  col-md-12\">  \n      <button class=\"btn btn-primary\" type=\"button\" (click)=\"goList()\" >List</button> \n      <button class=\"btn btn-primary\" type=\"button\" (click)=\"goNew()\" >New</button>      \n      <button class=\"btn btn-primary\" id=\"mySave\" type=\"submit\" >Save</button>          \n      <button class=\"btn btn-primary\" disabled id=\"mydelete\"  type=\"button\" (click)=\"goDelete();\" >Delete</button> \n    </div>\n    <div class=\"row col-md-12\">&nbsp;</div> \n\n    <div class=\"form-group\">    \n    <div class=\"col-md-10\">\n\n    <div class=\"form-group\">\n      <rp-input [(rpModel)]=\"_list.commRef\"rpType=\"text\" rpLabel=\"Commission Reference\" rpReadonly=\"true\"></rp-input>\n    </div>\n\n    <div class=\"form-group\">\n      <rp-input [(rpModel)]=\"_list.description\"rpType=\"text\" rpLabel=\"Description \" rpLabelRequired=\"true\"></rp-input>\n    </div>\n\n    <div class=\"form-group\" >          \n      <label class=\"col-md-3\" > Main Type <font class=\"mandatoryfont\">*</font></label> \n      <div class=\"col-md-4\">\n          <input type = \"checkbox\" [(ngModel)]=chkMainType (change)=\"checkMainType($event)\" [ngModelOptions]=\"{standalone: true}\"> &nbsp; Is Tier                   \n      </div>\n    </div> \n\n    <div class=\"form-group\">\n      <label class=\"col-md-3\"> Zone <font class=\"mandatoryfont\">*</font> </label>\n      <div class=\"col-md-4\">\n        <select [ngModelOptions]=\"{standalone: true}\"  (change)=\"changeZone($event)\"  [(ngModel)]=_obj.zone class=\"form-control col-md-0\">\n          <option *ngFor=\"let item of ref._lov1.ref026\" value=\"{{item.value}}\" >{{item.caption}}</option> \n        </select>                \n      </div> \n    </div>\n\n    <div class=\"form-group\">\n    <label class=\"col-md-3\"> Commission Type <font class=\"mandatoryfont\">*</font> </label>\n    <div class=\"col-md-4\">\n      <select [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=_obj.commType (change)=\"changeCommissionType($event)\"  class=\"form-control col-md-0\">\n        <option *ngFor=\"let item of ref._lov1.ref027\" value=\"{{item.value}}\" >{{item.caption}}</option> \n      </select>                \n      </div> \n    </div>\n    \n    <div *ngIf=\"isMainType == false\">\n      <div class=\"form-group\">\n        <rp-input [(rpModel)]=\"_obj.fromAmt\" rpLabel=\"From Amount \" rpType=\"text\" rpLabelRequired=\"true\"></rp-input>\n      </div>\n\n      <div class=\"form-group\">\n        <rp-input [(rpModel)]=\"_obj.toAmt\" rpLabel=\"To Amount \" rpType=\"text\" rpLabelRequired=\"true\"></rp-input>\n      </div>\n    </div>\n\n    <div class=\"form-group\">\n      <rp-input [(rpModel)]=\"_obj.amount\" rpLabel=\"Commission Amount \" rpType=\"text\" rpLabelRequired=\"true\"></rp-input>\n    </div>\n\n    <div *ngIf=\"isPercentage == false\">\n      <div class=\"form-group\">\n        <rp-input [(rpModel)]=\"_obj.minAmt\" rpLabel=\"Minimum Amount \" rpType=\"text\" rpLabelRequired=\"true\"></rp-input>\n      </div>\n\n      <div class=\"form-group\">\n        <rp-input [(rpModel)]=\"_obj.maxAmt\" rpLabel=\"Maximum Amount \" rpType=\"text\" rpLabelRequired=\"true\"></rp-input>\n      </div>\n    </div>\n\n    <div class=\"form-group\"> \n      <form> <button class=\"btn btn-primary\" id=\"myadd\" (click)=\"goAddAll()\" >Add</button> </form>  \n    </div> \n\n    </div>\n    </div>\n\n    <div class=\"form-group\">         \n    <!--Start Table-->\n      <table class=\"table table-striped table-condensed table-hover tblborder\" style=\"font-size:14px;\">\n        <thead>\n          <tr>\n            <th>Sr No.</th>\n            <th>Zone</th>\n            <th>Commission Type</th>\n            <th>From Amount</th>\n            <th>To Amount</th>\n            <th>Commission Amount</th>\n            <th>Minimum Amount </th>\n            <th>Maximum Amount </th>  \n            <th>&nbsp; </th> \n          </tr>\n        </thead>                \n        <tbody>\n          <tr *ngFor = \"let obj of _showlist.commdetailArr,let i=index\">                  \n            <td class = \"col-md-1 right\">  {{i+1 }}</td>\n            <td class = \"col-md-2\" > {{obj.zone}} </td>                    \n            <td class = \"col-md-2\" *ngIf=\"obj.commType == 0\"> Flat </td>\n            <td class = \"col-md-2\" *ngIf=\"obj.commType == 1\"> Percentage </td>                    \n            <td class = \"col-md-2 right\" > {{obj.fromAmt}}</td>\n            <td class = \"col-md-2 right\" > {{obj.toAmt}} </td>\n            <td class = \"col-md-2 right\" > {{obj.amount}} </td>  \n            <td class = \"col-md-2 right\" > {{obj.minAmt}} </td>\n            <td class = \"col-md-2 right\" > {{obj.maxAmt}} </td>  \n            <td> <img src=\"image/remove.png\" alt=\"remove.png\" height=\"20\" width=\"20\"  (click)=\"goRemove(obj)\"/></td>\n          </tr>\n        </tbody>\n        </table> \n     </div>\n    <!--End Table--> \n    </form>\n    </div>\n    </div>\n    </div>\n       \n    <div id=\"sessionalert\" class=\"modal fade\">\n      <div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n          <div class=\"modal-body\">\n            <p>{{sessionAlertMsg}}</p>\n          </div>\n          <div class=\"modal-footer\">\n          </div>\n        </div>\n      </div>\n    </div>\n\n<div [hidden] = \"_mflag\">\n  <div class=\"modal\" id=\"loader\"></div>\n</div> \n    \n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmFlexCommRateSetupComponent);
    return FrmFlexCommRateSetupComponent;
}());
exports.FrmFlexCommRateSetupComponent = FrmFlexCommRateSetupComponent;
//# sourceMappingURL=frmflexcommratesetup.component.js.map