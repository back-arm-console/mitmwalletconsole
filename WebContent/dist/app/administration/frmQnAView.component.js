"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var FrmQnAViewComponent = (function () {
    function FrmQnAViewComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._totalcount = 1;
        this._searchVal = "";
        this.key = 0;
        this._time = "";
        this._array = [];
        this._likearr = [];
        this._pager = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 0, "size": 0, "totalcount": 1 };
        this._image = [];
        this.count = 0;
        this.syskey = 0;
        this._cmtArray = [];
        this._replyArray = [];
        this._obj = [];
        this._tmpObj = { '_key': 0, '_index': 0 };
        this._util = new rp_client_util_1.ClientUtil();
        this._ansObj = { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": "", "modifiedDate": "", "modifiedTime": "", "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [] };
        this._replyObj = { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": "", "modifiedDate": "", "modifiedTime": "", "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [] };
        this._answer = [{ "value": "", "caption": "" }];
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        this._time = this._util.getTodayTime();
        this._ansObj.createdTime = this._time;
        this._ansObj.modifiedTime = this._time;
        this._replyObj.createdTime = this._time;
        this._replyObj.modifiedTime = this._time;
        this.syskey = Number(this.ics._profile.t1);
    }
    FrmQnAViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "new") {
                _this.goNew();
            }
            else if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this.goGet(id);
            }
        });
    };
    FrmQnAViewComponent.prototype.goGet = function (p) {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'serviceQuestion/viewByID?id=' + p).subscribe(function (response) {
            if (response.state) {
                _this._obj = response.data;
                _this.key = response.data[0].n7;
            }
        }, function (error) { }, function () { });
    };
    FrmQnAViewComponent.prototype.search = function (p) {
        var _this = this;
        if (p.end == 0) {
            p.end = this.ics._profile.n1;
        }
        if (p.size == 0) {
            p.size = this.ics._profile.n1;
        }
        var url = this.ics._apiurl + 'serviceQuestion/searchQuestion?searchVal=' + this._searchVal;
        var json = p;
        this.http.doPost(url, json).subscribe(function (response) {
            if (response != null && response != undefined && response.state) {
                _this._totalcount = response.totalCount;
                _this._array = response.data;
            }
            else {
                _this._array = [];
                _this._totalcount = 1;
                _this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": "Data not found!" });
            }
        }, function (error) { }, function () { });
    };
    FrmQnAViewComponent.prototype.searchVal = function () {
        this._pager = { "current": 1, "prev": 1, "last": 1, "next": 1, "start": 1, "end": 0, "size": 0, "totalcount": 1 };
        this.search(this._pager);
    };
    FrmQnAViewComponent.prototype.goto = function (p) {
        this._router.navigate(['/question', 'read', p]);
    };
    FrmQnAViewComponent.prototype.goNew = function () {
        this._router.navigate(['/question', 'new']);
    };
    FrmQnAViewComponent.prototype.goLike = function (key) {
        var _this = this;
        jQuery("#likeModal").modal();
        this.http.doGet(this.ics._apiurl + 'serviceQuestion/searchLike?key=' + key).subscribe(function (response) {
            if (response.state)
                _this._likearr = response.data;
        }, function (error) { }, function () { });
    };
    FrmQnAViewComponent.prototype.changedPager = function (event) {
        var k = event.flag;
        this._pager = event.obj;
        if (k) {
            this.search(this._pager);
        }
    };
    FrmQnAViewComponent.prototype.goClickLike = function (key, index) {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'serviceQuestion/clickLikeQuestion?key=' + key + '&userSK=' + this.key).subscribe(function (data) {
            if (data.state) {
                _this.goLikeCount(key, index);
            }
        }, function (error) { }, function () { });
    };
    FrmQnAViewComponent.prototype.ClickLike = function (key, index) {
        if (this.count == 0 && key == this._obj[index].syskey) {
            this._obj[index].n2 = this._obj[index].n2 + 1;
            this.count = this.count + 1;
            this.goClickLike(key, index);
        }
        else if (this.count == 1 && key == this._obj[index].syskey) {
            this._obj[index].n2 = this._obj[index].n2 - 1;
            this.count = 0;
            this.goClickLike(key, index);
        }
    };
    FrmQnAViewComponent.prototype.goLikeCount = function (key, index) {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'serviceQuestion/searchLikeCount?key=' + key).subscribe(function (response) {
            if (response.state)
                _this._obj = response.data;
        }, function (error) { }, function () { });
    };
    FrmQnAViewComponent.prototype.goClickUnlike = function (key, index) {
        this.http.doGet(this.ics._apiurl + 'serviceQuestion/clickUnlikeQuestion?key=' + key).subscribe(function (data) {
            if (data.state)
                console.log("Unlike...");
        }, function (error) { }, function () { });
    };
    FrmQnAViewComponent.prototype.goAnswer = function (key, index) {
        var _this = this;
        jQuery("#commentModal").modal();
        this._tmpObj._key = key;
        this._tmpObj._index = index;
        this._cmtArray = [];
        this.http.doGet(this.ics._apiurl + 'serviceQuestion/getAnswers?id=' + key).subscribe(function (response) {
            if (response.state)
                _this._cmtArray = response.data;
        }, function (error) { }, function () { });
    };
    FrmQnAViewComponent.prototype.saveAnswer = function (cmtObj) {
        var _this = this;
        cmtObj.n1 = this._tmpObj._key;
        cmtObj.n5 = Number(this.ics._profile.t1);
        cmtObj.t12 = '';
        cmtObj.t13 = '';
        if (cmtObj.t2 != '') {
            var url = this.ics._apiurl + 'serviceQuestion/saveAnswer';
            var json = cmtObj;
            this.http.doPost(url, json).subscribe(function (response) {
                _this._cmtArray = [];
                if (response.state)
                    _this._cmtArray = response.data;
                _this._ansObj.t2 = "";
            }, function (error) {
                _this.showMessage("Can't Saved This Record!!!", undefined);
            }, function () { });
        }
    };
    FrmQnAViewComponent.prototype.replyAnswer = function (key, num) {
        var _this = this;
        this._tmpObj._index = num;
        this._cmtArray[num].t13 = 'true';
        this._replyArray = [];
        this.http.doGet(this.ics._apiurl + 'serviceQuestion/getAnswers?id=' + key).subscribe(function (response) {
            if (response.state)
                _this._replyArray = response.data;
        }, function (error) { }, function () { });
    };
    FrmQnAViewComponent.prototype.saveReply = function (skey) {
        var _this = this;
        this._replyObj.n1 = skey;
        this._replyObj.n3 = -1;
        this._replyObj.n5 = Number(this.ics._profile.t1);
        if (this._replyObj.t2 != '') {
            var url = this.ics._apiurl + 'serviceQuestion/saveAnswer';
            var json = this._replyObj;
            this.http.doPost(url, json).subscribe(function (response) {
                _this._replyArray = [];
                if (response.state)
                    _this._replyArray = response.data;
                _this._cmtArray[_this._tmpObj._index].n3 = _this._cmtArray[_this._tmpObj._index].n3 + 1;
                _this._replyObj.t2 = "";
            }, function (error) {
                _this.showMessage("Can't Saved This Record!!!", undefined);
            }, function () { });
        }
    };
    FrmQnAViewComponent.prototype.editAnswer = function (obj, num) {
        this._cmtArray[num].t12 = 'true';
    };
    FrmQnAViewComponent.prototype.deleteAnswer = function (obj, num) {
        var _this = this;
        var url = this.ics._apiurl + 'serviceQuestion/deleteAnswer';
        var json = obj;
        this.http.doPost(url, json).subscribe(function (response) {
            _this._cmtArray = [];
            if (response.state)
                _this._cmtArray = response.data;
        }, function (error) { }, function () { });
    };
    FrmQnAViewComponent.prototype.convertToAns = function (obj, num) {
        var _this = this;
        obj.n5 = Number(this.ics._profile.t1);
        var url = this.ics._apiurl + 'serviceQuestion/convertToAns';
        var json = obj;
        this.http.doPost(url, json).subscribe(function (response) {
            _this._cmtArray = [];
            if (response.state) {
                _this._cmtArray = response.data;
                console.log(JSON.stringify(_this._cmtArray) + " convert to answer ");
            }
        }, function (error) { }, function () { });
    };
    FrmQnAViewComponent.prototype.setImgUrl = function (str) {
        return 'upload/image/' + str;
    };
    FrmQnAViewComponent.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    FrmQnAViewComponent.prototype.goImage = function (key, index) {
        var _this = this;
        jQuery("#commentModal").modal();
        this._tmpObj._key = key;
        this._tmpObj._index = index;
        this._image = [];
        this.http.doGet(this.ics._apiurl + 'serviceQuestion/getImage?id=' + key).subscribe(function (response) {
            if (response.state) {
                _this._image = response.uploads;
            }
        }, function (error) { }, function () { });
    };
    FrmQnAViewComponent = __decorate([
        core_1.Component({
            selector: 'fmr-QnAView',
            template: "\n    <div class=\"container\">\n    <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n    <form class=\"form-inline\" ngNoForm> \n    <legend>Question View</legend>\n        <div class=\"row  col-md-12\"> \n            <button class=\"btn btn-danger\" [disabled]=\"flagnew\" id=\"new\" type=\"button\" (click)=\"goNew()\">Back</button>      \n        </div>      \n    </form>\n  \n    <hr>\n       <div  *ngFor=\"let obj of _obj;let num = index\" class=\"col-md-12\">\n            <span style=\"color:gray;font-style:italic;font-size:11px\">{{obj.userName}}</span>\n            <div class=\"form-group\">\n                <div *ngFor=\"let img of obj.upload\">\n                    <img src={{setImgUrl(img)}} alt={{img}} height=\"240\" width=\"400\" />\n                </div>\n            </div>\n            <p>Quesiton: {{obj.t2}}</p>\n            <div class=\"form-group\">\n                <div *ngFor=\"let ans of obj.answer;let num = index \">\n                    <p>Answer {{num+1}} : {{ans}}</p>\n                </div>\n            </div>\n            <table><tr>\n             <td><button class=\"btn btn-link\" type=\"button\" (click)=\"goClickLike(obj.syskey,num)\" ><span class=\"glyphicon glyphicon-thumbs-up\"></span></button><span *ngIf=\"obj.n2!=0\"><a (click)=\"goLike(obj.syskey)\">{{obj.n2}}</a></span></td>\n            <td><button class=\"btn btn-link\" type=\"button\" (click)=\"goAnswer(obj.syskey,num)\"><span class=\"glyphicon glyphicon-comment\"></span></button><span *ngIf=\"obj.n3!=0\">{{obj.n3}}</span></td> \n            </tr></table>\n            <hr>\n        </div> \n        \n    </div>\n    </div>\n    </div>\n\n   <!-- Modal -->\n    <div id=\"commentModal\" class=\"modal fade\" role=\"dialog\">\n    <div class=\"modal-dialog modal-lg\">  \n        <div class=\"modal-content\">\n        <div class=\"modal-header\">\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n            <h4 class=\"modal-title\">Comments</h4>\n        </div>\n        <div id=\"jbody\" class=\"modal-body\" >\n            <div style=\"overflow: hidden; position: relative;\">                \n                <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n                    <div *ngFor=\"let obj of _cmtArray;let num2 = index\" class=\"col-md-12\">\n                        <div *ngIf=\"obj.t12!='true'\">\n                            <label class=\"col-md-2\"><img style='height: 50px; width: 50px;'  src='image/farmerpng.png'  ></label>\n                                <div class=\"col-md-7\" >\n                                     <h4>{{obj.userName}}</h4>                                         \n                                     <p>{{obj.t2}}</p>\n                                </div> \n                                <br>\n                        </div>\n                        <div *ngIf=\"obj.t12=='true'\"> <!-- Edit -->\n                             <label class=\"col-md-2\"><img style='height: 50px; width: 50px;'  src='image/farmerpng.png'  ></label>\n                            <div class=\"col-md-7\">\n                                <textarea type=\"text\" [(ngModel)]=\"obj.t2\" class=\"form-control input-md\" rows=\"3\" ></textarea>\n                           </div>\n                           <br>\n                        </div>\n                        <button *ngIf=\"obj.t12=='true'\" class=\"btn btn-link\" type=\"button\" (click)=\"saveAnswer(obj)\">Save</button> \n                        <button *ngIf=\"obj.t12!='true' && obj.n5 == key\" class=\"btn btn-link\" type=\"button\" (click)=\"editAnswer(obj,num2)\">Edit</button>\n                        <button class=\"btn btn-link\" type=\"button\" (click)=\"replyAnswer(obj.syskey,num2)\">Reply</button>\n                        <button class=\"btn btn-link\" type=\"button\" (click)=\"deleteAnswer(obj,num2)\">Delete</button>\n                        <button *ngIf=\"obj.n2==0\" class=\"btn btn-link\" type=\"button\" (click)=\"convertToAns(obj,num2)\"><span class=\"glyphicon glyphicon-circle-arrow-right\"></span>Convert To Answer</button>\n                       \n                       \n                        <div *ngIf=\"obj.t13=='true'\">\n                            <div *ngFor=\"let replyObj of _replyArray\">                           \n                                <div class=\"form-group col-md-12\">\n                                     <label class=\"col-md-2\"></label>\n                                    <label class=\"col-md-2\"><img style='height: 50px; width: 50px;'  src='image/farmerpng.png'  ></label>\n                                    <div class=\"col-md-8\" >\n                                        <h4>{{replyObj.userName}}</h4>                         \n                                        <p>{{replyObj.t2}}</p>\n                                    </div>\n                                </div> \n                            </div>\n\n                         <div class=\"form-group col-md-12\">\n                              <label class=\"col-md-2\"></label>\n                            <div class=\"col-md-8\">                            \n                                <textarea type=\"text\" [(ngModel)]=\"_replyObj.t2\" class=\"form-control input-md\" rows=\"3\" ></textarea>\n                            </div>\n                            <div class=\"col-md-2\"> \n                                <button class=\"btn btn-danger\" type=\"button\" (click)=\"saveReply(obj.syskey)\">Reply</button>  \n                            </div>\n                        </div>\n                        </div>\n                        <hr>\n                    </div>\n                    <div class=\"form-group col-md-12\">\n                        <label class=\"col-md-2\"><img style='height: 50px; width: 50px;'  src='image/farmerpng.png'  ></label>\n                        <div class=\"col-md-8\">                             \n                            <textarea type=\"text\" [(ngModel)]=\"_ansObj.t2\" class=\"form-control input-md\" rows=\"3\" ></textarea>\n                        </div>\n                        <div class=\"col-md-2\"> \n                            <button class=\"btn btn-danger\" type=\"button\" (click)=\"saveAnswer(_ansObj)\">Save</button>  \n                        </div>\n                    </div> \n                </div>\n            </div>\n        </div>\n        </div>\n    </div>\n    </div>\n\n     <!-- Modal -->\n    <div id=\"likeModal\" class=\"modal fade\" role=\"dialog\">\n    <div class=\"modal-dialog modal-md\">  \n        <div class=\"modal-content\">\n        <div class=\"modal-header\">\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>           \n            <div class=\"form-group col-md-12\">\n                <label class=\"col-md-4\"></label>\n                    <div class=\"col-md-6\" >\n                        <h4 class=\"modal-title\">People who like this!</h4>\n                    </div> \n                    <div class=\"col-md-2\"></div>\n            </div>\n        </div>\n        <div id=\"jbody\" class=\"modal-body\" >\n            <div style=\"overflow: hidden; position: relative;\">                \n                <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n                    <div class=\"form-group col-md-12\">\n                        <div class=\"col-md-12\" *ngFor=\"let obj of _likearr\">\n                             <label class=\"col-md-12\">{{obj.t2}}</label>\n                        </div> \n                        <div class=\"col-md-0\"></div>\n                    </div>                 \n                   \n                </div>\n            </div>\n        </div>\n        </div>\n    </div>\n    </div>\n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmQnAViewComponent);
    return FrmQnAViewComponent;
}());
exports.FrmQnAViewComponent = FrmQnAViewComponent;
//# sourceMappingURL=frmQnAView.component.js.map