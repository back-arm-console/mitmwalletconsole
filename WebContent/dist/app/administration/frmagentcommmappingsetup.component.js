"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var Rx_1 = require('rxjs/Rx');
core_2.enableProdMode();
var FrmAgentCommMappingSetupComponent = (function () {
    function FrmAgentCommMappingSetupComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
        this._obj = this.getDefaultObj();
        this._sessionMsg = "";
        this._key = "";
        this._mflag = false;
        this._chkloginId = 'false';
        this.sessionAlertMsg = "";
        this._util = new rp_client_util_1.ClientUtil();
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = false;
            this.checkSession();
            this.getAllCommRef();
            this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
            this._obj = this.getDefaultObj();
            this.getTransferType();
        }
    }
    FrmAgentCommMappingSetupComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this._key = id;
                _this.goReadBySyskey(id);
            }
        });
    };
    FrmAgentCommMappingSetupComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    FrmAgentCommMappingSetupComponent.prototype.getDefaultObj = function () {
        jQuery("#mydelete").prop("disabled", true);
        jQuery("#mySave").prop("disabled", false);
        return { "processingCode": "", "type": "", "syskey": 0, "select": "", "recordStatus": 0, "agentType": "", "t1": "", "t2": "", "t3": "", "commRef1": "", "commRef2": "", "commRef3": "", "commRef4": "", "commRef5": "", "bankCharges": 0.0, "drawingCharges": 0.0, "encashCharges": 0.0, "code": "", "desc": "", "userID": "", "sessionID": "", "n1": 0, "n2": 0, "n3": 0 };
    };
    FrmAgentCommMappingSetupComponent.prototype.goReadBySyskey = function (p) {
        var _this = this;
        try {
            this._mflag = false;
            var url = this.ics._apiurl + 'service001/getAgentCommData?agentType=' + p + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
            this.http.doGet(url).subscribe(function (data) {
                if (data.code == '0016') {
                    _this.sessionAlertMsg = data.desc;
                    _this.showMessage();
                }
                else {
                    jQuery("#mydelete").prop("disabled", false);
                    _this._obj = data;
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmAgentCommMappingSetupComponent.prototype.goList = function () {
        this._router.navigate(['/agentcommmappinglist']);
    };
    FrmAgentCommMappingSetupComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this.goLogOut();
        });
    };
    FrmAgentCommMappingSetupComponent.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    /* checkSession() {
        try {
            let url: string = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
            this.http.doGet(url).subscribe(
                data => {
                    if (data.msgCode == '0016') {
                        this.sessionAlertMsg = data.msgDesc;
                        this.showMessage();
                    }
                },
                error => {
                    if (error._body.type == 'error') {
                        alert("Connection Timed Out!");
                    }
                    else {

                    }
                }, () => { });
        } catch (e) {
            alert("Invalid URL");
        }

    } */
    FrmAgentCommMappingSetupComponent.prototype.showMessageAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmAgentCommMappingSetupComponent.prototype.clearData = function () {
        this._obj = this.getDefaultObj();
        this._obj.agentType = this.tmp;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
        this._chkloginId = 'false';
        jQuery("#mydelete").prop("disabled", true);
        jQuery("#mySave").prop("disabled", true);
    };
    FrmAgentCommMappingSetupComponent.prototype.goNew = function () {
        this.clearData();
        jQuery("#mySave").prop("disabled", false);
        jQuery("#mydelete").prop("disabled", true);
    };
    FrmAgentCommMappingSetupComponent.prototype.changeType = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex; //Get Selected Index
        var value = options[options.selectedIndex].value; //Get Selected Index's Value
        this._obj.agentType = value;
        for (var i = 1; i < this.ref._lov3.refTransferType.length; i++) {
            if (this.ref._lov3.refTransferType[i].value == value) {
                this._obj.agentType = this.ref._lov3.refTransferType[i].value;
                break;
            }
        }
    };
    FrmAgentCommMappingSetupComponent.prototype.getTransferType = function () {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'service001/getTransferType').subscribe(function (data) {
            _this.ref._lov3.refTransferType = data.refTransferType;
            var arr = [];
            if (_this.ref._lov3.refTransferType != null) {
                if (!(_this.ref._lov3.refTransferType instanceof Array)) {
                    var m = [];
                    m[0] = _this.ref._lov3.refTransferType;
                    _this.ref._lov3.refTransferType = m;
                }
                else {
                    for (var j = 0; j < _this.ref._lov3.refTransferType.length; j++) {
                        arr.push(_this.ref._lov3.refTransferType[j]);
                    }
                }
            }
            _this._obj.agentType = _this.ref._lov3.refTransferType[0].value;
            _this.tmp = _this.ref._lov3.refTransferType[0].value;
            _this._mflag = true;
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
        }, function () { });
    };
    FrmAgentCommMappingSetupComponent.prototype.getAllCommRef = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics._apiurl + 'service001/getAllCommRef').subscribe(function (data) {
                if (data != null && data != undefined) {
                    var Chargescombo = [{ "value": "-", "caption": "-" }];
                    for (var i = 0; i < data.refcharges.length; i++) {
                        Chargescombo.push({ "value": data.refcharges[i].value, "caption": data.refcharges[i].caption });
                    }
                    _this.ref._lov3.refcharges = Chargescombo;
                }
                else {
                    _this.ref._lov3.refcharges = [{ "value": "", "caption": "-" }];
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmAgentCommMappingSetupComponent.prototype.goSave = function () {
        this._mflag = false;
        if (this._obj.agentType == '') {
            this.showMessageAlert("Please select Agent Type");
            this._mflag = true;
        }
        else if (this._obj.commRef1 == '' && this._obj.commRef2 == '' && this._obj.commRef3 == '' && this._obj.commRef4 == '' && this._obj.commRef5 == '') {
            this.showMessageAlert("Please select Charges");
            this._mflag = true;
        }
        else if (this._obj.bankCharges == 0) {
            this.showMessageAlert("Please Fill Bank Charges");
            this._mflag = true;
        }
        else {
            this.saveAgentCommMapping();
        }
    };
    FrmAgentCommMappingSetupComponent.prototype.saveAgentCommMapping = function () {
        var _this = this;
        try {
            this._mflag = false;
            this._obj.sessionID = this.ics._profile.sessionID;
            this._obj.userID = this.ics._profile.userID;
            var url = this.ics._apiurl + 'service001/saveACommRateMapping';
            var json = this._obj;
            this.http.doPost(url, json).subscribe(function (data) {
                _this._returnResult = data;
                if (_this._returnResult.state == 'true') {
                    jQuery("#mydelete").prop("disabled", false);
                    _this.showMessageAlert(data.msgDesc);
                }
                else {
                    if (_this._returnResult.msgCode == '0016') {
                        _this.sessionAlertMsg = data.msgDesc;
                        _this.showMessage();
                    }
                    else {
                        _this.showMessageAlert(data.msgDesc);
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmAgentCommMappingSetupComponent.prototype.goDelete = function () {
        var _this = this;
        try {
            this._mflag = false;
            this._obj.sessionID = this.ics._profile.sessionID;
            this._obj.userID = this.ics._profile.userID;
            var json = this._obj;
            var url = this.ics._apiurl + 'service001/deleteAgentCommRateMapping';
            this.http.doPost(url, json).subscribe(function (data) {
                _this._returnResult = data;
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMessage();
                }
                else {
                    _this.showMessageAlert(data.msgDesc);
                    if (data.state) {
                        _this.clearData();
                    }
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
            this._mflag = true;
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmAgentCommMappingSetupComponent.prototype.changeSelectVal = function (options) {
        this.selectedOptions = Array.apply(null, options) // convert to real array
            .filter(function (option) { return option.selected; })
            .map(function (option) { return option.value; });
        this._obj.select = this.selectedOptions[0];
    };
    FrmAgentCommMappingSetupComponent.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    FrmAgentCommMappingSetupComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsgAlert(data.desc);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsgAlert(data.desc);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmAgentCommMappingSetupComponent.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmAgentCommMappingSetupComponent.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    FrmAgentCommMappingSetupComponent = __decorate([
        core_1.Component({
            selector: 'frmagentcommapping-setup',
            template: "\n    <div class=\"container\">\n    <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n    <form class= \"form-horizontal\" (ngSubmit) = \"goSave()\"> \n    <!-- Form Name -->\n    <legend>Agent Comm Mapping </legend>\n    <div class=\"row  col-md-12\">  \n      <button class=\"btn btn-primary\" type=\"button\" (click)=\"goList()\" >List</button> \n      <button class=\"btn btn-primary\" type=\"button\" (click)=\"goNew()\" >New</button>      \n      <button class=\"btn btn-primary\" id=\"mySave\" type=\"submit\" >Save</button>          \n      <button class=\"btn btn-primary\" disabled id=\"mydelete\"  type=\"button\" (click)=\"goDelete();\" >Delete</button> \n    </div>\n    <div class=\"row col-md-12\">&nbsp;</div> \n\n    <div class=\"form-group\">    \n    <div class=\"col-md-10\">\n\n    <div class=\"form-group\">\n      <label class=\"col-md-3\">Agent Type <font class=\"mandatoryfont\">*</font></label>\n      <div class=\"col-md-4\" > \n        <select [(ngModel)]=_obj.agentType [ngModelOptions]=\"{standalone: true}\" required=\"true\" (change)=\"changeSelectVal($event.target.options)\"  class=\"form-control\"  >\n            <option *ngFor=\"let item of ref._lov3.refTransferType\" value=\"{{item.value}}\">{{item.caption}}</option> \n        </select>  \n      </div>   \n    </div>\n \n    <div class=\"form-group\">\n      <label class=\"col-md-3\" > Charges 1 <font class=\"mandatoryfont\">*</font></label>\n      <div class=\"col-md-4\">\n        <select [ngModelOptions]=\"{standalone: true}\" required=\"true\"  [(ngModel)]=_obj.commRef1 class=\"form-control col-md-0\">\n          <option *ngFor=\"let item of ref._lov3.refcharges\" value=\"{{item.value}}\" >{{item.caption}}</option> \n        </select>                \n      </div> \n    </div>\n\n\n    <div class=\"form-group\">\n        <label class=\"col-md-3\" > Bank Charges <font class=\"mandatoryfont\">*</font></label>\n        <div class=\"col-md-4\">\n            <input id=\"numinput\" class=\"form-control\" type = \"number\" [(ngModel)]=\"_obj.bankCharges\" [ngModelOptions]=\"{standalone: true}\" required>                              \n        </div> \n         <!-- <rp-input id=\"numinput\" [(rpModel)]=\"_obj.bankCharges\" rpLabelRequired='true' rpType=\"number\"  rpLabel=\" Bank Charges \"  rpClass=\"col-md-4 control-label\"  [ngModelOptions]=\"{standalone: true}\" rpRequired=\"true\"></rp-input>-->\n    </div>\n\n    <div class=\"form-group\">\n        <label class=\"col-md-3\" > Drawing Charges</label>\n        <div class=\"col-md-4\">\n            <input id=\"numinput\" class=\"form-control\" type = \"number\" [(ngModel)]=\"_obj.drawingCharges\" [ngModelOptions]=\"{standalone: true}\" required>                              \n        </div> \n    </div>\n\n    <div class=\"form-group\">\n        <label class=\"col-md-3\" > Encash Charges</label>\n        <div class=\"col-md-4\">\n            <input id=\"numinput\" class=\"form-control\" type = \"number\" [(ngModel)]=\"_obj.encashCharges\" [ngModelOptions]=\"{standalone: true}\" required>                              \n        </div> \n    </div>\n\n    </div>\n    </div>\n\n    </form>\n    </div>\n    </div>\n    </div>\n       \n    <div id=\"sessionalert\" class=\"modal fade\">\n      <div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n          <div class=\"modal-body\">\n            <p>{{sessionAlertMsg}}</p>\n          </div>\n          <div class=\"modal-footer\">\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <div [hidden]=\"_mflag\">\n        <div class=\"modal\" id=\"loader\"></div>\n    </div>\n    \n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmAgentCommMappingSetupComponent);
    return FrmAgentCommMappingSetupComponent;
}());
exports.FrmAgentCommMappingSetupComponent = FrmAgentCommMappingSetupComponent;
//# sourceMappingURL=frmagentcommmappingsetup.component.js.map