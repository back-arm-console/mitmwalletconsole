"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var Observable_1 = require('rxjs/Observable');
var core_2 = require('@angular/core');
// RP Framework
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_client_util_1 = require('../util/rp-client.util');
core_2.enableProdMode();
var FrmEncashmentListComponent = (function () {
    function FrmEncashmentListComponent(ics, _router, http) {
        this.ics = ics;
        this._router = _router;
        this.http = http;
        // Application Specific
        this._searchVal = ""; // simple search
        this._flagas = true; // flag advance search
        this._col_list = [{ "col_no": "1", "flag": false }, { "col_no": "2", "flag": false }];
        this._sorting = { _sort_type: "asc", _sort_col: "1" };
        this._mflag = true;
        this._util = new rp_client_util_1.ClientUtil();
        this.sessionAlertMsg = "";
        this._encashobj = {
            "data": [{
                    "syskey": 0, "userID": "", "sessionID": "", "fromAccount": "", "fromName": "",
                    "toAccount": "", "toName": "", "amount": "", "bankCharges": 0, "narrative": "",
                    "refNo": "", "draweeName": "", "draweeNRC": "", "draweePhone": "",
                    "payeeName": "", "payeeNRC": "", "payeePhone": "", "reqDate": "", "remittedDate": "", "field1": "", "field2": ""
                }], "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0, "msgCode": "", "msgDesc": ""
        };
        this._pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 0 };
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            jQuery("#loader").modal('hide');
            this.search();
        }
    }
    // list sorting part
    FrmEncashmentListComponent.prototype.changeDefault = function () {
        for (var i = 0; i < this._col_list.length; i++) {
            this._col_list[i].flag = false;
        }
    };
    FrmEncashmentListComponent.prototype.addSort = function (e) {
        for (var i = 0; i < this._col_list.length; i++) {
            if (this._col_list[i].col_no == e) {
                var _tmp_flag = this._col_list[i].flag;
                this.changeDefault();
                this._col_list[i].flag = !_tmp_flag;
                this._sorting._sort_type = !_tmp_flag ? "desc" : "asc";
                this._sorting._sort_col = e;
                break;
            }
        }
    };
    FrmEncashmentListComponent.prototype.changedPager = function (event) {
        if (this._encashobj.totalCount != 0) {
            this._pgobj = event;
            var current = this._encashobj.currentPage;
            var size = this._encashobj.pageSize;
            this._encashobj.currentPage = this._pgobj.current;
            this._encashobj.pageSize = this._pgobj.size;
            if (this._pgobj.current != current || this._pgobj.size != size) {
                this.search();
            }
        }
    };
    FrmEncashmentListComponent.prototype.search = function () {
        var _this = this;
        try {
            this._mflag = false;
            this.http.doGet(this.ics._apiurl + 'service001/getAgentDrawingDataList?searchVal=' + encodeURIComponent(this._encashobj.searchText) + '&pagesize=' + this._encashobj.pageSize + '&currentpage=' + this._encashobj.currentPage + '&operation=activate&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMessage();
                }
                else {
                    if (data != null && data != undefined) {
                        _this._pgobj.totalcount = data.totalCount;
                        _this._encashobj = data;
                        if (_this._encashobj.totalCount == 0) {
                            _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Data not found!" });
                        }
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmEncashmentListComponent.prototype.searchKeyup = function (e) {
        if (e.which == 13) {
            this._encashobj.currentPage = 1;
            this._msghide = true;
            this._recordhide = false;
            this.search();
        }
    };
    FrmEncashmentListComponent.prototype.Searching = function () {
        this._encashobj.currentPage = 1;
        this._msghide = true;
        this._recordhide = false;
        this.search();
    };
    FrmEncashmentListComponent.prototype.goto = function (p) {
        this._router.navigate(['/Encashment', 'read', p]);
    };
    FrmEncashmentListComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Observable_1.Observable.timer(3000).subscribe(function (x) {
            _this.goLogOut();
        });
    };
    FrmEncashmentListComponent.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    FrmEncashmentListComponent.prototype.goNew = function () {
        this._router.navigate(['/Encashment', 'new']);
    };
    FrmEncashmentListComponent = __decorate([
        core_1.Component({
            selector: 'frmencashmentlist',
            template: " \n  <div class=\"container col-md-12\">\n    <form class=\"form-inline\"> \n    \n    <!-- Form Name -->\n    <legend>Encashment List</legend>\n      <div *ngIf=\"_flagas\" class=\"input-group\">\n      <span class=\"input-group-btn input-md\" >\n      <button class=\"btn btn-primary\" type=\"button\" (click)=\"goNew();\">New</button>\n  </span>\n        <input id=\"textinput\" name=\"textinput\" type=\"text\" placeholder=\"Search\" [(ngModel)]=\" _encashobj.searchText\" (keyup)=\"searchKeyup($event)\" maxlength=\"50\" class=\"form-control input-md\">\n        <span class=\"input-group-btn input-md\">\n\t        <button class=\"btn btn-primary input-md\" type=\"button\" (click)=\"Searching()\" >\n\t          <span class=\"glyphicon glyphicon-search\"></span>Search\n\t        </button>\n\t      </span>        \n\t    </div>      \n    </form>\n    \n   <div style = \"margin-top : 10px\">\n   <adminpager rpPageSizeMax=\"100\" [(rpModel)]=\" _encashobj.totalCount\" (rpChanged)=\"changedPager($event)\" ></adminpager> \n  </div> \n  <table class=\"table table-striped table-condensed table-hover tblborder\" style=\"font-size:14px;\">\n    <thead>\n     <tr>\n        <th>Ref No.</th>\n        <th>Date</th> \n        <th>From Account </th>\n        <th>From Name </th>\n        <th>To Account </th>\n        <th>To Name </th>    \n    </tr>\n  </thead>\n  <tbody>\n    <tr *ngFor=\"let obj of  _encashobj.data\">\n        <td><a (click)=\"goto(obj.refNo)\">{{obj.refNo}}</a></td>\n        <td>{{obj.remittedDate}}</td>\n        <td>{{obj.fromAccount}}</td>\n        <td>{{obj.fromName}}</td>\n        <td>{{obj.toAccount}}</td>\n        <td>{{obj.toName}}</td>\n    </tr>  \n  </tbody>\n  </table>\n  </div> \n  \n    <div [hidden] = \"_mflag\">\n        <div class=\"loader modal\"id=\"loader\"></div>\n    </div>\n   "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_http_service_1.RpHttpService])
    ], FrmEncashmentListComponent);
    return FrmEncashmentListComponent;
}());
exports.FrmEncashmentListComponent = FrmEncashmentListComponent;
//# sourceMappingURL=frmencashmentlist.component.js.map