"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var Rx_1 = require('rxjs/Rx');
core_2.enableProdMode();
var FrmForceSignOutComponent = (function () {
    function FrmForceSignOutComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
        this._obj = this.getDefaultObj();
        this.sessionAlertMsg = "";
        this.isvalidate = "";
        this._mflag = true;
        this._util = new rp_client_util_1.ClientUtil();
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this.checkSession();
            this._obj = this.getDefaultObj();
        }
    }
    FrmForceSignOutComponent.prototype.getDefaultObj = function () {
        this.msg = "";
        return { "name": "", "t1": "", "loginstatus": 0 };
    };
    FrmForceSignOutComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this.goLogOut();
        });
    };
    FrmForceSignOutComponent.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    /* checkSession() {
      try {
        let url: string = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
        this.http.doGet(url).subscribe(
          data => {
            if (data.msgCode == '0016') {
              this.sessionAlertMsg = data.msgDesc;
              this.showMessage();
            }
          },
          error => {
            if (error._body.type == 'error') {
              alert("Connection Timed Out!");
            }
          }, () => { });
      } catch (e) {
        alert("Invalid URL");
      }
    } */
    FrmForceSignOutComponent.prototype.goClear = function () {
        this.isvalidate = "";
        this._obj = this.getDefaultObj();
    };
    FrmForceSignOutComponent.prototype.showMessageAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmForceSignOutComponent.prototype.goCheck = function () {
        var _this = this;
        try {
            this._mflag = false;
            this.http.doGet(this.ics._apiurl + 'service001/getUserNameAndStatus?id=' + this._obj.t1 + '&sessionID=' + this.ics._profile.sessionID + '&userIDForSession=' + this.ics._profile.userID).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMessage();
                }
                else {
                    if (data.t1 == '') {
                        _this.isvalidate = "";
                        jQuery("#mylogout").prop("disabled", true);
                        _this.showMessageAlert("Invalid User ID");
                    }
                    else {
                        _this._obj = data;
                        _this.isvalidate = "Validate";
                        if (_this._obj.loginstatus == 1) {
                            _this.msg = "Active Session";
                            if (data.t1 == _this.ics._profile.userID) {
                                jQuery("#mylogout").prop("disabled", true);
                                _this.showMessageAlert("Please Sign Out");
                            }
                            else {
                                jQuery("#mylogout").prop("disabled", false);
                            }
                        }
                        else {
                            _this.msg = "Invalid Session";
                        }
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmForceSignOutComponent.prototype.confirmLogout = function () {
        jQuery("#logoutconfirm").modal();
    };
    FrmForceSignOutComponent.prototype.goForcedLogout = function () {
        var _this = this;
        jQuery("#logoutconfirm").modal('hide');
        try {
            this._mflag = false;
            this.http.doGet(this.ics._apiurl + 'service001/forcedlogoutbyId?userId=' + this._obj.t1 + '&sessionID=' + this.ics._profile.sessionID + '&userIDForSession=' + this.ics._profile.userID).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMessage();
                }
                else {
                    /* if (data.state == 'true') {
                      this.msg = "Invalid Session";
                    }
                    jQuery("#mylogout").prop("disabled", true);
                    this.showMessageAlert(data.msgDesc);
                  } */
                    _this._mflag = true;
                    _this._returnResult = data;
                    if (_this._returnResult.state) {
                        _this.msg = "Invalid Session";
                        _this.msghide = false;
                        jQuery("#mylogout").prop("disabled", true);
                    }
                    _this.showMessageAlert(data.msgDesc);
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmForceSignOutComponent.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    FrmForceSignOutComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsgAlert(data.desc);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsgAlert(data.desc);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmForceSignOutComponent.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmForceSignOutComponent.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    FrmForceSignOutComponent = __decorate([
        core_1.Component({
            selector: 'sign-out',
            template: "\n    <div class=\"container\">\n    <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n    <form class= \"form-horizontal\"> \n    <!-- Form Name -->\n    <legend>Forced Sign Out</legend>\n\n    <div class=\"row  col-md-12\">   \n      <button class=\"btn btn-primary\" disabled id=\"mylogout\"  type=\"button\" (click)=\"confirmLogout()\" >Sign Out</button>\n      <button class=\"btn btn-primary\" type=\"button\" id=\"myclear\" (click)=\"goClear()\" >Clear</button>\n    </div> \n    <div class=\"row col-md-12\">&nbsp;</div>\n    <div class=\"form-group\">\n    <div class=\"col-md-9\">   \n      <div class=\"form-group\">\n        <label class=\"col-md-3\"> Login ID <font class=\"mandatoryfont\" >*</font> </label>\n        <div class=\"col-md-4\">\n              <div class=\"input-group\">\n                <input type=\"text\" [(ngModel)]=_obj.t1 [ngModelOptions]=\"{standalone: true}\" required=\"true\"  class=\"form-control col-md-0\">\n                <span class=\"input-group-btn\">\n                  <button class=\"btn btn-primary\" type=\"button\" (click)=\"goCheck()\"><i class=\"glyphicon glyphicon-ok-sign\"></i> Check </button>\n                </span>\n              </div>\n        </div>\n        <div class=\"col-md-2\">\n          <label style=\"color:green;font-size:20px\">&nbsp;{{isvalidate}}</label>   \n        </div>\n      </div>  \n    \n      <div class=\"form-group\">\n        <rp-input  rpType=\"text\" rpLabel=\"Name\" [(rpModel)]=\"_obj.name\"  rpReadonly=\"true\"></rp-input>    \n      </div>     \n    </div>   \n    \n    <div class=\"col-md-3\">\n      <div [hidden]=\"msghide\">\n        <h3 align=\"right\" style=\"font-size:25px\"><b>{{msg}}</b></h3>\n      </div>\n  </div>\n    \n    </div>\n\n    </form>\n    </div>\n    </div>\n    </div>\n\n    <div id=\"logoutconfirm\" class=\"modal fade\">\n    <div class=\"modal-dialog\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n          <h4 class=\"modal-title\">Confirmation</h4>\n        </div>\n        <div class=\"modal-body\">\n          <p>Do you want to end this user session?</p>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-primary\" (click)=\"goForcedLogout()\">Yes</button>\n          <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\">Cancel</button>\n        </div>\n      </div>\n    </div>\n  </div>\n\n    <div id=\"sessionalert\" class=\"modal fade\">\n      <div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n          <div class=\"modal-body\">\n            <p>{{sessionAlertMsg}}</p>\n          </div>\n          <div class=\"modal-footer\">\n          </div>\n        </div>\n      </div>\n    </div>\n\n<div [hidden] = \"_mflag\">\n  <div class=\"modal\" id=\"loader\"></div>\n</div>\n    \n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmForceSignOutComponent);
    return FrmForceSignOutComponent;
}());
exports.FrmForceSignOutComponent = FrmForceSignOutComponent;
//# sourceMappingURL=frmforcesignout.component.js.map