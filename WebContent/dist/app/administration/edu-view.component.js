"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var EduViewComponent = (function () {
    function EduViewComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._alertflag = true;
        this._alertmsg = "";
        this.t2 = "";
        this._totalcount = 1;
        this._searchVal = "";
        this._array = [];
        this._likearr = [];
        this._pager = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 0, "size": 0, "totalcount": 1 };
        this._mflag = false;
        this.count = 0;
        this.key = 0;
        this.syskey = 0;
        this._time = "";
        this.savecommentflag = false;
        this.replycommentflag = false;
        this.deletereplycomment = false;
        this._cmtArray = [];
        this._replyArray = [];
        this._obj = [];
        this._util = new rp_client_util_1.ClientUtil();
        this._tmpObj = { '_key': 0, '_index': 0 };
        this._ansObj = { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": "", "modifiedDate": "", "modifiedTime": "", "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [] };
        this._replyObj = { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": "", "modifiedDate": "", "modifiedTime": "", "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [] };
        this._viewsk = 0;
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        this._time = this._util.getTodayTime();
        this._ansObj.createdTime = this._time;
        this._ansObj.modifiedTime = this._time;
        this._replyObj.createdTime = this._time;
        this._replyObj.modifiedTime = this._time;
        this.syskey = Number(this.ics._profile.t1);
        this.profileImageLink = this.ics._profileImage1;
    }
    EduViewComponent.prototype.closeCommentModel = function () {
        this._router.navigate(['/eduView', 'read']);
    };
    EduViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "new") {
                _this.goNew();
            }
            else if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this._viewsk = id;
                _this.goGet(id);
            }
        });
    };
    EduViewComponent.prototype.goGet = function (p) {
        var _this = this;
        this.http.doGet(this.ics.cmsurl + 'serviceEduAdm/viewByID?id=' + p + '&key=' + this.ics._profile.t1).subscribe(function (response) {
            if (response.state) {
                console.log("View data=" + response.data[0].t2);
                _this._obj = response.data;
                console.log("View data=" + _this._obj);
                _this.key = response.data[0].n7;
            }
            _this._mflag = true;
        }, function (error) { }, function () { });
    };
    /* search(p) {
        this._mflag = false;
        if (p.end == 0) { p.end = this.ics._profile.n1; }
        if (p.size == 0) { p.size = this.ics._profile.n1; }
        let url: string = this.ics.cmsurl + 'serviceEduAdm/searchEdu?searchVal=' + this._searchVal;
        let json: any = p;
        this.http.doPost(url, json).subscribe(
            response => {
                if (response != null && response != undefined && response.state) {
                    this._totalcount = response.totalCount;
                    this._array = response.data;
                }
                else {
                    this._array = [];
                    this._totalcount = 1;
                    this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": "Data not found!" });
                }
                this._mflag = true;
            },
            error => { },
            () => { }
        );
    } */
    /* searchVal() {
        this._pager = { "current": 1, "prev": 1, "last": 1, "next": 1, "start": 1, "end": 0, "size": 0, "totalcount": 1 };
        this.search(this._pager);
    } */
    EduViewComponent.prototype.goto = function (p) {
        this._router.navigate(['/contentmenu', 'read', p]);
    };
    EduViewComponent.prototype.goback = function (p) {
        this._router.navigate(['/contentmenu', 'read', p]);
    };
    EduViewComponent.prototype.goNew = function () {
        this._router.navigate(['/contentmenu', 'new']);
    };
    EduViewComponent.prototype.goLike = function (key, type) {
        var _this = this;
        jQuery("#likeModal").modal();
        this.http.doGet(this.ics.cmsurl + 'serviceEduAdm/searchLike?type=' + type + '&key=' + key).subscribe(function (response) {
            // if (response.state) {
            //     this._likearr = response.data;
            //     this._mflag = true;
            // }
            if (response.state && response.data.length > 0) {
                _this._likearr = [];
                for (var i = 0; i < response.data.length; i++) {
                    _this._likearr.push({
                        name: response.data[i].userName,
                        img: response.data[i].photo
                    });
                }
                _this._mflag = true;
            }
            else {
                _this._likearr = [];
            }
        }, function (error) { }, function () { });
    };
    /* changedPager(event) {
        let k = event.flag;
        this._pager = event.obj;
        if (k) { this.search(this._pager); }
    } */
    EduViewComponent.prototype.goClickLike = function (key, index) {
        var _this = this;
        this.http.doGet(this.ics.cmsurl + 'serviceEduAdm/clickLikeEdu?key=' + key + '&userSK=' + this.key).subscribe(function (data) {
            if (data.state) {
                _this.goLikeCount(key, index);
            }
            _this._mflag = true;
        }, function (error) { }, function () { });
    };
    EduViewComponent.prototype.ClickLike = function (key, index) {
        if (this.count == 0 && key == this._obj[index].syskey) {
            this._obj[index].n2 = this._obj[index].n2 + 1;
            this.count = this.count + 1;
            this.goClickLike(key, index);
        }
        else if (this.count == 1 && key == this._obj[index].syskey) {
            this._obj[index].n2 = this._obj[index].n2 - 1;
            this.count = 0;
            this.goClickLike(key, index);
        }
    };
    EduViewComponent.prototype.goClickUnlike = function (key, index) {
        var _this = this;
        this.http.doGet(this.ics.cmsurl + 'serviceArticleAdm/clickUnlikeArticle?key=' + key).subscribe(function (data) {
            if (data.state)
                console.log("Unlike...");
            _this._mflag = true;
        }, function (error) { }, function () { });
    };
    EduViewComponent.prototype.goLikeCount = function (key, index) {
        var _this = this;
        this.http.doGet(this.ics.cmsurl + 'serviceEduAdm/searchLikeCount?key=' + key).subscribe(function (response) {
            if (response.state)
                _this._obj = response.data;
            _this._mflag = true;
        }, function (error) { }, function () { });
    };
    EduViewComponent.prototype.goComment = function (key, index) {
        var _this = this;
        console.log(JSON.stringify(key) + " edu get ");
        jQuery("#commentModal").modal();
        this._tmpObj._key = key;
        this._tmpObj._index = index;
        this._cmtArray = [];
        //this.http.doGet(this.ics.cmsurl + 'serviceEduAdm/getComments?id=' + key).subscribe(
        this.http.doGet(this.ics.cmsurl + 'serviceQuestion/getCommentmobile?id=' + key + '&userSK=' + Number(this.ics._profile.t1)).subscribe(function (response) {
            if (response.state)
                _this._cmtArray = response.data;
            _this._mflag = true;
        }, function (error) { }, function () { });
    };
    EduViewComponent.prototype.saveComment = function (cmtObj) {
        var _this = this;
        this.savecommentflag = true;
        var valid = false;
        if (cmtObj.t2 != undefined && cmtObj.t2 != null && cmtObj.t2 != '') {
            cmtObj.t2 = cmtObj.t2.trim();
            if (cmtObj.t2.length > 0) {
                valid = true;
            }
        }
        this._mflag = false;
        cmtObj.n1 = this._tmpObj._key;
        cmtObj.n5 = Number(this.ics._profile.t1);
        // cmtObj.t12 = '';
        //cmtObj.t13 = '';
        //cmtObj.t3 = 'news';
        cmtObj.t1 = 'answer';
        console.log("UI data : " + JSON.stringify(cmtObj));
        if (valid) {
            //let url: string = this.ics.cmsurl + 'serviceEduAdm/saveComment';
            var url = this.ics.cmsurl + 'serviceQuestion/saveAnswer';
            var json = cmtObj;
            this.http.doPost(url, json).subscribe(function (response) {
                _this._cmtArray = [];
                if (response.state)
                    _this._cmtArray = response.data;
                _this._ansObj.t2 = "";
                _this._mflag = true;
                _this.savecommentflag = false;
                _this._obj[0].n3 = _this._cmtArray[0].n4;
                console.log(JSON.stringify(_this._cmtArray));
            }, function (error) {
                _this.showMsgAlert("Can't Saved This Record!", undefined);
                _this._mflag = true;
                _this.savecommentflag = false;
            }, function () { });
        }
        else {
            this.showMsgAlert("Please write message.", false);
            this._mflag = true;
            this.savecommentflag = false;
        }
    };
    EduViewComponent.prototype.showMsgAlert = function (msg, bool) {
        var type = "";
        if (bool == true) {
            type = "success";
        }
        if (bool == false) {
            type = "warning";
        }
        if (bool == undefined) {
            type = "danger";
        }
        this._alertmsg = msg;
        this._alertflag = false;
        var _snack_style = 'msg-info';
        if (type == "success")
            _snack_style = 'msg-success';
        else if (type == "warning")
            _snack_style = 'msg-warning';
        else if (type == "danger")
            _snack_style = 'msg-danger';
        else if (type == "information")
            _snack_style = 'msg-info';
        document.getElementById("snackbar1").innerHTML = this._alertmsg;
        var snackbar1 = document.getElementById("snackbar1");
        snackbar1.className = "show " + _snack_style;
        setTimeout(function () { snackbar1.className = snackbar1.className.replace("show", ""); }, 3000);
    };
    /* replyComment(key, num) {
        this._tmpObj._index = num;
        for (var i = 0; i < this._cmtArray.length; i++) {
            if (num == i) {
                this._cmtArray[i].t13 = 'true';
            }
            else this._cmtArray[i].t13 = 'false';
        }
        //this._cmtArray[num].t13 = 'true';
        this._replyArray = [];
        //this.http.doGet(this.ics.cmsurl + 'serviceEduAdm/getComments?id=' + key).subscribe(
        this.http.doGet(this.ics.cmsurl + 'serviceQuestion/getCommentReplymobile?id=' + key + '&userSK=' + Number(this.ics._profile.t1)).subscribe(

            response => {
                if (response.state) this._replyArray = response.data;
                this._mflag = true;
            },
            error => { },
            () => { }
        );
    } */
    /* editReply(replyObj) {
        let valid = false;
        if (replyObj.t2 != undefined && replyObj.t2 != null && replyObj.t2 != '') {
            replyObj.t2 = replyObj.t2.trim();

            if (replyObj.t2.length > 0) {
                valid = true;
            }
        }
        this._mflag = false;
        //this._replyObj.n1 = replyObj.syskey;
        //this._replyObj.n3 = -1;
        replyObj.n5 = Number(this.ics._profile.t1);
        replyObj.t1 = 'reply';

        if (valid) {
            //let url: string = this.ics.cmsurl + 'serviceEduAdm/saveComment';
            let url: string = this.ics.cmsurl + 'serviceQuestion/saveCommentReply';
            let json: any = replyObj;
            this.http.doPost(url, json).subscribe(
                response => {
                    this._replyArray = [];
                    if (response.state) this._replyArray = response.data;
                    //this._cmtArray[this._tmpObj._index].n3 = this._cmtArray[this._tmpObj._index].n3 + 1;
                    this._replyObj.t2 = "";
                    //this._replyArray[0].t2 = "";
                    this._mflag = true;
                },
                error => {
                    this.showMsgAlert("Can't Saved This Record!", undefined);
                },
                () => { }
            );
        } else {
            this.showMsgAlert("Please write message.", false);
            this._mflag = true;
        }
    } */
    /* saveReply(skey, index) {
        this.replycommentflag = true;
        let valid = false;
        if (this._replyObj.t2 != undefined && this._replyObj.t2 != null && this._replyObj.t2 != '') {
            this._replyObj.t2 = this._replyObj.t2.trim();

            if (this._replyObj.t2.length > 0) {
                valid = true;
            }
        }
        this._mflag = false;
        this._replyObj.n1 = skey;
        //this._replyObj.n3 = -1;
        this._replyObj.n5 = Number(this.ics._profile.t1);
        this._replyObj.t1 = 'reply';

        if (valid) {
            // let url: string = this.ics.cmsurl + 'serviceEduAdm/saveComment';
            let url: string = this.ics.cmsurl + 'serviceQuestion/saveCommentReply';

            let json: any = this._replyObj;
            this.http.doPost(url, json).subscribe(
                response => {
                    this._replyArray = [];
                    if (response.state) this._replyArray = response.data;
                    this._cmtArray[this._tmpObj._index].n3 = this._cmtArray[this._tmpObj._index].n3 + 1;
                    this._replyObj.t2 = "";
                    this._mflag = true;
                    this.replycommentflag = false;
                    this._cmtArray[index].n11 += 1;
                },
                error => {
                    this.showMsgAlert("Can't Saved This Record!", undefined);
                    this.replycommentflag = false;
                },
                () => { }
            );
        } else {
            this.showMsgAlert("Please write message.", false);
            this._mflag = true;
            this.replycommentflag = false;
        }
    } */
    EduViewComponent.prototype.editComment = function (obj, num) {
        this._cmtArray[num].t12 = 'true';
    };
    /* editReplyComment(obj, num) {
        this._replyArray[num].t12 = 'true';
    } */
    EduViewComponent.prototype.deleteComment = function (obj, num) {
        var _this = this;
        this._mflag = false;
        // let url: string = this.ics.cmsurl + 'serviceEduAdm/deleteComment';
        var json = obj;
        //this.http.doPost(url, json).subscribe(
        this.http.doPost(this.ics.cmsurl + 'serviceQuestion/deleteComment?syskey=' + obj.syskey, json).subscribe(function (response) {
            // this._cmtArray = [];
            // if (response.state) this._cmtArray = response.data;
            // this._mflag = true;
            _this._cmtArray = [];
            if (response.state)
                _this._cmtArray = response.data;
            _this._ansObj.t2 = "";
            _this._mflag = true;
            if (_this._cmtArray.length > 0)
                _this._obj[0].n3 = _this._cmtArray[0].n4;
            else
                _this._obj[0].n3 = 0;
        }, function (error) { }, function () { });
    };
    /* deleteReplyComment(repObj, index) {
        this.deletereplycomment = true;
        let temp = repObj.syskey;
        this._mflag = false;
        //let url: string = this.ics.cmsurl + 'serviceEduAdm/deleteReplyComment';
        //let json: any = repObj;
        //this.http.doPost(url, json).subscribe(
        this.http.doGet(this.ics.cmsurl + 'serviceQuestion/deleteReplyComment?syskey=' + temp).subscribe(
            response => {
                if (response.state){
                    if(this._cmtArray[index].n11 > 0)
                        this._cmtArray[index].n11 -= 1;
                }
                    this.showMessage(response.msgDesc, true);
                this._mflag = true;
                for (let i = 0; i < this._replyArray.length; i++) {
                    if (temp == this._replyArray[i].syskey) {
                        this._replyArray.splice(i, 1);
                        i--;
                        this.deletereplycomment = false;
                    }
                }
                console.log(JSON.stringify(repObj));
                console.log(JSON.stringify(index));
            },
            error => { },
            () => { }
        );
    } */
    /* setImgUrl(str, p) {
        return p + str;
        //return 'upload/image/' + str;
    } */
    EduViewComponent.prototype.setImgUrl = function (str) {
        console.log("" + JSON.stringify(this.ics._imgurl + 'upload/smallImage/contentImage/' + str));
        return this.ics._imgurl + 'upload/smallImage/contentImage/' + str;
    };
    EduViewComponent.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    EduViewComponent = __decorate([
        core_1.Component({
            selector: 'fmr-eduView',
            template: "\n    <div class=\"container col-md-12 col-sm-12 col-xs-12\" >\n    <div class=\"row clearfix\" >\n        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n        \t<form class=\"form-horizontal\" ngNoForm>\n        \t\t<legend>Content View</legend>\n        \t\t<div class=\"cardview list-height\">   \n                    <div class=\"col-md-12\" *ngFor=\"let obj of _obj;let num = index\" style=\"margin-bottom: 10px;\">         \n        \t\t\t\t<button class=\"btn btn-sm btn-primary\" [disabled]=\"flagnew\" id=\"new\" type=\"button\" (click)=\"goback(obj.syskey)\">Back</button>\n        \t\t\t\t<h4 class=\"uni\"><a (click)=\"goto(obj.syskey)\" style=\"line-height: 1.5;\" style=\"overflow-wrap: break-word;\">{{obj.t1}}</a></h4>\n                        <span style=\"color:gray;font-style:italic;font-size:11px\">{{obj.userName}}</span>            \n                        <div class=\"viewform\">\n                            <p class=\"uni\" [innerHTML]=\"obj.t2\">{{obj.t2}}</p>\n                        </div>\n                        <div class=\"form-group\">\n                        <div *ngFor=\"let img of obj.upload\">\n                        <img src=\"{{img}}\" onError=\"this.src='./image/image_not_found.png';\" height=\"240\" width=\"400\" />\n                        <div>&nbsp;</div>\n                        </div>\n                        </div>\n        \t\t\t\t<hr>\n        \t\t\t</div>\n        \n        \t\t</div>\n            </form>\n        </div>\n    </div>\n</div>\n    <div [hidden]=\"_mflag\">\n    <div  id=\"loader\" class=\"modal\" ></div>\n    </div>\n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], EduViewComponent);
    return EduViewComponent;
}());
exports.EduViewComponent = EduViewComponent;
//# sourceMappingURL=edu-view.component.js.map