"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var FrmContentMenuListComponent = (function () {
    function FrmContentMenuListComponent(ics, ref, _router, route, http) {
        var _this = this;
        this.ics = ics;
        this.ref = ref;
        this._router = _router;
        this.route = route;
        this.http = http;
        // Application Specific
        this._totalcount = 1;
        this._searchVal = "";
        this._StateType = "";
        this._util = new rp_client_util_1.ClientUtil();
        this._array = [];
        this._pager = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 0, "size": 0, "totalcount": 1, "t1": "", "n1": 0, "userid": "", "t2": "" };
        this._image = [];
        this._cmtArray = [];
        this._replyArray = [];
        this._tmpObj = { '_key': 0, '_index': 0 };
        this._obj = { "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": "", "modifiedDate": "", "modifiedTime": "", "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "article", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [], "videoUpload": [], "comData": [], "cropdata": [], "agrodata": [], "ferdata": [], "towndata": [], "crop": null, "fert": null, "agro": null, "addTown": null, "uploadlist": [] };
        this._ansObj = { "syskey": 0, "autokey": 0, "createdDate": "", "modifiedDate": "", "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [] };
        this._replyObj = { "syskey": 0, "autokey": 0, "createdDate": "", "modifiedDate": "", "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": "", "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0, "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0, "upload": [] };
        this._roleval = "";
        this.mstatus = 0;
        this._usersk = 0;
        this._userid = "";
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        this._roleval = ics._profile.t1;
        this.mstatus = ics._profile.loginStatus;
        this.getStatusList(this.mstatus);
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                //this._usersk = id;
                _this._userid = id;
                //this._pager.n1 = this._usersk;
                _this._pager.t2 = _this._userid;
                _this.search(_this._pager);
            }
            else {
                // if(this._usersk!=0){
                //     //this._pager.n1=this._usersk; 
                // }
                if (_this._userid != "") {
                    _this._pager.t2 = _this._userid;
                }
                _this.search(_this._pager);
            }
        });
    }
    FrmContentMenuListComponent.prototype.ChangeStatus = function (options) {
        this._StateType = options[options.selectedIndex].value;
        this.searchVal();
    };
    FrmContentMenuListComponent.prototype.search = function (p) {
        var _this = this;
        // if(this._usersk!=0){
        //     this._pager.n1=this._usersk;
        // }
        if (this._userid != "") {
            this._pager.t2 = this._userid;
        }
        if (p.end == 0) {
            p.end = this.ics._profile.n1;
        }
        if (p.size == 0) {
            p.size = this.ics._profile.n1;
        }
        p.t1 = this._searchVal;
        p.userid = this.ics._profile.userID;
        //alert("syskey: " + this.ics._profile.userid);
        //  let url: string = this.ics._apiurl + 'servicecontentmenu/searchContentMenuList?searchVal=' + this._searchVal + '&croptype=' + this._CropType + '&status=' + this.mstatus + '&statetype=' + this._StateType;
        if (this._StateType == undefined || this._StateType == null || this._StateType == '') {
            this._StateType = "5";
        }
        var url = this.ics._apiurl + 'serviceQuestion/searchQuesList?searchVal=' + this._searchVal + '&status=' + this.mstatus + '&statetype=' + this._StateType + '&searchtype=' + 'news';
        var json = p;
        this.http.doPost(url, json).subscribe(function (response) {
            if (response != null && response != undefined && response.state) {
                _this._totalcount = response.totalCount;
                _this._array = response.data;
                console.log("list data=" + JSON.stringify(_this._array));
                for (var i = 0; i < _this._array.length; i++) {
                    console.log(JSON.stringify("img: " + _this._array[i].upload[i] + " / " + _this._array[i].t13));
                    if (_this._array[i].t1.length > 30) {
                        _this._array[i].t1 = _this._array[i].t1.substring(0, 29) + "...";
                    }
                    if (_this._array[i].t2.length > 75) {
                        _this._array[i].t2 = _this._array[i].t2.substring(0, 74) + "...";
                    }
                    _this._array[i].modifiedDate = _this._util.changeStringtoDateFromDB(_this._array[i].modifiedDate);
                    if (!(_this._array[i].upload.length > 0)) {
                        _this._array[i].t9 = 0;
                    }
                    else {
                        _this._array[i].t9 = 1;
                    }
                }
            }
            else {
                _this._array = [];
                _this._totalcount = 1;
                _this.showMsgAlert("Data not found!");
            }
        }, function (error) { }, function () { });
    };
    FrmContentMenuListComponent.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmContentMenuListComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    FrmContentMenuListComponent.prototype.searchVal = function () {
        this._pager = { "current": 1, "prev": 1, "last": 1, "next": 1, "start": 1, "end": 0, "size": 0, "totalcount": 1, "t1": "", "n1": 0, "userid": "", "t2": "" };
        this.search(this._pager);
    };
    FrmContentMenuListComponent.prototype.goto = function (p) {
        this._router.navigate(['/contentmenu', 'read', p]);
    };
    FrmContentMenuListComponent.prototype.goNew = function () {
        this._router.navigate(['/contentmenu', 'new']);
    };
    FrmContentMenuListComponent.prototype.goView = function (p) {
        this._router.navigate(['/eduView', 'read', p]);
    };
    FrmContentMenuListComponent.prototype.changedPager = function (event) {
        var k = event.flag;
        this._pager = event.obj;
        if (k) {
            this.search(this._pager);
        }
    };
    FrmContentMenuListComponent.prototype.goClickLike = function (key, index) {
        this._array[index].n2 = this._array[index].n2 + 1;
        this.http.doGet(this.ics._apiurl + 'serviceArticle/clickLikeArticle?key=' + key).subscribe(function (data) {
            if (data.state)
                console.log("Like...");
        }, function (error) { }, function () { });
    };
    FrmContentMenuListComponent.prototype.goClickUnlike = function (key, index) {
        this._array[index].n4 = this._array[index].n4 + 1;
        this.http.doGet(this.ics._apiurl + 'serviceArticle/clickUnlikeArticle?key=' + key).subscribe(function (data) {
            if (data.state)
                console.log("Unlike...");
        }, function (error) { }, function () { });
    };
    FrmContentMenuListComponent.prototype.goComment = function (key, index) {
        var _this = this;
        jQuery("#commentModal").modal();
        this._tmpObj._key = key;
        this._tmpObj._index = index;
        this._cmtArray = [];
        this.http.doGet(this.ics._apiurl + 'serviceArticle/getComments?id=' + key).subscribe(function (response) {
            if (response.state)
                _this._cmtArray = response.data;
        }, function (error) { }, function () { });
    };
    FrmContentMenuListComponent.prototype.saveComment = function (cmtObj) {
        var _this = this;
        cmtObj.n1 = this._tmpObj._key;
        cmtObj.n5 = Number(this.ics._profile.t1);
        cmtObj.t12 = '';
        cmtObj.t13 = '';
        cmtObj.t3 = 'news';
        var url = this.ics._apiurl + 'serviceArticle/saveComment';
        var json = cmtObj;
        this.http.doPost(url, json).subscribe(function (response) {
            _this._cmtArray = [];
            if (response.state)
                _this._cmtArray = response.data;
            _this._array[_this._tmpObj._index].n3 = _this._array[_this._tmpObj._index].n3 + 1;
            _this._ansObj.t2 = "";
        }, function (error) {
            _this.showMessage("Can't Saved This Record!!!", undefined);
        }, function () { });
    };
    FrmContentMenuListComponent.prototype.replyComment = function (key, num) {
        var _this = this;
        this._tmpObj._index = num;
        this._cmtArray[num].t13 = 'true';
        this._replyArray = [];
        this.http.doGet(this.ics._apiurl + 'serviceArticle/getComments?id=' + key).subscribe(function (response) {
            if (response.state)
                _this._replyArray = response.data;
        }, function (error) { }, function () { });
    };
    FrmContentMenuListComponent.prototype.saveReply = function (skey) {
        var _this = this;
        this._replyObj.n1 = skey;
        this._replyObj.n3 = -1;
        this._replyObj.n5 = Number(this.ics._profile.t1);
        var url = this.ics._apiurl + 'serviceArticle/saveComment';
        var json = this._replyObj;
        this.http.doPost(url, json).subscribe(function (response) {
            _this._replyArray = [];
            if (response.state)
                _this._replyArray = response.data;
            _this._cmtArray[_this._tmpObj._index].n3 = _this._cmtArray[_this._tmpObj._index].n3 + 1;
            _this._replyObj.t2 = "";
        }, function (error) {
            _this.showMessage("Can't Saved This Record!!!", undefined);
        }, function () { });
    };
    FrmContentMenuListComponent.prototype.editComment = function (obj, num) {
        this._cmtArray[num].t12 = 'true';
    };
    FrmContentMenuListComponent.prototype.deleteComment = function (obj, num) {
        var _this = this;
        var url = this.ics._apiurl + 'serviceArticle/deleteComment';
        var json = obj;
        this.http.doPost(url, json).subscribe(function (response) {
            _this.showMessage(response.msgDesc, response.state);
            _this._cmtArray = [];
            if (response.state)
                _this._cmtArray = response.data;
        }, function (error) { }, function () { });
    };
    FrmContentMenuListComponent.prototype.setImgUrl = function (str, p) {
        //console.log(p + "Image Path");
        //return 'upload/image/' + str;
        // return str;
        //  alert("p+str: " + p+str);
        return p + str;
    };
    FrmContentMenuListComponent.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "primary", "t3": msg });
        }
    };
    FrmContentMenuListComponent.prototype.goImage = function (key, index) {
        var _this = this;
        jQuery("#commentModal").modal();
        this._tmpObj._key = key;
        this._tmpObj._index = index;
        this._image = [];
        this.http.doGet(this.ics._apiurl + 'serviceQuestion/getImage?id=' + key).subscribe(function (response) {
            if (response.state) {
                _this._image = response.uploads;
            }
        }, function (error) { }, function () { });
    };
    /*  getCropComboList() {
         this.http.doGet(this.ics._apiurl + 'serviceCrop/getSenderList').subscribe(
             response => {
                 if ((response != null || response != undefined) && response.data.length > 0) {
                     this.ref._lov3.cropcombo = this._util.changeArray(response.data, this._obj, 1);
                 }
             },
             error => { },
             () => { }
         );
     } */
    FrmContentMenuListComponent.prototype.getStatusList = function (mstatus) {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'serviceQuestion/getStatusList?status=' + this.mstatus).subscribe(function (response) {
            if ((response != null || response != undefined) && response.data.length > 0) {
                _this.ref._lov3.statuscombo = _this._util.changeArray(response.data, _this._obj, 1);
                _this._StateType = _this.ref._lov3.statuscombo[3].value;
            }
        }, function (error) { }, function () { });
    };
    FrmContentMenuListComponent = __decorate([
        core_1.Component({
            selector: 'fmr-eduList',
            template: "\n    <div class=\"container\">\n    <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n    <form class=\"form-inline\" > \n    <legend>Content List</legend>\n    <div class=\"input-group\" style=\"padding-right: 10px;\">\n        <span class=\"input-group-btn input-md\">\n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"goNew();\">New</button>\n        </span>  \n             <input id=\"textinput\" name=\"textinput\" type=\"text\" placeholder=\"Search\" [(ngModel)]=\"_searchVal\" (keyup.enter)=\"searchVal()\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control input-md\">\n        <span class=\"input-group-btn input-md\">\n\t        <button class=\"btn btn-primary input-md\" type=\"button\" (click)=\"searchVal()\" >\n\t    <span class=\"glyphicon glyphicon-search\"></span>Search\n\t        </button>\n        </span>\n    </div>        \n\t<div class=\"input-group\">\n        <label class=\"col-md-4\" style=\"padding-left: 0px;\">Status</label>\n        <div class=\"col-md-8\" style=\"padding-left: 0px;\">\n            <select [(ngModel)]=\"_StateType\" (change)=\"ChangeStatus($event.target.options)\" class=\"form-control col-md-0\" [ngModelOptions]=\"{standalone: true}\" required>\n                <option *ngFor=\"let item of ref._lov3.statuscombo\" value=\"{{item.value}}\">{{item.caption}}</option>\n            </select>\n        </div>\n    </div>           \n    </form>\n    <pager id=\"pgarticle\" [(rpModel)]=\"_totalcount\" [(rpCurrent)]=\"_pager.current\" (rpChanged)=\"changedPager($event)\"></pager> \n         <div class=\"table-responsive\">\n            <table class=\"table table-striped table-condensed table-hover tblborder\" id=\"tblstyle\">\n                <thead>\n                    <tr>\n                          <th width=\"20%\">Title</th>\n                          <th width=\"33%\">Content</th>\n                          <th width=\"5%\">Image</th>\n                          <th width=\"5%\">Status</th>\n                          <th width=\"5%\">Type</th>\n                          <th width=\"5%\">Date</th>\n                          <th width=\"7%\">Time</th>\n                          <th width=\"10%\">Posted By</th>\n                          <th width=\"10%\">Modified By</th>\n                    </tr>\n                </thead>\n                <tbody *ngFor=\"let obj of _array;let num = index\">\n                <tr>\n                    <td><a (click)=\"goto(obj.syskey)\" class=\"uni\" ><p>{{obj.t1}}</p></a></td>\n                    <td><p  class=\"uni\">{{obj.t2}}</p></td>\n                    <td *ngIf =\"obj.t9==0\"><p></p></td>\n                    <td *ngIf =\"obj.t9==1\"> <img src={{obj.upload[0]}} height=\"50\" width=\"50\" /></td>\n                    <td><p  class=\"uni\">{{obj.t10}}</td>\n                    <td><p  class=\"uni\">{{obj.t3}}</p></td>\n                    <td><p  class=\"uni\">{{obj.modifiedDate}}</p></td>\n                    <td><p  class=\"uni\">{{obj.modifiedTime}}</p></td>\n                    <td><span style=\"color:gray;font-style:italic;font-size:11px\">{{obj.userName}}</span></td>\n                    <td><span style=\"color:gray;font-style:italic;font-size:11px\">{{obj.modifiedUserName}}</span></td>\n                </tr>\n                </tbody>\n            </table>\n        </div>\n    </div>\n   </div>\n   </div>\n\n    <!-- Image Modal -->\n    <div id=\"commentModal\" class=\"modal fade\" role=\"dialog\">\n    <div class=\"modal-dialog modal-md\">  \n        <div class=\"modal-content\">\n        <div class=\"modal-header\">\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>           \n            <div class=\"form-group col-md-12\">\n                        <h4 align=\"center\" class=\"modal-title\">Image</h4>\n            </div>\n        </div>\n        <div id=\"jbody\" class=\"modal-body\" >\n            <div style=\"overflow: hidden; position: relative;\">                \n                <div class=\"col-xs-7 col-sm-7 col-md-7 col-lg-7  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n                    <div class=\"form-group col-md-12\">\n                        <label class=\"col-md-6\"></label>\n                        <div class=\"col-md-6\" *ngFor=\"let img of _image\">\n                            <img src={{setImgUrl(img)}} alt={{img}} height=\"140\" width=\"200\" align=\"middle\" />\n                        </div> \n                        <div class=\"col-md-0\"></div>\n                    </div>                 \n                   \n                </div>\n            </div>\n        </div>\n        </div>\n    </div>\n</div>\n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, rp_references_1.RpReferences, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService])
    ], FrmContentMenuListComponent);
    return FrmContentMenuListComponent;
}());
exports.FrmContentMenuListComponent = FrmContentMenuListComponent;
//# sourceMappingURL=frmcontentmenulist.component.js.map