"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var AdminPager = (function () {
    function AdminPager(ics, http) {
        this.ics = ics;
        this.http = http;
        this.rpModelChange = new core_1.EventEmitter();
        this.rpChanged = new core_1.EventEmitter();
        this._pages = [];
        this._sizes = [];
        this.currentPage = 1;
        this.prev = 1;
        this.last = 1;
        this.next = 2;
        this.start = 1;
        this.end = 10;
        this.pageSize = 10;
        this.totalCount = 1;
        this._flag = false;
        this._obj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 1 };
        this._calcPag = "";
        if (this.rpId == null || this.rpId == "")
            this.rpId = "myid";
        if (this.rpClass == null || this.rpClass == "")
            this.rpClass = "col-md-3";
        if (this.rpPageSize == null || this.rpPageSize == "")
            this.rpPageSize = "10";
        if (this.rpPageSizeMax == null || this.rpPageSizeMax == 0)
            this.rpPageSizeMax = 100;
        if (this.rpModel == null || this.rpModel < 0)
            this.rpModel = 1;
    }
    AdminPager.prototype.ngAfterContentInit = function () {
        //console.log("ngInit: "+JSON.stringify(this.rpModel));
        this.totalCount = +this.rpModel;
        this.last = Math.ceil(this.totalCount / this.pageSize);
        this.fillPageSizes(this.rpPageSizeMax);
        this.fillPages();
        this.updatePager(this.currentPage);
    };
    AdminPager.prototype.ngOnChanges = function (rpModel) {
        //console.log("ngOnChanges: "+JSON.stringify(this.rpModel));
        this.totalCount = +this.rpModel;
        this.currentPage = 1;
        this.end = 10;
        this.pageSize = 10;
        this.last = Math.ceil(this.totalCount / this.pageSize);
        this.fillPages();
        this.updatePager(this.currentPage);
    };
    AdminPager.prototype.initializePager = function (num) {
        this._obj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 1, "size": 10, "totalcount": this.totalCount };
    };
    AdminPager.prototype.updatePager = function (k) {
        //custom pager by hms... 20160614 11:00pm... ymk...
        var num = +k;
        this.currentPage = num;
        this.prev = num > 1 ? num - 1 : 1;
        this.next = (num + 1) < this.last ? (num + 1) : this.last;
        this.start = this.totalCount < 1 ? this.totalCount : ((num - 1) * this.pageSize) + 1;
        this.end = (num * this.pageSize) > this.totalCount ? this.totalCount : (num * this.pageSize);
        this._obj = {
            "current": this.currentPage, "prev": this.prev, "last": this.last,
            "next": this.next, "start": this.start, "end": this.end, "size": this.pageSize,
            "totalcount": this.totalCount
        };
        var t = Number(this._obj.totalcount);
        var i = Number(this._obj.size);
        var c = Number(this._obj.current);
        var s = t > 0 ? ((c - 1) * i) + 1 : 0;
        var m = (((c - 1) * i) + i) > t ? t : (((c - 1) * i) + i);
        this._calcPag = s + " - " + m + " of " + t;
        this.rpChanged.emit(this._obj);
        //console.log(JSON.stringify(this._obj));
    };
    AdminPager.prototype.fillPageSizes = function (pg) {
        var size = +pg;
        this._obj.current = 1;
        var total = size / this.pageSize;
        //console.log("total" + total);
        for (var i = 1; i <= total; i++) {
            var k = { "value": 0, "caption": "" };
            k.value = i * 10;
            k.caption = "" + (i * 10);
            this._sizes.push(k);
        }
    };
    AdminPager.prototype.fillPages = function () {
        this._pages = [];
        this._obj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 1, "size": 10, "totalcount": this.totalCount };
        //console.log("running fillPages...");
        var num = Math.ceil(this.totalCount / this.pageSize);
        for (var i = 1; i <= num; i++) {
            var k = { "value": 0, "caption": "" };
            k.value = i;
            k.caption = "" + i;
            this._pages.push(k);
        }
    };
    AdminPager.prototype.goFirst = function () {
        this.currentPage = 1;
        this.updatePager(this.currentPage);
    };
    AdminPager.prototype.goLast = function () {
        this.currentPage = Math.ceil(this.totalCount / this.pageSize);
        this.updatePager(this.currentPage);
    };
    AdminPager.prototype.goPrev = function () {
        var k = this.prev;
        this.updatePager(k);
    };
    AdminPager.prototype.goNext = function () {
        var k = this.next;
        this.updatePager(k);
    };
    AdminPager.prototype.changRows = function () {
        this.fillPages();
        this.last = Math.ceil(this.totalCount / this.pageSize);
        this.updatePager(1);
    };
    AdminPager.prototype.changPage = function () {
        this.updatePager(this.currentPage);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], AdminPager.prototype, "rpId", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], AdminPager.prototype, "rpClass", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], AdminPager.prototype, "rpPageSize", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], AdminPager.prototype, "rpPageSizeMax", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], AdminPager.prototype, "rpModel", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], AdminPager.prototype, "rpModelChange", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], AdminPager.prototype, "rpChanged", void 0);
    AdminPager = __decorate([
        core_1.Component({
            selector: 'adminpager',
            template: "  \n    <div>\n    <table>\n    <tr>\n    <td>\n    <nav>\n      <ul class=\"pagination\">\n        <li class=\"page-item\">\n          <a class=\"page-link\" (click)=\"goFirst()\" style=\"cursor: pointer\">First</a>\n        </li>\n        <li class=\"page-item\">\n          <a class=\"page-link\" (click)=\"goPrev()\" style=\"cursor: pointer\">Prev</a>\n        </li>\n        <li class=\"page-item\">\n          <span class=\"page-link\">\n            <select [(ngModel)]=\"currentPage\" class=\"page-item\" (ngModelChange)=\"changPage()\">\n              <option *ngFor=\"#item of _pages\" value=\"{{item.value}}\" >{{item.caption}}</option>\n            </select>\n          </span>\n        </li>\n        <li class=\"page-item\">\n          <a class=\"page-link\" (click)=\"goNext()\" style=\"cursor: pointer\">Next</a>\n        </li>\n        <li class=\"page-item\">\n          <a class=\"page-link\" (click)=\"goLast()\" style=\"cursor: pointer\">Last</a>\n        </li>        \n        <li class=\"page-item  active\">\n          <span class=\"page-link\">\n            <select [(ngModel)]=\"pageSize\" class=\"page-item text-primary\" (ngModelChange)=\"changRows()\">\n              <option class=\"text-primary\" *ngFor=\"#item of _sizes\" value=\"{{item.value}}\" >{{item.caption}}</option>\n            </select>\n          </span>\n        </li>\n      </ul>\n      </nav>\n      </td>\n      <td><label>{{_calcPag}}</label></td>\n      </tr>\n      </table>\n    </div>\n    ",
            providers: [rp_http_service_1.RpHttpService]
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, rp_http_service_1.RpHttpService])
    ], AdminPager);
    return AdminPager;
}());
exports.AdminPager = AdminPager;
//# sourceMappingURL=adminpager.component.js.map