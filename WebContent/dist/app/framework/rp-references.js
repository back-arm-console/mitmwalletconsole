"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var RpReferences = (function () {
    function RpReferences() {
        //fix combo
        this._lov1 = {
            //utility report status
            "refstatus": [{ "value": "ALL", "caption": "ALL" }, { "value": "0", "caption": "Unpaid" }, { "value": "1", "caption": "Paid" }],
            //User Report 
            "ref021": [{ "value": "all", "caption": "All" }, { "value": "save", "caption": "Save" }, { "value": "activate", "caption": "Activate" }, { "value": "deactivate", "caption": "Deactivate" }, { "value": "lock", "caption": "Lock" }, { "value": "delete", "caption": "Delete" }],
            //"refUserType": [{ "value": "1", "caption": "End User" },{ "value": "2", "caption": "Branch User" },{ "value": "3", "caption": "Merchant User" }],
            "ref010": [{ "value": "EXCEL", "caption": "EXCEL" }, { "value": "PDF", "caption": "PDF" }],
            "ref011": [{ "value": "All", "caption": "All" }, { "value": "1", "caption": "Wallet To Wallet" }, { "value": "2", "caption": "Merchant payment" }, { "value": "4", "caption": "Agent to Wallet" }],
            "ref025": [{ "value": "2", "caption": "Admin Console" }, { "value": "3", "caption": "Merchant Console" }],
            // SMS Site
            "refsite": [{ "value": "From", "caption": "From" }, { "value": "To", "caption": "To" }, { "value": "Both", "caption": "Both" }],
            "reffrsite": [{ "value": "From", "caption": "From" }],
            "refActive": [{ "value": "1", "caption": "Yes" }, { "value": "0", "caption": "No" }],
            "refLanguage": [{ "value": "1", "caption": "English" }, { "value": "2", "caption": "Myanmar" }],
            //Commission Type
            "ref027": [{ "value": "0", "caption": "Flat" }, { "value": "1", "caption": "Percentage" }],
            //Pay Template
            "ref029": [{ "value": "text", "caption": "Text" }, { "value": "lov", "caption": "List of values" }],
            "ref030": [{ "value": "", "caption": "-" }, { "value": "text", "caption": "Text" }, { "value": "lov", "caption": "List of values" }],
            "ref032": [{ "value": "", "caption": "" }],
            "stock": [{ "value": "1", "caption": "STOCK" }],
            "currency": [{ "value": "1", "caption": "USD" }],
            "gender": [{ "value": "", "caption": "" }, { "value": "m", "caption": "male" }, { "value": "f", "caption": "female" }, { "value": "0", "caption": "others" }],
            "prefix": [{ "value": "", "caption": "" }, { "value": "5", "caption": "Dr" }, { "value": "3", "caption": "Miss" }, { "value": "1", "caption": "Mr" }, { "value": "2", "caption": "Mrs" }, { "value": "4", "caption": "Ms" }],
            "string": [{ "value": "eq", "caption": "Equals" }, { "value": "c", "caption": "Contains" }, { "value": "bw", "caption": "Begins with" }, { "value": "ew", "caption": "End with" }],
            "numeric": [{ "value": "eq", "caption": "Equals" }, { "value": "gt", "caption": "Greater than" }, { "value": "lt", "caption": "Less than" }, { "value": "geq", "caption": "Greater than or Equal" },
                { "value": "leq", "caption": "Less than or Equal" }, { "value": "bt", "caption": "Between" }],
            "date": [{ "value": "eq", "caption": "Equals" }, { "value": "gt", "caption": "Greater than" }, { "value": "lt", "caption": "Less than" }, { "value": "geq", "caption": "Greater than or Equal" },
                { "value": "leq", "caption": "Less than or Equal" }, { "value": "bt", "caption": "Between" }],
            "yesno": [{ "value": "1", "caption": "NO" }, { "value": "2", "caption": "YES" }],
            "ref017": [{ "value": "0", "caption": "NRC" }, { "value": "1", "caption": "CIF" }, { "value": "2", "caption": "Account" }, { "value": "3", "caption": "Name" }],
            "ref023": [{ "value": "0", "caption": "Equals" }, { "value": "1", "caption": "Contains" }, { "value": "2", "caption": "Begins With" }, { "value": "3", "caption": "End With" }],
            //Agent Transfer Type
            //"ref033": [{ "value": "1", "caption": "Agent to Agent" }, { "value": "2", "caption": "Agent to Customer" }, { "value": "3", "caption": "Agent to Non-Customer" }],
            //Agent Status
            //"ref034": [{ "value": "1", "caption": "Drawing" }, { "value": "2", "caption": "Encash" }],
            "transferType": [{ "value": "All", "caption": "All" }, { "value": "Own", "caption": "Own Transfer" }, { "value": "Internal", "caption": "Internal Transfer" }],
            "refaccount": [{ "value": "0", "caption": "Branch Code" }, { "value": "1", "caption": "Account" }, { "value": "2", "caption": "Name" }],
            "refgl": [{ "value": "0", "caption": "Accdesc" }, { "value": "1", "caption": "AccNo" }, { "value": "2", "caption": "SubGroup" }, { "value": "3", "caption": "SubGroupDesc" }],
            "refmenu": [{ "value": "1", "caption": "Customer" }, { "value": "2", "caption": "MIT" }],
            "videoType": [{ "value": "", "caption": "-" }, { "value": "Video", "caption": "Video" }],
            "filetype": [{ "value": "0", "caption": "Law" }, { "value": "1", "caption": "Form" }],
            "postingstate": [{ "value": "1", "caption": "Draft" }, { "value": "2", "caption": "Pending" }, { "value": "4", "caption": "Approve" }, { "value": "5", "caption": "Publish" }, { "value": "6", "caption": "Reject" }]
        };
        this._lov2 = {
            "ref001": [{ "value": "", "caption": "Empty" }]
        };
        this._lov3 = {
            "ref011": [{ "value": "", "caption": "Empty" }],
            "ref001": [{ "value": "", "caption": "Empty" }],
            "refstate": [{ "value": "", "caption": "Empty" }],
            "refchannel": [{ "value": "", "caption": "Empty" }],
            "refAppCode": [{ "value": "-", "caption": "-" }],
            "refVersionStatusCode": [{ "value": "-", "caption": "-" }],
            "refAccountTransferType": [{ "value": "-", "caption": "-" }],
            "refOrder": [{ "value": "-", "caption": "-" }],
            "msgCode": "",
            "msgDesc": "",
            "ref032": [{ "value": "", "caption": "" }],
            "refTicketStatus": [{ "value": "-", "caption": "-" }],
            "refFileType": [{ "value": "", "caption": "Empty" }],
            "refchannelkey": [{ "value": "", "caption": "-" }],
            "domainType": [{ "value": "", "caption": "-" }],
            "refUserType": [{ "value": "", "caption": "-" }],
        };
    }
    RpReferences = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], RpReferences);
    return RpReferences;
}());
exports.RpReferences = RpReferences;
//# sourceMappingURL=rp-references.js.map