"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
require('rxjs/add/operator/map');
var DashboardService = (function () {
    function DashboardService() {
    }
    DashboardService.prototype.generatePieChart = function (id, d) {
        Highcharts.chart(id, {
            chart: { plotBackgroundColor: null, plotBorderWidth: null, plotShadow: false, type: 'pie' },
            title: { text: '' },
            tooltip: { pointFormat: '</b>{point.percentage:.2f}%</b>' },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '{point.percentage:.2f}%',
                        style: { color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black' }
                    },
                    showInLegend: true
                }
            },
            credits: { enabled: false },
            series: [{ name: 'Brands', colorByPoint: true, data: d }]
        });
    };
    DashboardService.prototype.generateLineChart = function (id, d, c, yt) {
        Highcharts.chart(id, {
            chart: { type: 'line' },
            title: { text: '' },
            subtitle: { text: '' },
            xAxis: {
                categories: c
            },
            yAxis: {
                title: {
                    text: yt
                }
            },
            credits: { enabled: false },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series: d
        });
    };
    /* generateBarChart(id,d,c,yt){
        Highcharts.chart(id, {
            chart: { type: 'column' },
            title: { text: '' },
            subtitle: { text: '' },
            xAxis: {
                categories: c,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: { text: yt }
            },
            credits: { enabled: false },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.2f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: d
        });
    }
 */
    DashboardService.prototype.generateBarChart = function (id, d, c, yt, minNumber) {
        Highcharts.chart(id, {
            chart: { type: 'column' },
            title: { text: '' },
            subtitle: { text: '' },
            xAxis: {
                categories: c,
                crosshair: true
            },
            yAxis: {
                min: minNumber,
                title: { text: '' }
            },
            credits: { enabled: false },
            tooltip: {
                headerFormat: '<span style="font-size:10px"><b>{point.key}</b></span><table>',
                pointFormat: '<tr><td style="padding:0; font-size:11px"> Amount   </td>' +
                    '<td style="padding:0; font-size:11px">:{point.y:.2f}</td>' +
                    '</tr>' +
                    '<tr><td style="padding:0; font-size:11px"> Transaction </td>' +
                    '<td style="padding:0; font-size:11px">:{point.count}</td>' +
                    '</tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    pointWidth: 28,
                    borderWidth: 0
                }
            },
            series: d
        });
    };
    DashboardService.prototype.generateBarChartWTW = function (id, d, c, yt, minNumber) {
        Highcharts.chart(id, {
            chart: { type: 'column' },
            title: { text: '' },
            subtitle: { text: '' },
            xAxis: {
                categories: c,
                crosshair: true
            },
            yAxis: {
                min: minNumber,
                title: { text: '' }
            },
            credits: { enabled: false },
            tooltip: {
                headerFormat: '<span style="font-size:10px"><b>{point.key}</b></span><table>',
                pointFormat: '<tr><td style="padding:0; font-size:11px"> Amount   </td>' +
                    '<td style="padding:0; font-size:11px">:{point.y:.2f}</td>' +
                    '</tr>' +
                    '<tr><td style="padding:0; font-size:11px"> Transaction </td>' +
                    '<td style="padding:0; font-size:11px">:{point.count}</td>' +
                    '</tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    pointWidth: 28,
                    borderWidth: 0
                }
            },
            series: d
        });
    };
    DashboardService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], DashboardService);
    return DashboardService;
}());
exports.DashboardService = DashboardService;
//# sourceMappingURL=dashboard-service.js.map