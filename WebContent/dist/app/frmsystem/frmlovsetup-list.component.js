"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var Observable_1 = require('rxjs/Observable');
// RP Framework
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_client_util_1 = require('../util/rp-client.util');
// Application Specific
var FrmLOVSetupListComponent = (function () {
    function FrmLOVSetupListComponent(ics, _router, http) {
        this.ics = ics;
        this._router = _router;
        this.http = http;
        this._col_list = [{ "col_no": "1", "flag": false }, { "col_no": "2", "flag": false }];
        this._sorting = { _sort_type: "asc", _sort_col: "1" };
        this._mflag = true;
        this._util = new rp_client_util_1.ClientUtil();
        this.sessionAlertMsg = "";
        this._totalCount = 1;
        this._list = { "data": [{ "sysKey": 0, "lovNo": "", "lovDesc2": "", "lovType": "" }], "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0 };
        this._pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 1 };
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = false;
            this.search();
        }
    }
    // list sorting part
    FrmLOVSetupListComponent.prototype.changeDefault = function () {
        for (var i = 0; i < this._col_list.length; i++) {
            this._col_list[i].flag = false;
        }
    };
    FrmLOVSetupListComponent.prototype.addSort = function (e) {
        for (var i = 0; i < this._col_list.length; i++) {
            if (this._col_list[i].col_no == e) {
                var _tmp_flag = this._col_list[i].flag;
                this.changeDefault();
                this._col_list[i].flag = !_tmp_flag;
                this._sorting._sort_type = !_tmp_flag ? "desc" : "asc";
                this._sorting._sort_col = e;
                break;
            }
        }
    };
    FrmLOVSetupListComponent.prototype.changedPager = function (event) {
        if (this._list.totalCount != 0) {
            this._pgobj = event;
            var current = this._list.currentPage;
            var size = this._list.pageSize;
            this._list.currentPage = this._pgobj.current;
            this._list.pageSize = this._pgobj.size;
            if (this._pgobj.current != current || this._pgobj.size != size) {
                this.search();
            }
        }
    };
    FrmLOVSetupListComponent.prototype.search = function () {
        var _this = this;
        this._mflag = false;
        try {
            var url = this.ics._apiurl + 'service001/lovSetuplist';
            var json = this._list;
            this.http.doPost(url, json).subscribe(function (res) {
                _this._mflag = false;
                if (res.msgCode == '0016') {
                    _this.sessionAlertMsg = res.msgDesc;
                    _this.showMessage();
                }
                else {
                    if (res != null) {
                        _this._totalCount = res.totalCount;
                        _this._list = res;
                        if (_this._totalCount == 0) {
                            _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Data not found!" });
                        }
                        if (res.data != null) {
                            if (!(res.data instanceof Array)) {
                                /* let m = [];
                                m[0] = response.commdetailArr;
                                this._flexobj.commdetailArr = m;
                                this._flexobj.totalCount = response.totalCount; */
                                var m = [];
                                m[0] = res.data;
                                _this._list.data = m;
                                _this._list.totalCount = res.totalCount;
                            }
                            else {
                                _this._list = res;
                            }
                        }
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmLOVSetupListComponent.prototype.searchol = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics._apiurl + 'service001/getUserListing?searchVal=' + encodeURIComponent(this._list.searchText) + '&pagesize=' + this._list.pageSize + '&currentpage=' + this._list.currentPage + '&operation=alluserprofile&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMessage();
                }
                else {
                    _this._mflag = true;
                    if (data != null && data != undefined) {
                        _this._pgobj.totalcount = data.totalCount;
                        _this._list = data;
                        if (_this._list.totalCount == 0) {
                            _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Data not found!" });
                        }
                    }
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmLOVSetupListComponent.prototype.searchKeyup = function (e) {
        if (e.which == 13) {
            this._list.currentPage = 1;
            this._msghide = true;
            this._recordhide = false;
            this.search();
        }
    };
    FrmLOVSetupListComponent.prototype.Searching = function () {
        this._list.currentPage = 1;
        this._msghide = true;
        this._recordhide = false;
        this.search();
    };
    FrmLOVSetupListComponent.prototype.goto = function (p) {
        this._router.navigate(['/QuickPayLOVSetup', 'read', p]);
    };
    FrmLOVSetupListComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Observable_1.Observable.timer(3000).subscribe(function (x) {
            _this.goLogOut();
        });
    };
    FrmLOVSetupListComponent.prototype.goNew = function () {
        this._router.navigate(['/QuickPayLOVSetup', 'new']);
    };
    FrmLOVSetupListComponent.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    FrmLOVSetupListComponent = __decorate([
        core_1.Component({
            selector: 'lovsetup-list',
            template: " \n  <div class=\"container\">\n  <div class=\"row clearfix\">\n    <form class=\"form-inline\"> \n      <!-- Form Name -->\n      <legend>List of Values </legend>\n      <div class=\"input-group\">\n        <span class=\"input-group-btn input-md\" >\n          <button class=\"btn btn-primary\" type=\"button\" (click)=\"goNew();\">New</button>\n        </span>\n        <input id=\"textinput\" name=\"textinput\" type=\"text\" placeholder=\"Search\" [(ngModel)]=\"_list.searchText\" (keyup)=\"searchKeyup($event)\" maxlength=\"50\" class=\"form-control input-md\">\n        <span class=\"input-group-btn input-md\">\n          <button class=\"btn btn-primary input-md\" type=\"button\" (click)=\"Searching()\" >\n            <span class=\"glyphicon glyphicon-search\"></span>Search\n          </button>\n        </span>        \n      </div>  \n    </form>\n    <div style = \"margin-top : 10px\">\n      <adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"_list.totalCount\" (rpChanged)=\"changedPager($event)\" ></adminpager> \n    </div> \n    <table class=\"table table-striped table-condensed table-hover tblborder\" id=\"tblstyle\">\n      <thead>\n        <tr>\n          <th >Code</th>\n          <th >Description</th>   \n        </tr>\n      </thead>\n      <tbody>\n        <tr *ngFor=\"let obj of  _list.data\">\n          <td><a (click)=\"goto(obj.sysKey)\">{{obj.lovNo}}</a></td>\n          <td>{{obj.lovDesc2}}</td>                                      \n        </tr> \n      </tbody>\n    </table>\n  </div> \n  </div>\n  <div [hidden] = \"_mflag\">\n    <div class=\"modal\" id=\"loader\"></div>\n  </div>   \n   "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_http_service_1.RpHttpService])
    ], FrmLOVSetupListComponent);
    return FrmLOVSetupListComponent;
}());
exports.FrmLOVSetupListComponent = FrmLOVSetupListComponent;
//# sourceMappingURL=frmlovsetup-list.component.js.map