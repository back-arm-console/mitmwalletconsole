"use strict";
var router_1 = require('@angular/router');
var frmusersetup_component_1 = require('./frmusersetup.component');
var frmuser_list_component_1 = require('./frmuser-list.component');
var frmversionhistory_component_1 = require('./frmversionhistory.component');
var frmversionhistory_list_component_1 = require('./frmversionhistory-list.component');
var frmpasswordpolicysetup_component_1 = require('./frmpasswordpolicysetup.component');
var frmforcesignout_component_1 = require('./frmforcesignout.component');
var frmmenusetup_component_1 = require('./frmmenusetup.component');
var frmmenusteup_list_component_1 = require('./frmmenusteup-list.component');
var frmrolesetup_component_1 = require('./frmrolesetup.component');
var frmrolesetup_list_component_1 = require('./frmrolesetup-list.component');
var frmlovsetup_component_1 = require('./frmlovsetup.component');
var frmlovsetup_list_component_1 = require('./frmlovsetup-list.component');
var frmsystemRoutes = [
    { path: 'Password Policy', component: frmpasswordpolicysetup_component_1.FrmPasswordPolicyComponent },
    { path: 'Forced Log out', component: frmforcesignout_component_1.FrmForceSignOutComponent },
    { path: 'QuickPayLOVSetup', component: frmlovsetup_component_1.FrmLovSetupComponent },
    { path: 'QuickPayLOVSetup/:cmd', component: frmlovsetup_component_1.FrmLovSetupComponent },
    { path: 'QuickPayLOVSetup/:cmd/:id', component: frmlovsetup_component_1.FrmLovSetupComponent },
    { path: 'lovsetuplist', component: frmlovsetup_list_component_1.FrmLOVSetupListComponent },
    { path: 'version-history', component: frmversionhistory_component_1.FrmVersionHistoryComponent },
    { path: 'version-history/:cmd', component: frmversionhistory_component_1.FrmVersionHistoryComponent },
    { path: 'version-history/:cmd/:id', component: frmversionhistory_component_1.FrmVersionHistoryComponent },
    { path: 'version-history-list', component: frmversionhistory_list_component_1.FrmVersionHistoryListComponent },
    { path: 'User Setup', component: frmusersetup_component_1.FrmUserSetupComponent },
    { path: 'User Setup/:cmd', component: frmusersetup_component_1.FrmUserSetupComponent },
    { path: 'User Setup/:cmd/:id', component: frmusersetup_component_1.FrmUserSetupComponent },
    { path: 'UserList', component: frmuser_list_component_1.FrmUserListComponent },
    { path: 'Menu', component: frmmenusetup_component_1.FrmMenuSetupComponent },
    { path: 'Menu/:cmd', component: frmmenusetup_component_1.FrmMenuSetupComponent },
    { path: 'Menu/:cmd/:id', component: frmmenusetup_component_1.FrmMenuSetupComponent },
    { path: 'menusetuplist', component: frmmenusteup_list_component_1.FrmMenuSetupListComponent },
    { path: 'Role', component: frmrolesetup_component_1.FrmRoleSetupComponent },
    { path: 'Role/:cmd', component: frmrolesetup_component_1.FrmRoleSetupComponent },
    { path: 'Role/:cmd/:id', component: frmrolesetup_component_1.FrmRoleSetupComponent },
    { path: 'rolesetuplist', component: frmrolesetup_list_component_1.FrmRoleSetupListComponent },
];
exports.FrmSystemRouting = router_1.RouterModule.forChild(frmsystemRoutes);
//# sourceMappingURL=frmsystem.routing.js.map