"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var Observable_1 = require('rxjs/Observable');
var core_2 = require('@angular/core');
// RP Framework
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
core_2.enableProdMode();
var FrmUserListComponent = (function () {
    function FrmUserListComponent(ics, _router, http) {
        this.ics = ics;
        this._router = _router;
        this.http = http;
        this._key = "";
        this._mflag = true;
        this.confirmpwd = "";
        this._sessionMsg = "";
        this._obj = {
            "refkey": "", "refsvytype": "", "hub": "", "division": "",
            "township": "", "branch": "", "branchno": "", "villagename": "", "gpscoordinate": "",
            "t1": "", "t2": "", "t3": "", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "", "t9": "", "t10": "", "t11": ""
        };
        this._priobj = {
            "data": [{ "refkey": "", "type": "", "hub": "", "division": "", "township": "", "branch": "", "period": "" },
                { "refkey": "", "type": "", "hub": "", "division": "", "township": "", "branch": "", "period": "" }]
        };
        this._returnResult = { "keyResult": 0, "longResult": "", "state": "", "stringResult": "", "userId": "", "msgCode": "", "msgDesc": "" };
        this._userobj = {
            "data": [{ "syskey": 0, "autokey": 0, "createdDate": "", "modifiedDate": "", "userId": "", "userName": "", "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "parentId": 0, "usersyskey": 0, "t1": "", "username": "", "n2": "" }],
            "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0
        };
        this._pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 0 };
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this.loader = true;
            //jQuery("#loader").modal('hide');
            this._mflag = false;
            this.search();
        }
    }
    FrmUserListComponent.prototype.changedPager = function (event) {
        if (this._userobj.totalCount != 0) {
            this._pgobj = event;
            var current = this._userobj.currentPage;
            var size = this._userobj.pageSize;
            this._userobj.currentPage = this._pgobj.current;
            this._userobj.pageSize = this._pgobj.size;
            if (this._pgobj.current != current || this._pgobj.size != size)
                this.search();
        }
    };
    FrmUserListComponent.prototype.search = function () {
        var _this = this;
        try {
            this._mflag = false;
            this.http.doGet(this.ics._apiurl + 'service001/getUserListing?searchVal=' + encodeURIComponent(this._userobj.searchText) + '&pagesize=' + this._userobj.pageSize + '&currentpage=' + this._userobj.currentPage + '&operation=allbankuser' + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (response) {
                if (response.msgCode == '0016') {
                    _this._sessionMsg = response.msgDesc;
                    _this.showMessage();
                }
                if (response.data != null && response.data != undefined) {
                    if (!(response.data instanceof Array)) {
                        var m = [];
                        m[0] = response.data;
                        _this._userobj.data = m;
                        _this._userobj.totalCount = response.totalCount;
                    }
                    else {
                        _this._userobj = response;
                        if (response.totalCount == 0) {
                            _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Data not found!" });
                        }
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmUserListComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Observable_1.Observable.timer(3000).subscribe(function (x) {
            _this._router.navigate(['Login', , { p1: '*' }]);
            jQuery("#sessionalert").modal('hide');
        });
    };
    FrmUserListComponent.prototype.Searching = function () {
        this._userobj.currentPage = 1;
        this.search();
    };
    FrmUserListComponent.prototype.searchKeyup = function (e) {
        if (e.which == 13) {
            this._userobj.currentPage = 1;
            this.search();
        }
    };
    FrmUserListComponent.prototype.goNew = function () {
        this._router.navigate(['User Setup', , { cmd: "NEW" }]);
    };
    FrmUserListComponent.prototype.goto = function (p) {
        this._router.navigate(['/User Setup', 'read', p]);
    };
    FrmUserListComponent = __decorate([
        core_1.Component({
            selector: 'FrmUserList',
            template: " \n    <div class=\"container\">\n    <div class=\"row clearfix\">\n        <form class=\"form-inline\"> \n        <!-- Form Name -->\n         <legend>Users</legend>\n            <div  class=\"input-group\">\n                <span class=\"input-group-btn input-md\"  style=\"width:20px;\">\n                    <button class=\"btn btn-primary\" type=\"button\" (click)=\"goNew();\">New</button>\n                </span> \n                <input id=\"textinput\" name=\"textinput\" type=\"text\"  placeholder=\"Search\" [(ngModel)]=\"_userobj.searchText\" (keyup)=\"searchKeyup($event)\" maxlength=\"50\" class=\"form-control input-md\">\n                <span class=\"input-group-btn input-md\">\n                    <button class=\"btn btn-primary input-md\" type=\"button\" (click)=\"Searching()\" >\n                        <span class=\"glyphicon glyphicon-search\"></span>Search\n                    </button>\n                </span>        \n         </div>  \n         \n         <div style = \"margin-top : 10px\">\n             <adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"_userobj.totalCount\" (rpChanged)=\"changedPager($event)\" ></adminpager> \n         </div>\n\n         <div [hidden]=\"_recordhide\">\n         <table class=\"table table-striped table-condensed table-hover tblborder\" id=\"tblstyle\">\n            <thead>\n                <tr>\n                    <th>ID</th>\n                    <th>Name</th>\n                    <th>Status</th>  \n                    <th>User Module</th>        \n                </tr>\n            </thead>\n            <tbody>\n                <tr *ngFor=\"let obj of _userobj.data\">\n                <td><a (click)=\"goto(obj.syskey)\">{{obj.t1}}</a></td>\n                <td>{{obj.username}}</td>\n                <td *ngIf=\"obj.recordStatus == 1 \" >Save</td>\n                <td *ngIf=\"obj.recordStatus == 2 && obj.n7 == 0 \" >Activate</td>\n                <td *ngIf=\"obj.recordStatus == 21 \" >Deactivate</td>\n                <td *ngIf=\"obj.recordStatus == 2 && obj.n7 == 11 \" >Lock</td>\n                \n                <td *ngIf=\"obj.n2 == 2 \" >Admin User</td>\n                <td *ngIf=\"obj.n2 == 3 \" >Merchant User</td>\n                </tr>  \n            </tbody>\n        </table>\n        </div>\n    </form>\n    </div>\n\n    <div [hidden] = \"_mflag\">\n    <div class=\"modal\" id=\"loader\"></div>\n  </div>\n  </div> \n         "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_http_service_1.RpHttpService])
    ], FrmUserListComponent);
    return FrmUserListComponent;
}());
exports.FrmUserListComponent = FrmUserListComponent;
//# sourceMappingURL=frmuser-list.component.js.map