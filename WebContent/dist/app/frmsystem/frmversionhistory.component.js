"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
core_2.enableProdMode();
var FrmVersionHistoryComponent = (function () {
    function FrmVersionHistoryComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._btntext = "";
        this._output1 = "";
        this._message = "";
        this._key = "";
        this._versionKey = "";
        this._mflag = false;
        // to get general methods
        this._util = new rp_client_util_1.ClientUtil();
        // about date picker
        this.today = this._util.getTodayDate();
        this.myDatePickerOptions = this._util.getDatePicker();
        this._dates = { "startdate": this.dateobj, "duedate": this.dateobj };
        //about default obj
        this._obj = this.getDefaultObj();
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this.checkSession();
            this._btntext = "Save";
            jQuery("#vh_delete_btn").prop("disabled", true);
            this._obj = this.getDefaultObj();
            this.setTodayDateObj();
            this.setDateObjIntoString();
            this.getAppCodes();
            this.getStatusCodes();
        }
    }
    FrmVersionHistoryComponent.prototype.getDefaultObj = function () {
        return {
            "sessionID": "", "userID": "", "msgCode": "", "msgDesc": "", "state": "",
            "autokey": "", "appcode": "", "versionkey": "", "version": "", "versiontitle": "",
            "description": "", "startdate": "", "duedate": "", "remark": "", "status": "",
            "t1": "", "t2": "", "t3": "", "n1": "", "n2": "", "n3": ""
        };
    };
    FrmVersionHistoryComponent.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    FrmVersionHistoryComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsgAlert(data.desc);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsgAlert(data.desc);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmVersionHistoryComponent.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmVersionHistoryComponent.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    FrmVersionHistoryComponent.prototype.setTodayDateObj = function () {
        this._dates.startdate = this._util.changestringtodateobject(this.today);
        this._dates.duedate = this._util.changestringtodateobject(this.today);
    };
    FrmVersionHistoryComponent.prototype.setDateObjIntoString = function () {
        this._obj.startdate = this._util.getDatePickerDate(this._dates.startdate);
        this._obj.duedate = this._util.getDatePickerDate(this._dates.duedate);
    };
    FrmVersionHistoryComponent.prototype.getAppCodes = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/getAppCodesForVersionHistory';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.msgCode == "0016") {
                        _this.showMsgAlert(data.msgDesc);
                        _this.logout();
                    }
                    if (data.msgCode != "0000") {
                        _this.showMsgAlert(data.msgDesc);
                    }
                    if (data.msgCode == "0000") {
                        if (!(data.refAppCode instanceof Array)) {
                            var m = [];
                            m[0] = data.refAppCode;
                            _this.ref._lov3.refAppCode = m;
                        }
                        else {
                            _this.ref._lov3.refAppCode = data.refAppCode;
                        }
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmVersionHistoryComponent.prototype.getStatusCodes = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/getStatusCodesForVersionHistory';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                _this._mflag = true;
                if (data != null) {
                    if (data.msgCode == "0016") {
                        _this.showMsgAlert(data.msgDesc);
                        _this.logout();
                    }
                    if (data.msgCode != "0000") {
                        _this.showMsgAlert(data.msgDesc);
                    }
                    if (data.msgCode == "0000") {
                        if (!(data.refVersionStatusCode instanceof Array)) {
                            var m = [];
                            m[0] = data.refVersionStatusCode;
                            _this.ref._lov3.refVersionStatusCode = m;
                        }
                        else {
                            _this.ref._lov3.refVersionStatusCode = data.refVersionStatusCode;
                        }
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmVersionHistoryComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
                _this._mflag = false;
                //this._btntext = "Update";
                jQuery("#vh_delete_btn").prop("disabled", false);
                var id = params['id'];
                _this._key = id;
                _this.goReadById(_this._key);
            }
        });
    };
    FrmVersionHistoryComponent.prototype.goReadById = function (_autokeyid) {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/getOneVersionHistoryByAutoKey';
            this._obj.sessionID = this.ics._profile.sessionID;
            this._obj.userID = this.ics._profile.userID;
            this._obj.autokey = _autokeyid;
            var json = this._obj;
            this.http.doPost(url, json).subscribe(function (data) {
                _this._mflag = true;
                var temp = _this._obj;
                if (data != null) {
                    _this._obj = data;
                    if (_this._obj.msgCode == "0016") {
                        _this.showMsgAlert(_this._obj.msgDesc);
                        _this.logout();
                    }
                    if (_this._obj.msgCode != "0000") {
                        _this.showMsgAlert(_this._obj.msgDesc);
                        _this._obj = temp;
                    }
                    if (_this._obj.msgCode == "0000") {
                        jQuery("#vh_delete_btn").prop("disabled", false);
                        //this._btntext = "Update";
                        _this._dates.startdate = _this._util.changestringtodateobject(_this._obj.startdate);
                        _this._dates.duedate = _this._util.changestringtodateobject(_this._obj.duedate);
                        _this.getAppCodes();
                        _this.getStatusCodes();
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmVersionHistoryComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    FrmVersionHistoryComponent.prototype.goSave = function () {
        var _this = this;
        this.setDateObjIntoString();
        if (this._obj.appcode == "undefined" || this._obj.appcode.trim() == "") {
            this.showMsgAlert("Blank App Code.");
        }
        else if (this._obj.version == "undefined" || this._obj.version.trim() == "") {
            this.showMsgAlert("Blank Version.");
        }
        else if (this._obj.version.trim().length > 20) {
            this.showMsgAlert("Invalid Version. Its Maximum Character Is 20.");
        }
        else if (this._obj.versiontitle.trim().length > 50) {
            this.showMsgAlert("Invalid Version Title. Its Maximum Character Is 50.");
        }
        else if (this._obj.description.trim().length > 500) {
            this.showMsgAlert("Invalid Description. Its Maximum Character is 500.");
        }
        else if (this._obj.startdate == "undefined" || this._obj.startdate.trim() == "") {
            this.showMsgAlert("Blank Start Date.");
        }
        else if (this._obj.startdate.trim().length != 0 && !/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/.test(this._obj.startdate)) {
            this.showMsgAlert("Invalid Start Date.");
        }
        else if (this._obj.duedate == "undefined" || this._obj.duedate.trim() == "") {
            this.showMsgAlert("Blank Due Date.");
        }
        else if (this._obj.duedate.trim().length != 0 && !/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/.test(this._obj.duedate)) {
            this.showMsgAlert("Invalid Due Date.");
        }
        else if (this._obj.status == "undefined" || this._obj.status.trim() == "") {
            this.showMsgAlert("Blank Status.");
        }
        else {
            this._mflag = false;
            try {
                var url = this.ics._apiurl + 'service001/saveVersionHistory';
                this._obj.sessionID = this.ics._profile.sessionID;
                this._obj.userID = this.ics._profile.userID;
                var json = this._obj;
                this.http.doPost(url, json).subscribe(function (data) {
                    _this._mflag = true;
                    var temp = _this._obj;
                    _this._output1 = JSON.stringify(data);
                    if (data != null) {
                        _this._obj = data;
                        if (_this._obj.msgCode == "0016") {
                            _this.showMsgAlert(_this._obj.msgDesc);
                            _this.logout();
                        }
                        if (_this._obj.msgCode != "0000") {
                            _this.showMsgAlert(_this._obj.msgDesc);
                            _this._obj = temp;
                        }
                        if (_this._obj.msgCode == "0000") {
                            jQuery("#vh_delete_btn").prop("disabled", false);
                            //this._btntext = "Update";
                            _this._key = _this._obj.autokey;
                            _this._versionKey = _this._obj.versionkey;
                            _this._message = _this._obj.msgDesc;
                            _this._obj = temp;
                            _this._obj.autokey = _this._key;
                            _this._obj.versionkey = _this._versionKey;
                            _this.showMsgAlert(_this._message);
                        }
                    }
                }, function (error) {
                    if (error._body.type == "error") {
                        alert("Connection Timed Out.");
                    }
                }, function () { });
            }
            catch (e) {
                alert("Invalid URL.");
            }
        }
    };
    FrmVersionHistoryComponent.prototype.goList = function () {
        this._router.navigate(['/version-history-list']);
    };
    FrmVersionHistoryComponent.prototype.goNew = function () {
        this._mflag = false;
        this._btntext = "Save";
        jQuery("#vh_delete_btn").prop("disabled", true);
        this.clear();
        this.getAppCodes();
        this.getStatusCodes();
    };
    FrmVersionHistoryComponent.prototype.clear = function () {
        this._output1 = "";
        this._message = "";
        this._key = "";
        this._versionKey = "";
        this._obj = this.getDefaultObj();
        this.today = this._util.getTodayDate();
        this.setTodayDateObj();
        this.setDateObjIntoString();
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
    };
    FrmVersionHistoryComponent.prototype.goDelete = function () {
        var _this = this;
        this._mflag = false;
        try {
            var url = this.ics._apiurl + 'service001/deleteVersionHistory';
            this._obj.sessionID = this.ics._profile.sessionID;
            this._obj.userID = this.ics._profile.userID;
            var json = this._obj;
            this.http.doPost(url, json).subscribe(function (data) {
                _this._mflag = true;
                var temp = _this._obj;
                _this._output1 = JSON.stringify(data);
                if (data != null) {
                    _this._obj = data;
                    if (_this._obj.msgCode == "0016") {
                        _this.showMsgAlert(_this._obj.msgDesc);
                        _this.logout();
                    }
                    if (_this._obj.msgCode != "0000") {
                        _this.showMsgAlert(_this._obj.msgDesc);
                        _this._obj = temp;
                    }
                    if (_this._obj.msgCode == "0000") {
                        _this.showMsgAlert(_this._obj.msgDesc);
                        _this.goNew();
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmVersionHistoryComponent = __decorate([
        core_1.Component({
            selector: 'version-history',
            template: "\n    <div class=\"container\">\n      <div class=\"row clearfix\">\n        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n          <form class=\"form-horizontal\" (ngSubmit)=\"goSave()\">\n          <!-- Form Name -->\n            <legend>Version History</legend>\n\n            <div class=\"row col-md-12\">\n                <button class=\"btn btn-primary\" id=\"vh_list_btn\" type=\"button\" (click)=\"goList()\">List</button>\n                <button class=\"btn btn-primary\" id=\"vh_new_btn\" type=\"button\" (click)=\"goNew()\">New</button>\n                <button class=\"btn btn-primary\" id=\"vh_save_btn\" type=\"submit\">{{_btntext}}</button>\n                <button class=\"btn btn-primary\" disabled id=\"vh_delete_btn\" type=\"button\" (click)=\"goDelete()\">Delete</button>\n            </div>\n\n            <div class=\"row col-md-12\">&nbsp;</div>\n\n            <div class=\"row col-md-12\" id=\"custom-form-alignment-margin\">\n              <div class=\"col-md-6\">\n                <div class=\"form-group\">\n                  <label class=\"col-md-4\">App Code <font size=\"4\" color=\"#FF0000\">*</font></label>\n                  <div class=\"col-md-8\">\n                      <select *ngIf=\"_key==''\" [(ngModel)]=\"_obj.appcode\" class=\"form-control col-md-0\" [ngModelOptions]=\"{standalone: true}\" required>\n                        <option *ngFor=\"let item of ref._lov3.refAppCode\" value=\"{{item.value}}\">{{item.caption}}</option>\n                      </select>\n\n                      <select *ngIf=\"_key!=''\" [(ngModel)]=\"_obj.appcode\" class=\"form-control col-md-0\" [ngModelOptions]=\"{standalone: true}\" disabled>\n                        <option *ngFor=\"let item of ref._lov3.refAppCode\" value=\"{{item.value}}\">{{item.caption}}</option>\n                      </select>\n                  </div>\n                </div>\n\n                <div class=\"form-group\">\n                    <label class=\"col-md-4\">Version Key </label>\n                    <div class=\"col-md-8\">\n                        <input disabled readonly type=\"text\" [(ngModel)]=\"_obj.versionkey\" class=\"form-control\" [ngModelOptions]=\"{standalone: true}\">\n                    </div>\n                </div>\n\n                <div class=\"form-group\">\n                    <label class=\"col-md-4\">Version <font size=\"4\" color=\"#FF0000\">*</font></label>\n                    <div class=\"col-md-8\">\n                        <input class=\"form-control\" type=\"text\" [(ngModel)]=\"_obj.version\" required=\"true\" maxlength=\"20\" [ngModelOptions]=\"{standalone: true}\">\n                    </div>\n                </div>\n\n                <div class=\"form-group\">\n                    <label class=\"col-md-4\">Version Title </label>\n                    <div class=\"col-md-8\">\n                        <input class=\"form-control\" type=\"text\" [(ngModel)]=\"_obj.versiontitle\" maxlength=\"50\" [ngModelOptions]=\"{standalone: true}\">\n                    </div>\n                </div>\n                \n                <div class=\"form-group\">\n                    <label class=\"col-md-4\">Description </label>\n                    <div class=\"col-md-8\">\n                        <textarea class=\"form-control\" rows=\"3\" [(ngModel)]=\"_obj.description\" maxlength=\"500\" [ngModelOptions]=\"{standalone: true}\"></textarea>\n                    </div>\n                </div>\n              </div>\n              <div class=\"col-md-6\">\n                <div class=\"form-group\">\n                    <label class=\"col-md-4\">Start Date <font size=\"4\" color=\"#FF0000\">*</font></label>\n                    <div class=\"col-md-8\">\n                      <my-date-picker name=\"mystartdate\" [options]=\"myDatePickerOptions\" [(ngModel)]=\"_dates.startdate\" ngDefaultControl></my-date-picker>\n                    </div>\n                </div>\n\n                <div class=\"form-group\">\n                    <label class=\"col-md-4\">Due Date <font size=\"4\" color=\"#FF0000\">*</font></label>\n                    <div class=\"col-md-8\">\n                      <my-date-picker name=\"myduedate\" [options]=\"myDatePickerOptions\" [(ngModel)]=\"_dates.duedate\" ngDefaultControl></my-date-picker>\n                    </div>\n                </div>\n\n                <div class=\"form-group\">\n                    <label class=\"col-md-4\">Remark </label>\n                    <div class=\"col-md-8\">\n                        <textarea id=\"textarea-custom\" class=\"form-control\" rows=\"3\" [(ngModel)]=\"_obj.remark\" [ngModelOptions]=\"{standalone: true}\"></textarea>\n                    </div>\n                </div>\n\n                <div class=\"form-group\">\n                    <label class=\"col-md-4\">Status <font size=\"4\" color=\"#FF0000\">*</font></label>\n                    <div class=\"col-md-8\">\n                        <select [(ngModel)]=\"_obj.status\" class=\"form-control col-md-0\" [ngModelOptions]=\"{standalone: true}\" required>\n                          <option *ngFor=\"let item of ref._lov3.refVersionStatusCode\" value=\"{{item.value}}\">{{item.caption}}</option>\n                        </select>\n                    </div>\n                </div>\n              </div>\n            </div>\n          </form>\n        </div>\n      </div>\n    </div>\n\n    <div [hidden]=\"_mflag\">\n      <div class=\"modal\" id=\"loader\"></div>\n    </div>\n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmVersionHistoryComponent);
    return FrmVersionHistoryComponent;
}());
exports.FrmVersionHistoryComponent = FrmVersionHistoryComponent;
//# sourceMappingURL=frmversionhistory.component.js.map