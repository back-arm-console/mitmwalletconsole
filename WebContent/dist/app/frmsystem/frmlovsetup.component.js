"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// RP Framework
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var Rx_1 = require('rxjs/Rx');
core_2.enableProdMode();
var FrmLovSetupComponent = (function () {
    function FrmLovSetupComponent(ics, _router, l_util, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.l_util = l_util;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._classname = "";
        this._timer = 0;
        this._mflag = false;
        this._returnResult = { "keyResult": 0, "longResult": "", "state": "", "stringResult": "", "userId": "", "msgCode": "", "msgDesc": "" };
        this._lovsetupobj = this.getDefaultObj();
        this.sessionAlertMsg = "";
        this._key = "";
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        // RP Framework 
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = true;
            this.checkSession();
            this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "", "msgCode": "" };
            this._lovsetupobj.createdDate = this.l_util.getTodayDate();
            this._lovsetupobj.modiDate = this.l_util.getTodayDate();
        }
    }
    FrmLovSetupComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this._key = id;
                _this.goPostBySysKey(id);
            }
        });
    };
    FrmLovSetupComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    FrmLovSetupComponent.prototype.getDefaultObj = function () {
        return { "sysKey": 0, "lovNo": "TBA", "createdDate": "", "modiDate": "", "userID": "User1", "recStatus": "0", "lovDesc2": "", "lovType": "singleselect", "data": [{ "srno": "1", "lovCde": "", "lovDesc1": "", "price": 0.0 }], "messageCode": "", "messageDesc": "" };
    };
    /* checkSession() {
      try {
        let url: string = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "TBA", "msgCode": "" };
        this.http.doGet(url).subscribe(
          data => {
            if (data.msgCode == '0016') {
              this.sessionAlertMsg = data.msgDesc;
              this.showMessage();
            }
          },
          () => { });
      } catch (e) {
        alert("Invalid URL");
      }
  
    } */
    FrmLovSetupComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this.goLogOut();
        });
    };
    FrmLovSetupComponent.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    FrmLovSetupComponent.prototype.goNew = function () {
        console.log("In goNew ");
        this._lovsetupobj = this.getDefaultObj();
        this._lovsetupobj.createdDate = this.l_util.getTodayDate();
        this._lovsetupobj.modiDate = this.l_util.getTodayDate();
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "", "msgCode": "" };
    };
    FrmLovSetupComponent.prototype.goSave = function () {
        var _this = this;
        this._mflag = false;
        var url = this.ics._apiurl + 'service001/savelovSetup';
        var json = this._lovsetupobj;
        this.http.doPost(url, json).subscribe(function (res) {
            if (res != null) {
                _this._timer = 3000;
                if (res.messageCode == "0000" || res.messageCode == "0001") {
                    _this._returnResult.state = "true";
                }
                else {
                    _this._returnResult.state = "false";
                }
                _this.showMessageAlert(res.messageDesc);
                _this._lovsetupobj.lovNo = res.lovNo;
                _this._lovsetupobj.sysKey = res.sysKey;
            }
            _this._mflag = true;
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Error!");
            }
        }, function () { });
    };
    FrmLovSetupComponent.prototype.goList = function () {
        this._router.navigate(['/lovsetuplist']);
    };
    FrmLovSetupComponent.prototype.goPostBySysKey = function (k) {
        var _this = this;
        var url = this.ics._apiurl + 'service001/getLOVbySysKey';
        this._lovsetupobj.sysKey = k;
        var json = this._lovsetupobj;
        this.http.doPost(url, json).subscribe(function (res) {
            if (res != null) {
                _this._lovsetupobj = res;
                if (!(res.data instanceof Array)) {
                    var m = [];
                    m[0] = res.data;
                    _this._lovsetupobj.data = m;
                }
                _this.goEnable();
                _this._mflag = true;
            }
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Error!");
            }
        }, function () { });
    };
    FrmLovSetupComponent.prototype.goRemove = function (p) {
        var index = this._lovsetupobj.data.indexOf(p);
        var length = this._lovsetupobj.data.length;
        if (length < 2) {
            this._lovsetupobj.data[0] = { srno: ("").toString(), lovCde: "", lovDesc1: "", "price": 0.0 };
        }
        else {
            this._lovsetupobj.data.splice(index, 1);
        }
        for (var i = 0; i < length; i++) {
            var maxsrno = i;
            maxsrno = maxsrno + 1;
            this._lovsetupobj.data[i].srno = (maxsrno).toString();
        }
    };
    FrmLovSetupComponent.prototype.goAddLovsetup = function () {
        if (this._lovsetupobj.data[0].srno == "") {
            this._lovsetupobj.data[0].srno = "1";
        }
        var maxsrno = this._lovsetupobj.data.length;
        maxsrno = maxsrno + 1;
        this._lovsetupobj.data.push({ srno: (maxsrno).toString(), lovCde: "", lovDesc1: "", "price": 0.0 });
    };
    FrmLovSetupComponent.prototype.goEnable = function () {
        jQuery("#mycopybtn").prop("disabled", false);
    };
    FrmLovSetupComponent.prototype.showMessageAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmLovSetupComponent.prototype.goCopy = function () {
        this._lovsetupobj.lovNo = "TBA";
        this._lovsetupobj.lovDesc2 = "";
    };
    FrmLovSetupComponent.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    FrmLovSetupComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsgAlert(data.desc);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsgAlert(data.desc);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmLovSetupComponent.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmLovSetupComponent.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    FrmLovSetupComponent = __decorate([
        core_1.Component({
            selector: 'frmlovsetup',
            template: "\n  <div class=\"container\">\n\t  <div class=\"row clearfix\">\n\t\t  <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n\t\t\t  <form class= \"form-horizontal\" (ngSubmit) = \"goSave()\">  \n          <legend>List of Values</legend>\n          \n\t\t\t\t\t<div class=\"form-group\"> \n\t\t\t\t\t\t<div class=\"col-md-5\">\n\t\t\t\t\t\t  <button type=\"button\" class=\"btn btn-primary\" (click)=\"goList();\">List</button>\n\t\t\t\t\t\t  <button type=\"button\" class=\"btn btn-primary\" (click)=\"goNew()\">New</button>\n\t\t\t\t\t\t  <button type=\"submit\" class=\"btn btn-primary\">Save</button>            \n\t\t\t\t\t\t  <button id=\"mycopybtn\" class=\"btn btn-primary\" type=\"button\" (click)=\"goCopy();\" disabled>Copy to</button>            \n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\n\t\t\t\t\t<div class=\"col-md-12\">&nbsp;</div>\n\n\t\t\t\t\t<div class=\"col-md-12\" id=\"custom-form-alignment-margin\">\n\t\t\t\t\t\t<div class=\"col-md-6\">\n\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t<rp-input rpType=\"text\" rpClass=\"col-md-8\" rpLabelClass =\"col-md-4 control-label\" rpLabel=\"Code\" rpPlaceHolder = \"TBA\"  [(rpModel)]=\"_lovsetupobj.lovNo\" rpReadonly = \"true\">TBA</rp-input>         \n\t\t\t\t\t\t\t</div>         \n\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t<rp-input rpType=\"text\" rpClass=\"col-md-8\" rpLabelClass =\"col-md-4 control-label\" rpLabel=\"Description\"  [(rpModel)]=\"_lovsetupobj.lovDesc2\" rpRequired = \"true\"  ></rp-input>                            \n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-md-12\">&nbsp;</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div calss = \"row col-md-10\">\n\t\t\t\t\t\t\t\t<table class=\"table table-striped table-condensed table-hover tblborder\" style=\"font-size:14px;margin-left:30px;\">\n\t\t\t\t\t\t\t\t\t<thead>\n\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t  <th>No.</th>\n\t\t\t\t\t\t\t\t\t\t  <th>Value</th>\n\t\t\t\t\t\t\t\t\t\t  <th>Description</th>\n\t\t\t\t\t\t\t\t\t\t  <th>Price</th>\n\t\t\t\t\t\t\t\t\t\t  <th>&nbsp;</th>\n\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t</thead>\n\t\t\t\t\t\t\t\t\t<tbody>\n\t\t\t\t\t\t\t\t\t\t<tr *ngFor=\"let obj of _lovsetupobj.data\">\n\t\t\t\t\t\t\t\t\t\t\t<td style=\"vertical-align:middle;text-align:right;\" class=\"col-md-1\">{{obj.srno}}</td>\n\t\t\t\t\t\t\t\t\t\t\t<td class=\"col-md-3\">\n\t\t\t\t\t\t\t\t\t\t\t\t<rp-input rpRequired = \"true\" [(rpModel)]=\"obj.lovCde\" rpType=\"text\"  rpClass = \"col-md-0\"></rp-input>\n\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t<rp-input rpRequired = \"true\" [(rpModel)]=\"obj.lovDesc1\" rpType=\"text\"  rpClass = \"col-md-0\"></rp-input>\n\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t<td class=\"col-md-2\">\n                       <input id=\"numinput\"  class=\"form-control\" type = \"number\" [(ngModel)]=\"obj.price\" [ngModelOptions]=\"{standalone: true}\" required>                    \n\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t<td><img src=\"image/remove.png\" alt=\"remove.png\" height=\"20\" width=\"20\"  (click)=\"goRemove(obj)\"/></td>\n\t\t\t\t\t\t\t\t\t\t</tr> \n\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t<td>             \n\t\t\t\t\t\t\t\t\t\t\t   <button class=\"btn btn-primary\" type=\"button\" (click)=\"goAddLovsetup()\">Add</button>              \n\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t</tbody>\n\t\t\t\t\t\t\t\t</table>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n      \n          <div class=\"form-group\"> \n            <div class=\"col-md-12\">\n              <button type=\"button\" class=\"btn btn-primary btn-md\" (click)=\"goList();\">List</button> \n              <button type=\"button\" class=\"btn btn-primary btn-md\" (click)=\"goNew()\">New</button>\n              <button type=\"submit\" class=\"btn btn-primary btn-md\">Save</button>\n              <button id=\"mycopybtn\" class=\"btn btn-primary\" type=\"button\" (click)=\"goCopy();\" disabled>Copy to</button>\n            </div>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n\n  <div id=\"sessionalert\" class=\"modal fade\">\n    <div class=\"modal-dialog\">\n      <div class=\"modal-content\">\n        <div class=\"modal-body\">\n          <p>{{sessionAlertMsg}}</p>\n        </div>\n        <div class=\"modal-footer\">\n        </div>\n      </div>\n    </div>\n  </div>\n  \n  <div [hidden] = \"_mflag\">\n    <div class=\"modal\" id=\"loader\"></div>\n  </div>\n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_client_util_1.ClientUtil, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmLovSetupComponent);
    return FrmLovSetupComponent;
}());
exports.FrmLovSetupComponent = FrmLovSetupComponent;
//# sourceMappingURL=frmlovsetup.component.js.map