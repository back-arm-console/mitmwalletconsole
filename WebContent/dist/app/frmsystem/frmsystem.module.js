"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var common_1 = require('@angular/common');
var rp_input_module_1 = require('../framework/rp-input.module');
var mydatepicker_1 = require('mydatepicker');
var rp_http_service_1 = require('../framework/rp-http.service');
var adminpager_module_1 = require('../util/adminpager.module');
var pager_module_1 = require('../util/pager.module');
var frmsystem_routing_1 = require('./frmsystem.routing');
var frmusersetup_component_1 = require('./frmusersetup.component');
var frmuser_list_component_1 = require('./frmuser-list.component');
var frmpasswordpolicysetup_component_1 = require('./frmpasswordpolicysetup.component');
var frmforcesignout_component_1 = require('./frmforcesignout.component');
var frmmenusetup_component_1 = require('./frmmenusetup.component');
var frmmenusteup_list_component_1 = require('./frmmenusteup-list.component');
var frmrolesetup_component_1 = require('./frmrolesetup.component');
var frmrolesetup_list_component_1 = require('./frmrolesetup-list.component');
var frmversionhistory_component_1 = require('./frmversionhistory.component');
var frmversionhistory_list_component_1 = require('./frmversionhistory-list.component');
var frmlovsetup_component_1 = require('./frmlovsetup.component');
var frmlovsetup_list_component_1 = require('./frmlovsetup-list.component');
var FrmSystemModule = (function () {
    function FrmSystemModule() {
    }
    FrmSystemModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                forms_1.FormsModule,
                rp_input_module_1.RpInputModule,
                adminpager_module_1.AdminPagerModule,
                pager_module_1.PagerModule,
                mydatepicker_1.MyDatePickerModule,
                frmsystem_routing_1.FrmSystemRouting,
            ],
            exports: [],
            declarations: [
                frmusersetup_component_1.FrmUserSetupComponent,
                frmuser_list_component_1.FrmUserListComponent,
                frmpasswordpolicysetup_component_1.FrmPasswordPolicyComponent,
                frmforcesignout_component_1.FrmForceSignOutComponent,
                frmmenusetup_component_1.FrmMenuSetupComponent,
                frmmenusteup_list_component_1.FrmMenuSetupListComponent,
                frmrolesetup_component_1.FrmRoleSetupComponent,
                frmrolesetup_list_component_1.FrmRoleSetupListComponent,
                frmversionhistory_component_1.FrmVersionHistoryComponent,
                frmversionhistory_list_component_1.FrmVersionHistoryListComponent,
                frmlovsetup_component_1.FrmLovSetupComponent,
                frmlovsetup_list_component_1.FrmLOVSetupListComponent,
            ],
            providers: [
                rp_http_service_1.RpHttpService
            ],
            schemas: [core_1.CUSTOM_ELEMENTS_SCHEMA]
        }), 
        __metadata('design:paramtypes', [])
    ], FrmSystemModule);
    return FrmSystemModule;
}());
exports.FrmSystemModule = FrmSystemModule;
//# sourceMappingURL=frmsystem.module.js.map