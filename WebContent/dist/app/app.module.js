"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var platform_browser_1 = require('@angular/platform-browser');
var forms_1 = require('@angular/forms');
var http_1 = require('@angular/http');
var common_2 = require('@angular/common');
var mydatepicker_1 = require('mydatepicker');
var rp_root_component_1 = require('./rp-root.component');
var app_routing_1 = require('./app.routing');
//import { SystemModule } from './system/rp-system.module';
var rp_input_module_1 = require('./framework/rp-input.module');
var pager_module_1 = require('./util/pager.module');
var adminpager_module_1 = require('./util/adminpager.module');
var advancedsearch_module_1 = require('./util/advancedsearch.module');
var rp_login_component_1 = require('./rp-login.component');
var rp_http_service_1 = require('./framework/rp-http.service');
var rp_intercom_service_1 = require('./framework/rp-intercom.service');
var rp_menu_component_1 = require('./framework/rp-menu.component');
var rp_references_1 = require('./framework/rp-references');
var rp_client_util_1 = require('./util/rp-client.util');
var ngx_tabs_1 = require("ngx-tabs");
var frm000_module_1 = require('./frm000/frm000.module');
var administration_module_1 = require('./administration/administration.module');
var settings_module_1 = require('./settings/settings.module');
var system_module_1 = require('./system/system.module');
var report_module_1 = require('./report/report.module');
var GeoLocationService_1 = require('./framework/GeoLocationService');
var core_2 = require('angular2-cookie/core');
var wallet_module_1 = require('./wallet/wallet.module');
var dashboard_module_1 = require('./dashboard/dashboard.module');
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                forms_1.FormsModule,
                http_1.HttpModule,
                app_routing_1.routing,
                settings_module_1.settingsModule,
                rp_input_module_1.RpInputModule,
                pager_module_1.PagerModule,
                adminpager_module_1.AdminPagerModule,
                advancedsearch_module_1.AdvancedSearchModule,
                mydatepicker_1.MyDatePickerModule,
                ngx_tabs_1.TabsModule,
                frm000_module_1.Frm000Module,
                administration_module_1.administrationModule,
                system_module_1.SystemModule,
                report_module_1.reportModule,
                wallet_module_1.walletModule,
                dashboard_module_1.dashBoardModule
            ],
            declarations: [
                rp_root_component_1.RpRootComponent,
                rp_login_component_1.RpLoginComponent,
                rp_menu_component_1.RpMenuComponent
            ],
            providers: [
                rp_http_service_1.RpHttpService,
                rp_intercom_service_1.RpIntercomService,
                rp_references_1.RpReferences,
                rp_client_util_1.ClientUtil,
                GeoLocationService_1.GeoLocationService,
                core_2.CookieService,
                { provide: common_1.APP_BASE_HREF, useValue: '/' },
                { provide: common_2.LocationStrategy, useClass: common_2.HashLocationStrategy }
            ],
            schemas: [
                core_1.CUSTOM_ELEMENTS_SCHEMA
            ],
            bootstrap: [rp_root_component_1.RpRootComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map