"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var Rx_1 = require('rxjs/Rx');
core_2.enableProdMode();
var FrmVoidReportComponent = (function () {
    function FrmVoidReportComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._obj = this.getDefaultObj();
        this._mflag = false;
        this._totalcount = 1;
        this._sessionMsg = "";
        this._entryhide = true;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "" };
        this._pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 1 };
        this._list = {
            "data": [{
                    "syskey": 0,
                    "meterID": "", "scheduleName": "", "status": "", "remark": ""
                }],
            "totalCount": 0, "currentPage": 1, "pageSize": 10, "msgCode": "", "msgDesc": ""
        };
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = true;
            this._entryhide = true;
        }
    }
    FrmVoidReportComponent.prototype.getDefaultObj = function () {
        return {
            "sessionID": "", "fromMeterid": "", "toMeterid": "", "name": "", "buildingName": "", "msgCode": "", "msgDesc": "",
            "select": "EXCEL", "totalCount": 0, "currentPage": 1, "pageSize": 10, "msgstatus": "",
        };
    };
    FrmVoidReportComponent.prototype.Validation = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
            this.http.doGet(url).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this._sessionMsg = data.msgDesc;
                    _this.showMessage();
                }
                else {
                    _this.goDownload();
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmVoidReportComponent.prototype.goDownload = function () {
        var templateData = "";
        var formatJSP = "";
        formatJSP = "void.jsp";
        this.ics.sendBean({ "t1": "rp-msg", t2: "Information ", t3: "Please wait ..." });
        var url = this.ics._rpturl + formatJSP + "?aFromMeterID=" + this._obj.fromMeterid + "&" + "aToMeterID=" + this._obj.toMeterid + "&aSelect=" + this._obj.select;
        this.ics.sendBean({ "t1": "rp-popup", "t2": "Information", "t3": url });
    };
    FrmVoidReportComponent.prototype.changeSelectVal = function (options) {
        this.selectedOptions = Array.apply(null, options) // convert to real array
            .filter(function (option) { return option.selected; })
            .map(function (option) { return option.value; });
        this._obj.select = this.selectedOptions[0];
    };
    FrmVoidReportComponent.prototype.goClear = function () {
        this._entryhide = true;
        this._obj = this.getDefaultObj();
    };
    FrmVoidReportComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this._router.navigate(['Login', , { p1: '*' }]);
            jQuery("#sessionalert").modal('hide');
        });
    };
    FrmVoidReportComponent.prototype.changedPager = function (event) {
        this._pgobj = event;
        this._obj.currentPage = this._pgobj.current;
        this._obj.pageSize = this._pgobj.size;
        this._obj.totalCount = this._pgobj.totalcount;
        if (this._pgobj.totalcount > 1) {
            this.goList();
        }
    };
    FrmVoidReportComponent.prototype.goList = function () {
        var _this = this;
        this._mflag = false;
        this._obj.sessionID = this.ics._profile.sessionID;
        var url = this.ics._apiurl + 'service001/getMeterIDList?sessionID=' + this._obj.sessionID + '&fromMeterid=' + this._obj.fromMeterid + '&toMeterid=' + this._obj.toMeterid + '&pagesize=' + this._obj.pageSize + '&currentpage=' + this._obj.currentPage;
        var capValue = "";
        var json = this._obj;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data.msgCode == '0016') {
                _this._sessionMsg = data.msgDesc;
                _this._returnResult.msgDesc = "";
                _this.showMessage();
            }
            else {
                _this._entryhide = false;
                _this._totalcount = data.totalCount;
                _this._obj.currentPage = data.currentPage;
                _this._obj.pageSize = data.pageSize;
                _this._obj.totalCount = data.totalCount;
                if (_this._obj.totalCount == 0) {
                    _this._obj.msgstatus = "Data not Found!";
                    var fmId = _this._obj.fromMeterid, tmId = _this._obj.toMeterid;
                    _this.ics.sendBean({ "t1": "rp-msg", t2: "Information", t3: _this._obj.msgstatus });
                    _this.goClear();
                    _this._obj.fromMeterid = fmId;
                    _this._obj.toMeterid = tmId;
                }
                else if (data != null) {
                    if (data.data != null) {
                        if (!(data.data instanceof Array)) {
                            var m = [];
                            m[0] = data.data;
                            _this._list.data = m;
                        }
                        else {
                            _this._list = data;
                        }
                    }
                }
            }
            _this._mflag = true;
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
        }, function () { });
    };
    FrmVoidReportComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
            this.http.doGet(url).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this._sessionMsg = data.msgDesc;
                    _this.showMessage();
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmVoidReportComponent = __decorate([
        core_1.Component({
            selector: 'frmvoidreport',
            template: "\n  <div class=\"container\">\n  <div class=\"row clearfix\">\n     <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n       <form class= \"form-horizontal\" (ngSubmit)=\"goList()\"> \n      <!-- Form Name -->\n       <legend>Void Report</legend>\n\n       <div class=\"row  col-md-12\">  \n         <button class=\"btn btn-primary\" type=\"button\" (click)=\"goClear()\" >Clear</button> \n         <button class=\"btn btn-primary\" >List</button>      \n         <button class=\"btn btn-primary\" type=\"button\" (click)=\" Validation();\">Download</button>          \n      </div>\n\n      <div class=\"row col-md-12\">&nbsp;</div>\n  \n      <div class=\"row col-md-12\" id=\"custom-form-alignment-margin\"><!--Start column -->\n        <div class=\"col-md-6\"> \n           <div class=\"form-group\">\n               <label class=\"col-md-4\" > From MeterID <font class=\"mandatoryfont\"> * </font></label>\n                   <div class=\"col-md-8\">\n                       <input type=\"text\"  class=\"form-control\" [(ngModel)]=\"_obj.fromMeterid\" [ngModelOptions]=\"{standalone: true}\" required/>\n                   </div>\n           </div>\n\n           <div class=\"form-group\">\n             <label class=\"col-md-4\" > To MeterID <font class=\"mandatoryfont\"> * </font> </label>\n               <div class=\"col-md-8\">\n       <input type=\"text\"  class=\"form-control\" [(ngModel)]=\"_obj.toMeterid\" [ngModelOptions]=\"{standalone: true}\" required />\n       </div>\n           </div>\n\n           <div class=\"form-group\">\n             <label class=\"col-md-4\"></label>\n             <div class=\"col-md-8\">\n               <select [(ngModel)]=_obj.select [ngModelOptions]=\"{standalone: true}\"  (change)=\"changeSelectVal($event.target.options)\" required=\"true\"   class=\"form-control col-md-0\"  >\n                 <option *ngFor=\"let item of ref._lov1.ref010\" value=\"{{item.value}}\" >{{item.caption}}</option> \n               </select>                     \n             </div>\n           </div>  \n     \n   </div>\n </div>\n    </form>\n   </div>\n </div>\n</div>\n\n<div [hidden]=\"_entryhide\">\n <div class=\"form-group\">\n   <div style = \"margin-top : 10px\">\n     <adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"_totalcount\" (rpChanged)=\"changedPager($event)\" ></adminpager> \n   </div>\n </div>\n\n <table class=\"table table-striped table-condensed table-hover tblborder\" style=\"font-size:14px;\">\n <thead>\n   <tr > \n     <th>NO</th>\n     <th>Meter ID </th>\n     <th> Status </th>\n     <th> Remark </th>  \n   </tr>\n </thead>\n <tbody>\n   <tr *ngFor=\"let obj of _list.data,  let i=index\" >    \n     <td>{{i+1}}</td>                  \n     <td>{{obj.meterID}}</td>\n     <td>{{obj.status}}</td>\n     <td>{{obj.remark}}</td>     \n   </tr>  \n</tbody>\n</table>\n</div>\n\n<div [hidden] = \"_mflag\">\n <div class=\"modal\" id=\"loader\">\n</div>\n\n "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmVoidReportComponent);
    return FrmVoidReportComponent;
}());
exports.FrmVoidReportComponent = FrmVoidReportComponent;
//# sourceMappingURL=frmvoidreport.component.js.map