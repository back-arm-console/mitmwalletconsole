"use strict";
var router_1 = require('@angular/router');
var accounttransfer_component_1 = require('./accounttransfer.component');
var user_report_component_1 = require('./user-report.component');
var wtopup_report_component_1 = require('./wtopup-report.component');
var trialbalancereport_component_1 = require('./trialbalancereport.component');
var dailyreport_component_1 = require('./dailyreport.component');
var transactionreport_component_1 = require('./transactionreport.component');
var settlement_report_component_1 = require('./settlement-report.component');
var merchantTransferReport_component_1 = require('./merchantTransferReport.component');
var ReportRoutes = [
    { path: 'acctransfer', component: accounttransfer_component_1.accountTransfer },
    { path: 'acctransfer/:cmd', component: accounttransfer_component_1.accountTransfer },
    { path: 'acctransfer/:cmd/:id', component: accounttransfer_component_1.accountTransfer },
    { path: 'merchant-transfer', component: merchantTransferReport_component_1.merchantTransferReport },
    { path: 'merchant-transfer/:cmd', component: merchantTransferReport_component_1.merchantTransferReport },
    { path: 'merchant-transfer/:cmd/:id', component: merchantTransferReport_component_1.merchantTransferReport },
    { path: 'User Report', component: user_report_component_1.UserReport },
    { path: 'wtopup Report', component: wtopup_report_component_1.wtopupReport },
    { path: 'trialbalance', component: trialbalancereport_component_1.TrialBalanceReport },
    { path: 'dailyreport', component: dailyreport_component_1.DailyReport },
    { path: 'transactionreport', component: transactionreport_component_1.TransactionReport },
    { path: 'settlement-report', component: settlement_report_component_1.settlementReport },
];
exports.ReportRouting = router_1.RouterModule.forChild(ReportRoutes);
//# sourceMappingURL=report.routing.js.map