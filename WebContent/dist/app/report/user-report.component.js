"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require("@angular/platform-browser");
var router_1 = require('@angular/router');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
core_1.enableProdMode();
var UserReport = (function () {
    function UserReport(el, l_util, renderer, ics, _router, route, http, ref, sanitizer) {
        this.el = el;
        this.l_util = l_util;
        this.renderer = renderer;
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this.sanitizer = sanitizer;
        this._util = new rp_client_util_1.ClientUtil();
        this.today = this._util.getTodayDate();
        this.myDatePickerOptions = this._util.getDatePicker();
        this._dates = { "fromDate": this.dateobj, "toDate": this.dateobj };
        this._entryhide = true;
        this._mflag = true;
        this._sessionObj = this.getSessionObj();
        this._toggleSearch = true;
        this._SearchString = "";
        this.i = 0;
        this.file_type = "";
        this._OperationMode = "";
        this._obj = this.getDefaultObj();
        this._pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 1 };
        this.walletuserArr = [["all", "All"], ["save", "Save"], ["delete", "Delete"]];
        this.consoleuserArr = [["all", "All"], ["save", "Save"], ["activate", "Activate"], ["deactivate", "Deactivate"], ["lock", "Lock"], ["delete", "Delete"]];
        this._list = {
            "data": [{
                    "cif": "", "createdby": "", "createddate": "", "modifiedby": "", "modifieddate": "",
                    "n1": 0, "n7": 0,
                    "nrc": "", "phoneno": "", "recordStatus": 0, "syskey": 0, "userID": "", "username": "", "userstatus": ""
                }],
            "aFromDate": "", "aToDate": "", "alldate": "", "branchCode": "", "click": "", "fileType": "", "msgCode": "", "msgDesc": "", "msgstatus": "", "nrc": "", "phoneno": "", "select": "", "sessionID": "", "status": "", "totalCount": 0,
            "currentPage": 1, "pageSize": 10
        };
        this._FilterDataset = {
            filterList: [
                { "itemid": "", "caption": "", "fieldname": "", "datatype": "", "condition": "", "t1": "", "t2": "" }
            ],
            "pageNo": 1, "pageSize": 10, "filterSource": 0, "sessionID": "", "userID": "", "userName": this.ics._profile.userName
        };
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        this.setTodayDateObj();
        this.setDateObjIntoString();
        this.getUserType();
        this._pdfdata = false;
    }
    UserReport.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    UserReport.prototype.getDefaultObj = function () {
        return {
            "sessionID": "", "msgCode": "", "msgDesc": "", "alldate": true, "fileType": "EXCEL",
            "aFromDate": "", "aToDate": "", "select": "EXCEL", "totalCount": 0, "currentPage": 1, "pageSize": 10, "msgstatus": "",
            "username": "", "nrc": "", "accountNo": "", "phoneno": "", "usertype": "", "status": "all", "branchCode": "", "userID": "", "click": 0, "condition": "", "simpleSearch": ""
        };
    };
    UserReport.prototype.ngOnInit = function () {
        //this.filterSearch();
        this.loadAdvancedSearchData();
    };
    UserReport.prototype.filterSearch = function () {
        if (this._toggleSearch)
            this.filterCommonSearch();
        else
            this.ics.send001("FILTER");
    };
    UserReport.prototype.filterCommonSearch = function () {
        var l_DateRange;
        var l_SearchString = "";
        this._FilterDataset.filterList = [];
        this._FilterDataset.filterSource = 0;
        if (jQuery.trim(this._SearchString) != "") {
            l_SearchString = jQuery.trim(this._SearchString);
            this._FilterDataset.filterList =
                [
                    { "itemid": "1", "caption": "Search String", "fieldname": "SearchString", "datatype": "simplesearch", "condition": "c", "t1": l_SearchString, "t2": "" }
                ];
        }
        //  l_DateRange = { "itemid": "4", "caption": "Date Range", "fieldname": "DateRange", "datatype": "date", "condition": "eq", "t1": this.l_util.getTodayDate(), "t2": "" };
        //   this._FilterDataset.filterList.push(l_DateRange);
        if (this._OperationMode != "prepareFilter") {
            if (this._FilterDataset.pageNo == 1)
                this.goList();
            else
                this._FilterDataset.pageNo = 1;
        }
    };
    UserReport.prototype.setTodayDateObj = function () {
        this._dates.fromDate = this._util.changestringtodateobject(this.today);
        this._dates.toDate = this._util.changestringtodateobject(this.today);
    };
    UserReport.prototype.setDateObjIntoString = function () {
        this._obj.aFromDate = this._util.getDatePickerDateymd(this._dates.fromDate);
        this._obj.aToDate = this._util.getDatePickerDateymd(this._dates.toDate);
    };
    UserReport.prototype.getAllDate = function (event) {
        this._entryhide = true;
        this._list.currentPage = 1;
        this._list.totalCount = 0;
        this._obj.alldate = event.target.checked;
    };
    UserReport.prototype.loadAdvancedSearchData = function () {
        this._TypeList =
            {
                "lovStatus": [],
                "lovFileType": [
                    { "value": "1", "caption": "EXCEL" },
                    { "value": "2", "caption": "PDF" }
                ],
                "lovUserType": [
                    { "value": "1", "caption": "Console User" },
                    { "value": "2", "caption": "Wallet User" },
                ],
                "lovStatusType": [
                    { "value": "all", "caption": "All" },
                    { "value": "save", "caption": "Save" },
                    { "value": "delete", "caption": "Delete" }
                ]
            };
        //this.loadStatus();  
        //this.getAllMerchant();
        //this.loadMerchantType();
        this._FilterList =
            [
                { "itemid": "1", "caption": "Search String", "fieldname": "SearchString", "datatype": "simplesearch", "condition": "c", "t1": "", "t2": "", "t3": "" },
                { "itemid": "2", "caption": "Date", "fieldname": "FromDate", "datatype": "date", "condition": "bt", "t1": this.l_util.getTodayDate(), "t2": this.l_util.getTodayDate(), "t3": "true" },
                { "itemid": "3", "caption": "User Type", "fieldname": "UserType", "datatype": "lovUserType", "condition": "", "t1": "", "t2": "", "t3": "" },
                { "itemid": "4", "caption": "Status", "fieldname": "Status", "datatype": "lovStatusType", "condition": "c", "t1": "", "t2": "", "t3": "" },
                { "itemid": "5", "caption": "User Name", "fieldname": "UserName", "datatype": "simplesearch", "condition": "c", "t1": "", "t2": "", "t3": "" },
                { "itemid": "6", "caption": "NRC", "fieldname": "NRC", "datatype": "simplesearch", "condition": "c", "t1": "", "t2": "", "t3": "true" },
                { "itemid": "7", "caption": "Phone No.", "fieldname": "PhoneNo", "datatype": "simplesearch", "condition": "", "t1": "", "t2": "", "t3": "" },
            ];
    };
    UserReport.prototype.closePage = function () {
        this._router.navigate(['000', { cmd: "READ", p1: this.ics._profile.userID }]);
    };
    UserReport.prototype.closeList = function () {
        this.goClear();
    };
    UserReport.prototype.btnToggleSearch_onClick = function () {
        this.toggleSearch(!this._toggleSearch);
    };
    UserReport.prototype.toggleSearch = function (aToggleSearch) {
        // Set Flag
        this._toggleSearch = aToggleSearch;
        // Clear Simple Search
        if (!this._toggleSearch)
            this._SearchString = '';
        // Enable/Disabled Simple Search Textbox
        jQuery('#isInputSearch').prop('disabled', !this._toggleSearch);
        // Hide Advanced Search
        if (this._toggleSearch) {
            this.ics.send001("CLEAR");
            //this._FilterDataset = null;
            this._obj = this.getDefaultObj();
        }
        // Show/Hide Advanced Search    
        //  
        if (this._toggleSearch)
            jQuery("#asAdvancedSearch").css("display", "none"); // true     - Simple Search
        else {
            jQuery("#asAdvancedSearch").css("display", "inline-block"); // false    - Advanced Search
            this._obj = this.getDefaultObj();
        }
        // Set Icon 
        //  
        if (this._toggleSearch)
            jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-up").addClass("glyphicon glyphicon-menu-down"); // true     - Simple Search
        else
            jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-down").addClass("glyphicon glyphicon-menu-up"); // false    - Advanced Search
        // Set Tooltip
        //
        var l_Tooltip = (this._toggleSearch) ? "Expand" : "Collapse";
        jQuery('#btnToggleSearch').attr('title', l_Tooltip);
    };
    UserReport.prototype.renderAdvancedSearch = function (event) {
        this.toggleSearch(!event);
    };
    UserReport.prototype.filterAdvancedSearch = function (event) {
        this._FilterDataset.filterSource = 1;
        this._FilterDataset.filterList = [];
        this._FilterDataset.filterList = event;
        if (this._OperationMode != "prepareFilter") {
            if (this._FilterDataset.pageNo == 1)
                this.goList();
            else
                this._FilterDataset.pageNo = 1;
        }
    };
    UserReport.prototype.goPrint = function () {
        var _this = this;
        var l_Data = [];
        l_Data = this._FilterDataset;
        this._mflag = false;
        this.setDateObjIntoString();
        if (this._obj.aFromDate > this._obj.aToDate) {
            this._mflag = true;
            this.showMsg("From Date Should Not Exceed To Date.", false);
        } /* if (!/^(([1-9]|1[0-9])\/[A-Za-z]+\([N|n]\)[0-9]{6})$/.test(this._obj.nrc)) {
          this.showMsg("Invalid NRC No.", false);
        } */
        else {
            if (l_Data != null) {
                for (this.i = 0; this.i < l_Data.filterList.length; this.i++) {
                    if (l_Data.filterList[this.i].itemid == "1") {
                        this._obj.simpleSearch = l_Data.filterList[this.i].t1;
                    }
                    else if (l_Data.filterList[this.i].itemid == "2") {
                        this._obj.aFromDate = l_Data.filterList[this.i].t1;
                        this._obj.aToDate = l_Data.filterList[this.i].t2;
                        this._obj.condition = l_Data.filterList[this.i].condition;
                    }
                    else if (l_Data.filterList[this.i].itemid == "3") {
                        this._obj.usertype = l_Data.filterList[this.i].t1;
                    }
                    else if (l_Data.filterList[this.i].itemid == "4") {
                        this._obj.status = l_Data.filterList[this.i].t1;
                    }
                    else if (l_Data.filterList[this.i].itemid == "5") {
                        this._obj.username = l_Data.filterList[this.i].t1;
                    }
                    else if (l_Data.filterList[this.i].itemid == "6") {
                        this._obj.nrc = l_Data.filterList[this.i].t1;
                    }
                    else if (l_Data.filterList[this.i].itemid == "7") {
                        this._obj.phoneno = l_Data.filterList[this.i].t1;
                    }
                }
            }
            try {
                this._obj.fileType = "PDF";
                var url = this.ics.cmsurl + 'ServiceReportAdm/getUserReportList1?download=1';
                var json = this._obj;
                this._obj.sessionID = this.ics._profile.sessionID;
                this._obj.userID = this.ics._profile.userID;
                this.http.doPost(url, json).subscribe(function (data) {
                    _this._mflag = true;
                    if (data.msgCode = "0016") {
                        _this.showMsg(data.msgDesc, false);
                    }
                    if (data.state) {
                        var url2 = _this.ics.cmsurl + 'ServiceReportAdm/downloadReport?fileType=' + _this._obj.fileType +
                            '&fileName=' + data.stringResult[0] + '&sessionID=' + _this.ics._profile.sessionID + '&userID=' + _this.ics._profile.userID;
                        if (_this._obj.fileType == "PDF") {
                            _this._mflag = true;
                            jQuery("#downloadPdf").html("<iframe src=" + url2 + "></iframe>");
                            _this.showMsg("Download Successfully", true);
                        }
                        if (_this._obj.fileType == "EXCEL") {
                            _this._mflag = true;
                            jQuery("#downloadExcel").html("<iframe src=" + url2 + "></iframe>");
                            _this.showMsg("Download Successfully", true);
                        }
                    }
                }, function (error) {
                    _this._mflag = true;
                    _this.showMsg("HTTP Error Type ", false);
                }, function () {
                    _this._mflag = true;
                });
            }
            catch (e) {
                alert("Invalid URL.");
            }
        }
    };
    UserReport.prototype.goPrintExcel = function () {
        var _this = this;
        var l_Data = [];
        l_Data = this._FilterDataset;
        this._mflag = false;
        this.setDateObjIntoString();
        if (this._obj.aFromDate > this._obj.aToDate) {
            this._mflag = true;
            this.showMsg("From Date Should Not Exceed To Date.", false);
        } /* if (!/^(([1-9]|1[0-9])\/[A-Za-z]+\([N|n]\)[0-9]{6})$/.test(this._obj.nrc)) {
          this.showMsg("Invalid NRC No.", false);
        } */
        else {
            if (l_Data != null) {
                for (this.i = 0; this.i < l_Data.filterList.length; this.i++) {
                    if (l_Data.filterList[this.i].itemid == "1") {
                        this._obj.simpleSearch = l_Data.filterList[this.i].t1;
                    }
                    else if (l_Data.filterList[this.i].itemid == "2") {
                        this._obj.aFromDate = l_Data.filterList[this.i].t1;
                        this._obj.aToDate = l_Data.filterList[this.i].t2;
                        this._obj.condition = l_Data.filterList[this.i].condition;
                    }
                    else if (l_Data.filterList[this.i].itemid == "3") {
                        this._obj.usertype = l_Data.filterList[this.i].t1;
                    }
                    else if (l_Data.filterList[this.i].itemid == "4") {
                        this._obj.status = l_Data.filterList[this.i].t1;
                    }
                    else if (l_Data.filterList[this.i].itemid == "5") {
                        this._obj.username = l_Data.filterList[this.i].t1;
                    }
                    else if (l_Data.filterList[this.i].itemid == "6") {
                        this._obj.nrc = l_Data.filterList[this.i].t1;
                    }
                    else if (l_Data.filterList[this.i].itemid == "7") {
                        this._obj.phoneno = l_Data.filterList[this.i].t1;
                    }
                }
            }
            try {
                this._obj.fileType = "EXCEL";
                var url = this.ics.cmsurl + 'ServiceReportAdm/getUserReportList1?download=1';
                var json = this._obj;
                this._obj.sessionID = this.ics._profile.sessionID;
                this._obj.userID = this.ics._profile.userID;
                this.http.doPost(url, json).subscribe(function (data) {
                    _this._mflag = true;
                    if (data.msgCode = "0016") {
                        _this.showMsg(data.msgDesc, false);
                    }
                    if (data.state) {
                        var url2 = _this.ics.cmsurl + 'ServiceReportAdm/downloadReport?fileType=' + _this._obj.fileType +
                            '&fileName=' + data.stringResult[0] + '&sessionID=' + _this.ics._profile.sessionID + '&userID=' + _this.ics._profile.userID;
                        if (_this._obj.fileType == "PDF") {
                            _this._mflag = true;
                            jQuery("#downloadPdf").html("<iframe src=" + url2 + "></iframe>");
                            _this.showMsg("Download Successfully", true);
                        }
                        if (_this._obj.fileType == "EXCEL") {
                            _this._mflag = true;
                            jQuery("#downloadExcel").html("<iframe src=" + url2 + "></iframe>");
                            _this.showMsg("Download Successfully", true);
                        }
                    }
                }, function (error) {
                    _this._mflag = true;
                    _this.showMsg("HTTP Error Type ", false);
                }, function () {
                    _this._mflag = true;
                });
            }
            catch (e) {
                alert("Invalid URL.");
            }
        }
    };
    UserReport.prototype.goClear = function () {
        this._list.currentPage = 1;
        this._list.totalCount = 0;
        this._list.data = [];
        this._entryhide = true;
        this._obj = this.getDefaultObj();
        this.setTodayDateObj();
        this._obj.usertype = this.ref._lov3.refUserType[0].value;
        var combo = [];
        if (this._obj.usertype == "1") {
            for (var j = 0; j < 3; j++) {
                var l = 0;
                combo.push({ "value": this.walletuserArr[j][l++], "caption": this.walletuserArr[j][l++] });
            }
            this.ref._lov1.ref021 = combo;
        }
        else if (this._obj.usertype == "2") {
            for (var j = 0; j < 6; j++) {
                var l = 0;
                combo.push({ "value": this.consoleuserArr[j][l++], "caption": this.consoleuserArr[j][l++] });
            }
            this.ref._lov1.ref021 = combo;
        }
    };
    UserReport.prototype.changeStatus = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex; //Get Selected Index
        var value = options[options.selectedIndex].value; //Get Selected Index's Value 
        if (value == "ALL" || value == "") {
            this._obj.status = "ALL";
        }
        else {
            this._obj.status = value;
            for (var i = 1; i < this.ref._lov1.ref021.length; i++) {
                if (this.ref._lov1.ref021[i].value == value) {
                    this._obj.status = this.ref._lov1.ref021[i].value;
                }
            }
        }
    };
    UserReport.prototype.changeUserType = function (event) {
        this._entryhide = true;
        this._list.currentPage = 1;
        this._list.pageSize = 10;
        this._list.totalCount = 0;
        var options = event.target.options;
        var k = options.selectedIndex;
        var value = options[options.selectedIndex].value;
        var combo = [];
        if (value == 1) {
            for (var j = 0; j < 3; j++) {
                var l = 0;
                combo.push({ "value": this.walletuserArr[j][l++], "caption": this.walletuserArr[j][l++] });
            }
            this.ref._lov1.ref021 = combo;
        }
        else if (value == 2) {
            for (var j = 0; j < 6; j++) {
                var l = 0;
                combo.push({ "value": this.consoleuserArr[j][l++], "caption": this.consoleuserArr[j][l++] });
            }
            this.ref._lov1.ref021 = combo;
        }
    };
    UserReport.prototype.changeFileType = function (options) {
        var value = options[options.selectedIndex].value;
        for (var i = 0; i < this.ref._lov1.ref010.length; i++) {
            if (this.ref._lov1.ref010[i].value = value) {
                this._obj.fileType = value;
            }
        }
    };
    UserReport.prototype.getUserType = function () {
        var _this = this;
        try {
            var url = this.ics.cmsurl + 'serviceAdmLOV/getUserType';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.msgCode == "0016") {
                        _this.showMsg(data.msgDesc, false);
                    }
                    if (data.msgCode == "0000") {
                        if (!(data.refUserType instanceof Array)) {
                            var m = [];
                            m[0] = data.refUserType;
                            _this.ref._lov3.refUserType = m;
                        }
                        else {
                            _this.ref._lov3.refUserType = data.refUserType;
                        }
                        _this._obj.usertype = _this.ref._lov3.refUserType[0].value;
                        var combo = [];
                        if (_this._obj.usertype == "1") {
                            for (var j = 0; j < 3; j++) {
                                var l = 0;
                                combo.push({ "value": _this.walletuserArr[j][l++], "caption": _this.walletuserArr[j][l++] });
                            }
                            _this.ref._lov1.ref021 = combo;
                        }
                        else if (_this._obj.usertype == "2") {
                            for (var j = 0; j < 6; j++) {
                                var l = 0;
                                combo.push({ "value": _this.consoleuserArr[j][l++], "caption": _this.consoleuserArr[j][l++] });
                            }
                            _this.ref._lov1.ref021 = combo;
                        }
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    UserReport.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    UserReport.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    UserReport.prototype.goList = function () {
        var _this = this;
        // let l_Data: any = [];
        this._FilterDataset.sessionID = this.ics._profile.sessionID;
        this._FilterDataset.userID = this.ics._profile.userID;
        var l_Data = this._FilterDataset;
        l_Data = this._FilterDataset;
        this._mflag = false;
        this.setDateObjIntoString();
        if (this._obj.aFromDate > this._obj.aToDate) {
            this._mflag = true;
            this.showMsg("From Date Should Not Exceed To Date.", false);
        }
        else {
            if (l_Data != null) {
                for (this.i = 0; this.i < l_Data.filterList.length; this.i++) {
                    if (l_Data.filterList[this.i].itemid == "1") {
                        this._obj.simpleSearch = l_Data.filterList[this.i].t1;
                    }
                    else if (l_Data.filterList[this.i].itemid == "2") {
                        this._obj.aFromDate = l_Data.filterList[this.i].t1;
                        this._obj.aToDate = l_Data.filterList[this.i].t2;
                        this._obj.condition = l_Data.filterList[this.i].condition;
                    }
                    else if (l_Data.filterList[this.i].itemid == "3") {
                        this._obj.usertype = l_Data.filterList[this.i].t1;
                    }
                    else if (l_Data.filterList[this.i].itemid == "4") {
                        this._obj.status = l_Data.filterList[this.i].t1;
                    }
                    else if (l_Data.filterList[this.i].itemid == "5") {
                        this._obj.username = l_Data.filterList[this.i].t1;
                    }
                    else if (l_Data.filterList[this.i].itemid == "6") {
                        this._obj.nrc = l_Data.filterList[this.i].t1;
                    }
                    else if (l_Data.filterList[this.i].itemid == "7") {
                        this._obj.phoneno = l_Data.filterList[this.i].t1;
                    }
                    else if (l_Data.filterList[this.i].itemid == "8") {
                        this.file_type = l_Data.filterList[this.i].t1;
                        if (this.file_type == "1") {
                            this._obj.fileType = "EXCEL";
                        }
                        else
                            this._obj.fileType = "PDF";
                    }
                }
            }
            var url = this.ics.cmsurl + 'ServiceReportAdm/getUserReportList1?download=0';
            var json = this._obj;
            this._obj.sessionID = this.ics._profile.sessionID;
            this._obj.userID = this.ics._profile.userID;
            this.http.doPost(url, json).subscribe(function (data) {
                _this._mflag = true;
                if (data != null) {
                    _this._obj.totalCount = data.totalCount;
                    if (_this._obj.totalCount == 0) {
                        _this.showMsg("Data not Found!", false);
                        _this._entryhide = true;
                    }
                    else {
                        _this._obj.currentPage = data.currentPage;
                        _this._obj.pageSize = data.pageSize;
                        _this._list = data;
                        if (data.usdata != null) {
                            if (!(data.usdata instanceof Array)) {
                                var m = [];
                                m[0] = data.usdata;
                                data.usdata = m;
                            }
                            _this._list.data = data.usdata;
                            if (_this._obj.totalCount != 0)
                                _this._entryhide = false;
                        }
                    }
                }
            });
        }
    };
    UserReport.prototype.changedPager = function (event) {
        this._pgobj = event;
        this._obj.currentPage = this._pgobj.current;
        this._obj.pageSize = this._pgobj.size;
        this._obj.totalCount = this._pgobj.totalcount;
        if (this._pgobj.totalcount > 1) {
            //this._obj.click = 1;
            this.goList();
        }
    };
    UserReport = __decorate([
        core_1.Component({
            selector: 'fmraccounttrans',
            template: " \n  <div *ngIf=\"!_pdfdata\">\n  <div class=\"container-fluid\">\n  <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n      <form class= \"form-horizontal\" ngNoForm>        \n          <legend>User Report\n            <button type=\"button\" class=\"btn btn-sm btn-primary\" style=\"cursor:pointer;text-align:center;margin-left: 930px;width:30px;height:34px; color:white ;font-size:14px\" (click)=\"goPrint()\" title=\"Download PDF File\">\n              <i  class=\"glyphicon glyphicon-save\" style=\"color:white;margin-left:-4px;\"></i>\n            </button> \n            <button type=\"button\" class=\"btn btn-sm btn-primary\" style=\"cursor:pointer;text-align:center;margin-left: 10px;width:30px;height:34px; color:white ;font-size:20px\" (click)=\"goPrintExcel()\" title=\"Download Excel File\">\n              <i  class=\"fa fa-file-excel-o\" style=\"color:white;margin-left:-4px;\"></i>\n            </button>\n          <div style=\"float:right;text-align:right;\">                                \n          <div style=\"display:inline-block;padding-right:0px;width:280px;\">\n            <div class=\"input-group\"> \n              <input class=\"form-control\" type=\"text\" id=\"isInputSearch\" placeholder=\"Search\" autocomplete=\"off\" spellcheck=\"false\" [(ngModel)]=\"_SearchString\" [ngModelOptions]=\"{standalone: true}\" (keyup.enter)=\"filterSearch()\">\n\n                <span id=\"btnSimpleSearch\" class=\"input-group-addon\">\n                  <i class=\"glyphicon glyphicon-search\" style=\"cursor: pointer;color:#2e8690\" (click)=\"filterSearch()\"></i>\n                </span>\n            </div>\n          </div>\n\n            <button id=\"btnToggleSearch\" class=\"btn btn-sm btn-primary\" type=\"button\" style=\"cursor:pointer;text-align:center;margin-top:-24px;height:34px;\" (click)=\"btnToggleSearch_onClick()\" title=\"Collapse\">\n              <i id=\"lblToggleSearch\" class=\"glyphicon glyphicon-menu-down\"></i> \n            </button>\n\n            <button *ngIf=\"false\" id=\"btnTogglePagination\" type=\"button\" class=\"btn btn-sm btn-primary\" style=\"cursor:pointer;text-align:center;margin-left:25px;margin-top:-24px;width:30px;height:34px;\" (click)=\"btnTogglePagination_onClick()\" title=\"Collapse\">\n              <i id=\"lblTogglePagination\" class=\"glyphicon glyphicon-eject\" style=\"color:#2e8690;margin-left:-4px;transform: rotate(90deg)\"></i>\n            </button>\n\n           <!-- <div id=\"divPagination\" style=\"display:none;margin-left:13px;margin-top:-7px;margin-bottom:5px;\">\n              <adminpager id=\"pgPagination\" rpPageSizeMax=\"100\" [(rpModel)]=\"_ListingDataset.totalCount\" (rpChanged)=\"changedPager($event)\" style=\"font-size:14px;text-align:right;margin-top:-7px;margin-right:10px;\"></adminpager>\n            </div> -->\n\n            <button id=\"btnClose\" type=\"button\" class=\"btn btn-sm btn-primary\" (click)=\"closePage()\" (mouseenter)=\"btnClose_MouseEnter()\" (mouseleave)=\"btnClose_MouseLeave()\" title=\"Close\" style=\"cursor:pointer;height:34px;text-align:center;margin-top:-24px;\">\n              <i id=\"lblClose\" class=\"glyphicon glyphicon-remove\"></i> \n            </button>                     \n        </div>\n          </legend>\n  \t\t\t\t<div class=\"cardview list-height\"> \n  \t\t\t\t  <!--<div class=\"row col-md-12\">  \n            <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goClear()\">Clear</button> \n            <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goList()\">Show</button>             \n  \t\t\t\t  <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goDownload()\">Download</button>          \n           </div> -->\n           <advanced-search id=\"asAdvancedSearch\" [FilterList]=\"_FilterList\" [TypeList]=\"_TypeList\" rpVisibleItems=5 (rpHidden)=\"renderAdvancedSearch($event)\" (rpChanged)=\"filterAdvancedSearch($event)\" style=\"display:none;\"></advanced-search>\n           <!--<div class=\"row col-md-12\">&nbsp;</div> -->\n          <div class=\"form-group\"> \n            <div class=\"col-md-12\">\n  \t\t\t\t\t\t<!--<div class=\"form-group\">            \n  \t\t\t\t\t\t\t<label class=\"col-md-2\">From Date</label>\t\t\t\t\t\t\t\t\n  \t\t\t\t\t\t\t  <div class=\"col-md-3\">\n  \t\t\t\t\t\t\t\t  <my-date-picker name=\"mydate\"  [options]=\"myDatePickerOptions\" [(ngModel)]=\"_dates.fromDate\" ngDefaultControl></my-date-picker>                  \n  \t\t\t\t\t\t\t  </div>\n  \t\t\t\t\t\t\t<label class=\"col-md-2\">To Date</label>\n  \t\t\t\t\t\t\t  <div class=\"col-md-3\">\n  \t\t\t\t\t\t\t\t  <my-date-picker name=\"mydate\" [options]=\"myDatePickerOptions\" [(ngModel)]=\"_dates.toDate\" ngDefaultControl></my-date-picker>                           \n  \t\t\t\t\t\t\t  </div>\t\t\t\t\t\t\t\t\n  \t\t\t\t\t\t\t<div class=\"col-md-2\">\n  \t\t\t\t\t\t\t<label>\n  \t\t\t\t\t\t\t<input type='checkbox' [(ngModel)]=\"_obj.alldate\" [ngModelOptions]=\"{standalone: true}\" (click)='getAllDate($event)'>\n  \t\t\t\t\t\t\tALL\n  \t\t\t\t\t\t\t</label>\n  \t\t\t\t\t\t\t</div>\n  \t\t\t\t\t\t</div>\n  \t\t\t\t\t\t<div class=\"form-group\">\n  \t\t\t\t\t\t  <label class=\"col-md-2\" >User Type</label>\n  \t\t\t\t\t\t  <div class=\"col-md-3\">\n  \t\t\t\t\t\t\t<select [(ngModel)]=\"_obj.usertype\" [ngModelOptions]=\"{standalone: true}\"  (change)=\"changeUserType($event)\"  required=\"true\"   class=\"form-control col-md-0\"  >\n  \t\t\t\t\t\t\t   <option *ngFor=\"let item of ref._lov3.refUserType\" value=\"{{item.value}}\" >{{item.caption}}</option> \n  \t\t\t\t\t\t\t</select>                     \n  \t\t\t\t\t\t  </div>\n  \t\t\t\t\t\t  <label class=\"col-md-2\" >Status</label>\n  \t\t\t\t\t\t\t  <div class=\"col-md-3\">\n  \t\t\t\t\t\t\t\t  <select [(ngModel)]=\"_obj.status\" [ngModelOptions]=\"{standalone: true}\"  (change)=\"changeStatus($event)\"  required=\"true\"   class=\"form-control col-md-0\"  >\n  \t\t\t\t\t\t\t\t\t<option *ngFor=\"let item of ref._lov1.ref021\" value=\"{{item.value}}\" >{{item.caption}}</option> \n  \t\t\t\t\t\t\t\t  </select>                      \n  \t\t\t\t\t\t\t  </div>\n  \t\t\t\t\t\t</div>\n  \t\t\t\t\t\t<div class=\"form-group\">\n  \t\t\t\t\t\t  <label class=\"col-md-2\" > User Name </label>\n  \t\t\t\t\t\t\t  <div class=\"col-md-3\">\n  \t\t\t\t\t\t\t\t  <input type=\"text\"  class=\"form-control\" [(ngModel)]=\"_obj.username\" [ngModelOptions]=\"{standalone: true}\" />\n  \t\t\t\t\t\t\t  </div>\n  \t\t\t\t\t\t\t  <label class=\"col-md-2\" > NRC</label>\n  \t\t\t\t\t\t\t<div class=\"col-md-3\">\n  \t\t\t\t\t\t\t\t<input type=\"text\"  class=\"form-control\" [(ngModel)]=\"_obj.nrc\" [ngModelOptions]=\"{standalone: true}\" />\n  \t\t\t\t\t\t\t</div>\n  \t\t\t\t\t\t</div>\n  \t\t\t\t\t\t<div class=\"form-group\">\n  \t\t\t\t\t\t\t<label class=\"col-md-2\" >Phone No.</label>\n  \t\t\t\t\t\t\t<div class=\"col-md-3\">\n  \t\t\t\t\t\t\t\t<input type=\"text\"  class=\"form-control\" [(ngModel)]=\"_obj.phoneno\" [ngModelOptions]=\"{standalone: true}\" />\n                </div>\n                <rp-input  [(rpModel)]=\"_obj.fileType\" rpRequired =\"true\" rpLabelClass = \"col-md-2\" rpClass=\"col-md-3\" rpType=\"ref010\" rpLabel=\"File Type\" (ngModelChange)=\"changeFileType($event)\"></rp-input>\n              </div>  -->\n            \n            <div [hidden]=\"_entryhide\">\n              <div style = \"margin-top : 10px\">\n              <adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"_list.totalCount\" (rpChanged)=\"changedPager($event)\" ></adminpager>\n              </div>\n              <button id=\"btnClose\" type=\"button\" class=\"btn btn-sm btn-primary\" (click)=\"closeList()\" (mouseenter)=\"btnClose_MouseEnter()\" (mouseleave)=\"btnClose_MouseLeave()\" title=\"Close\" style=\"cursor:pointer;height:34px;text-align:center;\">\n                <i id=\"lblClose\" class=\"glyphicon glyphicon-remove\"></i> \n              </button>\n               <div class=\"row col-md-12\" style=\"overflow-x:auto;\">\n                 <table class=\"table table-striped table-condensed table-hover tblborder\" id=\"tblstyle\">\n                   <thead >\n                    <tr>          \n                      <th class=\"right\" width=\"5%\" style=\"background-color: #e6f2ff; color: #333333\">No</th>\n                      <th class=\"left\" width=\"10%\" style=\"background-color: #e6f2ff; color: #333333\">User ID</th>\n                      <th width=\"10%\" style=\"background-color: #e6f2ff; color: #333333\"> User Name</th>\n                      <th width=\"10%\" style=\"background-color: #e6f2ff; color: #333333\">NRC</th>\n                      <th width=\"10%\" style=\"background-color: #e6f2ff; color: #333333\"> Phone No</th>\n                      <th width=\"10%\" style=\"background-color: #e6f2ff; color: #333333\">Created Date</th>\n                      <th width=\"10%\" style=\"background-color: #e6f2ff; color: #333333\"> Modified </th>\n                      <th width=\"10%\" style=\"background-color: #e6f2ff; color: #333333\">User Status</th>\n                    </tr>\n                   </thead>\n                   <tbody>\n                     <tr *ngFor=\"let obj of _list.data\" >\n                      <td class=\"right\">{{obj.syskey}}</td>\n                      <td >{{obj.userID}}</td> \n                      <td class=\"left\">{{obj.username}}</td> \n                      <td class=\"left\">{{obj.nrc}}</td> \n                      <td class=\"left\">{{obj.phoneno}}</td> \n                      <td>{{obj.createddate}}</td> \n                      <td>{{obj.modifieddate}}</td> \n                      <td class=\"left\">{{obj.userstatus}}</td>\n                      </tr>  \n                   </tbody>\n                  </table>\n               </div>\n           </div>\n          </div>\n          </div>\n          </div>\n      </form>\n      </div>\n      <div>\n      <div id=\"downloadPdf\" style=\"display: none;width: 0px;height: 0px;\"></div>\n      <div id=\"downloadExcel\" style=\"display: none;width: 0px;height: 0px;\"></div>\n   </div>\n  </div>\n</div>\n</div>     \n    <div [hidden] = \"_mflag\">\n      <div class=\"modal\" id=\"loader\"></div>\n   </div> \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n  ",
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef, rp_client_util_1.ClientUtil, core_1.Renderer, rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences, platform_browser_1.DomSanitizer])
    ], UserReport);
    return UserReport;
}());
exports.UserReport = UserReport;
//# sourceMappingURL=user-report.component.js.map