"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require("@angular/platform-browser");
var router_1 = require('@angular/router');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
core_1.enableProdMode();
var accountTransfer = (function () {
    function accountTransfer(el, l_util, renderer, ics, _router, route, http, ref, sanitizer) {
        this.el = el;
        this.l_util = l_util;
        this.renderer = renderer;
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this.sanitizer = sanitizer;
        this._util = new rp_client_util_1.ClientUtil();
        this.today = this._util.getTodayDate();
        this.myDatePickerOptions = this._util.getDatePicker();
        this._dates = { "fromDate": this.dateobj, "toDate": this.dateobj };
        this._entryhide = true;
        this.showflag = false;
        this.total_Amount = "0.00";
        this.total_BankCharges = "0.00";
        this._isLoading = true;
        this._SearchString = "";
        this.changePage = true;
        this.current = 0;
        this.size = 0;
        this.i = 0;
        this._showListing = false;
        this._showPagination = false;
        //_FilterList: any;
        this._toggleSearch = true; // true - Simple Search, false - Advanced Search
        this._togglePagination = true;
        this._OperationMode = "";
        this._shownull = true;
        //  _mflag = false;
        this._mflag = true;
        this._tflag = false;
        this._ButtonInfo = { "role": "", "formname": "", "button": "" };
        this._sessionObj = this.getSessionObj();
        this._obj = this.getDefaultObj();
        this._pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 1 };
        this._list = {
            "data": [{
                    "amount": "", "autokey": 0, "bankCharges": "", "code": "", "crAccNumber": "",
                    "createDate": "", "currencyCode": "", "drAccNumber": "", "fromAccount": "", "createddate": "", "modifieddate": "", "commissionCharges": "",
                    "fromDate": "", "i1": 0, "mbankingkey": "", "merchantID": "", "merchantName": "", "modifiedDate": "", "n1": 0, "n2": 0, "n3": 0, "settlementDate": "state", "t1": "", "t2": "", "t3": "", "t4": "",
                    "t5": "", "t6": "", "t7": "", "t8": "", "toAccount": "", "toDate": "", "transID": "", "transType": "", "userID": ""
                }],
            "totalCount": 0, "currentPage": 1, "pageSize": 10
        };
        this._FilterDataset = {
            filterList: [
                { "itemid": "", "caption": "", "fieldname": "", "datatype": "", "condition": "", "t1": "", "t2": "" }
            ],
            "pageNo": 1, "pageSize": 10, "filterSource": 0, "sessionID": "", "userID": "", "userName": this.ics._profile.userName
        };
        this._ListingDataset = {
            //  contentData:
            //  [
            //      {
            //          "srno": 0, "syskey": 0, "autokey": 0, "createdDate": "", "createdTime": "", "modifiedDate": "",
            //          "modifiedTime": "", "userId": "", "userName": "", "modifiedUserId": "", "modifiedUserName": "",
            //          "recordStatus": 0, "syncStatus": 0, "syncBatch": 0, "userSyskey": 0, "t1": "",
            //          "t2": "", "t3": "", "t4": "", "t5": "", "t6": "",
            //          "t7": "", "t8": "", "t9": "", "t10": "", "t11": "",
            //          "t12": "", "t13": "", "n1": 0, "n2": 0, "n3": 0,
            //          "n4": 0, "n5": 0, "n6": 0, "n7": 0, "n8": 0,
            //          "n9": 0, "n10": 0, "n11": 0, "n12": 0, "n13": 0,"statusdata":"",
            //          "upload": null, "answer": null, "videoUpload": null, "comData": null, "ansData": null,
            //          "cropdata": null, "agrodata": null, "ferdata": null, "towndata": null, "crop": null,
            //          "fert": null, "agro": null, "addTown": null, "uploadlist": null, "resizelist": null,
            //          "uploadDatalist": null, "uploadedPhoto": null
            //      }
            //  ],
            "pageNo": 0, "pageSize": 10, "totalCount": 1, "userID": "", "userName": ""
        };
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        this.setTodayDateObj();
        this.setDateObjIntoString();
        //this.transfertype();
    }
    accountTransfer.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    /////////////////////////////////////////////
    accountTransfer.prototype.getDefaultObj = function () {
        return {
            "sessionID": "", "userID": "", "fromDate": "", "toDate": "", "fromAccount": "", "alldate": true, "t1": "", "t2": "", "transtype": "All", "condition": "", "simpleSearch": "",
            "toAccount": "", "totalCount": 0, "currentPage": 1, "pageSize": 10, "fileType": "PDF"
        };
    };
    accountTransfer.prototype.ngOnInit = function () {
        // this._mflag = false;
        // this.filterSearch();
        this.goList();
        this.loadAdvancedSearchData();
    };
    accountTransfer.prototype.setTodayDateObj = function () {
        this._dates.fromDate = this._util.changestringtodateobject(this.today);
        this._dates.toDate = this._util.changestringtodateobject(this.today);
    };
    accountTransfer.prototype.setDateObjIntoString = function () {
        this._obj.fromDate = this._util.getDatePickerDateymd(this._dates.fromDate);
        this._obj.toDate = this._util.getDatePickerDateymd(this._dates.toDate);
    };
    accountTransfer.prototype.getAllDate = function (event) {
        this._entryhide = true;
        this._list.currentPage = 1;
        this._list.totalCount = 0;
        this._obj.alldate = event.target.checked;
    };
    accountTransfer.prototype.btnToggleSearch_onClick = function () {
        this.toggleSearch(!this._toggleSearch);
    };
    accountTransfer.prototype.btnTogglePagination_onClick = function () {
        this.togglePagination(!this._togglePagination);
    };
    accountTransfer.prototype.toggleSearch = function (aToggleSearch) {
        // Set Flag
        this._toggleSearch = aToggleSearch;
        // Clear Simple Search
        if (!this._toggleSearch)
            this._SearchString = '';
        // Enable/Disabled Simple Search Textbox
        jQuery('#isInputSearch').prop('disabled', !this._toggleSearch);
        // Hide Advanced Search
        if (this._toggleSearch) {
            this.ics.send001("CLEAR");
            // this._obj.fromAccount = "";
            // this._obj.toAccount = "";
            // this._obj.fromDate = "";
            // this._obj.toDate = "";
            // this._obj.t1 = "";
            // this._obj.t2 = "";
            // this._obj.pageSize
            this._obj = this.getDefaultObj();
        }
        // Show/Hide Advanced Search    
        //  
        if (this._toggleSearch)
            jQuery("#asAdvancedSearch").css("display", "none"); // true     - Simple Search
        else {
            jQuery("#asAdvancedSearch").css("display", "inline-block"); // false    - Advanced Search
        }
        // Set Icon 
        //  
        if (this._toggleSearch)
            jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-up").addClass("glyphicon glyphicon-menu-down"); // true     - Simple Search
        else
            jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-down").addClass("glyphicon glyphicon-menu-up"); // false    - Advanced Search
        // Set Tooltip
        //
        var l_Tooltip = (this._toggleSearch) ? "Expand" : "Collapse";
        jQuery('#btnToggleSearch').attr('title', l_Tooltip);
    };
    accountTransfer.prototype.togglePagination = function (aTogglePagination) {
        // Set Flag
        this._togglePagination = aTogglePagination;
        // Show/Hide Pagination
        //
        if (this._showPagination && this._togglePagination)
            jQuery("#divPagination").css("display", "inline-block");
        else
            jQuery("#divPagination").css("display", "inline-block");
        // Rotate Icon
        //
        if (!this._togglePagination)
            jQuery("#lblTogglePagination").css("transform", "rotate(270deg)");
        else
            jQuery("#lblTogglePagination").css("transform", "rotate(90deg)");
        // Set Icon Position
        if (!this._togglePagination)
            jQuery("#lblTogglePagination").css({ "margin-left": "-5px", "margin-top": "0px" });
        else
            jQuery("#lblTogglePagination").css({ "margin-left": "-5px", "margin-top": "-1px" });
        // Set Tooltip
        //
        var l_Tooltip = (this._togglePagination) ? "Collapse" : "Expand";
        jQuery('#btnTogglePagination').attr('title', l_Tooltip);
    };
    accountTransfer.prototype.closePage = function () {
        this._router.navigate(['000', { cmd: "READ", p1: this.ics._profile.userID }]);
    };
    accountTransfer.prototype.closeList = function () {
        this.goClear();
    };
    accountTransfer.prototype.goList = function () {
        var _this = this;
        this._mflag = false;
        this.changePage = true;
        this.setDateObjIntoString();
        if (this._obj.fromDate > this._obj.toDate) {
            this._mflag = true;
            this.showMsg("From Date Should Not Exceed To Date.", false);
        }
        else {
            var url = this.ics.cmsurl + 'ServiceReportAdm/getWalletTopupReport?download=0';
            var json = this._obj;
            this._obj.sessionID = this.ics._profile.sessionID;
            this._obj.userID = this.ics._profile.userID;
            this.http.doPost(url, json).subscribe(function (data) {
                _this._mflag = true;
                _this.total_Amount = data.total_Amount;
                _this.total_BankCharges = data.total_BankCharges;
                if (_this._obj.alldate == true) {
                    _this.total_Amount = data.total_Allamount;
                }
                if (data != null) {
                    _this._obj.totalCount = data.totalCount;
                    if (_this._obj.totalCount == 0) {
                        _this.showMsg("Data not Found!", false);
                        _this._entryhide = true;
                    }
                    else {
                        _this._obj.currentPage = data.currentPage;
                        _this._obj.pageSize = data.pageSize;
                        _this._list = data;
                        if (data.wldata != null) {
                            if (!(data.wldata instanceof Array)) {
                                var m = [];
                                m[0] = data.wldata;
                                data.wldata = m;
                            }
                            _this._list.data = data.wldata;
                            if (_this._obj.totalCount == 0) {
                                _this._entryhide = true;
                            }
                            else {
                                _this._entryhide = false;
                                _this._mflag = true;
                            }
                        }
                    }
                }
            });
        }
    };
    accountTransfer.prototype.goPrint = function () {
        var _this = this;
        this._mflag = false;
        this.setDateObjIntoString();
        var l_Data = [];
        l_Data = this._FilterDataset;
        if (l_Data != null) {
            for (this.i = 0; this.i < l_Data.filterList.length; this.i++) {
                if (l_Data.filterList[this.i].itemid == "1") {
                    this._obj.simpleSearch = l_Data.filterList[this.i].t1;
                }
                else if (l_Data.filterList[this.i].itemid == "2") {
                    this._obj.fromDate = l_Data.filterList[this.i].t1;
                    this._obj.toDate = l_Data.filterList[this.i].t2;
                    this._obj.condition = l_Data.filterList[this.i].condition;
                }
                else if (l_Data.filterList[this.i].itemid == "3") {
                    this._obj.fromAccount = l_Data.filterList[this.i].t1;
                }
                else if (l_Data.filterList[this.i].itemid == "4") {
                    this._obj.toAccount = l_Data.filterList[this.i].t1;
                }
                else if (l_Data.filterList[this.i].itemid == "7") {
                    this._obj.transtype = l_Data.filterList[this.i].t1;
                }
            }
        }
        if (this._obj.fromDate > this._obj.toDate) {
            this._mflag = true;
            this.showMsg("From Date Should Not Exceed To Date.", false);
        } /* if (!/^(([1-9]|1[0-9])\/[A-Za-z]+\([N|n]\)[0-9]{6})$/.test(this._obj.nrc)) {
          this.showMsg("Invalid NRC No.", false);
        } */
        else {
            try {
                this._obj.fileType = "PDF";
                var url = this.ics.cmsurl + 'ServiceReportAdm/getWalletTopupReport?download=1';
                var json = this._obj;
                this._obj.sessionID = this.ics._profile.sessionID;
                this._obj.userID = this.ics._profile.userID;
                this.http.doPost(url, json).subscribe(function (data) {
                    _this._mflag = true;
                    if (data.msgCode = "0016") {
                        _this.showMsg(data.msgDesc, false);
                    }
                    if (data.state) {
                        var url2 = _this.ics.cmsurl + 'ServiceReportAdm/downloadReport?fileType=' + _this._obj.fileType +
                            "&fileName=" + data.stringResult[0] + '&sessionID=' + _this.ics._profile.sessionID + '&userID=' + _this.ics._profile.userID;
                        if (_this._obj.fileType == "PDF") {
                            _this._mflag = true;
                            jQuery("#downloadPdf").html("<iframe src=" + url2 + "></iframe>");
                            _this.showMsg("Download Successfully", true);
                        }
                        if (_this._obj.fileType == "EXCEL") {
                            _this._mflag = true;
                            jQuery("#downloadExcel").html("<iframe src=" + url2 + "></iframe>");
                            _this.showMsg("Download Successfully", true);
                        }
                    }
                }, function (error) {
                    _this._mflag = true;
                    _this.showMsg("HTTP Error Type ", false);
                }, function () {
                    _this._mflag = true;
                });
            }
            catch (e) {
                alert("Invalid URL.");
            }
        }
    };
    accountTransfer.prototype.goPrintExcel = function () {
        var _this = this;
        this._mflag = false;
        this.setDateObjIntoString();
        var l_Data = [];
        l_Data = this._FilterDataset;
        if (l_Data != null) {
            for (this.i = 0; this.i < l_Data.filterList.length; this.i++) {
                if (l_Data.filterList[this.i].itemid == "1") {
                    this._obj.simpleSearch = l_Data.filterList[this.i].t1;
                }
                else if (l_Data.filterList[this.i].itemid == "2") {
                    this._obj.fromDate = l_Data.filterList[this.i].t1;
                    this._obj.toDate = l_Data.filterList[this.i].t2;
                    this._obj.condition = l_Data.filterList[this.i].condition;
                }
                else if (l_Data.filterList[this.i].itemid == "3") {
                    this._obj.fromAccount = l_Data.filterList[this.i].t1;
                }
                else if (l_Data.filterList[this.i].itemid == "4") {
                    this._obj.toAccount = l_Data.filterList[this.i].t1;
                }
                else if (l_Data.filterList[this.i].itemid == "7") {
                    this._obj.transtype = l_Data.filterList[this.i].t1;
                }
            }
        }
        if (this._obj.fromDate > this._obj.toDate) {
            this._mflag = true;
            this.showMsg("From Date Should Not Exceed To Date.", false);
        } /* if (!/^(([1-9]|1[0-9])\/[A-Za-z]+\([N|n]\)[0-9]{6})$/.test(this._obj.nrc)) {
          this.showMsg("Invalid NRC No.", false);
        } */
        else {
            try {
                this._obj.fileType = "EXCEL";
                var url = this.ics.cmsurl + 'ServiceReportAdm/getWalletTopupReport?download=1';
                var json = this._obj;
                this._obj.sessionID = this.ics._profile.sessionID;
                this._obj.userID = this.ics._profile.userID;
                this.http.doPost(url, json).subscribe(function (data) {
                    _this._mflag = true;
                    if (data.msgCode = "0016") {
                        _this.showMsg(data.msgDesc, false);
                    }
                    if (data.state) {
                        var url2 = _this.ics.cmsurl + 'ServiceReportAdm/downloadReport?fileType=' + _this._obj.fileType +
                            "&fileName=" + data.stringResult[0] + '&sessionID=' + _this.ics._profile.sessionID + '&userID=' + _this.ics._profile.userID;
                        if (_this._obj.fileType == "PDF") {
                            _this._mflag = true;
                            jQuery("#downloadPdf").html("<iframe src=" + url2 + "></iframe>");
                            _this.showMsg("Download Successfully", true);
                        }
                        if (_this._obj.fileType == "EXCEL") {
                            _this._mflag = true;
                            jQuery("#downloadExcel").html("<iframe src=" + url2 + "></iframe>");
                            _this.showMsg("Download Successfully", true);
                        }
                    }
                }, function (error) {
                    _this._mflag = true;
                    _this.showMsg("HTTP Error Type ", false);
                }, function () {
                    _this._mflag = true;
                });
            }
            catch (e) {
                alert("Invalid URL.");
            }
        }
    };
    /* goPrintPDF() {
       this._mflag = false;
       this.setDateObjIntoString();
       let l_Data: any = this._FilterDataset;
       if(l_Data != null ){
     
       for( this.i= 0;this.i<l_Data.filterList.length;this.i++){
   
         if(l_Data.filterList[this.i].itemid == "1"){
           this._obj.simpleSearch = l_Data.filterList[this.i].t1;
         }
         else if(l_Data.filterList[this.i].itemid == "2"){
           this._obj.fromDate = l_Data.filterList[this.i].t1;
           this._obj.toDate = l_Data.filterList[this.i].t2;
           this._obj.condition = l_Data.filterList[this.i].condition;
         }
         else if(l_Data.filterList[this.i].itemid == "3"){
           this._obj.fromAccount = l_Data.filterList[this.i].t1;
         }
        else if(l_Data.filterList[this.i].itemid == "4"){
           this._obj.toAccount = l_Data.filterList[this.i].t1;
         }
        else  if(l_Data.filterList[this.i].itemid == "7"){
           this._obj.transtype = l_Data.filterList[this.i].t1;
         }
       //  else if(l_Data.filterList[this.i].itemid == "8"){
       //    if(l_Data.filterList[this.i].t1 == "1")
       //     this._obj.fileType = "EXCEL";
       //   }
       //   else if(l_Data.filterList[this.i].t1 == "2"){
       //   this._obj.fileType = "PDF";
       this._obj.fileType = "PDF";
         
        }
         
   
       }
     
       if (this._obj.fromDate > this._obj.toDate) {
         this._mflag = true;
         this.showMsg("From Date Should Not Exceed To Date.", false);
       } /* if (!/^(([1-9]|1[0-9])\/[A-Za-z]+\([N|n]\)[0-9]{6})$/.test(this._obj.nrc)) {
         this.showMsg("Invalid NRC No.", false);
       } */ /*else {
      try {
        this._obj.fileType = "PDF";
        let url: string = this.ics.cmsurl + 'ServiceReportAdm/getWalletTopupReport?download=1';
        let json: any = this._obj;
        this._obj.sessionID = this.ics._profile.sessionID;
        this._obj.userID = this.ics._profile.userID;
        this.http.doPost(url, json).subscribe(
          data => {
            this._mflag = true;
            if(data.msgCode="0016"){
              this.showMsg(data.msgDesc,false);
            }
            if (data.state) {
              let url2: string = this.ics.cmsurl + 'ServiceReportAdm/downloadReport?fileType=' + this._obj.fileType +
                "&fileName=" + data.stringResult[0] + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
              if (this._obj.fileType == "PDF") {
                this._mflag = true;
                jQuery("#downloadPdf").html("<iframe src=" + url2 + "></iframe>");
                this.showMsg("Download Successfully",true);
              }
              if (this._obj.fileType == "EXCEL") {
                this._mflag = true;
                jQuery("#downloadExcel").html("<iframe src=" + url2 + "></iframe>");
                this.showMsg("Download Successfully",true);
              }
            }
          },
          error => {
            this._mflag = true;
            this.showMsg("HTTP Error Type ", false);
          },
          () => {
            this._mflag = true;
          }
        );
      } catch (e) {
        alert("Invalid URL.");
      }
    }
  }  */
    accountTransfer.prototype.goClear = function () {
        this._entryhide = true;
        this._list.currentPage = 1;
        this._list.totalCount = 0;
        this._list.data = [];
        this._obj = this.getDefaultObj();
        this.setTodayDateObj();
    };
    accountTransfer.prototype.changeTransType = function (event) {
        this._entryhide = true;
        this._list.currentPage = 1;
        this._list.totalCount = 0;
        var options = event.target.options;
        var k = options.selectedIndex; //Get Selected Index
        var value = options[options.selectedIndex].value; //Get Selected Index's Value  
        for (var i = 0; i < this.ref._lov1.ref011.length; i++) {
            if (this.ref._lov1.ref011[i].value == value)
                this._obj.transtype = value;
        }
    };
    accountTransfer.prototype.transfertype = function () {
        var _this = this;
        try {
            this._mflag = false;
            var url = this.ics.cmsurl + 'serviceAdmLOV/getTransferTypes';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                _this._mflag = true;
                if (data != null) {
                    if (data.msgCode == "0016") {
                        _this.showMsg(data.msgDesc, false);
                        _this.logout();
                    }
                    if (data.msgCode != "0000") {
                        _this.showMsg(data.msgDesc, true);
                    }
                    if (data.msgCode == "0000") {
                        if (data.refAccountTransferType != null) {
                            if (!(data.refAccountTransferType instanceof Array)) {
                                var m = [];
                                m[0] = data.refAccountTransferType;
                                _this.ref._lov3.refAccountTransferType = m;
                                _this._obj.transtype = _this.ref._lov3.refAccountTransferType[0].value;
                            }
                            else {
                                _this.ref._lov3.refAccountTransferType = data.refAccountTransferType;
                                _this._obj.transtype = _this.ref._lov3.refAccountTransferType[0].value;
                            }
                        }
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out!");
                    _this._mflag = true;
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert(e);
            this._mflag = true;
        }
    };
    accountTransfer.prototype.changeTransferType = function (options) {
        var value1 = options[options.selectedIndex].value;
        for (var i = 1; i < this.ref._lov3.refAccountTransferType.length; i++) {
            if (this.ref._lov3.refAccountTransferType[i].value == value1) {
                this._obj.transtype = value1;
                break;
            }
        }
    };
    accountTransfer.prototype.filterSearch = function () {
        if (this._toggleSearch)
            this.filterCommonSearch();
        else
            this.ics.send001("FILTER");
    };
    accountTransfer.prototype.filterCommonSearch = function () {
        var l_DateRange;
        var l_SearchString = "";
        this._FilterDataset.filterList = [];
        this._FilterDataset.filterSource = 0;
        if (jQuery.trim(this._SearchString) != "") {
            l_SearchString = jQuery.trim(this._SearchString);
            this._FilterDataset.filterList =
                [
                    { "itemid": "1", "caption": "Search String", "fieldname": "SearchString", "datatype": "simplesearch", "condition": "c", "t1": l_SearchString, "t2": "" }
                ];
        }
        //  l_DateRange = { "itemid": "4", "caption": "Date Range", "fieldname": "DateRange", "datatype": "date", "condition": "eq", "t1": this.l_util.getTodayDate(), "t2": "" };
        //   this._FilterDataset.filterList.push(l_DateRange);
        if (this._OperationMode != "prepareFilter") {
            if (this._FilterDataset.pageNo == 1)
                this.filterRecords();
            else
                this._FilterDataset.pageNo = 1;
        }
    };
    accountTransfer.prototype.changeFileType = function (options) {
        var value = options[options.selectedIndex].value;
        for (var i = 0; i < this.ref._lov1.ref010.length; i++) {
            if (this.ref._lov1.ref010[i].value = value)
                this._obj.fileType = value;
        }
    };
    accountTransfer.prototype.formatNumber = function (amt) {
        if (amt != undefined && amt != "0") {
            return this.thousand_sperator(parseFloat(amt).toFixed(2));
        }
        else {
            return "0.00";
        }
    };
    accountTransfer.prototype.thousand_sperator = function (num) {
        if (num != "" && num != undefined && num != null) {
            num = num.replace(/,/g, "");
        }
        var parts = num.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts; // return 100,000.00
    };
    accountTransfer.prototype.restrictSpecialCharacter = function (event, fid, value) {
        if ((event.key >= '0' && event.key <= '9') || event.key == '.' || event.key == ',' || event.key == 'Home' || event.key == 'End' ||
            event.key == 'ArrowLeft' || event.key == 'ArrowRight' || event.key == 'Delete' || event.key == 'Backspace' || event.key == 'Tab' ||
            (event.which == 67 && event.ctrlKey === true) || (event.which == 88 && event.ctrlKey === true) ||
            (event.which == 65 && event.ctrlKey === true) || (event.which == 86 && event.ctrlKey === true)) {
            if (fid == 101) {
                if (/^[\\\"\'\;\:\>\|~`!@#\$%^&*\(\)]$/i.test(event.key)) {
                    event.preventDefault();
                }
            }
            if (value.includes(".")) {
                if (/^[\\\"\'\;\:\>\|~`!@#\$%^&*.\(\)]$/i.test(event.key)) {
                    event.preventDefault();
                }
            }
        }
        else {
            event.preventDefault();
        }
    };
    accountTransfer.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    accountTransfer.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    accountTransfer.prototype.changedPager = function (event) {
        this._pgobj = event;
        this.current = this._pgobj.current;
        this.size = this._pgobj.size;
        this._obj.currentPage = this._pgobj.current;
        this._obj.pageSize = this._pgobj.size;
        this._obj.totalCount = this._pgobj.totalcount;
        if (this._pgobj.totalcount > 1) {
            //this._obj.click = 1;
            if (this.changePage == true) {
                this.goList();
            }
            else {
                this.filterRecords();
            }
        }
    };
    accountTransfer.prototype.loadAdvancedSearchData = function () {
        this._TypeList =
            {
                "lovStatus": [],
                "lovFileType": [
                    { "value": "1", "caption": "EXCEL" },
                    { "value": "2", "caption": "PDF" }
                ],
                // 4--agent to wallet
                // 1--wallet to wallet
                //2--merchant payment
                // 5--wallet to agent
                "lovTransferType": [
                    { "value": "All", "caption": "All" },
                    { "value": "1", "caption": "Wallet to wallet" },
                    { "value": "2", "caption": "Merchant payment" },
                    { "value": "4", "caption": "Agent to wallet" },
                    { "value": "5", "caption": "Wallet to agent" }
                ],
            };
        // this.loadStatus();  
        this._FilterList =
            [{ "itemid": "1", "caption": "Search String", "fieldname": "SearchString", "datatype": "simplesearch", "condition": "c", "t1": "", "t2": "", "t3": "" },
                { "itemid": "2", "caption": "Transaction Date", "fieldname": "FromDate", "datatype": "date", "condition": "bt", "t1": this.l_util.getTodayDate(), "t2": this.l_util.getTodayDate(), "t3": "true" },
                //{ "itemid": "3", "caption": "To Date", "fieldname": "ToDate", "datatype": "date", "condition": "", "t1": this.l_util.getTodayDate(), "t2":"", "t3": "true"  },
                { "itemid": "3", "caption": "From Account", "fieldname": "FromAccount", "datatype": "simplesearch", "condition": "", "t1": "", "t2": "", "t3": "" },
                { "itemid": "4", "caption": "To Account", "fieldname": "ToAccount", "datatype": "simplesearch", "condition": "", "t1": "", "t2": "", "t3": "" },
                { "itemid": "5", "caption": "From Name", "fieldname": "FromName", "datatype": "simplesearch", "condition": "", "t1": "", "t2": "", "t3": "" },
                { "itemid": "6", "caption": "To Name", "fieldname": "ToName", "datatype": "simplesearch", "condition": "", "t1": "", "t2": "", "t3": "" },
                { "itemid": "7", "caption": "Transfer Type", "fieldname": "SearchString", "datatype": "lovTransferType", "condition": "c", "t1": "", "t2": "", "t3": "" },
            ];
    };
    accountTransfer.prototype.renderAdvancedSearch = function (event) {
        this.toggleSearch(!event);
    };
    accountTransfer.prototype.filterAdvancedSearch = function (event) {
        this._FilterDataset.filterSource = 1;
        this._FilterDataset.filterList = [];
        this._FilterDataset.filterList = event;
        if (this._OperationMode != "prepareFilter") {
            if (this._FilterDataset.pageNo == 1)
                this.filterRecords();
            else
                this._FilterDataset.pageNo = 1;
        }
    };
    accountTransfer.prototype.filterRecords = function () {
        var _this = this;
        this._mflag = false;
        this._FilterDataset.sessionID = this.ics._profile.sessionID;
        this._FilterDataset.userID = this.ics._profile.userID;
        var l_Data = this._FilterDataset;
        // this._obj.fromDate = this._FilterDataset.filterList[0].t1;
        // this._obj.toDate = this._FilterDataset.filterList[0].t2;
        // this._obj.fromAccount = this._FilterDataset.filterList[1].t1;
        // this._obj.toAccount = this._FilterDataset.filterList[2].t1;
        // this._obj.transtype = this._FilterDataset.filterList[6].t1;
        // this._obj.fileType = this._FilterDataset.filterList[7].t1;
        var l_ServiceURL = this.ics.cmsurl + 'ServiceReportAdm/getWalletTopupReportWithAdvancedSearch?currentPage=' + this.current + "&size=" + this.size;
        // Show loading animation
        this.http.doPost(l_ServiceURL, l_Data).subscribe(function (data) {
            // this._mflag = false;
            _this._mflag = true;
            _this.changePage = false;
            _this.total_Amount = data.total_Amount;
            _this._entryhide = true;
            // this.total_BankCharges = data.total_BankCharges
            // if (this._obj.alldate == true){
            _this.total_Amount = data.total_Allamount;
            // }
            if (data != null) {
                _this._obj.totalCount = data.totalCount;
                if (_this._obj.totalCount == 0) {
                    _this.showMsg("Data not Found!", false);
                    _this._entryhide = true;
                    _this._mflag = true;
                }
                else {
                    _this._obj.currentPage = data.currentPage;
                    _this._obj.pageSize = data.pageSize;
                    _this._list = data;
                    if (data.wldata != null) {
                        if (!(data.wldata instanceof Array)) {
                            var m = [];
                            m[0] = data.wlata;
                            data.wldata = m;
                        }
                        _this._list.data = data.wldata;
                        if (_this._obj.totalCount == 0) {
                            _this._entryhide = true;
                        }
                        else {
                            _this._entryhide = false;
                            _this._mflag = true;
                        }
                    }
                }
            }
        });
    };
    accountTransfer = __decorate([
        core_1.Component({
            selector: 'wallettopup-report',
            template: " \n  <div *ngIf=\"!_pdfdata\">  \n    <div class=\"container-fluid\">\n      <div class=\"row clearfix\">\n        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n\t\t      <form class=\"form-horizontal\" ngNoForm>\n            <legend>Wallet Transfer Report\n            <button type=\"button\" class=\"btn btn-sm btn-primary\" style=\"cursor:pointer;text-align:center;margin-left: 830px;width:30px;height:34px; color:white ;font-size:14px\" (click)=\"goPrint()\" title=\"Download PDF File\">\n                    <i  class=\"glyphicon glyphicon-save\" style=\"color:white;margin-left:-4px;\"></i>\n              </button> \n              <button type=\"button\" class=\"btn btn-sm btn-primary\" style=\"cursor:pointer;text-align:center;margin-left: 10px;width:30px;height:34px; color:white ;font-size:20px\" (click)=\"goPrintExcel()\" title=\"Download Excel File\">\n                    <i  class=\"fa fa-file-excel-o\" style=\"color:white;margin-left:-4px;\"></i>\n              </button>\n      \n              <div style=\"float:right;text-align:right;\">                                \n                <div style=\"display:inline-block;padding-right:0px;width:280px;\">\n                  <div class=\"input-group\"> \n                    <input class=\"form-control\" type=\"text\" id=\"isInputSearch\" placeholder=\"Search\" autocomplete=\"off\" spellcheck=\"false\" [(ngModel)]=\"_SearchString\" [ngModelOptions]=\"{standalone: true}\" (keyup.enter)=\"filterSearch()\">\n\n                      <span id=\"btnSimpleSearch\" class=\"input-group-addon\">\n                        <i class=\"glyphicon glyphicon-search\" style=\"cursor: pointer;color:#2e8690\" (click)=\"filterSearch()\"></i>\n                      </span>\n                  </div>\n                </div>\n\n                <button id=\"btnToggleSearch\" class=\"btn btn-sm btn-primary\" type=\"button\" style=\"cursor:pointer;text-align:center;margin-top:-24px;height:34px;\" (click)=\"btnToggleSearch_onClick()\" title=\"Collapse\">\n                  <i id=\"lblToggleSearch\" class=\"glyphicon glyphicon-menu-down\"></i> \n                </button>\n\n                <button *ngIf=\"false\" id=\"btnTogglePagination\" type=\"button\" class=\"btn btn-sm btn-primary\" style=\"cursor:pointer;text-align:center;margin-left:25px;margin-top:-24px;width:30px;height:34px;\" (click)=\"btnTogglePagination_onClick()\" title=\"Collapse\">\n                  <i id=\"lblTogglePagination\" class=\"glyphicon glyphicon-eject\" style=\"color:#2e8690;margin-left:-4px;transform: rotate(90deg)\"></i>\n                </button>\n\n                <div id=\"divPagination\" style=\"display:none;margin-left:13px;margin-top:-7px;margin-bottom:5px;\">\n                  <adminpager id=\"pgPagination\" rpPageSizeMax=\"100\" [(rpModel)]=\"totalCount\" (rpChanged)=\"changedPager($event)\" style=\"font-size:14px;text-align:right;margin-top:-7px;margin-right:10px;\"></adminpager>\n                </div>\n\n                <button id=\"btnClose\" type=\"button\" class=\"btn btn-sm btn-primary\" (click)=\"closePage()\" (mouseenter)=\"btnClose_MouseEnter()\" (mouseleave)=\"btnClose_MouseLeave()\" title=\"Close\" style=\"cursor:pointer;height:34px;text-align:center;margin-top:-24px;\">\n                  <i id=\"lblClose\" class=\"glyphicon glyphicon-remove\"></i> \n                </button>                     \n              </div>\n      \n            </legend>\n\t\t\t\t    <div class=\"cardview list-height\"> \n\t\t\t\t\t    <!--<div class=\"row col-md-12\">\n                <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goClear()\">Clear</button> \n                 <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goList()\">Show </button>     \n                <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goPrint()\">Download </button> -->\n              <!--  <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goPrintPDF()\">Download PDF</button> -->\n             <!-- </div>-->\n            <!-- <div class=\"row col-md-12\">\n              <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"gotoBarChart()\">Bar Chart</button> \n             </div> -->\n              <div style=\"padding: 5px 10px;\"></div>\n              <advanced-search id=\"asAdvancedSearch\" [FilterList]=\"_FilterList\" [TypeList]=\"_TypeList\" rpVisibleItems=5 (rpHidden)=\"renderAdvancedSearch($event)\" (rpChanged)=\"filterAdvancedSearch($event)\" style=\"display:none; margin-left:5px\"></advanced-search>\n             <!-- <div class=\"row col-md-12\">&nbsp;</div> -->\n                <div class=\"form-group\">\n                  <div class=\"col-md-12\">\t\n                    <div [hidden]=\"_entryhide\">\n                      <div class=\"form-group\">\n                        <div class=\"col-md-8\">\n                          <div style=\"margin-top: 10px;\">  \n                            <adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"_list.totalCount\" (rpChanged)=\"changedPager($event)\"></adminpager>\n                          </div>\n                           <button id=\"btnClose\" type=\"button\" class=\"btn btn-sm btn-primary\" (click)=\"closeList()\" (mouseenter)=\"btnClose_MouseEnter()\" (mouseleave)=\"btnClose_MouseLeave()\" title=\"Close\" style=\"cursor:pointer;height:34px;text-align:center;\">\n                            <i id=\"lblClose\" class=\"glyphicon glyphicon-remove\"></i> \n                          </button>\n                        \n                        </div>\n\n                        <div class=\"col-md-4\" style=\"margin-top: 30px;overflow-x:auto;\">      \n                          <table style=\"align: right; font-size: 14px;float:right;\" cellpadding=\"10px\" cellspacing=\"10px\">\n                            <tbody>\n                              <colgroup>\n                                <col span=\"1\" style=\"width: 50%;\">\n                                <col span=\"1\" style=\"width: 50%;\">                                  \n                              </colgroup>                           \n                              <tr>\n                                <td style=\"color: blue;\" class=\"left\">\n                                  <p><b>Grand Total Amount </b></p>\n                                </td>\n                                <td style=\"color: blue;\" class=\"center\"><p>&nbsp;&nbsp;:&nbsp;&nbsp;</p></td>\n                                <td style=\"color: blue;\" class=\"right\">\n                                  <p><b>{{formatNumber(this.total_Amount)}}</b></p>\n                                </td>\n                              </tr>\n                              <tr>\n                               \n                                <td style=\"color: blue;\" class=\"left\">\n                                  <p><b>Grand Total Bank Charges</b></p>\n                                </td>\n                                <td style=\"color: blue;\" class=\"center\"><p>&nbsp;&nbsp;:&nbsp;&nbsp;</p></td>\n                                <td style=\"color: blue;\" class=\"right\">\n                                  <p><b>{{formatNumber(this.total_BankCharges)}}</b></p>\n                                </td>\n                              </tr>\n                            </tbody>\n                          </table>\n                        </div>\n                      </div>\n                     \n\n\t\t\t\t\t\t          <!-- <div *ngIf=\"_shownull!=false\" style=\"color:#ccc;\">No result found!</div>-->\n\n              \n                      <div class=\"row col-md-12\" style=\"overflow-x:auto;\">\n                      <table class=\"table table-striped table-condensed table-hover tblborder\" id=\"tblstyle\">\n                          <thead style=\"background-color: #e6f2ff;\">\n                            <tr> \n                             <!-- <th width=\"1%\" class=\"left\">\n                                <button id=\"btnClose\" type=\"button\" class=\"btn btn-sm btn-primary\" (click)=\"closeList()\" (mouseenter)=\"btnClose_MouseEnter()\" (mouseleave)=\"btnClose_MouseLeave()\" title=\"Close\" style=\"cursor:pointer;height:34px;text-align:center;\">\n                                  <i id=\"lblClose\" class=\"glyphicon glyphicon-remove\"></i> \n                                </button>\n                              </th>  -->       \n                              <th width=\"1%\" class=\"right\" style=\"background-color: #e6f2ff; color: #333333\">No</th>\n                              <th  width=\"5%\" class=\"right\" style=\"background-color: #e6f2ff; color: #333333\">Transfer ID</th>\n                              <th width=\"10%\" style=\"background-color: #e6f2ff; color: #333333\"> From Name</th>\n                              <th width=\"10%\" style=\"background-color: #e6f2ff; color: #333333\">To Name </th>\n                              <th width=\"10%\" style=\"background-color: #e6f2ff; color: #333333\"> From Account</th>\n                              <th width=\"10%\" style=\"background-color: #e6f2ff; color: #333333\">To Account</th>\n                              <th width=\"10%\" style=\"background-color: #e6f2ff; color: #333333\"> Transfer Date/Time </th>\n                              <th width=\"5%\" class=\"right\" style=\"background-color: #e6f2ff; color: #333333\">Amount</th>\n                              <th width=\"10%\" class=\"right\" style=\"background-color: #e6f2ff; color: #333333\">Commission Charges</th>\n                            </tr>\n                          </thead>\n                          <tbody>\n                            <tr *ngFor=\"let obj of _list.data\" style=\"background-color: #ffffff;\">\n                              <!--<td></td> -->\n                              <td class=\"right\" style=\"padding:8px\">{{obj.autokey}}</td>\n                              <td class=\"right\">{{obj.transID}}</td> \n                              <td class=\"left\">{{obj.t1}}</td> \n                              <td class=\"left\">{{obj.t2}}</td> \n                              <td class=\"left\">{{obj.fromAccount}}</td> \n                              <td class=\"left\">{{obj.toAccount}}</td> \n                              <td class=\"left\">{{obj.transDate}}</td> \n                              <td class=\"right\">{{obj.amount}}</td> \n                              <td class=\"right\">{{obj.commissionCharges}}\n                            </tr>  \n                          </tbody>\n                        </table>\n                      </div>\n                    </div>\n                    <!--<div class=\"form-group\">\n                      <rp-input  [(rpModel)]=\"_obj.fileType\" rpRequired =\"true\" rpLabelClass = \"col-md-2\" rpClass=\"col-md-3\" rpType=\"ref010\" rpLabel=\"File Type\" (ngModelChange)=\"changeFileType($event)\"></rp-input>\n\t\t\t\t\t\t        </div>-->\n                  </div>            \t\t\t\t\t\t\t\t\t\t\n                </div>\n              </div>\n            </form> \n          </div>\n        </div>\n        <div id=\"downloadExcel\" style=\"display: none;width: 0px;height: 0px;\"></div>       \t\t\t\t\n        <div id=\"downloadPdf\" style=\"display: none;width: 0px;height: 0px;\"></div>       \t\t\t\t\n\t</div>\n</div>    \n\t\t<div [hidden] = \"_mflag\">\n\t  <div class=\"modal\" id=\"loader\"></div>\n\t</div>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n  ",
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef, rp_client_util_1.ClientUtil, core_1.Renderer, rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences, platform_browser_1.DomSanitizer])
    ], accountTransfer);
    return accountTransfer;
}());
exports.accountTransfer = accountTransfer;
//# sourceMappingURL=accounttransfer.component.js.map