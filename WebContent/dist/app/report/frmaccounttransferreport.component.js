"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
core_2.enableProdMode();
var FrmAccountTransferReportComponent = (function () {
    function FrmAccountTransferReportComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._util = new rp_client_util_1.ClientUtil();
        this.today = this._util.getTodayDate();
        this.myDatePickerOptions = this._util.getDatePicker();
        this._dates = { "fromDate": this.dateobj, "toDate": this.dateobj };
        this._entryhide = true;
        this._mflag = false;
        this._totalAmount = "";
        this._totalBankCharges = "";
        this._obj = this.getDefaultObj();
        this._pgobj = {
            "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1,
            "end": 10, "size": 10, "totalcount": 1
        };
        this._list = {
            "msgCode": "", "msgDesc": "", "grandTotal": "", "totalAmount": "", "totalBankCharges": "",
            "totalCount": 0, "currentPage": 1, "pageSize": 10,
            "data": [{
                    "transDate": "", "xRef": "", "transRefNo": "", "mobileUserId": "", "fromAccount": "",
                    "toAccount": "", "amount": "", "bankCharges": "", "currency": ""
                }]
        };
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this.checkSession();
            this._obj = this.getDefaultObj();
            this.setTodayDateObj();
            this.setDateObjIntoString();
            this.getAccountTransferTypes();
        }
    }
    FrmAccountTransferReportComponent.prototype.getDefaultObj = function () {
        return {
            "sessionId": "", "userID": "", "fromDate": "", "toDate": "", "fromAccount": "",
            "toAccount": "", "transRefNo": "", "xRef": "", "transferType": "", "select": "EXCEL",
            "totalCount": 0, "currentPage": 1, "pageSize": 10, "msgStatus": ""
        };
    };
    FrmAccountTransferReportComponent.prototype.setTodayDateObj = function () {
        this._dates.fromDate = this._util.changestringtodateobject(this.today);
        this._dates.toDate = this._util.changestringtodateobject(this.today);
    };
    FrmAccountTransferReportComponent.prototype.setDateObjIntoString = function () {
        this._obj.fromDate = this._util.getDatePickerDate(this._dates.fromDate);
        this._obj.toDate = this._util.getDatePickerDate(this._dates.toDate);
    };
    FrmAccountTransferReportComponent.prototype.getAccountTransferTypes = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/getAccountTransferTypes';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                _this._mflag = true;
                if (data != null) {
                    if (data.msgCode == "0016") {
                        _this.showMsgAlert(data.msgDesc);
                        _this.logout();
                    }
                    if (data.msgCode != "0000") {
                        _this.showMsgAlert(data.msgDesc);
                    }
                    if (data.msgCode == "0000") {
                        if (data.refAccountTransferType != null) {
                            if (!(data.refAccountTransferType instanceof Array)) {
                                var m = [];
                                m[0] = data.refAccountTransferType;
                                _this.ref._lov3.refAccountTransferType = m;
                                _this._obj.transferType = _this.ref._lov3.refAccountTransferType[0].value;
                            }
                            else {
                                _this.ref._lov3.refAccountTransferType = data.refAccountTransferType;
                                _this._obj.transferType = _this.ref._lov3.refAccountTransferType[0].value;
                            }
                        }
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmAccountTransferReportComponent.prototype.changedPager = function (event) {
        this._pgobj = event;
        this._obj.currentPage = this._pgobj.current;
        this._obj.pageSize = this._pgobj.size;
        this._obj.totalCount = this._pgobj.totalcount;
        if (this._pgobj.totalcount > 1) {
            this.goList();
        }
    };
    FrmAccountTransferReportComponent.prototype.goList = function () {
        var _this = this;
        this._mflag = false;
        this.setDateObjIntoString();
        if (this._obj.fromDate > this._obj.toDate) {
            this._mflag = true;
            this.showMsgAlert("From Date Should Not Exceed To Date.");
        }
        else {
            try {
                var url = this.ics._apiurl + 'service001/getAccountTransferReport';
                this._obj.sessionId = this.ics._profile.sessionID;
                this._obj.userID = this.ics._profile.userID;
                this._totalAmount = "0.00";
                this._totalBankCharges = "0.00";
                var json = this._obj;
                this.http.doPost(url, json).subscribe(function (data) {
                    _this._mflag = true;
                    if (data != null) {
                        if (data.msgCode == "0016") {
                            _this.showMsgAlert(data.msgDesc);
                            _this.logout();
                        }
                        if (data.msgCode != "0000") {
                            _this.showMsgAlert(data.msgDesc);
                        }
                        if (data.msgCode == "0000") {
                            if (data.totalCount == 0) {
                                _this._entryhide = true;
                                _this.showMsgAlert(data.msgDesc);
                            }
                            if (data.totalCount != 0) {
                                _this._entryhide = false;
                                _this._obj.totalCount = data.totalCount;
                                _this._obj.currentPage = data.currentPage;
                                _this._obj.pageSize = data.pageSize;
                                _this._list = data;
                                if (data.data != null) {
                                    if (!(data.data instanceof Array)) {
                                        var m = [];
                                        m[0] = data.data;
                                        _this._list.data = m;
                                    }
                                    else {
                                        _this._list.data = data.data;
                                    }
                                    _this._totalAmount = _this._list.totalAmount;
                                    _this._totalBankCharges = _this._list.totalBankCharges;
                                }
                            }
                        }
                    }
                }, function (error) {
                    if (error._body.type == "error") {
                        alert("Connection Timed Out.");
                    }
                }, function () { });
            }
            catch (e) {
                alert("Invalid URL.");
            }
        }
    };
    FrmAccountTransferReportComponent.prototype.goDownload = function () {
        this.setDateObjIntoString();
        this.showMsgAlert("Please wait ...");
        var url = this.ics._rpturl
            + "account-transfer.jsp"
            + "?fromDate=" + this._obj.fromDate
            + "&toDate=" + this._obj.toDate
            + "&fromAccount=" + this._obj.fromAccount
            + "&toAccount=" + this._obj.toAccount
            + "&transRefNo=" + this._obj.transRefNo
            + "&xRef=" + this._obj.xRef
            + "&transferType=" + this._obj.transferType
            + "&select=" + this._obj.select
            + '&sessionID=' + this.ics._profile.sessionID
            + '&userID=' + this.ics._profile.userID;
        this.ics.sendBean({ "t1": "rp-popup", "t2": "Information", "t3": url });
    };
    FrmAccountTransferReportComponent.prototype.goClear = function () {
        this._mflag = false;
        this._entryhide = true;
        this._obj = this.getDefaultObj();
        this.setTodayDateObj();
        this.setDateObjIntoString();
        this.getAccountTransferTypes();
    };
    FrmAccountTransferReportComponent.prototype.Validation = function () {
        var _this = this;
        this._mflag = false;
        this.setDateObjIntoString();
        if (this._obj.fromDate > this._obj.toDate) {
            this._mflag = true;
            this.showMsgAlert("From Date Should Not Exceed To Date.");
        }
        else {
            try {
                var url = this.ics._apiurl + 'service001/checkSessionTime';
                this._sessionObj.sessionID = this.ics._profile.sessionID;
                this._sessionObj.userID = this.ics._profile.userID;
                var json = this._sessionObj;
                this.http.doPost(url, json).subscribe(function (data) {
                    _this._mflag = true;
                    if (data != null) {
                        if (data.code == "0016") {
                            _this.showMsgAlert(data.desc);
                            _this.logout();
                        }
                        else if (data.code == "0014") {
                            _this.showMsgAlert(data.desc);
                        }
                        else {
                            _this.goDownload();
                        }
                    }
                }, function (error) {
                    if (error._body.type == "error") {
                        alert("Connection Timed Out.");
                    }
                }, function () { });
            }
            catch (e) {
                alert("Invalid URL.");
            }
        }
    };
    FrmAccountTransferReportComponent.prototype.changeSelectVal = function (options) {
        this._obj.select = options[options.selectedIndex].value;
    };
    FrmAccountTransferReportComponent.prototype.changeTransferType = function (options) {
        var value1 = options[options.selectedIndex].value;
        for (var i = 1; i < this.ref._lov3.refAccountTransferType.length; i++) {
            if (this.ref._lov3.refAccountTransferType[i].value == value1) {
                this._obj.transferType = value1;
                break;
            }
        }
    };
    FrmAccountTransferReportComponent.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    FrmAccountTransferReportComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsgAlert(data.desc);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsgAlert(data.desc);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmAccountTransferReportComponent.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmAccountTransferReportComponent.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    FrmAccountTransferReportComponent = __decorate([
        core_1.Component({
            selector: 'account-transfer-report',
            template: "\n  <div class=\"container\">\n    <div class=\"row clearfix\"> \n      <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n        <form class=\"form-horizontal\" (ngSubmit)=\"goList()\"> \n          <legend>Account Transfer Report</legend>\n          <div class=\"row col-md-12\">  \n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"goClear()\" >Clear</button> \n            <button class=\"btn btn-primary\" type=\"submit\">List</button>      \n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"Validation()\">Download</button>          \n          </div>\n          <div class=\"row col-md-12\">&nbsp;</div>\n          <div class=\"row col-md-12\" id=\"custom-form-alignment-margin\">\n            <div class=\"col-md-6\">\n              <div class=\"form-group\">            \n                <label class=\"col-md-4\">From Date <font class=\"mandatoryfont\">*</font></label>\n                <div class=\"col-md-8\">\n                  <my-date-picker name=\"mydate\"  [options]=\"myDatePickerOptions\" [(ngModel)]=\"_dates.fromDate\" ngDefaultControl></my-date-picker>\n                  <!--<input class=\"form-control\" type=\"date\" [(ngModel)]=\"_obj.fromDate\" required=\"true\" [ngModelOptions]=\"{standalone: true}\">    -->                          \n                </div>\n              </div>\n\n              <div class=\"form-group\">\n                <label class=\"col-md-4\">To Date <font class=\"mandatoryfont\">*</font></label>\n                <div class=\"col-md-8\">\n                  <my-date-picker name=\"mydate\" [options]=\"myDatePickerOptions\" [(ngModel)]=\"_dates.toDate\" ngDefaultControl></my-date-picker>\n                  <!-- <input class=\"form-control\" type=\"date\" [(ngModel)]=\"_obj.toDate\" required=\"true\" [ngModelOptions]=\"{standalone: true}\">  -->                            \n                </div>\n              </div>\n\n              <div class=\"form-group\">              \n                <rp-input rpType=\"text\" rpClass=\"col-md-8\" rpLabelClass =\"col-md-4 control-label\" rpLabel=\"From Account&nbsp;\" [(rpModel)]=\"_obj.fromAccount\" required autofocus></rp-input>\n              </div>\n\n              <div class=\"form-group\">              \n                <rp-input rpType=\"text\" rpClass=\"col-md-8\" rpLabelClass =\"col-md-4 control-label\" rpLabel=\"To Account&nbsp;\" [(rpModel)]=\"_obj.toAccount\" required autofocus></rp-input> \n              </div>           \n            </div>\n\n            <div class=\"col-md-6\">             \n              <div class=\"form-group\">              \n                <rp-input rpType=\"text\" rpClass=\"col-md-8\" rpLabelClass =\"col-md-4 control-label\" rpLabel=\"Transaction Ref No.&nbsp;\" [(rpModel)]=\"_obj.transRefNo\" required autofocus></rp-input>\n              </div>\n\n              <div class=\"form-group\">              \n                <rp-input rpType=\"text\" rpClass=\"col-md-8\" rpLabelClass =\"col-md-4 control-label\" rpLabel=\"XRef&nbsp;\" [(rpModel)]=\"_obj.xRef\" required autofocus></rp-input>\n              </div>\n\n              <div class=\"form-group\">\n                <label class=\"col-md-4\">Transfer Type </label>\n                <div class=\"col-md-8\" > \n                  <select [(ngModel)]=\"_obj.transferType\" [ngModelOptions]=\"{standalone: true}\" (change)=\"changeTransferType($event.target.options)\" class=\"form-control col-md-0\">\n                    <option *ngFor=\"let item of ref._lov3.refAccountTransferType\" value=\"{{item.value}}\" >{{item.caption}}</option>\n                  </select> \n                </div>\n              </div>\n\n              <div class=\"form-group\">\n                <label class=\"col-md-4\"></label>\n                <div class=\"col-md-8\">\n                  <select [(ngModel)]=\"_obj.select\" [ngModelOptions]=\"{standalone: true}\" (change)=\"changeSelectVal($event.target.options)\" class=\"form-control col-md-0\" required=\"true\">\n                    <option *ngFor=\"let item of ref._lov1.ref010\" value=\"{{item.value}}\">{{item.caption}}</option> \n                  </select>                     \n                </div>\n              </div>\n            </div>\n          </div>\n        </form> \n      </div>\n    </div>\n  </div>\n\n  <div [hidden]=\"_entryhide\">\n    <div class=\"form-group\">\n      <div class=\"col-md-9\">\n        <div style=\"margin-top: 10px;\">  \n          <adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"_list.totalCount\" (rpChanged)=\"changedPager($event)\"></adminpager>\n        </div>\n      </div>\n\n      <div class=\"col-md-3\">      \n        <table style=\"align: right; font-size: 14px;\" cellpadding=\"10px\" cellspacing=\"10px\">\n          <tbody>\n            <colgroup>\n              <col span=\"1\" style=\"width: 50%;\">\n              <col span=\"1\" style=\"width: 50%;\">                                  \n            </colgroup>                           \n            <tr>\n              <td style=\"color: blue; text-align: left;\">\n                <label>Grand Total Amount &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;</label>\n              </td>\n              <td style=\"color: blue; display: block; text-align: right;\">\n                <label>{{_totalAmount}}</label>\n              </td>\n            </tr>\n            <tr>\n              <td style=\"color: blue; text-align: left;\">\n                <label>Grand Total Bank Charges &nbsp;&nbsp;:&nbsp;</label>\n              </td>\n              <td style=\"color: blue; display: block; text-align: right;\">\n                <label>{{_totalBankCharges}}</label>\n              </td>\n            </tr>\n          </tbody>\n        </table>\n      </div>\n    </div>\n\n    <div>\n      <table class=\"table table-striped table-condense table-hover tblborder\" style=\"font-size: 14px;\">\n        <thead>\n          <tr>\n            <th align=\"center\"><div style=\"width:150\">Transaction Date</div></th>\n            <th align=\"center\"><div style=\"width:100\">XRef</div></th>\n            <th align=\"center\"><div style=\"width:220\">Transaction Reference No.</div></th>\n            <th align=\"center\"><div style=\"width:150\">Mobile User ID</div></th>\n            <th align=\"center\"><div style=\"width:150\">From Account</div></th>\n            <th align=\"center\"><div style=\"width:150\">To Account</div></th>\n            <th align=\"center\"><div style=\"width:100\">Amount</div></th>\n            <th align=\"center\"><div style=\"width:100\">Bank Charges</div></th>\n            <th align=\"center\"><div style=\"width:100\">Currency Code</div></th>\n          </tr>\n        </thead>\n        <tbody>\n          <tr *ngFor=\"let obj of _list.data\">\n            <td align=\"left\">{{obj.transDate}}</td>\n            <td align=\"left\">{{obj.xRef}}</td>\n            <td align=\"left\">{{obj.transRefNo}}</td>\n            <td align=\"left\">{{obj.mobileUserId}}</td>\n            <td align=\"left\">{{obj.fromAccount}}</td>\n            <td align=\"left\">{{obj.toAccount}}</td>                                     \n            <td align=\"right\">{{obj.amount}}</td>                           \n            <td align=\"right\">{{obj.bankCharges}}</td>\n            <td align=\"right\">{{obj.currency}}</td>\n          </tr> \n        </tbody>                     \n      </table>\n    </div>\n  </div>\n\n  <div [hidden] = \"_mflag\">\n    <div class=\"modal\" id=\"loader\"></div>\n  </div> \n   "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmAccountTransferReportComponent);
    return FrmAccountTransferReportComponent;
}());
exports.FrmAccountTransferReportComponent = FrmAccountTransferReportComponent;
//# sourceMappingURL=frmaccounttransferreport.component.js.map