"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var Rx_1 = require('rxjs/Rx');
core_2.enableProdMode();
var FrmMerchantreportComponent = (function () {
    function FrmMerchantreportComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        // Application Specific
        this.id = "";
        this._totalcount = 1;
        this._entryhide = true;
        this._util = new rp_client_util_1.ClientUtil();
        this.myDatePickerOptions = this._util.getDatePicker();
        this._dates = { "fromDate": this.dateobj, "toDate": this.dateobj };
        this.today = this._util.getTodayDate();
        this._hideDetailTran = true;
        this._hideDetailDownload = true;
        this._mflag = false;
        this._grandAmtTotal = "";
        this._commercialTaxAmt = "";
        this._serviceAmt = "";
        this._deduAmt = "";
        this._checkmerchant = "";
        this._sessionMsg = "";
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "" };
        //_deliveryOrderHidden: boolean;
        //_obj={"aMerchantID":"","aFromDate":"","aToDate":"","select":"EXCEL","totalCount":0,"currentPage":1,"pageSize":10,"msgstatus":"","debitaccount":"","collectionaccount":"","did":""};
        /* _obj = {
          "sessionID": "", "aFeature": "All",
          "aMerchantID": "", "aCustomerID": "", "aFromDate": "", "aToDate": "", "status": "ALL", "select": "EXCEL", "totalCount": 0, "currentPage": 1, "pageSize": 10, "msgstatus": "",
          "debitaccount": "", "collectionaccount": "", "did": "", "customername": "", "description": "", "initiatedby": "ALL", "transrefno": "", "processingCode": "", "proNotiCode": "", "proDisCode": ""
          , "templatedata": [{ "hKey": "", "fieldID": "", "fieldOrder": 0, "dataType": "", "caption": "", "disPayField": "", "fieldvalue": "", "regExpKey": "", "isMandatory": 0, "lovKey": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "", "lovDesc2": "" }] }]
        }; */
        this._obj = this.getDefaultObj();
        this._pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 1 };
        this._list = {
            "data": [{
                    "syskey": 0, "feature": "", "xref": "",
                    "transactiondate": "", "tranrefnumber": "", "trantaxrefnumber": "", "activebranch": "", "debitaccount": "", "debitbranch": "", "collectionaccount": "",
                    "branch": "", "amountst": "", "commchargest": "", "commuchargest": "", "customerCode": "", "customerNumber": "", "fileUploadDateTime": "",
                    "chequeNo": "", "currencyCode": "", "t7": "", "t8": "", "t10": "", "t11": "", "t12": "", "t13": "", "t15": "", "t16": "", "t17": "", "t19": "", "t20": "", "t21": "", "t22": "",
                    "t23": "", "t24": "", "t25": "", "n1": 0, "n7": 0, "n8": 0, "n9": 0, "n10": 0, "n17": 0, "n18": 0, "n19": 0, "n20": 0, "distributorName": "", "distributorAddress": "", "messageCode": "", "messageDesc": ""
                }],
            "grandTotal": "", "commericalAmt": "", "serviceAmt": "", "deduAmt": "", "totalCount": 0, "currentPage": 1, "pageSize": 10, "detailData": [{ "payCaption": "", "paydatatype": "", "paydispayfield": "" }]
        };
        this._mInfo = { "loginUser": "", "merchantId": "", "merchantName": "" };
        this._mObject = {
            "sysKey": 0, "merchantID": "", "name": "", "processingCode": "", "proNotiCode": "", "proDisCode": "", "detailOrder": "", "detailLov": [{ "srno": 0, "lovCde": "", "lovDesc1": "", "price": 0 }],
            "detailData": [{ "sysKey": 0, "hKey": 0, "srno": 1, "code": "", "description": "", "price": 0, "qty": 0, "amount": 0 }], "data": [{ "hKey": "", "fieldID": "", "fieldOrder": 0, "dataType": "", "caption": "", "disPayField": "", "fieldvalue": "", "regExpKey": "", "isMandatory": 0, "lovKey": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "", "lovDesc2": "" }] }],
        };
        this.loginUser = "";
        this._listpopup = { "arr": [{ "syskey": 0, "hkey": 0, "srno": 0, "code": "", "description": "", "price": "", "quantity": 0, "amount": "" }] };
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = false;
            this.checkSession();
            this.ics.confirmUpload(false);
            this.loginUser = this.ics._profile.userID;
            this.getMerchantIdByloginUser(this.loginUser);
            this.getmerchantidlist();
            this.getAllFeature();
            this.getStatus();
            this.getInitiatedBy();
            this.messagehide = true;
            this._mInfo = { "loginUser": "", "merchantId": "", "merchantName": "" };
            //this._obj.aMerchantID = 
            this._dates.fromDate = this._util.changestringtodateobject(this.today);
            this._dates.toDate = this._util.changestringtodateobject(this.today);
            var merchantID = this._mInfo.merchantId;
            this._mObject = {
                "sysKey": 0, "merchantID": "", "name": "", "processingCode": "", "proNotiCode": "", "proDisCode": "", "detailOrder": "", "detailLov": [{ "srno": 0, "lovCde": "", "lovDesc1": "", "price": 0 }],
                "detailData": [{ "sysKey": 0, "hKey": 0, "srno": 1, "code": "", "description": "", "price": 0, "qty": 0, "amount": 0 }], "data": [{ "hKey": "", "fieldID": "", "fieldOrder": 0, "dataType": "", "caption": "", "disPayField": "", "fieldvalue": "", "regExpKey": "", "isMandatory": 0, "lovKey": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "", "lovDesc2": "" }] }]
            };
        }
    }
    FrmMerchantreportComponent.prototype.newFunction = function () {
        return this._dates.fromDate;
    };
    FrmMerchantreportComponent.prototype.getMerchantIdByloginUser = function (loginUser) {
        var _this = this;
        this._mflag = false;
        this._mInfo.loginUser = loginUser;
        var json = this._mInfo;
        var url = this.ics._apiurl + 'service001/getMerchantIdByloginUser';
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                _this._mInfo = data;
                //this._obj.aMerchantID = this._mInfo.merchantId;
                _this.getPortalFormControl(_this._obj.aMerchantID);
            }
            _this._mflag = true;
        });
    };
    FrmMerchantreportComponent.prototype.showdetailspopup = function (syskey) {
        var _this = this;
        this._listpopup.arr = [];
        if (this.ics._apiurl != "") {
            this.http.doGet(this.ics._apiurl + 'service001/getshowdetails?syskey=' + syskey).subscribe(function (data) {
                _this._listpopup.arr = data.detailsData;
                if (data != null) {
                    if (data.detailsData != null) {
                        if (!(data.detailsData instanceof Array)) {
                            var m = [];
                            m[0] = data.detailsData;
                            data.detailsData = m;
                        }
                        _this._listpopup.arr = data.detailsData;
                    }
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        jQuery("#lu001popup").modal();
    };
    FrmMerchantreportComponent.prototype.changedPager = function (event) {
        this._pgobj = event;
        this._obj.currentPage = this._pgobj.current;
        this._obj.pageSize = this._pgobj.size;
        this._obj.totalCount = this._pgobj.totalcount;
        if (this._pgobj.totalcount > 1) {
            this.goList();
        }
    };
    FrmMerchantreportComponent.prototype.date = function () {
        this._obj.aFromDate = this._util.getDatePickerDate(this._dates.fromDate);
        this._obj.aToDate = this._util.getDatePickerDate(this._dates.toDate);
    };
    FrmMerchantreportComponent.prototype.goList = function () {
        var _this = this;
        this._mflag = false;
        this.date();
        var url = this.ics._apiurl + 'service001/getMerchantList';
        var capValue = "";
        this._obj.sessionID = this.ics._profile.sessionID;
        this._grandAmtTotal = "0.00";
        this._commercialTaxAmt = "0.00";
        this._deduAmt = "0.00";
        this._serviceAmt = "0.00";
        var json = this._obj;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data.msgCode == "0016") {
                _this._sessionMsg = data.msgDesc;
                _this.showMessage();
            }
            else {
                _this._entryhide = false;
                _this._totalcount = data.totalCount;
                _this._obj.currentPage = data.currentPage;
                _this._obj.pageSize = data.pageSize;
                _this._obj.totalCount = data.totalCount;
                if (_this._obj.aFromDate > _this._obj.aToDate) {
                    _this._obj.msgstatus = "From Date must not exceed To Date!";
                    _this.ics.sendBean({ "t1": "rp-msg", t2: "Information", t3: _this._obj.msgstatus });
                    _this.goClear();
                }
                else {
                    if (_this._obj.totalCount == 0) {
                        _this._obj.msgstatus = "Data not Found!";
                        _this._entryhide = true;
                        _this.ics.sendBean({ "t1": "rp-msg", t2: "Information", t3: _this._obj.msgstatus });
                    }
                }
                if (data != null) {
                    _this._list = data;
                    if (data.data != null) {
                        if (!(data.data instanceof Array)) {
                            var m = [];
                            m[0] = data.data;
                            data.data = m;
                        }
                        _this._grandAmtTotal = _this._list.grandTotal;
                        _this._commercialTaxAmt = _this._list.commericalAmt;
                        _this._serviceAmt = _this._list.serviceAmt;
                        _this._deduAmt = _this._list.deduAmt;
                        _this._list.data = data.data;
                    }
                    if (data.detailData != null) {
                        if (!(data.detailData instanceof Array)) {
                            var m = [];
                            m[0] = data.detailData;
                            data.detailData = m;
                        }
                    }
                    _this._list.detailData = data.detailData;
                }
            }
            _this._mflag = true;
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
            else {
            }
        }, function () { });
    };
    FrmMerchantreportComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this._router.navigate(['Login', , { p1: '*' }]);
            jQuery("#sessionalert").modal('hide');
        });
    };
    //   routerCanDeactivate(next: ComponentInstruction, prev: ComponentInstruction) {
    //     return true;
    //   }
    FrmMerchantreportComponent.prototype.messagealert = function () {
        var _this = this;
        this.messagehide = false;
        setTimeout(function () { return _this.messagehide = true; }, 3000);
    };
    FrmMerchantreportComponent.prototype.getDefaultObj = function () {
        return {
            "sessionID": "", "aFeature": "All",
            "aMerchantID": "", "aCustomerID": "", "aFromDate": "", "aToDate": "", "status": "ALL", "select": "EXCEL", "totalCount": 0, "currentPage": 1, "pageSize": 10, "msgstatus": "",
            "debitaccount": "", "collectionaccount": "", "did": "", "customername": "", "description": "", "initiatedby": "ALL", "transrefno": "", "processingCode": "", "proNotiCode": "", "proDisCode": "",
            "templatedata": [{ "hKey": "", "fieldID": "", "fieldOrder": 0, "dataType": "", "caption": "", "disPayField": "", "fieldvalue": "", "regExpKey": "", "isMandatory": 0, "lovKey": 0,
                    "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "", "lovDesc2": "" }] }]
        };
    };
    FrmMerchantreportComponent.prototype.goDownload = function () {
        this.date();
        var templateData = "";
        var formatJSP = "";
        var merID = encodeURI(this._obj.aMerchantID);
        if (this._obj.aMerchantID != this._checkmerchant) {
            this._obj.msgstatus = "Please select merchant!";
            this.ics.sendBean({ "t1": "rp-msg", t2: "Information", t3: this._obj.msgstatus });
        }
        else {
            if (this._mObject.processingCode == '01' || this._mObject.processingCode == '03') {
                for (var k = 0; k < this._obj.templatedata.length; k++) {
                    if (k == 0) {
                        templateData = "fid!" + this._obj.templatedata[k].fieldID + "!!fval!" + this._obj.templatedata[k].fieldvalue + "!!disField!" + this._obj.templatedata[k].disPayField + "!!lovkey!" + this._obj.templatedata[k].lovKey;
                    }
                    else {
                        templateData = templateData + "@@fid!" + this._obj.templatedata[k].fieldID + "!!fval!" + this._obj.templatedata[k].fieldvalue + "!!disField!" + this._obj.templatedata[k].disPayField + "!!lovkey!" + this._obj.templatedata[k].lovKey;
                    }
                }
            }
            if (this._mObject.detailOrder == 'lov') {
                formatJSP = "merchantdetailsexcel.jsp";
            }
            else {
                formatJSP = "merchant.jsp";
            }
            this.ics.sendBean({ "t1": "rp-msg", t2: "Information ", t3: "Please wait ..." });
            var url = this.ics._rpturl + formatJSP + "?aMerchantID=" + this._obj.aMerchantID + "&aFeature=" + this._obj.aFeature + "&debitaccount=" + this._obj.debitaccount
                + "&collectionaccount=" + this._obj.collectionaccount + "&" +
                "aFromDate=" + this._obj.aFromDate + "&" + "aToDate=" + this._obj.aToDate + "&" + "aSelect=" + this._obj.select + "&" + "did=" + this._obj.did
                + "&initiatedby=" + this._obj.initiatedby + "&transrefno=" + this._obj.transrefno + "&processingCode=" + this._obj.processingCode
                + "&proNotiCode=" + this._obj.proNotiCode + "&proDisCode=" + this._obj.proDisCode + "&templatedata=" + templateData + "&status=" + this._obj.status + '&sessionID=' + this.ics._profile.sessionID;
            this.ics.sendBean({ "t1": "rp-popup", "t2": "Information", "t3": url });
        }
    };
    FrmMerchantreportComponent.prototype.goDownloadDetail = function () {
        var templateData = "";
        if (this._mObject.processingCode == '01' || this._mObject.processingCode == '03') {
            for (var k = 0; k < this._obj.templatedata.length; k++) {
                if (k == 0) {
                    templateData = "fid!" + this._obj.templatedata[k].fieldID + "!!fval!" + this._obj.templatedata[k].fieldvalue + "!!disField!" + this._obj.templatedata[k].disPayField + "!!lovkey!" + this._obj.templatedata[k].lovKey;
                }
                else {
                    templateData = templateData + "@@fid!" + this._obj.templatedata[k].fieldID + "!!fval!" + this._obj.templatedata[k].fieldvalue + "!!disField!" + this._obj.templatedata[k].disPayField + "!!lovkey!" + this._obj.templatedata[k].lovKey;
                }
            }
        }
        this.ics.sendBean({ "t1": "rp-msg", t2: "Information ", t3: "Please wait ..." });
        var url = this.ics._rpturl + "merchantdetailsexcel.jsp?aMerchantID=" + this._obj.aMerchantID + "&debitaccount=" + this._obj.debitaccount
            + "&collectionaccount=" + this._obj.collectionaccount + "&" +
            "aFromDate=" + this._obj.aFromDate + "&" + "aToDate=" + this._obj.aToDate + "&" + "aSelect=" + this._obj.select + "&" + "did=" + this._obj.did
            + "&initiatedby=" + this._obj.initiatedby + "&transrefno=" + this._obj.transrefno + "&processingCode=" + this._obj.processingCode
            + "&proNotiCode=" + this._obj.proNotiCode + "&proDisCode=" + this._obj.proDisCode + "&templatedata=" + templateData;
        this.ics.sendBean({ "t1": "rp-popup", "t2": "Information", "t3": url });
    };
    FrmMerchantreportComponent.prototype.goClear = function () {
        this._entryhide = true;
        var tmptemplateObj = this._obj.templatedata;
        var tmpprocessingCode = this._obj.processingCode;
        var tmpproNotiCode = this._obj.proNotiCode;
        var tmpproDisCode = this._obj.proDisCode;
        var merchantID = this._obj.aMerchantID;
        this._obj = this.getDefaultObj();
        this._obj.processingCode = tmpprocessingCode;
        this._obj.proNotiCode = tmpproNotiCode;
        this._obj.proDisCode = tmpproDisCode;
        this._obj.templatedata = tmptemplateObj;
        this._obj.aMerchantID = merchantID;
        this._dates.fromDate = this._util.changestringtodateobject(this.today);
        this._dates.toDate = this._util.changestringtodateobject(this.today);
    };
    FrmMerchantreportComponent.prototype.Validation = function () {
        var _this = this;
        this.date();
        try {
            var url = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
            this.http.doGet(url).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this._sessionMsg = data.msgDesc;
                    _this.showMessage();
                }
                else {
                    if (_this._obj.aMerchantID == "") {
                        _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Please Type MerchantID!" });
                    }
                    else if (_this._obj.aFromDate.length == 0) {
                        _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "From Date should not blank!" });
                    }
                    else if (_this._obj.aToDate.length == 0) {
                        _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "To Date should not blank!" });
                    }
                    else if (_this._obj.aFromDate > _this._obj.aToDate) {
                        _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "From Date must not exceed ToDate!" });
                        _this.goClear();
                    }
                    else {
                        _this.goDownload();
                    }
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmMerchantreportComponent.prototype.ValidationDetails = function () {
        if (this._obj.aMerchantID == "") {
            this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Please Type MerchantID!" });
        }
        else if (this._obj.aFromDate > this._obj.aToDate) {
            this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "From Date must not exceed ToDate!" });
            this.goClear();
        }
        else {
            this.goDownloadDetail();
        }
    };
    FrmMerchantreportComponent.prototype.changeInitiatedVal = function (options) {
        this.selectedOptions = Array.apply(null, options) // convert to real array
            .filter(function (option) { return option.selected; })
            .map(function (option) { return option.value; });
        this._obj.initiatedby = this.selectedOptions[0];
    };
    FrmMerchantreportComponent.prototype.changeSelectVal = function (options) {
        this.selectedOptions = Array.apply(null, options) // convert to real array
            .filter(function (option) { return option.selected; })
            .map(function (option) { return option.value; });
        this._obj.select = this.selectedOptions[0];
    };
    FrmMerchantreportComponent.prototype.getPortalFormControl = function (merchantId) {
        var _this = this;
        this._mObject.merchantID = merchantId;
        var url = this.ics._apiurl + 'service001/getPortalFormControlByID';
        var json = this._mObject;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                _this._mObject = data;
                _this._obj.processingCode = _this._mObject.processingCode;
                _this._obj.proNotiCode = _this._mObject.proNotiCode;
                _this._obj.proDisCode = _this._mObject.proDisCode;
                if (_this._mObject.detailOrder == "text" || _this._mObject.detailOrder == "lov") {
                    _this._hideDetailTran = false;
                    _this._hideDetailDownload = false;
                }
                //this._obj.templatedata = this._mObject.data;
                if (data.data != null) {
                    if (!(data.data instanceof Array)) {
                        var m = [];
                        m[0] = data.data;
                        _this._mObject.data = m;
                    }
                    else {
                        _this._mObject.data = data.data;
                    }
                    _this._obj.templatedata = [];
                    for (var i = 0; i < _this._mObject.data.length; i++) {
                        if (_this._mObject.data[i].dataType == "lov") {
                            if (!(_this._mObject.data[i].lovData instanceof Array)) {
                                var n = [{ "srno": "1", "lovCde": "", "lovDesc1": "", "lovDesc2": "" }];
                                n[0] = data.data[i].lovData;
                                _this._mObject.data[i].lovData = n;
                            }
                            else {
                                _this._mObject.data[i].lovData = data.data[i].lovData;
                            }
                            if (_this._mObject.processingCode == '01' || _this._mObject.processingCode == '03') {
                                //01 is Telenor 03 is Myanmar Beer
                                //Add ---ALL--- in Lov and item selected that '---ALL---' when initailize load form 
                                if (_this._mObject.data[i].fieldID == 'R4' || _this._mObject.data[i].fieldID == 'R5') {
                                    var lovObj = [{ "srno": "1", "lovCde": "000", "lovDesc1": "ALL", "lovDesc2": "" }];
                                    for (var j = 0; j < _this._mObject.data[i].lovData.length; j++) {
                                        lovObj.push({ "srno": j + 1 + "", "lovCde": _this._mObject.data[i].lovData[j].lovCde, "lovDesc1": _this._mObject.data[i].lovData[j].lovDesc1, "lovDesc2": "" });
                                    }
                                    _this._mObject.data[i].lovData = lovObj;
                                    _this._mObject.data[i].fieldvalue = _this._mObject.data[i].lovData[0].lovCde;
                                    _this._obj.templatedata.push(_this._mObject.data[i]);
                                }
                            }
                        }
                    }
                }
                if (_this._mObject.processingCode == '01') {
                    _this._customerTypeHidden = true;
                }
            }
        });
    };
    FrmMerchantreportComponent.prototype.changePaymentType = function (options, index) {
        this.selectedOptions = Array.apply(null, options) // convert to real array
            .filter(function (option) { return option.selected; })
            .map(function (option) { return option.value; });
        this._obj.templatedata[index].fieldvalue = this.selectedOptions[0];
        if (this._obj.templatedata[index].lovData != null) {
            if (this.selectedOptions[0] == this._obj.templatedata[index].lovData[2].lovCde) {
                //Is PostPaid
                this._customerTypeHidden = false;
            }
            else {
                //Other
                for (var i = 0; i < this._obj.templatedata.length; i++) {
                    if (this._obj.templatedata[i].fieldID == 'R5') {
                        this._obj.templatedata[i].fieldvalue = this._obj.templatedata[i].lovData[0].lovCde;
                    }
                }
                this._customerTypeHidden = true;
            }
        }
    };
    FrmMerchantreportComponent.prototype.changeCustomerType = function (options, index) {
        this.selectedOptions = Array.apply(null, options) // convert to real array
            .filter(function (option) { return option.selected; })
            .map(function (option) { return option.value; });
        this._obj.templatedata[index].fieldvalue = this.selectedOptions[0];
    };
    FrmMerchantreportComponent.prototype.changeMMBearyPaymentType = function (options, index) {
        this.selectedOptions = Array.apply(null, options) // convert to real array
            .filter(function (option) { return option.selected; })
            .map(function (option) { return option.value; });
        this._obj.templatedata[index].fieldvalue = this.selectedOptions[0];
    };
    FrmMerchantreportComponent.prototype.getmerchantidlist = function () {
        var _this = this;
        this._mflag = false;
        this.http.doGet(this.ics._apiurl + 'service001/getmerchantidlistdetail?userID=' + this.loginUser).subscribe(function (data) {
            _this.ref._lov3.ref015 = data.ref015;
            // let merchant = [{ "value": "", "caption": ""}];
            if (_this.ref._lov3.ref015 != null) {
                if (!(_this.ref._lov3.ref015 instanceof Array)) {
                    var m = [];
                    m[1] = _this.ref._lov3.ref015;
                }
            }
            //this._obj.aMerchantID =  this.ref._lov3.ref015[0].value;
            //this._obj.processingCode = this.ref._lov3.ref015[0].processingCode;
            _this._mflag = true;
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
            else {
            }
        }, function () { });
    };
    // getmerchantidlist() {
    //   this.http.doGet(this.ics._apiurl + 'service001/getmerchantidlistdetail?userID=' + this.loginUser).subscribe(
    //     data => {
    //       let merchant = [{"value":"","caption":""}];
    //      if (!(data.ref015 instanceof Array)) {
    //         let m = [];
    //         m[1] = data.ref015;
    //         merchant.push({"value":  m[1].value  ,"caption": m[1].caption });
    //         this.ref._lov3.ref015 = merchant;
    //       }
    //      else  if (data.ref015 != null && data.ref015 != undefined) {          
    //         for(let i=0 ; i < data.ref015.length ; i++){
    //           merchant.push({"value":  data.ref015[i].value  ,"caption": data.ref015[i].caption });
    //         }
    //         this.ref._lov3.ref015 = data.ref015;
    //         this.ref._lov3.ref015 = merchant;
    //       }
    //       else {
    //         this.ref._lov3.ref015 = [{ "value": "0", "caption": "" }];
    //       }
    //     },
    //     error => alert(error),
    //     () => { }
    //   );
    // }
    FrmMerchantreportComponent.prototype.changeStatus = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex; //Get Selected Index
        var value = options[options.selectedIndex].value; //Get Selected Index's Value    
        this._obj.status = value;
        for (var i = 0; i < this.ref._lov3.ref013.length; i++) {
            if (this.ref._lov3.ref013[i].value == value) {
                this._obj.status = this.ref._lov3.ref013[i].value;
                break;
            }
        }
    };
    FrmMerchantreportComponent.prototype.getStatus = function () {
        var _this = this;
        this._mflag = false;
        this.http.doGet(this.ics._apiurl + 'service001/getStatus?userID=' + this.loginUser).subscribe(function (data) {
            _this.ref._lov3.ref013 = data.ref013;
            // let merchant = [{ "value": "", "caption": ""}];
            var arr = [];
            if (_this.ref._lov3.ref013 != null) {
                if (!(_this.ref._lov3.ref013 instanceof Array)) {
                    var m = [];
                    m[0] = _this.ref._lov3.ref013;
                    _this.ref._lov3.ref013 = m;
                }
                else {
                    for (var j = 0; j < _this.ref._lov3.ref013.length; j++) {
                        arr.push(_this.ref._lov3.ref013[j]);
                    }
                }
            }
            _this._mflag = true;
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
            else {
            }
        }, function () { });
    };
    FrmMerchantreportComponent.prototype.changeInitiated = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex; //Get Selected Index
        var value = options[options.selectedIndex].value; //Get Selected Index's Value    
        this._obj.initiatedby = value;
        for (var i = 0; i < this.ref._lov3.ref012.length; i++) {
            if (this.ref._lov3.ref012[i].value == value) {
                this._obj.initiatedby = this.ref._lov3.ref012[i].value;
                break;
            }
        }
    };
    FrmMerchantreportComponent.prototype.getInitiatedBy = function () {
        var _this = this;
        this._mflag = false;
        this.http.doGet(this.ics._apiurl + 'service001/getInitiatedBy?userID=' + this.loginUser).subscribe(function (data) {
            _this.ref._lov3.ref012 = data.ref012;
            var arr = [];
            if (_this.ref._lov3.ref012 != null) {
                if (!(_this.ref._lov3.ref012 instanceof Array)) {
                    var m = [];
                    m[0] = _this.ref._lov3.ref012;
                    _this.ref._lov3.ref012 = m;
                }
                else {
                    for (var j = 0; j < _this.ref._lov3.ref012.length; j++) {
                        arr.push(_this.ref._lov3.ref012[j]);
                    }
                }
            }
            _this._mflag = true;
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
            else {
            }
        }, function () { });
    };
    FrmMerchantreportComponent.prototype.changeMerchant = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex; //Get Selected Index
        var value = options[options.selectedIndex].value; //Get Selected Index's Value    
        this._obj.aMerchantID = value;
        this._checkmerchant = value;
        for (var i = 0; i < this.ref._lov3.ref015.length; i++) {
            if (this.ref._lov3.ref015[i].value == value) {
                this._obj.aMerchantID = this.ref._lov3.ref015[i].value;
                this._obj.processingCode = this.ref._lov3.ref015[i].processingCode;
                break;
            }
        }
    };
    FrmMerchantreportComponent.prototype.getAllFeature = function () {
        var _this = this;
        try {
            this._mflag = false;
            this.http.doGet(this.ics._apiurl + 'service001/getAllFeatures').subscribe(function (data) {
                if (data != null && data != undefined) {
                    var combo = [{ "value": "All", "caption": "All" }];
                    for (var i = 0; i < data.refFeature.length; i++) {
                        combo.push({ "value": data.refFeature[i].value, "caption": data.refFeature[i].caption });
                    }
                    _this.ref._lov3.refFeature = combo;
                }
                else {
                    _this.ref._lov3.refFeature = [{ "value": "All", "caption": "All" }];
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmMerchantreportComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
            this.http.doGet(url).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this._sessionMsg = data.msgDesc;
                    _this.showMessage();
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmMerchantreportComponent = __decorate([
        core_1.Component({
            selector: 'frmmerchantreport',
            template: "\n<div class=\"container\">\n    <div class=\"row clearfix\"> \n        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n\t\t <form class= \"form-horizontal\" (ngSubmit) = \"goList()\"> \n            <legend>Merchant Transaction Report</legend>\n                <div class=\"row  col-md-12\">  \n                    <button class=\"btn btn-primary\" type=\"button\" (click)=\"goClear()\" >Clear</button> \n                    <button class=\"btn btn-primary\" type=\"submit\">List</button>      \n                    <button class=\"btn btn-primary\" type=\"button\" (click)=\" Validation();\">Download</button>          \n                </div>\n                <div class=\"row col-md-12\">&nbsp;</div>\n\n                <div class=\"row col-md-12\" id=\"custom-form-alignment-margin\">   \n                  <div class=\"col-md-6\">           \n                    <div class=\"form-group\">            \n                        <label class=\"col-md-4\" > From Date <font class=\"mandatoryfont\">*</font></label>\n                        <div class=\"col-md-8\">\n                            <my-date-picker name=\"mydate\" [options]=\"myDatePickerOptions\" [(ngModel)]=\"_dates.fromDate\" ngDefaultControl ></my-date-picker>\n                        </div>\n                    </div>            \n            \n                    <div class=\"form-group\">\n                      <label class=\"col-md-4\" > To Date <font class=\"mandatoryfont\">*</font></label>\n                      <div class=\"col-md-8\">\n                          <my-date-picker name=\"mydate\" [options]=\"myDatePickerOptions\" [(ngModel)]=\"_dates.toDate\" ngDefaultControl></my-date-picker>\n                      </div>\n                    </div>\n            \n                    <div class=\"form-group\">\n                    <label class=\"col-md-4\">Merchant&nbsp; <font class=\"mandatoryfont\">*</font></label>\n                    <div class=\"col-md-8\" > \n                        <select [(ngModel)]=\"_obj.aMerchantID\" [ngModelOptions]=\"{standalone: true}\" (change)=\"changeMerchant($event)\"  class=\"form-control col-md-0\" required>\n                          <option *ngFor=\"let item of ref._lov3.ref015\" value=\"{{item.value}}\" >{{item.caption}}</option>\n                        </select> \n                    </div>             \n                  </div>\n\n                    <div class=\"form-group\">\n                      <label class=\"col-md-4\">Initiated By&nbsp;<font class=\"mandatoryfont\">*</font></label>\n                        <div class=\"col-md-8\" > \n                            <select [(ngModel)]=\"_obj.initiatedby\" [ngModelOptions]=\"{standalone: true}\" (change)=\"changeInitiated($event)\"  class=\"form-control col-md-0\" required>\n                              <option *ngFor=\"let item of ref._lov3.ref012\" value=\"{{item.value}}\" >{{item.caption}}</option>\n                            </select> \n                        </div>             \n                    </div>\n               \n                    <div class=\"form-group\">\n                      <label class=\"col-md-4\">Status &nbsp;<font class=\"mandatoryfont\">*</font></label>\n                        <div class=\"col-md-8\" > \n                            <select [(ngModel)]=\"_obj.status\" [ngModelOptions]=\"{standalone: true}\" (change)=\"changeStatus($event)\"  class=\"form-control col-md-0\" required>\n                              <option *ngFor=\"let item of ref._lov3.ref013\" value=\"{{item.value}}\" >{{item.caption}}</option>\n                            </select> \n                        </div>             \n                    </div>\n            \n                    <div class=\"form-group\">\n                      <label class=\"col-md-4\">Feature&nbsp; <font class=\"mandatoryfont\">*</font></label>\n                        <div class=\"col-md-8\" > \n                            <select [(ngModel)]=\"_obj.aFeature\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control col-md-0\" required>\n                              <option *ngFor=\"let item of ref._lov3.refFeature\" value=\"{{item.value}}\" >{{item.caption}}</option>\n                            </select> \n                        </div>             \n                    </div>\n\t\t\t\t\t\n                   </div>\n\n                  <div class=\"col-md-6\">             \n                    <div class=\"form-group\">              \n                      <rp-input rpType=\"text\" rpClass=\"col-md-8\" rpLabelClass =\"col-md-4 control-label\" rpLabel=\"Debit Account&nbsp;\" [(rpModel)]=\"_obj.debitaccount\"  required autofocus  ></rp-input> \n                    </div>\n\n                    <div class=\"form-group\">\n                      <rp-input rpType=\"text\" rpClass=\"col-md-8\" rpLabelClass =\"col-md-4 control-label\" rpLabel=\"Collection Account&nbsp;\" [(rpModel)]=\"_obj.collectionaccount\"  required autofocus></rp-input>\n                    </div>\n      \n                    <div class=\"form-group\">               \n                      <rp-input rpType=\"text\" rpClass=\"col-md-8\" rpLabelClass =\"col-md-4 control-label\" rpLabel=\"Customer Code&nbsp;\" [(rpModel)]=\"_obj.did\"  required autofocus  ></rp-input>             \n                    </div>\n                \n                    <div class=\"form-group\">              \n                      <rp-input rpType=\"text\" rpClass=\"col-md-8\" rpLabelClass =\"col-md-4 control-label\" rpLabel=\"Transaction Ref No.&nbsp;\" [(rpModel)]=\"_obj.transrefno\"  required autofocus  ></rp-input>              \n                    </div>\n\n                    <div class=\"form-group\">\n                      <rp-input rpClass=\"col-md-8\" rpLabelClass =\"col-md-4 control-label\"  rpType=\"ref010\"  [(rpModel)]=\"_obj.select\" (change)=\"changeSelectVal($event.target.options)\"></rp-input>\n                    </div>            \n                  </div>\n                </div>      \n            </form> \n        </div>\n      </div>\n</div>\n\n<div [hidden]=\"_entryhide\">\n<div class = \"form-group\">\n  <div class=\"form-group\">\n    <div class=\"col-md-9\">\n      <div style = \"margin-top : 10px\">  \n        <adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"_list.totalCount\" (rpChanged)=\"changedPager($event)\" ></adminpager>\n      </div>\n    </div>\n  \n    <div class = \"col-md-3\">      \n      <table style=\"align:right;font-size:14px\" cellpadding=\"10px\" cellspacing=\"10px\">\n        <tbody>\n          <colgroup>\n            <col span=\"1\" style=\"width: 50%;\">\n            <col span=\"1\" style=\"width: 50%;\">                                  \n          </colgroup>                           \n          <tr>\n            <td style=\"color:blue ; text-align:left\">\n              <label >Grand Total Amount &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;</label>\n            </td>\n            <td style=\"color:blue;display:block; text-align:right; \">\n              <label >{{_grandAmtTotal}}</label>\n            </td>\n          </tr>\n          <tr>\n            <td style=\"color:blue; text-align:left\">\n              <label >Grand Total Service Charges &nbsp;:&nbsp;</label>  \n            </td>\n            <td  style=\"color:blue;display:block; text-align:right; \">\n              <label >{{_serviceAmt}}</label>\n            </td>\n          </tr>\n          <tr *ngIf = \"_obj.processingCode == '080400'\" >\n            <td style=\"color:blue ; text-align:left\">\n              <label >Grand Total Tax Charges &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;</label>\n            </td>\n            <td style=\"color:blue;display:block; text-align:right; \">\n              <label >{{_commercialTaxAmt}}</label>\n            </td>\n          </tr>\n          <!-- wcs -->\n          <tr *ngIf = \"_obj.processingCode == '050200' || _obj.processingCode == '040300'\">\n          <td style=\"color:blue ; text-align:left\">\n          <label >Grand Total Deduction Charges &nbsp; : &nbsp; </label>\n        </td>\n        <td style=\"color:blue;display:block; text-align:right; \">\n          <label >{{_deduAmt}}</label>\n        </td>\n      </tr>\n        </tbody>\n      </table>\n    </div>\n  </div><!-- second form-group close tag -->\n  <div>\n    <table  class=\"table table-striped table-condense table-hover tblborder\" style=\"font-size: 14px;\" >\n      <thead>\n        <tr>\n          <th align=\"center\"><div style=\"width:150\">Transaction Date</div></th>\n          <th align=\"center\"><div style=\"width:100\">XRef</div></th>\n          <th align=\"center\"  *ngIf = \"_obj.processingCode != '080400'  && _obj.processingCode != '050200'\"><div style=\"width:220\">Transaction Reference No. </div></th>\n          <th align=\"center\"  *ngIf = \"_obj.processingCode != '080400' && _obj.processingCode == '050200' && _obj.aFeature != '2'\"><div style=\"width:220\">Transaction Reference No. </div></th>\n          <th align=\"center\"  *ngIf = \"_obj.processingCode == '080400'\"><div style=\"width:220\">Transaction Reference No. 1</div></th>\n          <th align=\"center\"  *ngIf = \"_obj.processingCode == '080400'\"><div style=\"width:220\" >Transaction Reference No. 2</div></th>\n          <th align=\"center\"  *ngIf = \"_obj.processingCode == '050200' && _obj.aFeature == '2'\"><div style=\"width:220\">Transaction Reference No. 1</div></th>\n          <th align=\"center\"  *ngIf = \"_obj.processingCode == '050200' && _obj.aFeature == '2'\"><div style=\"width:220\" >Transaction Reference No. 2</div></th>\n\n          <th align=\"center\"><div style=\"width:140\">Debit Account</div></th>\n          <th align=\"center\"><div style=\"width:150\">Collection Account</div></th>\n          <th align=\"center\"><div style=\"width:150\">Customer Code</div></th>\n          <th align=\"center\"><div style=\"width:150\">Customer Name</div></th>\n          <th align=\"center\"><div style=\"width:100\">Initiated By</div></th>\n          <th align=\"right\"><div style=\"width:100\">Amount</div></th>\n          <th align=\"right\"><div style=\"width:150\">Service Charges</div></th>\n          <th align=\"right\" *ngIf = \"_obj.processingCode == '080400'\"><div style=\"width:150\">Commercial Tax</div></th>\n          <th align=\"right\" *ngIf = \"_obj.processingCode != '080400' && _obj.processingCode != '090500'\" ><div style=\"width:150\">Deduction Amount</div></th>\n          <th align=\"center\"><div style=\"width:200\">TPS Message</div></th>\n          <th align=\"center\"><div style=\"width:150\">Feature</div></th>\n        </tr>\n      </thead>\n        <tbody>\n          <tr *ngFor=\"let obj of _list.data\">\n            <td align=\"left\">{{obj.transactiondate}}</td>\n            <td align=\"left\">{{obj.xref}}</td>\n            <td align=\"left\" >{{obj.tranrefnumber}}</td> \n            <td align=\"left\" *ngIf = \"_obj.processingCode == '080400' \" >{{obj.trantaxrefnumber}}</td> \n            <td align=\"left\" *ngIf = \"_obj.processingCode == '050200' && _obj.aFeature == '2' \" >{{obj.trantaxrefnumber}}</td> \n            <td align=\"left\">{{obj.debitaccount}}</td>\n            <td align=\"left\">{{obj.collectionaccount}}</td>\n            <td align=\"left\">{{obj.did}}</td>\n            <td align=\"left\">{{obj.customername}}</td>   \n            <td align=\"left\" >{{obj.branch}}</td>                                       \n            <td align = \"right\">{{obj.amountst}}</td><!-- amount -->\n            <td align = \"right\">{{obj.commchargest}}</td><!-- service charges -->\n            <td align=\"right\" *ngIf = \"_obj.processingCode == '080400'\">{{obj.commuchargest}}</td>\n            <td align=\"right\" *ngIf = \"_obj.processingCode != '080400' && _obj.processingCode != '090500'\" >{{obj.commuchargest}}</td>\n            <td align = \"left\">{{obj.messageCode}} : {{obj.messageDesc}} </td>                             \n            <td  align=\"left\" >{{obj.feature}}</td>\n          </tr> \n        </tbody>                     \n    </table> \n</div>              \n</div>\n</div> \n <!-- End -->\n \n <!-- Start Popup -->\n \n <div id=\"lu001popup\" class=\"modal fade\" role=\"dialog\">\n      <div id=\"lu001popupsize\" class=\"modal-dialog modal-lg\">  \n        <div class=\"modal-content\">\n          <div class=\"modal-header\">\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n            <h4 id=\"lu001popuptitle\" class=\"modal-title\">Details of Myanmar Brewery</h4>\n          </div> \n          <div id=\"lu001popupbody\" class=\"modal-body\"> \n        <table class=\"table table-striped\">\n            <thead>\n              <tr>\n                <th>SrNo</th>\n                <th>Items</th> \n                <th>Price</th>\n                <th>Quantity</th>   \n                <th>Amount</th>                \n              </tr>\n            </thead>\n            <tbody>\n              <tr *ngFor=\"let obj of _listpopup.arr \">\n                <td class=\"right\"> {{obj.srno}}</td>\n                <td>{{obj.description}}</td>\n                <td> {{obj.price}}</td>\n                <td>{{obj.quantity}}</td>\n                <td> {{obj.amount}}</td>               \n              </tr>\n            </tbody>\n          </table>  \n          </div>\n          <div class=\"modal-footer\">\n            <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n          </div>\n        </div>\n      </div>\n    </div> \n \n  <!-- End -->\n  \n\n\n<div id=\"sessionalert\" class=\"modal fade\">\n  <div class=\"modal-dialog\">\n  <div class=\"modal-content\">\n    <div class=\"modal-body\">\n    <p>{{_sessionMsg}}</p>\n    </div>\n    <div class=\"modal-footer\">\n    </div>\n  </div>\n  </div>\n  </div>\n\n\n  <div [hidden]=\"_mflag\">\n    <div class=\"loader modal\" id=\"loader\"></div>\n  </div>\n   "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmMerchantreportComponent);
    return FrmMerchantreportComponent;
}());
exports.FrmMerchantreportComponent = FrmMerchantreportComponent;
//# sourceMappingURL=frmmerchantreport.component.js.map