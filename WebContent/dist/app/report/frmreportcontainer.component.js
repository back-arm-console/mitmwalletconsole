"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var rp_intercom_service_1 = require("../framework/rp-intercom.service");
var platform_browser_1 = require("@angular/platform-browser");
var rp_http_service_1 = require("../framework/rp-http.service");
var ReportContainerComponent = (function () {
    function ReportContainerComponent(ics, sanitizer, http) {
        this.ics = ics;
        this.sanitizer = sanitizer;
        this.http = http;
        this._show = true;
        this._showPDF = false;
        this._showExcel = false;
        this.rpClose = new core_1.EventEmitter();
    }
    Object.defineProperty(ReportContainerComponent.prototype, "rpSrc", {
        set: function (src) {
            if (src != undefined && src != "") {
                this._srcPrint = this.sanitizer.bypassSecurityTrustResourceUrl(src);
            }
        },
        enumerable: true,
        configurable: true
    });
    ReportContainerComponent.prototype.closePrint = function () {
        if (this._show)
            this.rpClose.emit();
        else if (this._showPDF || this._showExcel) {
            this.reset();
        }
    };
    ReportContainerComponent.prototype.pdf_print = function () {
        var printContents, popupWin;
        printContents = document.getElementById('frame1').outerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write("\n            <html>\n                <head>\n                    <title>Print tab</title>\n                    <style>\n                    //........Customized style.......\n                    </style>\n                </head>\n            <body onload=\"window.print();window.close()\">" + printContents + "</body>\n            </html>");
        popupWin.document.close();
    };
    ReportContainerComponent.prototype.pdf_download = function () {
        var _this = this;
        setTimeout(function () {
            _this._show = false;
            _this._showPDF = true;
            _this._showExcel = false;
            var url = _this.ics.icbsrpturl + "/blank.jsp?exportType=downloadpdf";
            _this._srcPDF = _this.sanitizer.bypassSecurityTrustResourceUrl(url);
        }, 50);
    };
    ReportContainerComponent.prototype.exl_download = function () {
        var _this = this;
        setTimeout(function () {
            _this._show = false;
            _this._showPDF = false;
            _this._showExcel = true;
            var url = _this.ics.icbsrpturl + "/blank.jsp?exportType=excel";
            _this._srcExcel = _this.sanitizer.bypassSecurityTrustResourceUrl(url);
        }, 500);
    };
    ReportContainerComponent.prototype.reset = function () {
        var _this = this;
        setTimeout(function () {
            _this._show = true;
            _this._showPDF = false;
            _this._showExcel = false;
        }, 500);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object), 
        __metadata('design:paramtypes', [Object])
    ], ReportContainerComponent.prototype, "rpSrc", null);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], ReportContainerComponent.prototype, "rpClose", void 0);
    ReportContainerComponent = __decorate([
        core_1.Component({
            selector: 'rpt-container',
            template: "\n        <div style=\"margin-top:-6px;\">\n            <button id=\"btnClose\" type=\"button\" class=\"btn btn-md btn-secondary\" (click)=\"closePrint()\"  title=\"Close\" style=\"cursor:pointer;text-align:center;width:34px;height:34px;padding:0;float:right;margin-bottom:3px;\">\n                <i id=\"lblClose\" class=\"glyphicon glyphicon-remove\" style=\"font-size:11px;\"></i> \n            </button>\n            <button title=\"Print\" id=\"btnPrint\" class=\"btn btn-primary btn-md\" (click)=\"pdf_print()\" type=\"button\" style=\"float:right;margin-right:3px;cursor:pointer;text-align:center;height:34px;margin-bottom:3px;\">\n                <i class=\"glyphicon glyphicon-print\" style=\"cursor: pointer\"></i>\n                <span style=\"font-size: small;\">&nbsp;&nbsp;Print</span>    \n            </button>\n            <button title=\"Export PDF\" id=\"btnPDF\" class=\"btn btn-primary btn-md\" (click)=\"pdf_download()\" type=\"button\" style=\"float:right;margin-right:3px;cursor:pointer;text-align:center;height:34px;margin-bottom:3px;\">\n                <i class=\"glyphicon glyphicon-export\"  style=\"cursor: pointer\"></i>\n                <span style=\"font-size: small;\">&nbsp;&nbsp;PDF</span>    \n            </button>\n            <button title=\"Export Excel\" id=\"btnNew\" class=\"btn btn-primary btn-md\" (click)=\"exl_download()\" type=\"button\" style=\"float:right;margin-right:3px;cursor:pointer;text-align:center;height:34px;margin-bottom:3px;\">\n                <i class=\"glyphicon glyphicon-export\"  style=\"cursor: pointer\"></i>\n                <span style=\"font-size: small;\">&nbsp;&nbsp;Excel</span>    \n            </button>\n             <iframe *ngIf=\"_show\" id=\"frame1\" style=\"width: 100%;height: 100%;\" [src]=\"_srcPrint\"></iframe>  \n             <iframe *ngIf=\"_showPDF\" id=\"framePDF\" style=\"width: 100%;height: 100%;\" (load)=\"reset()\" [src]=\"_srcPDF\"></iframe>  \n             <iframe *ngIf=\"_showExcel\" id=\"frameExl\" style=\"width: 100%;height: 100%;\" (load)=\"reset()\" [src]=\"_srcExcel\"></iframe>  \n             \n             <!--\n            <div id=\"content\" #content>\n                <h1>Title</h1>\n                <p>\n                Letter writing can be fun, help children learn to compose written text, and provide handwriting practice \u2014 and letters are valuable keepsakes. This guide was written for England's \"Write a Letter Week\" and contains activities to help children ages 5\u20139 put pen to paper and make someone's day with a handwritten letter.\n                </p>\n            </div>\n            -->\n        </div>\n    "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, platform_browser_1.DomSanitizer, rp_http_service_1.RpHttpService])
    ], ReportContainerComponent);
    return ReportContainerComponent;
}());
exports.ReportContainerComponent = ReportContainerComponent;
//# sourceMappingURL=frmreportcontainer.component.js.map