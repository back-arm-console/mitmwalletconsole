"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var Rx_1 = require('rxjs/Rx');
core_2.enableProdMode();
var FrmBatchTransactionReportComponent = (function () {
    function FrmBatchTransactionReportComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this.id = "";
        this._mflag = false;
        this._obj = this.getDefaultObj();
        this._totalcount = 1;
        this._shownotsuccess = false;
        this._hideBatchAcc = false;
        this._util = new rp_client_util_1.ClientUtil();
        this.myDatePickerOptions = this._util.getDatePicker();
        this._dates = { "fromDate": this.dateobj, "toDate": this.dateobj };
        this.today = this._util.getTodayDate();
        this._activebank = "";
        this._hideDetailTran = true;
        this._hideDetailDownload = true;
        this._grandAmtTotal = "";
        this._curgrandAmtTotal = "";
        this._savgrandAmtTotal = "";
        this._sessionMsg = "";
        this._key = "";
        this._entryhide = true;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "" };
        this._pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 1 };
        this._list = {
            "data": [{
                    "nrmsg": "",
                    "trandate": "", "amount": "", "flexcubeID": "", "drAcc": "", "crAcc": "", "flexcubeId": "", "xref": "", "accType": "ALL", "merchantId": "",
                    "commission ": "", "custRefNo": "", "batchNo": "", "activebankname": ""
                }],
            "grandTotal": "", "curgrandTotal": "", "savgrandTotal": "", "totalCount": 0, "currentPage": 1, "pageSize": 10
        };
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = false;
            this.getAccountType();
            this._obj.crAcc = this.ics._profile.userID;
            this._dates.fromDate = this._util.changestringtodateobject(this.today);
            this._dates.toDate = this._util.changestringtodateobject(this.today);
            this._entryhide = true;
            this._hideBatchAcc = false;
            this._shownotsuccess = true;
            this.getBatchStatus();
        }
    }
    FrmBatchTransactionReportComponent.prototype.getDefaultObj = function () {
        return {
            "drAcc": "", "crAcc": "", "aFromDate": "", "aToDate": "", "select": "EXCEL", "totalCount": 0, "currentPage": 1, "pageSize": 10, "msgstatus": "",
            "flexcubeId": "", "xref": "", "commission": "", "merchantId": "", "amount": "", "accType": "ALL", "status": "", "batchNo": ""
        };
    };
    FrmBatchTransactionReportComponent.prototype.changeSelectVal = function (options) {
        this.selectedOptions = Array.apply(null, options) // convert to real array
            .filter(function (option) { return option.selected; })
            .map(function (option) { return option.value; });
        this._obj.select = this.selectedOptions[0];
    };
    FrmBatchTransactionReportComponent.prototype.goClear = function () {
        this._entryhide = true;
        this._obj = this.getDefaultObj();
        this._obj.crAcc = this.ics._profile.userID;
        /*  this._dates.fromDate = this._util.changestringtodateobject(this.today);
         this._dates.toDate = this._util.changestringtodateobject(this.today); */
    };
    FrmBatchTransactionReportComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this._router.navigate(['Login', , { p1: '*' }]);
            jQuery("#sessionalert").modal('hide');
        });
    };
    FrmBatchTransactionReportComponent.prototype.changedPager = function (event) {
        this._pgobj = event;
        this._obj.currentPage = this._pgobj.current;
        this._obj.pageSize = this._pgobj.size;
        this._obj.totalCount = this._pgobj.totalcount;
        if (this._pgobj.totalcount > 1) {
            this.goList();
        }
    };
    FrmBatchTransactionReportComponent.prototype.checkdateformat = function (date) {
        if (!/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/.test(date)) {
            this._returnResult.state = 'false';
            this._returnResult.msgDesc = "Invalid Date!";
        }
    };
    FrmBatchTransactionReportComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
            this.http.doGet(url).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this._sessionMsg = data.msgDesc;
                    _this.showMessage();
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmBatchTransactionReportComponent.prototype.date = function () {
        this._obj.aFromDate = this._util.getDatePickerDate(this._dates.fromDate);
        this._obj.aToDate = this._util.getDatePickerDate(this._dates.toDate);
    };
    FrmBatchTransactionReportComponent.prototype.goList = function () {
        var _this = this;
        this._mflag = false;
        if (this._obj.status == "") {
            this._obj.msgstatus = "Status is blank!";
            this.ics.sendBean({ "t1": "rp-msg", t2: "Information", t3: this._obj.msgstatus });
        }
        else {
            var url = this.ics._apiurl + 'service001/getCNPTransactionList';
            this.date();
            var json = this._obj;
            this.http.doPost(url, json).subscribe(function (data) {
                _this._entryhide = false;
                _this._totalcount = data.totalCount;
                _this._obj.currentPage = data.currentPage;
                _this._obj.pageSize = data.pageSize;
                _this._obj.totalCount = data.totalCount;
                if (_this._obj.aFromDate > _this._obj.aToDate) {
                    _this._obj.msgstatus = "From Date must not exceed To Date!";
                    _this.ics.sendBean({ "t1": "rp-msg", t2: "Information", t3: _this._obj.msgstatus });
                    _this.goClear();
                }
                else {
                    if (_this._obj.totalCount == 0) {
                        _this._obj.msgstatus = "Data not Found!";
                        _this.ics.sendBean({ "t1": "rp-msg", t2: "Information", t3: _this._obj.msgstatus });
                        var frdate = _this._obj.aFromDate;
                        var todate = _this._obj.aToDate;
                        var status_1 = _this._obj.status;
                        var acctype = _this._obj.accType;
                        _this.goClear();
                        _this._obj.aFromDate = frdate;
                        _this._obj.aToDate = todate;
                        _this._obj.status = status_1;
                        _this._obj.accType = acctype;
                        _this.checkShowHide(_this._obj.status);
                        _this._mflag = true;
                    }
                    else {
                        if (_this._obj.status == "1") {
                            _this._shownotsuccess = true;
                            _this._entryhide = false;
                            _this._hideBatchAcc = false;
                        }
                        else {
                            _this._shownotsuccess = false;
                            // this._entryhide = true;
                            _this._hideBatchAcc = true;
                            _this._mflag = true;
                        }
                    }
                }
                if (data != null) {
                    if (data.cnpdata != null) {
                        if (!(data.cnpdata instanceof Array)) {
                            var m = [];
                            m[0] = data.cnpdata;
                            data.cnpdata = m;
                        }
                        _this._list.data = data.cnpdata;
                        _this._activebank = _this._list.data[0].activebankname;
                        _this._curgrandAmtTotal = data.curtotal;
                        _this._savgrandAmtTotal = data.savTotal;
                        _this._grandAmtTotal = data.grandTotal;
                    }
                    _this._mflag = true;
                }
                //this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
    };
    FrmBatchTransactionReportComponent.prototype.getBatchStatus = function () {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'service001/getBatchStatus').subscribe(function (data) {
            _this.ref._lov3.refBatchStatus = data.refBatchStatus;
            var arr = [];
            if (_this.ref._lov3.refBatchStatus != null) {
                if (!(_this.ref._lov3.refBatchStatus instanceof Array)) {
                    var m = [];
                    m[0] = _this.ref._lov3.refBatchStatus;
                    _this.ref._lov3.refBatchStatus = m;
                }
                else {
                    for (var j = 0; j < _this.ref._lov3.refBatchStatus.length; j++) {
                        arr.push(_this.ref._lov3.refBatchStatus[j]);
                    }
                }
            }
            _this._mflag = true;
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
            else {
            }
        }, function () { });
    };
    FrmBatchTransactionReportComponent.prototype.checkShowHide = function (p) {
        this._obj.status = p;
        if (this._obj.status == "1") {
            this._hideBatchAcc = false;
        }
        else {
            this._hideBatchAcc = true;
        }
    };
    FrmBatchTransactionReportComponent.prototype.changeStatus = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex; //Get Selected Index
        var value = options[options.selectedIndex].value; //Get Selected Index's Value    
        this._obj.status = value;
        this.checkShowHide(this._obj.status);
        for (var i = 0; i < this.ref._lov3.refBatchStatus.length; i++) {
            if (this.ref._lov3.refBatchStatus[i].value == value) {
                this._obj.status = this.ref._lov3.refBatchStatus[i].value;
                break;
            }
        }
    };
    FrmBatchTransactionReportComponent.prototype.getAccountType = function () {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'service001/getAccountType').subscribe(function (data) {
            _this.ref._lov3.ref016 = data.ref016;
            var arr = [];
            if (_this.ref._lov3.ref016 != null) {
                if (!(_this.ref._lov3.ref016 instanceof Array)) {
                    var m = [];
                    m[0] = _this.ref._lov3.ref016;
                    _this.ref._lov3.ref016 = m;
                }
                else {
                    for (var j = 0; j < _this.ref._lov3.ref016.length; j++) {
                        arr.push(_this.ref._lov3.ref016[j]);
                    }
                }
            }
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
        }, function () { });
    };
    FrmBatchTransactionReportComponent.prototype.changeAccount = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex;
        var value = options[options.selectedIndex].value;
        if (value == "ALL" || value == "") {
            this._obj.accType = "ALL";
        }
        else {
            this._obj.accType = value;
            for (var i = 1; i < this.ref._lov3.ref016.length; i++) {
                if (this.ref._lov3.ref016[i].value == value) {
                    this._obj.accType = this.ref._lov3.ref016[i].value;
                    break;
                }
            }
        }
    };
    FrmBatchTransactionReportComponent.prototype.Validation = function () {
        this.date();
        if (this._obj.aFromDate > this._obj.aToDate) {
            this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "From Date must not exceed ToDate!" });
            this.goClear();
        }
        else {
            this.goDownload();
        }
    };
    FrmBatchTransactionReportComponent.prototype.goDownload = function () {
        this.date();
        var templateData = "";
        var formatJSP = "";
        formatJSP = "cnp-transaction.jsp";
        this.ics.sendBean({ "t1": "rp-msg", t2: "Information ", t3: "Please wait ..." });
        var url = this.ics._rpturl + formatJSP + "?accType=" + this._obj.accType + "&aFromDate=" + this._obj.aFromDate + "&" + "aToDate=" + this._obj.aToDate + "&" + "aSelect=" + this._obj.select + "&aStatus=" + this._obj.status + "&aBatchNo=" + this._obj.batchNo;
        this.ics.sendBean({ "t1": "rp-popup", "t2": "Information", "t3": url });
    };
    FrmBatchTransactionReportComponent = __decorate([
        core_1.Component({
            selector: 'frmbatchtransactionreport',
            template: "\n  <div class=\"container\">\n\t<div class=\"row clearfix\">\n\t\t<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n\t\t\t<form class= \"form-horizontal\" (ngSubmit) = \"goList()\"> \n     <!-- Form Name -->\n\t\t\t<legend>Batch Transaction Report</legend>\n\n\t\t\t<div class=\"row  col-md-12\">  \n\t\t\t\t<button class=\"btn btn-primary\" type=\"button\" (click)=\"goClear()\" >Clear</button> \n\t\t\t\t<button class=\"btn btn-primary\" type=\"submit\">List</button>          \n\t\t\t\t<button class=\"btn btn-primary\" type=\"button\" (click)=\" Validation();\">Download</button>          \n\t\t   </div>\n\n\t\t\t<div class=\"row col-md-12\">&nbsp;</div>\n\t\t\t<div class=\"row col-md-12\" id=\"custom-form-alignment-margin\"><!--Start column -->\n\t\t\t<div class=\"col-md-6\"> \n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t  <label class=\"col-md-4\" > From Date</label>\n\t\t\t\t\t<div class=\"col-md-8\">\n\t\t\t\t\t   <my-date-picker name=\"mydate\" [options]=\"myDatePickerOptions\" [(ngModel)]=\"_dates.fromDate\" ngDefaultControl></my-date-picker>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t<label class=\"col-md-4\">To Date</label>\n\t\t\t\t\t\t<div class=\"col-md-8\">\n\t\t\t\t\t\t\t<my-date-picker name=\"mydate\" [options]=\"myDatePickerOptions\" [(ngModel)]=\"_dates.toDate\" ngDefaultControl></my-date-picker>\n\t\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t<label class=\"col-md-4\" > Status <font class=\"mandatoryfont\"> * </font> </label>\n\t\t\t\t\t<div class=\"col-md-8\">\n\t\t\t\t\t  <select [(ngModel)]=_obj.status [ngModelOptions]=\"{standalone: true}\"  (change)=\"changeStatus($event)\"  required=\"true\"   class=\"form-control col-md-0\"  >\n\t\t\t\t\t\t<option *ngFor=\"let item of ref._lov3.refBatchStatus\" value=\"{{item.value}}\" >{{item.caption}}</option> \n\t\t\t\t\t  </select>                     \n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<div class=\"col-md-6\">\n\t\t\t\t<div class=\"form-group\" *ngIf=\"_hideBatchAcc==false\">\n\t\t\t\t\t<label class=\"col-md-4\">Account Types &nbsp; <font class=\"mandatoryfont\"> * </font></label>\n\t\t\t\t\t  <div class=\"col-md-8\" > \n\t\t\t\t\t\t<select [(ngModel)]=\"_obj.accType\" [ngModelOptions]=\"{standalone: true}\"  (change)=\"changeAccount($event)\"  class=\"form-control col-md-0\" required>\n\t\t\t\t\t\t\t<option *ngFor=\"let item of ref._lov3.ref016\" value=\"{{item.value}}\" >{{item.caption}}</option>\n\t\t\t\t\t\t</select> \n\t\t\t\t\t  </div>             \n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"form-group\" *ngIf=\"_hideBatchAcc==false\">\n\t\t\t\t\t<label class=\"col-md-4\">Batch No.</label>\n\t\t\t\t\t  <div class=\"col-md-8\">\n\t\t\t\t\t\t<input type=\"text\"  class=\"form-control\" [(ngModel)]=\"_obj.batchNo\" [ngModelOptions]=\"{standalone: true}\" />\n\t\t\t\t\t  </div>            \n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t<label class=\"col-md-4\"></label>\n\t\t\t\t\t\t  <div class=\"col-md-8\">\n\t\t\t\t\t\t\t<select [(ngModel)]=_obj.select [ngModelOptions]=\"{standalone: true}\"  (change)=\"changeSelectVal($event.target.options)\" required=\"true\"   class=\"form-control col-md-0\"  >\n\t\t\t\t\t\t\t<option *ngFor=\"let item of ref._lov1.ref010\" value=\"{{item.value}}\" >{{item.caption}}</option> \n\t\t\t\t\t\t\t</select>                     \n\t\t\t\t\t\t  </div>\n\t\t\t\t</div>          \n\t\t\t</div>\n\t\t\t</div>\n     </form>\n    </div>\n  </div>\n </div>\n\n  <div [hidden]=\"_entryhide\">\n  <div class=\"form-group\">\n    <legend></legend>    \n  </div> \n\n  <div class=\"form-group\">\n  <div class=\"col-md-9\">\n    <div style = \"margin-top : 10px\">\n    <adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"_totalcount\" (rpChanged)=\"changedPager($event)\" ></adminpager> \n    </div>\n  </div>\n  <div class=\"col-md-3\" style=\"algin:right;\">\n    <table style=\"align:left;font-size:15px\" cellpadding=\"10px\" cellspacing=\"10px\">    \n    <tbody>\n    <colgroup>  <col span=\"1\" style=\"width: 40%;\"> <col span=\"1\" style=\"width: 60%;\"> </colgroup>\n      <tr *ngIf =\"_shownotsuccess\">\n      <td style=\"color:blue \"> <label >Total Current Amount &nbsp;&nbsp;:&nbsp;</label> </td>\n      <td  style=\"color:blue;display:block; text-align:right; \"> <label>{{_curgrandAmtTotal}}</label> </td>\n      </tr>\n  \n      <tr *ngIf =\"_shownotsuccess\">\n      <td style=\"color:blue \"><label >Total Savings Amount &nbsp;:&nbsp;</label> </td>\n      <td  style=\"color:blue;display:block; text-align:right; \"> <label>{{_savgrandAmtTotal}}</label> </td>\n      </tr>\n  \n      <tr>\n      <td style=\"color:blue ; text-align:left\">\n        <label >Total Amount &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;</label>   \n      </td>\n      <td  style=\"color:blue;display:block; text-align:right; \">\n        <label>{{_grandAmtTotal}}</label>\n      </td>\n      </tr>                   \n    </tbody>\n    </table>\n  </div> \n  </div>\n\n  <span  *ngIf =\"_shownotsuccess\">\n  <table class=\"table table-striped table-condensed table-hover tblborder\" style=\"font-size:14px;\">\n  <thead>\n    <tr > \n    <!--<th align=\"center\">No.</th>-->\n    <th align=\"center\"><div style=\"width:150\">Transaction Date</div></th>  \n    <th align = \"left\" *ngIf=\"_activebank == 'SHWE'\"><div style=\"width:150\">Trn Ref No.1 </div></th> \n    <th align=\"left\" *ngIf=\"_activebank == 'SHWE'\"><div style=\"width:150\">Trn Ref No.2 </div></th> \n    <th align=\"left\" *ngIf=\"_activebank == 'TCB'\"><div style=\"width:150\">Batch Ref No.</div></th>\n    <th align=\"left\" *ngIf=\"_activebank == 'TCB'\"><div style=\"width:150\">Bank Ref No.</div></th>         \n    <th align=\"center\"><div style=\"width:200\">Customer Ref No.</div></th>              \n    <th align=\"center\"><div style=\"width:80\">Amount</div></th>\n    <th align=\"center\"><div style=\"width:150\">Debit Account</div></th>\n    <th align=\"center\"><div style=\"width:150\">Credit Account</div></th>      \n    <th align=\"center\"><div style=\"width:150\">Account Type</div></th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr *ngFor=\"let obj of _list.data,  let i=index\" >\n    <!--<td >{{ i +1 }}</td>-->\n    <td >{{obj.trandate}}</td>\n    <td align = \"left\"> {{obj.batchNo}}</td>\n    <td align = \"left\">{{obj.flexcubeId}}</td> \n    <td align = \"left\"> {{obj.custRefNo}}</td>\n    <td align = \"right\">{{obj.amount}}</td>              \n    <td>{{obj.drAcc}}</td>\n    <td>{{obj.crAcc}}</td>              \n    <td >{{obj.accType}}</td>     \n    </tr>  \n  </tbody>\n  </table>\n  </span>\n\n  <span *ngIf=\"_shownotsuccess==false\">\n  \n    <table class=\"table table-striped table-condensed table-hover tblborder\" style=\"font-size:14px;\">\n    <thead>\n    <tr > \n      <!--<th>No.</th>-->\n      <th>Transaction Date</th>          \n      <th>Customer Ref No.</th>              \n      <th>Amount</th>\n      <th>Bank Ref No.</th>\n      <th>Debit Account</th>\n      <th>Credit Account</th> \n      <th align=\"left\" *ngIf=\"_obj.status == '4'\">Message</th>  \n    </tr>\n    </thead>\n    <tbody>\n    <tr *ngFor=\"let obj of _list.data,  let i=index\" >\n      <!--<td >{{ i +1 }}</td>-->\n      <td >{{obj.trandate}}</td>              \n      <td > {{obj.custRefNo}}</td>\n      <td  align = \"right\">{{obj.amount}}</td>\n      <td>{{obj.flexcubeId}}</td>\n      <td>{{obj.drAcc}}</td>\n      <td>{{obj.crAcc}}</td>\n      <td *ngIf=\"_obj.status == '4'\">{{obj.nrmsg}}</td>     \n    </tr>  \n  </tbody>\n  </table>\n  </span>\n</div>\n\n<div [hidden] = \"_mflag\">\n<div class=\"modal\" id=\"loader\"></div>\n</div>\n\n "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmBatchTransactionReportComponent);
    return FrmBatchTransactionReportComponent;
}());
exports.FrmBatchTransactionReportComponent = FrmBatchTransactionReportComponent;
//# sourceMappingURL=frmbatchtransactionreport.component.js.map