"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var Rx_1 = require('rxjs/Rx');
core_2.enableProdMode();
var FrmUserReportComponent = (function () {
    function FrmUserReportComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this.id = "";
        this._mflag = false;
        this._obj = this.getDefaultObj();
        this._util = new rp_client_util_1.ClientUtil();
        this.today = this._util.getTodayDate();
        this.myDatePickerOptions = this._util.getDatePicker();
        this._dates = { "fromDate": this.dateobj, "toDate": this.dateobj };
        this._sessionMsg = "";
        this._key = "";
        this._entryhide = true;
        this.active = false;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "" };
        this._pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 1 };
        this._list = {
            "data": [{
                    "syskey": 0, "cif": "",
                    "userID": "", "username": "", "nrc": "", "phoneno": "", "createddate": "", "modifieddate": "",
                    "createdby": "", "modifiedby": "", "userstatus": "", "recordStatus": 0, "n7": 0
                }],
            "totalCount": 0, "currentPage": 1, "pageSize": 10
        };
        this._listpopup = { "arr": [{ "syskey": 0, "hkey": 0, "srno": 0, "code": "", "description": "", "price": "", "quantity": 0, "amount": "" }] };
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "" };
            jQuery("#mydelete").prop("disabled", true);
            jQuery("#mySave").prop("disabled", false);
            this._obj = this.getDefaultObj();
            this.getActiveBank();
            this.getUserType();
            this.checkSession();
            this.getAllBranch();
            this.messagehide = true;
        }
    }
    FrmUserReportComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this._key = id;
                _this.Validation();
            }
        });
    };
    FrmUserReportComponent.prototype.getDefaultObj = function () {
        return {
            "sessionID": "", "msgCode": "", "msgDesc": "",
            "aFromDate": "", "aToDate": "", "select": "EXCEL", "totalCount": 0, "currentPage": 1, "pageSize": 10, "msgstatus": "",
            "username": "", "nrc": "", "phoneno": "", "usertype": "", "status": "all", "branchCode": "", "userID": "", "click": 0
        };
    };
    FrmUserReportComponent.prototype.goDownload = function () {
        this.date();
        var templateData = "";
        var formatJSP = "";
        var uName = encodeURI(this._obj.username);
        formatJSP = "user.jsp";
        this.ics.sendBean({ "t1": "rp-msg", t2: "Information ", t3: "Please wait ..." });
        /*  let url = this.ics._rpturl + formatJSP + "?aFromDate=" + this._obj.aFromDate + "&" + "aToDate=" + this._obj.aToDate + "&" + "aSelect=" + this._obj.select + "&"
           + "username=" + uName
           + "&nrc=" + this._obj.nrc + "&phoneno=" + this._obj.phoneno + "&usertype=" + this._obj.usertype
           + "&status=" + this._obj.status+ "&branchCode=" + this._obj.branchCode; */
        var url = this.ics._rpturl + formatJSP + "?aFromDate=" + this._obj.aFromDate + "&" + "aToDate=" + this._obj.aToDate + "&" + "aSelect=" + this._obj.select + "&"
            + "userID=" + this.ics._profile.userID
            + "&nrc=" + this._obj.nrc + "&phoneno=" + this._obj.phoneno + "&usertype=" + this._obj.usertype
            + "&status=" + this._obj.status + "&branchCode=" + this._obj.branchCode + "&userID=" + this.ics._profile.userID;
        this.ics.sendBean({ "t1": "rp-popup", "t2": "Information", "t3": url });
    };
    FrmUserReportComponent.prototype.changeSelectVal = function (options) {
        this.selectedOptions = Array.apply(null, options) // convert to real array
            .filter(function (option) { return option.selected; })
            .map(function (option) { return option.value; });
        this._obj.select = this.selectedOptions[0];
    };
    FrmUserReportComponent.prototype.changeStatus = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex; //Get Selected Index
        var value = options[options.selectedIndex].value; //Get Selected Index's Value 
        if (value == "ALL" || value == "") {
            this._obj.status = "ALL";
        }
        else {
            this._obj.status = value;
            for (var i = 1; i < this.ref._lov1.ref021.length; i++) {
                if (this.ref._lov1.ref021[i].value == value) {
                    this._obj.status = this.ref._lov1.ref021[i].value;
                }
            }
        }
    };
    FrmUserReportComponent.prototype.changeUserType = function (event) {
        var branchuserArr = [["all", "All"], ["save", "Save"], ["delete", "Delete"]];
        var enduserArr = [["all", "All"], ["save", "Save"], ["activate", "Activate"], ["deactivate", "Deactivate"], ["lock", "Lock"], ["delete", "Delete"]];
        var options = event.target.options;
        var k = options.selectedIndex;
        var value = options[options.selectedIndex].value;
        var combo = [];
        if (value == 2) {
            for (var j = 0; j < 3; j++) {
                var l = 0;
                combo.push({ "value": branchuserArr[j][l++], "caption": branchuserArr[j][l++] });
            }
            this.ref._lov1.ref021 = combo;
        }
        else if (value == 1) {
            for (var j = 0; j < 6; j++) {
                var l = 0;
                combo.push({ "value": enduserArr[j][l++], "caption": enduserArr[j][l++] });
            }
            this.ref._lov1.ref021 = combo;
        }
        if (value == "ALL" || value == "") {
            this._obj.usertype = "ALL";
        }
        else {
            this._obj.usertype = value;
            for (var i = 1; i < this.ref._lov1.refUserType.length; i++) {
                if (this.ref._lov1.refUserType[i].value == value) {
                    this._obj.usertype = this.ref._lov1.refUserType[i].value;
                    break;
                }
            }
        }
    };
    FrmUserReportComponent.prototype.goClear = function () {
        this._entryhide = true;
        this._dates.fromDate = this._util.changestringtodateobject("");
        this._dates.toDate = this._util.changestringtodateobject("");
        this._obj = this.getDefaultObj();
    };
    FrmUserReportComponent.prototype.date = function () {
        this._obj.aFromDate = this._util.getDatePickerDate(this._dates.fromDate);
        this._obj.aToDate = this._util.getDatePickerDate(this._dates.toDate);
    };
    FrmUserReportComponent.prototype.goList = function () {
        var _this = this;
        this._mflag = false;
        this.date();
        this._obj.sessionID = this.ics._profile.sessionID;
        this._obj.userID = this.ics._profile.userID;
        var url = this.ics._apiurl + 'service001/getUserReportList';
        var capValue = "";
        var json = this._obj;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data.msgCode == '0016') {
                _this._sessionMsg = data.msgDesc;
                _this._returnResult.msgDesc = "";
                _this.showMessage();
            }
            else {
                _this._entryhide = false;
                _this._obj.currentPage = data.currentPage;
                _this._obj.pageSize = data.pageSize;
                _this._obj.totalCount = data.totalCount;
                if (_this._obj.aFromDate > _this._obj.aToDate) {
                    _this._obj.msgstatus = "From Date must not exceed To Date!";
                    _this.ics.sendBean({ "t1": "rp-msg", t2: "Information", t3: _this._obj.msgstatus });
                    _this._entryhide = true;
                }
                else {
                    if (_this._obj.totalCount == 0) {
                        _this._obj.msgstatus = "Data not Found!";
                        _this.ics.sendBean({ "t1": "rp-msg", t2: "Information", t3: _this._obj.msgstatus });
                        _this._entryhide = true;
                    }
                }
                if (data != null) {
                    _this._list = data;
                    if (data.data != null) {
                        if (!(data.data instanceof Array)) {
                            var m = [];
                            m[0] = data.data;
                            data.data = m;
                        }
                        _this._list.data = data.data;
                    }
                    if (data.detailData != null) {
                        if (!(data.detailData instanceof Array)) {
                            var m = [];
                            m[0] = data.detailData;
                            data.detailData = m;
                        }
                    }
                }
            }
            _this._mflag = true;
            _this._obj.click = 0;
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
        }, function () { });
    };
    FrmUserReportComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this._router.navigate(['Login', , { p1: '*' }]);
            jQuery("#sessionalert").modal('hide');
        });
    };
    FrmUserReportComponent.prototype.changedPager = function (event) {
        this._pgobj = event;
        this._obj.currentPage = this._pgobj.current;
        this._obj.pageSize = this._pgobj.size;
        this._obj.totalCount = this._pgobj.totalcount;
        if (this._pgobj.totalcount > 1) {
            this._obj.click = 1;
            this.goList();
        }
    };
    FrmUserReportComponent.prototype.signOut = function () {
        var _this = this;
        this._obj.sessionID = this.ics._profile.sessionID;
        var url = this.ics._apiurl + 'service001/signout?sessionID=' + this.ics._profile.sessionID;
        var json = this.ics._profile.userID;
        var _returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "" };
        this.http.doPost(url, json).subscribe(function (data) {
            _returnResult = data;
            if (_returnResult.state == "true") {
                _this._router.navigate(['Login', , { p1: '*' }]);
            }
        }, function (error) { return alert(error); }, function () { });
    };
    FrmUserReportComponent.prototype.checkdateformat = function (date) {
        if (!/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/.test(date)) {
            this._returnResult.state = 'false';
            this._returnResult.msgDesc = "Invalid Date!";
        }
    };
    FrmUserReportComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
            this.http.doGet(url).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this._sessionMsg = data.msgDesc;
                    _this.showMessage();
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmUserReportComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    FrmUserReportComponent.prototype.showdetailspopup = function (syskey) {
        var _this = this;
        this._listpopup.arr = [];
        if (this.ics._apiurl != "") {
            this.http.doGet(this.ics._apiurl + 'service001/getshowdetails?syskey=' + syskey).subscribe(function (data) {
                _this._listpopup.arr = data.detailsData;
                if (data != null) {
                    if (data.detailsData != null) {
                        if (!(data.detailsData instanceof Array)) {
                            var m = [];
                            m[0] = data.detailsData;
                            data.detailsData = m;
                        }
                        _this._listpopup.arr = data.detailsData;
                    }
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        jQuery("#lu001popup").modal();
    };
    FrmUserReportComponent.prototype.Validation = function () {
        var _this = this;
        try {
            this.date();
            var url = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
            this.http.doGet(url).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this._sessionMsg = data.msgDesc;
                    _this.showMessage();
                }
                else {
                    if (_this._obj.aFromDate == "") {
                        _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Required From Date" });
                        return false;
                    }
                    if (_this._obj.aToDate == "") {
                        _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Required To Date" });
                        return false;
                    }
                    if (_this._obj.aFromDate.length != 0) {
                        if (!/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/.test(_this._obj.aFromDate)) {
                            _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Invalid From Date!" });
                        }
                    }
                    if (_this._obj.aToDate.length != 0) {
                        if (!/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/.test(_this._obj.aToDate)) {
                            _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Invalid To Date!" });
                        }
                    }
                    if (_this._obj.aFromDate > _this._obj.aToDate) {
                        _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "From Date must not exceed ToDate!" });
                        _this.goClear();
                    }
                    else if (_this._obj.usertype == "") {
                        _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Please select User Type!" });
                    }
                    else if (_this._obj.status == "" || _this._obj.status == "null") {
                        _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Please select User status!" });
                    }
                    else {
                        _this.goDownload();
                    }
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmUserReportComponent.prototype.goValidate = function () {
        this.date();
        if (this._obj.aFromDate == "") {
            this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Required From Date" });
            return false;
        }
        if (this._obj.aToDate == "") {
            this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Required To Date" });
            return false;
        }
        if (!/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/.test(this._obj.aFromDate)) {
            this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Invalid From Date!" });
        }
        else if (!/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/.test(this._obj.aToDate)) {
            this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Invalid To Date!" });
        }
        else if (this._obj.nrc.length > 0) {
            if (!/^(([1-9]|1[0-9])\/[A-Za-z]+\([N|n]\)[0-9]{6})$/.test(this._obj.nrc)) {
                this._returnResult.state = 'false';
                this._returnResult.msgDesc = "Invalid NRC No.";
            }
            else {
                this.goList();
            }
        }
        else {
            this.goList();
        }
    };
    FrmUserReportComponent.prototype.getUserType = function () {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'service001/getUserType').subscribe(function (data) {
            _this.ref._lov1.refUserType = data.refUserType;
            var arr = [];
            if (_this.ref._lov1.refUserType != null) {
                if (!(_this.ref._lov1.refUserType instanceof Array)) {
                    var m = [];
                    m[0] = _this.ref._lov1.refUserType;
                    _this.ref._lov1.refUserType = m;
                }
                else {
                    for (var j = 0; j < _this.ref._lov1.refUserType.length; j++) {
                        arr.push(_this.ref._lov1.refUserType[j]);
                    }
                }
            }
            _this._mflag = true;
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
            else {
            }
        }, function () { });
    };
    FrmUserReportComponent.prototype.getAllBranch = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics._apiurl + 'service001/getAllBranch').subscribe(function (data) {
                if (data != null && data != undefined) {
                    _this.ref._lov3.ref018 = [{ "value": "", "caption": "ALL" }];
                    for (var i = 1; i < data.ref018.length; i++) {
                        _this.ref._lov3.ref018[i] = data.ref018[i - 1];
                    }
                }
                else {
                    _this.ref._lov3.ref018 = [{ "value": "", "caption": "Empty" }];
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmUserReportComponent.prototype.getActiveBank = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service001/getActiveBank';
        var json = { userID: this.ics._profile.userID, sessionID: this.ics._profile.sessionID };
        var _returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "", "bankName": "" };
        this.http.doPost(url, json).subscribe(function (data) {
            _returnResult = data;
            if (_returnResult.bankName === 'TCB') {
                _this.active = true;
            }
            if (_returnResult.state == "true") {
                _this._router.navigate(['Login', , { p1: '*' }]);
            }
        }, function (error) { return alert(error); }, function () { });
    };
    FrmUserReportComponent = __decorate([
        core_1.Component({
            selector: 'frmuserreport',
            template: "\n  <div class=\"container\">\n     <div class=\"row clearfix\">\n        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n          <form class= \"form-horizontal\" (ngSubmit)=\"goValidate()\"> \n         <!-- Form Name -->\n          <legend>User Report</legend>\n\n          <div class=\"row  col-md-12\">  \n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"goClear()\" >Clear</button> \n            <button class=\"btn btn-primary\" >List</button>      \n            <button class=\"btn btn-primary\" type=\"button\" (click)=\" Validation();\">Download</button>          \n         </div>\n\n         <div class=\"row col-md-12\">&nbsp;</div>\n         <div class=\"row col-md-12\" id=\"custom-form-alignment-margin\"><!--Start column -->\n           <div class=\"col-md-6\"> \n              <div class=\"form-group\">\n                  <label class=\"col-md-4\" > From Date  <font class=\"mandatoryfont\"> * </font> </label>\n                      <div class=\"col-md-8\">\n                            <my-date-picker name=\"mydate\"  [options]=\"myDatePickerOptions\" [(ngModel)]=\"_dates.fromDate\" ngDefaultControl></my-date-picker>\n                      </div>\n              </div>\n\n              <div class=\"form-group\">\n              <label class=\"col-md-4\">To Date  <font class=\"mandatoryfont\"> * </font> </label>\n                  <div class=\"col-md-8\">\n                      <my-date-picker name=\"mydate\" [options]=\"myDatePickerOptions\" [(ngModel)]=\"_dates.toDate\" ngDefaultControl></my-date-picker>\n                  </div>\n            </div>\n          <div class=\"form-group\">\n              <label class=\"col-md-4\" >  User Type <font class=\"mandatoryfont\"> * </font> </label>\n              <div class=\"col-md-8\">\n                <select [(ngModel)]=_obj.usertype [ngModelOptions]=\"{standalone: true}\"  (change)=\"changeUserType($event)\"  required=\"true\"   class=\"form-control col-md-0\"  >\n                   <option *ngFor=\"let item of ref._lov1.refUserType\" value=\"{{item.value}}\" >{{item.caption}}</option> \n                </select>                     \n              </div>\n          </div>\n\n          <div class=\"form-group\" *ngIf=\"active  && _obj.usertype =='1'\">\n              <label class=\"col-md-4\" align=\"left\"> Branch Code <font size=\"4\" color=\"#FF0000\">*</font></label>\n              <div class=\"col-md-8\">\n                <select [(ngModel)]=\"_obj.branchCode\" class=\"form-control col-md-0\" [ngModelOptions]=\"{standalone: true}\">\n                  <option *ngFor=\"let item of ref._lov3.ref018\" value=\"{{item.value}}\">{{item.caption}}</option> \n                </select>                \n              </div> \n           </div> \n          \n          <div class=\"form-group\">\n          <label class=\"col-md-4\" > Status <font class=\"mandatoryfont\"> * </font> </label>\n              <div class=\"col-md-8\">\n                  <select [(ngModel)]=\"_obj.status\" [ngModelOptions]=\"{standalone: true}\"  (change)=\"changeStatus($event)\"  required=\"true\"   class=\"form-control col-md-0\"  >\n                    <option *ngFor=\"let item of ref._lov1.ref021\" value=\"{{item.value}}\" >{{item.caption}}</option> \n                  </select>                      \n              </div>\n           </div>\n           </div> <!-- Close tag first col-md-6 -->          \n\n        <div class=\"col-md-6\">\n            <div class=\"form-group\">\n              <label class=\"col-md-4\" > User Name </label>\n                  <div class=\"col-md-8\">\n                      <input type=\"text\"  class=\"form-control\" [(ngModel)]=\"_obj.username\" [ngModelOptions]=\"{standalone: true}\" />\n                  </div>\n            </div>\n\n          <div class=\"form-group\">\n            <label class=\"col-md-4\" > NRC</label>\n                <div class=\"col-md-8\">\n                    <input type=\"text\"  class=\"form-control\" [(ngModel)]=\"_obj.nrc\" [ngModelOptions]=\"{standalone: true}\" />\n                </div>\n          </div>\n\n          <div class=\"form-group\">\n            <label class=\"col-md-4\" >Phone No.</label>\n                <div class=\"col-md-8\">\n                    <input type=\"text\"  class=\"form-control\" [(ngModel)]=\"_obj.phoneno\" [ngModelOptions]=\"{standalone: true}\" />\n                </div>\n          </div>\n\n          <div class=\"form-group\">\n          <label class=\"col-md-4\"></label>\n              <div class=\"col-md-8\">\n                <select [(ngModel)]=_obj.select [ngModelOptions]=\"{standalone: true}\"  (change)=\"changeSelectVal($event.target.options)\" required=\"true\"   class=\"form-control col-md-0\"  >\n                   <option *ngFor=\"let item of ref._lov1.ref010\" value=\"{{item.value}}\" >{{item.caption}}</option> \n                </select>                     \n              </div>\n              </div>\n        </div><!-- Close tag first col-md-6 -->\n      </div>\n       </form>\n      </div>\n    </div>\n </div>\n\n <div [hidden]=\"_entryhide\">\n    <div class=\"form-group\">\n    <div style = \"margin-top : 10px\">\n       <adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"_list.totalCount\" (rpChanged)=\"changedPager($event)\" ></adminpager> \n    </div>\n    </div> \n <table class=\"table table-striped table-condensed table-hover tblborder\" style=\"font-size:14px;\">\n    <thead>\n        <tr>\n         \n            <th align=\"left\">User ID </th>\n            <th  *ngIf =\"_obj.usertype=='1'\"> Customer ID </th>\n            <th align=\"left\"> User Name </th>\n            <th align=\"left\"> NRC </th>\n            <th align=\"left\"> Phone No. </th>\n            <th align=\"left\"> Created Date </th>\n            <th align=\"left\"> Modified Date  </th>\n            <th align=\"left\"> Created By </th>\n            <th align=\"left\"> Modified By </th>\n            <th align=\"left\"> User Status </th>\n        </tr>\n    </thead>\n    <tbody>\n        <tr *ngFor=\"let obj of _list.data\">\n            <td align=\"left\">{{obj.userID}}</td>\n            <td  *ngIf =\"_obj.usertype=='1'\"> {{obj.cif}} </td>\n            <td align=\"left\">{{obj.username}}</td> \n            <td align=\"left\">{{obj.nrc}}</td>\n            <td align=\"left\">{{obj.phoneno}}</td>\n            <td align=\"left\">{{obj.createddate}}</td>\n            <td align=\"left\">{{obj.modifieddate}}</td>   \n            <td align=\"left\" >{{obj.createdby}}</td>                                       \n            <td align=\"left\">{{obj.modifiedby}}</td>\n            <td align = \"left\" *ngIf=\"obj.recordStatus == 1 \" >Save</td>\n            <td align = \"left\" *ngIf=\"obj.recordStatus == 2 && obj.n7 == 0 \" >Activate</td>\n            <td align = \"left\" *ngIf=\"obj.recordStatus == 21 \" >Deactivate</td>\n            <td align = \"left\" *ngIf=\"obj.recordStatus == 4 \" > Delete</td>\n            <td align = \"left\" *ngIf=\"obj.recordStatus == 2 && obj.n7 == 11 \" >Lock</td>\n        </tr>  \n    </tbody>\n</table>\n</div>\n<div [hidden] = \"_mflag\">\n<div class=\"modal\" id=\"loader\"></div>\n</div> \n "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmUserReportComponent);
    return FrmUserReportComponent;
}());
exports.FrmUserReportComponent = FrmUserReportComponent;
//# sourceMappingURL=frmuserreport.component.js.map