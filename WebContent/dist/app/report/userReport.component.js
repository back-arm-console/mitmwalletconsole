"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var platform_browser_1 = require("@angular/platform-browser");
core_2.enableProdMode();
var userReport = (function () {
    function userReport(el, renderer, ics, _router, route, http, ref, sanitizer) {
        this.el = el;
        this.renderer = renderer;
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this.sanitizer = sanitizer;
        this._util = new rp_client_util_1.ClientUtil();
        this.today = this._util.getTodayDate();
        this.myDatePickerOptions = this._util.getDatePicker();
        this._dates = { "fromDate": this.dateobj, "toDate": this.dateobj };
        this._mflag = true;
        this._sessionObj = this.getSessionObj();
        this._obj = this.getDefaultObj();
        this.walletuserArr = [["all", "All"], ["save", "Save"], ["delete", "Delete"]];
        this.consoleuserArr = [["all", "All"], ["save", "Save"], ["activate", "Activate"], ["deactivate", "Deactivate"], ["lock", "Lock"], ["delete", "Delete"]];
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        this.setTodayDateObj();
        this.setDateObjIntoString();
        this.getUserType();
        this._pdfdata = false;
    }
    userReport.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    userReport.prototype.getDefaultObj = function () {
        return {
            "sessionID": "", "msgCode": "", "msgDesc": "", "alldate": true,
            "aFromDate": "", "aToDate": "", "select": "EXCEL", "totalCount": 0, "currentPage": 1, "pageSize": 10, "msgstatus": "",
            "username": "", "nrc": "", "accountNo": "", "phoneno": "", "usertype": "", "status": "all", "branchCode": "", "userID": "", "click": 0
        };
    };
    userReport.prototype.setTodayDateObj = function () {
        this._dates.fromDate = this._util.changestringtodateobject(this.today);
        this._dates.toDate = this._util.changestringtodateobject(this.today);
    };
    userReport.prototype.setDateObjIntoString = function () {
        this._obj.aFromDate = this._util.getDatePickerDateymd(this._dates.fromDate);
        this._obj.aToDate = this._util.getDatePickerDateymd(this._dates.toDate);
    };
    userReport.prototype.getAllDate = function (event) {
        this._obj.alldate = event.target.checked;
    };
    userReport.prototype.goPrint = function (fileType) {
        var _this = this;
        this._mflag = false;
        this.setDateObjIntoString();
        if (this._obj.aFromDate > this._obj.aToDate) {
            this._mflag = true;
            this.showMsg("From Date Should Not Exceed To Date.", false);
        } /* if (!/^(([1-9]|1[0-9])\/[A-Za-z]+\([N|n]\)[0-9]{6})$/.test(this._obj.nrc)) {
          this.showMsg("Invalid NRC No.", false);
        } */
        else {
            try {
                var url = this.ics.cmsurl + 'ServiceReportAdm/getUserReportList1?fileType=' + fileType;
                var json = this._obj;
                this._obj.sessionID = this.ics._profile.sessionID;
                this._obj.userID = this.ics._profile.userID;
                this.http.doPost(url, json).subscribe(function (data) {
                    _this._mflag = true;
                    if (data.state) {
                        var url2 = _this.ics.cmsurl + 'ServiceReportAdm/downloadReport?fileType=' + fileType +
                            "&fileName=" + data.stringResult[0] + '&sessionID=' + _this.ics._profile.sessionID + '&userID=' + _this.ics._profile.userID;
                        if (fileType == "pdf") {
                            _this._printUrl = _this.sanitizer.bypassSecurityTrustResourceUrl(url2);
                            _this._pdfdata = true;
                        }
                        else if (fileType == "excel") {
                            jQuery("#accdownload").html("<iframe src=" + url2 + "></iframe>");
                        }
                    }
                    else {
                        _this.showMsg(data.msgDesc, false);
                    }
                }, function (error) {
                    _this._mflag = true;
                    _this.showMsg("HTTP Error Type ", false);
                }, function () {
                    _this._mflag = true;
                });
            }
            catch (e) {
                alert("Invalid URL.");
            }
        }
    };
    userReport.prototype.goClose = function () {
        this._pdfdata = false;
        this.setTodayDateObj();
        this._obj = this.getDefaultObj();
        this._obj.usertype = this.ref._lov3.refUserType[0].value;
        var combo = [];
        if (this._obj.usertype == "1") {
            for (var j = 0; j < 3; j++) {
                var l = 0;
                combo.push({ "value": this.walletuserArr[j][l++], "caption": this.walletuserArr[j][l++] });
            }
            this.ref._lov1.ref021 = combo;
        }
        else if (this._obj.usertype == "2") {
            for (var j = 0; j < 6; j++) {
                var l = 0;
                combo.push({ "value": this.consoleuserArr[j][l++], "caption": this.consoleuserArr[j][l++] });
            }
            this.ref._lov1.ref021 = combo;
        }
    };
    userReport.prototype.changeStatus = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex; //Get Selected Index
        var value = options[options.selectedIndex].value; //Get Selected Index's Value 
        if (value == "ALL" || value == "") {
            this._obj.status = "ALL";
        }
        else {
            this._obj.status = value;
            for (var i = 1; i < this.ref._lov1.ref021.length; i++) {
                if (this.ref._lov1.ref021[i].value == value) {
                    this._obj.status = this.ref._lov1.ref021[i].value;
                }
            }
        }
    };
    userReport.prototype.changeUserType = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex;
        var value = options[options.selectedIndex].value;
        var combo = [];
        if (value == 1) {
            for (var j = 0; j < 3; j++) {
                var l = 0;
                combo.push({ "value": this.walletuserArr[j][l++], "caption": this.walletuserArr[j][l++] });
            }
            this.ref._lov1.ref021 = combo;
        }
        else if (value == 2) {
            for (var j = 0; j < 6; j++) {
                var l = 0;
                combo.push({ "value": this.consoleuserArr[j][l++], "caption": this.consoleuserArr[j][l++] });
            }
            this.ref._lov1.ref021 = combo;
        }
    };
    userReport.prototype.getUserType = function () {
        var _this = this;
        try {
            var url = this.ics.cmsurl + 'serviceAdmLOV/getUserType';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.msgCode == "0016") {
                        _this.showMsg(data.msgDesc, false);
                    }
                    if (data.msgCode == "0000") {
                        if (!(data.refUserType instanceof Array)) {
                            var m = [];
                            m[0] = data.refUserType;
                            _this.ref._lov3.refUserType = m;
                        }
                        else {
                            _this.ref._lov3.refUserType = data.refUserType;
                        }
                        _this._obj.usertype = _this.ref._lov3.refUserType[0].value;
                        var combo = [];
                        if (_this._obj.usertype == "1") {
                            for (var j = 0; j < 3; j++) {
                                var l = 0;
                                combo.push({ "value": _this.walletuserArr[j][l++], "caption": _this.walletuserArr[j][l++] });
                            }
                            _this.ref._lov1.ref021 = combo;
                        }
                        else if (_this._obj.usertype == "2") {
                            for (var j = 0; j < 6; j++) {
                                var l = 0;
                                combo.push({ "value": _this.consoleuserArr[j][l++], "caption": _this.consoleuserArr[j][l++] });
                            }
                            _this.ref._lov1.ref021 = combo;
                        }
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    userReport.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    userReport.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    userReport = __decorate([
        core_1.Component({
            selector: 'fmraccounttrans',
            template: " \n  <div *ngIf=\"!_pdfdata\">\n  <div class=\"container col-md-12 col-sm-12 col-xs-12\" style=\"padding: 0px 5%;\">\n    <form class= \"form-horizontal\">        \n\t\t\t\t  <legend>User Report</legend>\n\t\t\t\t<div class=\"cardview list-height\"> \n\t\t\t\t  <div class=\"col-md-12\" style=\"margin-bottom: 10px;\">  \n\t\t\t\t  <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goPrint('pdf')\">Preview</button>              \n\t\t\t\t  <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goPrint('excel')\">Export</button>          \n\t\t\t\t </div>\n\t\t\t\t\t<div class=\"col-md-10\">\n\t\t\t\t\t\t<div class=\"form-group\">            \n\t\t\t\t\t\t\t<label class=\"col-md-2\">Date Range</label>\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t<div class=\"col-md-3\">\n\t\t\t\t\t\t\t\t<my-date-picker name=\"mydate\"  [options]=\"myDatePickerOptions\" [(ngModel)]=\"_dates.fromDate\" ngDefaultControl></my-date-picker>                  \n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<label class=\"col-md-1\" style=\"text-align: center;\">-</label>\n\t\t\t\t\t\t\t<div class=\"col-md-3\">\n\t\t\t\t\t\t\t\t<my-date-picker name=\"mydate\" [options]=\"myDatePickerOptions\" [(ngModel)]=\"_dates.toDate\" ngDefaultControl></my-date-picker>                           \n\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t<div class=\"col-md-2\">\n\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t<input type='checkbox' [(ngModel)]=\"_obj.alldate\" [ngModelOptions]=\"{standalone: true}\" (click)='getAllDate($event)'>\n\t\t\t\t\t\t\tALL\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t  <label class=\"col-md-2\" >  User Type</label>\n\t\t\t\t\t\t  <div class=\"col-md-3\">\n\t\t\t\t\t\t\t<select [(ngModel)]=_obj.usertype [ngModelOptions]=\"{standalone: true}\"  (change)=\"changeUserType($event)\"  required=\"true\"   class=\"form-control col-md-0\"  >\n\t\t\t\t\t\t\t   <option *ngFor=\"let item of ref._lov3.refUserType\" value=\"{{item.value}}\" >{{item.caption}}</option> \n\t\t\t\t\t\t\t</select>                     \n\t\t\t\t\t\t  </div>\n\t\t\t\t\t\t  <label class=\"col-md-1 col-sm-1\" >Status</label>\n\t\t\t\t\t\t\t  <div class=\"col-md-3\">\n\t\t\t\t\t\t\t\t  <select [(ngModel)]=\"_obj.status\" [ngModelOptions]=\"{standalone: true}\"  (change)=\"changeStatus($event)\"  required=\"true\"   class=\"form-control col-md-0\"  >\n\t\t\t\t\t\t\t\t\t<option *ngFor=\"let item of ref._lov1.ref021\" value=\"{{item.value}}\" >{{item.caption}}</option> \n\t\t\t\t\t\t\t\t  </select>                      \n\t\t\t\t\t\t\t  </div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t  <label class=\"col-md-2\" > User Name </label>\n\t\t\t\t\t\t\t  <div class=\"col-md-3\">\n\t\t\t\t\t\t\t\t  <input type=\"text\"  class=\"form-control\" [(ngModel)]=\"_obj.username\" [ngModelOptions]=\"{standalone: true}\" />\n\t\t\t\t\t\t\t  </div>\n\t\t\t\t\t\t\t  <label class=\"col-md-1 col-sm-1\" > NRC</label>\n\t\t\t\t\t\t\t<div class=\"col-md-3\">\n\t\t\t\t\t\t\t\t<input type=\"text\"  class=\"form-control\" [(ngModel)]=\"_obj.nrc\" [ngModelOptions]=\"{standalone: true}\" />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-md-2\" >Phone No.</label>\n\t\t\t\t\t\t\t<div class=\"col-md-3\">\n\t\t\t\t\t\t\t\t<input type=\"text\"  class=\"form-control\" [(ngModel)]=\"_obj.phoneno\" [ngModelOptions]=\"{standalone: true}\" />\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t  \n\t\t\t\t\t</div>\n\t\t\t  </div>\n    </form>\n       <div id=\"accdownload\" style=\"display: none;width: 0px;height: 0px;\"></div>\n</div>\n\n <div *ngIf=\"_pdfdata\">\n      <img style='margin-left: 10px;cursor: pointer;' src='image/Excel-icon.png'  (click)=\"goPrint('excel')\">\n      <button type=\"button\" class=\"close\"  (click)=\"goClose()\">&times;</button>\n      <iframe id=\"frame1\" [src]=\"_printUrl\" width=\"100%\" height=\"100%\"></iframe>\n    <div id=\"accdownload\" style=\"display: none;width: 0px;height: 0px;\"></div>\n</div>\n\n<div [hidden] = \"_mflag\">\n<div class=\"modal\" id=\"loader\"></div>\n</div> \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n  ",
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef, core_1.Renderer, rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences, platform_browser_1.DomSanitizer])
    ], userReport);
    return userReport;
}());
exports.userReport = userReport;
//# sourceMappingURL=userreport.component.js.map