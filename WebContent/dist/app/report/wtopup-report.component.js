"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var platform_browser_1 = require("@angular/platform-browser");
core_2.enableProdMode();
var wtopupReport = (function () {
    function wtopupReport(el, renderer, ics, _router, route, http, ref, sanitizer) {
        this.el = el;
        this.renderer = renderer;
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this.sanitizer = sanitizer;
        this._util = new rp_client_util_1.ClientUtil();
        this.today = this._util.getTodayDate();
        this.myDatePickerOptions = this._util.getDatePicker();
        this._dates = { "fromDate": this.dateobj, "toDate": this.dateobj };
        this._mflag = true;
        this._sessionObj = this.getSessionObj();
        this._obj = this.getDefaultObj();
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        this.setTodayDateObj();
        this.setDateObjIntoString();
        //this.transfertype();
        this._pdfdata = false;
    }
    wtopupReport.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    wtopupReport.prototype.getDefaultObj = function () {
        return {
            "sessionID": "", "userID": "", "fromDate": "", "toDate": "", "fromAccount": "", "alldate": true, "t1": "", "t2": "", "transtype": "4",
            "toAccount": "", "totalCount": 0, "currentPage": 1, "pageSize": 10,
        };
    };
    wtopupReport.prototype.setTodayDateObj = function () {
        this._dates.fromDate = this._util.changestringtodateobject(this.today);
        this._dates.toDate = this._util.changestringtodateobject(this.today);
    };
    wtopupReport.prototype.setDateObjIntoString = function () {
        this._obj.fromDate = this._util.getDatePickerDateymd(this._dates.fromDate);
        this._obj.toDate = this._util.getDatePickerDateymd(this._dates.toDate);
    };
    wtopupReport.prototype.getAllDate = function (event) {
        this._obj.alldate = event.target.checked;
    };
    wtopupReport.prototype.goPrint = function (fileType) {
        var _this = this;
        this._mflag = false;
        this.setDateObjIntoString();
        if (this._obj.fromDate > this._obj.toDate) {
            this._mflag = true;
            this.showMsg("From Date Should Not Exceed To Date.", false);
        } /* if (!/^(([1-9]|1[0-9])\/[A-Za-z]+\([N|n]\)[0-9]{6})$/.test(this._obj.nrc)) {
          this.showMsg("Invalid NRC No.", false);
        } */
        else {
            try {
                var url = this.ics.cmsurl + 'ServiceReportAdm/getWalletTopupReport?fileType=' + fileType;
                var json = this._obj;
                this._obj.sessionID = this.ics._profile.sessionID;
                this._obj.userID = this.ics._profile.userID;
                this.http.doPost(url, json).subscribe(function (data) {
                    _this._mflag = true;
                    if (data.state) {
                        var url2 = _this.ics.cmsurl + 'ServiceReportAdm/downloadReport?fileType=' + fileType +
                            "&fileName=" + data.stringResult[0] + '&sessionID=' + _this.ics._profile.sessionID + '&userID=' + _this.ics._profile.userID;
                        if (fileType == "pdf") {
                            _this._printUrl = _this.sanitizer.bypassSecurityTrustResourceUrl(url2);
                            _this._pdfdata = true;
                        }
                        else if (fileType == "excel") {
                            jQuery("#accdownload").html("<iframe src=" + url2 + "></iframe>");
                        }
                    }
                    else {
                        _this.showMsg(data.msgDesc, false);
                    }
                }, function (error) {
                    _this._mflag = true;
                    _this.showMsg("HTTP Error Type ", false);
                }, function () {
                    _this._mflag = true;
                });
            }
            catch (e) {
                alert("Invalid URL.");
            }
        }
    };
    wtopupReport.prototype.goClose = function () {
        this._pdfdata = false;
        //this.setTodayDateObj();
        //this._obj = this.getDefaultObj();   
    };
    /*   transfertype() {
        try {
          this._mflag=false;
          let url: string = this.ics.cmsurl + 'serviceAdmLOV/getTransferTypes';
    
          this._sessionObj.sessionID = this.ics._profile.sessionID;
          this._sessionObj.userID = this.ics._profile.userID;
          let json: any = this._sessionObj;
    
          this.http.doPost(url, json).subscribe(
            data => {
              this._mflag = true;
    
              if (data != null) {
                if (data.msgCode == "0016") {
                  this.showMsg(data.msgDesc,false);
                  this.logout();
                }
    
                if (data.msgCode != "0000") {
                  this.showMsg(data.msgDesc,true);
                }
    
                if (data.msgCode == "0000") {
                  if (data.refAccountTransferType != null) {
                    if (!(data.refAccountTransferType instanceof Array)) {
                      let m = [];
                      m[0] = data.refAccountTransferType;
                      this.ref._lov3.refAccountTransferType = m;
                      this._obj.transtype = this.ref._lov3.refAccountTransferType[0].value;
                    } else {
                      this.ref._lov3.refAccountTransferType = data.refAccountTransferType;
                      this._obj.transtype = this.ref._lov3.refAccountTransferType[0].value;
                    }
                  }
                }
              }
              this._mflag=true;
            },
            error => {
              if (error._body.type == "error") {
                alert("Connection Timed Out!");
                this._mflag=true;
              } else {
              }
            },
            () => { }
          );
        } catch (e) {
          alert(e);
          this._mflag=true;
        }
      } */
    wtopupReport.prototype.changeTransferType = function (options) {
        var value1 = options[options.selectedIndex].value;
        for (var i = 1; i < this.ref._lov3.refAccountTransferType.length; i++) {
            if (this.ref._lov3.refAccountTransferType[i].value == value1) {
                this._obj.transtype = value1;
                break;
            }
        }
    };
    wtopupReport.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    wtopupReport.prototype.restrictSpecialCharacter = function (event, fid, value) {
        if ((event.key >= '0' && event.key <= '9') || event.key == '.' || event.key == ',' || event.key == 'Home' || event.key == 'End' ||
            event.key == 'ArrowLeft' || event.key == 'ArrowRight' || event.key == 'Delete' || event.key == 'Backspace' || event.key == 'Tab' ||
            (event.which == 67 && event.ctrlKey === true) || (event.which == 88 && event.ctrlKey === true) ||
            (event.which == 65 && event.ctrlKey === true) || (event.which == 86 && event.ctrlKey === true)) {
            if (fid == 101) {
                if (/^[\\\"\'\;\:\>\|~`!@#\$%^&*\(\)]$/i.test(event.key)) {
                    event.preventDefault();
                }
            }
            if (value.includes(".")) {
                if (/^[\\\"\'\;\:\>\|~`!@#\$%^&*.\(\)]$/i.test(event.key)) {
                    event.preventDefault();
                }
            }
        }
        else {
            event.preventDefault();
        }
    };
    wtopupReport.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    wtopupReport = __decorate([
        core_1.Component({
            selector: 'wallettopup-report',
            template: " \n  <div *ngIf=\"!_pdfdata\">  \n\t<div class=\"container col-md-12 col-sm-12 col-xs-12\" style=\"padding: 0px 5%;\">\n\t\t<form class=\"form-horizontal\" ngNoForm>\n\t\t\t<legend>Wallet Top up Report</legend>\n\t\t\t\t<div class=\"cardview list-height\"> \n\t\t\t\t\t<div class=\"col-md-12\" style=\"margin-bottom: 10px;\">\n          <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goPrint('pdf')\">Preview</button>              \n\t\t\t\t  <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goPrint('excel')\">Export</button>\n          </div>\n          <div class=\"col-md-12\">\n\t\t\t\t\t\t\t<div class=\"form-group\">            \n\t\t\t\t\t\t\t\t<label class=\"col-md-2\">From Date</label>\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t<div class=\"col-md-3\">\n\t\t\t\t\t\t\t\t\t<my-date-picker name=\"mydate\"  [options]=\"myDatePickerOptions\" [(ngModel)]=\"_dates.fromDate\" ngDefaultControl></my-date-picker>                  \n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<label class=\"col-md-2\">To Date</label>\n\t\t\t\t\t\t\t\t<div class=\"col-md-3\">\n\t\t\t\t\t\t\t\t\t<my-date-picker name=\"mydate\" [options]=\"myDatePickerOptions\" [(ngModel)]=\"_dates.toDate\" ngDefaultControl></my-date-picker>\n\t\t\t\t\t\t\t\t\t<!-- <input class=\"form-control\" type=\"date\" [(ngModel)]=\"_obj.toDate\" required=\"true\" [ngModelOptions]=\"{standalone: true}\">  -->                            \n\t\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t<div class=\"col-md-2\">\n\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t<input type='checkbox' [(ngModel)]=\"_obj.alldate\" [ngModelOptions]=\"{standalone: true}\" (click)='getAllDate($event)'>\n\t\t\t\t\t\t\t\tALL\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\t\n\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t<label class=\"col-md-2\">From Account</label>\n\t\t\t\t\t\t\t\t<div class=\"col-md-3\">\n\t\t\t\t\t\t\t\t  <input type=\"text\" [(ngModel)]=\"_obj.fromAccount\" class=\"form-control input-sm\" (keydown)=\"restrictSpecialCharacter($event, 101 ,_obj.fromAccount)\" pattern=\"[0-9.,]+\" required autofocus/>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<label class=\"col-md-2\">To Account</label>\n\t\t\t\t\t\t\t\t<div class=\"col-md-3\">\n\t\t\t\t\t\t\t\t  <input type=\"text\" [(ngModel)]=\"_obj.toAccount\" (keydown)=\"restrictSpecialCharacter($event, 101 ,_obj.toAccount)\" pattern=\"[0-9.,]+\"  class=\"form-control input-sm\" />\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t<label class=\"col-md-2\">From Name</label>\n\t\t\t\t\t\t\t\t<div class=\"col-md-3\">\n\t\t\t\t\t\t\t\t  <input type=\"text\" [(ngModel)]=\"_obj.t1\" class=\"form-control input-sm\" required autofocus/>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<label class=\"col-md-2\">To Name</label>\n\t\t\t\t\t\t\t\t<div class=\"col-md-3\">\n\t\t\t\t\t\t\t\t  <input type=\"text\" [(ngModel)]=\"_obj.t2\" class=\"form-control input-sm\" />\n\t\t\t\t\t\t\t\t</div>\n              </div>\n            </div>\n\t\t\t\t\t\t\t<!--<div class=\"form-group\">\n\t\t\t\t\t\t\t  <label class=\"col-md-2\">Transfer Type</label>\n\t\t\t\t\t\t\t  <div class=\"col-md-3\">\n\t\t\t\t\t\t\t  <select [(ngModel)]=\"_obj.transtype\" [ngModelOptions]=\"{standalone: true}\"  (change)=\"changeTransferType($event.target.options)\"  required=\"true\"   class=\"form-control input-sm\"  >\n\t\t\t\t\t\t\t\t <option *ngFor=\"let item of ref._lov3.refAccountTransferType\" value=\"{{item.value}}\" >{{item.caption}}</option>\n\t\t\t\t\t\t\t  </select>                     \n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>-->\t\t\t\t\n\t\t\t\t</div>\n        </form> \n        <div id=\"accdownload\" style=\"display: none;width: 0px;height: 0px;\"></div>       \t\t\t\t\n\t</div>\n</div>    \t\n\n\t<div *ngIf=\"_pdfdata\">\n    <img style='margin-left: 10px;cursor: pointer;' src='image/Excel-icon.png'  (click)=\"goExport('excel')\">\n\t  <button type=\"button\" class=\"close\"  (click)=\"goClose()\">&times;</button>\n\t  <iframe id=\"frame1\" [src]=\"_printUrl\" width=\"100%\" height=\"100%\"></iframe>\n    <div id=\"accdownload\" style=\"display: none;width: 0px;height: 0px;\"></div>\n\t</div>\n\t\n\t<div [hidden] = \"_mflag\">\n\t  <div class=\"modal\" id=\"loader\"></div>\n\t</div>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n  ",
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef, core_1.Renderer, rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences, platform_browser_1.DomSanitizer])
    ], wtopupReport);
    return wtopupReport;
}());
exports.wtopupReport = wtopupReport;
//# sourceMappingURL=wtopup-report.component.js.map