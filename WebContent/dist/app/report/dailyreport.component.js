"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require("@angular/platform-browser");
var router_1 = require('@angular/router');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
core_1.enableProdMode();
var DailyReport = (function () {
    function DailyReport(el, renderer, ics, _router, route, http, ref, sanitizer) {
        this.el = el;
        this.renderer = renderer;
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this.sanitizer = sanitizer;
        this._util = new rp_client_util_1.ClientUtil();
        this.today = this._util.getTodayDate();
        this.myDatePickerOptions = this._util.getDatePicker();
        this._dates = { "date": this.dateobj };
        this._mflag = true;
        this._rpList = true;
        this._divPrint = false;
        this._OrderByList = [];
        this._ReportByList = [];
        this._ReportFormatList = [];
        this._CurrencyList = [{ "value": "MMK", "caption": "MMK" }];
        this._ProductTypeList = [{ "value": "00", "caption": "Domestic Account" }, { "value": "01", "caption": "Wallet Account" }];
        this._CounterList = [{ "value": "ALL", "caption": "ALL" }];
        this.bnk = 15;
        this._hide = true;
        this._curname = "";
        this.isDBanking = false;
        ////////
        this._hidechkOD = false;
        this._hidechkSchedule = false;
        this._hidechkReversal = true;
        this._hidechkOrder = true;
        this._hidechkRevaluate = true;
        this._hidechkSummary = false;
        this._hidechkByMonth = false;
        ////////
        this._disablechkrevaluate = true;
        this._disablechkReversal = false;
        this._disablechkOrder = false;
        this._disablechkSummary = false;
        this._disablechkByMonth = false;
        this._disablechkOD = false;
        this._disablechkSchedule = false;
        this._disablecurCode = false;
        this.obj = this.getDefaultObj();
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        this.setTodayDateObj();
        this.setDateObjIntoString();
    }
    /////
    DailyReport.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    DailyReport.prototype.getDefaultObj = function () {
        return { "rdoType": "daybook",
            "cboDayBookType": "",
            "branchCode": "",
            "myDate": "",
            "reportBy": "2",
            "reportOptions": "",
            "orderBy": "A",
            "serialNoFrom": "1",
            "serialNoTo": "9999999999",
            "curCode": "MMK",
            "counterType": "ALL",
            "cboRFormat": "",
            "_disablechkReversalI": false,
            "_disablechkDO": false,
            "_disablechkrevaluate": false,
            "chkReversal": false,
            "chkOrder": false,
            "chkrevaluate": false,
            "chkSummary": false,
            "chkByMonth": false,
            "chkOD": false,
            "chkSchedule": false
        };
    };
    DailyReport.prototype.ngOnInit = function () {
        this.orderByList();
        this.ReportByList();
        this.switchRadio("daybook");
    };
    DailyReport.prototype.setTodayDateObj = function () {
        this._dates.date = this._util.changestringtodateobject(this.today);
    };
    DailyReport.prototype.setDateObjIntoString = function () {
        this.obj.myDate = this._util.getDatePickerDateymd(this._dates.date);
        console.log('My Date' + this.obj.myDate);
    };
    DailyReport.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    DailyReport.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    DailyReport.prototype.isDomesticBanking = function () {
        if (this.bnk == 1) {
            this.isDBanking = true;
        }
        else {
            this.isDBanking = false;
        }
    };
    DailyReport.prototype.keyPressNumber = function (event) {
        var pattern = /[0-9]/;
        var inputChar = String.fromCharCode(event.charCode);
        if (!pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    DailyReport.prototype.orderByList = function () {
        this._OrderByList = [];
        this._OrderByList = [{ "caption": "Account Number", "value": "A" }, { "caption": "Trans No", "value": "T" }];
        if (this._OrderByList.length > 0) {
            this.obj.orderBy = this._OrderByList[0].value;
        }
    };
    DailyReport.prototype.ReportByList = function () {
        this._ReportByList = [];
        this._ReportByList = [{ "caption": "GL Code", "value": "2" }];
        if (this._ReportByList.length > 0) {
            this.obj.reportBy = this._ReportByList[0].value;
        }
        if (this._ProductTypeList.length > 0) {
            this.obj.cboDayBookType = this._ProductTypeList[0].value;
        }
    };
    DailyReport.prototype.switchRadio = function (rdoType) {
        if (rdoType == "daybook") {
            this._hidechkReversal = true;
            this._hidechkOrder = true;
            this._hidechkSchedule = false;
            this._hidechkRevaluate = true;
            this._disablechkrevaluate = false;
            this.obj.chkrevaluate = true;
            this._hidechkSummary = false;
            this._hidechkByMonth = false;
            this._hide = true;
            this._ReportFormatList = [{ "caption": "Format 1", "value": "1" }, { "caption": "Format 3", "value": "3" }, { "caption": "Format 4", "value": "4" }];
            if (this._ReportFormatList.length > 0) {
                this.obj.cboRFormat = this._ReportFormatList[0].value;
            }
            this._hidechkRevaluate = true; //setVisible(true) 
        }
        else if (rdoType == "transferscroll") {
            this._hidechkReversal = true;
            this._hidechkOrder = true;
            this._hidechkSchedule = false;
            this._hidechkRevaluate = true;
            this._disablechkrevaluate = false;
            this.obj.chkrevaluate = false;
            this._hidechkSummary = false;
            this._hidechkByMonth = false;
            this._hide = true;
            this._ReportFormatList = [{ "caption": "Format 1", "value": "1" }, { "caption": "Format 3", "value": "3" }];
            if (this._ReportFormatList.length > 0) {
                this.obj.cboRFormat = this._ReportFormatList[0].value;
            }
            //////
            this._disablecurCode = false;
            this._hidechkRevaluate = true; //setVisible(true)
            this._hidechkSummary = false;
            if (this.bnk == 1) {
            }
            else {
            }
            this.obj.chkrevaluate = false;
            this._disablechkrevaluate = false;
        }
        else if (rdoType == "cleancash") {
            this._hidechkReversal = true;
            this._hidechkOrder = true;
            this._hidechkSchedule = false;
            this._hidechkRevaluate = false;
            this._hidechkSummary = false;
            this._hidechkByMonth = false;
            this._hide = false;
            this._ReportFormatList = [{ "caption": "Format 1", "value": "1" }];
            if (this._ReportFormatList.length > 0) {
                this.obj.cboRFormat = this._ReportFormatList[0].value;
            }
            ///////////
            this._disablecurCode = false;
            this._hidechkRevaluate = false;
            this._hidechkSummary = false;
            //this.showCounter(false);
            //bindCounter();
            this.obj.chkrevaluate = false;
            this._disablechkrevaluate = false;
        }
    };
    DailyReport.prototype.goPrint = function () {
        this._rpList = false;
        this._divPrint = true;
        this._printUrl = "";
        //var BCode =this._branchcode;
        var DBDate = this._util.getDatePickerDateymd(this._dates.date);
        var PType = this.obj.cboDayBookType;
        if (PType == "0000" || PType == "00") {
            PType = "GL";
        }
        var tsf = this.obj.serialNoFrom;
        var tst = this.obj.serialNoTo;
        var PS = "1";
        var RF = "";
        if (this.obj.curCode == "Base" && PType != "GL") {
            RF = "1";
        }
        else {
            RF = this.obj.cboRFormat;
        }
        var RB = "";
        RB = this.obj.reportBy;
        var BM = "";
        if (this.obj.chkByMonth == true) {
            BM = "1";
        }
        var OB = this.obj.orderBy;
        var incReversal = this.obj.chkReversal;
        var t1 = false;
        var order = this.obj.chkOrder;
        var t2 = false;
        var DBType = "";
        var pBCode = "";
        var RC = "";
        if (this.obj.rdoType == "daybook") {
            DBType = "DB";
            PType = PType;
            OB = OB;
            tsf = tsf;
            tst = tst;
            t1 = incReversal;
            t2 = order;
            DBDate = DBDate;
            pBCode = "002";
            PS = PS;
            RC = this.obj.curCode;
            RF = RF;
            RB = RB;
            BM = BM;
        }
        else if (this.obj.rdoType == "transferscroll") {
            DBType = "TS";
            PType = PType;
            OB = OB;
            tsf = tsf;
            tst = tst;
            t1 = incReversal;
            t2 = order;
            DBDate = DBDate;
            pBCode = "002";
            PS = PS;
            RC = this.obj.curCode;
            RF = RF;
            RB = RB;
            BM = BM;
        }
        else if (this.obj.rdoType == "cleancash") {
            DBType = "CC";
            PType = "";
            OB = "";
            tsf = "";
            tst = "";
            t1 = incReversal;
            t2 = false;
            DBDate = DBDate;
            pBCode = "002";
            PS = PS;
            RC = this.obj.curCode;
            RF = RF;
            RB = RB;
            BM = BM;
        }
        var url = this.ics.icbsrpturl + "/PrepareReport.jsp?";
        //url += "ip="+this.ics._profile.serverIP;
        url += "&UID=" + this.ics._profile.userID;
        url += "&RT=" + DBType;
        url += "&PType=" + PType;
        url += "&Date=" + this._util.getDatePickerDateymd(this._dates.date);
        url += "&OB=" + OB;
        url += "&SNF=" + tsf;
        url += "&SNT=" + tst;
        url += "&IRT=" + t1;
        url += "&IDO=" + t2;
        url += "&BC=" + pBCode;
        url += "&PS=" + PS;
        url += "&RC=" + this.obj.curCode;
        url += "&RF=" + RF;
        url += "&RB=" + RB;
        url += "&BM=" + BM;
        url += "&PDL=" + this.obj.cboDayBookType;
        if (this.obj.rdoType == "daybook" || this.obj.rdoType == "transferscroll") {
            url += "&IREV=false";
            if (this.bnk == 1) {
                if (this.obj.branchCode != "999") {
                    if (this.obj.rdoType == "daybook" && this.obj.cboDayBookType == "All" || this.obj.rdoType == "transferscroll") {
                    }
                }
            }
            else {
                url += "&CID=" + "";
            }
        }
        if (this.obj.chkOD == true) {
            url += "&PF=" + "OD";
        }
        //    if(this.isCard){
        //   url += "&PF="+ "CARD";
        //  }
        if (this.obj.chkSchedule == true) {
            url += "&Type=" + this.obj.chkSchedule;
        }
        else {
            url += "&Type=false";
        }
        if (this.obj.cboDayBookType == "daybook" && this.obj.cboDayBookType.includes("Army")) {
            url += "&Fields=Army";
        }
        if (this.obj.cboDayBookType == "daybook" && this.obj.cboDayBookType.includes("Schedule")) {
            url += "&Fields=Schedule";
        }
        var Summary = "";
        if (this.obj.cboDayBookType == "GL" && this.obj.rdoType == "daybook") {
            if (this.obj.chkSummary == true) {
                Summary = "SummaryOn";
            }
            else {
                Summary = "SummaryOff";
            }
            url += "&AccName=" + Summary;
        }
        url += "&FBC=002";
        this._printUrl = url;
    };
    DailyReport.prototype.goClosePrint = function () {
        this._divPrint = false;
        this._rpList = true;
    };
    DailyReport.prototype.goClear = function () {
        this.obj = this.getDefaultObj();
        this.obj.cboRFormat = "1";
        this.obj.cboDayBookType = "00";
    };
    DailyReport = __decorate([
        core_1.Component({
            selector: 'dailyreport',
            template: " \n  <div *ngIf='_rpList' >\n  <div class=\"container-fluid\">\n  <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n      <form class= \"form-horizontal\" ngNoForm>        \n  \t\t\t\t<legend>Daily Report</legend>\n  \t\t\t\t<div class=\"cardview list-height\"> \n  \t\t\t\t  <div class=\"row col-md-12\">  \n  \t\t\t\t  <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goClear()\">Clear</button>              \n  \t\t\t\t  <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goPrint()\">Print</button>          \n                   </div>\n           <div class=\"row col-md-12\">&nbsp;</div>\n                <div class=\"form-group\"> \n                   <div class=\"col-md-12\">\n                       <div class=\"form-group\">\n                       <div class=\"col-md-2\" >&nbsp;</div>\n                           <div class=\"col-md-2\" >\n                               <input type=\"radio\" value=\"daybook\" name=\"rdType\" [(ngModel)]=\"obj.rdoType\" (click)=\"switchRadio($event.target.value)\" [checked]=\"rdoDayBook\"> <label style=\"font-weight:normal;\" disabled>DayBook</label>\n                            </div>\n                            <div class=\"col-md-2\" >  \n                                <input type=\"radio\" value=\"transferscroll\" name=\"rdType\" [(ngModel)]=\"obj.rdoType\" (click)=\"switchRadio($event.target.value)\" [checked]=\"rdoTransferScroll\"> <label style=\"font-weight:normal;\" disabled>Transfer Scroll</label>\n                            </div>\n                            <div class=\"col-md-2\" >  \n                                <input type=\"radio\" value=\"cleancash\" name=\"rdType\" [(ngModel)]=\"obj.rdoType\" (click)=\"switchRadio($event.target.value)\" [checked]=\"rdoCleanCash\"> <label style=\"font-weight:normal;\" disabled>Clean Cash</label>\n                            </div>\n                       </div> \n                       <div class=\"form-group\">\n                           <label class=\"col-md-2\" >Product Type</label>\n                           <div class=\"col-md-3\">\n                             <select  required=\"true\" class=\"form-control col-md-0\"  [(ngModel)]=\"obj.cboDayBookType\"  >\n                             <option *ngFor=\"let obj of _ProductTypeList\" value=\"{{obj.value}}\">{{obj.caption}}</option>\n                             </select>                     \n                           </div>\n                           <label class=\"col-md-2\" >Order By</label>\n                               <div class=\"col-md-3\">\n                                   <select  required=\"true\"   [(ngModel)]=\"obj.orderBy\"   class=\"form-control col-md-0\"  >\n                                   <option *ngFor=\"let obj of _OrderByList\" value=\"{{obj.value}}\">{{obj.caption}}</option>\n                                   </select>                      \n                               </div>\n                        </div>\n                        <div class=\"form-group\">\n                               <label class=\"col-md-2\" >Serial No.</label>\n                               <div class=\"col-md-3\">\n                                   <input id=\"idSerialNo1\" type=\"text\" class=\"input-sm fontSizeSmall fontWeightNormal\" style=\"width: 46%;  border-radius:5px;border:1px #D2D2D2 solid; \" [ngModelOptions]=\"{standalone: true}\" \n                                    [(ngModel)]=\"obj.serialNoFrom\" (keypress)=\"keyPressNumber($event)\" >\n                                    <label >-</label>\n                                    <input id=\"idSerialNo2\" type=\"text\" class=\"input-sm fontSizeSmall fontWeightNormal\" style=\"width: 50%;  border-radius:5px;border:1px #D2D2D2 solid; \" [ngModelOptions]=\"{standalone: true}\" \n                                    [(ngModel)]=\"obj.serialNoTo\"  (keypress)=\"keyPressNumber($event)\" >\n                                </div> \n                                <label class=\"col-md-2\">Date</label>\t\t\t\t\t\t\t\t\n                                <div class=\"col-md-3\">\n                                    <my-date-picker name=\"mydate\"   [options]=\"myDatePickerOptions\"  [(ngModel)]=\"_dates.date\" (ngModelChange)=\"changedate()\" ngDefaultControl></my-date-picker>                  \n                                </div>\n                        </div>\n                        <div class=\"form-group\">  \n                            <label class=\"col-md-2\">Currency</label>\n                            <div class=\"col-md-3\">\n                            <select  required=\"true\"  [(ngModel)]=\"obj.curCode\"     class=\"form-control col-md-0\"  >\n                            <option *ngFor=\"let code of _CurrencyList\" value=\"{{code.value}}\">{{code.caption}}</option>\n                             </select>\n                            </div>\n                            <label class=\"col-md-2\" >Report By</label>\n                            <div class=\"col-md-3\">\n                              <select   required=\"true\"   [(ngModel)]=\"obj.reportBy\"  class=\"form-control col-md-0\"  >\n                                          <option *ngFor=\"let item  of _ReportByList\" value=\"{{item.value}}\" >{{item.caption}}</option> \n                              </select>                     \n                          </div>\n                        </div>\n                        <div class=\"form-group\">\n                \t\t\t\t\t<label class=\"col-md-2\" >Report Format</label>\n                \t\t\t\t\t\t<div class=\"col-md-3\">\n                \t\t\t\t\t\t\t<select   required=\"true\"   [(ngModel)]=\"obj.cboRFormat\"  class=\"form-control col-md-0\"  >\n                                            <option *ngFor=\"let obj of _ReportFormatList\" value=\"{{obj.value}}\">{{obj.caption}}</option>\n                \t\t\t\t\t\t\t</select>                      \n                \t\t\t\t\t\t</div>\n                        </div>\n                        <div class=\"form-group\">\n                              <label class=\"col-md-2\" >Report Options</label>\n                              <div *ngIf=\"_hidechkReversal\"  class=\"col-md-2\">\n                                    <label>\n                                    <input type='checkbox' [(ngModel)]=\"obj.chkReversal\" [disabled]=\"_disablechkReversal\" [ngModelOptions]=\"{standalone: true}\" >\n                                        Reversal Inclusive\n                                    </label>                 \n                                </div>\n                                <div  *ngIf=\"_hidechkOrder\" class=\"col-md-2\">\n                                    <label>\n                                    <input type='checkbox'[(ngModel)]=\"obj.chkOrder\" [disabled]=\"_disablechkOrder\" [ngModelOptions]=\"{standalone: true}\" >\n                                    Descending Order\n                                    </label>                 \n                                </div>\n                               \n                                <div  *ngIf=\"_hidechkSummary\" class=\"col-md-2\">\n                                    <label>\n                                    <input type='checkbox' [(ngModel)]=\"obj.chkSummary\"  [disabled]=\"_disablechkSummary\" [ngModelOptions]=\"{standalone: true}\" >\n                                    Summary\n                                    </label>                 \n                                </div>\n                                <div  *ngIf=\"_hidechkByMonth\" class=\"col-md-2\">\n                                    <label>\n                                    <input type='checkbox'  [(ngModel)]=\"obj.chkByMonth\"  [disabled]=\"_disablechkByMonth\" [ngModelOptions]=\"{standalone: true}\" >\n                                    By Month\n                                    </label>                 \n                                </div>\n                        </div>\n                    </div>\n              </div>\n            </div>\n      </form>\n      </div>\n      <div>\n      <div id=\"downloadPdf\" style=\"display: none;width: 0px;height: 0px;\"></div>\n      <div id=\"downloadExcel\" style=\"display: none;width: 0px;height: 0px;\"></div>\n   </div>\n  </div>\n</div>\n</div>     \n    <div [hidden] = \"_mflag\">\n      <div class=\"modal\" id=\"loader\"></div>\n   </div> \n   <rpt-container *ngIf='_divPrint' (rpClose)=\"goClosePrint()\" [rpSrc]=\"_printUrl\"></rpt-container> \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n  ",
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef, core_1.Renderer, rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences, platform_browser_1.DomSanitizer])
    ], DailyReport);
    return DailyReport;
}());
exports.DailyReport = DailyReport;
//# sourceMappingURL=dailyreport.component.js.map