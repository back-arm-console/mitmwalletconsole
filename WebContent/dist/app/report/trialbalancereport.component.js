"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require("@angular/platform-browser");
var router_1 = require('@angular/router');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
core_1.enableProdMode();
var TrialBalanceReport = (function () {
    function TrialBalanceReport(el, renderer, ics, _router, route, http, ref, sanitizer) {
        this.el = el;
        this.renderer = renderer;
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this.sanitizer = sanitizer;
        this._util = new rp_client_util_1.ClientUtil();
        this.today = this._util.getTodayDate();
        this.myDatePickerOptions = this._util.getDatePicker();
        this._dates = { "date": this.dateobj };
        this._mflag = true;
        this._rpList = true;
        this._divPrint = false;
        this.flag = false;
        this.chkflag = false;
        this._trial = [{ "value": "1", "caption": "Complete Trial" }, { "value": "2", "caption": "First Trial" }, { "value": "3", "caption": "Second Trial" }];
        this._reportBy = [{ "value": "2", "caption": "GL Code" }];
        this._currency = [{ "value": "MMK", "caption": "MMK" }];
        this._display = [{ "value": "A", "caption": "Display Debit(Assets)First" }, { "value": "L", "caption": "Display Debit(Liabilities)First" }];
        this._reportFormat = [{ "value": "1", "caption": "Format1" }, { "value": "2", "caption": "Format2" }, { "value": "4", "caption": "Format4" }];
        this.obj = this.getTrialObject();
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        this.setTodayDateObj();
        this.setDateObjIntoString();
    }
    TrialBalanceReport.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    TrialBalanceReport.prototype.getTrialObject = function () {
        return { "type": "G", "display": "A", "date": {}, "reportCode": "2", "branch": "002", "currency": "MMK", "reportFormat": "1", "reftrial": "1" };
    };
    TrialBalanceReport.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    TrialBalanceReport.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    TrialBalanceReport.prototype.setTodayDateObj = function () {
        this._dates.date = this._util.changestringtodateobject(this.today);
    };
    TrialBalanceReport.prototype.setDateObjIntoString = function () {
        this.obj.date = this._util.getDatePickerDateymd(this._dates.date);
        console.log('My Date' + this.obj.date);
    };
    TrialBalanceReport.prototype.changedate = function () {
        var date1 = {};
        this.chkflag = false;
        date1 = this._util.getDatePickerDateymd(this._dates.date);
        var year = date1.toString().substring(0, 4);
        var month = date1.toString().substring(4, 6);
        var day = date1.toString().substring(6, 8);
        if (month == '03' && day == '31') {
            this.flag = true;
        }
        else if (month == '09' && day == '30') {
            this.flag = true;
        }
        else {
            this.flag = false;
        }
    };
    TrialBalanceReport.prototype.convertDate3 = function (m) {
        if (m != null) {
            // var returnDate = m.date.year + "-" + ("0" + m.date.month).slice(-2) + "-" + ("0" + m.date.day).slice(-2);
            var returnDate = ("0" + m.date.day).slice(-2) + "/" + ("0" + m.date.month).slice(-2) + "/" + m.date.year;
            return returnDate;
        }
    };
    TrialBalanceReport.prototype.changeTrial = function (value) {
        this.type = value;
        if (this.type == 1) {
            this.chkflag = false;
        }
        else {
            this.chkflag = true;
        }
    };
    TrialBalanceReport.prototype.goClear = function () {
        // this._BranchList=[];
        this._display = [{ "value": "A", "caption": "Display Debit(Assets)First" }, { "value": "L", "caption": "Display Debit(Liabilities)First" }];
        this._reportFormat = [{ "value": "1", "caption": "Format1" }, { "value": "2", "caption": "Format2" }, { "value": "4", "caption": "Format4" }, { "value": "6", "caption": "Format6" }];
        this.obj.display = this._display[0].value;
        this.obj.reportFormat = this._reportFormat[0].value;
        this.obj.type = "G";
        this.flag = false;
    };
    TrialBalanceReport.prototype.goClosePrint = function () {
        this._rpList = true;
        this._divPrint = false;
    };
    TrialBalanceReport.prototype.convertDate = function (m) {
        if (m != null) {
            var returnDate = m.date.year + ("0" + m.date.month).slice(-2) + ("0" + m.date.day).slice(-2);
            return returnDate;
        }
    };
    TrialBalanceReport.prototype.goPrint = function () {
        this._rpList = false;
        this._divPrint = true;
        this._printUrl = "";
        // let url =this.ics.icbsrpturl+"/PrepareReportThree.jsp?";
        var url = this.ics.icbsrpturl + "/PrepareReportThree.jsp?";
        //url += "&ip=" + this.ics._profile.serverIP;
        url += "&UID=" + this.ics._profile.userID;
        //url += "ip=103.233.205.144";
        //url += "&UID=SysAdmin";
        url += "&RT=TRIAL";
        url += "&TT=" + this.obj.type;
        url += "&Date=" + this._util.getDatePickerDateymd(this._dates.date);
        ;
        url += "&DO=" + this.obj.display;
        url += "&BC=ALL";
        url += "&WS=ALL";
        url += "&RC=" + this.obj.currency;
        url += "&PS=1";
        url += "&RF=" + this.obj.reportFormat;
        url += "&BNK=15";
        url += "&RB=" + this.obj.reportCode;
        url += "&FBC=999";
        if (this.obj.reftrial == '2') {
            url += "&YE=FT";
        }
        else if (this.obj.reftrial == '3') {
            url += "&YE=ST";
        }
        if (this.type == 1) {
            url += "&TS=false";
        }
        else {
            url += "&TS=true";
        }
        this._printUrl = url;
    };
    TrialBalanceReport = __decorate([
        core_1.Component({
            selector: 'fmtrialbalance',
            template: " \n  <div *ngIf='_rpList' >\n  <div class=\"container-fluid\">\n  <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n      <form class= \"form-horizontal\" ngNoForm>        \n  \t\t\t\t<legend>Trial Balance Report</legend>\n  \t\t\t\t<div class=\"cardview list-height\"> \n  \t\t\t\t  <div class=\"row col-md-12\">  \n  \t\t\t\t  <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goClear()\">Clear</button>              \n  \t\t\t\t  <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goPrint()\">Print</button>          \n                   </div>\n           <div class=\"row col-md-12\">&nbsp;</div>\n                <div class=\"form-group\"> \n                   <div class=\"col-md-12\">\n                       <div class=\"form-group\">\n                       <div class=\"col-md-2\"></div>\n                           <div class=\"col-md-2\" >\n                               <input type=\"radio\" name=\"G\" value=\"G\"  [(ngModel)]=\"obj.type\" >\n                               <label style=\"font-weight:normal;\">&nbsp;General</label>\n                            </div>\n                            <div class=\"col-md-2\" >  \n                                    <input type=\"radio\" name=\"D\" value=\"D\"  [(ngModel)]=\"obj.type\" >\n                                    <label style=\"font-weight:normal;\">&nbsp;Detail</label>\n                            </div>\n                       </div>\n  \t\t\t\t\t\t\n  \t\t\t\t\t\t<div class=\"form-group\">\n  \t\t\t\t\t\t  <label class=\"col-md-2\" >Display</label>\n  \t\t\t\t\t\t  <div class=\"col-md-3\">\n  \t\t\t\t\t\t\t<select  required=\"true\"  [(ngModel)]=\"obj.display\"   class=\"form-control col-md-0\"  >\n  \t\t\t\t\t\t\t   <option *ngFor=\"let item  of _display\" value=\"{{item.value}}\" >{{item.caption}}</option> \n  \t\t\t\t\t\t\t</select>                     \n                </div>\n                <label class=\"col-md-2\">Date</label>\t\t\t\t\t\t\t\t\n  \t\t\t\t\t\t\t    <div class=\"col-md-3\">\n                        <my-date-picker name=\"mydate\"   [options]=\"myDatePickerOptions\"  [(ngModel)]=\"_dates.date\" (ngModelChange)=\"changedate()\" ngDefaultControl></my-date-picker> \n                                                                       \n  \t\t\t\t\t\t\t    </div>\n  \t\t\t\t\t\t \n                 </div>\n                    <div class=\"form-group\">            \n  \t\t\t\t\t\t\t    \n  \t\t\t\t\t\t\t    <label class=\"col-md-2\">Currency</label>\n  \t\t\t\t\t\t\t    <div class=\"col-md-3\">\n                                  <select  required=\"true\"  [(ngModel)]=\"obj.currency\"   class=\"form-control col-md-0\"  >\n                                  <option *ngFor=\"let item  of _currency\" value=\"{{item.value}}\" >{{item.caption}}</option>\n                                   </select>\n                    </div>\n                    <label class=\"col-md-2\" >Report By</label>\n              \t\t\t\t\t<div class=\"col-md-3\">\n              \t\t\t\t\t\t<select   required=\"true\"  [(ngModel)]=\"obj.reportCode\"  class=\"form-control col-md-0\"  >\n                                        <option *ngFor=\"let item  of _reportBy\" value=\"{{item.value}}\" >{{item.caption}}</option> \n              \t\t\t\t\t\t</select>                     \n          \t\t\t\t\t\t</div>\n                      </div>\n                      <div class=\"form-group\">\n              \t\t\t\t\t\t<label class=\"col-md-2\" >Report Format</label>\n              \t\t\t\t\t\t\t<div class=\"col-md-3\">\n              \t\t\t\t\t\t\t\t<select   required=\"true\"   [(ngModel)]=\"obj.reportFormat\"  class=\"form-control col-md-0\"  >\n                                                <option *ngFor=\"let item  of _reportFormat\" value=\"{{item.value}}\" >{{item.caption}}</option> \n              \t\t\t\t\t\t\t\t</select>                      \n                            </div>\n                            <label class=\"col-md-2\" >&nbsp;</label>\n                              <div *ngIf=\"flag\" class=\"col-md-3\">\n                                  <select   required=\"true\"  class=\"form-control col-md-0\" [(ngModel)]=\"obj.reftrial\" (change)=\"changeTrial(t.value)\" [disabled]=\"editMode\" #t >\n                                    <option *ngFor=\"let item  of _trial\" value=\"{{item.value}}\" >{{item.caption}}</option> \n                                  </select>                     \n                              </div>\n                              <div  *ngIf='chkflag' class=\"col-md-2\">\n                                <label>\n                                <input type='checkbox' [(ngModel)]=\"_chkTrial\" [ngModelOptions]=\"{standalone: true}\" >\n                               Trial Save\n                                </label>                 \n                                </div>\n                      </div>\n                    </div>\n              </div>\n            </div>\n      </form>\n      </div>\n      <div>\n      <div id=\"downloadPdf\" style=\"display: none;width: 0px;height: 0px;\"></div>\n      <div id=\"downloadExcel\" style=\"display: none;width: 0px;height: 0px;\"></div>\n   </div>\n  </div>\n</div>\n</div>     \n    <div [hidden] = \"_mflag\">\n      <div class=\"modal\" id=\"loader\"></div>\n   </div> \n   <rpt-container *ngIf='_divPrint' (rpClose)=\"goClosePrint()\" [rpSrc]=\"_printUrl\"></rpt-container> \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n  ",
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef, core_1.Renderer, rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences, platform_browser_1.DomSanitizer])
    ], TrialBalanceReport);
    return TrialBalanceReport;
}());
exports.TrialBalanceReport = TrialBalanceReport;
//# sourceMappingURL=trialbalancereport.component.js.map