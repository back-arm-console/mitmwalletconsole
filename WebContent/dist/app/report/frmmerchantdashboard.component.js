"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var dashboard_service_1 = require('../framework/dashboard-service');
core_2.enableProdMode();
var FrmMerchantDashBoardComponent = (function () {
    function FrmMerchantDashBoardComponent(ics, _router, dashboard, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.dashboard = dashboard;
        this.http = http;
        this.ref = ref;
        this.loginUser = "";
        this._hideCNP = true;
        this._hideSkypay = true;
        this._hideSkynet = true;
        this._obj = this._obj = this.getDefaultObj();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!this.ics.getRole() || this.ics.getRole() == 0)
            this._router.navigate(['/login']);
        else
            this.ics.confirmUpload(true);
        this.getmerchantidlist();
    }
    FrmMerchantDashBoardComponent.prototype.getDefaultObj = function () {
        return {
            "merchantID": "", "merchantName": "", "processingCode": "", "feature": 0
        };
    };
    FrmMerchantDashBoardComponent.prototype.getmerchantidlist = function () {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'service001/getmerchantidlistdetail?userID=' + this.loginUser).subscribe(function (data) {
            _this.ref._lov3.ref015 = data.ref015;
            //let merchant = [{ "value": "", "caption": ""}];
            if (_this.ref._lov3.ref015 != null) {
                if (!(_this.ref._lov3.ref015 instanceof Array)) {
                    var m = [];
                    m[1] = _this.ref._lov3.ref015;
                }
            }
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
            else {
            }
        }, function () { });
    };
    FrmMerchantDashBoardComponent.prototype.changeMerchant = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex;
        var value = options[options.selectedIndex].value;
        this._obj.merchantID = value;
        for (var i = 0; i < this.ref._lov3.ref015.length; i++) {
            if (this.ref._lov3.ref015[i].value == value) {
                this._obj.merchantID = this.ref._lov3.ref015[i].value;
                this._obj.processingCode = this.ref._lov3.ref015[i].processingCode;
            }
        }
        if (this._obj.processingCode == '050200') {
            this._hideCNP = false;
            this.getDashBoardGeneralCNPBill();
            this.getDashBoardGeneralCNPTopup();
            this.getDashBoardBarMonthlyCountBill();
            this.getDashBoardBarMonthlyAmountBill();
            this.getBarTxnStatusofCurrentMonthBill();
            this.getDashBoardBarMonthlyCountTopup();
            this.getDashBoardBarMonthlyAmountTopup();
            this.getBarTxnStatusofCurrentMonthTopup();
        }
        else if (this._obj.processingCode == '040300') {
            this._hideSkypay = false;
            this.getDashBoardGeneralSky();
            this.getDashBoardBarMonthlyCount();
            this.getDashBoardBarMonthlyAmount();
            this.getBarTxnStatusofCurrentMonth();
        }
        else if (this._obj.processingCode == '080400') {
            this._hideSkynet = false;
            this.getDashBoardBarMonthlyskynetCount();
            this.getDashBoardBarMonthlyskynetAmount();
            this.getBarTxnStatusofskynetCurrentMonth();
        }
    };
    FrmMerchantDashBoardComponent.prototype.getDashBoardGeneralCNPBill = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service002/getGeneralDashboard?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
        this._obj.feature = 1;
        var json = this._obj;
        var dbArray = [{ name: '', y: 0.00 }];
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                dbArray = data.data;
                _this.dashboard.generatePieChart('billtotal', dbArray);
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    FrmMerchantDashBoardComponent.prototype.getDashBoardGeneralCNPTopup = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service002/getGeneralDashboard?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
        this._obj.feature = 2;
        var json = this._obj;
        var dbArray = [{ name: '', y: 0.00 }];
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                dbArray = data.data;
                _this.dashboard.generatePieChart('topuptotal', dbArray);
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    FrmMerchantDashBoardComponent.prototype.getDashBoardBarMonthlyCountBill = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service002/getBarMonthlyTxnCount?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
        this._obj.feature = 1;
        var json = this._obj;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                _this.dashboard.generateBarChart('billmlycount', data.detaildata, data.month, 'Y Title');
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    FrmMerchantDashBoardComponent.prototype.getDashBoardBarMonthlyAmountBill = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service002/getBarMonthlyTxnAmount?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
        this._obj.feature = 1;
        var json = this._obj;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                _this.dashboard.generateBarChart('billmlyamount', data.detaildata, data.month, 'Y Title');
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    FrmMerchantDashBoardComponent.prototype.getDashBoardBarMonthlyCountTopup = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service002/getBarMonthlyTxnCount?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
        this._obj.feature = 2;
        var json = this._obj;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                _this.dashboard.generateBarChart('topupmlycount', data.detaildata, data.month, 'Y Title');
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    FrmMerchantDashBoardComponent.prototype.getDashBoardBarMonthlyAmountTopup = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service002/getBarMonthlyTxnAmount?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
        this._obj.feature = 2;
        var json = this._obj;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                _this.dashboard.generateBarChart('topupmlyamount', data.detaildata, data.month, 'Y Title');
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    FrmMerchantDashBoardComponent.prototype.getBarTxnStatusofCurrentMonthBill = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service002/getBarTxnStatusofCurrentMonth?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
        this._obj.feature = 1;
        var json = this._obj;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                _this.dashboard.generateBarChart('billstatus', data.detaildata, data.month, 'Y Title');
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    FrmMerchantDashBoardComponent.prototype.getBarTxnStatusofCurrentMonthTopup = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service002/getBarTxnStatusofCurrentMonth?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
        this._obj.feature = 2;
        var json = this._obj;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                _this.dashboard.generateBarChart('topupstatus', data.detaildata, data.month, 'Y Title');
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    FrmMerchantDashBoardComponent.prototype.getDashBoardGeneralSky = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service002/getGeneralDashboard?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
        var json = this._obj;
        var dbArray = [{ name: '', y: 0.00 }];
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                dbArray = data.data;
                _this.dashboard.generatePieChart('skytotal', dbArray);
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    FrmMerchantDashBoardComponent.prototype.getDashBoardBarMonthlyCount = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service002/getBarMonthlyTxnCount?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
        var json = this._obj;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                _this.dashboard.generateBarChart('skymlycount', data.detaildata, data.month, 'Y Title');
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    FrmMerchantDashBoardComponent.prototype.getDashBoardBarMonthlyAmount = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service002/getBarMonthlyTxnAmount?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
        var json = this._obj;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                _this.dashboard.generateBarChart('skymlyamount', data.detaildata, data.month, 'Y Title');
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    FrmMerchantDashBoardComponent.prototype.getBarTxnStatusofCurrentMonth = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service002/getBarTxnStatusofCurrentMonth?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
        var json = this._obj;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                _this.dashboard.generateBarChart('skystatus', data.detaildata, data.month, 'Y Title');
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    FrmMerchantDashBoardComponent.prototype.getDashBoardBarMonthlyskynetCount = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service002/getBarMonthlyTxnCount?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
        var json = this._obj;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                _this.dashboard.generateBarChart('skynetmlycount', data.detaildata, data.month, 'Y Title');
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    FrmMerchantDashBoardComponent.prototype.getDashBoardBarMonthlyskynetAmount = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service002/getBarMonthlyTxnAmount?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
        var json = this._obj;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                _this.dashboard.generateBarChart('skynetmlyamount', data.detaildata, data.month, 'Y Title');
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    FrmMerchantDashBoardComponent.prototype.getBarTxnStatusofskynetCurrentMonth = function () {
        var _this = this;
        var url = this.ics._apiurl + 'service002/getBarTxnStatusofCurrentMonth?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
        var json = this._obj;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                _this.dashboard.generateBarChart('skynetstatus', data.detaildata, data.month, 'Y Title');
            }
        }, function (error) {
            _this.showMessage("Can't Get Data!!", undefined);
        }, function () { });
    };
    FrmMerchantDashBoardComponent.prototype.showMessage = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    FrmMerchantDashBoardComponent = __decorate([
        core_1.Component({
            selector: 'frmmerchantdashboard',
            providers: [dashboard_service_1.DashboardService],
            template: "\n  <div class=\"container\">\n  <div class=\"row clearfix\"> \n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n    <form>\n    <legend style=\"margin-left:10px;\">Merchant Transaction DashBoard</legend>\n    <div class=\"row  col-md-12\">  \n\t\t  <label class=\"col-md-2\">Choose Merchant&nbsp; <font class=\"mandatoryfont\">*</font></label>\n      <div class=\"col-md-4\" > \n        <select [(ngModel)]=\"_obj.aMerchantID\" [ngModelOptions]=\"{standalone: true}\" (change)=\"changeMerchant($event)\"  class=\"form-control col-md-0\" required>\n          <option *ngFor=\"let item of ref._lov3.ref015\" value=\"{{item.value}}\" >{{item.caption}}</option>\n        </select> \n      </div> \n      </div>\n      </form>     \n  </div>\n  \n</div>\n  <div class=\"row col-md-12\">&nbsp;</div>\n\n   <div class=\"row col-xs-12 col-sm-12 col-md-12 col-lg-12\" *ngIf = \"_obj.processingCode == '050200' \" [hidden]=_hideCNP>\n   <div>\n        <div class=\"col-md-6\"><div class=\"dbox\">\n          <div class=\"title\"> Bill Payment Transaction  </div>\n          <div class=\"body\" id=\"billtotal\"></div>\n        </div></div>\n        <div class=\"col-md-6\"><div class=\"dbox\">\n          <div class=\"title\"> Topup Transaction  </div>\n          <div class=\"body\" id=\"topuptotal\"></div>\n        </div></div>\n    </div>\n    <div>\n        <div class=\"col-md-6\"><div class=\"dbox\">\n          <div class=\"title\"> Bill Payment Monthly Transaction Count</div>\n          <div class=\"body\" id=\"billmlycount\"></div>\n        </div></div>\n        <div class=\"col-md-6\"><div class=\"dbox\">\n          <div class=\"title\"> Bill Payment Monthly Transaction Amount</div>\n          <div class=\"body\" id=\"billmlyamount\"></div>\n        </div></div>\n      </div>\n      <div><!--start -->\n      <div class=\"col-md-6\"><div class=\"dbox\">\n        <div class=\"title\">Bill Payment Transaction Count By Status</div>\n        <div class=\"body\" id=\"billstatus\"></div>\n      </div></div>\n      <div class=\"col-md-6\"><div class=\"dbox\">\n        <div class=\"title\">Topup Monthly Transaction Count </div>\n        <div class=\"body\" id=\"topupmlycount\"></div>\n      </div></div>\n    </div>\n     <div>\n      <div class=\"col-md-6\"><div class=\"dbox\">\n        <div class=\"title\">Topup Monthly Transaction Amount</div>\n        <div class=\"body\" id=\"topupmlyamount\"></div>\n      </div></div>\n      <div class=\"col-md-6\"><div class=\"dbox\">\n        <div class=\"title\">Topup Transaction Count By Status</div>\n        <div class=\"body\" id=\"topupstatus\"></div>\n      </div></div>\n    </div> \n    </div>\n\n    <div class=\"row col-xs-12 col-sm-12 col-md-12 col-lg-12\" *ngIf = \"_obj.processingCode == '040300' \" [hidden]=_hideSkypay>\n      <div>\n        <div class=\"col-md-6\"><div class=\"dbox\">\n          <div class=\"title\">Topup Transaction  </div>\n          <div class=\"body\" id=\"skytotal\"></div>\n        </div></div>\n        <div class=\"col-md-6\"><div class=\"dbox\">\n          <div class=\"title\">Topup Monthly Transaction Count</div>\n          <div class=\"body\" id=\"skymlycount\"></div>\n        </div></div>\n      </div>\n      <div>\n        <div class=\"col-md-6\"><div class=\"dbox\">\n          <div class=\"title\">Topup Monthly Transaction Amount </div>\n          <div class=\"body\" id=\"skymlyamount\"></div>\n        </div></div>\n        <div class=\"col-md-6\"><div class=\"dbox\">\n          <div class=\"title\">Topup Transaction Count By Status </div>\n          <div class=\"body\" id=\"skystatus\"></div>\n        </div></div>\n      </div>\n    </div>\n\n    <div class=\"row col-xs-12 col-sm-12 col-md-12 col-lg-12\" *ngIf = \"_obj.processingCode == '080400' \"[hidden]=_hideSkynet>\n      <div>\n       <!-- <div class=\"col-md-6\"><div class=\"dbox\">\n          <div class=\"title\">Topup Transaction  </div>\n          <div class=\"body\" id=\"skytotal\"></div>\n        </div></div> -->\n        <div class=\"col-md-6\"><div class=\"dbox\">\n          <div class=\"title\">Topup Monthly Transaction Count</div>\n          <div class=\"body\" id=\"skynetmlycount\"></div>\n        </div></div>\n      </div>\n      <div>\n        <div class=\"col-md-6\"><div class=\"dbox\">\n          <div class=\"title\">Topup Monthly Transaction Amount </div>\n          <div class=\"body\" id=\"skynetmlyamount\"></div>\n        </div></div>\n        <div class=\"col-md-6\"><div class=\"dbox\">\n          <div class=\"title\">Topup Transaction Count By Status </div>\n          <div class=\"body\" id=\"skynetstatus\"></div>\n        </div></div>\n\n      </div>\n    </div>\n    \n</div>\n"
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, dashboard_service_1.DashboardService, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmMerchantDashBoardComponent);
    return FrmMerchantDashBoardComponent;
}());
exports.FrmMerchantDashBoardComponent = FrmMerchantDashBoardComponent;
//# sourceMappingURL=frmmerchantdashboard.component.js.map