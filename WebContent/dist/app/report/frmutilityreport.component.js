"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var Rx_1 = require('rxjs/Rx');
core_2.enableProdMode();
var FrmUtilityReportComponent = (function () {
    function FrmUtilityReportComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this.id = "";
        this._mflag = false;
        this._obj = this.getDefaultObj();
        this._totalcount = 1;
        this._util = new rp_client_util_1.ClientUtil();
        this.myDatePickerOptions = this._util.getDatePicker();
        this._dates = { "dfromDate": this.dateobj, "dtoDate": this.dateobj, "lfromDate": this.dateobj, "ltoDate": this.dateobj };
        this.today = this._util.getTodayDate();
        this._sessionMsg = "";
        this._key = "";
        this._entryhide = true;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "" };
        this._pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 1 };
        this._excelobj = {
            "arr": [{
                    "lnNo": "", "billType": "", "scheduleName": "", "meterID": "", "survey": "", "billNo": "", "lastUnit": "", "thisUnit": "", "totalUnit": "", "rate": "", "conservationFee": "",
                    "amount": "", "lastDate": "", "status": "", "remark": "", "errdesc": "", "msgCode": "", "msgDesc": "", "createddate": "",
                }], "searchText": "", "pageSize": 10, "currentPage": 1, "totalCount": 0
        };
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = true;
            this._dates.dfromDate = this._util.changestringtodateobject(this.today);
            this._dates.dtoDate = this._util.changestringtodateobject(this.today);
            this._dates.lfromDate = this._util.changestringtodateobject(this.today);
            this._dates.ltoDate = this._util.changestringtodateobject(this.today);
            this._entryhide = true;
        }
    }
    FrmUtilityReportComponent.prototype.getDefaultObj = function () {
        return {
            "sessionID": "", "msgCode": "", "msgDesc": "",
            "aDelFromDate": "", "aDelToDate": "", "aLastFromDate": "", "aLastToDate": "", "select": "EXCEL", "totalCount": 0, "currentPage": 1, "pageSize": 10, "msgstatus": "",
            "aToMeterID": "", "aFromMeterID": "", "status": "ALL"
        };
    };
    FrmUtilityReportComponent.prototype.changeSelectVal = function (options) {
        this.selectedOptions = Array.apply(null, options) // convert to real array
            .filter(function (option) { return option.selected; })
            .map(function (option) { return option.value; });
        this._obj.select = this.selectedOptions[0];
    };
    /* changeSelectValForStatus(options) {
      this.selectedOptions = Array.apply(null, options)  // convert to real array
        .filter(option => option.selected)
        .map(option => option.value);
      this._obj.status = this.selectedOptions[0];
    } */
    FrmUtilityReportComponent.prototype.goClear = function () {
        this._entryhide = true;
        this._obj = this.getDefaultObj();
        this._dates.dfromDate = this._util.changestringtodateobject(this.today);
        this._dates.dtoDate = this._util.changestringtodateobject(this.today);
        this._dates.lfromDate = this._util.changestringtodateobject(this.today);
        this._dates.ltoDate = this._util.changestringtodateobject(this.today);
    };
    FrmUtilityReportComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this._router.navigate(['Login', , { p1: '*' }]);
            jQuery("#sessionalert").modal('hide');
        });
    };
    FrmUtilityReportComponent.prototype.changedPager = function (event) {
        this._pgobj = event;
        this._obj.currentPage = this._pgobj.current;
        this._obj.pageSize = this._pgobj.size;
        this._obj.totalCount = this._pgobj.totalcount;
        if (this._pgobj.totalcount > 1) {
            this.goList();
        }
    };
    FrmUtilityReportComponent.prototype.date = function () {
        this._obj.aDelFromDate = this._util.getDatePickerDate(this._dates.dfromDate);
        this._obj.aDelToDate = this._util.getDatePickerDate(this._dates.dtoDate);
        this._obj.aLastFromDate = this._util.getDatePickerDate(this._dates.lfromDate);
        this._obj.aLastToDate = this._util.getDatePickerDate(this._dates.ltoDate);
    };
    FrmUtilityReportComponent.prototype.Validation = function () {
        var _this = this;
        this.date();
        try {
            var url = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
            this.http.doGet(url).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this._sessionMsg = data.msgDesc;
                    _this.showMessage();
                }
                else {
                    if (_this._obj.aDelFromDate.length == 0) {
                        _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Delivery From Date should not blank!" });
                    }
                    else if (_this._obj.aDelToDate.length == 0) {
                        _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Delivery To Date should not blank!" });
                    }
                    else if (_this._obj.aDelFromDate > _this._obj.aDelToDate) {
                        _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Delivery From Date must not exceed Delivery ToDate!" });
                        _this.goClear();
                    }
                    if (_this._obj.aLastFromDate.length == 0) {
                        _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Last From Date should not blank!" });
                    }
                    else if (_this._obj.aLastToDate.length == 0) {
                        _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Last To Date should not blank!" });
                    }
                    else if (_this._obj.aLastFromDate > _this._obj.aLastToDate) {
                        _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Last From Date must not exceed Last ToDate!" });
                        _this.goClear();
                    }
                    else if (!/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/.test(_this._obj.aDelFromDate)) {
                        _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Invalid Delivery From Date!" });
                    }
                    else if (!/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/.test(_this._obj.aDelToDate)) {
                        _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Invalid Delivery To Date!" });
                    }
                    else if (!/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/.test(_this._obj.aLastFromDate)) {
                        _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Invalid Last From Date!" });
                    }
                    else if (!/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/.test(_this._obj.aLastToDate)) {
                        _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Invalid Last To Date!" });
                    }
                    else {
                        _this.goDownload();
                    }
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmUtilityReportComponent.prototype.goDownload = function () {
        this.date();
        var templateData = "";
        var formatJSP = "";
        formatJSP = "utility.jsp";
        this.ics.sendBean({ "t1": "rp-msg", t2: "Information ", t3: "Please wait ..." });
        var url = this.ics._rpturl + formatJSP + "?aDelFromDate=" + this._obj.aDelFromDate + "&" + "aDelToDate=" + this._obj.aDelToDate + "&" + "aLastFromDate=" + this._obj.aLastFromDate + "&" + "aLastToDate=" + this._obj.aLastToDate + "&aFromMeterID=" + this._obj.aFromMeterID + "&aToMeterID=" + this._obj.aToMeterID + "&" + "aSelect=" + this._obj.select + "&aStatus=" + this._obj.status;
        this.ics.sendBean({ "t1": "rp-popup", "t2": "Information", "t3": url });
    };
    FrmUtilityReportComponent.prototype.goList = function () {
        var _this = this;
        this._mflag = false;
        this.date();
        this._obj.sessionID = this.ics._profile.sessionID;
        var url = this.ics._apiurl + 'service001/getUtilityReportList';
        var capValue = "";
        var json = this._obj;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data.msgCode == '0016') {
                _this._sessionMsg = data.msgDesc;
                _this._returnResult.msgDesc = "";
                _this.showMessage();
            }
            else {
                _this._entryhide = false;
                _this._totalcount = data.totalCount;
                _this._obj.currentPage = data.currentPage;
                _this._obj.pageSize = data.pageSize;
                _this._obj.totalCount = data.totalCount;
                if (_this._obj.aDelFromDate > _this._obj.aDelToDate) {
                    _this._obj.msgstatus = "Deliver From Date must not exceed To Date!";
                    _this.ics.sendBean({ "t1": "rp-msg", t2: "Information", t3: _this._obj.msgstatus });
                    _this.goClear();
                }
                else if (_this._obj.aLastFromDate > _this._obj.aLastToDate) {
                    _this._obj.msgstatus = "Last From Date must not exceed To Date!";
                    _this.ics.sendBean({ "t1": "rp-msg", t2: "Information", t3: _this._obj.msgstatus });
                    _this.goClear();
                }
                else {
                    if (_this._obj.totalCount == 0) {
                        _this._obj.msgstatus = "Data not Found!";
                        _this._entryhide = true;
                        _this.ics.sendBean({ "t1": "rp-msg", t2: "Information", t3: _this._obj.msgstatus });
                    }
                }
                if (data != null) {
                    // this._list = data;
                    _this._excelobj = data;
                    if (data.data != null) {
                        if (!(data.data instanceof Array)) {
                            var m = [];
                            m[0] = data.data;
                            data.data = m;
                        }
                        // this._list.data = data.data;
                        _this._excelobj.arr = data.arr;
                    }
                    if (data.arr != null) {
                        if (!(data.arr instanceof Array)) {
                            var m = [];
                            m[0] = data.arr;
                            data.arr = m;
                        }
                    }
                }
            }
            _this._mflag = true;
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
            else {
            }
        }, function () { });
    };
    FrmUtilityReportComponent.prototype.checkdateformat = function (date) {
        if (!/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/.test(date)) {
            this._returnResult.state = 'false';
            this._returnResult.msgDesc = "Invalid Date!";
        }
    };
    FrmUtilityReportComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
            this.http.doGet(url).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this._sessionMsg = data.msgDesc;
                    _this.showMessage();
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmUtilityReportComponent = __decorate([
        core_1.Component({
            selector: 'frmutilityreport',
            template: "\n  <div class=\"container\">\n     <div class=\"row clearfix\">\n        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n          <form class= \"form-horizontal\" (ngSubmit)=\"goList()\"> \n         <!-- Form Name -->\n          <legend>Utility Payment Report</legend>\n\n          <div class=\"row  col-md-12\">  \n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"goClear()\" >Clear</button> \n            <button class=\"btn btn-primary\" >List</button>      \n            <button class=\"btn btn-primary\" type=\"button\" (click)=\" Validation();\">Download</button>          \n         </div>\n\n         <div class=\"row col-md-12\">&nbsp;</div>\n         <div class=\"row col-md-12\" id=\"custom-form-alignment-margin\"><!--Start column -->\n           <div class=\"col-md-6\"> \n              <div class=\"form-group\">\n                  <label class=\"col-md-4\" >Delivery From Date <font class=\"mandatoryfont\" >*</font></label>\n                      <div class=\"col-md-8\">\n                          <my-date-picker name=\"mydate\" [options]=\"myDatePickerOptions\" [(ngModel)]=\"_dates.dfromDate\" ngDefaultControl></my-date-picker>\n                      </div>\n              </div>\n\n              <div class=\"form-group\">\n              <label class=\"col-md-4\" >Last From Date <font class=\"mandatoryfont\" >*</font></label>\n                  <div class=\"col-md-8\">\n                      <my-date-picker name=\"mydate\" [options]=\"myDatePickerOptions\" [(ngModel)]=\"_dates.lfromDate\" ngDefaultControl></my-date-picker>\n                  </div>\n              </div>\n\n              <div class=\"form-group\">\n              <label class=\"col-md-4\" > From MeterID </label>\n                  <div class=\"col-md-8\">\n                      <input type=\"text\"  class=\"form-control\" [(ngModel)]=\"_obj.aFromMeterID\" [ngModelOptions]=\"{standalone: true}\"/>\n                  </div>\n             </div>\n\n              <div class=\"form-group\">\n                <label class=\"col-md-4\" > Status <font class=\"mandatoryfont\"> * </font> </label>\n                <div class=\"col-md-8\">\n                  <select [(ngModel)]=_obj.status [ngModelOptions]=\"{standalone: true}\" required=\"true\" class=\"form-control col-md-0\"  >\n                    <option *ngFor=\"let item of ref._lov1.refstatus\" value=\"{{item.value}}\" >{{item.caption}}</option> \n                  </select>                     \n                </div>\n               </div>\n\n              <div class=\"form-group\">\n                <label class=\"col-md-4\"></label>\n                <div class=\"col-md-8\">\n                  <select [(ngModel)]=_obj.select [ngModelOptions]=\"{standalone: true}\"  (change)=\"changeSelectVal($event.target.options)\" required=\"true\"   class=\"form-control col-md-0\"  >\n                    <option *ngFor=\"let item of ref._lov1.ref010\" value=\"{{item.value}}\" >{{item.caption}}</option> \n                  </select>                     \n                </div>\n              </div>           \n        </div>\n\n        <div class=\"col-md-6\">\n          <div class=\"form-group\">\n            <label class=\"col-md-4\"> Delivery To Date  <font class=\"mandatoryfont\" >*</font> </label>\n            <div class=\"col-md-8\">\n                <my-date-picker name=\"mydate\" [options]=\"myDatePickerOptions\" [(ngModel)]=\"_dates.dtoDate\" ngDefaultControl></my-date-picker>\n            </div>\n          </div>\n\n          <div class=\"form-group\">\n          <label class=\"col-md-4\"> Last To Date  <font class=\"mandatoryfont\" >*</font> </label>\n          <div class=\"col-md-8\">\n              <my-date-picker name=\"mydate\" [options]=\"myDatePickerOptions\" [(ngModel)]=\"_dates.ltoDate\" ngDefaultControl></my-date-picker>\n          </div>\n        </div>\n\n        <div class=\"form-group\">\n        <label class=\"col-md-4\" > To MeterID </label>\n          <div class=\"col-md-8\">\n          <input type=\"text\"  class=\"form-control\" [(ngModel)]=\"_obj.aToMeterID\" [ngModelOptions]=\"{standalone: true}\" />\n        </div>\n       </div>\n        </div>\n        </div>\n       </form>\n      </div>\n    </div>\n </div>\n\n  <div [hidden]=\"_entryhide\">\n    <div class=\"form-group\">\n      <legend></legend>    \n    </div> \n\n    <div class=\"form-group\">\n      <div style = \"margin-top : 10px\">\n        <adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"_totalcount\" (rpChanged)=\"changedPager($event)\" ></adminpager> \n      </div>\n    </div>\n\n    <table class=\"table table-striped table-condensed table-hover tblborder\" style=\"font-size:14px;\">\n    <thead>\n      <tr > \n        <th> Delivery Date </th>\n        <th> Meter ID</th>\n        <th> Building No. </th>\n        <th> Bill No</th>\n        <th> Amount </th>\n        <th> Entry Date </th>\n        <th> LastDate </th>\n        <th> Status </th>\n      </tr>\n    </thead>\n    <tbody>\n      <tr *ngFor=\"let obj of _excelobj.arr,let i=index\">\n        <td>{{obj.createddate}}</td>\n        <td>{{obj.meterID}}</td>\n        <td>{{obj.scheduleName}}</td> \n        <td>{{obj.billNo}}</td>\n        <td>{{obj.amount}}</td>\n        <td>{{obj.lastDate}}</td>\n        <td>{{obj.lastDate}}</td>\n        <td *ngIf = \"obj.status == 0\">UnPaid</td>\n        <td *ngIf = \"obj.status != 0\">Paid</td>      \n      </tr>  \n  </tbody>\n  </table>\n  \n</div>\n<div [hidden] = \"_mflag\">\n  <div class=\"modal\" id=\"loader\"></div>\n</div>\n "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmUtilityReportComponent);
    return FrmUtilityReportComponent;
}());
exports.FrmUtilityReportComponent = FrmUtilityReportComponent;
//# sourceMappingURL=frmutilityreport.component.js.map