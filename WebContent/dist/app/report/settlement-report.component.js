"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var rp_client_util_1 = require('../util/rp-client.util');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var router_1 = require('@angular/router');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
core_1.enableProdMode();
var settlementReport = (function () {
    function settlementReport(el, renderer, ics, _router, route, http, ref) {
        this.el = el;
        this.renderer = renderer;
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._util = new rp_client_util_1.ClientUtil();
        this.myDatePickerOptions = this._util.getDatePicker();
        this._dates = { "fromDate": this.dateobj, "toDate": this.dateobj };
        this.today = this._util.getTodayDate();
        this._obj = { "sessionID": "", "userID": "", "fileType": "EXCEL", "fromDate": "", };
        this._mflag = true;
        this._list = { "wldata": [] };
        this.tableflag = false;
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        this.setTodayDateObj();
        this.setDateObjIntoString();
    }
    settlementReport.prototype.setTodayDateObj = function () {
        this._dates.fromDate = this._util.changestringtodateobject(this.today);
    };
    settlementReport.prototype.setDateObjIntoString = function () {
        this._obj.fromDate = this._util.getDatePickerDateymd(this._dates.fromDate);
    };
    settlementReport.prototype.goShow = function () {
        var _this = this;
        this._mflag = false;
        this.setDateObjIntoString();
        try {
            var url = this.ics.cmsurl + 'ServiceReportAdm/getSettlementReport?download=';
            var json = this._obj;
            this._obj.sessionID = this.ics._profile.sessionID;
            this._obj.userID = this.ics._profile.userID;
            this.http.doPost(url, json).subscribe(function (data) {
                _this._mflag = true;
                _this._list = data;
                if (data.wldata.length != 0) {
                    if (!(data.wldata instanceof Array)) {
                        var m = [];
                        m[0] = data.wldata;
                        data.wldata = m;
                    }
                    _this._list.wldata = data.wldata;
                    _this.tableflag = true;
                }
                else {
                    _this.showMsg("No Record Found", false);
                    _this.tableflag = false;
                }
            }, function (error) {
                _this.tableflag = true;
                _this._mflag = true;
                _this.showMsg("HTTP Error Type ", false);
            }, function () {
                _this._mflag = true;
            });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    settlementReport.prototype.goPrint = function () {
        var _this = this;
        this._mflag = false;
        this.setDateObjIntoString();
        try {
            var url = this.ics.cmsurl + 'ServiceReportAdm/getSettlementReport?download=1';
            var json = this._obj;
            this._obj.sessionID = this.ics._profile.sessionID;
            this._obj.userID = this.ics._profile.userID;
            this.http.doPost(url, json).subscribe(function (data) {
                _this._mflag = true;
                if (data.msgCode = "0016") {
                    _this.showMsg(data.msgDesc, false);
                }
                if (data.state) {
                    var url2 = _this.ics.cmsurl + 'ServiceReportAdm/downloadReport?fileType=' + _this._obj.fileType +
                        "&fileName=" + data.stringResult[0] + '&sessionID=' + _this.ics._profile.sessionID + '&userID=' + _this.ics._profile.userID;
                    if (_this._obj.fileType == "PDF") {
                        _this._mflag = true;
                        jQuery("#downloadPdf").html("<iframe src=" + url2 + "></iframe>");
                        _this.showMsg("Download Successfully", true);
                    }
                    if (_this._obj.fileType == "EXCEL") {
                        _this._mflag = true;
                        jQuery("#downloadExcel").html("<iframe src=" + url2 + "></iframe>");
                        _this.showMsg("Download Successfully", true);
                    }
                }
            }, function (error) {
                _this._mflag = true;
                _this.showMsg("HTTP Error Type ", false);
            }, function () {
                _this._mflag = true;
            });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    settlementReport.prototype.goProcess = function () {
        var _this = this;
        this._mflag = false;
        this.setDateObjIntoString();
        try {
            var url = this.ics.cmsurl + 'ServiceReportAdm/gopostTransaction';
            var json = this._obj;
            this._obj.sessionID = this.ics._profile.sessionID;
            this._obj.userID = this.ics._profile.userID;
            this.http.doPost(url, json).subscribe(function (data) {
                _this._mflag = true;
                if (data.msgCode == "0000") {
                    _this._list = data;
                    if (data.wldata != null) {
                        if (!(data.wldata instanceof Array)) {
                            var m = [];
                            m[0] = data.wldata;
                            data.wldata = m;
                        }
                        _this._list.wldata = data.wldata;
                        _this.tableflag = true;
                        if (data.state == true) {
                            _this.showMsg(data.msgDesc, true);
                        }
                        else {
                            _this.showMsg(data.msgDesc, false);
                        }
                    }
                }
                else if (data.msgCode == "0016") {
                    _this.showMsg(data.msgDesc, false);
                }
            }, function (error) {
                _this._mflag = true;
                _this.showMsg("HTTP Error Type ", false);
            }, function () {
                _this._mflag = true;
            });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    /* changeFileType(options){
        let value = options[options.selectedIndex].value;
        for(var i=0; i< this.ref._lov1.ref010.length; i++){
          if(this.ref._lov1.ref010[i].value = value)
          this._obj.fileType = value;
        }
      } */
    settlementReport.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    settlementReport.prototype.goClear = function () {
        this._list = { "wldata": [] };
        this.tableflag = false;
    };
    settlementReport = __decorate([
        core_1.Component({
            selector: 'wallettopup-report',
            template: " \n  <div *ngIf=\"!_pdfdata\">  \n  <div class=\"container-fluid\">\n  <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n\t\t<form class=\"form-horizontal\" ngNoForm>\n\t\t\t<legend>Settlement Report</legend>\n\t\t\t<div class=\"cardview list-height\"> \n\t\t\t\t<div class=\"row col-md-12\">\n          <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goClear()\">Clear</button>\n          <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goShow()\">Show</button>              \n          <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goProcess()\">Process</button> \n\t\t\t\t\t<button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goPrint()\">Export</button>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"row col-md-12\">&nbsp;</div>\n\t\t\t\t  <div class=\"form-group\">\n\t\t\t\t\t<div class=\" col-md-12\">\n\t\t\t\t\t\t<div class=\"form-group\">            \n\t\t\t\t\t\t\t<label class=\"col-md-2\">From Date</label>\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t<div class=\"col-md-3\">\n\t\t\t\t\t\t\t\t<my-date-picker name=\"mydate\"  [options]=\"myDatePickerOptions\" [(ngModel)]=\"_dates.fromDate\" ngDefaultControl></my-date-picker>                  \n\t\t\t\t\t\t\t</div> \n\t\t\t\t\t\t</div>\t\t\t\t\n\t\t\t\t\t\t<!--<div class=\"form-group\">\n\t\t\t\t\t\t<rp-input  [(rpModel)]=\"_obj.fileType\" rpRequired =\"true\" rpLabelClass = \"col-md-2\" rpClass=\"col-md-3\" rpType=\"ref010\" rpLabel=\"File Type\" ></rp-input>\n\t\t\t\t\t\t</div>  -->\t\t\t\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\" col-md-12\" style=\"overflow-x:auto\">       \n              <table  class=\"table table-striped table-condense table-hover tblborder\" id=\"tblstyle\" *ngIf=\"tableflag\">\n              <thead>\n                <tr>\n                <th style=\"background-color: #e6f2ff; color: #333333\"><div style=\"width:50\">No. </div></th>\n                <th style=\"background-color: #e6f2ff; color: #333333\"><div style=\"width:150\">From Account </div></th>\n                <th style=\"background-color: #e6f2ff; color: #333333\"><div style=\"width:150\">To Account</div></th>\n                <th style=\"background-color: #e6f2ff; color: #333333\"><div style=\"width:100\">Date </div></th>\n                <th style=\"background-color: #e6f2ff; color: #333333\"><div style=\"width:150\">Merchant Name</div></th>  \n                <th style=\"background-color: #e6f2ff; color: #333333\"><div style=\"width:100;float:right\">Amount</div></th>\n                <th style=\"background-color: #e6f2ff; color: #333333\"><div style=\"widtsh:150;float:right\">Commission Amount</div></th>  \n                </tr>\n              </thead>\n                <tbody>\n                <tr *ngFor=\"let obj of _list.wldata; let i= index\"> \n                  <td>{{i+1}}</td>\n                  <td>{{obj.drAccNumber}}</td>\n                  <td> {{obj.crAccNumber}}</td> \n                  <td class=\"uni\"> {{obj.settlementDate}} </td>\n                  <td class=\"uni\"> {{obj.merchantName}} </td>    \n                  <td class=\"uni\" style=\"float:right\"> {{obj.amount}}</td>\n                  <td class=\"uni\" class=\"right\"> {{obj.commissionCharges}} </td> \n                </tr> \n                </tbody>                     \n              </table>\n          </div>\n\t\t\t\t</div>\t\t\t\t\n\t\t\t</div>\n        </form> \n    </div>\n        </div>\n        <div id=\"downloadExcel\" style=\"display: none;width: 0px;height: 0px;\"></div>       \t\t\t\t\n        <div id=\"downloadPdf\" style=\"display: none;width: 0px;height: 0px;\"></div>       \t\t\t\t\n\t</div>\n</div>    \n\t\n\t<div [hidden] = \"_mflag\">\n\t  <div class=\"modal\" id=\"loader\"></div>\n\t</div>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n  ",
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef, core_1.Renderer, rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], settlementReport);
    return settlementReport;
}());
exports.settlementReport = settlementReport;
//# sourceMappingURL=settlement-report.component.js.map