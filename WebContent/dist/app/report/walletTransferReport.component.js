"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require("@angular/platform-browser");
var router_1 = require('@angular/router');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
core_1.enableProdMode();
var walletTransferReport = (function () {
    function walletTransferReport(el, renderer, ics, _router, route, http, ref, sanitizer) {
        this.el = el;
        this.renderer = renderer;
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this.sanitizer = sanitizer;
        this._util = new rp_client_util_1.ClientUtil();
        this.today = this._util.getTodayDate();
        this.myDatePickerOptions = this._util.getDatePicker();
        this._dates = { "fromDate": this.dateobj, "toDate": this.dateobj };
        this._mflag = true;
        this._divexport = false;
        this.merchant = [];
        this._sessionObj = this.getSessionObj();
        this._obj = this.getDefaultObj();
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        this.setTodayDateObj();
        this.setDateObjIntoString();
        this.getAllMerchant();
        //this.transfertype();
    }
    walletTransferReport.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    walletTransferReport.prototype.getDefaultObj = function () {
        return {
            "merchantID": "", "sessionID": "", "userID": "", "fromDate": "", "toDate": "", "fromAccount": "", "alldate": true, "t1": "", "t2": "", "transtype": "",
            "toAccount": "", "totalCount": 0, "currentPage": 1, "pageSize": 10, "fileType": "EXCEL"
        };
    };
    walletTransferReport.prototype.setTodayDateObj = function () {
        this._dates.fromDate = this._util.changestringtodateobject(this.today);
        this._dates.toDate = this._util.changestringtodateobject(this.today);
    };
    walletTransferReport.prototype.setDateObjIntoString = function () {
        this._obj.fromDate = this._util.getDatePickerDateymd(this._dates.fromDate);
        this._obj.toDate = this._util.getDatePickerDateymd(this._dates.toDate);
    };
    walletTransferReport.prototype.getAllDate = function (event) {
        this._obj.alldate = event.target.checked;
    };
    walletTransferReport.prototype.goClose = function () {
        this._obj = this.getDefaultObj();
        this.today = this._util.getTodayDate();
        this.setTodayDateObj();
        this._divexport = false;
    };
    walletTransferReport.prototype.goPrint = function () {
        var _this = this;
        this._mflag = false;
        this.setDateObjIntoString();
        if (this._obj.fromDate > this._obj.toDate) {
            this._mflag = true;
            this.showMsg("From Date Should Not Exceed To Date.", false);
        } /* if (!/^(([1-9]|1[0-9])\/[A-Za-z]+\([N|n]\)[0-9]{6})$/.test(this._obj.nrc)) {
          this.showMsg("Invalid NRC No.", false);
        } */
        else {
            try {
                var url = this.ics.cmsurl + 'ServiceReportAdm/getWalletTopupReportJasper';
                var json = this._obj;
                this._obj.sessionID = this.ics._profile.sessionID;
                this._obj.userID = this.ics._profile.userID;
                this.http.doPost(url, json).subscribe(function (data) {
                    _this._mflag = true;
                    if (data.state) {
                        _this._divexport = true;
                        var url2 = _this.ics.cmsurl + 'ServiceReportAdm/printTransactionReport';
                        _this._printUrl = _this.sanitizer.bypassSecurityTrustResourceUrl(url2);
                        _this.showMsg("Download Successfully", true);
                    }
                    else {
                        _this.showMsg("Data Not Found!", false);
                    }
                }, function (error) {
                    _this._mflag = true;
                    _this.showMsg("HTTP Error Type ", false);
                }, function () {
                    _this._mflag = true;
                });
            }
            catch (e) {
                alert("Invalid URL.");
            }
        }
    };
    walletTransferReport.prototype.goClear = function () {
        this._obj = this.getDefaultObj();
        this.setTodayDateObj();
    };
    /*   transfertype() {
        try {
          this._mflag=false;
          let url: string = this.ics.cmsurl + 'serviceAdmLOV/getTransferTypes';
    
          this._sessionObj.sessionID = this.ics._profile.sessionID;
          this._sessionObj.userID = this.ics._profile.userID;
          let json: any = this._sessionObj;
    
          this.http.doPost(url, json).subscribe(
            data => {
              this._mflag = true;
    
              if (data != null) {
                if (data.msgCode == "0016") {
                  this.showMsg(data.msgDesc,false);
                  this.logout();
                }
    
                if (data.msgCode != "0000") {
                  this.showMsg(data.msgDesc,true);
                }
    
                if (data.msgCode == "0000") {
                  if (data.refAccountTransferType != null) {
                    if (!(data.refAccountTransferType instanceof Array)) {
                      let m = [];
                      m[0] = data.refAccountTransferType;
                      this.ref._lov3.refAccountTransferType = m;
                      this._obj.transtype = this.ref._lov3.refAccountTransferType[0].value;
                    } else {
                      this.ref._lov3.refAccountTransferType = data.refAccountTransferType;
                      this._obj.transtype = this.ref._lov3.refAccountTransferType[0].value;
                    }
                  }
                }
              }
              this._mflag=true;
            },
            error => {
              if (error._body.type == "error") {
                alert("Connection Timed Out!");
                this._mflag=true;
              } else {
              }
            },
            () => { }
          );
        } catch (e) {
          alert(e);
          this._mflag=true;
        }
      }  */
    /* changeTransferType(options) {
      let value1 = options[options.selectedIndex].value;
  
      for (var i = 1; i < this.ref._lov3.refAccountTransferType.length; i++) {
        if (this.ref._lov3.refAccountTransferType[i].value == value1) {
          this._obj.transtype = value1;
          break;
        }
      }
    } */
    walletTransferReport.prototype.changeFileType = function (options) {
        var value = options[options.selectedIndex].value;
        for (var i = 0; i < this.ref._lov1.ref010.length; i++) {
            if (this.ref._lov1.ref010[i].value = value)
                this._obj.fileType = value;
        }
    };
    /* changeTransType(event){
      let options = event.target.options;
      let k = options.selectedIndex;//Get Selected Index
      let value = options[options.selectedIndex].value;//Get Selected Index's Value
      for(var i=0; i< this.ref._lov1.ref011.length; i++){
        if(this.ref._lov1.ref011[i].value == value)
        this._obj.transtype = value;
      }
      if(this._obj.transtype != "2"){
          this.ref._lov3.ref015 =[];
      }else{
          this.ref._lov3.ref015 = this.merchant;
      }
    } */
    walletTransferReport.prototype.getAllMerchant = function () {
        var _this = this;
        this.http.doGet(this.ics.cmsurl + 'serviceAdmLOV/getmerchantidlistdetail?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (data) {
            _this.ref._lov3.ref015 = data.ref015;
            if (_this.ref._lov3.ref015 != null && _this.ref._lov3.ref015 != undefined) {
                if (!(_this.ref._lov3.ref015 instanceof Array)) {
                    var m = [];
                    m[1] = _this.ref._lov3.ref015;
                    _this.merchant.push(m[1]);
                }
                _this.ref._lov3.ref015[0].value = "All";
                _this.ref._lov3.ref015[0].caption = "All";
                for (var j = 0; j < _this.ref._lov3.ref015.length; j++) {
                    _this.merchant.push(_this.ref._lov3.ref015[j]);
                }
            }
            /* if(this._obj.transtype != "2"){
                this.ref._lov3.ref015 =[];
            }else{
                this.ref._lov3.ref015 = this.merchant;
            } */
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
        }, function () { });
    };
    walletTransferReport.prototype.changeMerchant = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex; //Get Selected Index
        var value = options[options.selectedIndex].value; //Get Selected Index's Value    
        this._obj.merchantID = value;
        for (var i = 0; i < this.ref._lov3.ref015.length; i++) {
            if (this.ref._lov3.ref015[i].value == value) {
                this._obj.merchantID = this.ref._lov3.ref015[i].value;
                break;
            }
        }
    };
    walletTransferReport.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    walletTransferReport.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    walletTransferReport = __decorate([
        core_1.Component({
            selector: 'wallettopup-report',
            template: " \n  <div *ngIf=\"!_divexport\">  \n  <div class=\"container-fluid\">\n  <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n\t\t<form class=\"form-horizontal\" ngNoForm>\n\t\t\t<legend>Wallet Transfer Report</legend>\n\t\t\t\t<div class=\"cardview list-height\"> \n\t\t\t\t\t<div class=\"row col-md-12\">\n          <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goClear()\">Clear</button>              \n\t\t\t\t  <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goPrint()\">Download</button>\n          </div>\n\n          <div class=\"row col-md-12\">&nbsp;</div>\n          <div class=\"form-group\">\n          <div class=\"col-md-12\">\n\t\t\t\t\t\t\t<div class=\"form-group\">            \n\t\t\t\t\t\t\t\t<label class=\"col-md-2\">From Date</label>\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t<div class=\"col-md-3\">\n\t\t\t\t\t\t\t\t\t<my-date-picker name=\"mydate\"  [options]=\"myDatePickerOptions\" [(ngModel)]=\"_dates.fromDate\" ngDefaultControl></my-date-picker>                  \n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<label class=\"col-md-2\">To Date</label>\n\t\t\t\t\t\t\t\t<div class=\"col-md-3\">\n\t\t\t\t\t\t\t\t\t<my-date-picker name=\"mydate\" [options]=\"myDatePickerOptions\" [(ngModel)]=\"_dates.toDate\" ngDefaultControl></my-date-picker>\n\t\t\t\t\t\t\t\t\t<!-- <input class=\"form-control\" type=\"date\" [(ngModel)]=\"_obj.toDate\" required=\"true\" [ngModelOptions]=\"{standalone: true}\">  -->                            \n\t\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t<div class=\"col-md-2\">\n\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t<input type='checkbox' [(ngModel)]=\"_obj.alldate\" [ngModelOptions]=\"{standalone: true}\" (click)='getAllDate($event)'>\n\t\t\t\t\t\t\t\tALL\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\t\n\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t<label class=\"col-md-2\">From Account</label>\n\t\t\t\t\t\t\t\t<div class=\"col-md-3\">\n\t\t\t\t\t\t\t\t  <input type=\"text\" [(ngModel)]=\"_obj.fromAccount\" class=\"form-control input-sm\" required autofocus/>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<label class=\"col-md-2\">To Account</label>\n\t\t\t\t\t\t\t\t<div class=\"col-md-3\">\n\t\t\t\t\t\t\t\t  <input type=\"text\" [(ngModel)]=\"_obj.toAccount\" class=\"form-control input-sm\" />\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t<label class=\"col-md-2\">From Name</label>\n\t\t\t\t\t\t\t\t<div class=\"col-md-3\">\n\t\t\t\t\t\t\t\t  <input type=\"text\" [(ngModel)]=\"_obj.t1\" class=\"form-control input-sm\" required autofocus/>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<label class=\"col-md-2\">To Name</label>\n\t\t\t\t\t\t\t\t<div class=\"col-md-3\">\n\t\t\t\t\t\t\t\t  <input type=\"text\" [(ngModel)]=\"_obj.t2\" class=\"form-control input-sm\" />\n\t\t\t\t\t\t\t\t</div>\n              </div>\n              <div class=\"form-group\">\n              <!--<label class=\"col-md-2\">Transfer Type</label>\n              <div class=\"col-md-3\">  \n              <select  [(ngModel)]=\"_obj.transtype\" (change)=\"changeTransType($event)\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control input-sm\" required>\n                  <option *ngFor=\"let item of ref._lov1.ref011\" value=\"{{item.value}}\" >{{item.caption}}</option>\n                </select> \n              </div>-->\t              \n              <label class=\"col-md-2\">Merchant Type</label>\n              <div class=\"col-md-3\">  \n              <select  [(ngModel)]=\"_obj.merchantID\" (change)=\"changeMerchant($event)\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control input-sm\" required>\n                  <option *ngFor=\"let item of ref._lov3.ref015\" value=\"{{item.value}}\" >{{item.caption}}</option>\n                </select> \n              </div>\n              <rp-input  [(rpModel)]=\"_obj.fileType\" rpRequired =\"true\" rpLabelClass = \"col-md-2\" rpClass=\"col-md-3\" rpType=\"ref010\" rpLabel=\"File Type\" (ngModelChange)=\"changeFileType($event)\"></rp-input>\n\t\t\t\t\t\t  </div>\t\t\t\t\t\t\t\t\n              </div>\n             <!-- <div class=\"form-group\">\n                <rp-input  [(rpModel)]=\"_obj.fileType\" rpRequired =\"true\" rpLabelClass = \"col-md-2\" rpClass=\"col-md-3\" rpType=\"ref010\" rpLabel=\"File Type\" (ngModelChange)=\"changeFileType($event)\"></rp-input>\n\t\t\t\t\t\t  </div>\n            </div>            \n\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t  <label class=\"col-md-2\">Transfer Type</label>\n\t\t\t\t\t\t\t  <div class=\"col-md-3\">\n\t\t\t\t\t\t\t  <select [(ngModel)]=\"_obj.transtype\" [ngModelOptions]=\"{standalone: true}\"  (change)=\"changeTransferType($event.target.options)\"  required=\"true\"   class=\"form-control input-sm\"  >\n\t\t\t\t\t\t\t\t <option *ngFor=\"let item of ref._lov3.refAccountTransferType\" value=\"{{item.value}}\" >{{item.caption}}</option>\n\t\t\t\t\t\t\t  </select>                     \n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>-->\t\t\t\t\n        </div>\n        </div>\n        </form> \n        </div>\n        </div>                  \t\t\t\t\n\t</div>\n</div>    \n<div *ngIf=\"_divexport\">    \n\t  <button type=\"button\" class=\"close\"  (click)=\"goClose()\">&times;</button>\n\t  <iframe id=\"frame1\" [src]=\"_printUrl\" width=\"100%\" height=\"100%\"></iframe>\n\t</div>   \n\t<div [hidden] = \"_mflag\">\n\t  <div class=\"modal\" id=\"loader\"></div>\n\t</div>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n  ",
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef, core_1.Renderer, rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences, platform_browser_1.DomSanitizer])
    ], walletTransferReport);
    return walletTransferReport;
}());
exports.walletTransferReport = walletTransferReport;
//# sourceMappingURL=walletTransferReport.component.js.map