"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require("@angular/platform-browser");
var router_1 = require('@angular/router');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
core_1.enableProdMode();
var merchantTransferReport = (function () {
    function merchantTransferReport(el, l_util, renderer, ics, _router, route, http, ref, sanitizer) {
        this.el = el;
        this.l_util = l_util;
        this.renderer = renderer;
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this.sanitizer = sanitizer;
        this._util = new rp_client_util_1.ClientUtil();
        this.today = this._util.getTodayDate();
        this.myDatePickerOptions = this._util.getDatePicker();
        this._dates = { "fromDate": this.dateobj, "toDate": this.dateobj };
        this._entryhide = true;
        this._mflag = true;
        this._divexport = false;
        // merchant = [];
        this.total_Amount = "0.00";
        this.total_CommissionCharges = "0.00";
        this._sessionObj = this.getSessionObj();
        //////////////////////////////////////////////////////////////////////////////////////
        this._toggleSearch = true;
        this._SearchString = "";
        this._OperationMode = "";
        this.i = 0;
        this.file_type = "";
        this._obj = this.getDefaultObj();
        this._pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 1 };
        this._list = {
            "data": [{
                    "i1": 0, "n1": "", "n2": 0, "state": "", "t1": "",
                    "t2": "", "t3": "", "t4": "", "t5": "", "t6": "", "t7": "", "t8": "",
                    "t9": ""
                }],
            "totalCount": 0, "currentPage": 1, "pageSize": 10
        };
        this._FilterDataset = {
            filterList: [
                { "itemid": "", "caption": "", "fieldname": "", "datatype": "", "condition": "", "t1": "", "t2": "" }
            ],
            "pageNo": 1, "pageSize": 10, "filterSource": 0, "sessionID": "", "userID": "", "userName": this.ics._profile.userName
        };
        if (!ics.getRole() || ics.getRole() == 0)
            this._router.navigate(['/login']);
        this.setTodayDateObj();
        this.setDateObjIntoString();
        //this.getAllMerchant();
        //this.transfertype();
    }
    merchantTransferReport.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "", "lovDesc": "" };
    };
    merchantTransferReport.prototype.getDefaultObj = function () {
        return {
            "merchantID": "All", "sessionID": "", "userID": "", "fromDate": "", "toDate": "", "fromAccount": "", "alldate": true, "t1": "", "t2": "", "transtype": "",
            "toAccount": "", "totalCount": 0, "currentPage": 1, "pageSize": 10, "fileType": "EXCEL", "condition": "", "simpleSearch": ""
        };
    };
    merchantTransferReport.prototype.ngOnInit = function () {
        //this.filterSearch();
        this.loadAdvancedSearchData();
    };
    merchantTransferReport.prototype.filterSearch = function () {
        if (this._toggleSearch)
            this.filterCommonSearch();
        else
            this.ics.send001("FILTER");
    };
    merchantTransferReport.prototype.filterCommonSearch = function () {
        var l_DateRange;
        var l_SearchString = "";
        this._FilterDataset.filterList = [];
        this._FilterDataset.filterSource = 0;
        if (jQuery.trim(this._SearchString) != "") {
            l_SearchString = jQuery.trim(this._SearchString);
            this._FilterDataset.filterList =
                [
                    { "itemid": "1", "caption": "Search String", "fieldname": "SearchString", "datatype": "simplesearch", "condition": "c", "t1": l_SearchString, "t2": "" }
                ];
        }
        //  l_DateRange = { "itemid": "4", "caption": "Date Range", "fieldname": "DateRange", "datatype": "date", "condition": "eq", "t1": this.l_util.getTodayDate(), "t2": "" };
        //   this._FilterDataset.filterList.push(l_DateRange);
        if (this._OperationMode != "prepareFilter") {
            if (this._FilterDataset.pageNo == 1)
                this.goList();
            else
                this._FilterDataset.pageNo = 1;
        }
    };
    merchantTransferReport.prototype.setTodayDateObj = function () {
        this._dates.fromDate = this._util.changestringtodateobject(this.today);
        this._dates.toDate = this._util.changestringtodateobject(this.today);
    };
    merchantTransferReport.prototype.setDateObjIntoString = function () {
        this._obj.fromDate = this._util.getDatePickerDateymd(this._dates.fromDate);
        this._obj.toDate = this._util.getDatePickerDateymd(this._dates.toDate);
    };
    merchantTransferReport.prototype.getAllDate = function (event) {
        this._entryhide = true;
        this._list.currentPage = 1;
        this._list.totalCount = 0;
        this._obj.alldate = event.target.checked;
    };
    merchantTransferReport.prototype.closeList = function () {
        this.goClear();
    };
    merchantTransferReport.prototype.goClose = function () {
        this._obj = this.getDefaultObj();
        this.today = this._util.getTodayDate();
        this.setTodayDateObj();
        this._divexport = false;
    };
    merchantTransferReport.prototype.btnToggleSearch_onClick = function () {
        this.toggleSearch(!this._toggleSearch);
    };
    merchantTransferReport.prototype.toggleSearch = function (aToggleSearch) {
        // Set Flag
        this._toggleSearch = aToggleSearch;
        // Clear Simple Search
        if (!this._toggleSearch)
            this._SearchString = '';
        // Enable/Disabled Simple Search Textbox
        jQuery('#isInputSearch').prop('disabled', !this._toggleSearch);
        // Hide Advanced Search
        if (this._toggleSearch) {
            this.ics.send001("CLEAR");
            //this._FilterDataset = null;
            this._obj = this.getDefaultObj();
        }
        // Show/Hide Advanced Search    
        //  
        if (this._toggleSearch)
            jQuery("#asAdvancedSearch").css("display", "none"); // true     - Simple Search
        else {
            jQuery("#asAdvancedSearch").css("display", "inline-block"); // false    - Advanced Search
            this._obj = this.getDefaultObj();
        }
        // Set Icon 
        //  
        if (this._toggleSearch)
            jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-up").addClass("glyphicon glyphicon-menu-down"); // true     - Simple Search
        else
            jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-down").addClass("glyphicon glyphicon-menu-up"); // false    - Advanced Search
        // Set Tooltip
        //
        var l_Tooltip = (this._toggleSearch) ? "Expand" : "Collapse";
        jQuery('#btnToggleSearch').attr('title', l_Tooltip);
    };
    merchantTransferReport.prototype.renderAdvancedSearch = function (event) {
        this.toggleSearch(!event);
    };
    merchantTransferReport.prototype.filterAdvancedSearch = function (event) {
        this._FilterDataset.filterSource = 1;
        this._FilterDataset.filterList = [];
        this._FilterDataset.filterList = event;
        if (this._OperationMode != "prepareFilter") {
            if (this._FilterDataset.pageNo == 1)
                this.goList();
            else
                this._FilterDataset.pageNo = 1;
        }
    };
    merchantTransferReport.prototype.goPrint = function () {
        var _this = this;
        var l_Data = [];
        l_Data = this._FilterDataset;
        this._mflag = false;
        this.setDateObjIntoString();
        if (this._obj.fromDate > this._obj.toDate) {
            this._mflag = true;
            this.showMsg("From Date Should Not Exceed To Date.", false);
        } /* if (!/^(([1-9]|1[0-9])\/[A-Za-z]+\([N|n]\)[0-9]{6})$/.test(this._obj.nrc)) {
          this.showMsg("Invalid NRC No.", false);
        } */
        else {
            if (l_Data != null) {
                for (this.i = 0; this.i < l_Data.filterList.length; this.i++) {
                    if (l_Data.filterList[this.i].itemid == "1") {
                        this._obj.simpleSearch = l_Data.filterList[this.i].t1;
                    }
                    else if (l_Data.filterList[this.i].itemid == "2") {
                        this._obj.fromDate = l_Data.filterList[this.i].t1;
                        this._obj.toDate = l_Data.filterList[this.i].t2;
                        this._obj.condition = l_Data.filterList[this.i].condition;
                    }
                    else if (l_Data.filterList[this.i].itemid == "3") {
                        this._obj.merchantID = l_Data.filterList[this.i].t1;
                    }
                    else if (l_Data.filterList[this.i].itemid == "4") {
                        this.file_type = l_Data.filterList[this.i].t1;
                        if (this.file_type == "1") {
                            this._obj.fileType = "EXCEL";
                        }
                        else
                            this._obj.fileType = "PDF";
                    }
                }
            }
            try {
                var url = this.ics.cmsurl + 'ServiceReportAdm/getWalletTopupReportJasperDownload';
                var json = this._obj;
                this._obj.sessionID = this.ics._profile.sessionID;
                this._obj.userID = this.ics._profile.userID;
                this.http.doPost(url, json).subscribe(function (data) {
                    _this._mflag = true;
                    if (data.state) {
                        _this._divexport = true;
                        var url2 = _this.ics.cmsurl + 'ServiceReportAdm/MerchantTransferReport';
                        _this._printUrl = _this.sanitizer.bypassSecurityTrustResourceUrl(url2);
                    }
                    else {
                        _this.showMsg("Data Not Found!", false);
                    }
                }, function (error) {
                    _this._mflag = true;
                    _this.showMsg("HTTP Error Type ", false);
                }, function () {
                    _this._mflag = true;
                });
            }
            catch (e) {
                alert("Invalid URL.");
            }
        }
    };
    merchantTransferReport.prototype.goClear = function () {
        this._obj = this.getDefaultObj();
        this.setTodayDateObj();
        this._entryhide = true;
    };
    merchantTransferReport.prototype.changeFileType = function (options) {
        var value = options[options.selectedIndex].value;
        for (var i = 0; i < this.ref._lov1.ref010.length; i++) {
            if (this.ref._lov1.ref010[i].value = value)
                this._obj.fileType = value;
        }
    };
    /*changeMerchant(event) {
     let options = event.target.options;
     let k = options.selectedIndex;//Get Selected Index
     let value = options[options.selectedIndex].value;//Get Selected Index's Value
     this._obj.merchantID = value;
     for (var i = 0; i < this.merchant.length; i++) {
       if (this.merchant[i].value == value) {
         this._obj.merchantID = this.merchant[i].value;
         break;
       }
  
     }
   }*/
    merchantTransferReport.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    merchantTransferReport.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    merchantTransferReport.prototype.goList = function () {
        var _this = this;
        var l_Data = [];
        l_Data = this._FilterDataset;
        this._mflag = false;
        this.setDateObjIntoString();
        if (this._obj.fromDate > this._obj.toDate) {
            this._mflag = true;
            this.showMsg("From Date Should Not Exceed To Date.", false);
        }
        else {
            if (l_Data != null) {
                for (this.i = 0; this.i < l_Data.filterList.length; this.i++) {
                    if (l_Data.filterList[this.i].itemid == "1") {
                        this._obj.simpleSearch = l_Data.filterList[this.i].t1;
                    }
                    else if (l_Data.filterList[this.i].itemid == "2") {
                        this._obj.fromDate = l_Data.filterList[this.i].t1;
                        this._obj.toDate = l_Data.filterList[this.i].t2;
                        this._obj.condition = l_Data.filterList[this.i].condition;
                    }
                    else if (l_Data.filterList[this.i].itemid == "3") {
                        this._obj.merchantID = l_Data.filterList[this.i].t1;
                    }
                    else if (l_Data.filterList[this.i].itemid == "4") {
                        this.file_type = l_Data.filterList[this.i].t1;
                        if (this.file_type == "1") {
                            this._obj.fileType = "EXCEL";
                        }
                        else
                            this._obj.fileType = "PDF";
                    }
                }
            }
            var url = this.ics.cmsurl + 'ServiceReportAdm/getWalletTopupReportJasper?download=0';
            var json = this._obj;
            this._obj.sessionID = this.ics._profile.sessionID;
            this._obj.userID = this.ics._profile.userID;
            this.http.doPost(url, json).subscribe(function (data) {
                _this._mflag = true;
                _this.total_Amount = data.total_Amount;
                _this.total_CommissionCharges = data.total_CommissionCharges;
                if (data != null) {
                    _this._obj.totalCount = data.totalCount;
                    if (_this._obj.totalCount == 0) {
                        _this.showMsg("Data not Found!", false);
                        _this._entryhide = true;
                    }
                    else {
                        _this._obj.currentPage = data.currentPage;
                        _this._obj.pageSize = data.pageSize;
                        _this._list = data;
                        if (data.merchantdata != null) {
                            if (!(data.merchantdata instanceof Array)) {
                                var m = [];
                                m[0] = data.merchantdata;
                                data.merchantdata = m;
                            }
                            _this._list.data = data.merchantdata;
                            _this._entryhide = false;
                            if (_this._obj.totalCount != 0)
                                _this._entryhide = false;
                        }
                    }
                }
            });
        }
    };
    merchantTransferReport.prototype.changedPager = function (event) {
        this._pgobj = event;
        this._obj.currentPage = this._pgobj.current;
        this._obj.pageSize = this._pgobj.size;
        this._obj.totalCount = this._pgobj.totalcount;
        if (this._pgobj.totalcount > 1) {
            //this._obj.click = 1;
            this.goList();
        }
    };
    merchantTransferReport.prototype.formatNumber = function (amt) {
        if (amt != undefined && amt != "0") {
            return this.thousand_sperator(parseFloat(amt).toFixed(2));
        }
        else {
            return "0.00";
        }
    };
    merchantTransferReport.prototype.thousand_sperator = function (num) {
        if (num != "" && num != undefined && num != null) {
            num = num.replace(/,/g, "");
        }
        var parts = num.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts; // return 100,000.00
    };
    merchantTransferReport.prototype.closePage = function () {
        this._router.navigate(['000', { cmd: "READ", p1: this.ics._profile.userID }]);
    };
    merchantTransferReport.prototype.loadAdvancedSearchData = function () {
        this._TypeList =
            {
                "lovStatus": [],
                "lovFileType": [
                    { "value": "1", "caption": "EXCEL" },
                    { "value": "2", "caption": "PDF" }
                ],
            };
        //this.loadStatus();  
        //this.getAllMerchant();
        this.loadMerchantType();
        this._FilterList =
            [
                { "itemid": "1", "caption": "Search String", "fieldname": "SearchString", "datatype": "simplesearch", "condition": "c", "t1": "", "t2": "", "t3": "" },
                { "itemid": "2", "caption": "Transaction Date", "fieldname": "FromDate", "datatype": "date", "condition": "bt", "t1": this.l_util.getTodayDate(), "t2": this.l_util.getTodayDate(), "t3": "true" },
                { "itemid": "3", "caption": "Merchant Type", "fieldname": "MerchantType", "datatype": "lovStatus", "condition": "", "t1": "", "t2": "", "t3": "" },
            ];
    };
    merchantTransferReport.prototype.loadMerchantType = function () {
        var _this = this;
        try {
            var url = this.ics.cmsurl + 'serviceAdmLOV/getmerchantidlistdetail_adv';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            this._sessionObj.lovDesc = "StatusType";
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.msgCode == "0016") {
                        _this.showMsg(data.msgDesc, false);
                    }
                    if (data.msgCode == "0000") {
                        if (!(data.lovType instanceof Array)) {
                            var m = [];
                            m[0] = data.lovType;
                            _this.ref._lov3.MerchantType = m;
                        }
                        else {
                            _this.ref._lov3.MerchantType = data.lovType;
                        }
                        _this._TypeList.lovStatus = [{ "value": "All", "caption": "All" }];
                        _this.ref._lov3.MerchantType.forEach(function (iItem) {
                            var l_Item = { "value": "", "caption": "" };
                            l_Item['caption'] = iItem.caption;
                            l_Item['value'] = iItem.value;
                            _this._TypeList.lovStatus.push(iItem);
                        });
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    merchantTransferReport.prototype.goDownload = function () {
        if (this._obj.fileType == 'EXCEL')
            this.goPrintExcel();
        else
            this.goPrint();
    };
    merchantTransferReport.prototype.goPrintExcel = function () {
        var _this = this;
        this._mflag = false;
        this.setDateObjIntoString();
        var l_Data = [];
        l_Data = this._FilterDataset;
        if (l_Data != null) {
            for (this.i = 0; this.i < l_Data.filterList.length; this.i++) {
                if (l_Data.filterList[this.i].itemid == "1") {
                    this._obj.simpleSearch = l_Data.filterList[this.i].t1;
                }
                else if (l_Data.filterList[this.i].itemid == "2") {
                    this._obj.fromDate = l_Data.filterList[this.i].t1;
                    this._obj.toDate = l_Data.filterList[this.i].t2;
                    this._obj.condition = l_Data.filterList[this.i].condition;
                }
                else if (l_Data.filterList[this.i].itemid == "3") {
                    this._obj.merchantID = l_Data.filterList[this.i].t1;
                }
                else if (l_Data.filterList[this.i].itemid == "4") {
                    this.file_type = l_Data.filterList[this.i].t1;
                    if (this.file_type == "1") {
                        this._obj.fileType = "EXCEL";
                    }
                    else
                        this._obj.fileType = "PDF";
                }
            }
        }
        if (this._obj.fromDate > this._obj.toDate) {
            this._mflag = true;
            this.showMsg("From Date Should Not Exceed To Date.", false);
        } /* if (!/^(([1-9]|1[0-9])\/[A-Za-z]+\([N|n]\)[0-9]{6})$/.test(this._obj.nrc)) {
        this.showMsg("Invalid NRC No.", false);
      } */
        else {
            try {
                this._obj.fileType = "EXCEL";
                var url = this.ics.cmsurl + 'ServiceReportAdm/getMerchantReportList1?download=1';
                var json = this._obj;
                this._obj.sessionID = this.ics._profile.sessionID;
                this._obj.userID = this.ics._profile.userID;
                this.http.doPost(url, json).subscribe(function (data) {
                    _this._mflag = true;
                    if (data.msgCode = "0016") {
                        _this.showMsg(data.msgDesc, false);
                    }
                    if (data.state) {
                        var url2 = _this.ics.cmsurl + 'ServiceReportAdm/downloadReport?fileType=' + _this._obj.fileType +
                            "&fileName=" + data.stringResult[0] + '&sessionID=' + _this.ics._profile.sessionID + '&userID=' + _this.ics._profile.userID;
                        if (_this._obj.fileType == "PDF") {
                            _this._mflag = true;
                            jQuery("#downloadPdf").html("<iframe src=" + url2 + "></iframe>");
                            _this.showMsg("Download Successfully", true);
                        }
                        if (_this._obj.fileType == "EXCEL") {
                            _this._mflag = true;
                            jQuery("#downloadExcel").html("<iframe src=" + url2 + "></iframe>");
                            _this.showMsg("Download Successfully", true);
                        }
                    }
                }, function (error) {
                    _this._mflag = true;
                    _this.showMsg("HTTP Error Type ", false);
                }, function () {
                    _this._mflag = true;
                });
            }
            catch (e) {
                alert("Invalid URL.");
            }
        }
    };
    /*loadStatus(){
         
      try {
         let url: string = this.ics.cmsurl + 'serviceAdmLOV/getLovType';
         this._sessionObj.sessionID = this.ics._profile.sessionID;
         this._sessionObj.userID = this.ics._profile.userID;
         this._sessionObj.lovDesc ="StatusType";
         let json: any = this._sessionObj;
         this.http.doPost(url, json).subscribe(
             data => {
                  if (data != null) {
                      if (data.msgCode == "0016") {
                          this.showMsg(data.msgDesc, false);
                      }
                      if (data.msgCode == "0000") {
                          if (!(data.lovType instanceof Array)) {
                              let m = [];
                              m[0] = data.lovType;
                              this.ref._lov3.StatusType = m;
                          } else {
                              this.ref._lov3.StatusType = data.lovType;
                          }
    
                          this.ref._lov3.StatusType.forEach((iItem) => {
                              let l_Item = { "value": "", "caption": "" };
    
                              l_Item['caption'] = iItem.caption;
                              l_Item['value'] = iItem.value;
    
                              this._TypeList.lovStatus.push(iItem);
                          });
                      }
                  }
              },
              error => {
                  if (error._body.type == "error") {
                      alert("Connection Timed Out.");
                  }
              },
              () => { }
          );
      } catch (e) {
          alert("Invalid URL.");
      }
    }*/
    merchantTransferReport.prototype.getAllMerchant = function () {
        var _this = this;
        this.http.doGet(this.ics.cmsurl + 'serviceAdmLOV/getmerchantidlistdetail?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (data) {
            _this.ref._lov3.ref015 = data.ref015;
            if (_this.ref._lov3.ref015 != null && _this.ref._lov3.ref015 != undefined) {
                if (!(_this.ref._lov3.ref015 instanceof Array)) {
                    var m = [];
                    m[1] = _this.ref._lov3.ref015;
                    _this._TypeList.merchant.push(m[1]);
                }
                _this._TypeList.merchant = [{ "value": "All", "caption": "All" }];
                for (var j = 0; j < _this.ref._lov3.ref015.length; j++) {
                    _this._TypeList.merchant.push({ "value": _this.ref._lov3.ref015[j].value, "caption": _this.ref._lov3.ref015[j].caption });
                }
            }
            /* if(this._obj.transtype != "2"){
                this.ref._lov3.ref015 =[];
            }else{
                this.ref._lov3.ref015 = this.merchant;
            } */
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
        }, function () { });
    };
    merchantTransferReport = __decorate([
        core_1.Component({
            selector: 'wallettopup-report',
            template: " \n  <div *ngIf=\"!_divexport\">  \n    <div class=\"container-fluid\">\n      <div class=\"row clearfix\">\n        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n\t\t      <form class=\"form-horizontal\" ngNoForm>\n            <legend>Merchant Transfer Report &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n            <!-- <i class=\"fa fa-file-excel-o\"  (click)=\"goPrintExcel()></i>  -->\n            <!--<button type=\"button\"  class=\"fa fa-file-excel-o\"  (click)=\"goPrintExcel()\" title=\"Download Excel File\"></button>   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->\n            <!-- <button type=\"button\" class=\"glyphicon glyphicon-save\" (click)=\"goPrint()\" title=\"Download PDF File\" style=\"background-color: #3b5998!important;color:white\"></button> -->\n    \n              \n\n              <!--<i  class=\"glyphicon glyphicon-file\" style=\"margin-left:-4px;\"></i> -->\n              <!--<i  class=\"glyphicon-glyphicon-download-alt\" style=\"margin-left:-4px;\"></i> -->\n              <!-- <i class=\"glyphicon-glyphicon-export\" style=\"margin-left:-4px;\"></i> -->\n              <button type=\"button\" class=\"btn btn-sm btn-primary\" style=\"cursor:pointer;text-align:center;margin-left: 750px;width:30px;height:34px; color:white ;font-size:14px\" (click)=\"goPrint()\" title=\"Download PDF File\">\n                    <i  class=\"glyphicon glyphicon-save\" style=\"color:white;margin-left:-4px;\"></i>\n              </button> \n              <button type=\"button\" class=\"btn btn-sm btn-primary\" style=\"cursor:pointer;text-align:center;margin-left: 9px;width:30px;height:34px; color:white ;font-size:20px\" (click)=\"goPrintExcel()\" title=\"Download Excel File\">\n                    <i  class=\"fa fa-file-excel-o\" style=\"color:white;margin-left:-4px;\"></i>\n              </button>\n             \n              <div style=\"float:right;text-align:right;\">                                \n                <div style=\"display:inline-block;padding-right:0px;width:280px;\">\n                  <div class=\"input-group\"> \n                    <input class=\"form-control\" type=\"text\" id=\"isInputSearch\" placeholder=\"Search\" autocomplete=\"off\" spellcheck=\"false\" [(ngModel)]=\"_SearchString\" [ngModelOptions]=\"{standalone: true}\" (keyup.enter)=\"filterSearch()\">\n\n                      <span id=\"btnSimpleSearch\" class=\"input-group-addon\">\n                        <i class=\"glyphicon glyphicon-search\" style=\"cursor: pointer;color:#2e8690\" (click)=\"filterSearch()\"></i>\n                      </span>\n                  </div>\n                </div>\n\n                  <button id=\"btnToggleSearch\" class=\"btn btn-sm btn-primary\" type=\"button\" style=\"cursor:pointer;text-align:center;margin-top:-24px;height:34px;\" (click)=\"btnToggleSearch_onClick()\" title=\"Collapse\">\n                    <i id=\"lblToggleSearch\" class=\"glyphicon glyphicon-menu-down\"></i> \n                  </button>\n\n                  <button *ngIf=\"false\" id=\"btnTogglePagination\" type=\"button\" class=\"btn btn-sm btn-primary\" style=\"cursor:pointer;text-align:center;margin-left:25px;margin-top:-24px;width:30px;height:34px;\" (click)=\"btnTogglePagination_onClick()\" title=\"Collapse\">\n                    <i id=\"lblTogglePagination\" class=\"glyphicon glyphicon-eject\" style=\"color:#2e8690;margin-left:-4px;transform: rotate(90deg)\"></i>\n                  </button>\n\n                 <!-- <div id=\"divPagination\" style=\"display:none;margin-left:13px;margin-top:-7px;margin-bottom:5px;\">\n                    <adminpager id=\"pgPagination\" rpPageSizeMax=\"100\" [(rpModel)]=\"_ListingDataset.totalCount\" (rpChanged)=\"changedPager($event)\" style=\"font-size:14px;text-align:right;margin-top:-7px;margin-right:10px;\"></adminpager>\n                  </div> -->\n\n                  <button id=\"btnClose\" type=\"button\" class=\"btn btn-sm btn-primary\" (click)=\"closePage()\" (mouseenter)=\"btnClose_MouseEnter()\" (mouseleave)=\"btnClose_MouseLeave()\" title=\"Close\" style=\"cursor:pointer;height:34px;text-align:center;margin-top:-24px;\">\n                    <i id=\"lblClose\" class=\"glyphicon glyphicon-remove\"></i> \n                  </button>                     \n              </div>\n            </legend>\n\t\t\t\t  <div class=\"cardview list-height\"> \n\t\t\t\t\t  <div class=\"row col-md-12\">\n             <!-- <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goClear()\">Clear</button> \n              <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goList()\">Show</button>              \n\t\t\t\t      <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goDownload()\">Download</button> -->\n            </div>\n            <advanced-search id=\"asAdvancedSearch\" [FilterList]=\"_FilterList\" [TypeList]=\"_TypeList\" rpVisibleItems=5 (rpHidden)=\"renderAdvancedSearch($event)\" (rpChanged)=\"filterAdvancedSearch($event)\" style=\"display:none;\"></advanced-search>\n          \n           <!-- <div class=\"row col-md-12\">&nbsp;</div> -->\n              <div class=\"form-group\">\n                <div class=\"col-md-12\">\n                \n                  <div [hidden]=\"_entryhide\">\n                    <div class=\"form-group\">\n                      <div class=\"col-md-8\">\n                        <div style=\"margin-top: 10px;\">  \n                          <adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"_list.totalCount\" (rpChanged)=\"changedPager($event)\"></adminpager>\n                        </div>\n                        <button id=\"btnClose\" type=\"button\" class=\"btn btn-sm btn-primary\" (click)=\"closeList()\" (mouseenter)=\"btnClose_MouseEnter()\" (mouseleave)=\"btnClose_MouseLeave()\" title=\"Close\" style=\"cursor:pointer;height:34px;text-align:center;\">\n                            <i id=\"lblClose\" class=\"glyphicon glyphicon-remove\"></i> \n                        </button>\n                      </div>\n\n                      <div class=\"col-md-4\" style=\"margin-top: 30px;overflow-x:auto;\">      \n                        <table style=\"align: right; font-size: 14px;float:right;\" cellpadding=\"10px\" cellspacing=\"10px\">\n                          <tbody>\n                            <colgroup>\n                             <col span=\"1\" style=\"width: 50%;\">\n                             <col span=\"1\" style=\"width: 50%;\">                                  \n                            </colgroup>                           \n                            <tr>\n                              <td style=\"color: blue;\" class=\"left\">\n                                <p><b>Grand Total Amount </b></p>\n                              </td>\n                              <td style=\"color: blue;\" class=\"center\"><p>&nbsp;&nbsp;:&nbsp;&nbsp;</p></td>\n                              <td style=\"color: blue;\" class=\"right\">\n                              <p><b>{{formatNumber(this.total_Amount)}}</b></p>\n                              </td>\n                            </tr>\n                            <tr>\n                              <td style=\"color: blue;\" class=\"left\">\n                                <p><b>Grand Total Bank Charges</b></p>\n                              </td>\n                              <td style=\"color: blue;\" class=\"center\"><p>&nbsp;&nbsp;:&nbsp;&nbsp;</p></td>\n                              <td style=\"color: blue;\" class=\"right\">\n                                <p><b>{{formatNumber(this.total_CommissionCharges)}}</b></p>           \n                              </td>\n                            </tr>\n                        </tbody>\n                      </table>\n                    </div>\n                  </div>\n               <div class=\"row col-md-12\" style=\"overflow-x:auto;\">\n                 <table class=\"table table-striped table-condensed table-hover tblborder\" id=\"tblstyle\">\n                   <thead>\n                    <tr>          \n                      <th class=\"right\" width=\"1%\" style=\"background-color: #e6f2ff; color: #333333\">No</th>\n                      <th class=\"right\" width=\"5%\" style=\"background-color: #e6f2ff; color: #333333\">Transfer </th>\n                      <th width=\"10%\" style=\"background-color: #e6f2ff; color: #333333\"> From Name</th>\n                      <th width=\"10%\" style=\"background-color: #e6f2ff; color: #333333\">From Account </th>\n                      <th width=\"5%\" style=\"background-color: #e6f2ff; color: #333333\"> Transfer Date/Time</th>\n                      <th width=\"5%\" class=\"right\" style=\"background-color: #e6f2ff; color: #333333\">Amount </th>\n                      <th width=\"10%\" class=\"right\" style=\"background-color: #e6f2ff; color: #333333\">Commission Charges</th>\n                    </tr>\n                   </thead>\n                   <tbody>\n                     <tr *ngFor=\"let obj of _list.data\" >\n                      <td class=\"right\">{{obj.autokey}}</td>\n                      <td class=\"right\" >{{obj.t3}}</td> \n                      <td class=\"left\">{{obj.t1}}</td> \n                      <td class=\"left\">{{obj.t2}}</td> \n                      <td>{{obj.t4}}</td> \n                      <td class=\"right\">{{formatNumber(obj.n1)}}</td> \n                      <td  class=\"right\">{{formatNumber(obj.n2)}}</td> \n                      </tr>  \n                   </tbody>\n                  </table>\n                </div>\n            </div>\t\t\t\t\t\t\t\t\n          </div>\t\t\t\t\n          </div>\n        </div>\n        </form> \n      </div>\n    </div> \n    <div id=\"downloadExcel\" style=\"display: none;width: 0px;height: 0px;\"></div>       \t\t\t\t\n    <div id=\"downloadPdf\" style=\"display: none;width: 0px;height: 0px;\"></div>                    \t\t\t\t\n\t</div>\n</div>    \n<div *ngIf=\"_divexport\">    \n\t  <button type=\"button\" class=\"close\"  (click)=\"goClose()\">&times;</button>\n\t  <iframe id=\"frame1\" [src]=\"_printUrl\" width=\"100%\" height=\"100%\"></iframe>\n\t</div>   \n\t<div [hidden] = \"_mflag\">\n\t  <div class=\"modal\" id=\"loader\"></div>\n\t</div>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n  ",
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef, rp_client_util_1.ClientUtil, core_1.Renderer, rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences, platform_browser_1.DomSanitizer])
    ], merchantTransferReport);
    return merchantTransferReport;
}());
exports.merchantTransferReport = merchantTransferReport;
/*<div class="form-group">
<label class="col-md-2">From Date</label>
<div class="col-md-3">
  <my-date-picker name="mydate"  [options]="myDatePickerOptions" [(ngModel)]="_dates.fromDate" ngDefaultControl></my-date-picker>
</div>
<label class="col-md-2">To Date</label>
<div class="col-md-3">
  <my-date-picker name="mydate" [options]="myDatePickerOptions" [(ngModel)]="_dates.toDate" ngDefaultControl></my-date-picker>
  <!-- <input class="form-control" type="date" [(ngModel)]="_obj.toDate" required="true" [ngModelOptions]="{standalone: true}">  -->
</div>
<div class="col-md-2">
  <label>
    <input type='checkbox' [(ngModel)]="_obj.alldate" [ngModelOptions]="{standalone: true}" (click)='getAllDate($event)'>
    ALL
  </label>
</div>
</div>
<div class="form-group">
<label class="col-md-2">Merchant Type</label>
<div class="col-md-3">
  <select  [(ngModel)]="_obj.merchantID" (change)="changeMerchant($event)" [ngModelOptions]="{standalone: true}" class="form-control input-sm" required>
    <option *ngFor="let item of merchant" value="{{item.value}}" >{{item.caption}}</option>
  </select>
</div>
<label class="col-md-2">File Type</label>
<div class="col-md-3">
  <select [(ngModel)]="_obj.select" [ngModelOptions]="{standalone: true}" (change)="changeSelectVal($event.target.options)" class="form-control col-md-0" required="true">
    <option *ngFor="let item of ref._lov1.ref010" value="{{item.value}}">{{item.caption}}</option>
  </select>
</div>
<!--<rp-input  [(rpModel)]="_obj.fileType" rpRequired ="true" rpLabelClass = "col-md-2" rpClass="col-md-3" rpType="ref010" rpLabel="File Type" (ngModelChange)="changeFileType($event)"></rp-input>-->
</div>*/ 
//# sourceMappingURL=merchantTransferReport.component.js.map