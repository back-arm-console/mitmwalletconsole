"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var router_1 = require('@angular/router');
var core_1 = require('@angular/core');
var rp_client_util_1 = require('../util/rp-client.util');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_references_1 = require('../framework/rp-references');
var TransactionReport = (function () {
    function TransactionReport(ics, _router, http, route, l_util, ref) {
        var _this = this;
        this.ics = ics;
        this._router = _router;
        this.http = http;
        this.route = route;
        this.l_util = l_util;
        this.ref = ref;
        this._isLoading = true;
        this._OperationMode = "";
        this._CallerPage = "";
        this._SearchString = "";
        this._showListing = false;
        this._showPagination = false;
        this._toggleSearch = true; // true - Simple Search, false - Advanced Search
        this._togglePagination = true;
        this._FormData = {
            "Posting": false, "TransNo": "TBA", "effectiveDate": "", "myDate": {}, "Amount": "", "AmountInWords": "", "Remark": "",
            "AccountNo1": "", "CurrencyCode1": "", "GLCheck1": false, "Description1": "", "AccountNo2": "", "CurrencyCode2": "", "GLCheck2": false, "Description2": "",
        };
        this._FilterDataset = {
            filterList: [
                { "itemid": "", "caption": "", "fieldname": "", "datatype": "", "condition": "", "t1": "", "t2": "" }
            ],
            "pageNo": 1, "pageSize": 10, "filterSource": 0, "userID": "", "userName": "", "productType": "", "subtype": "", "loancode": ""
        };
        this._ListingDataset = {
            dataList: [
                {
                    "transNo": "", "terminalID": "", "transRef": "", "userID": "", "accountNumber": "", "effectiveDate": "", "description": "", "chequeNo": "", "currencyCode": "",
                    "amount": "", "previousBalance": "", "transactionType": "", "remark": "", "status": "", "subRef": "", "serialNo": "", "transactionTime": "", "supervisorID": "", "branchCode": "", "accRef": "", "branchName": "", "glDescription": ""
                }
            ],
            "pageNo": 0, "pageSize": 0, "totalRecords": 1, "userID": "", "userName": ""
        };
        //added
        this._appCodeCombo = [];
        this._appCodeComboData = { "datalist": [{ "productType": "", "description": "", "syskey": "", "data": [{ "mdCode": "", "type": "", "typeDesc": "" }] }] };
        this._appData = "";
        this._appCodeData = "";
        this._appCodeDataList = { "data": [{ "mdCode": "", "type": "", "typeDesc": "" }] };
        this._subtypecombo = [{ "value": "", "caption": "" }];
        this._groupType = "";
        this._isMRHF = false;
        if (!ics.getRole() || ics.getRole() == 0) {
            this.http.doGet(this.ics._apiurl + 'service001/signout?userid=' + this.ics._profile.userID + "&sessionid=" + this.ics._profile.sessionID).subscribe(function (data) { }, function (error) {
                _this.ics.sendBean({ t1: "rp-error", t2: "Server connection error." });
            }, function () { });
            this._router.navigate(['Login', { p1: '*' }]);
        }
    }
    TransactionReport.prototype.ngOnInit = function () {
        var _this = this;
        // Stored Caller Form Data
        //
        if (this.ics.getBean() != null && this.ics.getBean().FormData != null) {
            this._FormData = this.ics.getBean().FormData;
        }
        // Check Caller Page
        //
        this._ParamCaller = this.route.params.subscribe(function (params) {
            _this._CallerPage = params['pCaller'];
            if (_this._CallerPage != undefined) {
                if (_this._CallerPage != null && _this._CallerPage == "FromAccountListForm") {
                    var _SearchString = params['paramFromAccountListForm'];
                    if (_SearchString != undefined && _SearchString != null && _SearchString != "") {
                        _this._SearchString = _SearchString;
                    }
                }
                _this.filterCommonSearch();
            }
            else {
                _this.filterRecords();
            }
        });
        this.loadAdvancedSearchData();
        this.loadTransactionColumn();
        this._isMRHF = false;
    };
    TransactionReport.prototype.ngAfterViewChecked = function () {
        this._isLoading = false;
    };
    TransactionReport.prototype.loadAdvancedSearchData = function () {
        this._TypeList =
            {
                "lovTransType": [
                    { "value": "0", "caption": "Debit Transaction" },
                    { "value": "1", "caption": "Credit Transaction" },
                    { "value": "2", "caption": "Reversal Transaction" }
                ],
                "lovTransType1": [
                    { "value": "0", "caption": "Contracted" },
                    { "value": "1", "caption": "Disbursed" },
                    { "value": "2", "caption": "Repaid" },
                    { "value": "3", "caption": "Closed" },
                    { "value": "4", "caption": "Deposit" },
                    { "value": "5", "caption": "Withdraw" },
                    { "value": "6", "caption": "Transfer" }
                ]
            };
        this._FilterList =
            [
                { "itemid": "1", "caption": "Trans No", "fieldname": "TransNo", "datatype": "numeric", "condition": "bt", "t1": "", "t2": "", "t3": "true" },
                { "itemid": "2", "caption": "Terminal ID", "fieldname": "WorkStation", "datatype": "string", "condition": "c", "t1": "", "t2": "", "t3": "" },
                { "itemid": "3", "caption": "Trans Ref", "fieldname": "TransRef", "datatype": "numeric", "condition": "bt", "t1": "", "t2": "", "t3": "true" },
                { "itemid": "4", "caption": "User ID", "fieldname": "TellerID", "datatype": "string", "condition": "c", "t1": "", "t2": "", "t3": "" },
                { "itemid": "5", "caption": "Account Number", "fieldname": "AccNumber", "datatype": "string", "condition": "c", "t1": "", "t2": "", "t3": "" },
                { "itemid": "6", "caption": "Effective Date", "fieldname": "EffectiveDate", "datatype": "date", "condition": "bt", "t1": "", "t2": "", "t3": "true" },
                { "itemid": "7", "caption": "Description", "fieldname": "Description", "datatype": "string", "condition": "c", "t1": "", "t2": "", "t3": "" },
                { "itemid": "8", "caption": "Cheque No", "fieldname": "ChequeNo", "datatype": "string", "condition": "c", "t1": "", "t2": "", "t3": "" },
                { "itemid": "9", "caption": "Currency Code", "fieldname": "CurrencyCode", "datatype": "string", "condition": "c", "t1": "", "t2": "", "t3": "" },
                { "itemid": "10", "caption": "Amount", "fieldname": "Amount", "datatype": "numeric", "condition": "bt", "t1": "", "t2": "", "t3": "true" },
                { "itemid": "11", "caption": "Previous Balance", "fieldname": "PrevBalance", "datatype": "numeric", "condition": "bt", "t1": "", "t2": "", "t3": "true" },
                { "itemid": "12", "caption": "Transaction Type (In Digit)", "fieldname": "TransType", "datatype": "numeric", "condition": "bt", "t1": "", "t2": "", "t3": "true" },
                { "itemid": "13", "caption": "Remark", "fieldname": "Remark", "datatype": "string", "condition": "c", "t1": "", "t2": "", "t3": "" },
                { "itemid": "15", "caption": "Sub Ref", "fieldname": "SubRef", "datatype": "string", "condition": "c", "t1": "", "t2": "", "t3": "" },
                { "itemid": "16", "caption": "Filter Type (dr/cr/rev)", "fieldname": "TransType", "datatype": "lovTransType", "condition": "", "t1": "", "t2": "", "t3": "" },
                { "itemid": "17", "caption": "Product Name", "fieldname": "ProductName", "datatype": "string", "condition": "c", "t1": "", "t2": "", "t3": "" },
                { "itemid": "19", "caption": "Supervisor ID", "fieldname": "SupervisorID", "datatype": "string", "condition": "c", "t1": "", "t2": "", "t3": "" },
                { "itemid": "20", "caption": "Serial No", "fieldname": "SerialNo", "datatype": "numeric", "condition": "bt", "t1": "", "t2": "", "t3": "true" },
                { "itemid": "21", "caption": "Transaction Date", "fieldname": "TransTime", "datatype": "date", "condition": "bt", "t1": "", "t2": "", "t3": "true" },
                { "itemid": "23", "caption": "Currency Rate", "fieldname": "CurrencyRate", "datatype": "numeric", "condition": "bt", "t1": "", "t2": "", "t3": "true" },
                { "itemid": "24", "caption": "Previous Update", "fieldname": "PrevUpdate", "datatype": "date", "condition": "bt", "t1": "", "t2": "", "t3": "true" },
                { "itemid": "25", "caption": "Contra Date", "fieldname": "ContraDate", "datatype": "date", "condition": "bt", "t1": "", "t2": "", "t3": "true" },
                { "itemid": "26", "caption": "Acc Ref", "fieldname": "AccRef", "datatype": "string", "condition": "c", "t1": "", "t2": "", "t3": "" },
                { "itemid": "29", "caption": "Event Type", "fieldname": "TransType", "datatype": "lovTransType1", "condition": "", "t1": "", "t2": "", "t3": "" },
            ];
    };
    TransactionReport.prototype.loadTransactionColumn = function () {
        var _this = this;
        this.http.doGet('json/transactionlist.json?random=' + Math.random()).subscribe(function (data) {
            if (data != null) {
                _this._ItemList = data;
                if (!_this._isMRHF) {
                    _this._ItemList = _this._ItemList.filter(function (item) { return item != "22"; });
                }
                else {
                    _this._ItemList = _this._ItemList.filter(function (item) { return item != "25"; });
                }
            }
        });
    };
    TransactionReport.prototype.btnToggleSearch_onClick = function () {
        this.toggleSearch(!this._toggleSearch);
    };
    TransactionReport.prototype.btnTogglePagination_onClick = function () {
        this.togglePagination(!this._togglePagination);
    };
    TransactionReport.prototype.toggleSearch = function (aToggleSearch) {
        // Set Flag
        this._toggleSearch = aToggleSearch;
        // Clear Simple Search
        if (!this._toggleSearch)
            jQuery("#isInputSearch").val("");
        // Enable/Disabled Simple Search Textbox
        jQuery('#isInputSearch').prop('disabled', !this._toggleSearch);
        // Hide Advanced Search
        if (this._toggleSearch)
            this.ics.send001("CLEAR");
        // Show/Hide Advanced Search    
        //  
        if (this._toggleSearch)
            jQuery("#asAdvancedSearch").css("display", "none"); // true     - Simple Search
        else
            jQuery("#asAdvancedSearch").css("display", "inline-block"); // false    - Advanced Search
        // Set Icon 
        //  
        if (this._toggleSearch)
            jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-up").addClass("glyphicon glyphicon-menu-down"); // true     - Simple Search
        else
            jQuery("#lblToggleSearch").removeClass("glyphicon glyphicon-menu-down").addClass("glyphicon glyphicon-menu-up"); // false    - Advanced Search
        // Set Tooltip
        //
        var l_Tooltip = (this._toggleSearch) ? "Expand" : "Collapse";
        jQuery('#btnToggleSearch').attr('title', l_Tooltip);
    };
    TransactionReport.prototype.togglePagination = function (aTogglePagination) {
        // Set Flag
        this._togglePagination = aTogglePagination;
        // Show/Hide Pagination
        //
        if (this._showPagination && this._togglePagination)
            jQuery("#divPagination").css("display", "inline-block");
        else
            jQuery("#divPagination").css("display", "none");
        // Rotate Icon
        //
        if (!this._togglePagination)
            jQuery("#lblTogglePagination").css("transform", "rotate(270deg)");
        else
            jQuery("#lblTogglePagination").css("transform", "rotate(90deg)");
        // Set Icon Position
        if (!this._togglePagination)
            jQuery("#lblTogglePagination").css({ "margin-left": "-5px", "margin-top": "0px" });
        else
            jQuery("#lblTogglePagination").css({ "margin-left": "-5px", "margin-top": "-1px" });
        // Set Tooltip
        //
        var l_Tooltip = (this._togglePagination) ? "Collapse" : "Expand";
        jQuery('#btnTogglePagination').attr('title', l_Tooltip);
    };
    TransactionReport.prototype.goBack = function () {
        var l_URL = "";
        switch (this._CallerPage) {
            case "Receipt":
                l_URL = "icbs/Receipt1";
                break;
            case "Payment":
                l_URL = "icbs/Payment1";
                break;
            case "Transfer":
                l_URL = "icbs/Transfer1";
                break;
            case "FromAccountListForm":
                l_URL = "icbs/AccOpeningListing";
                break;
            default: break;
        }
        if (l_URL != "") {
            this.ics.sendBean({ "FormData": this._FormData });
            this._router.navigate([l_URL]);
        }
    };
    TransactionReport.prototype.btnClose_MouseEnter = function () {
        jQuery("#btnClose").removeClass('btn btn-md btn-secondary').addClass('btn btn-md btn-danger');
    };
    TransactionReport.prototype.btnClose_MouseLeave = function () {
        jQuery("#btnClose").removeClass('btn btn-md btn-danger').addClass('btn btn-md btn-secondary');
    };
    TransactionReport.prototype.closePage = function () {
        if (this._CallerPage != undefined) {
            this.goBack();
        }
        else {
            this._router.navigate(['000']);
        }
    };
    TransactionReport.prototype.renderAdvancedSearch = function (event) {
        this.toggleSearch(!event);
    };
    TransactionReport.prototype.changedPager = function (event) {
        if (!this._isLoading) {
            var l_objPagination = event.obj;
            this._FilterDataset.pageNo = l_objPagination.current;
            this._FilterDataset.pageSize = l_objPagination.size;
            this.filterRecords();
        }
    };
    TransactionReport.prototype.filterSearch = function () {
        if (this._toggleSearch)
            this.filterCommonSearch();
        else
            this.ics.send001("FILTER");
    };
    TransactionReport.prototype.filterCommonSearch = function () {
        var l_SearchString = "";
        this._FilterDataset.filterList = [];
        this._FilterDataset.filterSource = 0;
        if (jQuery.trim(this._SearchString) != "") {
            l_SearchString = jQuery.trim(this._SearchString);
            this._FilterDataset.filterList =
                [
                    { "itemid": "1", "caption": "Trans No", "fieldname": "TransNo", "datatype": "numeric", "condition": "eq", "t1": l_SearchString, "t2": "" },
                    { "itemid": "3", "caption": "Trans Ref", "fieldname": "TransRef", "datatype": "numeric", "condition": "eq", "t1": l_SearchString, "t2": "" },
                    { "itemid": "5", "caption": "Account Number", "fieldname": "AccNumber", "datatype": "string", "condition": "c", "t1": l_SearchString, "t2": "" },
                    { "itemid": "26", "caption": "Acc Ref", "fieldname": "AccRef", "datatype": "string", "condition": "c", "t1": l_SearchString, "t2": "" }
                ];
        }
        if (this._OperationMode == "prepareFilter") {
            this._OperationMode = "";
        }
        else {
            if (this._FilterDataset.pageNo == 1)
                this.filterRecords();
            else
                this._FilterDataset.pageNo = 1;
        }
    };
    TransactionReport.prototype.filterAdvancedSearch = function (event) {
        this._FilterDataset.filterSource = 1;
        this._FilterDataset.filterList = event;
        if (this._OperationMode == "prepareFilter") {
            this._OperationMode = "";
        }
        else {
            if (this._FilterDataset.pageNo == 1)
                this.filterRecords();
            else
                this._FilterDataset.pageNo = 1;
        }
    };
    TransactionReport.prototype.filterRecords = function () {
        var _this = this;
        this._FilterDataset.subtype = this._groupType;
        this._FilterDataset.productType = this._appData;
        this._FilterDataset.loancode = this._appCodeData;
        var l_Data = this._FilterDataset;
        var l_ServiceURL = this.ics.walleturl + 'serviceDBS/getTransactionListingDataset';
        // Show loading animation
        jQuery("#loader").modal();
        this.http.doPost(l_ServiceURL, l_Data).subscribe(function (data) {
            if (data != null) {
                _this._ListingDataset = data;
                // Convert to array for single item
                if (_this._ListingDataset.dataList != undefined) {
                    _this._ListingDataset.dataList = _this.l_util.convertToArray(_this._ListingDataset.dataList);
                    // Add thousand separator
                    //
                    for (var iIndex = 0; iIndex < _this._ListingDataset.dataList.length; iIndex++) {
                        _this._ListingDataset.dataList[iIndex].amount = _this.l_util.thousand_sperator(parseFloat(_this._ListingDataset.dataList[iIndex].amount).toFixed(2));
                        _this._ListingDataset.dataList[iIndex].previousBalance = _this.l_util.thousand_sperator(parseFloat(_this._ListingDataset.dataList[iIndex].previousBalance).toFixed(2));
                    }
                }
                // Show / Hide Listing
                _this._showListing = (_this._ListingDataset.dataList != undefined);
                // Show / Hide Pagination
                _this._showPagination = (_this._showListing && (_this._ListingDataset.totalRecords > 10));
                // Show / Hide Pagination
                _this.togglePagination(_this._showListing);
                // Hide loading animation
                jQuery("#loader").modal('hide');
            }
        }, function (error) {
            // Hide loading animation
            jQuery("#loader").modal('hide');
            // Show error message
            _this.ics.sendBean({ t1: "rp-error", t2: "Server connection error." });
        }, function () { });
    };
    TransactionReport.prototype.btnExport_click = function () {
        var _this = this;
        this._OperationMode = "prepareFilter";
        this.filterSearch();
        if (this._OperationMode == "") {
            jQuery("#loader").modal();
            var l_Data = this._FilterDataset;
            var l_Url = this.ics.walleturl + 'serviceDBS/transactionListDownload';
            this.http.doPost(l_Url, l_Data).subscribe(function (res) {
                if (res != null) {
                    var l_FileName = res.msgCode;
                    _this.downloadFile(l_FileName);
                }
                _this._OperationMode = "";
                jQuery("#loader").modal('hide');
            }, function (error) {
                _this._OperationMode = "";
                jQuery("#loader").modal('hide');
                _this.ics.sendBean({ t1: "rp-error", t2: "Server connection error." });
            }, function () { });
        }
        else {
            this._OperationMode == "";
            this.ics.sendBean({ t1: "rp-error", t2: "Please select the selection criteria." });
        }
    };
    TransactionReport.prototype.downloadFile = function (aFileName) {
        var l_Url = this.ics.walleturl + 'serviceDBS/downloadexcel?filename=' + aFileName;
        jQuery("#ExcelDownload").html("<iframe src=" + l_Url + "></iframe>");
    };
    TransactionReport.prototype.getCaption = function (aItemID) {
        var l_Caption = '';
        switch (aItemID) {
            case '1':
                l_Caption = 'Trans. No.';
                break;
            case '2':
                l_Caption = 'Terminal ID';
                break;
            case '3':
                l_Caption = 'Account Number';
                break;
            case '4':
                l_Caption = 'Description';
                break;
            case '5':
                l_Caption = 'Debit Amount';
                break;
            case '6':
                l_Caption = 'Credit Amount';
                break;
            case '7':
                l_Caption = 'Type';
                break;
            case '8':
                l_Caption = 'Cheque No.';
                break;
            case '9':
                l_Caption = 'Sub Ref.';
                break;
            case '10':
                l_Caption = 'Acc. Ref.';
                break;
            case '11':
                l_Caption = 'Currency';
                break;
            case '12':
                l_Caption = 'Remark';
                break;
            case '13':
                l_Caption = 'Effective Date';
                break;
            case '14':
                l_Caption = 'Trans. Time';
                break;
            case '15':
                l_Caption = 'Previous Bal.';
                break;
            case '16':
                l_Caption = 'Serial No.';
                break;
            case '17':
                l_Caption = 'Trans. Ref.';
                break;
            case '18':
                l_Caption = 'Branch Code';
                break;
            case '19':
                l_Caption = 'User ID';
                break;
            case '20':
                l_Caption = 'Counter';
                break;
            case '21':
                l_Caption = 'Status';
                break;
            case '22':
                l_Caption = 'Event Type';
                break;
            case '23':
                l_Caption = 'Branch Name';
                break;
            case '24':
                l_Caption = 'Account Description';
                break;
            case '25':
                l_Caption = 'Actions';
                break;
            default: break;
        }
        return l_Caption;
    };
    TransactionReport.prototype.getValue = function (aItemID, aItem) {
        var l_Value = '';
        switch (aItemID) {
            case '1':
                l_Value = aItem.transNo;
                break;
            case '2':
                l_Value = aItem.terminalID;
                break;
            case '3':
                l_Value = aItem.accountNumber;
                break;
            case '4':
                l_Value = aItem.description;
                break;
            case '5':
                l_Value = (aItem.transactionType >= 500) ? aItem.amount : '';
                break;
            case '6':
                l_Value = (aItem.transactionType < 500) ? aItem.amount : '';
                break;
            case '7':
                l_Value = aItem.transactionType;
                break;
            case '8':
                l_Value = aItem.chequeNo;
                break;
            case '9':
                l_Value = aItem.subRef;
                break;
            case '10':
                l_Value = aItem.accRef;
                break;
            case '11':
                l_Value = aItem.currencyCode;
                break;
            case '12':
                l_Value = aItem.remark;
                break;
            case '13':
                if (this._isMRHF)
                    l_Value = this.l_util.convertDate7(aItem.effectiveDate);
                else
                    l_Value = aItem.effectiveDate;
                break;
            case '14':
                if (this._isMRHF)
                    l_Value = aItem.transactionTime;
                else
                    l_Value = this.l_util.convertDBDateTime1(aItem.transactionTime);
                break;
            case '15':
                l_Value = aItem.previousBalance;
                break;
            case '16':
                l_Value = aItem.serialNo;
                break;
            case '17':
                l_Value = aItem.transRef;
                break;
            case '18':
                l_Value = aItem.branchCode;
                break;
            case '19':
                l_Value = aItem.userID;
                break;
            case '20':
                l_Value = aItem.supervisorID;
                break;
            case '21':
                l_Value = aItem.status;
                break;
            case '22':
                l_Value = aItem.transactionType1;
                break;
            case '23':
                l_Value = aItem.branchName;
                break;
            case '24':
                l_Value = aItem.glDescription;
                break;
            case '25':
                l_Value = "";
                break;
            default: break;
        }
        console.log('Item ID :', aItemID, ', Value : ', l_Value);
        return l_Value;
    };
    TransactionReport.prototype.goTo = function (obj) {
        var accNo = obj.accountNumber;
        console.log("accNo is" + accNo);
        this._router.navigate(['icbs/AccOpeningListing', { cmd: "FromTransactionListForm", p1: accNo }]);
    };
    TransactionReport.prototype.goAccountOpeningForm = function (obj) {
        var accNumber = obj.accountNumber;
        console.log("accNumber is" + accNumber);
        this._router.navigate(['icbs/AccOpening', { cmd: "FromTransactionListForm", p1: accNumber }]);
    };
    TransactionReport = __decorate([
        core_1.Component({
            selector: 'frmtransactionlist',
            template: "\n    <div class=\"container-fluid\" style=\"padding-top:15px;\">\n        <div class=\"row clearfix\">\n            <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n                <form class=\"form-horizontal\">\n                    <fieldset>\n                        <legend>\n                            <div style=\"float:left;display:inline;font-size:18px;margin-top:-3px;margin-left:10px;\">\n                                Transaction\n                               \n                           </div>\n\n                        <div style=\"float:right;text-align:right;margin-top:-8px;margin-bottom:5px;\">\n                            <div style=\"display:inline-block;padding-right:0px;width:280px;\">\n                                    <div class=\"input-group\"> \n                                        <input class=\"form-control\" type=\"text\" id=\"isInputSearch\" placeholder=\"Search\" autocomplete=\"off\" spellcheck=\"false\" [(ngModel)]=\"_SearchString\" [ngModelOptions]=\"{standalone: true}\" (keyup.enter)=\"filterSearch()\">\n\n                                        <span id=\"btnSimpleSearch\" class=\"input-group-addon\">\n                                            <i class=\"glyphicon glyphicon-search\" style=\"cursor: pointer\" (click)=\"filterSearch()\"></i>\n                                        </span>\n                                    </div>\n                                </div>\n\n                                <button id=\"btnToggleSearch\" class=\"btn btn-md btn-secondary\" type=\"button\" style=\"cursor:pointer;text-align:center;margin-top:-24px;height:34px;\" (click)=\"btnToggleSearch_onClick()\" title=\"Collapse\">\n                                    <i id=\"lblToggleSearch\" class=\"glyphicon glyphicon-menu-down\"></i> \n                                </button>\n\n                                <button *ngIf=\"false\" id=\"btnTogglePagination\" type=\"button\" class=\"btn btn-md btn-default\" style=\"cursor:pointer;text-align:center;margin-left:25px;margin-top:-24px;width:30px;height:34px;\" (click)=\"btnTogglePagination_onClick()\" title=\"Collapse\">\n                                    <i id=\"lblTogglePagination\" class=\"glyphicon glyphicon-eject\" style=\"color:#2e8690;margin-left:-4px;transform:rotate(90deg)\"></i>\n                                </button>\n\n                                <div id=\"divPagination\" style=\"display:none;margin-left:13px;margin-top:-7px;margin-bottom:5px;\">\n                                    <pager6 id=\"pgPagination\" rpPageSizeMax=\"100\" [(rpModel)]=\"_ListingDataset.totalRecords\" (rpChanged)=\"changedPager($event)\" style=\"font-size:14px;text-align:right;margin-top:-7px;margin-right:10px;\"></pager6>\n                                </div>\n\n                                <button id=\"btnClose\" type=\"button\" class=\"btn btn-md btn-secondary\" (click)=\"closePage()\" (mouseenter)=\"btnClose_MouseEnter()\" (mouseleave)=\"btnClose_MouseLeave()\" title=\"Close\" style=\"cursor:pointer;height:34px;text-align:center;margin-top:-24px;\">\n                                    <i id=\"lblClose\" class=\"glyphicon glyphicon-remove\"></i> \n                                </button>                      \n                            </div>\n                        </legend>\n\n                        <advanced-search id=\"asAdvancedSearch\" [FilterList]=\"_FilterList\" [TypeList]=\"_TypeList\" rpVisibleItems=5 (rpHidden)=\"renderAdvancedSearch($event)\" (rpChanged)=\"filterAdvancedSearch($event)\" style=\"display:none;\"></advanced-search>\n                        <div *ngIf=\"!_showListing\" style=\"color:#ccc;\">No result found!</div>\n                        \n                        <div *ngIf=\"_showListing\">\n                            <div class=\"table-responsive\">\n                                <table class=\"table table-striped table-condense table-hover tblborder table-bordered\" style=\"font-size: small; margin-bottom: 0px !important;\">\n                                    <thead>\n                                        <tr>\n                                            <th *ngFor=\"let iItemID of _ItemList\"  title=\"{{ getCaption(iItemID) }}\">\n                                                <div *ngIf=\"iItemID=='1'\" style=\"min-width: 80px;table-layout:fixed;vertical-align:middle;text-align:right;\">{{ getCaption(iItemID) }}</div>\n                                                <div *ngIf=\"iItemID=='2'\" style=\"min-width: 85px;table-layout:fixed;vertical-align:middle;text-align:center;\">{{ getCaption(iItemID) }}</div>\n                                                <div *ngIf=\"iItemID=='3'\" style=\"min-width: 140px;table-layout:fixed;vertical-align:middle;text-align:left;\">{{ getCaption(iItemID) }}</div>\n                                                <div *ngIf=\"iItemID=='4'\" style=\"min-width: 470px;table-layout:fixed;vertical-align:middle;text-align:left;\">{{ getCaption(iItemID) }}</div>\n                                                <div *ngIf=\"iItemID=='5'\" style=\"min-width: 110px;table-layout:fixed;vertical-align:middle;text-align:right;\">{{ getCaption(iItemID) }}</div>\n                                                <div *ngIf=\"iItemID=='6'\" style=\"min-width: 110px;table-layout:fixed;vertical-align:middle;text-align:right;\">{{ getCaption(iItemID) }}</div>\n                                                <div *ngIf=\"iItemID=='7'\" style=\"min-width: 50px;table-layout:fixed;vertical-align:middle;text-align:center;\">{{ getCaption(iItemID) }}</div>\n                                                <div *ngIf=\"iItemID=='8'\" style=\"min-width: 80px;table-layout:fixed;vertical-align:middle;text-align:center;\">{{ getCaption(iItemID) }}</div>\n                                                <div *ngIf=\"iItemID=='9'\" style=\"min-width: 80px;table-layout:fixed;vertical-align:middle;text-align:left;\">{{ getCaption(iItemID) }}</div>\n                                                <div *ngIf=\"iItemID=='10'\" style=\"min-width: 100px;table-layout:fixed;vertical-align:middle;text-align:left;\">{{ getCaption(iItemID) }}</div>\n                                                <div *ngIf=\"iItemID=='11'\" style=\"min-width: 60px;table-layout:fixed;vertical-align:middle;text-align:center;\">{{ getCaption(iItemID) }}</div>\n                                                <div *ngIf=\"iItemID=='12'\" style=\"min-width: 280px;table-layout:fixed;vertical-align:middle;text-align:left;\">{{ getCaption(iItemID) }}</div>\n                                                <div *ngIf=\"iItemID=='13'\" style=\"min-width: 100px;table-layout:fixed;vertical-align:middle;text-align:left;\">{{ getCaption(iItemID) }}</div>\n                                                <div *ngIf=\"iItemID=='14'\" style=\"min-width: 150px;table-layout:fixed;vertical-align:middle;text-align:left;\">{{ getCaption(iItemID) }}</div>\n                                                <div *ngIf=\"iItemID=='15'\" style=\"min-width: 110px;table-layout:fixed;vertical-align:middle;text-align:right;\">{{ getCaption(iItemID) }}</div>\n                                                <div *ngIf=\"iItemID=='16'\" style=\"min-width: 70px;table-layout:fixed;vertical-align:middle;text-align:right;\">{{ getCaption(iItemID) }}</div>\n                                                <div *ngIf=\"iItemID=='17'\" style=\"min-width: 80px;table-layout:fixed;vertical-align:middle;text-align:right;\">{{ getCaption(iItemID) }}</div>\n                                                <div *ngIf=\"iItemID=='18'\" style=\"min-width: 100px;table-layout:fixed;vertical-align:middle;text-align:center;\">{{ getCaption(iItemID) }}</div>\n                                                <div *ngIf=\"iItemID=='19'\" style=\"min-width: 200px;table-layout:fixed;vertical-align:middle;text-align:left;\">{{ getCaption(iItemID) }}</div>\n                                                <div *ngIf=\"iItemID=='20'\" style=\"min-width: 70px;table-layout:fixed;vertical-align:middle;text-align:center;\">{{ getCaption(iItemID) }}</div>\n                                                <div *ngIf=\"iItemID=='21'\" style=\"min-width: 50px;table-layout:fixed;vertical-align:middle;text-align:center;\">{{ getCaption(iItemID) }}</div>\n                                                <div *ngIf=\"iItemID=='22'\" style=\"min-width: 70px;table-layout:fixed;vertical-align:middle;text-align:center;\">{{ getCaption(iItemID) }}</div>\n                                                <div *ngIf=\"iItemID=='23'\" style=\"min-width: 140px;table-layout:fixed;vertical-align:middle;text-align:left;\">{{ getCaption(iItemID) }}</div>\n                                                <div *ngIf=\"iItemID=='24'\" style=\"min-width: 470px;table-layout:fixed;vertical-align:middle;text-align:left;\">{{ getCaption(iItemID) }}</div>\n                                                <div *ngIf=\"iItemID=='25'  && !_isMRHF\" style=\"min-width: 80px;table-layout:fixed;vertical-align:middle;text-align:left;\">{{ getCaption(iItemID) }}</div>\n                                            </th>\n                                        </tr>\n                                    </thead>\n\n                                    <tbody>\n                                        <tr *ngFor=\"let iItem of _ListingDataset.dataList\">\n                                           <td *ngFor=\"let iItemID of _ItemList\" title=\"{{ getValue(iItemID, iItem) }}\">\n                                                <div *ngIf=\"iItemID=='1'\" style=\"min-width: 80px;table-layout:fixed;vertical-align:middle;text-align:right;\">{{ getValue(iItemID, iItem) }}</div>\n                                                <div *ngIf=\"iItemID=='2'\" style=\"min-width: 85px;table-layout:fixed;vertical-align:middle;text-align:center;\">{{ getValue(iItemID, iItem) }}</div>\n                                                <div *ngIf=\"iItemID=='3'\" style=\"min-width: 140px;table-layout:fixed;vertical-align:middle;text-align:left;\">{{ getValue(iItemID, iItem) }}</div>\n                                                <div *ngIf=\"iItemID=='4'\" style=\"min-width: 470px;table-layout:fixed;vertical-align:middle;text-align:left;\">{{ getValue(iItemID, iItem) }}</div>\n                                                <div *ngIf=\"iItemID=='5'\" style=\"min-width: 110px;table-layout:fixed;vertical-align:middle;text-align:right;\">{{ getValue(iItemID, iItem) }}</div>\n                                                <div *ngIf=\"iItemID=='6'\" style=\"min-width: 110px;table-layout:fixed;vertical-align:middle;text-align:right;\">{{ getValue(iItemID, iItem) }}</div>\n                                                <div *ngIf=\"iItemID=='7'\" style=\"min-width: 50px;table-layout:fixed;vertical-align:middle;text-align:center;\">{{ getValue(iItemID, iItem) }}</div>\n                                                <div *ngIf=\"iItemID=='8'\" style=\"min-width: 80px;table-layout:fixed;vertical-align:middle;text-align:center;\">{{ getValue(iItemID, iItem) }}</div>\n                                                <div *ngIf=\"iItemID=='9'\" style=\"min-width: 80px;table-layout:fixed;vertical-align:middle;text-align:left;\">{{ getValue(iItemID, iItem) }}</div>\n                                                <div *ngIf=\"iItemID=='10'\" style=\"min-width: 100px;table-layout:fixed;vertical-align:middle;text-align:left;\">{{ getValue(iItemID, iItem) }}</div>\n                                                <div *ngIf=\"iItemID=='11'\" style=\"min-width: 60px;table-layout:fixed;vertical-align:middle;text-align:center;\">{{ getValue(iItemID, iItem) }}</div>\n                                                <div *ngIf=\"iItemID=='12'\" style=\"min-width: 280px;table-layout:fixed;vertical-align:middle;text-align:left;\">{{ getValue(iItemID, iItem) }}</div>\n                                                <div *ngIf=\"iItemID=='13'\" style=\"min-width: 100px;table-layout:fixed;vertical-align:middle;text-align:left;\">{{ getValue(iItemID, iItem) }}</div>\n                                                <div *ngIf=\"iItemID=='14'\" style=\"min-width: 150px;table-layout:fixed;vertical-align:middle;text-align:left;\">{{ getValue(iItemID, iItem) }}</div>\n                                                <div *ngIf=\"iItemID=='15'\" style=\"min-width: 110px;table-layout:fixed;vertical-align:middle;text-align:right;\">{{ getValue(iItemID, iItem) }}</div>\n                                                <div *ngIf=\"iItemID=='16'\" style=\"min-width: 70px;table-layout:fixed;vertical-align:middle;text-align:right;\">{{ getValue(iItemID, iItem) }}</div>\n                                                <div *ngIf=\"iItemID=='17'\" style=\"min-width: 80px;table-layout:fixed;vertical-align:middle;text-align:right;\">{{ getValue(iItemID, iItem) }}</div>\n                                                <div *ngIf=\"iItemID=='18'\" style=\"min-width: 100px;table-layout:fixed;vertical-align:middle;text-align:center;\">{{ getValue(iItemID, iItem) }}</div>\n                                                <div *ngIf=\"iItemID=='19'\" style=\"min-width: 200px;table-layout:fixed;vertical-align:middle;text-align:left;\">{{ getValue(iItemID, iItem) }}</div>\n                                                <div *ngIf=\"iItemID=='20'\" style=\"min-width: 70px;table-layout:fixed;vertical-align:middle;text-align:center;\">{{ getValue(iItemID, iItem) }}</div>\n                                                <div *ngIf=\"iItemID=='21'\" style=\"min-width: 50px;table-layout:fixed;vertical-align:middle;text-align:center;\">{{ getValue(iItemID, iItem) }}</div>\n                                                <div *ngIf=\"iItemID=='22'\" style=\"min-width: 70px;table-layout:fixed;vertical-align:middle;text-align:center;\">{{ getValue(iItemID, iItem) }}</div>\n                                                <div *ngIf=\"iItemID=='23'\" style=\"min-width: 140px;table-layout:fixed;vertical-align:middle;text-align:left;\">{{ getValue(iItemID, iItem) }}</div>\n                                                <div *ngIf=\"iItemID=='24'\" style=\"min-width: 470px;table-layout:fixed;vertical-align:middle;text-align:left;\">{{ getValue(iItemID, iItem) }}</div>\n                                                <div *ngIf=\"iItemID=='25' && !_isMRHF\" style=\"min-width: 80px;table-layout:fixed;vertical-align:middle;text-align:left;\">\n                                                    <div class=\"dropdown\">\n                                                        <button class=\"btn btn-link dropdown-toggle\" type=\"button\" id=\"customMenu\" data-toggle=\"dropdown\" style=\"text-decoration: none; outline: none; font-size: 12px; padding-left: 0px; padding-right: 0px;\">\n                                                            Actions\n                                                            <span class=\"caret\"></span>\n                                                        </button>\n                                                        <ul class=\"dropdown-menu\" style=\"top: 100%; right: 0; font-size: 12px; left: auto;\" role=\"menu\" aria-labelledby=\"customMenu\" data-container=\"body\">\n                                                            <li role=\"presentation\"><a role=\"menuitem\" tabindex=\"-1\" (click)=\"goTo(iItem)\">Account Inquiry</a></li>\n                                                            <li role=\"presentation\"><a role=\"menuitem\" tabindex=\"-1\" (click)=\"goAccountOpeningForm(iItem)\">Account Opening</a></li>\n                                                        </ul>\n                                                    </div>\n                                                </div>\n                                             </td>\n                                        </tr>\n                                    </tbody>\n                                </table>\n                            </div>\n                        </div>\n\n                        <div *ngIf=\"_showListing\" style=\"text-align:center\">Total {{ _ListingDataset.totalRecords }}</div>\n                    </fieldset> \n                </form>\n                <div id=\"ExcelDownload\" style=\"display: none;width: 0px;height: 0px;\"></div>\n                <div class=\"loader modal\" id=\"loader\" style=\"top:40%;left:40%\"></div>\n            </div> \n        </div>\n    </div>\n    ",
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_http_service_1.RpHttpService, router_1.ActivatedRoute, rp_client_util_1.ClientUtil, rp_references_1.RpReferences])
    ], TransactionReport);
    return TransactionReport;
}());
exports.TransactionReport = TransactionReport;
//# sourceMappingURL=transactionreport.component.js.map