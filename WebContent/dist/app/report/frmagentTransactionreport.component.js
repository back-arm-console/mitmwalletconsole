"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// RP Framework
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var Rx_1 = require('rxjs/Rx');
core_2.enableProdMode();
// Application Specific
var FrmagentTransactionComponent = (function () {
    function FrmagentTransactionComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this.id = "";
        this._mflag = false;
        this._obj = this.getDefaultObj();
        this._util = new rp_client_util_1.ClientUtil();
        this.today = this._util.getTodayDate();
        this.myDatePickerOptions = this._util.getDatePicker();
        this._dates = { "fromDate": this.dateobj, "toDate": this.dateobj };
        this._totalcount = 1;
        this._entryhide = true;
        this._sessionMsg = "";
        this._key = "";
        this._grandAmtTotal = "";
        this._BchargesAmt = "";
        this._DchargesAmt = "";
        this._EchargesAmt = "";
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "" };
        this._pgobj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 1 };
        this._list = {
            "arr": [{
                    "transdate": "", "t5": "", "flexrefno": "", "t24": "", "fromAccount": "", "toAccount": "", "drawingName": "", "drawingPhone": "", "payeeName": "",
                    "payeePhone": "", "amount": "", "n6": "", "n7": "", "n8": "", "currency": "",
                }], "grandTotal": "", "bchargesAmt": "", "dchargesAmt": "", "echargesAmt": "", "totalCount": 0, "currentPage": 1, "pageSize": 10, "msgstatus": "",
            "msgCode": "", "msgDesc": "",
        };
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = false;
            this.checkSession();
            this.ics.confirmUpload(false);
            this._dates.fromDate = this._util.changestringtodateobject(this.today);
            this._dates.toDate = this._util.changestringtodateobject(this.today);
            this.getTransferType();
            this.getTransferStatus();
        }
    }
    FrmagentTransactionComponent.prototype.changedPager = function (event) {
        this._pgobj = event;
        this._obj.currentPage = this._pgobj.current;
        this._obj.pageSize = this._pgobj.size;
        this._obj.totalCount = this._pgobj.totalcount;
        if (this._pgobj.totalcount > 1) {
            this.goList();
        }
    };
    FrmagentTransactionComponent.prototype.date = function () {
        this._obj.fromDate = this._util.getDatePickerDate(this._dates.fromDate);
        this._obj.toDate = this._util.getDatePickerDate(this._dates.toDate);
    };
    FrmagentTransactionComponent.prototype.goList = function () {
        var _this = this;
        this._mflag = false;
        this.date();
        this._obj.sessionID = this.ics._profile.sessionID;
        this._grandAmtTotal = "0.00";
        this._BchargesAmt = "0.00";
        this._DchargesAmt = "0.00";
        this._EchargesAmt = "0.00";
        var url = this.ics._apiurl + 'service001/getAgentTransaction';
        var capValue = "";
        var json = this._obj;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data.msgCode == "0016") {
                _this._sessionMsg = data.msgDesc;
                _this.showMessage();
            }
            else {
                _this._entryhide = false;
                _this._totalcount = data.totalCount;
                _this._obj.currentPage = data.currentPage;
                _this._obj.pageSize = data.pageSize;
                _this._obj.totalCount = data.totalCount;
                if (_this._obj.fromDate > _this._obj.toDate) {
                    _this._obj.msgstatus = "From Date must not exceed To Date!";
                    _this.ics.sendBean({ "t1": "rp-msg", t2: "Information", t3: _this._obj.msgstatus });
                    _this.goClear();
                }
                else {
                    if (_this._obj.totalCount == 0) {
                        _this._obj.msgstatus = "Data not Found!";
                        _this._entryhide = true;
                        _this.ics.sendBean({ "t1": "rp-msg", t2: "Information", t3: _this._obj.msgstatus });
                    }
                }
                if (data != null) {
                    _this._list = data;
                    if (data.arr != null) {
                        if (!(data.arr instanceof Array)) {
                            var m = [];
                            m[0] = data.arr;
                            _this._list.arr = m;
                        }
                        _this._grandAmtTotal = _this._list.grandTotal;
                        _this._BchargesAmt = _this._list.bchargesAmt;
                        _this._DchargesAmt = _this._list.dchargesAmt;
                        _this._EchargesAmt = _this._list.echargesAmt;
                    }
                }
            }
            _this._mflag = true;
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
            else {
            }
        }, function () { });
    };
    FrmagentTransactionComponent.prototype.getDefaultObj = function () {
        return {
            "sessionID": "", "msgCode": "", "msgDesc": "",
            "fromDate": "", "toDate": "", "status": "", "type": "ALL", "select": "EXCEL", "totalCount": 0, "currentPage": 1, "pageSize": 10, "msgstatus": "",
        };
    };
    FrmagentTransactionComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this._router.navigate(['Login', , { p1: '*' }]);
            jQuery("#sessionalert").modal('hide');
        });
    };
    FrmagentTransactionComponent.prototype.messagealert = function () {
        var _this = this;
        this.messagehide = false;
        setTimeout(function () { return _this.messagehide = true; }, 3000);
    };
    FrmagentTransactionComponent.prototype.goClear = function () {
        this._entryhide = true;
        this._obj = this.getDefaultObj();
        this._dates.fromDate = this._util.changestringtodateobject(this.today);
        this._dates.toDate = this._util.changestringtodateobject(this.today);
    };
    FrmagentTransactionComponent.prototype.Validation = function () {
        var _this = this;
        this.date();
        try {
            this._mflag = false;
            var url = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
            this.http.doGet(url).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this._sessionMsg = data.msgDesc;
                    _this.showMessage();
                }
                else {
                    if (_this._obj.type == "") {
                        _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Please Choose Transfer Type!" });
                        _this._mflag = true;
                    }
                    else if (_this._obj.status == "") {
                        _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "Please Choose Status!" });
                        _this._mflag = true;
                    }
                    else if (_this._obj.fromDate.length == 0) {
                        _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "From Date should not blank!" });
                        _this._mflag = true;
                    }
                    else if (_this._obj.toDate.length == 0) {
                        _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "To Date should not blank!" });
                        _this._mflag = true;
                    }
                    else if (_this._obj.fromDate > _this._obj.toDate) {
                        _this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": "From Date must not exceed ToDate!" });
                        _this.goClear();
                        _this._mflag = true;
                    }
                    else {
                        _this.goDownload();
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmagentTransactionComponent.prototype.goDownload = function () {
        this.date();
        this._mflag = false;
        var templateData = "";
        var formatJSP = "";
        formatJSP = "agent-transfer.jsp";
        this.ics.sendBean({ "t1": "rp-msg", t2: "Information ", t3: "Please wait ..." });
        var url = this.ics._rpturl + formatJSP + "?fromDate=" + this._obj.fromDate + "&" + "toDate=" + this._obj.toDate + "&" + "Select=" + this._obj.select + "&status=" + this._obj.status + "&" + "type=" + this._obj.type;
        this.ics.sendBean({ "t1": "rp-popup", "t2": "Information", "t3": url });
        this._mflag = true;
    };
    FrmagentTransactionComponent.prototype.changeSelectVal = function (options) {
        this.selectedOptions = Array.apply(null, options) // convert to real array
            .filter(function (option) { return option.selected; })
            .map(function (option) { return option.value; });
        this._obj.select = this.selectedOptions[0];
    };
    FrmagentTransactionComponent.prototype.changeType = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex; //Get Selected Index
        var value = options[options.selectedIndex].value; //Get Selected Index's Value   
        if (value == "ALL" || value == "") {
            this._obj.type = "ALL";
        }
        this._obj.type = value;
        for (var i = 1; i < this.ref._lov3.refTransferType.length; i++) {
            if (this.ref._lov3.refTransferType[i].value == value) {
                this._obj.type = this.ref._lov3.refTransferType[i].value;
                break;
            }
        }
    };
    FrmagentTransactionComponent.prototype.getTransferType = function () {
        var _this = this;
        this._mflag = false;
        this.http.doGet(this.ics._apiurl + 'service001/getTransferType').subscribe(function (data) {
            _this.ref._lov3.refTransferType = data.refTransferType;
            var arr = [];
            if (_this.ref._lov3.refTransferType != null) {
                if (!(_this.ref._lov3.refTransferType instanceof Array)) {
                    var m = [];
                    m[0] = _this.ref._lov3.refTransferType;
                    _this.ref._lov3.refTransferType = m;
                }
                else {
                    for (var j = 0; j < _this.ref._lov3.refTransferType.length; j++) {
                        arr.push(_this.ref._lov3.refTransferType[j]);
                    }
                }
            }
            _this._mflag = true;
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
        }, function () { });
    };
    FrmagentTransactionComponent.prototype.changeStatus = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex; //Get Selected Index
        var value = options[options.selectedIndex].value; //Get Selected Index's Value   
        /* if (value == "ALL" || value == "") {
          this._obj.status = "ALL";
        }  */
        this._obj.status = value;
        for (var i = 1; i < this.ref._lov3.refTransferStatus.length; i++) {
            if (this.ref._lov3.refTransferStatus[i].value == value) {
                this._obj.status = this.ref._lov3.refTransferStatus[i].value;
                break;
            }
        }
    };
    FrmagentTransactionComponent.prototype.getTransferStatus = function () {
        var _this = this;
        this._mflag = false;
        this.http.doGet(this.ics._apiurl + 'service001/getTransferStatus').subscribe(function (data) {
            _this.ref._lov3.refTransferStatus = data.refTransferStatus;
            var arr = [];
            if (_this.ref._lov3.refTransferStatus != null) {
                if (!(_this.ref._lov3.refTransferStatus instanceof Array)) {
                    var m = [];
                    m[0] = _this.ref._lov3.refTransferStatus;
                    _this.ref._lov3.refTransferStatus = m;
                }
                else {
                    for (var j = 0; j < _this.ref._lov3.refTransferStatus.length; j++) {
                        arr.push(_this.ref._lov3.refTransferStatus[j]);
                    }
                }
            }
            _this._mflag = true;
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Timed Out!");
            }
        }, function () { });
    };
    FrmagentTransactionComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
            this.http.doGet(url).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this._sessionMsg = data.msgDesc;
                    _this.showMessage();
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmagentTransactionComponent = __decorate([
        core_1.Component({
            selector: 'frmagentTransaction',
            template: " \n\n<div class=\"container\">\n  <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n        <form class= \"form-horizontal\" (ngSubmit)=\"goList()\"> \n            <!-- Form Name -->\n          <legend>Agent Transfer Report</legend>\n            <div class=\"row  col-md-12\">  \n                <button class=\"btn btn-primary\" type=\"button\" (click)=\"goClear()\" >Clear</button> \n                <button class=\"btn btn-primary\" >List</button>      \n                <button class=\"btn btn-primary\" type=\"button\" (click)=\" Validation();\">Download</button>          \n            </div>\n\n            <div class=\"row col-md-12\">&nbsp;</div>\n            <div class=\"row col-md-12\" id=\"custom-form-alignment-margin\"><!--Start column -->\n                <div class=\"col-md-6\">  \n                    <div class=\"form-group\">            \n                        <label class=\"col-md-4\"> From Date </label>\n                        <div class=\"col-md-8\">\n                          <my-date-picker name=\"mydate\"  [options]=\"myDatePickerOptions\" [(ngModel)]=\"_dates.fromDate\" ngDefaultControl></my-date-picker>\n                           <!-- <input type = \"date\" class=\"form-control\"  [(ngModel)]=\"_obj.fromDate\" [ngModelOptions]=\"{standalone: true}\" required>   -->                           \n                        </div>\n                    </div>\n                    \n                    <div class=\"form-group\">            \n                        <label class=\"col-md-4\"> From Account </label>\n                            <div class=\"col-md-8\">\n                                <input type = \"text\" class=\"form-control\"  [(ngModel)]=\"_obj.fromAccount\" [ngModelOptions]=\"{standalone: true}\" >                              \n                            </div>\n                    </div>     \n            \n                    <div class=\"form-group\">\n                        <label class=\"col-md-4\">Transfer Type&nbsp; <font class=\"mandatoryfont\">*</font></label>\n                        <div class=\"col-md-8\"> \n                            <select [(ngModel)]=_obj.type [ngModelOptions]=\"{standalone: true}\" required=\"true\" (change)=\"changeType($event)\"  class=\"form-control\"  >\n                                <option *ngFor=\"let item of ref._lov3.refTransferType\" value=\"{{item.value}}\" >{{item.caption}}</option> \n                            </select>  \n                        </div>\n                    </div> \n                    \n                  <div class=\"form-group\">\n                    <label class=\"col-md-4\"></label>\n                      <div class=\"col-md-8\">\n                        <select [(ngModel)]=\"_obj.select\" [ngModelOptions]=\"{standalone: true}\" (change)=\"changeSelectVal($event.target.options)\" class=\"form-control col-md-0\" required=\"true\">\n                          <option *ngFor=\"let item of ref._lov1.ref010\" value=\"{{item.value}}\">{{item.caption}}</option> \n                        </select>                     \n                      </div>\n                  </div>\n                </div><!-- for md 6-->\n\n                <div class=\"col-md-6\">\n                    <div class=\"form-group\">\n                        <label class=\"col-md-4\"> To Date </label>\n                        <div class=\"col-md-8\">\n                           <my-date-picker name=\"mydate\" [options]=\"myDatePickerOptions\" [(ngModel)]=\"_dates.toDate\" ngDefaultControl></my-date-picker>\n                           <!-- <input type = \"date\" class=\"form-control\"  [(ngModel)]=\"_obj.toDate\" [ngModelOptions]=\"{standalone: true}\" required> -->                             \n                        </div>\n                    </div>\n\n                <div class=\"form-group\">            \n                    <label class=\"col-md-4\"> To Account <font class=\"mandatoryfont\"></font></label>\n                        <div class=\"col-md-8\">\n                            <input type = \"text\" class=\"form-control\"  [(ngModel)]=\"_obj.toAccount\" [ngModelOptions]=\"{standalone: true}\">                              \n                        </div>\n                </div>  \n\n                 <div class=\"form-group\">\n                    <label class=\"col-md-4\" > Transfer Status <font class=\"mandatoryfont\">*</font> </label>\n                      <div class=\"col-md-8\">\n                        <select [(ngModel)]=_obj.status [ngModelOptions]=\"{standalone: true}\" (change)=changeStatus($event) required=\"true\" class=\"form-control\">\n                            <option *ngFor=\"let item of ref._lov3.refTransferStatus\" value=\"{{item.value}}\" >{{item.caption}}</option> \n                        </select>                     \n                      </div>\n                </div> \n                </div>\n          </div>\n        </form> \n    </div>\n </div>\n</div>\n  \n    <div [hidden]=\"_entryhide\">\n        <div class=\"form-group\">\n            <legend></legend>    \n        </div> \n\n        <div class=\"form-group\">\n          <div class=\"col-md-9\">\n            <div style = \"margin-top : 10px\">\n                <adminpager rpPageSizeMax=\"100\" [(rpModel)]=\"_list.totalCount\" (rpChanged)=\"changedPager($event)\" ></adminpager> \n            </div>\n        </div>\n        <div class = \"col-md-3\" style=\"algin:right;\">      \n        <table style=\"align:left;font-size:14px\" cellpadding=\"10px\" cellspacing=\"10px\">\n          <tbody>\n            <colgroup>\n              <col span=\"1\" style=\"width: 50%;\">\n              <col span=\"1\" style=\"width: 50%;\">                                  \n            </colgroup>                           \n            <tr>\n              <td style=\"color:blue ; text-align:right\">\n                <label >Grand Total Amount &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>\n              </td>\n              <td style=\"color:blue;display:block; text-align:right; \">\n                <label>{{_grandAmtTotal}}</label>\n              </td>\n            </tr>\n            <tr>\n              <td style=\"color:blue; text-align:left\">\n                <label >Grand Total Bank Charges &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</label>  \n              </td>\n              <td  style=\"color:blue;display:block; text-align:right; \">\n                <label>{{_BchargesAmt}}</label>\n              </td>\n            </tr>\n            <tr>\n            <td style=\"color:blue; text-align:left\">\n              <label>Grand Total Drawing Charges &nbsp;&nbsp;:&nbsp;&nbsp;</label>  \n            </td>\n            <td  style=\"color:blue;display:block; text-align:right; \">\n              <label>{{_DchargesAmt}}</label>\n            </td>\n          </tr>\n          <tr>\n          <td style=\"color:blue; text-align:left\">\n            <label>Grand Total Encash Charges &nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;</label>  \n          </td>\n          <td  style=\"color:blue;display:block; text-align:right; \">\n            <label>{{_EchargesAmt}}</label>\n          </td>\n        </tr>\n          </tbody>\n        </table>\n      </div>\n    </div>\n        <table class=\"table table-striped table-condensed table-hover tblborder\" style=\"font-size:14px;\">\n            <thead>\n                <tr>\n                    <th align=\"center\" >Transaction Date</th>\n                    <th align=\"center\" >XRef</th>\n                    <th align=\"center\">Transaction Reference No.</th>\n                    <th align=\"center\">Mobile User ID</th>\n                    <th align=\"center\">From Account</th>\n                    <th align=\"center\">To Account</th>\n                    <th align=\"center\"><div style=\"width:150\">Drawee Name</div></th>\n                    <th align=\"center\">Drawee Phone</th>\n                    <th align=\"center\"><div style=\"width:100\">Payee Name</div></th>\n                    <th align=\"center\">Payee Phone</th>\n                    <th align=\"center\">Amount</th>\n                    <th align=\"center\">Bank Charges</th>\n                    <th align=\"center\">Drawing Charges</th>\n                    <th align=\"center\">Encash Charges</th>\n                    <th align=\"center\">Currency Code</th>\n                </tr>\n            </thead>\n\n            <tbody>\n                <tr *ngFor=\"let obj of _list.arr\">\n                    <td align=\"left\">{{obj.transdate}}</td><!--Transaction date -->\n                    <td align=\"left\">{{obj.t5}}</td>      <!-- xref-->\n                    <td align=\"left\">{{obj.flexrefno}}</td> <!-- transaction ref no.-->\n                    <td align=\"left\">{{obj.t24}}</td>       <!-- mobileuserid-->\n                    <td align=\"left\">{{obj.fromAccount}}</td>                                   \n                    <td align=\"left\">{{obj.toAccount}}</td>\n                    <td align=\"left\">{{obj.drawingName}}</td>\n                    <td align=\"left\">{{obj.drawingPhone}}</td>\n                    <td align=\"left\">{{obj.payeeName}}</td>\n                    <td align=\"left\">{{obj.payeePhone}}</td>\n                    <td align=\"right\">{{obj.amount}}</td>\n                    <td align=\"right\">{{obj.n6}}</td>   <!-- bankcharges-->\n                    <td align=\"right\">{{obj.n7}}</td>\n                    <td align=\"right\">{{obj.n8}}</td>\n                    <td align=\"right\">{{obj.currency}}</td>\n                </tr> \n            </tbody>                     \n        </table> \n    </div>              \n  <div [hidden] = \"_mflag\">\n      <div class=\"modal\" id=\"loader\"></div>\n    </div> \n    \n\n    <div [hidden]=\"_mflag\">\n       <div class=\"loader modal\" id=\"loader\"></div>\n    </div>\n   "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmagentTransactionComponent);
    return FrmagentTransactionComponent;
}());
exports.FrmagentTransactionComponent = FrmagentTransactionComponent;
//# sourceMappingURL=frmagentTransactionreport.component.js.map