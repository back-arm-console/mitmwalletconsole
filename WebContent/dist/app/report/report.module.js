"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var common_1 = require('@angular/common');
var rp_input_module_1 = require('../framework/rp-input.module');
var mydatepicker_1 = require('mydatepicker');
var rp_http_service_1 = require('../framework/rp-http.service');
var adminpager_module_1 = require('../util/adminpager.module');
var pager_module_1 = require('../util/pager.module');
var advancedsearch_module_1 = require('../util/advancedsearch.module');
var multiselect_module_1 = require('../util/multiselect.module');
var report_routing_1 = require('./report.routing');
var accounttransfer_component_1 = require('./accounttransfer.component');
var user_report_component_1 = require('./user-report.component');
var wtopup_report_component_1 = require('./wtopup-report.component');
var trialbalancereport_component_1 = require('./trialbalancereport.component');
var frmreportcontainer_component_1 = require('./frmreportcontainer.component');
var dailyreport_component_1 = require('./dailyreport.component');
var transactionreport_component_1 = require('./transactionreport.component');
var settlement_report_component_1 = require('./settlement-report.component');
var merchantTransferReport_component_1 = require('./merchantTransferReport.component');
var reportModule = (function () {
    function reportModule() {
    }
    reportModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                forms_1.FormsModule,
                rp_input_module_1.RpInputModule,
                adminpager_module_1.AdminPagerModule,
                pager_module_1.PagerModule,
                mydatepicker_1.MyDatePickerModule,
                multiselect_module_1.MultiselectModule,
                advancedsearch_module_1.AdvancedSearchModule,
                report_routing_1.ReportRouting,
            ],
            exports: [],
            declarations: [
                accounttransfer_component_1.accountTransfer,
                user_report_component_1.UserReport,
                wtopup_report_component_1.wtopupReport,
                trialbalancereport_component_1.TrialBalanceReport,
                frmreportcontainer_component_1.ReportContainerComponent,
                dailyreport_component_1.DailyReport,
                transactionreport_component_1.TransactionReport,
                settlement_report_component_1.settlementReport,
                merchantTransferReport_component_1.merchantTransferReport,
                accounttransfer_component_1.accountTransfer
            ],
            providers: [
                rp_http_service_1.RpHttpService
            ],
            schemas: [core_1.CUSTOM_ELEMENTS_SCHEMA]
        }), 
        __metadata('design:paramtypes', [])
    ], reportModule);
    return reportModule;
}());
exports.reportModule = reportModule;
//# sourceMappingURL=report.module.js.map