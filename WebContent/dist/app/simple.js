"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_references_1 = require('../framework/rp-references');
var icbs_util_component_1 = require('../util/icbs-util.component');
var rp_client_util_1 = require('../util/rp-client.util');
var DashboardComponent = (function () {
    function DashboardComponent(ics, http, ref, _router, route, l_util, icbsUtil) {
        this.ics = ics;
        this.http = http;
        this.ref = ref;
        this._router = _router;
        this.route = route;
        this.l_util = l_util;
        this.icbsUtil = icbsUtil;
        this._FilterDataset = {
            filterList: [
                { "itemid": "", "caption": "Product Type", "fieldname": "", "datatype": "", "condition": "", "t1": "", "t2": "" }
            ],
            "pageNo": 1, "pageSize": 10, "filterSource": 0, "userID": this.ics._profile.userID, "userName": this.ics._profile.userName, "branchCode": ""
        };
        this._AccListingDataset = {
            dataList: [
                {
                    "accountNumber": "", "customerName": "", "customerID": "", "productCode": "", "productType": "", "accountType": "", "currencyCode": "", "currentBalance": "", "lastTransDate": "", "status": "", "zoneCode": "",
                    "openingDate": "", "BranchCode": ""
                }
            ],
            "pageNo": 0, "pageSize": 0, "totalRecords": "1", "userID": "", "userName": ""
        };
        this.custPieBarData = [];
        this.custCategory = [];
        this.custLineData = [];
        this.totAmtPieBarData = [];
        this.totAmtCategory = [];
        this.totAmtLineData = [];
        this.totTransPieBarData = [];
        this.totTransCategory = [];
        this.totTransLineData = [];
        this.totAmtByMonthPieBarData = [];
        this.totAmtByMonthCategory = [];
        this.totAmtByMonthLineData = [];
        this.month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        this.series = [];
        this._TopCombo = [];
        this._TypeCombo = [];
        this._customer = [];
        this._PendingBy = [];
        this._ProductList = [];
        this._Pending = [];
        this._totAmount = [];
        this._totCustomer = [];
        this._borByBranch = [];
        this._tranAmount = [];
        this._PendingByTop = [];
        this.pending = false;
        this.borrow = false;
        this._Chart = [];
        this._awaitAction = [];
        this.await = false;
        this.page = 0;
        this._yearList = [];
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['Login', , { p1: '*' }]);
        }
        this.getTop();
        this.getType();
        this.getChart();
    }
    DashboardComponent.prototype.ngOnInit = function () {
        this.getYear();
        this.getTotalAmount();
        this.getPendingApproved();
        this.getBorrowByMonth();
        this.getBorrowByBranch();
        this.getTransactionByMonth();
    };
    DashboardComponent.prototype.alertMessage = function (message, type) {
        this.ics.sendBean({ "t1": "rp-message", "t2": type, "t3": message });
    };
    DashboardComponent.prototype.clearDialog = function () {
        jQuery("#custPieDialog").css("display", "none");
        jQuery("#totAmtPieDialog").css("display", "none");
        jQuery("#custBarDialog").css("display", "none");
        jQuery("#totAmtBarDialog").css("display", "none");
        jQuery("#custLineDialog").css("display", "none");
        jQuery("#totAmtLineDialog").css("display", "none");
        jQuery("#totTransPieDialog").css("display", "none");
        jQuery("#totAmtByMonthPieDialog").css("display", "none");
        jQuery("#totTransBarDialog").css("display", "none");
        jQuery("#totAmtByMonthBarDialog").css("display", "none");
        jQuery("#totTransLineDialog").css("display", "none");
        jQuery("#totAmtByMonthLineDialog").css("display", "none");
    };
    DashboardComponent.prototype.showDashboardDialog = function (value) {
        this.clearDialog();
        if (value == "custPie")
            this.showCustPieDialog();
        else if (value == "custBar")
            this.showCustBarDialog();
        else if (value == "custLine")
            this.showCustLineDialog();
        else if (value == "totAmtPie")
            this.showTotAmtPieDialog();
        else if (value == "totAmtBar")
            this.showTotAmtBarDialog();
        else if (value == "totAmtLine")
            this.showTotAmtLineDialog();
        else if (value == "totTransPie")
            this.showTotTransPieDialog();
        else if (value == "totTransBar")
            this.showTotTransBarDialog();
        else if (value == "totTransLine")
            this.showTotTransLineDialog();
        else if (value == "totAmtByMonthPie")
            this.showTotAmtByMonthPieDialog();
        else if (value == "totAmtByMonthBar")
            this.showTotAmtByMonthBarDialog();
        else if (value == "totAmtByMonthLine")
            this.showTotAmtByMonthLineDialog();
        setTimeout(function () {
            jQuery("#divDashboardDialog").modal();
        }, 50);
    };
    DashboardComponent.prototype.getChart = function () {
        this._Chart =
            [
                { "code": 1, "description": "Pie" },
                { "code": 2, "description": "Bar" },
                { "code": 3, "description": "Line" }
            ];
    };
    DashboardComponent.prototype.getTop = function () {
        this._TopCombo =
            [
                { "value": 10, "caption": "Top 10" },
                { "value": 20, "caption": "Top 20" },
                { "value": 50, "caption": "Top 50" }
            ];
        this._top = this._TopCombo[0].value;
    };
    DashboardComponent.prototype.getType = function () {
        this._TypeCombo =
            [
                { "value": 40, "caption": "Approved" },
                { "value": 41, "caption": "Contract" },
                { "value": 101, "caption": "Disburse" }
            ];
        this._type = this._TypeCombo[0].value;
    };
    DashboardComponent.prototype.formatAmount = function (amt) {
        if (amt == "")
            return "0.00";
        else
            return this.l_util.thousand_sperator(parseFloat(amt.replace(/,/g, '')).toFixed(2));
    };
    DashboardComponent.prototype.clearCust = function () {
        jQuery("#custPie").css("display", "none");
        jQuery("#custBar").css("display", "none");
        jQuery("#custLine").css("display", "none");
    };
    DashboardComponent.prototype.showCustPie = function () {
        jQuery("#custPie").css("display", "block");
        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart('custPie', {
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            title: { text: 'Customer Count by Loan Product' },
            tooltip: { pointFormat: '<b>{point.y:,.0f}</b>' },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    depth: 35,
                    cursor: 'pointer',
                    events: {
                        click: function (event) {
                        },
                    },
                    dataLabels: {
                        enabled: false,
                        format: '{point.name}',
                        style: { color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black' }
                    },
                }
            },
            credits: { enabled: false },
            series: [{ name: 'Account', colorByPoint: true, data: this.custPieBarData }]
        });
    };
    DashboardComponent.prototype.showCustBar = function () {
        jQuery("#custBar").css("display", "block");
        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart('custBar', {
            chart: { type: 'column' },
            title: { text: 'Customer Count by Loan Product' },
            accessibility: {
                announceNewData: {
                    enabled: true
                }
            },
            xAxis: { type: 'category' },
            yAxis: {
                title: { text: '' }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: false,
                        format: '{point.y:,.2f}'
                    }
                }
            },
            tooltip: { pointFormat: '<b>{point.y:,.0f}</b><br/>' },
            credits: { enabled: false },
            series: [{ name: 'Account', colorByPoint: true, data: this.custPieBarData }]
        });
    };
    DashboardComponent.prototype.goClear = function () {
        jQuery("#piechartDialog").css("display", "none");
        jQuery("#barchartDialog").css("display", "none");
        jQuery("#linechartTranDialog").css("display", "none");
        jQuery("#linechartDialog").css("display", "none");
    };
    /*   gotoPieChart()
      {
          jQuery("#piechart").css("display", "block");
  
          Highcharts.setOptions({
              lang: {
                thousandsSep: ','
            }
          })
  
          Highcharts.chart('piechart',
          {
              chart: {
                  type: 'pie',
                  options3d: {
                      enabled: true,
                      alpha: 45,
                      beta: 0
                  }
              },
              title : { text : 'Customer Count by Loan Product' },
              tooltip : { pointFormat : '<b>{point.y:,.0f}</b>' },
              plotOptions : {
                  pie : {
                      allowPointSelect : true,
                      depth: 35,
                      cursor : 'pointer',
                      events: {
                          click: (event) => {
                              
                          },
                      },
                      dataLabels : {
                          enabled : false,
                          format : '{point.name}',
                          style : { color : (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black' }
                      },
                      // showInLegend : true
                  }
              },
              credits : { enabled : false },
              series : [{ name : 'Account', colorByPoint : true, data : this.data }]
          });
      }
       */
    DashboardComponent.prototype.showCustLine = function () {
        jQuery("#custLine").css("display", "block");
        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart('custLine', {
            title: {
                text: 'Customer Count by Loan Product'
            },
            yAxis: {
                title: { text: '' }
            },
            xAxis: {
                categories: this.custCategory,
                crosshair: true
            },
            credits: { enabled: false },
            tooltip: { pointFormat: '<b>{point.y:,.0f}</b>' },
            series: [{
                    name: 'Total Customer',
                    data: this.custLineData
                }],
            responsive: {
                rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
            }
        });
    };
    /* gotoPieChartDialog()
    {
        jQuery("#piechartDialog").css("display", "block");
        jQuery("#barchartDialog").css("display", "none");
        jQuery("#linechartTranDialog").css("display", "none");
        jQuery("#linechartDialog").css("display", "none");

        Highcharts.setOptions({
            lang: {
              thousandsSep: ','
          }
        })

        Highcharts.chart('piechartDialog',
        {
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            title : { text : 'Customer Count by Loan Product' },
            tooltip : { pointFormat : '<b>{point.y:,.0f}</b>' },
            plotOptions : {
                pie : {
                    allowPointSelect : true,
                    depth: 35,
                    cursor : 'pointer',
                    events: {
                        click: (event) => {
                            
                        },
                    },
                    dataLabels : {
                        enabled : true,
                        format : '{point.name}',
                        style : { color : (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black' }
                    },
                    showInLegend : true
                }
            },
            credits : { enabled : false },
            series : [{ name : 'Account', colorByPoint : true, data :   this.data }]
        });
    } */
    /*  gotoBarChart()
     {
         jQuery("#barchart").css("display", "block");
 
         Highcharts.setOptions({
             lang: {
               thousandsSep: ','
           }
         })
 
         Highcharts.chart('barchart',
         {
             chart: {
                 type: 'column'
             },
             title: {
                 text: 'Total Amount'
             },
             accessibility: {
                 announceNewData: {
                     enabled: true
                 }
             },
             xAxis: {
                 type: 'category'
             },
             yAxis: {
                 title: {
                     text: ''
                 }
             },
             legend: {
                 enabled: false
             },
             plotOptions: {
                 series: {
                     borderWidth: 0,
                     dataLabels: {
                         enabled: false,
                         format: '{point.y:,.2f}'
                     }
                 }
             },
             tooltip: {
                 pointFormat: '<b>{point.y:,.2f}</b><br/>'
             },
             credits : { enabled : false },
             series : [{ name : 'Account', colorByPoint : true, data :   this.bardata }]
         });
     } */
    DashboardComponent.prototype.showCustPieDialog = function () {
        jQuery("#custPieDialog").css("display", "block");
        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart('custPieDialog', {
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            title: { text: 'Customer Count by Loan Product' },
            tooltip: { pointFormat: '<b>{point.y:,.0f}</b>' },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    depth: 35,
                    cursor: 'pointer',
                    events: {
                        click: function (event) {
                        },
                    },
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}',
                        style: { color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black' }
                    },
                    showInLegend: true
                }
            },
            credits: { enabled: false },
            series: [{ name: 'Account', colorByPoint: true, data: this.custPieBarData }]
        });
    };
    DashboardComponent.prototype.showCustBarDialog = function () {
        jQuery("#custBarDialog").css("display", "block");
        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart('custBarDialog', {
            chart: { type: 'column' },
            title: { text: 'Customer Count by Loan Product' },
            accessibility: {
                announceNewData: {
                    enabled: true
                }
            },
            xAxis: { type: 'category' },
            yAxis: {
                title: { text: '' }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: false,
                        format: '{point.y:,.2f}'
                    },
                    showInLegend: true
                }
            },
            tooltip: { pointFormat: '<b>{point.y:,.0f}</b><br/>' },
            credits: { enabled: false },
            series: [{ name: 'Account', colorByPoint: true, data: this.custPieBarData }]
        });
    };
    DashboardComponent.prototype.showCustLineDialog = function () {
        jQuery("#custLineDialog").css("display", "block");
        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart('custLineDialog', {
            title: {
                text: 'Customer Count by Loan Product'
            },
            yAxis: {
                title: { text: '' }
            },
            xAxis: {
                categories: this.custCategory,
                crosshair: true
            },
            credits: { enabled: false },
            tooltip: { pointFormat: '<b>{point.y:,.0f}</b>' },
            series: [{
                    name: 'Total Customer',
                    data: this.custLineData
                }],
            responsive: {
                rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
            }
        });
    };
    DashboardComponent.prototype.clearTotAmt = function () {
        jQuery("#totAmtPie").css("display", "none");
        jQuery("#totAmtBar").css("display", "none");
        jQuery("#totAmtLine").css("display", "none");
    };
    DashboardComponent.prototype.showTotAmtPie = function () {
        jQuery("#totAmtPie").css("display", "block");
        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart('totAmtPie', {
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            title: { text: 'Total Amount' },
            tooltip: { pointFormat: '<b>{point.y:,.2f}</b>' },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    depth: 35,
                    cursor: 'pointer',
                    events: {
                        click: function (event) {
                        },
                    },
                    dataLabels: {
                        enabled: false,
                        format: '{point.name}',
                        style: { color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black' }
                    },
                }
            },
            credits: { enabled: false },
            series: [{ name: 'Account', colorByPoint: true, data: this.totAmtPieBarData }]
        });
    };
    DashboardComponent.prototype.showTotAmtBar = function () {
        jQuery("#totAmtBar").css("display", "block");
        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart('totAmtBar', {
            chart: { type: 'column' },
            title: { text: 'Total Amount' },
            accessibility: {
                announceNewData: {
                    enabled: true
                }
            },
            xAxis: { type: 'category' },
            yAxis: {
                title: { text: '' }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: false,
                        format: '{point.y:,.2f}'
                    }
                }
            },
            tooltip: { pointFormat: '<b>{point.y:,.2f}</b><br/>' },
            credits: { enabled: false },
            series: [{ name: 'Account', colorByPoint: true, data: this.totAmtPieBarData }]
        });
    };
    /*   gotoBarChartDialog()
      {
          jQuery("#barchartDialog").css("display", "block");
          jQuery("#piechartDialog").css("display", "none");
          jQuery("#linechartTranDialog").css("display", "none");
          jQuery("#linechartDialog").css("display", "none");
  
          Highcharts.setOptions({
              lang: {
                thousandsSep: ','
            }
          })
  
          Highcharts.chart('barchartDialog',
          {
              chart: {
                  type: 'column'
              },
              title: {
                  text: 'Total Amount'
              },
              accessibility: {
                  announceNewData: {
                      enabled: true
                  }
              },
              xAxis: {
                  type: 'category'
              },
              yAxis: {
                  title: {
                      text: ''
                  }
              },
              legend: {
                  enabled: false
              },
              plotOptions: {
                  series: {
                      borderWidth: 0,
                      dataLabels: {
                          enabled: false,
                          format: '{point.y:,.2f}'
                      },
                      showInLegend : true
                  }
              },
              tooltip: {
                  pointFormat: '<b>{point.y:,.2f}</b><br/>'
              },
              credits : { enabled : false },
              series : [{ name : 'Account', colorByPoint : true, data :   this.bardata }]
          });
      } */
    DashboardComponent.prototype.gotoLineChart = function () {
        jQuery("#linechart").css("display", "block");
        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart('linechart', {
            title: {
                text: 'Total Amount By Month'
            },
            yAxis: {
                title: { text: '' }
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                crosshair: true
            },
            credits: { enabled: false },
            series: [{
                    name: 'Total Amount',
                    data: this._totAmount
                }],
            responsive: {
                rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
            }
        });
    };
    DashboardComponent.prototype.gotoLineChartDialog = function () {
        jQuery("#linechartDialog").css("display", "block");
        jQuery("#piechartDialog").css("display", "none");
        jQuery("#barchartDialog").css("display", "none");
        jQuery("#linechartTranDialog").css("display", "none");
        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart('linechartDialog', {
            title: {
                text: 'Total Amount By Month'
            },
            yAxis: {
                title: { text: '' }
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                crosshair: true
            },
            credits: { enabled: false },
            series: [{
                    name: 'Total Amount',
                    data: this._totAmount
                }],
            responsive: {
                rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
            }
        });
    };
    DashboardComponent.prototype.showTotAmtLine = function () {
        jQuery("#totAmtLine").css("display", "block");
        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart('totAmtLine', {
            title: {
                text: 'Total Amount'
            },
            yAxis: {
                title: { text: '' }
            },
            xAxis: {
                categories: this.totAmtCategory,
                crosshair: true
            },
            credits: { enabled: false },
            tooltip: { pointFormat: '<b>{point.y:,.2f}</b>' },
            series: [{
                    name: 'Total Amount',
                    data: this.totAmtLineData
                }],
            responsive: {
                rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
            }
        });
    };
    DashboardComponent.prototype.showTotAmtPieDialog = function () {
        jQuery("#totAmtPieDialog").css("display", "block");
        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart('totAmtPieDialog', {
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            title: { text: 'Total Amount' },
            tooltip: { pointFormat: '<b>{point.y:,.2f}</b>' },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    depth: 35,
                    cursor: 'pointer',
                    events: {
                        click: function (event) {
                        },
                    },
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}',
                        style: { color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black' }
                    },
                    showInLegend: true
                }
            },
            credits: { enabled: false },
            series: [{ name: 'Account', colorByPoint: true, data: this.totAmtPieBarData }]
        });
    };
    DashboardComponent.prototype.showTotAmtBarDialog = function () {
        jQuery("#totAmtBarDialog").css("display", "block");
        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart('totAmtBarDialog', {
            chart: { type: 'column' },
            title: { text: 'Total Amount' },
            accessibility: {
                announceNewData: {
                    enabled: true
                }
            },
            xAxis: { type: 'category' },
            yAxis: {
                title: { text: '' }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: false,
                        format: '{point.y:,.2f}'
                    },
                    showInLegend: true
                }
            },
            tooltip: { pointFormat: '<b>{point.y:,.2f}</b><br/>' },
            credits: { enabled: false },
            series: [{ name: 'Account', colorByPoint: true, data: this.totAmtPieBarData }]
        });
    };
    DashboardComponent.prototype.gotoLineChartTran = function () {
        jQuery("#linechartTran").css("display", "block");
        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart('linechartTran', {
            title: {
                text: 'Total Transaction By Month'
            },
            yAxis: {
                title: { text: '' }
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                crosshair: true
            },
            credits: { enabled: false },
            series: [{
                    name: 'Transaction Amount',
                    data: this._tranAmount
                }],
            responsive: {
                rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
            }
        });
    };
    DashboardComponent.prototype.gotoLineChartTranDialog = function () {
        jQuery("#linechartTranDialog").css("display", "block");
        jQuery("#piechartDialog").css("display", "none");
        jQuery("#barchartDialog").css("display", "none");
        jQuery("#linechartDialog").css("display", "none");
        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart('linechartTranDialog', {
            title: {
                text: 'Total Transaction By Month'
            },
            yAxis: {
                title: { text: '' }
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                crosshair: true
            },
            credits: { enabled: false },
            series: [{
                    name: 'Transaction Amount',
                    data: this._tranAmount
                }],
            responsive: {
                rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
            }
        });
    };
    DashboardComponent.prototype.showTotAmtLineDialog = function () {
        jQuery("#totAmtLineDialog").css("display", "block");
        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart('totAmtLineDialog', {
            title: {
                text: 'Total Amount'
            },
            yAxis: {
                title: { text: '' }
            },
            xAxis: {
                categories: this.totAmtCategory,
                crosshair: true
            },
            credits: { enabled: false },
            tooltip: { pointFormat: '<b>{point.y:,.2f}</b>' },
            series: [{
                    name: 'Total Amount',
                    data: this.totAmtLineData
                }],
            responsive: {
                rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
            }
        });
    };
    DashboardComponent.prototype.clearTotTrans = function () {
        jQuery("#totTransPie").css("display", "none");
        jQuery("#totTransBar").css("display", "none");
        jQuery("#totTransLine").css("display", "none");
    };
    DashboardComponent.prototype.showTotTransPie = function () {
        jQuery("#totTransPie").css("display", "block");
        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart('totTransPie', {
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            title: { text: 'Total Transaction By Month' },
            tooltip: { pointFormat: '<b>{point.y:,.2f}</b>' },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    depth: 35,
                    cursor: 'pointer',
                    events: {
                        click: function (event) {
                        },
                    },
                    dataLabels: {
                        enabled: false,
                        format: '{point.name}',
                        style: { color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black' }
                    },
                }
            },
            credits: { enabled: false },
            series: [{ name: 'Account', colorByPoint: true, data: this.totTransPieBarData }]
        });
    };
    DashboardComponent.prototype.showTotTransBar = function () {
        jQuery("#totTransBar").css("display", "block");
        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart('totTransBar', {
            chart: { type: 'column' },
            title: { text: 'Total Transaction By Month' },
            accessibility: {
                announceNewData: {
                    enabled: true
                }
            },
            xAxis: { type: 'category' },
            yAxis: {
                title: { text: '' }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: false,
                        format: '{point.y:,.2f}'
                    }
                }
            },
            tooltip: { pointFormat: '<b>{point.y:,.2f}</b><br/>' },
            credits: { enabled: false },
            series: [{ name: 'Account', colorByPoint: true, data: this.totTransPieBarData }]
        });
    };
    DashboardComponent.prototype.showTotTransLine = function () {
        jQuery("#totTransLine").css("display", "block");
        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart('totTransLine', {
            title: {
                text: 'Total Transaction By Month'
            },
            yAxis: {
                title: { text: '' }
            },
            xAxis: {
                categories: this.totTransCategory,
                crosshair: true
            },
            credits: { enabled: false },
            tooltip: { pointFormat: '<b>{point.y:,.2f}</b>' },
            series: [{
                    name: 'Total Amount',
                    data: this.totTransLineData
                }],
            responsive: {
                rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
            }
        });
    };
    DashboardComponent.prototype.showTotTransPieDialog = function () {
        jQuery("#totTransPieDialog").css("display", "block");
        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart('totTransPieDialog', {
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            title: { text: 'Total Transaction By Month' },
            tooltip: { pointFormat: '<b>{point.y:,.2f}</b>' },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    depth: 35,
                    cursor: 'pointer',
                    events: {
                        click: function (event) {
                        },
                    },
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}',
                        style: { color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black' }
                    },
                    showInLegend: true
                }
            },
            credits: { enabled: false },
            series: [{ name: 'Account', colorByPoint: true, data: this.totTransPieBarData }]
        });
    };
    DashboardComponent.prototype.showTotTransBarDialog = function () {
        jQuery("#totTransBarDialog").css("display", "block");
        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart('totTransBarDialog', {
            chart: { type: 'column' },
            title: { text: 'Total Transaction By Month' },
            accessibility: {
                announceNewData: {
                    enabled: true
                }
            },
            xAxis: { type: 'category' },
            yAxis: {
                title: { text: '' }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: false,
                        format: '{point.y:,.2f}'
                    },
                    showInLegend: true
                }
            },
            tooltip: { pointFormat: '<b>{point.y:,.2f}</b><br/>' },
            credits: { enabled: false },
            series: [{ name: 'Account', colorByPoint: true, data: this.totTransPieBarData }]
        });
    };
    DashboardComponent.prototype.showTotTransLineDialog = function () {
        jQuery("#totTransLineDialog").css("display", "block");
        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart('totTransLineDialog', {
            title: {
                text: 'Total Transaction By Month'
            },
            yAxis: {
                title: { text: '' }
            },
            xAxis: {
                categories: this.totTransCategory,
                crosshair: true
            },
            credits: { enabled: false },
            tooltip: { pointFormat: '<b>{point.y:,.2f}</b>' },
            series: [{
                    name: 'Total Amount',
                    data: this.totTransLineData
                }],
            responsive: {
                rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
            }
        });
    };
    DashboardComponent.prototype.clearTotAmtByMonth = function () {
        jQuery("#totAmtByMonthPie").css("display", "none");
        jQuery("#totAmtByMonthBar").css("display", "none");
        jQuery("#totAmtByMonthLine").css("display", "none");
    };
    DashboardComponent.prototype.showTotAmtByMonthPie = function () {
        jQuery("#totAmtByMonthPie").css("display", "block");
        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart('totAmtByMonthPie', {
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            title: { text: 'Total Amount By Month' },
            tooltip: { pointFormat: '<b>{point.y:,.2f}</b>' },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    depth: 35,
                    cursor: 'pointer',
                    events: {
                        click: function (event) {
                        },
                    },
                    dataLabels: {
                        enabled: false,
                        format: '{point.name}',
                        style: { color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black' }
                    },
                }
            },
            credits: { enabled: false },
            series: [{ name: 'Account', colorByPoint: true, data: this.totAmtByMonthPieBarData }]
        });
    };
    DashboardComponent.prototype.showTotAmtByMonthBar = function () {
        jQuery("#totAmtByMonthBar").css("display", "block");
        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart('totAmtByMonthBar', {
            chart: { type: 'column' },
            title: { text: 'Total Amount By Month' },
            accessibility: {
                announceNewData: {
                    enabled: true
                }
            },
            xAxis: { type: 'category' },
            yAxis: {
                title: { text: '' }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: false,
                        format: '{point.y:,.2f}'
                    }
                }
            },
            tooltip: { pointFormat: '<b>{point.y:,.2f}</b><br/>' },
            credits: { enabled: false },
            series: [{ name: 'Account', colorByPoint: true, data: this.totAmtByMonthPieBarData }]
        });
    };
    DashboardComponent.prototype.showTotAmtByMonthLine = function () {
        jQuery("#totAmtByMonthLine").css("display", "block");
        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart('totAmtByMonthLine', {
            title: {
                text: 'Total Amount By Month'
            },
            yAxis: {
                title: { text: '' }
            },
            xAxis: {
                categories: this.totAmtByMonthCategory,
                crosshair: true
            },
            credits: { enabled: false },
            tooltip: { pointFormat: '<b>{point.y:,.2f}</b>' },
            series: [{
                    name: 'Total Amount',
                    data: this.totAmtByMonthLineData
                }],
            responsive: {
                rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
            }
        });
    };
    DashboardComponent.prototype.showTotAmtByMonthPieDialog = function () {
        jQuery("#totAmtByMonthPieDialog").css("display", "block");
        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart('totAmtByMonthPieDialog', {
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            title: { text: 'Total Amount By Month' },
            tooltip: { pointFormat: '<b>{point.y:,.2f}</b>' },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    depth: 35,
                    cursor: 'pointer',
                    events: {
                        click: function (event) {
                        },
                    },
                    dataLabels: {
                        enabled: false,
                        format: '{point.name}',
                        style: { color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black' }
                    },
                }
            },
            credits: { enabled: false },
            series: [{ name: 'Account', colorByPoint: true, data: this.totAmtByMonthPieBarData }]
        });
    };
    DashboardComponent.prototype.showTotAmtByMonthBarDialog = function () {
        jQuery("#totAmtByMonthBarDialog").css("display", "block");
        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart('totAmtByMonthBarDialog', {
            chart: { type: 'column' },
            title: { text: 'Total Amount By Month' },
            accessibility: {
                announceNewData: {
                    enabled: true
                }
            },
            xAxis: { type: 'category' },
            yAxis: {
                title: { text: '' }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: false,
                        format: '{point.y:,.2f}'
                    }
                }
            },
            tooltip: { pointFormat: '<b>{point.y:,.2f}</b><br/>' },
            credits: { enabled: false },
            series: [{ name: 'Account', colorByPoint: true, data: this.totAmtByMonthPieBarData }]
        });
    };
    DashboardComponent.prototype.showTotAmtByMonthLineDialog = function () {
        jQuery("#totAmtByMonthLineDialog").css("display", "block");
        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart('totAmtByMonthLineDialog', {
            title: {
                text: 'Total Amount By Month'
            },
            yAxis: {
                title: { text: '' }
            },
            xAxis: {
                categories: this.totAmtByMonthCategory,
                crosshair: true
            },
            credits: { enabled: false },
            tooltip: { pointFormat: '<b>{point.y:,.2f}</b>' },
            series: [{
                    name: 'Total Amount',
                    data: this.totAmtByMonthLineData
                }],
            responsive: {
                rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
            }
        });
    };
    DashboardComponent.prototype.getYear = function () {
        var _this = this;
        var year = [];
        var url = this.ics._apiurl + "serviceDBS/getAllYear";
        this.http.doGet(url).subscribe(function (response) {
            if (response != undefined && response != null) {
                year = _this.l_util.convertToArray(response.yearList);
                _this._yearList = year;
                _this._totCustYear = _this._yearList[0].yearCode;
                _this._totAmtYear = _this._yearList[0].yearCode;
                _this._totAmtByBranchYear = _this._yearList[0].yearCode;
                _this._totTranByMonthYear = _this._yearList[0].yearCode;
                _this._totAmtByMonthYear = _this._yearList[0].yearCode;
                _this.getTotalCustomer();
                _this.getTotalAmount();
                _this.getBorrowByBranch();
                _this.getTransactionByMonth();
                _this.getBorrowByMonth();
            }
        });
    };
    DashboardComponent.prototype.changeYear = function (aIndex, id) {
        if (id == '1') {
            this._totCustYear = this._yearList[aIndex].yearCode;
            this.getTotalCustomer();
        }
        else if (id == '2') {
            this._totAmtYear = this._yearList[aIndex].yearCode;
            this.getTotalAmount();
        }
        else if (id == '3') {
            this._totAmtByBranchYear = this._yearList[aIndex].yearCode;
            this.getBorrowByBranch();
        }
        else if (id == '4') {
            this._totTranByMonthYear = this._yearList[aIndex].yearCode;
            this.getTransactionByMonth();
        }
        else if (id == '5') {
            this._totAmtByMonthYear = this._yearList[aIndex].yearCode;
            this.getBorrowByMonth();
        }
    };
    DashboardComponent.prototype.getTotalCustomer = function () {
        var _this = this;
        var totalCustomer = [];
        var url = this.ics._apiurl + "serviceDBS/getTotalCustomer?year=" + this._totCustYear;
        this.http.doGet(url).subscribe(function (response) {
            if (response != undefined && response != null) {
                totalCustomer = _this.l_util.convertToArray(response.pendingStatus);
                _this._Pending = totalCustomer;
                _this.await = true;
                _this._Status = _this._Pending[0].code;
                for (var i = 0; i < totalCustomer.length; i++) {
                    _this.custPieBarData.push({ name: totalCustomer[i].description, y: parseFloat(totalCustomer[i].noCustomer) });
                    _this.custCategory.push(totalCustomer[i].description);
                    _this.custLineData.push(parseFloat(totalCustomer[i].noCustomer));
                }
            }
            _this.changeCustChart(0);
        });
    };
    /*  getBarChatData()
     {
         let totalAmount = [];
         let url = this.ics._apiurl + "serviceDBS/getTotalAmountFromAccByStatus";
         this.http.doGet(url).subscribe(response => {
             if(response != undefined && response != null){
                 totalAmount = this.l_util.convertToArray(response.totalAccountAmount);
 
                 for(let i = 0; i < totalAmount.length; i++)
                 {
                     this.bardata.push({name:totalAmount[i].description, y:Math.abs(parseFloat(totalAmount[i].amount))})
                 }
                 this.gotoBarChart();
             }
         });
     } */
    DashboardComponent.prototype.getTotalAmount = function () {
        var _this = this;
        var totalAmount = [];
        var url = this.ics._apiurl + "serviceDBS/getTotalAmountFromAccByStatus?year=" + this._totAmtYear;
        this.http.doGet(url).subscribe(function (response) {
            if (response != undefined && response != null) {
                totalAmount = _this.l_util.convertToArray(response.totalAccountAmount);
                for (var i = 0; i < totalAmount.length; i++) {
                    _this.totAmtPieBarData.push({ name: totalAmount[i].description, y: Math.abs(parseFloat(totalAmount[i].amount)) });
                    _this.totAmtCategory.push(totalAmount[i].description);
                    _this.totAmtLineData.push(Math.abs(parseFloat(totalAmount[i].amount)));
                }
                _this.changeTotAmtChart(1);
            }
        });
    };
    DashboardComponent.prototype.getPendingApproved = function () {
        var _this = this;
        var Approved = [];
        var url = this.ics._apiurl + "serviceDBS/getPendingApproved";
        this.http.doGet(url).subscribe(function (response) {
            if (response != undefined && response != null) {
                Approved = _this.l_util.convertToArray(response.pendingStatus);
                for (var i = 0; i < Approved.length; i++) {
                    var a = 0;
                    for (var j = 0; j < _this._Pending.length; j++) {
                        if (Approved[i].code == _this._Pending[j].code) {
                            _this._Pending[j].noContract = Approved[i].noContract;
                            a = 1;
                        }
                    }
                    if (a == 0) {
                        _this._Pending.push(Approved[i]);
                    }
                }
                _this._Status = _this._Pending[0].code;
            }
            _this._awaitAction = _this._Pending.slice(0, 2);
            _this.getPendingContract();
            _this.getPendingDisburse();
        });
    };
    DashboardComponent.prototype.getPendingContract = function () {
        var _this = this;
        var Contract = [];
        var url = this.ics._apiurl + "serviceDBS/getPendingContract";
        this.http.doGet(url).subscribe(function (response) {
            if (response != undefined && response != null) {
                Contract = _this.l_util.convertToArray(response.pendingStatus);
                for (var i = 0; i < Contract.length; i++) {
                    var a = 0;
                    for (var j = 0; j < _this._Pending.length; j++) {
                        if (Contract[i].code == _this._Pending[j].code) {
                            _this._Pending[j].noContract = Contract[i].noContract;
                            a = 1;
                        }
                    }
                    if (a == 0) {
                        _this._Pending.push(Contract[i]);
                    }
                }
                _this._Status = _this._Pending[0].code;
            }
            _this._awaitAction = _this._Pending.slice(0, 2);
            _this.getPendingApprovedBy();
        });
    };
    DashboardComponent.prototype.getPendingDisburse = function () {
        var _this = this;
        var Disburse = [];
        var url = this.ics._apiurl + "serviceDBS/getPendingDisburse";
        this.http.doGet(url).subscribe(function (response) {
            if (response != undefined && response != null) {
                Disburse = _this.l_util.convertToArray(response.pendingStatus);
                for (var i = 0; i < Disburse.length; i++) {
                    var a = 0;
                    for (var j = 0; j < _this._Pending.length; j++) {
                        if (Disburse[i].code == _this._Pending[j].code) {
                            _this._Pending[j].noDisburse = Disburse[i].noDisburse;
                            a = 1;
                        }
                    }
                    if (a == 0) {
                        _this._Pending.push(Disburse[i]);
                    }
                }
                _this._Status = _this._Pending[0].code;
            }
            _this._awaitAction = _this._Pending.slice(0, 2);
            _this.getPendingApprovedBy();
        });
    };
    DashboardComponent.prototype.getBorrowByMonth = function () {
        var _this = this;
        var borrow = [];
        this.totAmtByMonthCategory = [];
        var url = this.ics._apiurl + "serviceDBS/getBorrowByMonth?year=" + this._totAmtByMonthYear;
        this.http.doGet(url).subscribe(function (response) {
            if (response != undefined && response != null) {
                borrow = _this.l_util.convertToArray(response.borrowByMonth);
                for (var i = 0; i < borrow.length; i++) {
                    var a = Math.abs(borrow[i].month) - 1;
                    _this.totAmtByMonthPieBarData.push({ name: _this.month[a], y: Math.abs(parseFloat(borrow[i].amount)) });
                    _this.totAmtByMonthCategory.push(_this.month[a]);
                    _this.totAmtByMonthLineData.push(parseFloat(borrow[i].amount));
                }
            }
            _this.changeTotAmtByMonthChart(2);
        });
    };
    DashboardComponent.prototype.getBorrowByBranch = function () {
        var _this = this;
        this._borByBranch = [];
        this.borrow = false;
        var borrow = [];
        var url = this.ics._apiurl + "serviceDBS/getBorrowByBranch?year=" + this._totAmtByBranchYear;
        this.http.doGet(url).subscribe(function (response) {
            if (response != undefined && response != null) {
                borrow = _this.l_util.convertToArray(response.borrowByBranch);
                _this._borByBranch = borrow;
                _this.borrow = true;
            }
        });
    };
    DashboardComponent.prototype.getTransactionByMonth = function () {
        var _this = this;
        var transaction = [];
        this.totTransCategory = [];
        var url = this.ics._apiurl + "serviceDBS/getTransactionByMonth?year=" + this._totTranByMonthYear;
        this.http.doGet(url).subscribe(function (response) {
            if (response != undefined && response != null) {
                transaction = _this.l_util.convertToArray(response.borrowByMonth);
                for (var i = 0; i < transaction.length; i++) {
                    var a = Math.abs(transaction[i].month) - 1;
                    _this.totTransPieBarData.push({ name: _this.month[a], y: Math.abs(parseFloat(transaction[i].amount)) });
                    _this.totTransCategory.push(_this.month[a]);
                    _this.totTransLineData.push(parseFloat(transaction[i].amount));
                }
            }
            _this.changeTotTransChart(2);
        });
    };
    DashboardComponent.prototype.getPendingApprovedBy = function () {
        var _this = this;
        var pending = [];
        this._PendingBy = [];
        var url = this.ics._apiurl + "serviceDBS/getPendingApprovedBy?status=40&ref=" + this._Status;
        this.http.doGet(url).subscribe(function (response) {
            if (response != undefined && response != null) {
                pending = _this.l_util.convertToArray(response.pendingLoan);
                _this._PendingBy = pending;
                _this.changeTop(0);
            }
            jQuery("#loader").modal('hide');
        });
    };
    DashboardComponent.prototype.getPendingContractBy = function () {
        var _this = this;
        var pending = [];
        var url = this.ics._apiurl + "serviceDBS/getPendingContractBy?status=41&ref=" + this._Status;
        this.http.doGet(url).subscribe(function (response) {
            if (response != undefined && response != null) {
                pending = _this.l_util.convertToArray(response.pendingLoan);
                _this._PendingBy = pending;
                _this.changeTop(0);
            }
        });
    };
    DashboardComponent.prototype.getPendingDisburseBy = function () {
        var _this = this;
        var pending = [];
        var url = this.ics._apiurl + "serviceDBS/getPendingDisburseBy?status=101&ref=" + this._Status;
        ;
        this.http.doGet(url).subscribe(function (response) {
            if (response != undefined && response != null) {
                pending = _this.l_util.convertToArray(response.pendingLoan);
                _this._PendingBy = pending;
                _this.changeTop(0);
            }
        });
    };
    DashboardComponent.prototype.changeStatus = function (aIndex) {
        this._Status = this._Pending[aIndex].code;
        this.changeType(0);
    };
    DashboardComponent.prototype.changeType = function (aIndex) {
        this._type = this._TypeCombo[aIndex].value;
        if (this._TypeCombo[aIndex].value == 40) {
            this.getPendingApprovedBy();
        }
        else if (this._TypeCombo[aIndex].value == 41) {
            this.getPendingContractBy();
        }
        else if (this._TypeCombo[aIndex].value == 101) {
            this.getPendingDisburseBy();
        }
    };
    // goWorkSpace(){
    //     this._router.navigate(['set/workSpace', , ]);
    // }
    DashboardComponent.prototype.changeTop = function (aIndex) {
        this.pending = false;
        this._PendingByTop = [];
        this._top = this._TopCombo[aIndex].value;
        var datalength;
        if (this._PendingBy.length > 10) {
            datalength = this._top;
        }
        else {
            datalength = this._PendingBy.length;
        }
        for (var i = 0; i < datalength; i++) {
            this._PendingByTop.push(this._PendingBy[i]);
            this.pending = true;
        }
    };
    DashboardComponent.prototype.changeCustChart = function (aIndex) {
        this.clearCust();
        this._custChart = this._Chart[aIndex].code;
        if (this._custChart == 1)
            this.showCustPie();
        else if (this._custChart == 2)
            this.showCustBar();
        else if (this._custChart == 3)
            this.showCustLine();
    };
    DashboardComponent.prototype.changeTotAmtChart = function (aIndex) {
        this.clearTotAmt();
        this._totAmtChart = this._Chart[aIndex].code;
        if (this._totAmtChart == 1)
            this.showTotAmtPie();
        else if (this._totAmtChart == 2)
            this.showTotAmtBar();
        else if (this._totAmtChart == 3)
            this.showTotAmtLine();
    };
    DashboardComponent.prototype.changeTotTransChart = function (aIndex) {
        this.clearTotTrans();
        this._totTransChart = this._Chart[aIndex].code;
        if (this._totTransChart == 1)
            this.showTotTransPie();
        else if (this._totTransChart == 2)
            this.showTotTransBar();
        else if (this._totTransChart == 3)
            this.showTotTransLine();
    };
    DashboardComponent.prototype.changeTotAmtByMonthChart = function (aIndex) {
        this.clearTotAmtByMonth();
        this._totAmtByMonthChart = this._Chart[aIndex].code;
        if (this._totAmtByMonthChart == 1)
            this.showTotAmtByMonthPie();
        else if (this._totAmtByMonthChart == 2)
            this.showTotAmtByMonthBar();
        else if (this._totAmtByMonthChart == 3)
            this.showTotAmtByMonthLine();
    };
    DashboardComponent.prototype.prevPage = function () {
        var odd = this._Pending.length % 2;
        var dataLength = this._Pending.length;
        if (odd == 1)
            dataLength = dataLength + 1;
        this.page = this.page - 2;
        if (this.page < 0)
            this.page = dataLength - 2;
        this._awaitAction = this._Pending.slice(this.page, this.page + 2);
    };
    DashboardComponent.prototype.nextPage = function () {
        var odd = this._Pending.length % 2;
        var dataLength = this._Pending.length;
        if (odd == 1)
            dataLength = dataLength + 1;
        this.page = this.page + 2;
        if (this.page == dataLength)
            this.page = 0;
        this._awaitAction = this._Pending.slice(this.page, this.page + 2);
    };
    DashboardComponent = __decorate([
        core_1.Component({
            selector: 'dashboard',
            template: "  \n    <div class=\"container\"><!-- -fluid\"> -->\n        <div class=\"row clearfix\">\n            <!-- <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> -->\n                <form class=\"form-horizontal\">                    \n                    <fieldset style=\"padding-left: 5px; padding-right: 5px;\">\n                        <legend>\n                            <div style=\"margin-bottom: 5px;\">                                \n                                    <h4 style=\"margin-top: 0px;\"><a routerLink=\"/set/Dashboard\">Dashboard</a> &nbsp;&nbsp;&nbsp; <a  routerLink=\"/set/workSpace\">My Workspace</a>\n                                    &nbsp;&nbsp;&nbsp; <a  routerLink=\"/set/ProcessCenter\">Process Center</a>&nbsp;&nbsp;&nbsp; <a  routerLink=\"/set/reportComponent\">Reports</a>\n                                    &nbsp;&nbsp;&nbsp; <a  routerLink=\"/set/AdminConsole\">Admin Console</a>\n                                    </h4> \n                            </div>\n                        </legend>                                                      \n                    </fieldset>\n\n                    <fieldset style=\"padding-left: 5px; padding-right: 5px;\">\n                        <div class=\"col-md-12\" style=\"padding: 0;\">\n                        <div class=\"col-md-3\" style=\"padding: 1px; border: solid 1px silver;\">\n                                <div class=\"col-md-6\" style=\"padding: 0px;\">\n                                    <select class=\"form-control input-sm fontSizeSmall fontWeightNormal\" style=\"padding-left: 5px; padding-top: 0px; padding-bottom: 0px;\"\n                                        [(ngModel)]=\"_custChart\" (change)=\"changeCustChart($event.target.options.selectedIndex)\" [ngModelOptions]=\"{standalone: true}\">\n                                        <option *ngFor=\"let code of _Chart\" value=\"{{code.code}}\">{{code.description}}</option>\n                                    </select>\n                                </div>\n                                <div class=\"col-md-6\" style=\"padding: 0px;\">\n                                    <select class=\"form-control input-sm fontSizeSmall fontWeightNormal\" style=\"padding-left: 5px; padding-top: 0px; padding-bottom: 0px;\"\n                                        [(ngModel)]=\"_totCustYear\" (change)=\"changeYear($event.target.options.selectedIndex, '1')\" [ngModelOptions]=\"{standalone: true}\">\n                                        <option *ngFor=\"let code of _yearList\" value=\"{{code.yearCode}}\">{{code.yearCode}}</option>\n                                    </select>\n                                </div>\n                                <div class=\"col-md-12\" (click)=\"showDashboardDialog('custPie')\" id = \"custPie\" style=\"display:block; min-height: 200px; height: 35%;\"></div>\n                                <div class=\"col-md-12\" (click)=\"showDashboardDialog('custBar')\" id = \"custBar\" style=\"display:none; min-height: 200px; height: 35%;\"></div>\n                                <div class=\"col-md-12\" (click)=\"showDashboardDialog('custLine')\" id = \"custLine\" style=\"display:none; min-height: 200px; height: 35%;\"></div>\n                            </div>\n\n                            <div class=\"col-md-3\" style=\"padding: 1px; border: solid 1px silver;\">\n                                <div class=\"col-md-6\" style=\"padding: 0px;\">\n                                    <select class=\"form-control input-sm fontSizeSmall fontWeightNormal\" style=\"padding-left: 5px; padding-top: 0px; padding-bottom: 0px;\"\n                                        [(ngModel)]=\"_totAmtChart\" (change)=\"changeTotAmtChart($event.target.options.selectedIndex)\" [ngModelOptions]=\"{standalone: true}\">\n                                        <option *ngFor=\"let code of _Chart\" value=\"{{code.code}}\">{{code.description}}</option>\n                                    </select>\n                                </div>\n                                <div class=\"col-md-6\" style=\"padding: 0px;\">\n                                    <select class=\"form-control input-sm fontSizeSmall fontWeightNormal\" style=\"padding-left: 5px; padding-top: 0px; padding-bottom: 0px;\"\n                                        [(ngModel)]=\"_totAmtYear\" (change)=\"changeYear($event.target.options.selectedIndex, '2')\" [ngModelOptions]=\"{standalone: true}\">\n                                        <option *ngFor=\"let code of _yearList\" value=\"{{code.yearCode}}\">{{code.yearCode}}</option>\n                                    </select>\n                                </div>\n                                <div class=\"col-md-12\" (click)=\"showDashboardDialog('totAmtPie')\" id = \"totAmtPie\" style=\"display:none; min-height: 200px; height: 35%;\"></div>\n                                <div class=\"col-md-12\" (click)=\"showDashboardDialog('totAmtBar')\" id = \"totAmtBar\" style=\"display:block; min-height: 200px; height: 35%;\"></div>\n                                <div class=\"col-md-12\" (click)=\"showDashboardDialog('totAmtLine')\" id = \"totAmtLine\" style=\"display:none; min-height: 200px; height: 35%;\"></div>\n                            </div>\n\n                            <div class=\"col-md-3\" style=\"padding: 1px; border: solid 1px silver;\">\n                              <div class=\"col-md-12\" style=\"padding: 0px;\">\n                                    <select class=\"form-control input-sm fontSizeSmall fontWeightNormal\" style=\"padding-left: 5px; padding-top: 0px; padding-bottom: 0px;\"\n                                        [(ngModel)]=\"_totAmtByBranchYear\" (change)=\"changeYear($event.target.options.selectedIndex, '3')\" [ngModelOptions]=\"{standalone: true}\">\n                                        <option *ngFor=\"let code of _yearList\" value=\"{{code.yearCode}}\">{{code.yearCode}}</option>\n                                    </select>\n                                </div>\n                                <h4 style=\"text-align:center;\">Total Amount By Branch</h4>\n\n                                <div class=\"col-md-12\" style=\"overflow:auto; min-height: 171px; height: 25%;\">\n                                    <div *ngIf=\"!borrow\" style=\"color:#ccc;text-align:center\">No result found!</div>\n                                    <table *ngIf=\"borrow\" class=\"dev-table table table-hover\">\n                                        <tbody>\n                                            <tr *ngFor=\"let obj of _borByBranch; let i= index\">\n                                                <td class=\"fontSizeSmall fontWeightNormal\" style=\"table-layout:fixed;vertical-align:middle;text-align:left;\">{{ i+1 }}.</td>\n                                                <td class=\"fontSizeSmall fontWeightNormal\" style=\"table-layout:fixed;vertical-align:middle;text-align:left;\">{{ obj.branch }}</td>\n                                                <td class=\"fontSizeSmall fontWeightNormal\" style=\"table-layout:fixed;vertical-align:middle;text-align:right;\">{{ formatAmount(obj.amount) }}</td>\n                                            </tr>\n                                        </tbody>\n                                    </table>\n                                </div>\n                            </div>\n\n                            <div class=\"col-md-3\" style=\"padding: 1px; border: solid 1px silver;\">\n                              <div class=\"col-md-12\" style=\"min-height: 230px; height: 40%;overflow:auto; padding:0;\">\n                                    <h4 style=\"text-align:center;\">Awaiting for Action</h4>\n                                    <div class=\"col-md-12\" style=\"padding:0;display: flex;justify-content:center;align-items:center;\">\n                                        <a *ngIf=\"await\" style=\"color: #464242;\" (click)=\"prevPage()\">\n                                            <i class=\"glyphicon glyphicon-menu-left\"></i>\n                                        </a>\n                                        <div class=\"col-md-5\" *ngFor=\"let obj of _awaitAction;\" style=\"padding:0;\">\n                                            <div style=\"padding:0;\">\n                                        <h5 style=\"margin:0;\">{{obj.description}}</h5>\n                                        <div class=\"notification\" style=\"background-color: #3498DB;cursor:pointer;font-size:12px;\">\n                                            Approved\n                                            <span class=\"badge\">{{obj.noApproved}}</span>\n                                        </div>\n\n                                        <div class=\"notification\" style=\"background-color: #2ECC71;cursor:pointer;font-size:12px;\">\n                                            Contract\n                                            <span class=\"badge\">{{obj.noContract}}</span>\n                                        </div>\n\n                                        <div class=\"notification\" style=\"background-color: #F1C40F;cursor:pointer;font-size:12px;\">\n                                            Disburse\n                                            <span class=\"badge\">{{obj.noDisburse}}</span>\n                                        </div>\n                                    </div>\n                                </div>\n \t\t\t\t\t\t\t\t\t\t<a *ngIf=\"await\" style=\"color: #464242;\" (click)=\"nextPage()\">\n                                            <i class=\"glyphicon glyphicon-menu-right\"></i>\n                                        </a>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n\n                        <div class=\"col-md-12\" style=\"padding: 0;\">\n                            <div class=\"col-md-9\" style=\"padding: 0;\">\n                                <div class=\"col-md-6\" style=\"padding: 1px; border: solid 1px silver;\">\n                                    <div class=\"col-md-6\" style=\"padding: 0px;\">\n                                        <select class=\"form-control input-sm fontSizeSmall fontWeightNormal\" style=\"padding-left: 5px; padding-top: 0px; padding-bottom: 0px;\"\n                                            [(ngModel)]=\"_totTransChart\" (change)=\"changeTotTransChart($event.target.options.selectedIndex)\" [ngModelOptions]=\"{standalone: true}\">\n                                            <option *ngFor=\"let code of _Chart\" value=\"{{code.code}}\">{{code.description}}</option>\n                                        </select>\n                                    </div>\n                                    <div class=\"col-md-6\" style=\"padding: 0px;\">\n                                        <select class=\"form-control input-sm fontSizeSmall fontWeightNormal\" style=\"padding-left: 5px; padding-top: 0px; padding-bottom: 0px;\"\n                                            [(ngModel)]=\"_totTranByMonthYear\" (change)=\"changeYear($event.target.options.selectedIndex, '4')\" [ngModelOptions]=\"{standalone: true}\">\n                                            <option *ngFor=\"let code of _yearList\" value=\"{{code.yearCode}}\">{{code.yearCode}}</option>\n                                        </select>\n                                    </div>\n                                    <div class=\"col-md-12\" (click)=\"showDashboardDialog('totTransPie')\" id = \"totTransPie\" style=\"display:none; min-height: 200px; height: 35%;\"></div>\n                                    <div class=\"col-md-12\" (click)=\"showDashboardDialog('totTransBar')\" id = \"totTransBar\" style=\"display:none; min-height: 200px; height: 35%;\"></div>\n                                    <div class=\"col-md-12\" (click)=\"showDashboardDialog('totTransLine')\" id = \"totTransLine\" style=\"display:block; min-height: 200px; height: 35%;\"></div>\n                                </div>\n                                <div class=\"col-md-6\" style=\"padding: 1px; border: solid 1px silver;\">\n                                    <div class=\"col-md-6\" style=\"padding: 0px;\">\n                                        <select class=\"form-control input-sm fontSizeSmall fontWeightNormal\" style=\"padding-left: 5px; padding-top: 0px; padding-bottom: 0px;\"\n                                            [(ngModel)]=\"_totAmtByMonthChart\" (change)=\"changeTotAmtByMonthChart($event.target.options.selectedIndex)\" [ngModelOptions]=\"{standalone: true}\">\n                                            <option *ngFor=\"let code of _Chart\" value=\"{{code.code}}\">{{code.description}}</option>\n                                        </select>\n                                    </div>\n                                    <div class=\"col-md-6\" style=\"padding: 0px;\">\n                                        <select class=\"form-control input-sm fontSizeSmall fontWeightNormal\" style=\"padding-left: 5px; padding-top: 0px; padding-bottom: 0px;\"\n                                            [(ngModel)]=\"_totAmtByMonthYear\" (change)=\"changeYear($event.target.options.selectedIndex, '5')\" [ngModelOptions]=\"{standalone: true}\">\n                                            <option *ngFor=\"let code of _yearList\" value=\"{{code.yearCode}}\">{{code.yearCode}}</option>\n                                        </select>\n                                </div>\n                                 <div class=\"col-md-12\" (click)=\"showDashboardDialog('totAmtByMonthPie')\" id = \"totAmtByMonthPie\" style=\"display:none; min-height: 200px; height: 35%;\"></div>\n                                    <div class=\"col-md-12\" (click)=\"showDashboardDialog('totAmtByMonthBar')\" id = \"totAmtByMonthBar\" style=\"display:none; min-height: 200px; height: 35%;\"></div>\n                                    <div class=\"col-md-12\" (click)=\"showDashboardDialog('totAmtByMonthLine')\" id = \"totAmtByMonthLine\" style=\"display:block; min-height: 200px; height: 35%;\"></div>\n                                </div>\n                            </div>\n\n                            <div class=\"col-md-3\" style=\"padding: 1px; border: solid 1px silver;\">\n                                <div class=\"col-md-12\" style=\"min-height: 230px; height: 40%;\">\n                                    <h4 style=\"text-align:center;\">Awaiting List for Action</h4>\n                                    <div class=\"col-md-12\" style=\"margin-top: 5px;padding: 0;\">\n                                        <div class=\"col-md-4\" style=\"padding-left: 0px; padding-right: 0px;\">\n                                            <select class=\"form-control input-sm fontSizeSmall fontWeightNormal\" style=\"padding-left: 5px; padding-top: 0px; padding-bottom: 0px;\"\n                                                [(ngModel)]=\"_top\" (change)=\"changeTop($event.target.options.selectedIndex)\" [ngModelOptions]=\"{standalone: true}\">\n                                                <option *ngFor=\"let code of _TopCombo\" value=\"{{code.value}}\">{{code.caption}}</option>\n                                            </select>\n                                        </div>\n                                        <div class=\"col-md-4\" style=\"padding-left: 0px; padding-right: 0px;\">\n                                            <select class=\"form-control input-sm fontSizeSmall fontWeightNormal\" style=\"padding-left: 5px; padding-top: 0px; padding-bottom: 0px;\"\n                                                [(ngModel)]=\"_Status\" (change)=\"changeStatus($event.target.options.selectedIndex)\" [ngModelOptions]=\"{standalone: true}\">\n                                                <option *ngFor=\"let code of _Pending\" value=\"{{code.code}}\">{{code.description}}</option>\n                                            </select>\n                                        </div>\n                                        <div class=\"col-md-4\" style=\"padding-left: 0px; padding-right: 0px;\">\n                                            <select class=\"form-control input-sm fontSizeSmall fontWeightNormal\" style=\"padding-left: 5px; padding-top: 0px; padding-bottom: 0px;\"\n                                                [(ngModel)]=\"_type\" (change)=\"changeType($event.target.options.selectedIndex)\" [ngModelOptions]=\"{standalone: true}\">\n                                                <option *ngFor=\"let code of _TypeCombo\" value=\"{{code.value}}\">{{code.caption}}</option>\n                                            </select>\n                                        </div>\n                                    </div>\n\n                                     <div class=\"col-md-12\" style=\"overflow:auto;height: 65%; margin-top: 5px; padding: 0;\">\n                                        <div *ngIf=\"!pending\" style=\"color:#ccc;text-align:center\">No result found!</div>\n                                        <table *ngIf=\"pending\" class=\"dev-table table table-hover\">\n                                            <tbody>\n                                                <tr *ngFor=\"let obj of _PendingByTop; let i= index\">\n                                                    <td class=\"fontSizeSmall fontWeightNormal\" style=\"table-layout:fixed;vertical-align:middle;text-align:left;\">{{ i+1 }}.</td>\n                                                    <td class=\"fontSizeSmall fontWeightNormal\" style=\"table-layout:fixed;vertical-align:middle;text-align:left;\">{{ obj.appId }}</td>\n                                                    <td class=\"fontSizeSmall fontWeightNormal\" style=\"table-layout:fixed;vertical-align:middle;text-align:right;\">{{ formatAmount(obj.amount) }}</td>\n                                                </tr>\n                                            </tbody>\n                                        </table>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </fieldset>\n                </form>\n                <div class=\"loader modal\" id=\"loader\"></div> \n           <!-- </div>-->\n        </div>\n     </div>\n\n    <div id=\"divDashboardDialog\" class=\"modal fade\" role=\"dialog\">\n        <div class=\"modal-dialog modal-lg\">\n            <div class=\"modal-content\">\n                <div class=\"modal-body\" style=\"height: 80%;\">\n                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" style=\"margin-right: -1px !important; margin-top: 2px !important;\">&times;</button> \n                      \n                    <div class=\"col-md-12\" id = \"custPieDialog\" style=\"display:none;height: 90%;\"></div>\n                    <div class=\"col-md-12\" id = \"custBarDialog\" style=\"display:none;height: 90%;\"></div>\n                    <div class=\"col-md-12\" id = \"custLineDialog\" style=\"display:none;height: 90%;\"></div>\n\n                    <div class=\"col-md-12\" id = \"totAmtPieDialog\" style=\"display:none;height: 90%;\"></div>\n                    <div class=\"col-md-12\" id = \"totAmtBarDialog\" style=\"display:none;height: 90%;\"></div>\n                    <div class=\"col-md-12\" id = \"totAmtLineDialog\" style=\"display:none;height: 90%;\"></div>\n\n                    <div class=\"col-md-12\" id = \"totTransPieDialog\" style=\"display:none;height: 90%;\"></div>\n                    <div class=\"col-md-12\" id = \"totTransBarDialog\" style=\"display:none;height: 90%;\"></div>\n                    <div class=\"col-md-12\" id = \"totTransLineDialog\" style=\"display:none;height: 90%;\"></div>\n\n                    <div class=\"col-md-12\" id = \"totAmtByMonthPieDialog\" style=\"display:none;height: 90%;\"></div>\n                    <div class=\"col-md-12\" id = \"totAmtByMonthBarDialog\" style=\"display:none;height: 90%;\"></div>\n                    <div class=\"col-md-12\" id = \"totAmtByMonthLineDialog\" style=\"display:none;height: 90%;\"></div>\n                </div>\n            </div>\n        </div>\n    </div>\n    ",
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof rp_intercom_service_1.RpIntercomService !== 'undefined' && rp_intercom_service_1.RpIntercomService) === 'function' && _a) || Object, (typeof (_b = typeof rp_http_service_1.RpHttpService !== 'undefined' && rp_http_service_1.RpHttpService) === 'function' && _b) || Object, (typeof (_c = typeof rp_references_1.RpReferences !== 'undefined' && rp_references_1.RpReferences) === 'function' && _c) || Object, router_1.Router, router_1.ActivatedRoute, (typeof (_d = typeof rp_client_util_1.ClientUtil !== 'undefined' && rp_client_util_1.ClientUtil) === 'function' && _d) || Object, (typeof (_e = typeof icbs_util_component_1.iCBSUtil !== 'undefined' && icbs_util_component_1.iCBSUtil) === 'function' && _e) || Object])
    ], DashboardComponent);
    return DashboardComponent;
    var _a, _b, _c, _d, _e;
}());
exports.DashboardComponent = DashboardComponent;
//# sourceMappingURL=simple.js.map