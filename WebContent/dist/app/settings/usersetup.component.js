"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var Rx_1 = require('rxjs/Rx');
core_2.enableProdMode();
var UserSetupComponent = (function () {
    function UserSetupComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this.catch = "";
        this._output1 = "";
        this.message = "";
        this._key = "";
        this.state = [{ "code": "", "despEng": "" }];
        this.city = [{ "code": "", "despEng": "" }];
        this._chkloginId = 'false';
        this.confirmpass = "";
        this.confirmpwd = "";
        this._sessionMsg = "";
        this._role = "Role";
        this._objtest = { "sessionId": "", "msgCode": "", "msgDesc": "", "syskey": 0, "recordStatus": 0, "n4": 0, "t1": "", "t2": "", "t3": "", "t4": "", "name": "", "username": "", "rolesyskey": [], "userrolelist": [{ "syskey": 0, "t2": "", "flag": false }], "t7": "", "t5": "--- Select ---", "t6": "", "n5": 0, "n6": 0, "n7": 0, "n2": 0, "userId": "" };
        this._ans = "";
        this.strongRegex = new RegExp("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%]).{4,20})");
        this.mediumRegex = new RegExp("^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{5,}$");
        this.nrcPattern = "(\\d{1,2}/\\w{4,9}\\((N|Naing|n|p|P|naing|Y|y|E|e)\\)\\d{5,6}|(\\w{1,1}/\\w{3,5}|\\w{3,5})\\d{6})";
        this._returnResult = { "keyResult": 0, "longResult": "", "state": "", "stringResult": "", "userId": "", "msgCode": "", "msgDesc": "" };
        this._obj = this.getDefaultObj();
        this._mflag = false;
        this._ans1 = "";
        this.rolesyskey = [];
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = false;
            this.checkSession();
            jQuery("#mydelete").prop("disabled", true);
            this._obj = this.getDefaultObj();
            this._returnResult = { "keyResult": 0, "longResult": "", "state": "", "stringResult": "", "userId": "", "msgCode": "", "msgDesc": "" };
            this.isAdmin = true;
            //this.isMerchant = false;
            //this.loader = true;
            this.checkSession();
            this.getAllBranch();
            this.getAllMerchant();
            this.ics.confirmUpload(false);
            this.getUserRoleList();
            this.getStateList();
        }
    }
    UserSetupComponent.prototype.pad = function (s) {
        return (s < 10) ? '0' + s : s;
    };
    UserSetupComponent.prototype.getDefaultObj = function () {
        return { "sessionId": "", "msgCode": "", "msgDesc": "", "syskey": 0, "recordStatus": 0, "n4": 0, "t1": "", "t2": "", "t3": "", "t4": "", "name": "", "username": "", "rolesyskey": [], "userrolelist": [{ "syskey": 0, "t2": "", "flag": false }], "t7": "", "t5": "--- Select ---", "t6": "", "n5": 0, "n6": 0, "n7": 0, "n2": 0, "userId": "" };
    };
    UserSetupComponent.prototype.goNew = function () {
        this._mflag = false;
        this.clearData();
        this._mflag = true;
    };
    UserSetupComponent.prototype.clearData = function () {
        this._chkloginId = 'false';
        this.confirmpwd = "";
        this.message = "";
        this._result = false;
        this._key = "";
        this.rolesyskey = [];
        this._obj.rolesyskey = [];
        this._obj.t1 = "";
        this._obj.t2 = "";
        this._obj.t3 = "";
        this._obj.t5 = "--- Select ---";
        this._obj.t6 = "";
        this._obj.t7 = "";
        this._obj.t4 = "";
        this._obj.recordStatus = 0;
        this._obj.n2 = 0;
        this._obj.n5 = 0;
        this._obj.n6 = 0;
        this._obj.n7 = 0;
        this._obj.userId = "";
        this._obj.name = "";
        this._obj.username = "";
        this._obj.syskey = 0;
        this.messagehide = true;
        this.city = [{ "code": "", "despEng": "" }];
        this._returnResult = { "keyResult": 0, "longResult": "", "state": "", "stringResult": "", "userId": "", "msgCode": "", "msgDesc": "" };
        for (var i = 0; i < this._obj.userrolelist.length; i++) {
            this._obj.userrolelist[i].flag = false;
        }
        jQuery("#mydelete").prop("disabled", true);
    };
    UserSetupComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this._key = id;
                _this.goReadBySyskey(id);
            }
        });
    };
    UserSetupComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    /* checkSession() {
      try {
        let url: string = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "TBA", "msgCode": "" };
        this.http.doGet(url).subscribe(
          data => {
            if (data.msgCode == '0016') {
              this._sessionMsg = data.msgDesc;
              this.showMessage();
            }
          },
          () => { });
      } catch (e) {
        alert("Invalid URL");
      }
  
    } */
    UserSetupComponent.prototype.goReadBySyskey = function (p) {
        var _this = this;
        try {
            this._chkloginId = 'true';
            var url = this.ics._apiurl + 'service001/getBranchUserData?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
            var json = this._key;
            this.http.doPost(url, json).subscribe(function (data) {
                _this._output1 = JSON.stringify(data);
                _this.confirmpwd = data.t2;
                _this._obj = data;
                if (_this._obj.msgCode == '0016') {
                    _this._returnResult.msgDesc = "";
                    _this._sessionMsg = _this._obj.msgDesc;
                    _this.showMessage();
                }
                else {
                    /* if (this._obj.n2 == 3) {
                      //this.isMerchant = true;
                      this.isAdmin = false;
                    }
                    else {
                      //this.isMerchant = false;
                      this.isAdmin = true;
                    } */
                    for (var i = 0; i < _this._obj.userrolelist.length; i++) {
                        if (data.userrolelist[i].flag)
                            _this._obj.userrolelist[i].flag = true;
                        else
                            _this._obj.userrolelist[i].flag = false;
                    }
                    jQuery("#mydelete").prop("disabled", false);
                    _this.rolesyskey = [];
                    for (var i = 0; i < _this._obj.rolesyskey.length; i++) {
                        if (_this._obj.rolesyskey[i] != 0) {
                            _this.rolesyskey.push(_this._obj.rolesyskey[i]);
                        }
                    }
                    _this._obj.rolesyskey = _this.rolesyskey;
                    if (_this._obj.userrolelist[1].syskey == 0) {
                        if (_this._obj.userrolelist.length - 1 == _this.rolesyskey.length) {
                            _this._result = true;
                        }
                    }
                    else {
                        if (_this._obj.userrolelist.length == _this.rolesyskey.length) {
                            _this._result = true;
                        }
                    }
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    UserSetupComponent.prototype.goList = function () {
        this._router.navigate(['/User-list']);
    };
    UserSetupComponent.prototype.getAllMerchant = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics._apiurl + 'service001/getAllMerchant').subscribe(function (data) {
                if (data != null && data != undefined) {
                    if (!(data.ref019 instanceof Array)) {
                        var m = [];
                        m[0] = data.ref019;
                        _this.ref._lov3.ref019 = m;
                    }
                    else {
                        _this.ref._lov3.ref019 = data.ref019;
                    }
                }
                else {
                    _this.ref._lov3.ref019 = [{ "value": "", "caption": "Empty" }];
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    UserSetupComponent.prototype.getAllBranch = function () {
        var _this = this;
        try {
            this._mflag = false;
            this.http.doGet(this.ics._apiurl + 'service001/getAllBranch').subscribe(function (data) {
                if (data != null && data != undefined) {
                    var brcombo = [{ "value": "0", "caption": "--- Select ---" }];
                    for (var i = 0; i < data.ref018.length; i++) {
                        brcombo.push({ "value": data.ref018[i].value, "caption": data.ref018[i].caption });
                    }
                    _this.ref._lov3.ref018 = data.ref018;
                    _this.ref._lov3.ref018 = brcombo;
                }
                else {
                    _this.ref._lov3.ref018 = [{ "value": "0", "caption": "--- Select ---" }];
                }
                _this._mflag = true;
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    /* changeModule(event) {
      let options = event.target.options;
      let k = options.selectedIndex;//Get Selected Index
      let value = options[options.selectedIndex].value;//Get Selected Index's Value
      this._obj.n2 = value;
  
      for (var i = 1; i < this.ref._lov1.ref025.length; i++) {
        if (this.ref._lov1.ref025[i].value == value) {
          this._obj.n2 = this.ref._lov1.ref025[i].value;
        }
      }
  
      if (this._obj.n2 == 2) {
        this.isAdmin = true;
        this.isMerchant = false;
      } else {
        this.isMerchant = true;
        this.isAdmin = false;
        this.getAllMerchant();
  
      }
    } */
    UserSetupComponent.prototype.updateChecked111 = function (event) {
        if (event.target.checked) {
            for (var i = 0; i < this._obj.userrolelist.length; i++) {
                this._obj.userrolelist[i].flag = true;
                this._result = true;
                if (this._obj.userrolelist[i].syskey != 0) {
                    this.rolesyskey[i] = this._obj.userrolelist[i].syskey;
                }
            }
        }
        else {
            //if parentmenu is not check, uncheck all childmenu
            for (var i = 0; i < this._obj.userrolelist.length; i++) {
                this._obj.userrolelist[i].flag = false;
                var indexx = this.rolesyskey.indexOf(this._obj.userrolelist[i].syskey);
                this.rolesyskey.splice(indexx, 1);
                this._result = false;
            }
        }
        this._ans1 = JSON.stringify(this.rolesyskey);
        this._obj.rolesyskey = this.rolesyskey;
    };
    UserSetupComponent.prototype.clearconfirm = function (e) {
        this.confirmpass = "";
    };
    UserSetupComponent.prototype.keyupconfirm = function (e) {
        if (this._obj.t2 === "" || this._obj.t2 === null) {
            this.message = "";
        }
        else {
            if (this._obj.t2.length >= 6) {
                if (this.mediumRegex.test(this._obj.t2)) {
                    this.message = "Medium Password";
                    this.catch = "";
                }
                else if (this.strongRegex.test(this._obj.t2)) {
                    this.message = "Strong Password";
                    this.catch = "";
                }
            }
            else {
                this.message = "Invalid Password";
                this.catch = "aaa";
            }
        }
    };
    UserSetupComponent.prototype.updateChecked = function (value, event) {
        if (event.target.checked) {
            this.rolesyskey.push(value);
        }
        else if (!event.target.checked) {
            var indexx = this.rolesyskey.indexOf(value);
            this.rolesyskey.splice(indexx, 1);
        }
        this._ans = JSON.stringify(this.rolesyskey);
        this._obj.rolesyskey = this.rolesyskey;
        if (this._obj.userrolelist[1].syskey == 0) {
            if (this._obj.userrolelist.length - 1 == this.rolesyskey.length) {
                for (var i = 0; i < this._obj.userrolelist.length - 1; i++) {
                    this._obj.userrolelist[i].flag = true;
                }
                this._result = true;
            }
            else {
                this._result = false;
            }
        }
        else {
            if (this._obj.userrolelist.length == this.rolesyskey.length) {
                for (var i = 0; i < this._obj.userrolelist.length; i++) {
                    this._obj.userrolelist[i].flag = true;
                }
                this._result = true;
            }
            else {
                this._result = false;
            }
        }
    };
    UserSetupComponent.prototype.getStateList = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics._apiurl + 'service001/getStateListNew').subscribe(function (response) {
                if (response.refstate != null && response.refstate != undefined) {
                    //this.ref._lov3.refstate[i] = response.refstate[i - 1];
                    _this.ref._lov3.refstate = [{ "value": "00000000", "caption": "ALL" }];
                    for (var i = 1; i < response.refstate.length; i++) {
                        if (response.refstate[i].value == "13000000") {
                            _this.ref._lov3.refstate[1] = response.refstate[i];
                        }
                        if (response.refstate[i].value == "10000000") {
                            _this.ref._lov3.refstate[2] = response.refstate[i];
                        }
                        if (response.refstate[i].value == "00000000") {
                            _this.ref._lov3.refstate[3] = response.refstate[i];
                        }
                    }
                }
                else {
                    _this.ref._lov3.refstate = [{ "value": "", "caption": "Empty" }];
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Error!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    UserSetupComponent.prototype.getUserRoleList = function () {
        var _this = this;
        try {
            this._mflag = false;
            this.http.doGet(this.ics._apiurl + 'service001/getUserRolelist?userID=' + this.ics._profile.userID).subscribe(function (data) {
                _this._obj = data;
                for (var i = 0; i < _this._obj.userrolelist.length; i++) {
                    _this._obj.userrolelist[i].flag = false;
                }
                _this._mflag = true;
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    UserSetupComponent.prototype.validateWhiteSpace = function (s) {
        if (/\s/.test(s)) {
            return (false);
        }
        return (true);
    };
    UserSetupComponent.prototype.goSave = function () {
        var _this = this;
        this._mflag = false;
        this._returnResult = { "keyResult": 0, "longResult": "", "state": "", "stringResult": "", "userId": "", "msgCode": "", "msgDesc": "" };
        if (this._obj.t1.length > 0) {
            if (!this.validateWhiteSpace(this._obj.t1)) {
                this._returnResult.state = 'false';
                this._returnResult.msgDesc = "Login ID cannot contains space";
                this._mflag = true;
            }
            if (!/^([A-Za-z0-9]{1,20})$/.test(this._obj.t1)) {
                this._returnResult.state = 'false';
                this._returnResult.msgDesc = "Login ID should not contain special characters";
                this._mflag = true;
            }
            if (this._obj.t1.length > 20) {
                this._returnResult.state = 'false';
                this._returnResult.msgDesc = "Login ID should contain at most twenty characters ";
                this._mflag = true;
            }
        }
        if (this._obj.t2 != this.confirmpwd) {
            //this.confirmpass="Password and Confirm Password do not Match!"
            this._returnResult.state = 'false';
            this._returnResult.msgDesc = "Password and Confirm Password do not Match!";
            this._mflag = true;
        }
        if (this._obj.t2 == this.confirmpwd) {
            if (this._obj.t2.length < 6) {
                this._returnResult.state = 'false';
                this._returnResult.msgDesc = "Password length must be at least six characters";
                this._mflag = true;
            }
        }
        if (this._obj.t5 != "00000000" && this._obj.t5 != "13000000" && this._obj.t5 != "10000000") {
            this._returnResult.state = 'false';
            this._returnResult.msgDesc = "Please select region";
            this._mflag = true;
        }
        else if (this.catch.length > 0) {
            this.message = "Invalid Password,Try Again";
            this._mflag = true;
        }
        if (!/^([0-9]{7,20})$/.test(this._obj.t4)) {
            this._returnResult.state = 'false';
            this._returnResult.msgDesc = "Phone number is invalid";
            this._mflag = true;
        }
        if (this._obj.t3.length > 0) {
            if (!this.validateEmail(this._obj.t3)) {
                this._returnResult.state = 'false';
                this._returnResult.msgDesc = "Email address is invalid";
                this._mflag = true;
            }
        }
        try {
            if (this.confirmpass == '' && this._returnResult.msgDesc == '') {
                this.loader = false;
                // this._mflag = false;
                this._obj.userId = this.ics._profile.userID;
                this._obj.sessionId = this.ics._profile.sessionID;
                var url = this.ics._apiurl + 'service001/saveBranchUser';
                var json = this._obj;
                this.http.doPost(url, json).subscribe(function (data) {
                    _this._mflag = true;
                    if (data.msgCode == '0016') {
                        _this._sessionMsg = data.msgDesc;
                        _this.showMessage();
                    }
                    else if (data.msgCode == '0001') {
                        _this._returnResult = data;
                        _this.rolesyskey = [];
                        _this.showMessageAlert(_this._returnResult.msgDesc);
                    }
                    else {
                        _this._returnResult = data;
                        if (_this._returnResult.state == 'true') {
                            _this._chkloginId = 'true';
                            _this._obj.syskey = _this._returnResult.keyResult;
                            _this.message = "";
                            _this._key = _this._returnResult.keyResult + "";
                            jQuery("#mydelete").prop("disabled", false);
                        }
                        _this.showMessageAlert(_this._returnResult.msgDesc);
                    }
                }, function () { });
            }
            else {
                this.showMessageAlert(this._returnResult.msgDesc);
            }
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    UserSetupComponent.prototype.goDelete = function () {
        var _this = this;
        try {
            this._mflag = false;
            this._obj.userId = this.ics._profile.userID;
            var url = this.ics._apiurl + 'service001/deleteUser?sessionID=' + this.ics._profile.sessionID + '&userIDForSession=' + this.ics._profile.userID;
            var json = this._obj;
            this._objtest = this._obj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this._sessionMsg = data.msgDesc;
                    _this.showMessage();
                }
                else {
                    _this._returnResult = data;
                    _this.showMessageAlert(_this._returnResult.msgDesc);
                    if (data.state) {
                        _this._obj = { "sessionId": "", "msgCode": "", "msgDesc": "", "syskey": 0, "recordStatus": 0, "n4": 0, "t1": "", "t2": "", "t3": "", "t4": "", "name": "", "username": "", "rolesyskey": [], "userrolelist": [{ "syskey": 0, "t2": "", "flag": false }], "t7": "", "t5": "--- Select ---", "t6": "", "n5": 0, "n6": 0, "n7": 0, "n2": 0, "userId": "" };
                        _this.confirmpwd = "";
                        _this.getUserRoleList();
                        jQuery("#mydelete").prop("disabled", true);
                    }
                }
                _this._mflag = true;
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    UserSetupComponent.prototype.validateEmail = function (mail) {
        if (/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(mail)) {
            return (true);
        }
        return (false);
    };
    UserSetupComponent.prototype.showMessageAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    UserSetupComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this._router.navigate(['/login']);
            jQuery("#sessionalert").modal('hide');
        });
    };
    UserSetupComponent.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    UserSetupComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsgAlert(data.desc);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsgAlert(data.desc);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    UserSetupComponent.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    UserSetupComponent.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    UserSetupComponent = __decorate([
        core_1.Component({
            selector: 'usersetup',
            template: "\n  <div class=\"container col-md-12 col-sm-12 col-xs-12\" style=\"padding: 0px 5%;\">\n\t<form class=\"form-horizontal\" (ngSubmit) = \"goSave()\">\n\t\t<legend>User Setup</legend>\n\t\t<div class=\"cardview list-height\">\n                <div class=\"col-md-12\" style=\"margin-bottom: 10px;\">  \n                    <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goList()\" >List</button> \n                    <button class=\"btn btn-sm btn-primary\" type=\"button\" (click)=\"goNew()\" >New</button>      \n                    <button class=\"btn btn-sm btn-primary\" type=\"submit\">Save</button>          \n                    <button class=\"btn btn-sm btn-primary\" disabled id=\"mydelete\"  type=\"button\" (click)=\"goDelete();\" >Delete</button> \n                </div>\n\t\n\t\t\t\t<div class = \"col-md-12\" id=\"custom-form-alignment-margin\">\n\t\t\t\t\t<div class = \"col-md-6\">\n\t\t\t\t\t\t<div class=\"form-group\"> \n\t\t\t\t\t\t\t<label class=\"col-md-4\">Login ID <font class=\"mandatoryfont\">*</font></label>\n\t\t\t\t\t\t\t\t<div class=\"col-md-8\">                  \n\t\t\t\t\t\t\t\t  <input *ngIf=\"_chkloginId == 'true'\" class=\"form-control\" type = \"text\" [(ngModel)]=\"_obj.t1\"  [ngModelOptions]=\"{standalone: true}\"readonly class=\"form-control input-sm\"> \n\t\t\t\t\t\t\t\t  <input *ngIf=\"_chkloginId != 'true'\" class=\"form-control\" type = \"text\" [(ngModel)]=\"_obj.t1\" [ngModelOptions]=\"{standalone: true}\" required=\"true\" maxlength=\"20\" class=\"form-control input-sm\">                               \n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\"> \n\t\t\t\t\t\t\t<label class=\"col-md-4\">Name<font class=\"mandatoryfont\">*</font></label>\n\t\t\t\t\t\t\t\t<div class=\"col-md-8\">                  \n\t\t\t\t\t\t\t\t\t<input [(ngModel)]=\"_obj.name\" required type=\"text\" maxlength=\"50\"  class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\" />\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\"> \n\t\t\t\t\t\t\t<label class=\"col-md-4\">Password <font class=\"mandatoryfont\">*</font></label>\n\t\t\t\t\t\t\t\t<div class=\"col-md-8\">                  \n\t\t\t\t\t\t\t\t\t<input [(ngModel)]=\"_obj.t2\" required type=\"password\"  class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\"/>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\"> \n\t\t\t\t\t\t\t<label class=\"col-md-4\">Confirm Password <font class=\"mandatoryfont\">*</font></label>\n\t\t\t\t\t\t\t\t<div class=\"col-md-8\">                  \n\t\t\t\t\t\t\t\t\t<input [(ngModel)]=\"confirmpwd\" required type=\"password\"  class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\" (keydown)=\"clearconfirm($event)\" maxlength = \"20\" />\n\t\t\t\t\t\t\t\t</div> \n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-md-6\">\n\t\t\t\t\t\t<div class=\"form-group\"> \n\t\t\t\t\t\t\t<label class=\"col-md-4\">Phone No. <font class=\"mandatoryfont\">*</font></label>\n\t\t\t\t\t\t\t\t<div class=\"col-md-8\">                  \n\t\t\t\t\t\t\t\t\t<input [(ngModel)]=\"_obj.t4\" required type=\"text\" maxlength=\"20\"  class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\"/>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\"> \n\t\t\t\t\t\t\t<label class=\"col-md-4\">Email<font class=\"mandatoryfont\">*</font></label>\n\t\t\t\t\t\t\t\t<div class=\"col-md-8\">                  \n\t\t\t\t\t\t\t\t\t<input [(ngModel)]=\"_obj.t3\" required type=\"text\"  class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\"/>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\"> \n\t\t\t\t\t\t\t<div><!--*ngIf = \"isMerchant == false \"-->\n\t\t\t\t\t\t\t\t<label class=\"col-md-4\" *ngIf = \"isAdmin == true \">Region</label>\n\t\t\t\t\t\t\t\t\t<div class=\"col-md-8\">                  \n\t\t\t\t\t\t\t\t\t\t<select [(ngModel)]=\"_obj.t5\"   class=\"form-control input-sm\" [ngModelOptions]=\"{standalone: true}\">\n\t\t\t\t\t\t\t\t\t\t\t<option *ngFor=\"let c of ref._lov3.refstate\" value=\"{{c.value}}\">{{c.caption}}</option>\n\t\t\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>  \n\n          <ul class=\"nav nav-tabs\">\n              <li class=\"active\"><a data-toggle=\"tab\" href=\"#tab1\">Role</a></li>\n          </ul>\n\n\t\t\t\t\t<div class=\"tab-content\">\n\t\t\t\t\t\t<div id=\"tab1\" class=\"tab-pane fade in active\"> \n\t\t\t\t\t\t<div class=\"form-group\"> </div>\n\t\t\t\t\t\t  <div class=\"col-md-12\">\n\t\t\t\t\t\t\t<div class=\"form-group\"> \n\t\t\t\t\t\t\t  <label>\n\t\t\t\t\t\t\t\t  <input type=\"checkbox\"  [(ngModel)]=\"_result\" (change)=\"updateChecked111($event)\" [ngModelOptions]=\"{standalone: true}\"> Select All\n\t\t\t\t\t\t\t\t<!-- {{_role}} --> </label>             \n\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t<ul  style=\"list-style:none;\">\n\t\t\t\t\t\t\t  <li *ngFor=\"let role of _obj.userrolelist\">\n\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t<div *ngIf=\"role.syskey!=0\"> \n\t\t\t\t\t\t\t\t\t<input type=\"checkbox\"  [(ngModel)]=\"role.flag\" (change)=\"updateChecked(role.syskey,$event)\" [ngModelOptions]=\"{standalone: true}\">             \n\t\t\t\t\t\t\t\t\t{{role.t2}}\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>    \n\t\t\t\t\t\t\t  </li>\n\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n            </form>\n</div>  \n\n<div id=\"sessionalert\" class=\"modal fade\">\n<div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n        <div class=\"modal-body\">\n             <p>{{_sessionMsg}}</p>\n        </div>\n        <div class=\"modal-footer\">\n        </div>\n    </div>\n</div>\n</div>\n\n<div [hidden] = \"_mflag\">\n<div  id=\"loader\" class=\"modal\" ></div>\n</div> \n\n"
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], UserSetupComponent);
    return UserSetupComponent;
}());
exports.UserSetupComponent = UserSetupComponent;
//# sourceMappingURL=usersetup.component.js.map