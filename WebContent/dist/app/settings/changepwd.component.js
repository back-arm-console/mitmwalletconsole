"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var Rx_1 = require('rxjs/Rx');
var pwd = (function () {
    function pwd(ics, _util, _router, route, http, ref) {
        this.ics = ics;
        this._util = _util;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._pwdobj = { "password": "", "newpassword": "", "confirmnewpassword": "", "userid": "", "sessionID": "" };
        this._resultobj = { "msgCode": "", "msgDesc": "", "keyst": "", state: false };
        this._passPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[~`!@#$%^&()_+/':;?,.<>[]|\{}]).{6,20}$/;
        this.sessionAlertMsg = "";
        this._mflag = false;
        this._pobj = this.getDefaultObj();
        /* checkSession() {
            try {
                let url: string = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
                this._resultobj = { "msgCode": "", "msgDesc": "", "keyst": "", state: false };
                this.http.doGet(url).subscribe(
                    data => {
                        if (data.msgCode == '0016') {
                            this.sessionAlertMsg = data.msgDesc;
                            this.showMessage();
                        }
                    },
                    error => {
                        if (error._body.type == 'error') {
                            alert("Connection Error!");
                        }
                    }, () => { });
            } catch (e) {
                alert(e);
            }
    
        } */
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        // RP Framework
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = true;
            this.checkSession();
            this.messagehide = true;
            this.readPswPolicy();
            this._chkstr = "false";
        }
    }
    pwd.prototype.getDefaultObj = function () {
        return { "pswminlength": 0, "pswmaxlength": 0, "spchar": 0, "upchar": 0, "lowerchar": 0, "pswno": 0, "msgCode": "", "msgDesc": "", "sessionID": "", "userID": "" };
    };
    pwd.prototype.readPswPolicy = function () {
        var _this = this;
        this._mflag = false;
        var url = this.ics._apiurl + 'service001/readPswPolicy?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID + '&formID=changepwd';
        ;
        this.http.doGet(url).subscribe(function (data) {
            if (data.msgCode == '0016') {
                _this.sessionAlertMsg = data.msgDesc;
                _this.showMsg(data.msgDesc, false);
            }
            else {
                _this._pobj = data;
            }
            _this._mflag = true;
        }, function (error) {
            if (error._body.type == 'error') {
                alert("Connection Error!");
            }
        });
    };
    pwd.prototype.goChange = function () {
        var _this = this;
        this._mflag = false;
        if (this._pwdobj.newpassword == this._pwdobj.confirmnewpassword) {
            if (this._pwdobj.newpassword == this._pwdobj.password) {
                this.msgclass = "alert alert-danger";
                this.resultMsg = "New Password should not be the same as current password";
                this.popupMessage(this.resultMsg);
                this._mflag = true;
            }
            else {
                try {
                    var iv = this._util.getIvs();
                    var dm = this._util.getIvs();
                    var salt = this._util.getIvs();
                    this._pwdobj.userid = this.ics._profile.userID;
                    this._pwdobj.sessionID = this.ics._profile.sessionID;
                    var newpassword = this._util.getEncryptText(iv, salt, dm, this._pwdobj.newpassword);
                    var password = this._util.getEncryptText(iv, salt, dm, this._pwdobj.password);
                    var confirmnewpassword = this._util.getEncryptText(iv, salt, dm, this._pwdobj.confirmnewpassword);
                    var param = {
                        "password": password,
                        "newpassword": newpassword,
                        "confirmnewpassword": confirmnewpassword,
                        "userid": this.ics._profile.userID,
                        "sessionID": this.ics._profile.sessionID,
                        "iv": iv, "dm": dm, "salt": salt
                    };
                    var url = this.ics._apiurl + 'service001/changePassword';
                    var json = param;
                    this.http.doPost(url, json).subscribe(function (result) {
                        _this._resultobj = result;
                        if (_this._resultobj.msgCode == "0016") {
                            _this.sessionAlertMsg = result.msgDesc;
                            _this.showMsg(result.msgDesc, false);
                        }
                        else if (_this._resultobj.msgCode == "0014") {
                            _this.msgclass = "alert alert-danger";
                            _this.resultMsg = _this._resultobj.msgDesc;
                            _this.showMsg(_this.resultMsg, false);
                        }
                        else {
                            _this._chkstr = "true";
                            _this.msgclass = "alert alert-success";
                            _this.showMsg("Please Sign in again.", true);
                            _this._router.navigate(['/login']);
                        }
                        _this._mflag = true;
                    }, function (error) {
                        if (error._body.type == 'error') {
                            alert("Connection Timed Out!");
                        }
                    }, function () { });
                }
                catch (e) {
                    alert(e);
                }
            }
        }
        else {
            this.resultMsg = "New and confirm passwords must be same";
            this.showMsg(this.resultMsg, false);
        }
    };
    pwd.prototype.goCancel = function () {
        this._pwdobj = { "password": "", "newpassword": "", "confirmnewpassword": "", "userid": "", "sessionID": "" };
        this._chkstr = "false";
    };
    pwd.prototype.popupMessage = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", t2: "Password Information", t3: msg });
    };
    pwd.prototype.passwordValidation = function (newPass) {
        if (newPass.match(this._passPattern)) {
            return true;
        }
        else {
            return false;
        }
    };
    pwd.prototype.showMessage = function () {
        var _this = this;
        jQuery("#alertmodal").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this.goLogOut();
        });
    };
    pwd.prototype.goLogOut = function () {
        jQuery("#alertmodal").modal('hide');
        this._router.navigate(['Login', , { p1: '*' }]);
    };
    pwd.prototype.messagealert = function () {
        var _this = this;
        this.messagehide = false;
        setTimeout(function () { return _this.messagehide = true; }, 3000);
    };
    pwd.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    pwd.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsgAlert(data.desc);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsgAlert(data.desc);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    pwd.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    pwd.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    pwd.prototype.showMsg = function (msg, bool) {
        if (bool == true) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "success", "t3": msg });
        }
        if (bool == false) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "warning", "t3": msg });
        }
        if (bool == undefined) {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "danger", "t3": msg });
        }
    };
    pwd = __decorate([
        core_1.Component({
            selector: 'pwd',
            template: " \n    <div class=\"container-fluid\">\n    <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n\t<form class=\"form-horizontal\" (ngSubmit)=\"goChange()\">\n\t\t<legend>Change Password</legend>\n\t\t<div class=\"cardview list-height\">\n\t\t\t<div class=\"row col-md-12\">\n\t\t\t\t<button type=\"submit\" class=\"btn btn-sm btn-primary\" *ngIf=\"_chkstr == 'false'\" > Change </button>\n\t\t\t\t<button type=\"submit\" class=\"btn btn-sm btn-primary\" *ngIf=\"_chkstr == 'true'\" disabled> Change </button> \n\t\t\t\t<button type=\"button\" class=\"btn btn-sm btn-primary\" (click)=\"goCancel()\">Cancel</button> \n            </div> \n\n            <div class=\"row col-md-12\">&nbsp;</div>\n                <div class=\"form-group\">\n\t\t\t<div [hidden]=\"messagehide\"  class=\"col-md-8\" style=\"margin-bottom: 10px;\">\n\t\t\t\t<span  class={{msgclass}} >{{resultMsg}}</span>\n\t\t\t</div>\n\t\t\t<div style=\"color: #3b5998;\" class=\"col-md-12\" style=\"margin-bottom: 10px;\">\n\t\t\t\t<span>* Require Minimum Length {{_pobj.pswminlength}}, Maximum Length {{_pobj.pswmaxlength}}</span><span *ngIf=\"_pobj.spchar != 0\">, Special Character {{_pobj.spchar}}</span><span *ngIf=\"_pobj.upchar != 0\">, Uppercase Character {{_pobj.upchar}}</span><span *ngIf=\"_pobj.lowerchar != 0\">, Lowercase Character {{_pobj.lowerchar}}</span><span *ngIf=\"_pobj.pswno != 0\">, Number {{_pobj.pswno}}</span>.\n\t\t\t</div>  \n\t\t\t\n            <div class=\"col-md-6\">\n                <div class=\"form-group\">\n                    <label for=\"inputPassword\" class=\"col-md-4\">Current Password <font class =\"mandatoryfont\">*</font></label>\n                    <div class=\"col-md-8\">\n                        <input type=\"password\" id=\"inputPassword\" class=\"form-control input-sm\" placeholder=\"Password\" required [(ngModel)]=\"_pwdobj.password\" [ngModelOptions]=\"{standalone: true}\" maxlength=\"20\">\n                    </div>\n                </div> \n\n                <div class=\"form-group\">\n                    <label for=\"newPassword\" class=\"col-md-4\">New Password <font class =\"mandatoryfont\">*</font></label>\n                    <div class=\"col-md-8\">\n                        <input type=\"password\" id=\"newPassword\" class=\"form-control input-sm\" placeholder=\"New Password\" required [(ngModel)]=\"_pwdobj.newpassword\" [ngModelOptions]=\"{standalone: true}\"  maxlength=\"20\">\n                    </div>\n                </div>\n\n                <div class=\"form-group\">\n                    <label for=\"newconfirmPassword\" class=\"col-md-4\">Confirm Password <font class =\"mandatoryfont\">*</font></label>\n                    <div class=\"col-md-8\">\n                        <input type=\"password\" id=\"newconfirmPassword\" class=\"form-control input-sm\" placeholder=\"Confirm Password\" required [(ngModel)]=\"_pwdobj.confirmnewpassword\" [ngModelOptions]=\"{standalone: true}\" maxlength=\"20\">\n                    </div>\n                </div>\n            </div>\n        </div>\n        </div>\n    </form>\n</div>\n</div>\n</div>\n\n    <div id=\"alertmodal\" class=\"modal fade\">\n        <div class=\"modal-dialog\">\n            <div class=\"modal-content\">\n                <div class=\"modal-body\">\n                    <p>{{sessionAlertMsg}}</p>\n                </div>\n                <div class=\"modal-footer\">\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <div [hidden] = \"_mflag\">\n        <div  id=\"loader\" class=\"modal\" ></div>\n    </div>\n    "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, rp_client_util_1.ClientUtil, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], pwd);
    return pwd;
}());
exports.pwd = pwd;
//# sourceMappingURL=changepwd.component.js.map