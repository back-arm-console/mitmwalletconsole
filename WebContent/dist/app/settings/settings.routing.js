"use strict";
var router_1 = require('@angular/router');
var changepwd_component_1 = require('./changepwd.component');
var settingsRoutes = [
    { path: 'change-password', component: changepwd_component_1.pwd },
    { path: 'change-password/:cmd', component: changepwd_component_1.pwd },
    { path: 'change-password/:cmd/:id', component: changepwd_component_1.pwd },
];
exports.settingsRouting = router_1.RouterModule.forChild(settingsRoutes);
//# sourceMappingURL=settings.routing.js.map