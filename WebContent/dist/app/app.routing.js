"use strict";
var router_1 = require('@angular/router');
var rp_login_component_1 = require('./rp-login.component');
var appRoutes = [
    {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full'
    }, {
        path: 'login',
        component: rp_login_component_1.RpLoginComponent
    }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map