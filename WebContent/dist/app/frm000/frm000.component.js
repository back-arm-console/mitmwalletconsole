"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
core_1.enableProdMode();
var Frm000Component = (function () {
    function Frm000Component(ics, route, _router, http) {
        this.ics = ics;
        this.route = route;
        this._router = _router;
        this.http = http;
        this._imgurl = "";
        this._welcometext = "";
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!this.ics.getRole() || this.ics.getRole() == 0)
            this._router.navigate(['/login']);
        else {
            this._imgurl = ics._imglogo;
            this._welcometext = this.ics._welcometext;
            //this.getwelcometext();  
            this.ics.confirmUpload(true);
        }
    }
    Frm000Component.prototype.ngOnInit = function () {
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
                var userID = params['p1'];
            }
        });
    };
    Frm000Component = __decorate([
        core_1.Component({
            template: "\n  <!--<div class=\"container\">\n      <div class=\"row clearfix\">\n        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\">\n          <fieldset>  \n            <div align=\"center\" class=\"form-group\" >\n              <legend>  <h1>{{_welcometext}}</h1> </legend>    \n              <img [src]=_imgurl style = \"width:350px; height:250px\">\n            </div>\n          </fieldset>\n        </div>\n      </div>\n    </div>-->\n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.ActivatedRoute, router_1.Router, rp_http_service_1.RpHttpService])
    ], Frm000Component);
    return Frm000Component;
}());
exports.Frm000Component = Frm000Component;
//# sourceMappingURL=frm000.component.js.map