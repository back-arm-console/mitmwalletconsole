"use strict";
var router_1 = require('@angular/router');
var frm000_component_1 = require('./frm000.component');
//import { SearchComponent }       from './Frmsearch.component';
var Frm000Routes = [
    { path: '000', component: frm000_component_1.Frm000Component },
    { path: '000/:cmd', component: frm000_component_1.Frm000Component },
    { path: 'Welcome', component: frm000_component_1.Frm000Component },
];
exports.Frm000Routing = router_1.RouterModule.forChild(Frm000Routes);
//# sourceMappingURL=frm000.routing.js.map