"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var Rx_1 = require('rxjs/Rx');
core_2.enableProdMode();
var FrmEndUserSetupComponent = (function () {
    function FrmEndUserSetupComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._mflag = false;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "loginID": "", "phNo": "", "parentID": "" };
        this._obj = this.getDefaultObj();
        this._util = new rp_client_util_1.ClientUtil();
        this.myDatePickerOptions = this._util.getDatePicker();
        this._dates = { "dobDate": this.dateobj };
        this.state = [{ "code": "", "despEng": "" }];
        this.city = [{ "code": "", "despEng": "" }];
        this._showlist = { "data": [{ "customerid": "" }] };
        this._custinfo = { "accArr": [{ "srno": "0", "cif": "", "name": "", "nrc": "", "account": "", "ccy": "", "isvisible": 0, "check": false, "dob": "" }] };
        this._customer = { "customerID": "", "name": "", "nrc": "", "accountNo": "", "balance": "", "message": "", "glDesp": "", "chkAccount": "false", "chkGL": "false", "other": "false" };
        this._SearchData = { "searchcaption": "", "searchvalue": "1", "search": "0" };
        this._chkloginId = 'false';
        this.isvalidate = "";
        this._key = "";
        this.sessionAlertMsg = "";
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = false;
            this.checkSession();
            this._dates.dobDate = this._util.changestringtodateobject(this._util.getTodayDate());
            this.getStateListNew();
            this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "loginID": "", "phNo": "", "parentID": "" };
            this.hideselect = true;
            this.chkIsAgent = false;
            this.isvalidate = "";
            this.IsAgent = false;
            this.isGL = false;
            this.getAllAgent();
            this.goRemove(0);
            this.sessionAlertMsg = "hello";
            jQuery("#sessionalert").modal();
            jQuery("#mydelete").prop("disabled", true);
            jQuery("#mySave").prop("disabled", false);
            this._obj = this.getDefaultObj();
        }
    }
    FrmEndUserSetupComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this._key = id;
                _this._obj.syskey = id;
                _this.goReadBySyskey(id);
            }
        });
    };
    FrmEndUserSetupComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    FrmEndUserSetupComponent.prototype.getDefaultObj = function () {
        // console.log(this._util.getTodayDate());
        return { "t1": "", "t3": "", "t4": "", "t5": "", "t6": "", "t7": "", "n3": 0, "city": "", "name": "", "customerid": "", "accountNumber": "", "syskey": 0, "parentID": "", "keyResult": "", "chkAccount": "false", "chkGL": "false", "state": "", "data": [], "userId": "", "sessionId": "" };
    };
    FrmEndUserSetupComponent.prototype.goReadBySyskey = function (p) {
        var _this = this;
        try {
            this._mflag = false;
            this._chkloginId = 'true';
            var url = this.ics._apiurl + 'service001/getUserProfileData?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
            this.http.doPost(url, p).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMessage();
                }
                else {
                    _this._obj = data;
                    _this._dates.dobDate = _this._util.changestringtodateobject(_this._obj.t6);
                    if (data.data != null) {
                        if (!(data.data instanceof Array)) {
                            var m = [];
                            m[0] = data.data;
                            _this._showlist.data = m;
                        }
                        else {
                            _this._showlist.data = data.data;
                        }
                    }
                    if (_this._obj.n3 == 1) {
                        _this.chkIsAgent = true;
                        jQuery("#mySave").prop("disabled", true);
                    }
                    else {
                        jQuery("#mySave").prop("disabled", false);
                    }
                    if (_this._obj.chkGL == 'true') {
                        _this.isGL = true;
                    }
                    _this.getCityList(_this._obj.state);
                    jQuery("#mydelete").prop("disabled", false);
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Error!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmEndUserSetupComponent.prototype.goList = function () {
        this._router.navigate(['/endusersetuplist']);
    };
    FrmEndUserSetupComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this.goLogOut();
        });
    };
    FrmEndUserSetupComponent.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    FrmEndUserSetupComponent.prototype.getStateListNew = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics._apiurl + 'service001/getStateListNew').subscribe(function (response) {
                if (response != null && response != undefined) {
                    _this.ref._lov3.refstate = response.refstate;
                }
                else {
                    _this.ref._lov3.refstate = [{ "value": "", "caption": "Empty" }];
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Error!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmEndUserSetupComponent.prototype.getCityList = function (pstate) {
        var _this = this;
        try {
            this.http.doGet(this.ics._apiurl + 'service001/getCitylist?statecode=' + pstate).subscribe(function (response) {
                if (response != null && response != undefined) {
                    _this.city = response.data;
                }
                else {
                    _this.city = [{ "code": "", "despEng": "" }];
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Error!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmEndUserSetupComponent.prototype.lookuppopup = function () {
        this._custinfo.accArr = [];
        this._SearchData = { "searchcaption": "", "searchvalue": "1", "search": "0" };
        this.hideselect = true;
        jQuery("#popup").modal();
    };
    FrmEndUserSetupComponent.prototype.changeState = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex;
        var value = options[options.selectedIndex].value;
        this.getCityList(value);
    };
    /* checkSession() {
      try {
  
        let url: string = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
        this.http.doGet(url).subscribe(
          data => {
            if (data.msgCode == '0016') {
              this.sessionAlertMsg = data.msgDesc;
              this.showMessage();
            }
          },
          error => {
            if (error._body.type == 'error') {
              alert("Connection Timed Out!");
            }
            else {
  
            }
          }, () => { });
      } catch (e) {
        alert("Invalid URL");
      }
  
    } */
    FrmEndUserSetupComponent.prototype.showMessageAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmEndUserSetupComponent.prototype.goRemove = function (i) {
        var sindex = this._showlist.data.indexOf(i);
        this._showlist.data.splice(sindex, 1);
    };
    FrmEndUserSetupComponent.prototype.validateEmail = function (mail) {
        if (/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(mail)) {
            return (true);
        }
        return (false);
    };
    FrmEndUserSetupComponent.prototype.validateWhiteSpace = function (s) {
        if (/\s/.test(s)) {
            return (false);
        }
        return (true);
    };
    FrmEndUserSetupComponent.prototype.goSave = function () {
        var _this = this;
        this._mflag = false;
        if (this._dates.dobDate == null) {
            this._returnResult.state = 'false';
            this._returnResult.msgDesc = "Please Fill Date of Birth";
            this._mflag = true;
        }
        else {
            this._obj.t6 = this._util.getDatePickerDate(this._dates.dobDate);
        }
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "loginID": "", "phNo": "", "parentID": "" };
        if (this._obj.t1.length > 0) {
            if (!this.validateWhiteSpace(this._obj.t1)) {
                this._returnResult.state = 'false';
                this._returnResult.msgDesc = "Login ID cannot contains space";
                this._mflag = true;
            }
            if (!/^([A-Za-z0-9]{1,20})$/.test(this._obj.t1)) {
                this._returnResult.state = 'false';
                this._returnResult.msgDesc = "Login ID should not contain special characters";
                this._mflag = true;
            }
            if (this._obj.t1.length > 20) {
                this._returnResult.state = 'false';
                this._returnResult.msgDesc = "Login ID should contain at most twenty characters";
                this._mflag = true;
            }
        }
        if (this._obj.t3.length > 0) {
            if (!this.validateEmail(this._obj.t3)) {
                this._returnResult.state = 'false';
                this._returnResult.msgDesc = "Email address is invalid";
                this._mflag = true;
            }
        }
        /* if (!/^(([1-9]|1[0-9])\/[A-Za-z]+\([N|n]\)[0-9]{6})$/.test(this._obj.t7)) {
          this._returnResult.state = 'false';
          //this.showMessageAlert("Invalid NRC No.");
          this._returnResult.msgDesc = "NRC number is invalid.";
          this._mflag = true;
        } */
        if (!/^([0-9]{7,20})$/.test(this._obj.t4)) {
            this._returnResult.state = 'false';
            this._returnResult.msgDesc = "Phone No. is invalid.";
            this._mflag = true;
        }
        if (this._obj.t6.length == 0) {
            this._returnResult.state = 'false';
            this._returnResult.msgDesc = "Date of Birth is blank";
            this._mflag = true;
        }
        else if (this._obj.t6.length > 0) {
            if (!/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/.test(this._obj.t6)) {
                this._returnResult.state = 'false';
                this._returnResult.msgDesc = "Invalid Date of Birth";
                this._mflag = true;
            }
            else {
                var t6 = this._obj.t6;
                var dob = Date.parse(t6.replace(/-/g, " "));
                var todaysDate = new Date();
                todaysDate.setHours(0, 0, 0, 0);
                var dateDifference = Math.abs(Number(todaysDate) - dob);
                if (dateDifference < (31556926000 * 18)) {
                    this._returnResult.state = 'false';
                    this._returnResult.msgDesc = "The user to be created should be above 18 years of age and . Please re enter the date of birth.";
                    this._mflag = true;
                }
                else if (dateDifference > (31556926000 * 100)) {
                    this._returnResult.state = 'false';
                    this._returnResult.msgDesc = "Invalid age . Please re enter the date of birth";
                    this._mflag = true;
                }
            }
        }
        if (this._showlist.data.length == 0) {
            this._returnResult.state = 'false';
            this._returnResult.msgDesc = "Please Add Customer ID";
            this._mflag = true;
        }
        if (this._returnResult.msgDesc == '') {
            try {
                this._obj.userId = this.ics._profile.userID;
                this._obj.sessionId = this.ics._profile.sessionID;
                this._obj.data = this._showlist.data;
                var url = this.ics._apiurl + 'service001/saveUserProfile';
                var json = this._obj;
                this.http.doPost(url, json).subscribe(function (data) {
                    if (data.msgCode == '0016') {
                        _this.sessionAlertMsg = data.msgDesc;
                        _this.showMessage();
                    }
                    else {
                        _this._returnResult = data;
                        if (_this._returnResult.state) {
                            _this._chkloginId = 'true';
                            _this._obj.syskey = _this._returnResult.keyResult;
                            _this._obj.t1 = _this._returnResult.loginID;
                            _this._obj.t4 = _this._returnResult.phNo;
                            _this._obj.parentID = _this._returnResult.parentID;
                            _this._key = _this._returnResult.keyResult + "";
                            jQuery("#mydelete").prop("disabled", false);
                            _this.getAllAgent();
                        }
                        _this.showMessageAlert(_this._returnResult.msgDesc);
                    }
                    _this._mflag = true;
                }, function (error) {
                    if (error._body.type == 'error') {
                        alert("Connection Error!");
                    }
                }, function () { });
            }
            catch (e) {
                alert("Invalid URL");
            }
        }
        else {
            this.showMessageAlert(this._returnResult.msgDesc);
        }
    };
    FrmEndUserSetupComponent.prototype.gotobindCIF = function (arr) {
        this._obj.customerid = arr.cif;
        if (arr.dob == '' || arr.dob == 'null') {
            this._obj.t6 = this._util.getTodayDate();
            this._dates.dobDate = this._util.changestringtodateobject(this._util.getTodayDate());
        }
        else {
            this._obj.t6 = arr.dob;
            var adate = arr.dob;
            this._dates.dobDate = this._util.changestringtodateobject(adate);
        }
        this._obj.name = arr.name;
        this._obj.t7 = arr.nrc;
        jQuery("#popup").modal('hide');
    };
    FrmEndUserSetupComponent.prototype.checkAgent = function (event) {
        if (event.target.checked) {
            this.chkIsAgent = true;
            this.IsAgent = true;
            this._obj.n3 = 1;
            jQuery("#mySave").prop("disabled", true);
        }
        else {
            this.chkIsAgent = false;
            this.IsAgent = false;
            this.isGL = false;
            this._obj.n3 = 0;
            this._obj.accountNumber = "";
            this._obj.parentID = "";
            this.isvalidate = "";
            jQuery("#mySave").prop("disabled", false);
        }
    };
    FrmEndUserSetupComponent.prototype.checkGL = function (event) {
        if (event.target.checked) {
            this.isGL = true;
            this._obj.accountNumber = '';
            jQuery("#mySave").prop("disabled", true);
            this.isvalidate = "";
        }
        else {
            this.isGL = false;
            this._obj.accountNumber = '';
            jQuery("#mySave").prop("disabled", true);
            this.isvalidate = "";
        }
    };
    FrmEndUserSetupComponent.prototype.changeAgentHead = function (event) {
        var options = event.target.options;
        var k = options.selectedIndex; //Get Selected Index
        var value = options[options.selectedIndex].value; //Get Selected Index's Value  
        this._obj.parentID = value;
        for (var i = 1; i < this.ref._lov3.refAgent.length; i++) {
            if (this.ref._lov3.refAgent[i].value == value) {
                this._obj.parentID = this.ref._lov3.refAgent[i].value;
            }
        }
    };
    FrmEndUserSetupComponent.prototype.getAllAgent = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics._apiurl + 'service001/getAllAgent').subscribe(function (data) {
                if (data != null && data != undefined) {
                    var agentcombo = [{ "value": "0", "caption": "--- Select ---" }];
                    for (var i = 0; i < data.refAgent.length; i++) {
                        agentcombo.push({ "value": data.refAgent[i].value, "caption": data.refAgent[i].caption });
                    }
                    // this.ref._lov3.refAgent = data.refAgent;
                    _this.ref._lov3.refAgent = agentcombo;
                }
                else {
                    _this.ref._lov3.refAgent = [{ "value": "0", "caption": "--- Select ---" }];
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Error!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmEndUserSetupComponent.prototype.searchKeyup1 = function (e) {
        if (e.which == 13) {
            if (!(this._SearchData.searchcaption.length == 0)) {
                this.goSearch(this._SearchData.searchcaption, this._SearchData.searchvalue, this._SearchData.search);
            }
            else {
                this.showMessageAlert("Please enter data to search..");
            }
        }
    };
    FrmEndUserSetupComponent.prototype.goSearch = function (searchCaption, searchValue, search) {
        var _this = this;
        console.log("you click goSearch");
        this._mflag = false;
        this._custinfo.accArr = [];
        if (searchCaption == '' || searchValue == '' || search == '') {
            this.showMessageAlert("Please enter data to search..");
        }
        else {
            try {
                this.http.doGet(this.ics._apiurl + 'service001/checkCustomerID?searchCaption=' + searchCaption + '&searchValue=' + searchValue + '&searchWay=' + search).subscribe(function (data) {
                    var _selected;
                    if (data.accArr != null) {
                        _this.hideselect = false;
                        if (!(data.accArr instanceof Array)) {
                            var m = [];
                            m[0] = data.accArr;
                            _this._custinfo.accArr = m;
                        }
                        else {
                            _this._custinfo = data;
                            _selected = _this._custinfo.accArr;
                        }
                    }
                    else {
                        _this._SearchData.searchcaption = "";
                        _this._custinfo.accArr = [];
                        _this.showMessageAlert("Customer Data not found");
                    }
                    _this._mflag = true;
                }, function (error) {
                    if (error._body.type == 'error') {
                        alert("Connection Error!");
                    }
                }, function () { });
            }
            catch (e) {
                alert("Invalid URL");
            }
        }
    };
    FrmEndUserSetupComponent.prototype.goCheck = function () {
        var _this = this;
        try {
            this._mflag = false;
            var p = this._obj.accountNumber;
            var check = '';
            if (this.isGL) {
                check = 'GL';
            }
            else {
                check = 'Account';
            }
            this.http.doGet(this.ics._apiurl + 'service001/getCustomerNameandNrcByAccount?account=' + p + '&check=' + check).subscribe(function (data) {
                if (data.msgCode == '0000') {
                    _this._customer = data;
                    _this._obj.chkAccount = _this._customer.chkAccount;
                    _this._obj.chkGL = _this._customer.chkGL;
                    _this._obj.accountNumber = _this._obj.accountNumber.trim();
                    if (_this._customer.chkAccount) {
                        jQuery("#mySave").prop("disabled", false);
                        jQuery("#lu001popup2").modal();
                        _this.isvalidate = "Validate";
                    }
                    else if (_this._customer.chkGL) {
                        jQuery("#mySave").prop("disabled", false);
                        jQuery("#glpopup").modal();
                        _this.isvalidate = "Validate";
                        _this.isGL = true;
                    }
                }
                else {
                    _this._returnResult.msgDesc = "Invalid Account Number/GL";
                    _this.isvalidate = "";
                    _this.showMessageAlert(_this._returnResult.msgDesc);
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Error!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmEndUserSetupComponent.prototype.clearData = function () {
        this._dates.dobDate = null;
        this.isvalidate = "";
        this.chkIsAgent = false;
        this.isGL = false;
        this._showlist = { "data": [{ "customerid": "" }] };
        this._obj = this.getDefaultObj();
        this.goRemove(0);
        this.city = [{ "code": "", "despEng": "" }];
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "loginID": "", "phNo": "", "parentID": "" };
        this._chkloginId = 'false';
        jQuery("#mydelete").prop("disabled", true);
        jQuery("#mySave").prop("disabled", true);
    };
    FrmEndUserSetupComponent.prototype.goNew = function () {
        this._dates.dobDate = null;
        this._chkloginId = 'false';
        this.isvalidate = "";
        this.clearData();
        this.goRemove(0);
        jQuery("#mySave").prop("disabled", false);
        jQuery("#mydelete").prop("disabled", true);
    };
    FrmEndUserSetupComponent.prototype.goDelete = function () {
        var _this = this;
        try {
            this._mflag = false;
            this._obj.userId = this.ics._profile.userID;
            this._obj.sessionId = this.ics._profile.sessionID;
            var url = this.ics._apiurl + 'service001/deleteUser?sessionID=' + this.ics._profile.sessionID + '&userIDForSession=' + this.ics._profile.userID;
            var json = this._obj;
            this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "loginID": "", "phNo": "", "parentID": "" };
            this.http.doPost(url, json).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMessage();
                }
                else {
                    _this._returnResult = data;
                    _this.showMessageAlert(data.msgDesc);
                    if (_this._returnResult.state) {
                        _this.clearData();
                        _this.getAllAgent();
                    }
                }
                _this.goRemove(0);
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Error!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmEndUserSetupComponent.prototype.goAddAll = function (p) {
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "loginID": "", "phNo": "", "parentID": "" };
        if (this._obj.customerid == '') {
            this._returnResult.state = "false";
            this._returnResult.msgDesc = "Blank Customer Id";
        }
        if (this._showlist.data.length > 0) {
            for (var i = 0; i < this._showlist.data.length; i++) {
                if (this._obj.customerid == this._showlist.data[i].customerid) {
                    this._returnResult.state = "false";
                    this._returnResult.msgDesc = "Duplicate Customer Id";
                    break;
                }
            }
        }
        if (this._returnResult.msgDesc == '') {
            this._showlist.data.push({ "customerid": this._obj.customerid });
        }
        else {
            this.showMessageAlert(this._returnResult.msgDesc);
        }
    };
    FrmEndUserSetupComponent.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    FrmEndUserSetupComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsgAlert(data.desc);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsgAlert(data.desc);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmEndUserSetupComponent.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmEndUserSetupComponent.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    FrmEndUserSetupComponent = __decorate([
        core_1.Component({
            selector: 'endusersetup',
            template: "\n    <div class=\"container\">\n    <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n    <form class= \"form-horizontal\" (ngSubmit) = \"goSave()\"> \n    <!-- Form Name -->\n    <legend>Create User</legend>\n    <div class=\"row  col-md-12\">  \n      <button class=\"btn btn-primary\" type=\"button\" (click)=\"goList()\" >List</button> \n      <button class=\"btn btn-primary\" type=\"button\" (click)=\"goNew()\" >New</button>      \n      <button class=\"btn btn-primary\" id=\"mySave\" type=\"submit\" >Save</button>          \n      <button class=\"btn btn-primary\" disabled id=\"mydelete\"  type=\"button\" (click)=\"goDelete();\" >Delete</button> \n    </div>\n    <div class=\"row col-md-12\">&nbsp;</div>\n    <div class=\"row col-md-12\" id=\"custom-form-alignment-margin\">\n     <div class=\"col-md-6\">\n    <div class=\"form-group\">\n    <label class=\"col-md-4\" > Phone No. <font class=\"mandatoryfont\" >*</font></label>\n        <div class=\"col-md-8\">\n            <input type=\"text\" required=\"true\" class=\"form-control\" [(ngModel)]=\"_obj.t4\" [ngModelOptions]=\"{standalone: true}\" />\n        </div>\n    </div>      \n    <div class=\"form-group\"> \n      <label class=\"col-md-4\"> Customer ID <font class=\"mandatoryfont\" >*</font></label>\n      <div class=\"col-md-8\"  style=\"padding-left:16px !important;\">   \n        <div class=\"input-group\">\n          <input  class=\"form-control\" required=\"true\" type = \"text\" [(ngModel)]=\"_obj.customerid\" [ngModelOptions]=\"{standalone: true}\" readonly> \n          <span class=\"input-group-btn\">\n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"lookuppopup();\" >&equiv;</button> \n            <button class=\"btn btn-primary\" style=\"margin-left:5px\" type=\"button\" (click)=\"goAddAll()\" >Add</button> \n          </span>\n        </div>\n      </div>\n    </div>\n      \n    <div class=\"form-group\">\n      <label class=\"col-md-4\" > Name <font class=\"mandatoryfont\" >*</font></label>\n      <div class=\"col-md-8\">\n          <input  class=\"form-control\" type = \"text\" [(ngModel)]=\"_obj.name\" [ngModelOptions]=\"{standalone: true}\" required=\"true\" maxlength=\"50\">                              \n      </div>  \n  </div>\n\n  <div class=\"form-group\">\n    <label class=\"col-md-4\" > NRC <font class=\"mandatoryfont\" >*</font></label>\n    <div class=\"col-md-8\">\n        <input  class=\"form-control\" type = \"text\" [(ngModel)]=\"_obj.t7\" [ngModelOptions]=\"{standalone: true}\" required=\"true\">                              \n    </div>\n  </div>\n\n  <div class=\"form-group\">\n    <label class=\"col-md-4\" > Date of Birth <font class=\"mandatoryfont\" >*</font></label>\n    <div class=\"col-md-8\">\n        <my-date-picker name=\"mydate\" [options]=\"myDatePickerOptions\" [(ngModel)]=\"_dates.dobDate\" required=\"true\" ngDefaultControl></my-date-picker>\n    </div>\n  </div> \n\n  <div class=\"form-group\">\n    <label class=\"col-md-4\" > Email &nbsp;&nbsp;</label>\n    <div class=\"col-md-8\">\n      <input  class=\"form-control\" type = \"text\" [(ngModel)]=\"_obj.t3\" [ngModelOptions]=\"{standalone: true}\">                              \n    </div> \n  </div>\n\n  <div class=\"form-group\">\n    <label class=\"col-md-4\" >  &nbsp; &nbsp;&nbsp;</label>\n    <div class=\"col-md-8\">\n      <input type = \"checkbox\" [(ngModel)]=chkIsAgent [ngModelOptions]=\"{standalone: true}\" (change)=\"checkAgent($event)\"> &nbsp; Is Agent                         \n    </div> \n  </div> \n\n  <div *ngIf=\"chkIsAgent == true\">\n    <div class=\"form-group\" >\n      <label class=\"col-md-4\" > Agent Head <font class=\"mandatoryfont\" >*</font></label>\n      <div class=\"col-md-8\" > \n        <select [(ngModel)]=_obj.parentID [ngModelOptions]=\"{standalone: true}\" required=\"true\" (change)=\"changeAgentHead($event)\"  class=\"form-control col-md-0\"  >\n          <option *ngFor=\"let item of ref._lov3.refAgent\" value=\"{{item.value}}\" >{{item.caption}}</option> \n        </select> \n      </div>         \n    </div>\n              \n    <div class=\"form-group\">\n      <label class=\"col-md-4\"> Account Number/GL <font class=\"mandatoryfont\" >*</font> </label>\n      <div class=\"col-md-8\">\n        <div class=\"input-group\">\n          <input type=\"text\" [(ngModel)]=_obj.accountNumber [ngModelOptions]=\"{standalone: true}\" required=\"true\" *ngIf=\"isvalidate != ''\"  readonly  class=\"form-control col-md-0\">\n          <input type=\"text\" [(ngModel)]=_obj.accountNumber [ngModelOptions]=\"{standalone: true}\" required=\"true\" *ngIf=\"isvalidate == ''\"  class=\"form-control col-md-0\">\n          <span class=\"input-group-btn\">\n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"goCheck()\"><i class=\"glyphicon glyphicon-ok-sign\"></i> Check </button>\n          </span>\n        </div>\n      </div>\n    </div>  \n\n    <div class=\"form-group\">\n      <label class=\"col-md-4\" >  &nbsp; &nbsp;&nbsp;</label>\n      <div class=\"col-md-8\">\n        <input type = \"checkbox\" [(ngModel)]=isGL [ngModelOptions]=\"{standalone: true}\" (change)=\"checkGL($event)\"> &nbsp; Is GL \n        <label style=\"color:green;font-size:15px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{isvalidate}}</label>                      \n      </div> \n    </div>\n  </div> \n </div>\n\n     \n    <div class=\"col-md-6\"> \n        <div class=\"form-group\" >\n          <label class=\"col-md-4\"> Login ID </label>\n          <div class=\"col-md-8\">\n              <input  class=\"form-control\" type = \"text\" [(ngModel)]=\"_obj.t1\" readonly [ngModelOptions]=\"{standalone: true}\">                               \n          </div>\n        </div>\n      \n      <div class=\"form-group\">\n          <label class=\"col-md-4\" > Address &nbsp;&nbsp;</label>\n          <div class=\"col-md-8\" > \n              <textarea  class=\"form-control\" [(ngModel)]=\"_obj.t5\" rows=\"3\" maxlength=\"200\" [ngModelOptions]=\"{standalone: true}\"></textarea>                                \n          </div> \n      </div>\n      \n        <div class=\"form-group\"> \n          <label class=\"col-md-4\">State &nbsp;&nbsp;</label>\n          <div class=\"col-md-8\" > \n              <select [(ngModel)]=\"_obj.state\" [ngModelOptions]=\"{standalone: true}\" (change)=\"changeState($event)\" class=\"form-control col-md-0\">\n                <option *ngFor=\"let item of ref._lov3.refstate\" value=\"{{item.value}}\">{{item.caption}}</option> \n              </select> \n          </div>\n        </div>\n      \n        <div class=\"form-group\"> \n          <label class=\"col-md-4\">City &nbsp;&nbsp;</label>\n          <div class=\"col-md-8\" > \n              <select [(ngModel)]= \"_obj.city\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control col-md-0\">\n                <option *ngFor=\"let item of city\" value=\"{{item.code}}\" >{{item.despEng}}</option> \n              </select> \n          </div>      \n        </div>   \n\n        <div class=\"form-group\">\n          <div class=\"col-md-8 col-md-offset-4 control-label\">\n            <table class=\"table table-striped table-condensed table-hover tblborder\" style=\"font-size:14px;\">\n              <thead>\n                <tr>\n                  <th class = \"col-md-1\" >  Sr No.</th>\n                  <th>Customer ID</th>\n                  <th >\n                      <div id=\"margin:left;display:inline-block;\">&nbsp; </div>\n                  </th>                \n                </tr>\n              </thead>                \n              <tbody >\n                <tr *ngFor = \"let obj of _showlist.data,let i=index\">                    \n                  <td class = \"col-md-2 right\" >  {{i+1 }}</td> \n                  <td class = \"col-md-10 center\" > {{obj.customerid}} </td>\n                  <td> \n                      <img src=\"image/remove.png\" alt=\"remove.png\" height=\"20\" width=\"20\"  (click)=\"goRemove(obj)\"/>\n                  </td>\n                </tr>\n              </tbody>\n            </table>\n          </div>\n        </div>      \n      </div>     \n    </div>\n\n</form>\n</div>\n</div>\n</div>\n\n\n<div id=\"popup\" class=\"modal fade\" role=\"dialog\">\n<div id=\"popupsize\" class=\"modal-dialog modal-lg\">  \n    <div class=\"modal-content\"> \n    <div class=\"modal-header\">\n      <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n      <h4 id=\"popuptitle\" class=\"modal-title\">Customer Info Criteria</h4>\n    </div> \n    <div id=\"popupbody\" class=\"modal-body\">\n    <div class=\"form-group\"> \n      <div class=\"col-md-3\">\n          <div class=\"form-group\"> \n            <select [(ngModel)]=_SearchData.searchvalue class=\"form-control col-md-0\" tabindex=\"1\">\n              <option  *ngFor=\"let item of ref._lov1.ref017\" value=\"{{item.value}}\">{{item.caption}}</option>\n            </select>\n          </div>\n        </div> \n        \n        <div class=\"col-md-3\">\n          <div class=\"form-group\"> \n            <select [(ngModel)]=_SearchData.search class=\"form-control col-md-0\" tabindex=\"2\">\n              <option  *ngFor=\"let item of ref._lov1.ref023\" value=\"{{item.value}}\">{{item.caption}}</option>\n            </select>\n          </div>\n        </div> \n   \n        <div class=\"col-md-4\">\n          <div class=\"input-group\"> \n            <input class=\"form-control\" type=\"text\" [(ngModel)]=\"_SearchData.searchcaption\" (keyup)=\"searchKeyup1($event)\" tabindex=\"3\"> \n            <!--<span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-search\" style=\"cursor: pointer\" tabindex=\"4\" (keyup)=\"searchKeyup1($event)\" (click)=\"goSearch(_SearchData.searchcaption,_SearchData.searchvalue,_SearchData.search)\"></i></span>-->\n            <span class=\"input-group-btn input-md\">\n            <button class=\"btn btn-primary input-md\" type=\"button\" ((keyup)=\"searchKeyup1($event)\" (click)=\"goSearch(_SearchData.searchcaption,_SearchData.searchvalue,_SearchData.search)\"  tabindex=\"4\">\n              <span class=\"glyphicon glyphicon-search\"></span>Search\n            </button>\n          </span> \n          </div>\n        </div>\n    </div>           \n        \n\n  <div class=\"form-group\" [hidden]=\"hideselect\">  \n  <table class=\"table table-striped table-condensed table-hover tblborder\" style=\"font-size:14px;\">\n    <thead>\n      <tr>\n        <th>Sr No.</th>\n        <th>Customer ID</th>\n        <th>Name</th> \n        <th>NRC</th>\n        <th>Account</th>  \n        <th>Currency</th>             \n      </tr>\n    </thead>\n    <tbody>\n      <tr *ngFor=\"let obj of _custinfo.accArr,let i=index\">\n        <td>{{i+1}}</td>\n        <td> <a (click)=\"gotobindCIF(obj)\">{{obj.cif}}</a></td>\n        <td>{{obj.name}}</td>\n        <td>{{obj.nrc}}</td> \n        <td>{{obj.account}}</td>\n        <td>{{obj.ccy}}</td> \n      </tr> \n    </tbody>\n  </table> \n  \n</div> \n<div style = \"width : 500px; height: 20px\">&nbsp;</div>\n</div>\n<div class=\"modal-footer\">\n<button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\">Close</button>\n</div>\n\n</div>\n</div>\n</div>\n\n\n<div id=\"glpopup\" class=\"modal fade\" role=\"dialog\">\n<div id=\"lu001popupsize\" class=\"modal-dialog modal-lg\">  \n  <div class=\"modal-content\">\n    <div class=\"modal-header\">\n      <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n      <h4 id=\"lu001popuptitle\" class=\"modal-title\">GL Info</h4>\n    </div> \n    <div id=\"lu001popupbody\" class=\"modal-body\"> \n      <label> GL Description : {{_customer.glDesp}} </label>\n    </div>\n    <div class=\"modal-footer\">\n      <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\">Close</button>\n    </div>\n  </div>\n</div>\n</div>\n      \n<!--Vaildate pop up -->\n<div id=\"lu001popup2\" class=\"modal fade\" role=\"dialog\">\n<div id=\"lu001popupsize\" class=\"modal-dialog modal-lg\">  \n  <div class=\"modal-content\">\n    <div class=\"modal-header\">\n      <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n      <h4 id=\"lu001popuptitle\" class=\"modal-title\">Customer Info</h4>\n    </div> \n    <div id=\"lu001popupbody\" class=\"modal-body\"> \n        <table class=\"table table-striped table-condensed table-hover tblborder\">\n            <thead>\n              <tr>\n                <th class=\"col-md-6\">Customer Name</th>\n                <th class=\"col-md-6\">NRC</th>                \n              </tr>\n            </thead>\n            <tbody>\n              <tr>\n                <td>{{_customer.name}}</td>\n                <td>{{_customer.nrc}}</td> \n              </tr> \n            </tbody>\n          </table>  \n    </div>\n    <div class=\"modal-footer\">\n      <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\">Close</button>\n    </div>\n    \n  </div>\n</div>\n</div>\n    <div id=\"sessionalert\" class=\"modal fade\">\n        <div class=\"modal-dialog\">\n            <div class=\"modal-content\">\n                <div class=\"modal-body\">\n                  <p>{{sessionAlertMsg}}</p>\n                </div>\n                <div class=\"modal-footer\"> </div>\n            </div>\n        </div>\n    </div>\n\n    <div [hidden] = \"_mflag\">\n      <div class=\"modal\" id=\"loader\"></div>\n    </div> \n    \n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmEndUserSetupComponent);
    return FrmEndUserSetupComponent;
}());
exports.FrmEndUserSetupComponent = FrmEndUserSetupComponent;
//# sourceMappingURL=frmendusersetup.component.js.map