"use strict";
var router_1 = require('@angular/router');
var frmactivateuser_component_1 = require('./frmactivateuser.component');
var frmactivateuserList_component_1 = require('./frmactivateuserList.component');
var frmdeactivateuser_component_1 = require('./frmdeactivateuser.component');
var frmdeactivateuserList_component_1 = require('./frmdeactivateuserList.component');
var frmlockuser_component_1 = require('./frmlockuser.component');
var frmlockuserList_component_1 = require('./frmlockuserList.component');
var frmunlockuser_component_1 = require('./frmunlockuser.component');
var frmunlockuserList_component_1 = require('./frmunlockuserList.component');
var frmresetpassword_component_1 = require('./frmresetpassword.component');
var frmprintpassword_component_1 = require('./frmprintpassword.component');
var frmendusersetup_component_1 = require('./frmendusersetup.component');
var frmendusersetupList_component_1 = require('./frmendusersetupList.component');
var frmEndUserRoutes = [
    { path: 'Activate User', component: frmactivateuser_component_1.FrmActivateUserComponent },
    { path: 'Activate User/:cmd', component: frmactivateuser_component_1.FrmActivateUserComponent },
    { path: 'Activate User/:cmd/:id', component: frmactivateuser_component_1.FrmActivateUserComponent },
    { path: 'ActivateUserList', component: frmactivateuserList_component_1.FrmActivateUserListComponent },
    { path: 'Deactivate User', component: frmdeactivateuser_component_1.FrmDeactivateUserComponent },
    { path: 'Deactivate User/:cmd', component: frmdeactivateuser_component_1.FrmDeactivateUserComponent },
    { path: 'Deactivate User/:cmd/:id', component: frmdeactivateuser_component_1.FrmDeactivateUserComponent },
    { path: 'deactivateuserlist', component: frmdeactivateuserList_component_1.FrmDeactivateUserListComponent },
    { path: 'Lock User', component: frmlockuser_component_1.FrmLockUserComponent },
    { path: 'Lock User/:cmd', component: frmlockuser_component_1.FrmLockUserComponent },
    { path: 'Lock User/:cmd/:id', component: frmlockuser_component_1.FrmLockUserComponent },
    { path: 'lockuserlist', component: frmlockuserList_component_1.FrmLockUserListComponent },
    { path: 'Unlock User', component: frmunlockuser_component_1.FrmUnlockUserComponent },
    { path: 'Unlock User/:cmd', component: frmunlockuser_component_1.FrmUnlockUserComponent },
    { path: 'Unlock User/:cmd/:id', component: frmunlockuser_component_1.FrmUnlockUserComponent },
    { path: 'unlockuserlist', component: frmunlockuserList_component_1.FrmUnlockUserListComponent },
    { path: 'Reset Password', component: frmresetpassword_component_1.FrmResetPasswordComponent },
    { path: 'Print Password', component: frmprintpassword_component_1.FrmPrintPasswordComponent },
    { path: 'Create User', component: frmendusersetup_component_1.FrmEndUserSetupComponent },
    { path: 'Create User/:cmd', component: frmendusersetup_component_1.FrmEndUserSetupComponent },
    { path: 'Create User/:cmd/:id', component: frmendusersetup_component_1.FrmEndUserSetupComponent },
    { path: 'endusersetuplist', component: frmendusersetupList_component_1.FrmEndUserSetupListComponent },
];
exports.frmEndUserRouting = router_1.RouterModule.forChild(frmEndUserRoutes);
//# sourceMappingURL=enduser.routing.js.map