"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var Rx_1 = require('rxjs/Rx');
core_2.enableProdMode();
var FrmLockUserComponent = (function () {
    function FrmLockUserComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
        this._obj = this.getDefaultObj();
        this.sessionAlertMsg = "";
        this._key = "";
        this._mflag = false;
        this._dates = { "date1": "", "date2": "" };
        this._util = new rp_client_util_1.ClientUtil();
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = true;
            this.checkSession();
            jQuery("#mylock").prop("disabled", true);
            this.msghide = true;
            this._obj = this.getDefaultObj();
        }
    }
    FrmLockUserComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this._key = id;
                _this.goReadBySyskey(id);
            }
        });
    };
    FrmLockUserComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    FrmLockUserComponent.prototype.getDefaultObj = function () {
        return { "createdDate": "", "name": "", "t1": "", "t4": "", "t7": "" };
    };
    FrmLockUserComponent.prototype.goReadBySyskey = function (p) {
        var _this = this;
        try {
            this._mflag = false;
            var url = this.ics._apiurl + 'service001/getUserProfileData?sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
            var json = p;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMessage();
                }
                else {
                    _this._obj = data;
                    _this.registeredDate = _this._obj.createdDate;
                    _this._obj.createdDate = _this.registeredDate.substring(0, 4) + "-" + _this.registeredDate.substring(4, 6) + "-" + _this.registeredDate.substring(6, _this.registeredDate.length);
                    jQuery("#mylock").prop("disabled", false);
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmLockUserComponent.prototype.goList = function () {
        this._router.navigate(['/lockuserlist']);
    };
    FrmLockUserComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this.goLogOut();
        });
    };
    FrmLockUserComponent.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    /* checkSession() {
      try {
        
        let url: string = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
        this.http.doGet(url).subscribe(
          data => {
            if (data.msgCode == '0016') {
              this.sessionAlertMsg = data.msgDesc;
              this.showMessage();
            }
          },
          error => {
            if (error._body.type == 'error') {
              alert("Connection Timed Out!");
            }
            else {
  
            }
          }, () => { });
      } catch (e) {
        alert("Invalid URL");
      }
  
    } */
    FrmLockUserComponent.prototype.confirmLock = function () {
        jQuery("#lockconfirm").modal('show');
    };
    FrmLockUserComponent.prototype.showMessageAlert = function (msg) {
        /* if (bool == "true") { this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg }); }
        if (bool == "false") { this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg }); }
        if (bool == "undefined") { this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg }); } */
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmLockUserComponent.prototype.messagealert = function () {
        var _this = this;
        this.messagehide = false;
        setTimeout(function () { return _this.messagehide = true; }, 3000);
    };
    FrmLockUserComponent.prototype.goLock = function () {
        var _this = this;
        try {
            jQuery("#lockconfirm").modal('hide');
            this._mflag = false;
            var url = this.ics._apiurl + 'service001/lockunlockUser?k=lock&userid=' + this.ics._profile.userID + '&sessionID=' + this.ics._profile.sessionID;
            var json = this._key;
            this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
            this.http.doPost(url, json).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMessage();
                }
                else {
                    _this._returnResult = data;
                    if (_this._returnResult.state) {
                        _this.msg = "Locked";
                        _this.msghide = false;
                        jQuery("#mylock").prop("disabled", true);
                    }
                    _this.showMessageAlert(data.msgDesc);
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmLockUserComponent.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    FrmLockUserComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsgAlert(data.desc);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsgAlert(data.desc);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmLockUserComponent.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmLockUserComponent.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    FrmLockUserComponent = __decorate([
        core_1.Component({
            selector: 'lockuser',
            template: "\n    <div class=\"container\">\n    <div class=\"row clearfix\">\n    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n    <form class= \"form-horizontal\"> \n    <!-- Form Name -->\n    <legend>Lock User</legend>\n    <div class=\"row  col-md-12\">    \n      <button class=\"btn btn-primary\" type=\"button\"  (click)=\"goList()\">List</button>\n      <button class=\"btn btn-primary\" disabled id=\"mylock\"  type=\"button\"  (click)=\"confirmLock()\">Lock</button>\n    </div>\n    <div class=\"row col-md-12\">&nbsp;</div>\n    <div class=\"form-group\">    \n    <div class=\"col-md-10\">\n    <div class=\"form-group\">\n        <rp-input [(rpModel)]=\"_obj.t1\" rpRequired =\"true\" rpType=\"text\" rpLabel=\"Login ID\" rpReadonly=\"true\"></rp-input>\n    </div>\n\n    <div class=\"form-group\">\n        <rp-input [(rpModel)]=\"_obj.name\" rpRequired =\"true\" rpType=\"text\" rpLabel=\"Name\" rpReadonly=\"true\"></rp-input>\n    </div>\n\n    <div class=\"form-group\">\n        <rp-input  rpType=\"text\" rpLabel=\"NRC\" [(rpModel)]=\"_obj.t7\"  rpReadonly=\"true\"></rp-input>\n    </div>\n\n    <div class=\"form-group\">\n        <rp-input  rpType=\"text\" rpLabel=\"Phone No.\" [(rpModel)]=\"_obj.t4\"  rpReadonly=\"true\"></rp-input>   \n    </div>\n    </div> \n    <div class=\"col-md-2\">\n      <div [hidden]=\"msghide\">\n        <h3 align=\"right\"><b>{{msg}}</b></h3>\n      </div>\n    </div>\n    </div>\n\n    </form>\n    </div>\n    </div>\n    </div>\n\n    <div id=\"lockconfirm\" class=\"modal fade\">\n      <div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n          <div class=\"modal-header\">\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n            <h4 class=\"modal-title\">Confirmation</h4>\n          </div>\n          <div class=\"modal-body\">\n            <p>Do you want to lock?</p>\n          </div>\n          <div class=\"modal-footer\">\n            <button type=\"button\" class=\"btn btn-primary\" (click)=\"goLock()\">Yes</button>\n            <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\">Cancel</button>\n          </div>\n        </div>\n      </div>\n    </div>\n    \n    <div id=\"sessionalert\" class=\"modal fade\">\n      <div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n          <div class=\"modal-body\">\n            <p>{{sessionAlertMsg}}</p>\n          </div>\n          <div class=\"modal-footer\">\n          </div>\n        </div>\n      </div>\n    </div>\n\n<div [hidden] = \"_mflag\">\n  <div class=\"modal\" id=\"loader\"></div>\n</div>\n    \n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmLockUserComponent);
    return FrmLockUserComponent;
}());
exports.FrmLockUserComponent = FrmLockUserComponent;
//# sourceMappingURL=frmlockuser.component.js.map