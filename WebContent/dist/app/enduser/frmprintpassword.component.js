"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var Rx_1 = require('rxjs/Rx');
core_2.enableProdMode();
var FrmPrintPasswordComponent = (function () {
    function FrmPrintPasswordComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
        this._obj = this.getDefaultObj();
        this.sessionAlertMsg = "";
        this.isvalidate = "";
        this._mflag = false;
        this._util = new rp_client_util_1.ClientUtil();
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = true;
            this.checkSession();
            jQuery("#myprint").prop("disabled", true);
            this._obj = this.getDefaultObj();
        }
    }
    FrmPrintPasswordComponent.prototype.getDefaultObj = function () {
        return { "name": "", "t1": "", "t2": "", "t7": "" };
    };
    FrmPrintPasswordComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this.goLogOut();
        });
    };
    FrmPrintPasswordComponent.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    /* checkSession() {
      try {
        let url: string = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
        this.http.doGet(url).subscribe(
          data => {
            if (data.msgCode == '0016') {
              this.sessionAlertMsg = data.msgDesc;
              this.showMessage();
            }
          },
          error => {
            if (error._body.type == 'error') {
              alert("Connection Timed Out!");
            }
          }, () => { });
      } catch (e) {
        alert("Invalid URL");
      }
    } */
    FrmPrintPasswordComponent.prototype.goClear = function () {
        this.isvalidate = "";
        jQuery("#myprint").prop("disabled", true);
        this._obj = this.getDefaultObj();
    };
    FrmPrintPasswordComponent.prototype.goCheck = function () {
        var _this = this;
        try {
            this._mflag = false;
            this.http.doGet(this.ics._apiurl + 'service001/getUserNameAndNrc?id=' + this._obj.t1 + '&sessionID=' + this.ics._profile.sessionID + '&userIDForSession=' + this.ics._profile.userID).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMessage();
                }
                else {
                    if (data.t1 == '') {
                        jQuery("#myprint").prop("disabled", true);
                        _this.showMessageAlert("Invalid User ID");
                    }
                    else {
                        _this._obj = data;
                        _this.isvalidate = "Validate";
                        jQuery("#myprint").prop("disabled", false);
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmPrintPasswordComponent.prototype.goPrintPwd = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/markPrinted?userID=' + this._obj.t1 + '&sessionID=' + this.ics._profile.sessionID + '&userIDForSession=' + this.ics._profile.userID;
            this._returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
            this.http.doGet(url).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMessage();
                }
                else {
                    _this._returnResult = data;
                    if (_this._returnResult.state) {
                        var pwdopen = window.open();
                        pwdopen.window.focus();
                        pwdopen.document.write('<!DOCTYPE html><html><head><style media="print">@page{size: auto;margin: 0mm;}</style><title></title></head><body ><P></p>&nbsp;</div></body></html>');
                        pwdopen.document.write('<br>');
                        pwdopen.document.write('<br>');
                        pwdopen.document.write('&emsp;&emsp;&emsp;');
                        pwdopen.document.write("User ID : " + _this._obj.t1);
                        pwdopen.document.write('<br>');
                        pwdopen.document.write('&emsp;&emsp;&emsp;');
                        pwdopen.document.write("Password : " + _this._obj.t2);
                        pwdopen.document.write('<br>');
                        pwdopen.window.print();
                        pwdopen.window.close();
                    }
                    else {
                        _this.showMessageAlert(_this._returnResult.msgDesc);
                    }
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmPrintPasswordComponent.prototype.showMessageAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmPrintPasswordComponent.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    FrmPrintPasswordComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsgAlert(data.desc);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsgAlert(data.desc);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmPrintPasswordComponent.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmPrintPasswordComponent.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    FrmPrintPasswordComponent = __decorate([
        core_1.Component({
            selector: 'printpwd',
            template: "\n  <div class=\"container\">\n    <div class=\"row clearfix\">\n      <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n        <form class= \"form-horizontal\"> \n          <!-- Form Name -->\n          <legend>Print Password</legend>\n          <div class=\"row  col-md-12\"> \n            <button class=\"btn btn-primary\" type=\"button\" disabled id=\"myprint\" (click)=\"goPrintPwd()\" >Print</button>\n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"goClear()\" >Clear</button>   \n          </div>\n          <div class=\"row col-md-12\">&nbsp;</div>\n          <div class=\"row col-md-10\">\n            <div class=\"form-group\">\n              <label class=\"col-md-3\"> Login ID <font class=\"mandatoryfont\" >*</font> </label>\n              <div class=\"col-md-4\">\n                    <div class=\"input-group\">\n                      <input type=\"text\" [(ngModel)]=_obj.t1 [ngModelOptions]=\"{standalone: true}\" required=\"true\"  class=\"form-control col-md-0\">\n                      <span class=\"input-group-btn\">\n                        <button class=\"btn btn-primary\" type=\"button\" (click)=\"goCheck()\"><i class=\"glyphicon glyphicon-ok-sign\"></i> Check </button>\n                      </span>\n                    </div>\n              </div>\n              <div class=\"col-md-2\">\n                <label style=\"color:green;font-size:20px\">&nbsp;{{isvalidate}}</label>   \n              </div>\n            </div>  \n            <div class=\"form-group\">\n              <rp-input [(rpModel)]=\"_obj.name\" rpRequired =\"true\" rpType=\"text\" rpLabel=\"Name\" rpReadonly=\"true\"></rp-input>\n            </div>\n            <div class=\"form-group\">\n              <rp-input  rpType=\"text\" rpLabel=\"NRC\" [(rpModel)]=\"_obj.t7\"  rpReadonly=\"true\"></rp-input>\n            </div>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n\n  <div id=\"sessionalert\" class=\"modal fade\">\n    <div class=\"modal-dialog\">\n      <div class=\"modal-content\">\n        <div class=\"modal-body\">\n          <p>{{sessionAlertMsg}}</p>\n        </div>\n        <div class=\"modal-footer\">\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div [hidden] = \"_mflag\">\n    <div class=\"modal\" id=\"loader\"></div>\n  </div>\n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmPrintPasswordComponent);
    return FrmPrintPasswordComponent;
}());
exports.FrmPrintPasswordComponent = FrmPrintPasswordComponent;
//# sourceMappingURL=frmprintpassword.component.js.map