"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var common_1 = require('@angular/common');
var rp_input_module_1 = require('../framework/rp-input.module');
var mydatepicker_1 = require('mydatepicker');
var frmactivateuser_component_1 = require('./frmactivateuser.component');
var frmactivateuserList_component_1 = require('./frmactivateuserList.component');
var frmdeactivateuser_component_1 = require('./frmdeactivateuser.component');
var frmdeactivateuserList_component_1 = require('./frmdeactivateuserList.component');
var frmlockuser_component_1 = require('./frmlockuser.component');
var frmlockuserList_component_1 = require('./frmlockuserList.component');
var frmunlockuser_component_1 = require('./frmunlockuser.component');
var frmunlockuserList_component_1 = require('./frmunlockuserList.component');
var frmresetpassword_component_1 = require('./frmresetpassword.component');
var frmprintpassword_component_1 = require('./frmprintpassword.component');
var frmendusersetup_component_1 = require('./frmendusersetup.component');
var frmendusersetupList_component_1 = require('./frmendusersetupList.component');
var enduser_routing_1 = require('./enduser.routing');
var rp_http_service_1 = require('../framework/rp-http.service');
var adminpager_module_1 = require('../util/adminpager.module');
var endUserModule = (function () {
    function endUserModule() {
    }
    endUserModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                forms_1.FormsModule,
                rp_input_module_1.RpInputModule,
                enduser_routing_1.frmEndUserRouting,
                adminpager_module_1.AdminPagerModule,
                mydatepicker_1.MyDatePickerModule
            ],
            exports: [],
            declarations: [
                frmactivateuser_component_1.FrmActivateUserComponent,
                frmactivateuserList_component_1.FrmActivateUserListComponent,
                frmdeactivateuser_component_1.FrmDeactivateUserComponent,
                frmdeactivateuserList_component_1.FrmDeactivateUserListComponent,
                frmlockuser_component_1.FrmLockUserComponent,
                frmlockuserList_component_1.FrmLockUserListComponent,
                frmunlockuser_component_1.FrmUnlockUserComponent,
                frmunlockuserList_component_1.FrmUnlockUserListComponent,
                frmresetpassword_component_1.FrmResetPasswordComponent,
                frmprintpassword_component_1.FrmPrintPasswordComponent,
                frmendusersetup_component_1.FrmEndUserSetupComponent,
                frmendusersetupList_component_1.FrmEndUserSetupListComponent,
            ],
            providers: [
                rp_http_service_1.RpHttpService
            ],
            schemas: [core_1.CUSTOM_ELEMENTS_SCHEMA]
        }), 
        __metadata('design:paramtypes', [])
    ], endUserModule);
    return endUserModule;
}());
exports.endUserModule = endUserModule;
//# sourceMappingURL=enduser.module.js.map