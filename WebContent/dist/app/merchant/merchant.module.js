"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var common_1 = require('@angular/common');
var rp_input_module_1 = require('../framework/rp-input.module');
var rp_http_service_1 = require('../framework/rp-http.service');
var mercahant_routing_1 = require('../merchant/mercahant.routing');
var frmmerchantsetup_component_1 = require('../merchant/frmmerchantsetup.component');
var frmmerchantlist_component_1 = require('../merchant/frmmerchantlist.component');
var frmlovsetup_component_1 = require('../merchant/frmlovsetup.component');
var frmlovsetup_list_component_1 = require('../merchant/frmlovsetup-list.component');
var frmpaymenttransactionconfig_component_1 = require('../merchant/frmpaymenttransactionconfig.component');
var frmpaymenttransactionconfig_list_component_1 = require('../merchant/frmpaymenttransactionconfig-list.component');
var adminpager_module_1 = require('../util/adminpager.module');
var FrmMerchantModule = (function () {
    function FrmMerchantModule() {
    }
    FrmMerchantModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                forms_1.FormsModule,
                rp_input_module_1.RpInputModule,
                mercahant_routing_1.frmMerchantRouting,
                adminpager_module_1.AdminPagerModule
            ],
            exports: [],
            declarations: [
                frmmerchantsetup_component_1.FrmMerchantComponent,
                frmmerchantlist_component_1.FrmMerchantListComponent,
                frmlovsetup_component_1.FrmLovSetupComponent,
                frmlovsetup_list_component_1.FrmLOVSetupListComponent,
                frmpaymenttransactionconfig_list_component_1.FrmPaymentTransactionConfigListComponent,
                frmpaymenttransactionconfig_component_1.FrmPaymentTransactionConfigComponent,
            ],
            providers: [
                rp_http_service_1.RpHttpService
            ],
            schemas: [core_1.CUSTOM_ELEMENTS_SCHEMA]
        }), 
        __metadata('design:paramtypes', [])
    ], FrmMerchantModule);
    return FrmMerchantModule;
}());
exports.FrmMerchantModule = FrmMerchantModule;
//# sourceMappingURL=merchant.module.js.map