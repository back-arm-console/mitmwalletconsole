"use strict";
var router_1 = require('@angular/router');
var frmmerchantsetup_component_1 = require('../merchant/frmmerchantsetup.component');
var frmmerchantlist_component_1 = require('../merchant/frmmerchantlist.component');
var frmlovsetup_component_1 = require('../merchant/frmlovsetup.component');
var frmlovsetup_list_component_1 = require('../merchant/frmlovsetup-list.component');
var frmpaymenttransactionconfig_component_1 = require('../merchant/frmpaymenttransactionconfig.component');
var frmpaymenttransactionconfig_list_component_1 = require('../merchant/frmpaymenttransactionconfig-list.component');
var frmMerchantRoutes = [
    { path: 'Merchant Profile', component: frmmerchantsetup_component_1.FrmMerchantComponent },
    { path: 'Merchant Profile/:cmd', component: frmmerchantsetup_component_1.FrmMerchantComponent },
    { path: 'Merchant Profile/:cmd/:id', component: frmmerchantsetup_component_1.FrmMerchantComponent },
    { path: 'MerchantList', component: frmmerchantlist_component_1.FrmMerchantListComponent },
    { path: 'QuickPayLOVSetup', component: frmlovsetup_component_1.FrmLovSetupComponent },
    { path: 'QuickPayLOVSetup/:cmd', component: frmlovsetup_component_1.FrmLovSetupComponent },
    { path: 'QuickPayLOVSetup/:cmd/:id', component: frmlovsetup_component_1.FrmLovSetupComponent },
    { path: 'lovsetuplist', component: frmlovsetup_list_component_1.FrmLOVSetupListComponent },
    { path: 'PaymentTransactionSetup', component: frmpaymenttransactionconfig_component_1.FrmPaymentTransactionConfigComponent },
    { path: 'PaymentTransactionSetup/:cmd', component: frmpaymenttransactionconfig_component_1.FrmPaymentTransactionConfigComponent },
    { path: 'PaymentTransactionSetup/:cmd/:id1/:id2', component: frmpaymenttransactionconfig_component_1.FrmPaymentTransactionConfigComponent },
    { path: 'paytemplatelist', component: frmpaymenttransactionconfig_list_component_1.FrmPaymentTransactionConfigListComponent },
];
exports.frmMerchantRouting = router_1.RouterModule.forChild(frmMerchantRoutes);
//# sourceMappingURL=mercahant.routing.js.map