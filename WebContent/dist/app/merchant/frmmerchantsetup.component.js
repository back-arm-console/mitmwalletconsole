"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var Rx_1 = require('rxjs/Rx');
core_2.enableProdMode();
var FrmMerchantComponent = (function () {
    function FrmMerchantComponent(ics, _router, route, http, ref) {
        this.ics = ics;
        this._router = _router;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._features = [];
        this._selectref = [];
        this._obj = this.getDefaultObj();
        this._note = "";
        this.confirmpwd = "";
        this._output1 = "";
        this._returnResult = { "keyResult": 0, "longResult": "", "state": "", "stringResult": "", "userId": "", "msgCode": "", "msgDesc": "" };
        //_returnResult = { "keyResult": 0, "longResult": "", "msgCode": "", "msgDesc": "", "state": "", "stringResult": "" };
        this._brCode = { "startBrCode": 0, "endBrCode": 0 };
        this._mID = "";
        this._featurevalue = "";
        this._featurecaption = "";
        this._ordervalue = "";
        this._ordercaption = "";
        //_obj = { "sessionID": "", "msgCode": "", "msgDesc": "", "sysKey": 0, "t1": "TBA", "t3": "", "t4": "", "t5": "", "t6": "", "t11": "", "t12": "", "userName": "", "userId": "", "p1": "AA", "accountNumber": "", "branchname": "", "createdUserID": "", "branchCode": "", "processingCode": "", "chkAccount": "false", "chkGL": "false", "feature": "", "data": [] };
        this._list = { "data": [{ "feature": "", "showfeature": "", "accountNumber": "", "branchCode": "", "order": "" }] };
        //_showlist = { "data": [{ "feature": "", "accountNumber": "", "branchCode": "","order":"" }] };
        this._customer = { "customerID": "", "name": "", "nrc": "", "accountNo": "", "balance": "", "message": "", "glDesp": "", "chkAccount": "false", "chkGL": "false", "other": "false" };
        this._obj1 = { "a1": "admin" };
        this._key = "";
        this._ans = "";
        this.catch = "";
        this.message = "";
        this.isvalidate = "";
        this._classname = "";
        this._mflag = false;
        /* checkSession() {
          try {
            let url: string = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
            this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "TBA", "msgCode": "" };
      
            this.http.doGet(url).subscribe(
              data => {
                if (data.msgCode == '0016') {
                  this._returnResult.msgDesc = data.msgDesc;
                  this.showMessage();
                }
              },
              error => {
                if (error._body.type == 'error') {
                  alert("Connection Timed Out!");
                }
              },
              () => { }
            );
          } catch (e) {
            alert("Invalid URL");
          }
        } */
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        // RP Framework 
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = false;
            this.ics.confirmUpload(false);
            this._obj = this.getDefaultObj();
            this.checkSession();
            this.getBrCode();
            this.getAllBranch();
            this.getAllProcessingCode();
            this.getAllFeature();
            this.messagehide = true;
            this.isGL = false;
            this.goRemove(0);
            jQuery("#mydelete").prop("disabled", true);
            jQuery("#mySave").prop("disabled", true);
            this.msghide = true;
        }
    }
    FrmMerchantComponent.prototype.showMessageAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmMerchantComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
                var id = params['id'];
                _this._key = id;
                _this.goReadBysysKey(id);
            }
        });
    };
    FrmMerchantComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    FrmMerchantComponent.prototype.goReadBysysKey = function (p) {
        var _this = this;
        try {
            this._key = p;
            var json = this._key;
            var url = this.ics._apiurl + 'service001/getMerchantDataByID?id=' + this._key + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID;
            this.http.doGet(url).subscribe(function (data) {
                if (data != null) {
                    if (data.data != null) {
                        if (!(data.data instanceof Array)) {
                            var m = [];
                            m[0] = data.data;
                            // this._showlist.data = m;
                            if (data.data.feature != "") {
                                for (var i = 0; i < _this.ref._lov3.refFeature.length; i++) {
                                    if (data.data.feature == _this.ref._lov3.refFeature[i].value) {
                                        _this._featurecaption = _this.ref._lov3.refFeature[i].caption;
                                        //this._showlist.data.push({ "feature": this._featurecaption, "accountNumber": data.data.accountNumber, "branchCode": data.data.branchCode,"order":data.data.order });
                                        break;
                                    }
                                }
                            }
                            _this._list.data = m;
                            _this._obj = {
                                "sessionID": _this.ics._profile.sessionID, "msgCode": data.msgCode, "msgDesc": data.msgDesc, "sysKey": data.sysKey, "t1": data.t1, "t3": data.t3, "t4": data.t4,
                                "t5": data.t5, "t6": data.t6, "t11": data.t11, "t12": data.t12, "userName": data.userName, "userId": data.userId, "p1": "AA", "accountNumber": data.accountNumber,
                                "branchname": data.branchname, "createdUserID": data.createdUserID, "branchCode": data.branchCode, "processingCode": data.processingCode, "chkAccount": "false", "chkGL": "false", "feature": "", "order": data.order, "data": m
                            };
                            if (_this._obj.chkGL == 'true') {
                                _this.isGL = true;
                            }
                        }
                        else {
                            _this._obj = data;
                            for (var i = 0; i < _this.ref._lov3.refFeature.length; i++) {
                                for (var j = 0; j < data.data.length; j++) {
                                    if (data.data[j].feature == _this.ref._lov3.refFeature[i].value) {
                                        _this._featurecaption = _this.ref._lov3.refFeature[i].caption;
                                        _this._featurevalue = _this.ref._lov3.refFeature[i].value;
                                        // this._showlist.data.push({ "feature": this._featurecaption, "accountNumber": data.data[j].accountNumber, "branchCode": data.data[j].branchCode,"order":data.data[j].order });
                                        _this._list.data.push({ "feature": _this._featurevalue, "showfeature": _this._featurecaption, "accountNumber": data.data[j].accountNumber, "branchCode": data.data[j].branchCode, "order": data.data[j].order });
                                        break;
                                    }
                                }
                            }
                            if (_this._obj.chkGL == 'true') {
                                _this.isGL = true;
                            }
                        }
                    }
                    else {
                        _this._obj = data;
                    }
                }
                _this._output1 = JSON.stringify(data);
                _this.confirmpwd = data.t2;
                if (_this._obj.msgCode == '0016') {
                    _this._returnResult.msgDesc = _this._obj.msgDesc;
                    _this.showMessage();
                }
                jQuery("#mydelete").prop("disabled", false);
                jQuery("#mySave").prop("disabled", false);
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmMerchantComponent.prototype.getDefaultObj = function () {
        return { "sessionID": "", "msgCode": "", "msgDesc": "", "sysKey": 0, "t1": "TBA", "t3": "", "t4": "", "t5": "", "t6": "", "t11": "", "t12": "", "userName": "", "userId": "", "p1": "AA", "accountNumber": "", "branchname": "", "createdUserID": "", "branchCode": "", "processingCode": "", "chkAccount": "false", "chkGL": "false", "feature": "", "data": [], "order": "" };
    };
    FrmMerchantComponent.prototype.checkGL = function (event) {
        if (event.target.checked) {
            this.isGL = true;
            this._obj.accountNumber = '';
            jQuery("#mySave").prop("disabled", true);
            this.isvalidate = "";
        }
        else {
            this.isGL = false;
            this._obj.accountNumber = '';
            jQuery("#mySave").prop("disabled", true);
            this.isvalidate = "";
        }
    };
    FrmMerchantComponent.prototype.goDelete = function () {
        var _this = this;
        try {
            this._mflag = false;
            var url = this.ics._apiurl + 'service001/deleteMerchant';
            this._obj.sessionID = this.ics._profile.sessionID;
            this._obj.createdUserID = this.ics._profile.userID;
            var json = this._obj;
            this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "TBA", "msgCode": "" };
            this.http.doPost(url, json).subscribe(function (data) {
                _this._output1 = JSON.stringify(data);
                _this._obj.userId = data.userId;
                _this._returnResult = data;
                if (_this._returnResult.state) {
                    _this._obj = { "sessionID": "", "msgCode": "", "msgDesc": "", "sysKey": 0, "t1": "TBA", "t3": "", "t4": "", "t5": "", "t6": "", "t11": "", "t12": "", "userName": "", "userId": "", "p1": "AA", "accountNumber": "", "branchname": "", "createdUserID": "", "branchCode": "", "processingCode": "", "chkAccount": "false", "chkGL": "false", "feature": "", "order": "", "data": [] };
                    jQuery("#mydelete").prop("disabled", true);
                    jQuery("#mySave").prop("disabled", false);
                    _this._list = { "data": [{ "feature": "", "showfeature": "", "accountNumber": "", "branchCode": "", "order": "" }] };
                    // this._showlist = { "data": [{ "feature": "", "accountNumber": "", "branchCode": "" ,"order":data.data.order}] };
                    _this.isvalidate = "";
                    _this.goRemove(0);
                    _this.showMessageAlert(_this._returnResult.msgDesc);
                    _this._selectref = [];
                }
                else {
                    if (_this._returnResult.msgCode == '0016') {
                        _this._returnResult.msgDesc = _this._returnResult.msgDesc;
                        _this.showMessage();
                    }
                    else {
                        _this.showMessageAlert(_this._returnResult.msgDesc);
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
            }, function () { });
            this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "", "msgCode": "" };
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmMerchantComponent.prototype.validateEmail = function (mail) {
        if (/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(mail)) {
            return (true);
        }
        return (false);
    };
    FrmMerchantComponent.prototype.goPost = function () {
        var _this = this;
        this._mflag = false;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "", "msgCode": "" };
        if (this._list.data.length < 1) {
            this._returnResult.msgDesc = "Please Add Account Account Number/GL";
            this.showMessageAlert(this._returnResult.msgDesc);
            this._mflag = true;
            return false;
        }
        if (!/^([0-9]{7,20})$/.test(this._obj.t4)) {
            this._returnResult.msgDesc = "Contact number is invalid";
            this.showMessageAlert(this._returnResult.msgDesc);
            this._mflag = true;
            return false;
        }
        if (this._obj.t12.length > 0) {
            if (!this.validateEmail(this._obj.t12)) {
                this._returnResult.msgDesc = "CC email address is invalid";
                this.showMessageAlert(this._returnResult.msgDesc);
                this._mflag = true;
                return false;
            }
        }
        if (this._obj.t5.length > 0) {
            if (!this.validateEmail(this._obj.t5)) {
                this._returnResult.msgDesc = "Email address is invalid";
                this.showMessageAlert(this._returnResult.msgDesc);
                this._mflag = true;
                return false;
            }
        }
        if (this._returnResult.msgDesc == '') {
            try {
                this._obj.createdUserID = this.ics._profile.userID;
                this._obj.sessionID = this.ics._profile.sessionID;
                this._mID = this._obj.userId;
                this._obj.data = this._list.data;
                var url = this.ics._apiurl + 'service001/saveMerchant';
                var json = this._obj;
                this.http.doPost(url, json).subscribe(function (data) {
                    _this._output1 = JSON.stringify(data);
                    _this._obj.userId = data.userId;
                    _this._returnResult = data;
                    _this.isvalidate = "";
                    if (!(_this._returnResult.state)) {
                        if (_this._returnResult.msgCode == '0016') {
                            _this._returnResult.msgDesc = _this._returnResult.msgDesc;
                            _this.showMessageAlert(_this._returnResult.msgDesc);
                        }
                        _this.isvalidate = "";
                        _this._obj.sysKey = _this._returnResult.keyResult;
                        _this._obj.userId = _this._mID;
                        _this.message = "";
                        _this._key = _this._returnResult.keyResult + "";
                    }
                    else {
                        jQuery("#mydelete").prop("disabled", false);
                        _this._returnResult.msgDesc = _this._returnResult.msgDesc;
                        _this.showMessageAlert(_this._returnResult.msgDesc);
                    }
                    _this.showMessageAlert(_this._returnResult.msgDesc);
                    _this._mflag = true;
                }, function (error) {
                    if (error._body.type == 'error') {
                        alert("Connection Timed Out!");
                    }
                }, function () { });
            }
            catch (e) {
                alert("Invalid URL");
            }
        }
        else {
            this.showMessageAlert(this._returnResult.msgDesc);
        }
    };
    FrmMerchantComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this._router.navigate(['/login']);
            jQuery("#sessionalert").modal('hide');
        });
    };
    FrmMerchantComponent.prototype.goList = function () {
        this._router.navigate(['/MerchantList']);
    };
    FrmMerchantComponent.prototype.goNew = function () {
        this._list = { "data": [{ "feature": "", "showfeature": "", "accountNumber": "", "branchCode": "", "order": "" }] };
        //this._showlist = { "data": [{ "feature": "", "accountNumber": "", "branchCode": "","order":"" }] };
        this.clearData();
        this.goRemove(0);
    };
    /*
      messagealert() {
        this.messagehide = false;
        setTimeout(() => this.messagehide = true, 3000);
    
      } */
    FrmMerchantComponent.prototype.clearData = function () {
        this._obj = this.getDefaultObj();
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "", "msgCode": "" };
        jQuery("#mydelete").prop("disabled", true);
        this.isvalidate = "";
        this.isGL = false;
        jQuery("#mySave").prop("disabled", true);
        this._selectref = [];
        this._list = { "data": [{ "feature": "", "showfeature": "", "accountNumber": "", "branchCode": "", "order": "" }] };
        //this._showlist = { "data": [{ "feature": "", "accountNumber": "", "branchCode": "" ,"order":""}] };
        this.goRemove(0);
    };
    FrmMerchantComponent.prototype.getAllProcessingCode = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics._apiurl + 'service001/getAllProcessingCode').subscribe(function (data) {
                if (data != null && data != undefined) {
                    _this.ref._lov3.refProcessingCode = data.refProcessingCode;
                }
                else {
                    _this.ref._lov3.refProcessingCode = [{ "value": "", "caption": "Empty" }];
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmMerchantComponent.prototype.getBrCode = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics._apiurl + 'service001/getBrCode').subscribe(function (data) {
                _this._brCode = data;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmMerchantComponent.prototype.getAllBranch = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics._apiurl + 'service001/getAllBranch').subscribe(function (data) {
                if (data != null && data != undefined) {
                    _this.ref._lov3.ref018 = data.ref018;
                }
                else {
                    _this.ref._lov3.ref018 = [{ "value": "", "caption": "Empty" }];
                }
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmMerchantComponent.prototype.goCheck = function () {
        var _this = this;
        this._mflag = false;
        try {
            var p = this._obj.accountNumber;
            var check = '';
            if (this.isGL) {
                check = 'GL';
            }
            else {
                check = 'Account';
            }
            this.http.doGet(this.ics._apiurl + 'service001/getCustomerNameandNrcByAccount?account=' + p + '&check=' + check).subscribe(function (data) {
                if (data.msgCode == '0000') {
                    _this._customer = data;
                    _this._obj.chkAccount = _this._customer.chkAccount;
                    _this._obj.chkGL = _this._customer.chkGL;
                    _this._obj.accountNumber = _this._obj.accountNumber.trim();
                    if (_this._customer.chkAccount) {
                        jQuery("#mySave").prop("disabled", false);
                        jQuery("#lu001popup2").modal();
                        _this.isvalidate = "Validate";
                    }
                    else if (_this._customer.chkGL) {
                        jQuery("#mySave").prop("disabled", false);
                        jQuery("#glpopup").modal();
                        _this.isvalidate = "Validate";
                    }
                }
                else {
                    _this._returnResult.msgDesc = "Invalid Account Number/GL";
                    _this.isvalidate = "";
                    _this.showMessageAlert(_this._returnResult.msgDesc);
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmMerchantComponent.prototype.getOrderList = function () {
        var _this = this;
        try {
            this._mflag = false;
            this.http.doGet(this.ics._apiurl + 'service001/getOrderList').subscribe(function (data) {
                if (data != null && data != undefined) {
                    if (!(data.refOrder instanceof Array)) {
                        var m = [];
                        m[0] = data.refOrder;
                        _this.ref._lov3.refOrder = m;
                    }
                    else {
                        _this.ref._lov3.refOrder = data.refOrder;
                    }
                }
                else {
                    _this.ref._lov3.refOrder = [{ "value": "", "caption": "-" }];
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmMerchantComponent.prototype.getAllFeature = function () {
        var _this = this;
        try {
            this._mflag = false;
            this.http.doGet(this.ics._apiurl + 'service001/getAllFeatures').subscribe(function (data) {
                if (data != null && data != undefined) {
                    if (!(data.refFeature instanceof Array)) {
                        var m = [];
                        m[0] = data.refFeature;
                        _this.ref._lov3.refFeature = m;
                    }
                    else {
                        _this.ref._lov3.refFeature = data.refFeature;
                    }
                }
                else {
                    _this.ref._lov3.refFeature = [{ "value": "", "caption": "-" }];
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    /*getAllFeature(l) {
      this._features = [];
      this.http.doGet(this.ics._apiurl + 'service001/getAllFeatures').subscribe(
        data => {
          if (data != null) {
            if (data.refFeature != null) {
              if (!(data.refFeature instanceof Array)) {
                let m = [{ "caption": "", "value": "", "result": false }];
                m[0].caption = data.refFeature.caption;
                m[0].value = data.refFeature.value;
                this.ref._lov3.refFeature = m;
                this._features = m;
              } else {
                this.ref._lov3.refFeature = data.refFeature;
                for (var i = 0; i < data.refFeature.length; i++) {
                  if (data.refFeature[i].caption != "ALL Feature") {
                    var w = { "caption": "", "value": "", "result": false };
                    var y = {};
                    w['caption'] = data.refFeature[i].caption;
                    w['value'] = data.refFeature[i].value;
                    w['result'] = false;
                    y['id'] = i + '';
                    y['name'] = data.refFeature[i].caption;
                    y['value'] = data.refFeature[i].value;
                    // this._features.push(w);
                    this._features.push(y);
                  }
                }
              }
            }
            if (l == "true") {
              if (this._obj.feature != undefined) {
                this._selectref = [];
                for (var j = 0; j < this._obj.feature.length; j++) {
                  for (var k = 0; k < this._features.length; k++) {
                    if (this._features[k].value == this._obj.feature[j]) {
                      this._selectref[j] = Number(this._features[k].id);
                      break;
                    }
   
                  }
                }
              }
   
            }
          }
        }
      );
    }*/
    FrmMerchantComponent.prototype.checkAcctNoBrCode = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkAcctNoBrCode?branchCode=' + this._obj.branchCode + '&AcctNo=' + this._obj.accountNumber;
            this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "TBA", "msgCode": "" };
            this.http.doGet(url).subscribe(function (data) {
                _this.chkAcBr = data.state;
                window.alert("Valid : " + _this.chkAcBr);
            }, function (error) {
                if (error._body.type == 'error') {
                    alert("Connection Timed Out!");
                }
                else {
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmMerchantComponent.prototype.goAddAll = function (p) {
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "", "msgCode": "" };
        if (this.isvalidate == "") {
            this._returnResult.msgDesc = "Please check Account Number/GL again";
            this.showMessageAlert(this._returnResult.msgDesc);
            return false;
        }
        if (this._obj.accountNumber == '') {
            this._returnResult.msgDesc = "Please Fill Account Number";
            this.showMessageAlert(this._returnResult.msgDesc);
            return false;
        }
        if (this._obj.feature == '' || this._obj.feature == 'undefined') {
            this._returnResult.msgDesc = "Please Select Feature";
            this.showMessageAlert(this._returnResult.msgDesc);
            return false;
        }
        if (this._obj.branchCode == '' || this._obj.branchCode == 'undefined') {
            this._returnResult.msgDesc = "Please Select Branch Code";
            this.showMessageAlert(this._returnResult.msgDesc);
            return false;
        }
        if (this._customer.chkAccount) {
            var accbrcode = this._obj.accountNumber.substring(this._brCode.startBrCode, this._brCode.endBrCode);
            if (accbrcode != this._obj.branchCode) {
                this._returnResult.msgDesc = "Invalid Branch Code";
                this.showMessageAlert(this._returnResult.msgDesc);
                return false;
            }
        }
        if (this._list.data.length > 0) {
            for (var i = 0; i < this._list.data.length; i++) {
                if (this._obj.feature == this._list.data[i].feature) {
                    this._returnResult.msgDesc = "Duplicate Feature";
                    this.showMessageAlert(this._returnResult.msgDesc);
                    return false;
                }
            }
        }
        if (this._returnResult.msgDesc == '') {
            if (this._obj.feature != "") {
                for (var i = 0; i < this.ref._lov3.refFeature.length; i++) {
                    if (this._obj.feature == this.ref._lov3.refFeature[i].value) {
                        this._featurecaption = this.ref._lov3.refFeature[i].caption;
                        break;
                    }
                }
            }
            if (this._obj.order != "") {
                for (var i = 0; i < this.ref._lov3.refOrder.length; i++) {
                    if (this._obj.order == this.ref._lov3.refOrder[i].value) {
                        this._ordercaption = this.ref._lov3.refOrder[i].caption;
                        break;
                    }
                }
            }
            //this._showlist.data.push({ "feature": this._featurecaption, "accountNumber": this._obj.accountNumber, "branchCode": this._obj.branchCode,"order":this._ordercaption });
            this._list.data.push({ "feature": this._obj.feature, "showfeature": this._featurecaption, "accountNumber": this._obj.accountNumber, "branchCode": this._obj.branchCode, "order": "1" });
            this.isvalidate = '';
            this._obj.accountNumber = '';
            this._obj.branchCode = '';
            this._obj.feature = '';
            this.isGL = false;
        }
        else {
            this.showMessageAlert(this._returnResult.msgDesc);
        }
    };
    FrmMerchantComponent.prototype.goRemove = function (i) {
        var sindex = this._list.data.indexOf(i);
        this._list.data.splice(sindex, 1);
        // let index = this._list.data.indexOf(i);
        //this._list.data.splice(sindex, 1);
    };
    FrmMerchantComponent.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    FrmMerchantComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsgAlert(data.desc);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsgAlert(data.desc);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmMerchantComponent.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmMerchantComponent.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    FrmMerchantComponent = __decorate([
        core_1.Component({
            selector: 'merchantprofile',
            template: "\n  <div class=\"container\">\n\t  <div class=\"row clearfix\">\n\t\t  <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n\t\t\t  <form class= \"form-horizontal\" (ngSubmit)=\"goPost()\"> \n\t\t\t\t  <!-- Form Name -->\n\t\t\t\t  <legend>Merchant</legend>\n\t\t\t\t\t<div class=\"row  col-md-12\">    \n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"goList()\">List</button>\n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"goNew()\">New</button>      \n            <button class=\"btn btn-primary\" type=\"submit\">Save</button>\n            <button class=\"btn btn-primary\" disabled id=\"mydelete\" type=\"button\" (click)=\"goDelete();\">Delete</button> \n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"row col-md-12\">&nbsp;</div>\n          <div class=\"col-md-12\" id=\"custom-form-alignment-margin\">\n            <div class=\"col-md-6\">\n              <div class=\"form-group\">\n                <rp-input [(rpModel)]=\"_obj.userId\" rpRequired =\"true\" rpType=\"text\" rpLabelRequired='true' rpLabel=\"Merchant ID\" rpReadonly=\"true\" rpClass=\"col-md-8 control-label\" rpLabelClass=\"col-md-4\"></rp-input>\n              </div>\n\n              <div class=\"form-group\">\n                <rp-input rpType=\"text\" rpLabel=\"Contact Person\" [(rpModel)]=\"_obj.t3\" rpLabelRequired='true' rpReadonly=\"false\" rpClass=\"col-md-8 control-label\" rpLabelClass=\"col-md-4\" rpRequired=\"true\"></rp-input>\n              </div>\n\n            <div class=\"form-group\">\n              <label class=\"col-md-4\"> Account Number/GL </label>\n              <div class=\"col-md-8\">\n                <div class=\"input-group\">\n                  <input type=\"text\" [(ngModel)]=_obj.accountNumber [ngModelOptions]=\"{standalone: true}\" *ngIf=\"isvalidate != ''\"  readonly  class=\"form-control col-md-0\">\n                  <input type=\"text\" [(ngModel)]=_obj.accountNumber [ngModelOptions]=\"{standalone: true}\" *ngIf=\"isvalidate == ''\"  class=\"form-control col-md-0\">\n                  <span class=\"input-group-btn\">\n                    <button class=\"btn btn-primary\" type=\"button\" (click)=\"goCheck()\"><i class=\"glyphicon glyphicon-ok-sign\"></i> Check </button>\n                  </span>\n                </div>\n              </div>\n            </div>  \n\n              <div class=\"form-group\">\n                <label class=\"col-md-4\" >  &nbsp; &nbsp;&nbsp;</label>\n                <div class=\"col-md-8\">\n                  <input type = \"checkbox\" [(ngModel)]=isGL [ngModelOptions]=\"{standalone: true}\" (change)=\"checkGL($event)\"> &nbsp; Is GL \n                  <label style=\"color:green;font-size:15px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{isvalidate}}</label>                      \n                </div> \n              </div>\n\n              <!--<div class=\"form-group\">\n                <label class=\"col-md-4\" align=\"left\">Order&nbsp; <font size=\"4\" color=\"#FF0000\">*</font></label>\n                <div class=\"col-md-8\" > \n                  <select [(ngModel)]=\"_obj.order\" style=\"text-align-last: right;\"  class=\"form-control col-md-0\" [ngModelOptions]=\"{standalone: true}\">\n                    <option *ngFor=\"let item of ref._lov3.refOrder\" value=\"{{item.value}}\" style=\"direction: rtl;\">{{item.caption}}</option>\n                  </select> \n                </div> \n             </div>-->\n\n              <div class=\"form-group\">\n                <rp-input rpType=\"text\" rpLabel=\"Email\" [(rpModel)]=\"_obj.t5\" rpLabelRequired='true' rpReadonly=\"false\" rpClass=\"col-md-8 control-label\" rpLabelClass=\"col-md-4\" rpRequired=\"true\"></rp-input>\n              </div>\n\n              <div class=\"form-group\">\n                <rp-input rpType=\"textarea\" rpLabel=\" Address \" [(rpModel)]=\"_obj.t6\" rpReadonly=\"false\" rpClass=\"col-md-8 control-label\" rpLabelClass=\"col-md-4\"></rp-input>  \n              </div>\n\n              <div class=\"form-group\"> \n                <form><button class=\"btn btn-primary\" id=\"myadd\" (click)=\"goAddAll()\">Add</button></form>\n              </div>\n              \n              <div class=\"form-group\">\n                <table class=\"table table-striped table-condensed table-hover tblborder\" style=\"font-size:14px;\">\n                    <thead>\n                      <tr>\n                        <th> Sr No. </th>\n                        <th> Account Number </th>      \n                        <th> Feature</th>  \n                        <th> Branch Code </th>    \n                        <th> Order </th>   \n                        <th> &nbsp;</th>    \n                      </tr>\n                    </thead>\n                    <tbody>\n                      <tr *ngFor=\"let obj of _list.data,let i=index\">\n                        <td class=\"right\"> {{i+1}} </td> \n                        <td class=\"center\"> {{obj.accountNumber}} </td>\n                        <td> {{obj.showfeature}} </td>  \n                        <td> {{obj.branchCode}} </td> \n                        <td class=\"col-md-2\"> <input id=\"numinput\" min=\"1\" type=\"number\" [(ngModel)]='obj.order' [ngModelOptions]=\"{standalone: true}\"  class=\"form-control col-md-0\">  </td> \n                        <td> <img src=\"image/remove.png\" alt=\"remove.png\" height=\"20\" width=\"20\" (click)=\"goRemove(obj)\"/> </td>\n                      </tr>  \n                    </tbody>\n                </table>\n              </div>\n            </div> \n\n            <div  class=\"col-md-6\">\n              <div class=\"form-group\">\n                <rp-input rpType=\"text\" rpLabel=\" Company Name\" [(rpModel)]=\"_obj.userName\" rpLabelRequired='true' rpReadonly=\"false\" rpClass=\"col-md-8 control-label\" rpLabelClass=\"col-md-4\" rpRequired=\"true\"></rp-input>\n              </div>\n\n              <div class=\"form-group\">\n                <rp-input rpType=\"text\" rpLabel=\" Contact Number\" [(rpModel)]=\"_obj.t4\" rpLabelRequired='true' rpReadonly=\"false\" rpClass=\"col-md-8 control-label\" rpLabelClass=\"col-md-4\" rpRequired=\"true\"></rp-input>\n              </div>\n\n              <div class=\"form-group\">\n                <label class=\"col-md-4\" align=\"left\">Feature&nbsp; <font class=\"mandatoryfont\">*</font></label>\n                <div class=\"col-md-8\" > \n                  <select [(ngModel)]=\"_obj.feature\"  class=\"form-control col-md-0\" [ngModelOptions]=\"{standalone: true}\">\n                    <option *ngFor=\"let item of ref._lov3.refFeature\" value=\"{{item.value}}\">{{item.caption}}</option>\n                  </select> \n                </div> \n              </div>\n\n              <div class=\"form-group\">\n                <label class=\"col-md-4\" align=\"left\"> Branch Code <font size=\"4\" color=\"#FF0000\">*</font></label>\n                <div class=\"col-md-8\">\n                  <select [(ngModel)]=\"_obj.branchCode\" class=\"form-control col-md-0\" [ngModelOptions]=\"{standalone: true}\">\n                    <option *ngFor=\"let item of ref._lov3.ref018\" value=\"{{item.value}}\">{{item.caption}}</option> \n                  </select>                \n                </div> \n              </div> \n\n              <div class=\"form-group\">\n                <rp-input rpType=\"text\" rpLabel=\" CC Email \" [(rpModel)]=\"_obj.t12\" rpReadonly=\"false\" rpClass=\"col-md-8 control-label\" rpLabelClass=\"col-md-4\"></rp-input>\n              </div>\n\n              <div class=\"form-group\">\n                <rp-input rpType=\"textarea\" rpLabel=\" Remark \" [(rpModel)]=\"_obj.t11\" rpReadonly=\"false\" rpClass=\"col-md-8 control-label\" rpLabelClass=\"col-md-4\"></rp-input>       \n              </div> \n            </div>\n          </div>\n          <div class=\"col-md-2\">\n            <div [hidden]=\"msghide\">\n              <h3 align=\"right\"><b>{{msg}}</b></h3>\n            </div>\n          </div>\n\t\t\t  </form>\n\t\t  </div>\n\t  </div>\n  </div>\n\n  <div id=\"lu001popup2\" class=\"modal fade\" role=\"dialog\">\n    <div id=\"lu001popupsize\" class=\"modal-dialog modal-lg\">  \n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n          <h4 id=\"lu001popuptitle\" class=\"modal-title\">Customer Info</h4>\n        </div> \n        <div id=\"lu001popupbody\" class=\"modal-body\"> \n          <table class=\"table table-striped table-condensed table-hover tblborder\">\n            <thead>\n              <tr>\n                <th class=\"col-md-6\">Customer Name</th>\n                <th class=\"col-md-6\">NRC</th>                \n              </tr>\n            </thead>\n            <tbody>\n              <tr>\n                <td>{{_customer.name}}</td>\n                <td>{{_customer.nrc}}</td> \n              </tr> \n            </tbody>\n          </table>  \n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\">Close</button>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div id=\"glpopup\" class=\"modal fade\" role=\"dialog\">\n    <div id=\"lu001popupsize\" class=\"modal-dialog modal-lg\">  \n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n          <h4 id=\"lu001popuptitle\" class=\"modal-title\">GL Info</h4>\n        </div> \n        <div id=\"lu001popupbody\" class=\"modal-body\"> \n          <label> GL Description : {{_customer.glDesp}} </label>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\">Close</button>\n        </div>\n      </div>\n    </div>\n  </div>\n    \n  <div id=\"sessionalert\" class=\"modal fade\">\n    <div class=\"modal-dialog\">\n      <div class=\"modal-content\">\n        <div class=\"modal-body\">\n          <p>{{sessionAlertMsg}}</p>\n        </div>\n        <div class=\"modal-footer\">\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div [hidden] = \"_mflag\">\n    <div class=\"modal\" id=\"loader\"></div>\n  </div> \n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], FrmMerchantComponent);
    return FrmMerchantComponent;
}());
exports.FrmMerchantComponent = FrmMerchantComponent;
//# sourceMappingURL=frmmerchantsetup.component.js.map