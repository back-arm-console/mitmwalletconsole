"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var router_1 = require('@angular/router');
var core_2 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_references_1 = require('../framework/rp-references');
var rp_client_util_1 = require('../util/rp-client.util');
var Rx_1 = require('rxjs/Rx');
core_2.enableProdMode();
// Application Specifi
var FrmPaymentTransactionConfigComponent = (function () {
    function FrmPaymentTransactionConfigComponent(ics, _router, _util, route, http, ref, _DomSanitizer) {
        this.ics = ics;
        this._router = _router;
        this._util = _util;
        this.route = route;
        this.http = http;
        this.ref = ref;
        this._DomSanitizer = _DomSanitizer;
        this._fOrderKeyup = 1;
        this._readonly = "";
        this._mflag = false;
        this._pType = "";
        this._pcheck = true;
        //_flag=false;
        this._ReadOnly = false;
        this._read = true;
        this._hide = "";
        this._classname = "";
        this._timer = 0;
        this._check = false;
        this._type = "love";
        this._Date = "Date";
        this._returnResult = { "keyResult": 0, "longResult": "", "state": "", "stringResult": "", "userId": "", "msgCode": "", "msgDesc": "" };
        this._list = { "merchantID": "", "name": "", "messageCode": "", "messageDesc": "" };
        this._listpopup = { "arr": [{ "merchantID": "", "name": "" }] };
        this._imagename = "";
        this._imgpath = "about:blank";
        this._imgsrc = { "arr": [{ "src": "FormData" }] };
        this._paymentConfigobj = {
            "sysKey": 0, "merchantID": "", "name": "", "processCde": "", "imageValue": "", "imagename": "", "imgfilepath": "", "detailOrder": "",
            "data": [
                { "sysKey": 0, "srno": 1, "hKey": 0, "fieldID": "P1", "disPayField": "TransDate", "fOrder": "0", "dataType": "date", "caption": "Transaction Date", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Date" },
                { "sysKey": 0, "srno": 2, "hKey": 0, "fieldID": "P2", "disPayField": "Amount", "fOrder": "0", "dataType": "number", "caption": "Transaction Amount", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": true, "isOTCMandatory": 0, "isCheckOTCMandatory": true, "isCheckPortal": true, "isCheckPayment": true, "isCheckCounter": true, "readonly": "true", "fieldShow": 3, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Number" },
                { "sysKey": 0, "srno": 3, "hKey": 0, "fieldID": "P3", "disPayField": "CustomerCode", "fOrder": "0", "dataType": "text", "caption": "Customer Code", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": true, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Text" },
                { "sysKey": 0, "srno": 4, "hKey": 0, "fieldID": "P4", "disPayField": "T16", "fOrder": "0", "dataType": "text", "caption": "Invoice No.", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": true, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Text" },
                { "sysKey": 0, "srno": 5, "hKey": 0, "fieldID": "P5", "disPayField": "CustomerName", "fOrder": "0", "dataType": "text", "caption": "Customer Name", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": true, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Text" },
                { "sysKey": 0, "srno": 6, "hKey": 0, "fieldID": "P6", "disPayField": "T25", "fOrder": "0", "dataType": "text", "caption": "UDF1", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": true, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Text" },
                { "sysKey": 0, "srno": 7, "hKey": 0, "fieldID": "R1", "disPayField": "T8", "fOrder": "0", "dataType": "", "caption": "UDF2", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "readonly": "true", "isCheckCounter": true, "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "" },
                { "sysKey": 0, "srno": 8, "hKey": 0, "fieldID": "R2", "disPayField": "T13", "fOrder": "0", "dataType": "", "caption": "UDF3", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "readonly": "true", "isCheckCounter": true, "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "" },
                { "sysKey": 0, "srno": 9, "hKey": 0, "fieldID": "R3", "disPayField": "T10", "fOrder": "0", "dataType": "", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "readonly": "true", "isCheckCounter": false, "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "" },
                { "sysKey": 0, "srno": 10, "hKey": 0, "fieldID": "R4", "disPayField": "T17", "fOrder": "0", "dataType": "", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "" },
                { "sysKey": 0, "srno": 11, "hKey": 0, "fieldID": "R5", "disPayField": "T12", "fOrder": "0", "dataType": "", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "readonly": "true", "isCheckCounter": false, "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "" },
                { "sysKey": 0, "srno": 12, "hKey": 0, "fieldID": "R6", "disPayField": "T11", "fOrder": "0", "dataType": "", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "" },
                { "sysKey": 0, "srno": 13, "hKey": 0, "fieldID": "OPT1", "disPayField": "T15", "fOrder": "0", "dataType": "", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "" },
                { "sysKey": 0, "srno": 14, "hKey": 0, "fieldID": "OPT2", "disPayField": "T7", "fOrder": "0", "dataType": "", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "readonly": "true", "isCheckCounter": false, "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "" },
                { "sysKey": 0, "srno": 15, "hKey": 0, "fieldID": "OPT3", "disPayField": "T19", "fOrder": "0", "dataType": "", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "" },
                { "sysKey": 0, "srno": 16, "hKey": 0, "fieldID": "OPT4", "disPayField": "T20", "fOrder": "0", "dataType": "", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "" },
                { "sysKey": 0, "srno": 17, "hKey": 0, "fieldID": "OPT5", "disPayField": "CustomerNumber", "fOrder": "0", "dataType": "", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "" },
                { "sysKey": 0, "srno": 18, "hKey": 0, "fieldID": "OPT6", "disPayField": "FileUploadDateTime", "fOrder": "0", "dataType": "", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "" },
                { "sysKey": 0, "srno": 19, "hKey": 0, "fieldID": "OPT7", "disPayField": "T24", "fOrder": "0", "dataType": "", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "" },
                { "sysKey": 0, "srno": 20, "hKey": 0, "fieldID": "OPN1", "disPayField": "N7", "fOrder": "0", "dataType": "number", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Number" },
                { "sysKey": 0, "srno": 21, "hKey": 0, "fieldID": "OPN2", "disPayField": "N8", "fOrder": "0", "dataType": "number", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Number" },
                { "sysKey": 0, "srno": 22, "hKey": 0, "fieldID": "OPN3", "disPayField": "N9", "fOrder": "0", "dataType": "number", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Number" },
                { "sysKey": 0, "srno": 23, "hKey": 0, "fieldID": "OPN4", "disPayField": "N17", "fOrder": "0", "dataType": "number", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Number" },
                { "sysKey": 0, "srno": 24, "hKey": 0, "fieldID": "OPN5", "disPayField": "N18", "fOrder": "0", "dataType": "number", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Number" },
                { "sysKey": 0, "srno": 25, "hKey": 0, "fieldID": "OPN6", "disPayField": "N19", "fOrder": "0", "dataType": "number", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Number" },
                { "sysKey": 0, "srno": 26, "hKey": 0, "fieldID": "OPN7", "disPayField": "N20", "fOrder": "0", "dataType": "number", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Number" },
                { "sysKey": 0, "srno": 27, "hKey": 0, "fieldID": "OPN8", "disPayField": "T18", "fOrder": "0", "dataType": "number", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Number" },
                { "sysKey": 0, "srno": 28, "hKey": 0, "fieldID": "OTH1", "disPayField": "FromBranch", "fOrder": "0", "dataType": "text", "caption": "Branch Code", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "isMandatory": 0, "lovRef": "", "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Text" },
                { "sysKey": 0, "srno": 29, "hKey": 0, "fieldID": "OTH2", "disPayField": "FromAccount", "fOrder": "0", "dataType": "text", "caption": "From Account", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Text" },
                { "sysKey": 0, "srno": 30, "hKey": 0, "fieldID": "OTH3", "disPayField": "ToAccount", "fOrder": "0", "dataType": "text", "caption": "To Account", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Text" },
                { "sysKey": 0, "srno": 31, "hKey": 0, "fieldID": "OTH4", "disPayField": "ChequeNo", "fOrder": "0", "dataType": "text", "caption": "Cheque Number", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Text" },
                { "sysKey": 0, "srno": 32, "hKey": 0, "fieldID": "OTH5", "disPayField": "CurrencyCode", "fOrder": "0", "dataType": "text", "caption": "Currency Code", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Text" },
                { "sysKey": 0, "srno": 33, "hKey": 0, "fieldID": "OTH6", "disPayField": "N10", "fOrder": "0", "dataType": "number", "caption": "Is VIP", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Number" }
            ], "messageCode": "", "messageDesc": "", "sessionID": "", "userID": ""
        };
        this._lovsetupobj = { "sysKey": 0, "lovNo": "TBA", "createdDate": "", "modiDate": "", "userID": "User1", "recStatus": "0", "lovDesc2": "", "lovType": "singleselect", "data": [{ "srno": "1", "lovCde": "", "lovDesc1": "" }], "messagecode": "", "messagedesc": "", "sessionID": "" };
        this._flag = "false";
        // about session validation
        this.sessionTimeoutMsg = "";
        this._sessionObj = this.getSessionObj();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
        if (!ics.getRole() || ics.getRole() == 0) {
            this._router.navigate(['/login']);
        }
        else {
            this._mflag = false;
            this._pcheck = true;
            this._pType = "text";
            this._readonly = "true";
            this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "", "msgCode": "" };
            this.goNew();
            this.checkSession();
            this.getLoadLOV();
            this.getLoadProcessingCode();
        }
    }
    ;
    FrmPaymentTransactionConfigComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var cmd = params['cmd'];
            if (cmd != null && cmd != "" && cmd == "read") {
                _this._read = true;
                var syskey = params['id1'];
                var merchantID = params['id2'];
                if (status != "lookup") {
                    _this.getPaymentTransConfigBySysKey(syskey, merchantID);
                }
            }
        });
    };
    FrmPaymentTransactionConfigComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    FrmPaymentTransactionConfigComponent.prototype.getDefaultObj = function () {
        return { "sysKey": 0, "lovNo": "TBA", "createdDate": "", "modiDate": "", "userID": "User1", "recStatus": "0", "lovDesc2": "", "lovType": "singleselect", "data": [{ "srno": "1", "lovCde": "", "lovDesc1": "", "price": 0.0 }], "messageCode": "", "messageDesc": "" };
    };
    FrmPaymentTransactionConfigComponent.prototype.goNew = function () {
        this._read = true;
        this._returnResult = { "keyResult": 0, "longResult": "", "msgDesc": "", "state": "", "stringResult": "", "userId": "", "msgCode": "" };
        this._list = { "merchantID": "", "name": "", "messageCode": "", "messageDesc": "" };
        this._paymentConfigobj = {
            "sysKey": 0, "merchantID": "", "name": "", "processCde": "", "imageValue": "", "imagename": "", "imgfilepath": "", "detailOrder": "",
            "data": [
                { "sysKey": 0, "srno": 1, "hKey": 0, "fieldID": "P1", "disPayField": "TransDate", "fOrder": "0", "dataType": "date", "caption": "Transaction Date", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Date" },
                { "sysKey": 0, "srno": 2, "hKey": 0, "fieldID": "P2", "disPayField": "Amount", "fOrder": "0", "dataType": "number", "caption": "Transaction Amount", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": true, "isOTCMandatory": 0, "isCheckOTCMandatory": true, "isCheckPortal": true, "isCheckPayment": true, "isCheckCounter": true, "readonly": "true", "fieldShow": 3, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Number" },
                { "sysKey": 0, "srno": 3, "hKey": 0, "fieldID": "P3", "disPayField": "CustomerCode", "fOrder": "0", "dataType": "text", "caption": "Customer Code", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": true, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Text" },
                { "sysKey": 0, "srno": 4, "hKey": 0, "fieldID": "P4", "disPayField": "T16", "fOrder": "0", "dataType": "text", "caption": "Invoice No.", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": true, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Text" },
                { "sysKey": 0, "srno": 5, "hKey": 0, "fieldID": "P5", "disPayField": "CustomerName", "fOrder": "0", "dataType": "text", "caption": "Customer Name", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": true, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Text" },
                { "sysKey": 0, "srno": 6, "hKey": 0, "fieldID": "P6", "disPayField": "T25", "fOrder": "0", "dataType": "text", "caption": "UDF1", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": true, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Text" },
                { "sysKey": 0, "srno": 7, "hKey": 0, "fieldID": "R1", "disPayField": "T8", "fOrder": "0", "dataType": "", "caption": "UDF2", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "readonly": "true", "isCheckCounter": true, "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "" },
                { "sysKey": 0, "srno": 8, "hKey": 0, "fieldID": "R2", "disPayField": "T13", "fOrder": "0", "dataType": "", "caption": "UDF3", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "readonly": "true", "isCheckCounter": true, "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "" },
                { "sysKey": 0, "srno": 9, "hKey": 0, "fieldID": "R3", "disPayField": "T10", "fOrder": "0", "dataType": "", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "readonly": "true", "isCheckCounter": false, "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "" },
                { "sysKey": 0, "srno": 10, "hKey": 0, "fieldID": "R4", "disPayField": "T17", "fOrder": "0", "dataType": "", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "" },
                { "sysKey": 0, "srno": 11, "hKey": 0, "fieldID": "R5", "disPayField": "T12", "fOrder": "0", "dataType": "", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "readonly": "true", "isCheckCounter": false, "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "" },
                { "sysKey": 0, "srno": 12, "hKey": 0, "fieldID": "R6", "disPayField": "T11", "fOrder": "0", "dataType": "", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "" },
                { "sysKey": 0, "srno": 13, "hKey": 0, "fieldID": "OPT1", "disPayField": "T15", "fOrder": "0", "dataType": "", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "" },
                { "sysKey": 0, "srno": 14, "hKey": 0, "fieldID": "OPT2", "disPayField": "T7", "fOrder": "0", "dataType": "", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "readonly": "true", "isCheckCounter": false, "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "" },
                { "sysKey": 0, "srno": 15, "hKey": 0, "fieldID": "OPT3", "disPayField": "T19", "fOrder": "0", "dataType": "", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "" },
                { "sysKey": 0, "srno": 16, "hKey": 0, "fieldID": "OPT4", "disPayField": "T20", "fOrder": "0", "dataType": "", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "" },
                { "sysKey": 0, "srno": 17, "hKey": 0, "fieldID": "OPT5", "disPayField": "CustomerNumber", "fOrder": "0", "dataType": "", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "" },
                { "sysKey": 0, "srno": 18, "hKey": 0, "fieldID": "OPT6", "disPayField": "FileUploadDateTime", "fOrder": "0", "dataType": "", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "" },
                { "sysKey": 0, "srno": 19, "hKey": 0, "fieldID": "OPT7", "disPayField": "T24", "fOrder": "0", "dataType": "", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "" },
                { "sysKey": 0, "srno": 20, "hKey": 0, "fieldID": "OPN1", "disPayField": "N7", "fOrder": "0", "dataType": "number", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Number" },
                { "sysKey": 0, "srno": 21, "hKey": 0, "fieldID": "OPN2", "disPayField": "N8", "fOrder": "0", "dataType": "number", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Number" },
                { "sysKey": 0, "srno": 22, "hKey": 0, "fieldID": "OPN3", "disPayField": "N9", "fOrder": "0", "dataType": "number", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Number" },
                { "sysKey": 0, "srno": 23, "hKey": 0, "fieldID": "OPN4", "disPayField": "N17", "fOrder": "0", "dataType": "number", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Number" },
                { "sysKey": 0, "srno": 24, "hKey": 0, "fieldID": "OPN5", "disPayField": "N18", "fOrder": "0", "dataType": "number", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Number" },
                { "sysKey": 0, "srno": 25, "hKey": 0, "fieldID": "OPN6", "disPayField": "N19", "fOrder": "0", "dataType": "number", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Number" },
                { "sysKey": 0, "srno": 26, "hKey": 0, "fieldID": "OPN7", "disPayField": "N20", "fOrder": "0", "dataType": "number", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Number" },
                { "sysKey": 0, "srno": 27, "hKey": 0, "fieldID": "OPN8", "disPayField": "T18", "fOrder": "0", "dataType": "number", "caption": "", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Number" },
                { "sysKey": 0, "srno": 28, "hKey": 0, "fieldID": "OTH1", "disPayField": "FromBranch", "fOrder": "0", "dataType": "text", "caption": "Branch Code", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "isMandatory": 0, "lovRef": "", "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Text" },
                { "sysKey": 0, "srno": 29, "hKey": 0, "fieldID": "OTH2", "disPayField": "FromAccount", "fOrder": "0", "dataType": "text", "caption": "From Account", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Text" },
                { "sysKey": 0, "srno": 30, "hKey": 0, "fieldID": "OTH3", "disPayField": "ToAccount", "fOrder": "0", "dataType": "text", "caption": "To Account", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Text" },
                { "sysKey": 0, "srno": 31, "hKey": 0, "fieldID": "OTH4", "disPayField": "ChequeNo", "fOrder": "0", "dataType": "text", "caption": "Cheque Number", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Text" },
                { "sysKey": 0, "srno": 32, "hKey": 0, "fieldID": "OTH5", "disPayField": "CurrencyCode", "fOrder": "0", "dataType": "text", "caption": "Currency Code", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Text" },
                { "sysKey": 0, "srno": 33, "hKey": 0, "fieldID": "OTH6", "disPayField": "N10", "fOrder": "0", "dataType": "number", "caption": "Is VIP", "regularExLovCode": "", "lovCode": "", "lovDesc2": "", "lovRef": "", "isMandatory": 0, "isCheckMandatory": false, "isOTCMandatory": 0, "isCheckOTCMandatory": false, "isCheckPortal": false, "isCheckPayment": false, "isCheckCounter": false, "readonly": "true", "fieldShow": 0, "lovData": [{ "srno": "", "lovCde": "", "lovDesc1": "" }], "fieldValue": "Number" }
            ], "messageCode": "", "messageDesc": "", "sessionID": "", "userID": ""
        };
        this.goDisable();
        this._ReadOnly = false;
        this.getLoadLOV();
        // let k = params.get("p1");
        // this.getMerchantIDfromPopup(k);
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[2].isCheckMandatory = false) {
                this._paymentConfigobj.data[2].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory1 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory1").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[3].isCheckMandatory = false) {
                this._paymentConfigobj.data[3].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory1").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory2 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory2").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[4].isCheckMandatory = false) {
                this._paymentConfigobj.data[4].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory2").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory3 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory3").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[5].isCheckMandatory = false) {
                this._paymentConfigobj.data[5].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory3").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory4 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory4").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[6].isCheckMandatory = false) {
                this._paymentConfigobj.data[6].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory4").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory5 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory5").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[7].isCheckMandatory = false) {
                this._paymentConfigobj.data[7].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory5").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory6 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory6").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[8].isCheckMandatory = false) {
                this._paymentConfigobj.data[8].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory6").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory7 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory7").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[9].isCheckMandatory = false) {
                this._paymentConfigobj.data[9].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory7").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory8 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory8").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[10].isCheckMandatory = false) {
                this._paymentConfigobj.data[10].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory8").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory9 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory9").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[11].isCheckMandatory = false) {
                this._paymentConfigobj.data[11].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory9").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory10 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory10").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[12].isCheckMandatory = false) {
                this._paymentConfigobj.data[12].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory10").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory11 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory11").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[13].isCheckMandatory = false) {
                this._paymentConfigobj.data[13].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory11").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory12 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory12").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[14].isCheckMandatory = false) {
                this._paymentConfigobj.data[14].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory12").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory13 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory13").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[15].isCheckMandatory = false) {
                this._paymentConfigobj.data[15].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory13").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory14 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory14").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[16].isCheckMandatory = false) {
                this._paymentConfigobj.data[16].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory14").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory15 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory15").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[17].isCheckMandatory = false) {
                this._paymentConfigobj.data[17].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory15").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory16 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory16").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[18].isCheckMandatory = false) {
                this._paymentConfigobj.data[18].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory16").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory17 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory17").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[19].isCheckMandatory = false) {
                this._paymentConfigobj.data[19].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory17").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory18 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory18").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[20].isCheckMandatory = false) {
                this._paymentConfigobj.data[20].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory18").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory19 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory19").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[21].isCheckMandatory = false) {
                this._paymentConfigobj.data[21].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory19").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory20 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory20").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[22].isCheckMandatory = false) {
                this._paymentConfigobj.data[22].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory20").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory21 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory21").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[23].isCheckMandatory = false) {
                this._paymentConfigobj.data[23].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory21").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory22 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory22").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[24].isCheckMandatory = false) {
                this._paymentConfigobj.data[24].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory22").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory23 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory23").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[25].isCheckMandatory = false) {
                this._paymentConfigobj.data[25].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory23").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory24 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory24").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[26].isCheckMandatory = false) {
                this._paymentConfigobj.data[26].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory24").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory25 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory25").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[27].isCheckMandatory = false) {
                this._paymentConfigobj.data[27].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory25").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory26 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory26").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[28].isCheckMandatory = false) {
                this._paymentConfigobj.data[28].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory26").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory27 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory27").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[29].isCheckMandatory = false) {
                this._paymentConfigobj.data[29].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory27").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory28 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory28").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[30].isCheckMandatory = false) {
                this._paymentConfigobj.data[30].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory28").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory29 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory29").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[31].isCheckMandatory = false) {
                this._paymentConfigobj.data[31].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory29").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkMandatory30 = function (options, isCheck, btnmandatory) {
        if (isCheck == true) {
            jQuery("#btnmandatory30").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[32].isCheckMandatory = false) {
                this._paymentConfigobj.data[32].isMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnmandatory30").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkOTCMandatory1 = function (options, isCheck, btnotcmandatory) {
        if (isCheck == true) {
            jQuery("#btnotcmandatory1").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[2].isCheckOTCMandatory = false) {
                this._paymentConfigobj.data[2].isOTCMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnotcmandatory1").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkOTCMandatory2 = function (options, isCheck, btnotcmandatory) {
        if (isCheck == true) {
            jQuery("#btnotcmandatory2").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[3].isCheckOTCMandatory = false) {
                this._paymentConfigobj.data[3].isOTCMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnotcmandatory2").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkOTCMandatory3 = function (options, isCheck, btnotcmandatory) {
        if (isCheck == true) {
            jQuery("#btnotcmandatory3").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[4].isCheckOTCMandatory = false) {
                this._paymentConfigobj.data[4].isOTCMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnotcmandatory3").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkOTCMandatory4 = function (options, isCheck, btnotcmandatory) {
        if (isCheck == true) {
            jQuery("#btnotcmandatory4").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[5].isCheckOTCMandatory = false) {
                this._paymentConfigobj.data[5].isOTCMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnotcmandatory4").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkOTCMandatory5 = function (options, isCheck, btnotcmandatory) {
        if (isCheck == true) {
            jQuery("#btnotcmandatory5").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[6].isCheckOTCMandatory = false) {
                this._paymentConfigobj.data[6].isOTCMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnotcmandatory5").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.checkOTCMandatory6 = function (options, isCheck, btnotcmandatory) {
        if (isCheck == true) {
            jQuery("#btnotcmandatory6").prop("disabled", true); //disabled
            if (this._paymentConfigobj.data[7].isCheckOTCMandatory = false) {
                this._paymentConfigobj.data[7].isOTCMandatory = 0;
            }
        }
        if (isCheck == false) {
            jQuery("#btnotcmandatory6").prop("disabled", false); //enable
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.loadImg = function (event, i) {
        this._paymentConfigobj.imageValue = URL.createObjectURL(event.target.files[0]);
        this._imgpath = URL.createObjectURL(event.target.files[0]);
        this._imagename = event.target.files[0].name; //
        this._paymentConfigobj.imagename = event.target.files[0].name;
        this._paymentConfigobj.imgfilepath = "upload";
        var merchantid = this._paymentConfigobj.merchantID;
        var url = this.ics._apiurl + 'service001/uploadPhoto?f=upload&fn=' + event.target.files[0].name;
        var fd = new FormData();
        var xhr = new XMLHttpRequest();
        for (var i_1 = 0; i_1 < event.target.files.length; i_1++) {
            fd.append("uploads[]", event.target.files[i_1], event.target.files[i_1].name);
        }
        xhr.open('POST', url, true);
        xhr.send(fd);
    };
    FrmPaymentTransactionConfigComponent.prototype.lookuppopup = function () {
        var _this = this;
        if (this.ics._apiurl != "") {
            this.http.doGet(this.ics._apiurl + 'service001/getLovMerchantList').subscribe(function (data) {
                _this._listpopup.arr = data.merchantData;
                if (data != null) {
                    if (data.merchantData != null) {
                        if (!(data.merchantData instanceof Array)) {
                            var m = [];
                            m[0] = data.merchantData;
                            data.merchantData = m;
                        }
                        _this._listpopup.arr = data.merchantData;
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        jQuery("#lu001popup").modal();
    };
    FrmPaymentTransactionConfigComponent.prototype.gotomerchantid = function (merchantid) {
        var _this = this;
        this.http.doGet(this.ics._apiurl + 'service001/getPaymentTransMerchantSetupPopUpByID?payTansMerchantID=' + merchantid + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (data) {
            if (data != null) {
                if (data.messageCode == "0016") {
                    _this.sessionAlertMsg = data.messageDesc;
                    _this.showMessage();
                }
                else {
                    _this._list.merchantID = data.merchantID;
                    _this._list.name = data.name;
                    jQuery("#lu001popup").modal('hide');
                }
            }
        }, function (error) {
            if (error._body.type == "error") {
                alert("Connection Timed Out.");
            }
        }, function () { });
    };
    FrmPaymentTransactionConfigComponent.prototype.goSave = function () {
        var _this = this;
        this._mflag = false;
        for (var i_2 = 0; i_2 < this._paymentConfigobj.data.length; i_2++) {
            if (this._paymentConfigobj.data[i_2].isCheckPayment == false && this._paymentConfigobj.data[i_2].isCheckCounter == false || this._paymentConfigobj.data[i_2].isCheckPortal == false) {
                this._paymentConfigobj.data[i_2].fieldShow = 0;
            }
            if (this._paymentConfigobj.data[i_2].isCheckCounter == true && this._paymentConfigobj.data[i_2].isCheckPayment == false) {
                this._paymentConfigobj.data[i_2].fieldShow = 1;
            }
            if (this._paymentConfigobj.data[i_2].isCheckPayment == true && this._paymentConfigobj.data[i_2].isCheckCounter == false) {
                this._paymentConfigobj.data[i_2].fieldShow = 2;
            }
            if (this._paymentConfigobj.data[i_2].isCheckPayment == true && this._paymentConfigobj.data[i_2].isCheckCounter == true) {
                this._paymentConfigobj.data[i_2].fieldShow = 3;
            }
            if (this._paymentConfigobj.data[i_2].isCheckCounter == true && this._paymentConfigobj.data[i_2].isCheckPortal == true) {
                this._paymentConfigobj.data[i_2].fieldShow = 4;
            }
            if (this._paymentConfigobj.data[i_2].isCheckPayment == true && this._paymentConfigobj.data[i_2].isCheckPortal == true) {
                this._paymentConfigobj.data[i_2].fieldShow = 5;
            }
            if (this._paymentConfigobj.data[i_2].isCheckPayment == true && this._paymentConfigobj.data[i_2].isCheckCounter == true && this._paymentConfigobj.data[i_2].isCheckPortal == true) {
                this._paymentConfigobj.data[i_2].fieldShow = 6;
            }
            if (this._paymentConfigobj.data[i_2].isCheckMandatory == false) {
                this._paymentConfigobj.data[i_2].isMandatory = 0;
            }
            if (this._paymentConfigobj.data[i_2].isCheckMandatory == true) {
                this._paymentConfigobj.data[i_2].isMandatory = 1;
            }
            if (this._paymentConfigobj.data[i_2].isCheckOTCMandatory == false) {
                this._paymentConfigobj.data[i_2].isOTCMandatory = 0;
            }
            if (this._paymentConfigobj.data[i_2].isCheckOTCMandatory == true) {
                this._paymentConfigobj.data[i_2].isOTCMandatory = 1;
            }
        }
        this._paymentConfigobj.merchantID = this._list.merchantID;
        this._paymentConfigobj.name = this._list.name;
        this._paymentConfigobj.sessionID = this.ics._profile.sessionID;
        this._paymentConfigobj.userID = this.ics._profile.userID;
        var url = this.ics._apiurl + 'service001/savePayTranConfing';
        for (var i = 0; i < this._paymentConfigobj.data.length; i++) {
            if (this._paymentConfigobj.data[i].lovCode == null) {
                this._paymentConfigobj.data[i].lovCode = "";
            }
            if (this._paymentConfigobj.data[i].regularExLovCode == null) {
                this._paymentConfigobj.data[i].regularExLovCode = "";
            }
        }
        var json = this._paymentConfigobj;
        this.http.doPost(url, json).subscribe(function (data) {
            _this._paymentConfigobj = data;
            for (var i = 0; i < _this._paymentConfigobj.data.length; i++) {
                if (_this._paymentConfigobj.data[i].dataType == "number") {
                    _this._paymentConfigobj.data[i].fieldValue = "Number";
                }
                if (_this._paymentConfigobj.data[0].dataType == "date") {
                    _this._paymentConfigobj.data[0].fieldValue = "Date";
                }
                if (_this._paymentConfigobj.data[2].dataType == "text") {
                    _this._paymentConfigobj.data[2].fieldValue = "Text";
                }
                if (_this._paymentConfigobj.data[3].dataType == "text") {
                    _this._paymentConfigobj.data[3].fieldValue = "Text";
                }
                if (_this._paymentConfigobj.data[4].dataType == "text") {
                    _this._paymentConfigobj.data[4].fieldValue = "Text";
                }
                if (_this._paymentConfigobj.data[5].dataType == "text") {
                    _this._paymentConfigobj.data[5].fieldValue = "Text";
                }
                if (_this._paymentConfigobj.data[i].dataType == "lov") {
                    _this.callFieldCode1(_this._paymentConfigobj.data[i].dataType, i);
                }
                if (_this._paymentConfigobj.data[i].fieldShow == 0) {
                    _this._paymentConfigobj.data[i].isCheckCounter = false;
                    _this._paymentConfigobj.data[i].isCheckPayment = false;
                    _this._paymentConfigobj.data[i].isCheckPortal = false;
                }
                if (_this._paymentConfigobj.data[i].fieldShow == 1) {
                    _this._paymentConfigobj.data[i].isCheckCounter = true;
                    _this._paymentConfigobj.data[i].isCheckPayment = false;
                    _this._paymentConfigobj.data[i].isCheckPortal = false;
                }
                if (_this._paymentConfigobj.data[i].fieldShow == 2) {
                    _this._paymentConfigobj.data[i].isCheckCounter = false;
                    _this._paymentConfigobj.data[i].isCheckPayment = true;
                    _this._paymentConfigobj.data[i].isCheckPortal = false;
                }
                if (_this._paymentConfigobj.data[i].fieldShow == 3) {
                    _this._paymentConfigobj.data[i].isCheckCounter = true;
                    _this._paymentConfigobj.data[i].isCheckPayment = true;
                    _this._paymentConfigobj.data[i].isCheckPortal = false;
                }
                if (_this._paymentConfigobj.data[i].fieldShow == 4) {
                    _this._paymentConfigobj.data[i].isCheckCounter = true;
                    _this._paymentConfigobj.data[i].isCheckPayment = false;
                    _this._paymentConfigobj.data[i].isCheckPortal = true;
                }
                if (_this._paymentConfigobj.data[i].fieldShow == 5) {
                    _this._paymentConfigobj.data[i].isCheckCounter = false;
                    _this._paymentConfigobj.data[i].isCheckPayment = true;
                    _this._paymentConfigobj.data[i].isCheckPortal = true;
                }
                if (_this._paymentConfigobj.data[i].fieldShow == 6) {
                    _this._paymentConfigobj.data[i].isCheckCounter = true;
                    _this._paymentConfigobj.data[i].isCheckPayment = true;
                    _this._paymentConfigobj.data[i].isCheckPortal = true;
                }
                if (_this._paymentConfigobj.data[i].isMandatory == 0) {
                    _this._paymentConfigobj.data[i].isCheckMandatory = false;
                }
                if (_this._paymentConfigobj.data[i].isMandatory == 1) {
                    _this._paymentConfigobj.data[i].isCheckMandatory = true;
                }
                if (_this._paymentConfigobj.data[i].isOTCMandatory == 0) {
                    _this._paymentConfigobj.data[i].isCheckOTCMandatory = false;
                }
                if (_this._paymentConfigobj.data[i].isOTCMandatory == 1) {
                    _this._paymentConfigobj.data[i].isCheckOTCMandatory = true;
                }
            }
            if (data.messageCode == "0016") {
                _this.sessionAlertMsg = data.messageDesc;
                _this.showMessage();
            }
            else {
                if (data.messageCode == "0000") {
                    _this._classname = "alert alert-success";
                    _this._timer = 2000;
                    _this._returnResult.state = 'true';
                }
                else if (data.messageCode == "0051") {
                    _this._classname = "alert alert-danger";
                    _this._timer = 3000;
                    _this._returnResult.state = 'false'; // save fail
                }
                else if (data.messageCode == "0001") {
                    _this._classname = "alert alert-success";
                    _this._timer = 2000;
                    _this._returnResult.state = 'true';
                }
                else if (data.messageCode == "0050") {
                    //updated fail
                    _this._classname = "alert alert-danger";
                    _this._timer = 3000;
                    _this._returnResult.state = 'false';
                }
                else {
                    _this._classname = "alert alert-danger";
                    _this._timer = 3000;
                    _this._returnResult.state = 'false';
                }
                //   this.goNew();
                _this.alertMessage(data.messageDesc, _this._classname, _this._timer);
                //this.goEnable();
                jQuery("#mydelete").prop("disabled", false);
                jQuery("#mydelete2").prop("disabled", false);
            }
            _this._mflag = true;
        }, function (error) {
            if (error._body.type == "error") {
                alert("Connection Timed Out.");
            }
        }, function () { });
    };
    FrmPaymentTransactionConfigComponent.prototype.goDelete = function () {
        var _this = this;
        try {
            this._mflag = false;
            // this._lovsetupobj.userID = this.ics._profile.userID;
            var url = this.ics._apiurl + 'service001/deletePaymentTemplate?id=' + this.ics._profile.userID + '&sessionID=' + this.ics._profile.sessionID;
            var json = this._paymentConfigobj.sysKey;
            this._returnResult = { "keyResult": 0, "longResult": "", "state": "", "stringResult": "", "userId": "", "msgCode": "", "msgDesc": "" };
            this.http.doPost(url, json).subscribe(function (data) {
                if (data.msgCode == '0016') {
                    _this.sessionAlertMsg = data.msgDesc;
                    _this.showMessage();
                }
                else {
                    _this._returnResult = data;
                    _this.showMessageAlert(data.msgDesc);
                    if (_this._returnResult.state) {
                        _this.goNew();
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.showMessageAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmPaymentTransactionConfigComponent.prototype.alertMessage = function (msg, classname, timer) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmPaymentTransactionConfigComponent.prototype.popupMessage = function (message) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": message });
    };
    FrmPaymentTransactionConfigComponent.prototype.getMerchantIDfromPopup = function (p) {
        var _this = this;
        //payTansMerchantID
        this.http.doGet(this.ics._apiurl + 'service001/getPaymentTransMerchantSetupPopUpByID?payTansMerchantID=' + p + 'sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (data) {
            if (data.messageCode == '0016') {
                _this.sessionAlertMsg = data.messageDesc;
                _this.showMessage();
            }
            else {
                _this._list.merchantID = data.merchantID;
                _this._list.name = data.name;
                jQuery("#lu001popup").modal('hide');
            }
        }, function (error) {
            if (error._body.type == "error") {
                alert("Connection Timed Out.");
            }
        }, function () { });
    };
    FrmPaymentTransactionConfigComponent.prototype.goList = function () {
        this._router.navigate(['/paytemplatelist']);
    };
    FrmPaymentTransactionConfigComponent.prototype.goEnable = function () {
        // jQuery("#myBtnDelete").prop("disabled",false);
        jQuery("#btncopy").prop("disabled", false);
        jQuery("#btnmandatory").prop("disabled", false);
        jQuery("#btnmandatory1").prop("disabled", false);
        jQuery("#btnmandatory2").prop("disabled", false);
        jQuery("#btnmandatory3").prop("disabled", false);
        jQuery("#btnmandatory4").prop("disabled", false);
        jQuery("#btnmandatory5").prop("disabled", false);
        jQuery("#btnmandatory6").prop("disabled", false);
        jQuery("#btnmandatory7").prop("disabled", false);
        jQuery("#btnmandatory8").prop("disabled", false);
        jQuery("#btnmandatory9").prop("disabled", false);
        jQuery("#btnmandatory10").prop("disabled", false);
        jQuery("#btnmandatory11").prop("disabled", false);
        jQuery("#btnmandatory12").prop("disabled", false);
        jQuery("#btnmandatory12").prop("disabled", false);
        jQuery("#btnmandatory14").prop("disabled", false);
        jQuery("#btnmandatory15").prop("disabled", false);
        jQuery("#btnmandatory16").prop("disabled", false);
        jQuery("#btnmandatory17").prop("disabled", false);
        jQuery("#btnmandatory18").prop("disabled", false);
        jQuery("#btnmandatory19").prop("disabled", false);
        jQuery("#btnmandatory20").prop("disabled", false);
        jQuery("#btnmandatory21").prop("disabled", false);
        jQuery("#btnmandatory22").prop("disabled", false);
        jQuery("#btnmandatory23").prop("disabled", false);
        jQuery("#btnmandatory24").prop("disabled", false);
        jQuery("#btnmandatory25").prop("disabled", false);
        jQuery("#btnmandatory26").prop("disabled", false);
        jQuery("#btnmandatory27").prop("disabled", false);
        jQuery("#btnmandatory28").prop("disabled", false);
        jQuery("#btnmandatory29").prop("disabled", false);
        jQuery("#btnmandatory30").prop("disabled", false);
        jQuery("#btnotcmandatory1").prop("disabled", false);
        jQuery("#btnotcmandatory2").prop("disabled", false);
        jQuery("#btnotcmandatory3").prop("disabled", false);
        jQuery("#btnotcmandatory4").prop("disabled", false);
        jQuery("#btnotcmandatory5").prop("disabled", false);
        jQuery("#btnotcmandatory6").prop("disabled", false);
    };
    FrmPaymentTransactionConfigComponent.prototype.enableLovbtn = function () {
        jQuery("#btnlovedit").prop("disabled", false);
        jQuery("#btnlovnew").prop("disabled", false);
    };
    FrmPaymentTransactionConfigComponent.prototype.goDisable = function () {
        //jQuery("#myBtnDelete").prop("disabled",true);
        jQuery("#btncopy").prop("disabled", true);
        jQuery("#btnmandatory").prop("disabled", true);
        jQuery("#btnmandatory1").prop("disabled", true);
        jQuery("#btnmandatory2").prop("disabled", true);
        jQuery("#btnmandatory3").prop("disabled", true);
        jQuery("#btnmandatory4").prop("disabled", true);
        jQuery("#btnmandatory5").prop("disabled", true);
        jQuery("#btnmandatory6").prop("disabled", true);
        jQuery("#btnmandatory7").prop("disabled", true);
        jQuery("#btnmandatory8").prop("disabled", true);
        jQuery("#btnmandatory9").prop("disabled", true);
        jQuery("#btnmandatory10").prop("disabled", true);
        jQuery("#btnmandatory11").prop("disabled", true);
        jQuery("#btnmandatory12").prop("disabled", true);
        jQuery("#btnmandatory12").prop("disabled", true);
        jQuery("#btnmandatory14").prop("disabled", true);
        jQuery("#btnmandatory15").prop("disabled", true);
        jQuery("#btnmandatory16").prop("disabled", true);
        jQuery("#btnmandatory17").prop("disabled", true);
        jQuery("#btnmandatory18").prop("disabled", true);
        jQuery("#btnmandatory19").prop("disabled", true);
        jQuery("#btnmandatory20").prop("disabled", true);
        jQuery("#btnmandatory21").prop("disabled", true);
        jQuery("#btnmandatory22").prop("disabled", true);
        jQuery("#btnmandatory23").prop("disabled", true);
        jQuery("#btnmandatory24").prop("disabled", true);
        jQuery("#btnmandatory25").prop("disabled", true);
        jQuery("#btnmandatory26").prop("disabled", true);
        jQuery("#btnmandatory27").prop("disabled", true);
        jQuery("#btnmandatory28").prop("disabled", true);
        jQuery("#btnmandatory29").prop("disabled", true);
        jQuery("#btnmandatory30").prop("disabled", true);
        // jQuery("#btnotcmandatory1").prop("disabled", true);
        // jQuery("#btnotcmandatory2").prop("disabled", true);
        // jQuery("#btnotcmandatory3").prop("disabled", true);
        // jQuery("#btnotcmandatory4").prop("disabled", true);
        // jQuery("#btnotcmandatory5").prop("disabled", true);
        // jQuery("#btnotcmandatory6").prop("disabled", true);
    };
    FrmPaymentTransactionConfigComponent.prototype.goCopy = function () {
        this._list.merchantID = "";
        this._list.name = "";
        this._paymentConfigobj.processCde = "";
    };
    FrmPaymentTransactionConfigComponent.prototype.Validation = function () {
        if (this._list.merchantID == "") {
            this._classname = "alert alert-danger";
            this._timer = 2000;
            this.alertMessage("Please Choose Merchant ID !", this._classname, this._timer);
        }
        else if (this._list.merchantID != "") {
            this._flag = "true";
        }
        if (this._flag == "true") {
            this.goSave();
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.changeFieldCode = function (options, indexvalue) {
        var indexresult = indexvalue.srno - 1;
        var value = options[options.selectedIndex].value;
        this._paymentConfigobj.data[indexresult].dataType = value;
        // console.log("lov code" + JSON.stringify(this._paymentConfigobj.data[indexresult].lovData[0]));
        // this._paymentConfigobj.data[indexresult].lovCode = this._paymentConfigobj.data[indexresult].lovData[0].lovCde;
        if (value == "text") {
            this._paymentConfigobj.data[indexresult].lovRef = "";
        }
        if (value == "date") {
            this._paymentConfigobj.data[indexresult].lovRef = "";
        }
        if (value == "lov") {
            this._paymentConfigobj.data[indexresult].lovRef = "ref032";
            this._paymentConfigobj.data[indexresult].readonly = "false";
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.changemainLov = function (options, indexvalue) {
        var indexresult = indexvalue.srno - 1;
        var value = options[options.selectedIndex].value;
        this._paymentConfigobj.data[indexresult].lovCode = value;
    };
    FrmPaymentTransactionConfigComponent.prototype.changemaincustomlogic = function (options) {
        var value = options[options.selectedIndex].value;
        this._paymentConfigobj.processCde = value;
    };
    FrmPaymentTransactionConfigComponent.prototype.changemainorderdetails = function (options) {
        var value = options[options.selectedIndex].value;
        this._paymentConfigobj.detailOrder = value;
    };
    FrmPaymentTransactionConfigComponent.prototype.callFieldCode1 = function (options, indexvalue) {
        if (options == "text") {
            this._paymentConfigobj.data[indexvalue].lovRef = "";
        }
        else if (options == "date") {
            this._paymentConfigobj.data[indexvalue].lovRef = "";
        }
        else if (options == "lov") {
            this._paymentConfigobj.data[indexvalue].lovRef = "ref032";
            this._read = false;
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.getAllProcessingCode = function () {
        var _this = this;
        try {
            this.http.doGet(this.ics._apiurl + 'service001/getAllProcessingCode').subscribe(function (data) {
                if (data != null && data != undefined) {
                    _this.ref._lov3.refProcessingCode = data.refProcessingCode;
                }
                else {
                    _this.ref._lov3.refProcessingCode = [{ "value": "", "caption": "Empty" }];
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL");
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.getLoadProcessingCode = function () {
        var _this = this;
        this._mflag = false;
        this.http.doGet(this.ics._apiurl + 'service001/getProcessingCodeLov').subscribe(function (data) {
            if (data != null) {
                if (data.ref028 != null) {
                    if (!(data.ref028 instanceof Array)) {
                        var m = [];
                        m[0] = data.ref028;
                        _this.ref._lov3.ref028 = m;
                    }
                    else {
                        _this.ref._lov3.ref028 = data.ref028;
                    }
                }
            }
            _this._mflag = true;
        });
    };
    FrmPaymentTransactionConfigComponent.prototype.getLoadLOV = function () {
        var _this = this;
        this._mflag = false;
        this.http.doGet(this.ics._apiurl + 'service001/getLoadLOV').subscribe(function (data) {
            if (data != null) {
                if (data.ref032 != null) {
                    if (!(data.ref032 instanceof Array)) {
                        var m = [];
                        m[0] = data.ref032;
                        _this.ref._lov3.ref032 = m;
                    }
                    else {
                        _this.ref._lov3.ref032 = data.ref032;
                    }
                }
            }
            _this._mflag = true;
        });
    };
    FrmPaymentTransactionConfigComponent.prototype.lookup = function () {
        this._lovsetupobj = { "sysKey": 0, "lovNo": "TBA", "createdDate": "", "modiDate": "", "userID": "User1", "recStatus": "0", "lovDesc2": "", "lovType": "singleselect", "data": [{ "srno": "1", "lovCde": "", "lovDesc1": "" }], "messagecode": "", "messagedesc": "", "sessionID": "" };
        jQuery("#lookupLOV").modal();
    };
    FrmPaymentTransactionConfigComponent.prototype.lookuppreview = function () {
        this._lovsetupobj = { "sysKey": 0, "lovNo": "TBA", "createdDate": "", "modiDate": "", "userID": "User1", "recStatus": "0", "lovDesc2": "", "lovType": "singleselect", "data": [{ "srno": "1", "lovCde": "", "lovDesc1": "" }], "messagecode": "", "messagedesc": "", "sessionID": "" };
        for (var i = 0; i < this._paymentConfigobj.data.length; i++) {
            if (this._paymentConfigobj.data[i].isCheckPayment == true && this._paymentConfigobj.data[i].isCheckPortal == true) {
                this._paymentConfigobj.data[i].fieldShow = 3;
            }
            if (this._paymentConfigobj.data[i].isCheckPayment == true && this._paymentConfigobj.data[i].isCheckPortal == false) {
                this._paymentConfigobj.data[i].fieldShow = 1;
            }
            if (this._paymentConfigobj.data[i].isCheckPortal == true && this._paymentConfigobj.data[i].isCheckPayment == false) {
                this._paymentConfigobj.data[i].fieldShow = 2;
            }
            if (this._paymentConfigobj.data[i].isCheckPayment == false && this._paymentConfigobj.data[i].isCheckPortal == false) {
                this._paymentConfigobj.data[i].fieldShow = 0;
            }
            if (this._paymentConfigobj.data[i].dataType == 'lov' && (this._paymentConfigobj.data[i].fieldShow == 1 ||
                this._paymentConfigobj.data[i].fieldShow == 2 || this._paymentConfigobj.data[i].fieldShow == 3)) {
                this.getLovData(i);
            }
            if (this._paymentConfigobj.data[i].dataType == 'date' && (this._paymentConfigobj.data[i].fieldShow == 1 ||
                this._paymentConfigobj.data[i].fieldShow == 2 || this._paymentConfigobj.data[i].fieldShow == 3)) {
                this._paymentConfigobj.data[i].fieldValue = this._util.getTodayDate();
            }
        }
        // jQuery("#lookuppreview").modal();   //    
        jQuery("#lookuppreview").modal();
    };
    FrmPaymentTransactionConfigComponent.prototype.goAddLovsetup = function () {
        if (this._lovsetupobj.data[0].srno == "") {
            this._lovsetupobj.data[0].srno = "1";
        }
        var maxsrno = this._lovsetupobj.data.length;
        maxsrno = maxsrno + 1;
        this._lovsetupobj.data.push({ srno: (maxsrno).toString(), lovCde: "", lovDesc1: "" });
    };
    FrmPaymentTransactionConfigComponent.prototype.goRemoveLov = function (p) {
        var index = this._lovsetupobj.data.indexOf(p);
        var length = this._lovsetupobj.data.length;
        if (length < 2) {
            this._lovsetupobj.data[0] = { srno: ("").toString(), lovCde: "", lovDesc1: "" };
        }
        else {
            this._lovsetupobj.data.splice(index, 1);
        }
        for (var i = 0; i < length; i++) {
            var maxsrno = i;
            maxsrno = maxsrno + 1;
            this._lovsetupobj.data[i].srno = (maxsrno).toString();
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.goSaveLov = function () {
        var _this = this;
        this._mflag = false;
        var url = this.ics._apiurl + 'service001/savelovSetup';
        var json = this._lovsetupobj;
        this.http.doPost(url, json).subscribe(function (data) {
            if (data != null) {
                _this.popupMessage("Saved Successfully.");
                _this._lovsetupobj.lovNo = data.lovNo;
                _this._lovsetupobj.sysKey = data.sysKey;
                _this.getLoadLOV();
            }
            _this._mflag = true;
        }, function (error) {
            if (error._body.type == "error") {
                alert("Connection Timed Out.");
            }
        }, function () { });
    };
    FrmPaymentTransactionConfigComponent.prototype.goNewLov = function () {
        this._lovsetupobj = { "sysKey": 0, "lovNo": "TBA", "createdDate": "", "modiDate": "", "userID": "User1", "recStatus": "0", "lovDesc2": "", "lovType": "singleselect", "data": [{ "srno": "1", "lovCde": "", "lovDesc1": "" }], "messagecode": "", "messagedesc": "", "sessionID": "" };
    };
    FrmPaymentTransactionConfigComponent.prototype.footergoNewLov = function () {
        this._lovsetupobj = { "sysKey": 0, "lovNo": "TBA", "createdDate": "", "modiDate": "", "userID": "User1", "recStatus": "0", "lovDesc2": "", "lovType": "singleselect", "data": [{ "srno": "1", "lovCde": "", "lovDesc1": "" }], "messagecode": "", "messagedesc": "", "sessionID": "" };
    };
    FrmPaymentTransactionConfigComponent.prototype.goEdit = function (indexvalue) {
        var _this = this;
        this._mflag = false;
        var indexresult = indexvalue.srno - 1;
        if (this._paymentConfigobj.data[indexresult].lovCode != "" && this._paymentConfigobj.data[indexresult].lovCode != null) {
            var url = this.ics._apiurl + 'service001/getLOVbyCode';
            this._lovsetupobj.lovNo = this._paymentConfigobj.data[indexresult].lovCode;
            this._lovsetupobj.sessionID = this.ics._profile.sessionID;
            var json = this._lovsetupobj;
            this.http.doPost(url, json).subscribe(function (res) {
                if (res.messagecode == '0016') {
                    _this.sessionAlertMsg = res.messagedesc;
                    _this.showMessage();
                }
                else {
                    if (res != null) {
                        _this._lovsetupobj = res;
                        if (!(res.data instanceof Array)) {
                            var m = [];
                            m[0] = res.data;
                            _this._lovsetupobj.data = m;
                        }
                        _this.goEnable();
                    }
                }
                _this._mflag = true;
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
            jQuery("#lookupLOV").modal();
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.getLovData = function (indexvalue) {
        var _this = this;
        this._mflag = false;
        var url = this.ics._apiurl + 'service001/getLOVbyCode';
        this._lovsetupobj.lovNo = this._paymentConfigobj.data[indexvalue].lovCode;
        this._lovsetupobj.sessionID = this.ics._profile.sessionID;
        var json = this._lovsetupobj;
        this.http.doPost(url, json).subscribe(function (res) {
            if (res.messagecode == '0016') {
                _this.sessionAlertMsg = res.messagedesc;
                _this.showMessage();
            }
            else {
                if (res != null) {
                    if (res.data != null) {
                        if (!(res.data instanceof Array)) {
                            var m = [];
                            m[0] = res.data;
                            _this._paymentConfigobj.data[indexvalue].lovData = m;
                        }
                        _this._paymentConfigobj.data[indexvalue].lovData = res.data;
                    }
                }
            }
            _this._mflag = true;
        }, function (error) {
            if (error._body.type == "error") {
                alert("Connection Timed Out.");
            }
        }, function () { });
    };
    FrmPaymentTransactionConfigComponent.prototype.getPaymentTransConfigBySysKey = function (sysKey, merchantID) {
        var _this = this;
        this._mflag = false;
        this._read = false;
        //this.goEnable();
        this.http.doGet(this.ics._apiurl + 'service001/getPaymentConfigbySysKey?sysKey=' + sysKey + '&merchantid=' + merchantID + '&sessionID=' + this.ics._profile.sessionID + '&userID=' + this.ics._profile.userID).subscribe(function (data) {
            if (data.messageCode == '0016') {
                _this.sessionAlertMsg = data.messsageDesc;
                _this.showMessage();
            }
            else {
                if (data != null) {
                    _this._paymentConfigobj = data;
                    _this._list.merchantID = _this._paymentConfigobj.merchantID;
                    _this._list.name = _this._paymentConfigobj.name;
                    jQuery("#btncopy").prop("disabled", false);
                    // this.goEnable();
                    if (!(data.data instanceof Array)) {
                        var m = [];
                        m[0] = data.data;
                        _this._paymentConfigobj.data = m;
                    }
                    _this._paymentConfigobj = data;
                    for (var j = 0; j < _this._paymentConfigobj.data.length; j++) {
                        _this._paymentConfigobj.data[j].srno = j + 1;
                        if (_this._paymentConfigobj.data[j].dataType == "number") {
                            _this._paymentConfigobj.data[j].fieldValue = "Number";
                        }
                        if (_this._paymentConfigobj.data[0].dataType == "date") {
                            _this._paymentConfigobj.data[0].fieldValue = "Date";
                        }
                        if (_this._paymentConfigobj.data[2].dataType == "text") {
                            _this._paymentConfigobj.data[2].fieldValue = "Text";
                        }
                        if (_this._paymentConfigobj.data[3].dataType == "text") {
                            _this._paymentConfigobj.data[3].fieldValue = "Text";
                        }
                        if (_this._paymentConfigobj.data[4].dataType == "text") {
                            _this._paymentConfigobj.data[4].fieldValue = "Text";
                        }
                        if (_this._paymentConfigobj.data[5].dataType == "text") {
                            _this._paymentConfigobj.data[5].fieldValue = "Text";
                        }
                    }
                    for (var i = 0; i < _this._paymentConfigobj.data.length; i++) {
                        if (_this._paymentConfigobj.data[i].dataType == "lov") {
                            _this.callFieldCode1(_this._paymentConfigobj.data[i].dataType, i);
                        }
                        if (_this._paymentConfigobj.data[i].fieldShow == 0) {
                            _this._paymentConfigobj.data[i].isCheckCounter = false;
                            _this._paymentConfigobj.data[i].isCheckPayment = false;
                            _this._paymentConfigobj.data[i].isCheckPortal = false;
                        }
                        if (_this._paymentConfigobj.data[i].fieldShow == 1) {
                            _this._paymentConfigobj.data[i].isCheckCounter = true;
                            _this._paymentConfigobj.data[i].isCheckPayment = false;
                            _this._paymentConfigobj.data[i].isCheckPortal = false;
                        }
                        if (_this._paymentConfigobj.data[i].fieldShow == 2) {
                            _this._paymentConfigobj.data[i].isCheckCounter = false;
                            _this._paymentConfigobj.data[i].isCheckPayment = true;
                            _this._paymentConfigobj.data[i].isCheckPortal = false;
                        }
                        if (_this._paymentConfigobj.data[i].fieldShow == 3) {
                            _this._paymentConfigobj.data[i].isCheckCounter = true;
                            _this._paymentConfigobj.data[i].isCheckPayment = true;
                            _this._paymentConfigobj.data[i].isCheckPortal = false;
                        }
                        if (_this._paymentConfigobj.data[i].fieldShow == 4) {
                            _this._paymentConfigobj.data[i].isCheckCounter = true;
                            _this._paymentConfigobj.data[i].isCheckPayment = false;
                            _this._paymentConfigobj.data[i].isCheckPortal = true;
                        }
                        if (_this._paymentConfigobj.data[i].fieldShow == 5) {
                            _this._paymentConfigobj.data[i].isCheckCounter = false;
                            _this._paymentConfigobj.data[i].isCheckPayment = true;
                            _this._paymentConfigobj.data[i].isCheckPortal = true;
                        }
                        if (_this._paymentConfigobj.data[i].fieldShow == 6) {
                            _this._paymentConfigobj.data[i].isCheckCounter = true;
                            _this._paymentConfigobj.data[i].isCheckPayment = true;
                            _this._paymentConfigobj.data[i].isCheckPortal = true;
                        }
                        if (_this._paymentConfigobj.data[i].isMandatory == 0) {
                            _this._paymentConfigobj.data[i].isCheckMandatory = false;
                        }
                        if (_this._paymentConfigobj.data[i].isMandatory == 1) {
                            _this._paymentConfigobj.data[i].isCheckMandatory = true;
                        }
                        if (_this._paymentConfigobj.data[i].isOTCMandatory == 0) {
                            _this._paymentConfigobj.data[i].isCheckOTCMandatory = false;
                        }
                        if (_this._paymentConfigobj.data[i].isOTCMandatory == 1) {
                            _this._paymentConfigobj.data[i].isCheckOTCMandatory = true;
                        }
                        if (_this._paymentConfigobj.data[i].fieldID == "R2" && _this._paymentConfigobj.data[i].caption != "") {
                            jQuery("#reference2").attr('class', 'collapse in');
                        }
                        if (_this._paymentConfigobj.data[i].fieldID == "R3" && _this._paymentConfigobj.data[i].caption != "") {
                            jQuery("#reference3").attr('class', 'collapse in');
                        }
                        if (_this._paymentConfigobj.data[i].fieldID == "R5" && _this._paymentConfigobj.data[i].caption != "") {
                            jQuery("#subType2").attr('class', 'collapse in');
                        }
                        if (_this._paymentConfigobj.data[i].fieldID == "R6" && _this._paymentConfigobj.data[i].caption != "") {
                            jQuery("#subType3").attr('class', 'collapse in');
                        }
                        if (_this._paymentConfigobj.data[i].fieldID == "OPT2" && _this._paymentConfigobj.data[i].caption != "") {
                            jQuery("#optional2").attr('class', 'collapse in');
                        }
                        if (_this._paymentConfigobj.data[i].fieldID == "OPT3" && _this._paymentConfigobj.data[i].caption != "") {
                            jQuery("#optional3").attr('class', 'collapse in');
                        }
                        if (_this._paymentConfigobj.data[i].fieldID == "OPT4" && _this._paymentConfigobj.data[i].caption != "") {
                            jQuery("#optional4").attr('class', 'collapse in');
                        }
                        if (_this._paymentConfigobj.data[i].fieldID == "OPT5" && _this._paymentConfigobj.data[i].caption != "") {
                            jQuery("#optional5").attr('class', 'collapse in');
                        }
                        if (_this._paymentConfigobj.data[i].fieldID == "OPT6" && _this._paymentConfigobj.data[i].caption != "") {
                            jQuery("#optional6").attr('class', 'collapse in');
                        }
                        if (_this._paymentConfigobj.data[i].fieldID == "OPT7" && _this._paymentConfigobj.data[i].caption != "") {
                            jQuery("#optional7").attr('class', 'collapse in');
                        }
                        if (_this._paymentConfigobj.data[i].fieldID == "OPN2" && _this._paymentConfigobj.data[i].caption != "") {
                            jQuery("#numeric2").attr('class', 'collapse in');
                        }
                        if (_this._paymentConfigobj.data[i].fieldID == "OPN3" && _this._paymentConfigobj.data[i].caption != "") {
                            jQuery("#numeric3").attr('class', 'collapse in');
                        }
                        if (_this._paymentConfigobj.data[i].fieldID == "OPN4" && _this._paymentConfigobj.data[i].caption != "") {
                            jQuery("#numeric4").attr('class', 'collapse in');
                        }
                        if (_this._paymentConfigobj.data[i].fieldID == "OPN5" && _this._paymentConfigobj.data[i].caption != "") {
                            jQuery("#numeric5").attr('class', 'collapse in');
                        }
                        if (_this._paymentConfigobj.data[i].fieldID == "OPN6" && _this._paymentConfigobj.data[i].caption != "") {
                            jQuery("#numeric6").attr('class', 'collapse in');
                        }
                        if (_this._paymentConfigobj.data[i].fieldID == "OPN7" && _this._paymentConfigobj.data[i].caption != "") {
                            jQuery("#numeric7").attr('class', 'collapse in');
                        }
                        if (_this._paymentConfigobj.data[i].fieldID == "OPN8" && _this._paymentConfigobj.data[i].caption != "") {
                            jQuery("#numeric8").attr('class', 'collapse in');
                        }
                    }
                    if (_this._paymentConfigobj.data[2].isMandatory == 0) {
                        _this._paymentConfigobj.data[2].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[2].isCheckPayment == true) {
                            jQuery("#btnmandatory").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[3].isMandatory == 0) {
                        _this._paymentConfigobj.data[3].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[3].isCheckPayment == true) {
                            jQuery("#btnmandatory1").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory1").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[4].isMandatory == 0) {
                        _this._paymentConfigobj.data[4].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[4].isCheckPayment == true) {
                            jQuery("#btnmandatory2").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory2").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[5].isMandatory == 0) {
                        _this._paymentConfigobj.data[5].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[5].isCheckPayment == true) {
                            jQuery("#btnmandatory3").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory3").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[6].isMandatory == 0) {
                        _this._paymentConfigobj.data[6].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[6].isCheckPayment == true) {
                            jQuery("#btnmandatory4").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory4").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[7].isMandatory == 0) {
                        _this._paymentConfigobj.data[7].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[7].isCheckPayment == true) {
                            jQuery("#btnmandatory5").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory5").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[8].isMandatory == 0) {
                        _this._paymentConfigobj.data[8].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[8].isCheckPayment == true) {
                            jQuery("#btnmandatory6").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory6").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[9].isMandatory == 0) {
                        _this._paymentConfigobj.data[9].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[9].isCheckPayment == true) {
                            jQuery("#btnmandatory7").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory7").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[10].isMandatory == 0) {
                        _this._paymentConfigobj.data[10].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[10].isCheckPayment == true) {
                            jQuery("#btnmandatory8").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory8").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[11].isMandatory == 0) {
                        _this._paymentConfigobj.data[11].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[11].isCheckPayment == true) {
                            jQuery("#btnmandatory9").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory9").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[12].isMandatory == 0) {
                        _this._paymentConfigobj.data[12].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[12].isCheckPayment == true) {
                            jQuery("#btnmandatory10").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory10").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[13].isMandatory == 0) {
                        _this._paymentConfigobj.data[13].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[13].isCheckPayment == true) {
                            jQuery("#btnmandatory11").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory11").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[14].isMandatory == 0) {
                        _this._paymentConfigobj.data[14].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[14].isCheckPayment == true) {
                            jQuery("#btnmandatory12").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory12").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[15].isMandatory == 0) {
                        _this._paymentConfigobj.data[15].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[15].isCheckPayment == true) {
                            jQuery("#btnmandatory13").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory13").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[16].isMandatory == 0) {
                        _this._paymentConfigobj.data[16].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[16].isCheckPayment == true) {
                            jQuery("#btnmandatory14").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory14").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[17].isMandatory == 0) {
                        _this._paymentConfigobj.data[17].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[17].isCheckPayment == true) {
                            jQuery("#btnmandatory15").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory15").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[18].isMandatory == 0) {
                        _this._paymentConfigobj.data[18].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[18].isCheckPayment == true) {
                            jQuery("#btnmandatory16").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory16").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[19].isMandatory == 0) {
                        _this._paymentConfigobj.data[19].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[19].isCheckPayment == true) {
                            jQuery("#btnmandatory17").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory17").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[20].isMandatory == 0) {
                        _this._paymentConfigobj.data[20].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[20].isCheckPayment == true) {
                            jQuery("#btnmandatory18").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory18").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[21].isMandatory == 0) {
                        _this._paymentConfigobj.data[21].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[21].isCheckPayment == true) {
                            jQuery("#btnmandatory19").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory19").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[22].isMandatory == 0) {
                        _this._paymentConfigobj.data[22].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[22].isCheckPayment == true) {
                            jQuery("#btnmandatory20").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory20").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[23].isMandatory == 0) {
                        _this._paymentConfigobj.data[23].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[23].isCheckPayment == true) {
                            jQuery("#btnmandatory21").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory21").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[24].isMandatory == 0) {
                        _this._paymentConfigobj.data[24].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[24].isCheckPayment == true) {
                            jQuery("#btnmandatory22").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory22").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[25].isMandatory == 0) {
                        _this._paymentConfigobj.data[25].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[25].isCheckPayment == true) {
                            jQuery("#btnmandatory23").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory23").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[26].isMandatory == 0) {
                        _this._paymentConfigobj.data[26].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[26].isCheckPayment == true) {
                            jQuery("#btnmandatory24").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory24").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[27].isMandatory == 0) {
                        _this._paymentConfigobj.data[27].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[27].isCheckPayment == true) {
                            jQuery("#btnmandatory25").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory25").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[28].isMandatory == 0) {
                        _this._paymentConfigobj.data[28].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[28].isCheckPayment == true) {
                            jQuery("#btnmandatory26").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory26").prop("disabled", true);
                        }
                        ;
                    }
                    if (_this._paymentConfigobj.data[29].isMandatory == 0) {
                        _this._paymentConfigobj.data[29].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[29].isCheckPayment == true) {
                            jQuery("#btnmandatory27").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory27").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[30].isMandatory == 0) {
                        _this._paymentConfigobj.data[30].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[30].isCheckPayment == true) {
                            jQuery("#btnmandatory28").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory28").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[31].isMandatory == 0) {
                        _this._paymentConfigobj.data[31].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[31].isCheckPayment == true) {
                            jQuery("#btnmandatory29").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory29").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[32].isMandatory == 0) {
                        _this._paymentConfigobj.data[32].isCheckMandatory = false;
                        if (_this._paymentConfigobj.data[32].isCheckPayment == true) {
                            jQuery("#btnmandatory30").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnmandatory30").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[2].isMandatory == 1) {
                        _this._paymentConfigobj.data[2].isCheckMandatory = true;
                        jQuery("#btnmandatory").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[3].isMandatory == 1) {
                        _this._paymentConfigobj.data[3].isCheckMandatory = true;
                        jQuery("#btnmandatory1").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[4].isMandatory == 1) {
                        _this._paymentConfigobj.data[4].isCheckMandatory = true;
                        jQuery("#btnmandatory2").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[5].isMandatory == 1) {
                        _this._paymentConfigobj.data[5].isCheckMandatory = true;
                        jQuery("#btnmandatory3").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[6].isMandatory == 1) {
                        _this._paymentConfigobj.data[6].isCheckMandatory = true;
                        jQuery("#btnmandatory4").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[7].isMandatory == 1) {
                        _this._paymentConfigobj.data[7].isCheckMandatory = true;
                        jQuery("#btnmandatory5").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[8].isMandatory == 1) {
                        _this._paymentConfigobj.data[8].isCheckMandatory = true;
                        jQuery("#btnmandatory6").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[9].isMandatory == 1) {
                        _this._paymentConfigobj.data[9].isCheckMandatory = true;
                        jQuery("#btnmandatory7").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[10].isMandatory == 1) {
                        _this._paymentConfigobj.data[10].isCheckMandatory = true;
                        jQuery("#btnmandatory8").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[11].isMandatory == 1) {
                        _this._paymentConfigobj.data[11].isCheckMandatory = true;
                        jQuery("#btnmandatory9").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[12].isMandatory == 1) {
                        _this._paymentConfigobj.data[12].isCheckMandatory = true;
                        jQuery("#btnmandatory10").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[13].isMandatory == 1) {
                        _this._paymentConfigobj.data[13].isCheckMandatory = true;
                        jQuery("#btnmandatory11").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[14].isMandatory == 1) {
                        _this._paymentConfigobj.data[14].isCheckMandatory = true;
                        jQuery("#btnmandatory12").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[15].isMandatory == 1) {
                        _this._paymentConfigobj.data[15].isCheckMandatory = true;
                        jQuery("#btnmandatory13").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[16].isMandatory == 1) {
                        _this._paymentConfigobj.data[16].isCheckMandatory = true;
                        jQuery("#btnmandatory14").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[17].isMandatory == 1) {
                        _this._paymentConfigobj.data[17].isCheckMandatory = true;
                        jQuery("#btnmandatory15").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[18].isMandatory == 1) {
                        _this._paymentConfigobj.data[18].isCheckMandatory = true;
                        jQuery("#btnmandatory16").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[19].isMandatory == 1) {
                        _this._paymentConfigobj.data[19].isCheckMandatory = true;
                        jQuery("#btnmandatory17").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[20].isMandatory == 1) {
                        _this._paymentConfigobj.data[20].isCheckMandatory = true;
                        jQuery("#btnmandatory18").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[21].isMandatory == 1) {
                        _this._paymentConfigobj.data[21].isCheckMandatory = true;
                        jQuery("#btnmandatory19").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[22].isMandatory == 1) {
                        _this._paymentConfigobj.data[22].isCheckMandatory = true;
                        jQuery("#btnmandatory20").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[23].isMandatory == 1) {
                        _this._paymentConfigobj.data[23].isCheckMandatory = true;
                        jQuery("#btnmandatory21").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[24].isMandatory == 1) {
                        _this._paymentConfigobj.data[24].isCheckMandatory = true;
                        jQuery("#btnmandatory22").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[25].isMandatory == 1) {
                        _this._paymentConfigobj.data[25].isCheckMandatory = true;
                        jQuery("#btnmandatory23").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[26].isMandatory == 1) {
                        _this._paymentConfigobj.data[26].isCheckMandatory = true;
                        jQuery("#btnmandatory24").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[27].isMandatory == 1) {
                        _this._paymentConfigobj.data[27].isCheckMandatory = true;
                        jQuery("#btnmandatory25").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[28].isMandatory == 1) {
                        _this._paymentConfigobj.data[28].isCheckMandatory = true;
                        jQuery("#btnmandatory26").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[29].isMandatory == 1) {
                        _this._paymentConfigobj.data[29].isCheckMandatory = true;
                        jQuery("#btnmandatory27").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[30].isMandatory == 1) {
                        _this._paymentConfigobj.data[30].isCheckMandatory = true;
                        jQuery("#btnmandatory28").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[31].isMandatory == 1) {
                        _this._paymentConfigobj.data[31].isCheckMandatory = true;
                        jQuery("#btnmandatory29").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[32].isMandatory == 1) {
                        _this._paymentConfigobj.data[32].isCheckMandatory = true;
                        jQuery("#btnmandatory30").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[2].isOTCMandatory == 0) {
                        _this._paymentConfigobj.data[2].isCheckOTCMandatory = false;
                        if (_this._paymentConfigobj.data[2].isCheckCounter == true) {
                            jQuery("#btnotcmandatory1").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnotcmandatory1").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[3].isOTCMandatory == 0) {
                        _this._paymentConfigobj.data[3].isCheckOTCMandatory = false;
                        if (_this._paymentConfigobj.data[3].isCheckCounter == true) {
                            jQuery("#btnotcmandatory2").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnotcmandatory2").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[4].isOTCMandatory == 0) {
                        _this._paymentConfigobj.data[4].isCheckOTCMandatory = false;
                        if (_this._paymentConfigobj.data[4].isCheckCounter == true) {
                            jQuery("#btnotcmandatory3").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnotcmandatory3").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[5].isOTCMandatory == 0) {
                        _this._paymentConfigobj.data[5].isCheckOTCMandatory = false;
                        if (_this._paymentConfigobj.data[5].isCheckCounter == true) {
                            jQuery("#btnotcmandatory4").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnotcmandatory4").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[6].isOTCMandatory == 0) {
                        _this._paymentConfigobj.data[6].isCheckOTCMandatory = false;
                        if (_this._paymentConfigobj.data[6].isCheckCounter == true) {
                            jQuery("#btnotcmandatory5").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnotcmandatory5").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[7].isOTCMandatory == 0) {
                        _this._paymentConfigobj.data[7].isCheckOTCMandatory = false;
                        if (_this._paymentConfigobj.data[7].isCheckCounter == true) {
                            jQuery("#btnotcmandatory6").prop("disabled", false);
                        }
                        else {
                            jQuery("#btnotcmandatory6").prop("disabled", true);
                        }
                    }
                    if (_this._paymentConfigobj.data[2].isOTCMandatory == 1) {
                        _this._paymentConfigobj.data[2].isCheckOTCMandatory = true;
                        jQuery("#btnotcmandatory1").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[3].isOTCMandatory == 1) {
                        _this._paymentConfigobj.data[3].isCheckOTCMandatory = true;
                        jQuery("#btnotcmandatory2").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[4].isOTCMandatory == 1) {
                        _this._paymentConfigobj.data[4].isCheckOTCMandatory = true;
                        jQuery("#btnotcmandatory3").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[5].isOTCMandatory == 1) {
                        _this._paymentConfigobj.data[5].isCheckOTCMandatory = true;
                        jQuery("#btnotcmandatory4").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[6].isOTCMandatory == 1) {
                        _this._paymentConfigobj.data[6].isCheckOTCMandatory = true;
                        jQuery("#btnotcmandatory5").prop("disabled", false);
                    }
                    if (_this._paymentConfigobj.data[7].isOTCMandatory == 1) {
                        _this._paymentConfigobj.data[7].isCheckOTCMandatory = true;
                        jQuery("#btnotcmandatory6").prop("disabled", false);
                    }
                    jQuery("#mydelete").prop("disabled", false);
                    jQuery("#mydelete2").prop("disabled", false);
                }
                jQuery("#mydelete").prop("disabled", false);
                jQuery("#mydelete2").prop("disabled", false);
                _this.getLoadLOV();
            }
            _this._mflag = true;
        });
    };
    /* checkSession() {
      try {
        let url: string = this.ics._apiurl + 'service001/checkSession?sessionID=' + this.ics._profile.sessionID;
        this.http.doGet(url).subscribe(
          data => {
            if (data.msgCode == '0016') {
              this.sessionAlertMsg = data.msgDesc;
              this.showMessage();
            }
            else {
              this.getLoadLOV();
              this.getLoadProcessingCode();
              // this.getAllProcessingCode();
            }
          },
          error => {
            if (error._body.type == 'error') {
              alert("Connection Timed Out!");
            }
          }, () => { });
      } catch (e) {
        alert("Invalid URL");
      }
  
    } */
    FrmPaymentTransactionConfigComponent.prototype.showMessage = function () {
        var _this = this;
        jQuery("#sessionalert").modal();
        Rx_1.Observable.timer(3000).subscribe(function (x) {
            _this.goLogOut();
        });
    };
    FrmPaymentTransactionConfigComponent.prototype.goLogOut = function () {
        jQuery("#sessionalert").modal('hide');
        this._router.navigate(['/login']);
    };
    FrmPaymentTransactionConfigComponent.prototype.getSessionObj = function () {
        return { "sessionID": "", "userID": "" };
    };
    FrmPaymentTransactionConfigComponent.prototype.checkSession = function () {
        var _this = this;
        try {
            var url = this.ics._apiurl + 'service001/checkSessionTime';
            this._sessionObj.sessionID = this.ics._profile.sessionID;
            this._sessionObj.userID = this.ics._profile.userID;
            var json = this._sessionObj;
            this.http.doPost(url, json).subscribe(function (data) {
                if (data != null) {
                    if (data.code == "0016") {
                        _this.showMsgAlert(data.desc);
                        _this.logout();
                    }
                    if (data.code == "0014") {
                        _this.showMsgAlert(data.desc);
                    }
                }
            }, function (error) {
                if (error._body.type == "error") {
                    alert("Connection Timed Out.");
                }
            }, function () { });
        }
        catch (e) {
            alert("Invalid URL.");
        }
    };
    FrmPaymentTransactionConfigComponent.prototype.showMsgAlert = function (msg) {
        this.ics.sendBean({ "t1": "rp-msg", "t2": "Information", "t3": msg });
    };
    FrmPaymentTransactionConfigComponent.prototype.logout = function () {
        this._router.navigate(['/login']);
    };
    FrmPaymentTransactionConfigComponent = __decorate([
        core_1.Component({
            selector: 'app-paymenttransationsetup',
            template: "\n  <div class=\"container\">\n    <div class=\"row clearfix\">\n      <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12  column col-sm-offset-0 col-md-offset-0 col-lg-offset-0\"> \n        <form class=\"form-horizontal\" (ngSubmit)=\"Validation();\">\n          <legend>Payment Template</legend>       \n          <div class=\"row  col-md-12\">\n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"goList();\" >List</button> \n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"goNew();\" >New</button>\n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"Validation();\">Save</button> \n            <button class=\"btn btn-primary\" id=\"mydelete\" type=\"button\" (click)=\"goDelete();\" disabled>Delete</button>\n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"lookuppreview();\">Preview</button>\n          </div>\n          <div  class=\"row col-md-12\">&nbsp;</div>\n          <div class=\"col-md-12\" id=\"custom-form-alignment-margin\">\n            <div  class=\"col-md-6\">   \n              <div class=\"form-group\"> \n                <label class=\"col-md-4\"> Merchant ID </label>\n                <div class=\"col-md-8\" >   \n                  <div class=\"input-group\">\n                    <input  class=\"form-control\" required=\"true\" type = \"text\" [(ngModel)]=\"_list.merchantID\" [ngModelOptions]=\"{standalone: true}\" readonly> \n                    <span class=\"input-group-btn\">\n                      <button class=\"btn btn-primary\" type=\"button\" (click)=\"lookuppopup();\" >&equiv;</button>\n                    </span>\n                  </div>\n                </div>\n              </div> \n\n              <div class=\"form-group\"> \n                <rp-input   rpClass = \"col-md-8\"  rpLabelClass = \"col-md-4 control-label\" rpType=\"text\" rpLabel=\"Name\" [(rpModel)]=\"_list.name\" rpRequired=\"true\"></rp-input> \n              </div>      \n              <div class=\"form-group\" >\n                <rp-input   rpClass = \"col-md-8\"  rpLabelClass = \"col-md-4 control-label\" rpType=\"ref028\" rpLabel=\"Custom Logic\" (change)= \"changemaincustomlogic($event.target.options,_paymentConfigobj.processCde);\" [(rpModel)]=\"_paymentConfigobj.processCde\" rpRequired=\"true\" ></rp-input>   \n              </div> \n\n              <div class=\"form-group\" >\n                <rp-input   rpClass = \"col-md-8\"  rpLabelClass = \"col-md-4 control-label\" rpType=\"ref029\" rpLabel=\"Order Details\" (change)= \"changemainorderdetails($event.target.options,_paymentConfigobj.detailOrder);\" [(rpModel)]=\"_paymentConfigobj.detailOrder\"></rp-input>   \n              </div>\n\n              <div class=\"form-group\" > \n                <div class=\"col-md-12\">\n                  <label class=\"col-md-4\" style=\"padding-left:0px\">Merchant Logo</label>\n                  <div class=\"col-md-8\" style = \"margin: 0px 0px 0px -20px;\" >    \n                    <img class=\"col-md-3 col-sm-3 img-responsive\" [src]=\"_DomSanitizer.bypassSecurityTrustUrl(_paymentConfigobj.imageValue)\" style=\"width:230px;height:150px\">\n                    <div class=\"col-md-5\">\n                      <label class=\"btn btn-primary btn-file\" style=\"margin: 10px 0px 0px 0px;\">\n                        <i class=\"glyphicon glyphicon-folder-open\"></i>\n                        <span class=\"hidden-xs\">Browse \u2026</span>\n                        <input multiple=\"\" type=\"file\" accept=\"image/*\" style=\"display: none;\" (change)=\"loadImg($event,i)\" name=\"_paymentConfigobj.imagename\">\n                      </label>\n                    </div>\n                  </div>\n                </div>                                \n              </div>\n            </div>\n          </div>\n\n          <div id=\"lu001popup\" class=\"modal fade\" role=\"dialog\">\n            <div id=\"lu001popupsize\" class=\"modal-dialog modal-lg\">  \n              <div class=\"modal-content\">\n                <div class=\"modal-header\">\n                  <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n                  <h4 id=\"lu001popuptitle\" class=\"modal-title\">Payment Transaction Merchant Setup</h4>\n                </div> \n                <div id=\"lu001popupbody\" class=\"modal-body\"> \n                  <table class=\"table table-striped table-condensed table-hover tblborder\" style=\"font-size:14px;\">\n                    <thead>\n                      <tr>\n                        <th>Merchant ID</th>\n                        <th>Name</th>                \n                      </tr>\n                    </thead>\n                    <tbody>\n                      <tr *ngFor=\"let obj of _listpopup.arr \">\n                        <td class=\"col-md-3\"> <a (click)=\"gotomerchantid(obj.merchantID)\"> {{obj.merchantID}}</a></td>\n                        <td>{{obj.name}}</td>               \n                      </tr> \n                    </tbody>\n                  </table>  \n                </div>\n                <div class=\"modal-footer\">\n                  <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\">Close</button>\n                </div>\n              </div>\n            </div>\n          </div> \n\n          <div id=\"lookuppreview\" class=\"modal fade\" role=\"dialog\">\n            <div id=\"lookuppreviewSize\" class=\"modal-dialog modal-lg\">  \n              <div class=\"modal-content\" style=\"\">\n                <div class=\"modal-header\">\n                  <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n                </div>\n                <div class=\"modal-body\"   style=\"  margin-left: 70px;\">             \n                  <form class=\"form-horizontal\" (ngSubmit)=\"goPreview()\" >\n                    <fieldset>  \n                      <legend>Payment Transaction Preview</legend>            \n                      <div class=\"form-group\" >\n                        <rp-input    rpClass = \"col-md-4\" rpLabelClass = \"col-md-3 control-label\" rpType=\"text\" rpLabel=\"Merchant Name\" [(rpModel)]=\"_list.name\" rpReadonly = \"true\" ></rp-input> \n                      </div> \n\n                      <div class=\"form-group\" > \n                        <div class=\"col-md-11\">\n                          <label class=\"control-label col-md-3\" style=\"padding-left:0px;text-align:left;\">Merchant Logo</label>\n                          <div class=\"col-md-9\" >    \n                            <img class=\"col-md-3 col-sm-3 img-responsive\" [src]=\"_DomSanitizer.bypassSecurityTrustUrl(_paymentConfigobj.imageValue)\" style=\"width:230px;height:150px\">\n                            <label class=\"btn btn-primary btn-file\">                          \n                              Choose Logo<input type=\"file\" accept=\"image/*\" style=\"display: none;\" (change)=\"loadImg($event,i)\" name=\"_paymentConfigobj.imagename\">\n                            </label>\n                          </div>\n                        </div>                                \n                      </div> \n\n                      <span *ngFor=\"let item of _paymentConfigobj.data;let i=index;\" > \n\n                        <div *ngIf=\"_paymentConfigobj.data[i].dataType !='lov' && (_paymentConfigobj.data[i].fieldShow==1 || _paymentConfigobj.data[i].fieldShow==2 ||_paymentConfigobj.data[i].fieldShow==3) \" class=\"form-group\">    \n                          <div *ngIf=\"_paymentConfigobj.data[i].dataType =='text' && (_paymentConfigobj.data[i].fieldShow==1 || _paymentConfigobj.data[i].fieldShow==2 ||_paymentConfigobj.data[i].fieldShow==3) \" > \n                            <div *ngIf=\"_paymentConfigobj.data[i].regularExLovCode !=''\" > \n                              <label class=\"col-md-3 control-label\" >{{_paymentConfigobj.data[i].caption}}</label>\n                              <div class=\"col-md-4\">\n                                <input  class=\"form-control\" type = \"{{_paymentConfigobj.data[i].dataType}}\" pattern=\"{{_paymentConfigobj.data[i].regularExLovCode}}\" >\n                              </div>\n                            </div>\n\n                            <div *ngIf=\"_paymentConfigobj.data[i].regularExLovCode ==''\" >   \n                              <rp-input  rpClass = \"col-md-4\" rpLabelClass = \"col-md-3 control-label\" rpLabel={{_paymentConfigobj.data[i].caption}} rpType=\"{{_paymentConfigobj.data[i].dataType}}\" \t\trpLabel=\" \" ></rp-input> \n                            </div>\n\n                          </div>\n\n                          <div *ngIf=\"_paymentConfigobj.data[i].dataType =='number' && (_paymentConfigobj.data[i].fieldShow==1 || _paymentConfigobj.data[i].fieldShow==2 ||_paymentConfigobj.data[i].fieldShow==3)\">   \n                            <rp-input  rpClass = \"col-md-4\" rpLabelClass = \"col-md-3 control-label\" rpLabel={{_paymentConfigobj.data[i].caption}} rpType=\"{{_paymentConfigobj.data[i].dataType}}\" \t\trpLabel=\" \" ></rp-input> \n                          </div>\n\n                          <div *ngIf=\"_paymentConfigobj.data[i].dataType =='date' && (_paymentConfigobj.data[i].fieldShow==1 || _paymentConfigobj.data[i].fieldShow==2 ||_paymentConfigobj.data[i].fieldShow==3) \" >       \n                            <div *ngIf=\"_paymentConfigobj.data[i].fieldID !='P1'\">\n                              <rp-input rpClass = \"col-md-4\" rpLabelClass = \"col-md-3 control-label\" rpLabel={{_paymentConfigobj.data[i].caption}} rpType=\"{{_paymentConfigobj.data[i].dataType}}\" [(rpModel)]=\"_paymentConfigobj.data[i].fieldValue\"\t\trpLabel=\" \" ></rp-input> \n                            </div>\n                          </div> \n                        </div>      \n\n\n                        <div *ngIf=\"_paymentConfigobj.data[i].dataType=='lov' && (_paymentConfigobj.data[i].fieldShow==1 || _paymentConfigobj.data[i].fieldShow==2 ||_paymentConfigobj.data[i].fieldShow==3)\" class=\"form-group\" > \n                          <label class=\"control-label col-md-3\">{{_paymentConfigobj.data[i].caption}}</label>\n                          <div class=\"col-md-4\" > \n                            <select  class=\"form-control col-md-0\">\n                              <option *ngFor=\"let item2 of _paymentConfigobj.data[i].lovData\" value=\"{{item2.lovCde}}\" >{{item2.lovDesc1}}</option>\n                            </select> \n                          </div>   \n\n                        </div>\n\n                      </span>    \n                    </fieldset>\n                  </form>\n                </div>\n                <br>\n                <div class=\"modal-footer\">\n                  <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n                </div>\n              </div>\n            </div>\n          </div>\n\n          <div class=\"form-group\">        \n            <form>\n              <table class=\"table table-bordered\">  \n                <table class=\"table table-striped\" id=\"myTable\">             \n                  <tbody>\n                    <tr> \n                      <td> <b>Transaction Date</b></td>               \n                      <td class=\"col-md-9\" style=\"padding-left:3%;\">            \n                        <div class=\"form-group\" style=\"font-size:15px\">\n                          <rp-input rpType=\"text\" rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[0].caption\" rpLabel=\"Caption\" rpRequired=\"true\"></rp-input>                      \n                        </div> \n\n                        <div class=\"form-group\" style=\"font-size:15px\">                \n                          <rp-input rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[0].fieldValue\" rpLabel=\"Field Type\" rpReadonly=\"true\">Date</rp-input>                           \n                        </div>                 \n\n                        <div class=\"form-group\" style=\"font-size:15px\">\n                          <rp-input  rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[0].fOrder\" (keypress)=\"fOrderKeyup($event,obj)\" rpRequired=\"true\" rpLabel=\"Display Order\"  required autofocus ></rp-input>                      \n                        </div>\n\n                      </td>                                                                   \n                    </tr> \n                  </tbody> \n\n                  <tbody>\n                    <tr> \n                      <td><b>Transaction Amount</b> </td>               \n                      <td class=\"col-md-9\" style=\"padding-left:3%;\">            \n                        <div class=\"form-group\" style=\"font-size:15px\">\n                          <rp-input rpType=\"text\" rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[1].caption\" rpLabel=\"Caption\" rpRequired=\"true\"></rp-input>                      \n                        </div>                            \n                        <div class=\"form-group\" style=\"font-size:15px\">                \n                          <rp-input rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[1].fieldValue\" rpPlaceHolder=\"Number\" rpReadonly=\"true\" rpLabel=\"Field Type\"></rp-input>                           \n                        </div>                 \n\n                        <div class=\"form-group\" style=\"font-size:15px\">\n                          <rp-input  rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[1].fOrder\" (keypress)=\"fOrderKeyup($event,obj)\" rpRequired=\"true\" rpLabel=\"Display Order\"  required autofocus ></rp-input>                      \n                        </div>\n\n                      </td>                                                                   \n                    </tr> \n                  </tbody> \n                  <tbody>\n                    <tr> \n                      <td> <b>Customer Reference (Customer Code)</b></td>               \n                      <td class=\"col-md-9\" style=\"padding-left:3%;\">            \n                        <div class=\"form-group\" style=\"font-size:15px\">\n                          <rp-input rpType=\"text\" rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[2].caption\" rpLabel=\"Caption\" rpRequired=\"true\"></rp-input>                      \n                        </div>                            \n                        <div class=\"form-group\" style=\"font-size:15px\">                \n                          <rp-input rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[2].fieldValue\" rpPlaceHolder=\"Text\" rpLabel=\"Field Type\" rpReadonly=\"true\">Date</rp-input>                           \n                        </div> \n                        <div class=\"form-group\" style=\"font-size:15px\">                \n                          <rp-input rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[2].regularExLovCode\" rpPlaceHolder=\"Text\" rpLabel=\"Regular Expression\" >Date</rp-input>                           \n                        </div>                 \n\n                        <div class=\"form-group\" style=\"font-size:15px\">\n                          <rp-input  rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[2].fOrder\" (keypress)=\"fOrderKeyup($event,obj)\" rpRequired=\"true\" rpLabel=\"Display Order\"  required autofocus ></rp-input>                      \n                        </div>\n\n                        <div class=\"col-md-8\" > \n                          <label class=\"col-md-3 control-label\" ></label> \n                          <label class=\"checkbox-inline\" >\n                            <input  type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[2].isCheckCounter\" (click)=\"checkOTCMandatory1($event.options, _paymentConfigobj.data[2].isCheckCounter,'btnotcmandatory1')\" > Counter\n                          </label>\n                          <label class=\"checkbox-inline\" style=\"padding-left:6%\">\n                            <input  type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[2].isCheckOTCMandatory\" id=\"btnotcmandatory1\" enable  > Mandatory\n                          </label>\n                        </div> \n\n                        <div class=\"col-md-8\" > \n                          <label class=\"col-md-3 control-label\" ></label> \n                          <label class=\"checkbox-inline\" >\n                            <input  type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[2].isCheckPayment\" (click)=\"checkMandatory($event.options, _paymentConfigobj.data[2].isCheckPayment,'btnmandatory')\"  > Online\n                          </label>\n                          <label class=\"checkbox-inline\" style=\"padding-left:7.6%\">\n                            <input  type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[2].isCheckMandatory\" id=\"btnmandatory\" disabled > Mandatory\n                          </label>\n                        </div> \n\n                        <div class=\"col-md-8\" > \n                          <label class=\"col-md-3 control-label\" ></label> \n                          <label class=\"checkbox-inline\" >\n                            <input  type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[2].isCheckPortal\"  id=\"btnportal\"  > Portals\n                          </label>\n                        </div> \n\n\n                      </td>                                                                   \n                    </tr> \n                  </tbody>\n\n                  <tbody>\n                    <tr> \n                      <td><b>Payment Reference (Invoice No.)</b></td>               \n                      <td class=\"col-md-11\" style=\"padding-left:3%;\">            \n                        <div class=\"form-group\" style=\"font-size:15px\">\n                          <rp-input rpType=\"text\" rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[3].caption\" rpLabel=\"Caption\" rpRequired=\"true\"></rp-input>                      \n                        </div>                            \n                        <div class=\"form-group\" style=\"font-size:15px\">                \n                          <rp-input rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[3].fieldValue\" rpPlaceHolder=\"Date\" rpLabel=\"Field Type\" rpReadonly=\"true\"></rp-input>                           \n                        </div>    \n                        <div class=\"form-group\" style=\"font-size:15px\">                \n                          <rp-input rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[3].regularExLovCode\" rpPlaceHolder=\"Text\" rpLabel=\"Regular Expression\" ></rp-input>                           \n                        </div> \t             \n\n                        <div class=\"form-group\" style=\"font-size:15px\">\n                          <rp-input  rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[3].fOrder\" (keypress)=\"fOrderKeyup($event,obj)\" rpRequired=\"true\" rpLabel=\"Display Order\"  required autofocus ></rp-input>                      \n                        </div>\n                        <div class=\"col-md-8\" > \n                          <label class=\"col-md-3 control-label\" ></label>\n                          <label class=\"checkbox-inline\">\n                            <input type=\"checkbox\"   [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[3].isCheckCounter\" (click)=\"checkOTCMandatory2($event.options, _paymentConfigobj.data[3].isCheckCounter,'btnotcmandatory2')\" >\n                            Counter\n                          </label>\n                          <label class=\"checkbox-inline\" for=\"btnotcmandatory2\" style=\"padding-left:6%\">\n                            <input type=\"checkbox\"   [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[3].isCheckOTCMandatory\" id=\"btnotcmandatory2\" enable >\n                            Mandatory\n                          </label> \n                        </div>\n                        <div class=\"col-md-8 \"> \n                          <label class=\"col-md-3 control-label\" ></label>\n                          <label class=\"checkbox-inline\" >\n                            <input type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[3].isCheckPayment\" (click)=\"checkMandatory1($event.options, _paymentConfigobj.data[3].isCheckPayment,'btnmandatory1')\" >\n                            Online\n                          </label>\n                          <label class=\"checkbox-inline\" for=\"btnmandatory1\" style=\"padding-left:7.6%\">\n                            <input type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[3].isCheckMandatory\" id=\"btnmandatory1\" disabled >\n                            Mandatory\n                          </label> \n\n                        </div>\n                        <div class=\"col-md-8 \"> \n                          <label class=\"col-md-3 control-label\" ></label>\n                          <label class=\"checkbox-inline\">\n                            <input type=\"checkbox\"   [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[3].isCheckPortal\" >\n                            Portal\n                          </label>\n                        </div>\n\n                      </td>                                                                   \n                    </tr> \n                  </tbody>\n\n                  <tbody>\n                    <tr> \n                      <td><b>Customer Name</b></td>  \n                      <td class=\"col-md-9\" style=\"padding-left:3%;\">            \n                        <div class=\"form-group\" style=\"font-size:15px\">\n                          <rp-input rpType=\"text\" rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[4].caption\" rpLabel=\"Caption\" rpRequired=\"true\"></rp-input>                      \n                        </div>                            \n                        <div class=\"form-group\" style=\"font-size:15px\">                \n                          <rp-input rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[4].fieldValue\" rpPlaceHolder=\"Date\" rpLabel=\"Field Type\" rpReadonly=\"true\"></rp-input>                           \n                        </div>    \n                        <div class=\"form-group\" style=\"font-size:15px\">                \n                          <rp-input rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[4].regularExLovCode\" rpPlaceHolder=\"Text\" rpLabel=\"Regular Expression\" ></rp-input>                           \n                        </div>              \n\n                        <div class=\"form-group\" style=\"font-size:15px\">\n                          <rp-input  rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[4].fOrder\" (keypress)=\"fOrderKeyup($event,obj)\" rpRequired=\"true\" rpLabel=\"Display Order\"  required autofocus ></rp-input>                      \n                        </div>\n                        <div class=\"col-md-8\"> \n                          <label class=\"col-md-3 control-label\" ></label>\n                          <label class=\"checkbox-inline\">\n                            <input type=\"checkbox\"   [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[4].isCheckCounter\" (click)=\"checkOTCMandatory3($event.options, _paymentConfigobj.data[4].isCheckCounter,'btnotcmandatory3')\" >\n                            Counter\n                          </label>\n                          <label class=\"checkbox-inline\" for=\"btnotcmandatory3\" style=\"padding-left:6%\">\n                            <input type=\"checkbox\"    [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[4].isCheckOTCMandatory\" id=\"btnotcmandatory3\" enable >\n                            Mandatory\n                          </label> \n                        </div>\n                        <div class=\"col-md-8\" > \n                          <label class=\"col-md-3 control-label\" ></label>\n                          <label class=\"checkbox-inline\" >\n                            <input type=\"checkbox\"   [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[4].isCheckPayment\" (click)=\"checkMandatory2($event.options, _paymentConfigobj.data[4].isCheckPayment,'btnmandatory2')\" >\n                            Online\n                          </label>\n                          <label class=\"checkbox-inline\" for=\"btnmandatory2\" style=\"padding-left:7.6%\">\n                            <input type=\"checkbox\"    [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[4].isCheckMandatory\" id=\"btnmandatory2\" disabled >\n                            Mandatory\n                          </label> \n\n                        </div>             \n                        <div class=\"col-md-8\" > \n                          <label class=\"col-md-3 control-label\" ></label> \n                          <label class=\"checkbox-inline\">\n                            <input type=\"checkbox\"    [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[4].isCheckPortal\" >\n                            Portal\n                          </label>\n                        </div>                                                                           \n                      </td>                                                           \n                    </tr> \n                  </tbody>\n\n                  <tbody>\n                    <tr> \n                      <td><b>Counter Field</b></td>        \n                      <td class=\"col-md-9\" style=\"padding-left:3%;\">             \n                        <div class=\"form-group\" style=\"font-size:15px\">\n                          <rp-input rpType=\"text\" rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[5].caption\" rpLabel=\"Caption\" rpRequired=\"true\"></rp-input>                      \n                        </div>                            \n                        <div class=\"form-group\" style=\"font-size:15px\">                \n                          <rp-input rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[5].fieldValue\" rpPlaceHolder=\"Date\" rpLabel=\"Field Type\" rpReadonly=\"true\"></rp-input>                           \n                        </div>    \n                        <div class=\"form-group\" style=\"font-size:15px\">                \n                          <rp-input rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[5].regularExLovCode\" rpPlaceHolder=\"Text\" rpLabel=\"Regular Expression\" ></rp-input>                           \n                        </div>              \n\n                        <div class=\"form-group\" style=\"font-size:15px\">\n                          <rp-input  rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[5].fOrder\" (keypress)=\"fOrderKeyup($event,obj)\" rpRequired=\"true\" rpLabel=\"Display Order\"  required autofocus ></rp-input>                      \n                        </div>\n\n                        <div class=\"col-md-8\">   \n                          <label class=\"col-md-3 control-label\" ></label> \n                          <label class=\"checkbox-inline\">\n                            <input type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[5].isCheckCounter\" (click)=\"checkOTCMandatory4($event.options, _paymentConfigobj.data[5].isCheckCounter,'btnotcmandatory4')\" >\n                            Counter\n                          </label>\n                          <label class=\"checkbox-inline\" for=\"btnotcmandatory4\" style=\"padding-left:6%\">\n                            <input type=\"checkbox\"   [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[5].isCheckOTCMandatory\" id=\"btnotcmandatory4\" enable >\n                            Mandatory\n                          </label> \n                        </div>\n                        <div class=\"col-md-8\"> \n                          <label class=\"col-md-3 control-label\" ></label> \n                          <label class=\"checkbox-inline\" >\n                            <input type=\"checkbox\"   [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[5].isCheckPayment\" (click)=\"checkMandatory3($event.options, _paymentConfigobj.data[5].isCheckPayment,'btnmandatory3')\" >\n                            Online\n                          </label>\n                          <label class=\"checkbox-inline\" for=\"btnmandatory3\" style=\"padding-left:7.6%\">\n                            <input type=\"checkbox\"   [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[5].isCheckMandatory\" id=\"btnmandatory3\" disabled >\n                            Mandatory\n                          </label>  \n                        </div>\n                        <div class=\"col-md-8\">\n                          <label class=\"col-md-3 control-label\" ></label> \n                          <label class=\"checkbox-inline\">\n                            <input type=\"checkbox\"   [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[5].isCheckPortal\" >\n                            Portal\n                          </label>\n                        </div>                                                                                         \n                      </td>                                                                   \n                    </tr> \n                  </tbody> \n                </table>\n              </table>             \n            </form>\n          </div>\n\n          <div class=\"form-group\">        \n            <form>\n              <table class=\"table table-bordered\"> \n                <h4><b>References</b></h4>  \n                <table class=\"table table-striped\" id=\"myTable\">             \n                  <tbody>\n                    <tr> \n                      <button class=\"btn btn-link\" type=\"button\" data-toggle=\"collapse\" data-target=\"#reference1\">\n                        <td style=\"font-size:17px\"> <b> Reference 1</b></td> \n                      </button>              \n                      <td class=\"col-md-9\" style=\"padding-left:3%;\"> \n                        <div id=\"reference1\" class=\"collapse in\">            \n                          <div class=\"form-group\" style=\"font-size:15px\">\n                            <rp-input rpType=\"text\" rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[6].caption\" rpLabel=\"Caption\" rpRequired=\"true\"></rp-input>                      \n                          </div>\n                          <div class=\"form-group\" style=\"font-size:15px\">\n                            <rp-input rpType=\"ref030\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[6].dataType\" (change)=\"changeFieldCode($event.target.options,_paymentConfigobj.data[6])\" rpLabel=\"Field Type\"></rp-input>                 \n                            <div *ngIf=\"_paymentConfigobj.data[6].dataType=='lov'\">                                                                           \n                              <rp-input rpType={{_paymentConfigobj.data[6].lovRef}}  rpClass = \"col-md-4\" rpLabelClass = \"col-md-0 control-label\" [(rpModel)]=\"_paymentConfigobj.data[6].lovCode\" (change)= \"changemainLov($event.target.options,_paymentConfigobj.data[6]);\" rpReadonly={{_paymentConfigobj.data[6].readonly}}></rp-input>\n                              <button id=\"btnlovnew\" class=\"btn btn-link\" type=\"button\"  (click)=\"lookup();\">New</button>\n                              <button id=\"btnlovedit\" class=\"btn btn-link\" type=\"button\" (click)=\"goEdit(_paymentConfigobj.data[6]);\">Edit</button>                   \n                            </div>  \n                          </div>               \n\n                          <div *ngIf=\"_paymentConfigobj.data[6].dataType=='text'\">                              \n                            <div class=\"form-group\" style=\"font-size:15px\">\n                              <rp-input  rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[6].regularExLovCode\" rpRequired=\"true\" rpLabel=\"Regular Expression\"  required autofocus ></rp-input>                      \n                            </div>\n                          </div>   \n                          <div class=\"form-group\" style=\"font-size:15px\">\n                            <rp-input  rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[6].fOrder\" (keypress)=\"fOrderKeyup($event,obj)\" rpRequired=\"true\" rpLabel=\"Display Order\"  required autofocus ></rp-input>                      \n                          </div>         \n                          <div class=\"col-md-8\">\n                            <label class=\"col-md-3 control-label\" ></label> \n                            <label class=\"checkbox-inline\">\n                              <input type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[6].isCheckCounter\" (click)=\"checkOTCMandatory5($event.options, _paymentConfigobj.data[6].isCheckCounter,'btnotcmandatory5')\" >\n                              Counter\n                            </label>\n                            <label class=\"checkbox-inline\" for=\"btnotcmandatory5\" style=\"padding-left:6%\">\n                              <input type=\"checkbox\" class=\"col-md-2\"   [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[6].isCheckOTCMandatory\" id=\"btnotcmandatory5\" enable >\n                              Mandatory\n                            </label> \n                          </div>\n                          <div class=\"col-md-8\"> \n                            <label class=\"col-md-3 control-label\" ></label>\n                            <label class=\"checkbox-inline\" >\n                              <input type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[6].isCheckPayment\" (click)=\"checkMandatory4($event.options, _paymentConfigobj.data[6].isCheckPayment,'btnmandatory4')\" >\n                              Online\n                            </label>\n                            <label class=\"checkbox-inline\" for=\"btnmandatory4\" style=\"padding-left:8%\">\n                              <input type=\"checkbox\"   [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[6].isCheckMandatory\" id=\"btnmandatory4\" disabled >\n                              Mandatory\n                            </label> \n\n                          </div>\n                          <div class=\"col-md-8\"> \n                            <label class=\"col-md-3 control-label\" ></label>\n                            <label class=\"checkbox-inline\" >\n                              <input type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[6].isCheckPortal\"  >\n                              Portal\n                            </label>\n                          </div>\n                        </div> \n\n                      </td>                                                                   \n                    </tr> \n                  </tbody>\n\n                  <tbody>\n                    <tr> \n                      <button class=\"btn btn-link\" type=\"button\" data-toggle=\"collapse\" data-target=\"#reference2\"> \n                        <td style=\"font-size:17px\"> <b> Reference 2</b></td>    \n                      </button>             \n                      <td class=\"col-md-9\" style=\"padding-left:3%;\"> \n                        <div id=\"reference2\" class=\"collapse\">           \n                          <div class=\"form-group\" style=\"font-size:15px\">\n                            <rp-input rpType=\"text\" rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[7].caption\" rpLabel=\"Caption\" rpRequired=\"true\"></rp-input>                      \n                          </div>                            \n                          <div class=\"form-group\" style=\"font-size:15px\">\n                            <rp-input rpType=\"ref030\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[7].dataType\" (change)=\"changeFieldCode($event.target.options,_paymentConfigobj.data[7])\" rpLabel=\"Field Type\"></rp-input>                 \n                            <div *ngIf=\"_paymentConfigobj.data[7].dataType=='lov'\">                                                                           \n                              <rp-input rpType={{_paymentConfigobj.data[7].lovRef}}  rpClass = \"col-md-4\" rpLabelClass = \"col-md-0 control-label\" [(rpModel)]=\"_paymentConfigobj.data[7].lovCode\" (change)= \"changemainLov($event.target.options,_paymentConfigobj.data[7]);\" rpReadonly={{_paymentConfigobj.data[7].readonly}}></rp-input>  \n                              <button id=\"btnlovnew\" class=\"btn btn-link\" type=\"button\"  (click)=\"lookup();\">New</button>\n                              <button id=\"btnlovedit\" class=\"btn btn-link\" type=\"button\" (click)=\"goEdit(_paymentConfigobj.data[7]);\">Edit</button> \n                            </div>  \n                          </div>            \n\n                          <div *ngIf=\"_paymentConfigobj.data[7].dataType=='text'\">                              \n                            <div class=\"form-group\" style=\"font-size:15px\">\n                              <rp-input  rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[7].regularExLovCode\" rpRequired=\"true\" rpLabel=\"Regular Expression\"  required autofocus ></rp-input>                      \n                            </div>\n                          </div> \n\n                          <div class=\"form-group\" style=\"font-size:15px\">\n                            <rp-input  rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[7].fOrder\" (keypress)=\"fOrderKeyup($event,obj)\" rpRequired=\"true\" rpLabel=\"Display Order\"  required autofocus ></rp-input>                      \n                          </div>\n\n                          <div class=\"col-md-8\">\n                            <label class=\"col-md-3 control-label\" ></label> \n                            <label class=\"checkbox-inline\" >\n                              <input type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[7].isCheckCounter\" (click)=\"checkOTCMandatory6($event.options, _paymentConfigobj.data[7].isCheckCounter,'btnotcmandatory6')\" >\n                              Counter\n                            </label>\n                            <label class=\"checkbox-inline\" for=\"btnotcmandatory6\" style=\"padding-left:6%\">\n                              <input type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[7].isCheckOTCMandatory\" id=\"btnotcmandatory6\" enable >\n                              Mandatory\n                            </label> \n                          </div>\n                          <div class=\"col-md-8\"> \n                            <label class=\"col-md-3 control-label\" ></label>\n                            <label class=\"checkbox-inline\" >\n                              <input type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[7].isCheckPayment\" (click)=\"checkMandatory5($event.options, _paymentConfigobj.data[7].isCheckPayment,'btnmandatory5')\" >\n                              Online\n                            </label>\n                            <label class=\"checkbox-inline\" for=\"btnmandatory5\" style=\"padding-left:8%\">\n                              <input type=\"checkbox\"   [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[7].isCheckMandatory\" id=\"btnmandatory5\" disabled >\n                              Mandatory\n                            </label>\n                          </div> \n                          <div class=\"col-md-8\"> \n                            <label class=\"col-md-3 control-label\" ></label>\n                            <label class=\"checkbox-inline\">\n                              <input type=\"checkbox\"   [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[7].isCheckPortal\" >\n                              Portal\n                            </label>\n                          </div>\n                        </div>\n                      </td>                                                                   \n                    </tr>\n                  </tbody>\n                  <tbody>\n                    <tr> \n                      <button class=\"btn btn-link\" type=\"button\" data-toggle=\"collapse\" data-target=\"#reference3\"> \n                        <td style=\"font-size:17px\"> <b> Reference 3</b></td>    \n                      </button>             \n                      <td class=\"col-md-9\" style=\"padding-left:3%;\"> \n                        <div id=\"reference3\" class=\"collapse\">           \n                          <div class=\"form-group\" style=\"font-size:15px\">\n                            <rp-input rpType=\"text\" rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[8].caption\" rpLabel=\"Caption\" rpRequired=\"true\"></rp-input>                      \n                          </div>                            \n                          <div class=\"form-group\" style=\"font-size:15px\">\n                            <rp-input rpType=\"ref030\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[8].dataType\" (change)=\"changeFieldCode($event.target.options,_paymentConfigobj.data[8])\" rpLabel=\"Field Type\"></rp-input>                 \n                            <div *ngIf=\"_paymentConfigobj.data[8].dataType=='lov'\">                                                                           \n                              <rp-input rpType={{_paymentConfigobj.data[8].lovRef}}  rpClass = \"col-md-4\" rpLabelClass = \"col-md-0 control-label\" [(rpModel)]=\"_paymentConfigobj.data[8].lovCode\" (change)= \"changemainLov($event.target.options,_paymentConfigobj.data[8]);\" \n                              rpReadonly={{_paymentConfigobj.data[8].readonly}}></rp-input>  \n                              <button id=\"btnlovnew\" class=\"btn btn-link\" type=\"button\"  (click)=\"lookup();\">New</button>\n                              <button id=\"btnlovedit\" class=\"btn btn-link\" type=\"button\" (click)=\"goEdit(_paymentConfigobj.data[8]);\">Edit</button> \n                            </div>  \n                          </div>            \n\n                          <div *ngIf=\"_paymentConfigobj.data[8].dataType=='text'\">                              \n                            <div class=\"form-group\" style=\"font-size:15px\">\n                              <rp-input  rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[8].regularExLovCode\" rpRequired=\"true\" rpLabel=\"Regular Expression\"  required autofocus ></rp-input>                      \n                            </div>\n                          </div> \n\n                          <div class=\"form-group\" style=\"font-size:15px\">\n                            <rp-input  rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[8].fOrder\" (keypress)=\"fOrderKeyup($event,obj)\" rpRequired=\"true\" rpLabel=\"Display Order\"  required autofocus ></rp-input>                      \n                          </div>\n\n                          <div class=\"col-md-8\"> \n                            <label class=\"col-md-3 control-label\" ></label>\n                            <label class=\"checkbox-inline\" >\n                              <input type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[8].isCheckPayment\" (click)=\"checkMandatory6($event.options, _paymentConfigobj.data[8].isCheckPayment,'btnmandatory6')\" >\n                              Online\n                            </label>\n                            <label class=\"checkbox-inline\" for=\"btnmandatory6\">\n                              <input type=\"checkbox\"   [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[8].isCheckMandatory\" id=\"btnmandatory6\"   disabled >\n                              Mandatory\n                            </label> \n                            <label class=\"checkbox-inline\" >\n                              <input type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[8].isCheckPortal\"  >\n                              Portal\n                            </label> \n                          </div> \n                        </div>                                                                             \n                      </td>                                                                   \n                    </tr>\n                  </tbody>\n                </table>\n              </table>\n            </form>\n          </div>\n          <div class=\"form-group\">        \n            <form>\n              <table class=\"table table-bordered\"> \n                <h4><b>SubTypes</b></h4>  \n                <table class=\"table table-striped\" id=\"myTable\">\n                  <tbody>\n                    <tr> \n                      <button class=\"btn btn-link\" type=\"button\" data-toggle=\"collapse\" data-target=\"#subType1\"> \n                        <td style=\"font-size:17px\"> <b> Sub Type 1</b></td>    \n                      </button>             \n                      <td class=\"col-md-9\" style=\"padding-left:3%;\"> \n                        <div id=\"subType1\" class=\"collapse in\">           \n                          <div class=\"form-group\" style=\"font-size:15px\">\n                            <rp-input rpType=\"text\" rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[9].caption\" rpLabel=\"Caption\" rpRequired=\"true\"></rp-input>                      \n                          </div>                            \n                          <div class=\"form-group\" style=\"font-size:15px\">\n                            <rp-input rpType=\"ref030\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[9].dataType\" (change)=\"changeFieldCode($event.target.options,_paymentConfigobj.data[9])\" rpLabel=\"Field Type\"></rp-input>                 \n                            <div *ngIf=\"_paymentConfigobj.data[9].dataType=='lov'\">                                                                           \n                              <rp-input rpType={{_paymentConfigobj.data[9].lovRef}}  rpClass = \"col-md-4\" rpLabelClass = \"col-md-0 control-label\" [(rpModel)]=\"_paymentConfigobj.data[9].lovCode\" (change)= \"changemainLov($event.target.options,_paymentConfigobj.data[9]);\" \n                              rpReadonly={{_paymentConfigobj.data[9].readonly}}></rp-input>  \n                              <button id=\"btnlovnew\" class=\"btn btn-link\" type=\"button\"  (click)=\"lookup();\">New</button>\n                              <button id=\"btnlovedit\" class=\"btn btn-link\" type=\"button\" (click)=\"goEdit(_paymentConfigobj.data[9]);\">Edit</button> \n                            </div>  \n                          </div>            \n\n                          <div *ngIf=\"_paymentConfigobj.data[9].dataType=='text'\">                              \n                            <div class=\"form-group\" style=\"font-size:15px\">\n                              <rp-input  rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[9].regularExLovCode\" rpRequired=\"true\" rpLabel=\"Regular Expression\"  required autofocus ></rp-input>                      \n                            </div>\n                          </div> \n\n                          <div class=\"form-group\" style=\"font-size:15px\">\n                            <rp-input  rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[9].fOrder\" (keypress)=\"fOrderKeyup($event,obj)\" rpRequired=\"true\" rpLabel=\"Display Order\"  required autofocus ></rp-input>                      \n                          </div>\n\n                          <div class=\"col-md-8\"> \n                            <label class=\"col-md-3 control-label\" ></label>\n                            <label class=\"checkbox-inline\" >\n                              <input type=\"checkbox\"   [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[9].isCheckPayment\" (click)=\"checkMandatory7($event.options, _paymentConfigobj.data[9].isCheckPayment,'btnmandatory7')\" >\n                              Online\n                            </label>\n                            <label class=\"checkbox-inline\" for=\"btnmandatory7\">\n                              <input type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[9].isCheckMandatory\" id=\"btnmandatory7\"    disabled >\n                              Mandatory\n                            </label> \n                            <label class=\"checkbox-inline\" >\n                              <input type=\"checkbox\"    [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[9].isCheckPortal\"  >\n                              Portal\n                            </label> \n                          </div>\t\n\n                        </div>                                                                                 \n                      </td>                                                                   \n                    </tr>\n                  </tbody>\n                  <tbody>\n                    <tr>\n                      <button class=\"btn btn-link\" type=\"button\" data-toggle=\"collapse\" data-target=\"#subType2\"> \n                        <td style=\"font-size:17px\"> <b> Sub Type 2</b></td>     \n                      </button>          \n                      <td class=\"col-md-9\" style=\"padding-left:3%;\">\n                        <div id=\"subType2\" class=\"collapse\">            \n                          <div class=\"form-group\" style=\"font-size:15px\">\n                            <rp-input rpType=\"text\" rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[10].caption\" rpLabel=\"Caption\" rpRequired=\"true\"></rp-input>                      \n                          </div>                            \n                          <div class=\"form-group\" style=\"font-size:15px\">\n                            <rp-input rpType=\"ref030\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[10].dataType\" (change)=\"changeFieldCode($event.target.options,_paymentConfigobj.data[10])\" rpLabel=\"Field Type\"></rp-input>                 \n                            <div *ngIf=\"_paymentConfigobj.data[10].dataType=='lov'\">                                                                           \n                              <rp-input rpType={{_paymentConfigobj.data[10].lovRef}}  rpClass = \"col-md-4\" rpLabelClass = \"col-md-0 control-label\" [(rpModel)]=\"_paymentConfigobj.data[10].lovCode\" (change)= \"changemainLov($event.target.options,_paymentConfigobj.data[10]);\" rpReadonly={{_paymentConfigobj.data[10].readonly}}></rp-input>  \n                              <button id=\"btnlovnew\" class=\"btn btn-link\" type=\"button\"  (click)=\"lookup();\">New</button>\n                              <button id=\"btnlovedit\" class=\"btn btn-link\" type=\"button\" (click)=\"goEdit(_paymentConfigobj.data[10]);\">Edit</button> \t\t\t\t  \n                            </div>  \n                          </div>                 \n\n                          <div *ngIf=\"_paymentConfigobj.data[10].dataType=='text'\">                              \n                            <div class=\"form-group\" style=\"font-size:15px\">\n                              <rp-input  rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[10].regularExLovCode\" rpRequired=\"true\" rpLabel=\"Regular Expression\"  required autofocus ></rp-input>                      \n                            </div>\n                          </div> \n                          <div class=\"form-group\" style=\"font-size:15px\">\n                            <rp-input  rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[10].fOrder\" (keypress)=\"fOrderKeyup($event,obj)\" rpRequired=\"true\" rpLabel=\"Display Order\"  required autofocus ></rp-input>                      \n                          </div>\n                          <div class=\"col-md-8\"> \n                            <label class=\"col-md-3 control-label\" ></label>\n                            <label class=\"checkbox-inline\" >\n                              <input type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[10].isCheckPayment\" (click)=\"checkMandatory8($event.options, _paymentConfigobj.data[10].isCheckPayment,'btnmandatory8')\" >\n                              Online\n                            </label>\n                            <label class=\"checkbox-inline\" for=\"btnmandatory8\">\n                              <input type=\"checkbox\"    [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[10].isCheckMandatory\" id=\"btnmandatory8\" disabled >\n                              Mandatory\n                            </label> \n                            <label class=\"checkbox-inline\" >\n                              <input type=\"checkbox\"   [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[10].isCheckPortal\"  >\n                              Portal\n                            </label> \n                          </div>\n                        </div>                                                                             \n                      </td>                                                                   \n                    </tr> \n                  </tbody>\n                  <tbody>\n                    <tr> \n                      <button class=\"btn btn-link\" type=\"button\" data-toggle=\"collapse\" data-target=\"#subType3\">\n                        <td style=\"font-size:17px\"> <b> Sub Type 3</b></td>   \n                      </button>           \n                      <td class=\"col-md-9\" style=\"padding-left:3%;\">\n                        <div id=\"subType3\" class=\"collapse\">            \n                          <div class=\"form-group\" style=\"font-size:15px\">\n                            <rp-input rpType=\"text\" rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[11].caption\" rpLabel=\"Caption\" rpRequired=\"true\"></rp-input>                      \n                          </div>                            \n                          <div class=\"form-group\" style=\"font-size:15px\">\n                            <rp-input rpType=\"ref030\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[11].dataType\" (change)=\"changeFieldCode($event.target.options,_paymentConfigobj.data[11])\" rpLabel=\"Field Type\"></rp-input>                 \n                            <div *ngIf=\"_paymentConfigobj.data[11].dataType=='lov'\">                                                                           \n                              <rp-input rpType={{_paymentConfigobj.data[11].lovRef}}  rpClass = \"col-md-4\" rpLabelClass = \"col-md-0 control-label\" (change)= \"changemainLov($event.target.options,_paymentConfigobj.data[11]);\" [(rpModel)]=\"_paymentConfigobj.data[11].lovCode\" rpReadonly={{_paymentConfigobj.data[11].readonly}}></rp-input> \n                              <button id=\"btnlovnew\" class=\"btn btn-link\" type=\"button\"  (click)=\"lookup();\">New</button>\n                              <button id=\"btnlovedit\" class=\"btn btn-link\" type=\"button\" (click)=\"goEdit(_paymentConfigobj.data[11]);\">Edit</button>                   \n                            </div>  \n                          </div>                 \n\n                          <div *ngIf=\"_paymentConfigobj.data[11].dataType=='text'\">                              \n                            <div class=\"form-group\" style=\"font-size:15px\">\n                              <rp-input  rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[11].regularExLovCode\" rpRequired=\"true\" rpLabel=\"Regular Expression\"  required autofocus ></rp-input>                      \n                            </div>\n                          </div> \n                          <div class=\"form-group\" style=\"font-size:15px\">\n                            <rp-input  rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[11].fOrder\" (keypress)=\"fOrderKeyup($event,obj)\" rpRequired=\"true\" rpLabel=\"Display Order\"  required autofocus ></rp-input>                      \n                          </div>\n                          <div class=\"col-md-8 columns\"> \n                            <label class=\"col-md-3 control-label\" ></label>\n                            <label class=\"checkbox-inline\" >\n                              <input type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[11].isCheckPayment\" (click)=\"checkMandatory9($event.options, _paymentConfigobj.data[11].isCheckPayment,'btnmandatory9')\" >\n                              Online\n                            </label>\n                            <label class=\"checkbox-inline\" for=\"btnmandatory9\">\n                              <input type=\"checkbox\"   [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[11].isCheckMandatory\" id=\"btnmandatory9\"    disabled >\n                              Mandatory\n                            </label> \n                            <label class=\"checkbox-inline\" >\n                              <input type=\"checkbox\"   [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[11].isCheckPortal\"  >\n                              Portal\n                            </label> \n                          </div>\n                        </div>                                                                             \n                      </td>                                                                   \n                    </tr> \n                  </tbody>\n                </table>\n              </table>             \n            </form>\n          </div>\n          <div class=\"form-group\">        \n            <form>\n              <table class=\"table table-bordered\">\n                <h4><b>Other Data Fields</b></h4>    \n                <table class=\"table table-striped\" id=\"myTable\">                             \n                  <tbody>\n                    <tr> \n                      <td><b>Branch Code</b></td>               \n                      <td class=\"col-md-9\" style=\"padding-left:3%;\">            \n                        <div class=\"form-group\" style=\"font-size:15px\">\n                          <rp-input rpType=\"text\" rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[27].caption\" rpLabel=\"Caption\" rpRequired=\"true\"></rp-input>                      \n                        </div>                            \n                        <div class=\"form-group\" style=\"font-size:15px\">\n                          <rp-input  rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[27].fOrder\" (keypress)=\"fOrderKeyup($event,obj)\" rpRequired=\"true\" rpLabel=\"Display Order\"  required autofocus ></rp-input>                      \n                        </div>\n                        <div class=\"col-md-8\">\n                          <label class=\"col-md-3 control-label\" ></label> \n                          <label class=\"checkbox-inline\" >\n                            <input type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[27].isCheckPayment\" (click)=\"checkMandatory25($event.options, _paymentConfigobj.data[27].isCheckPayment,'btnmandatory25')\" >\n                            Online\n                          </label>\n                          <label class=\"checkbox-inline\" for=\"btnmandatory25\">\n                            <input type=\"checkbox\"   [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[27].isCheckMandatory\" id=\"btnmandatory25\"  disabled >\n                            Mandatory\n                          </label> \n                          <label class=\"checkbox-inline\" >\n                            <input type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[27].isCheckPortal\"  >\n                            Portal\n                          </label> \n                        </div>                                                                     \n                      </td>                                                                   \n                    </tr> \n                  </tbody>\n                  <tbody>\n                    <tr> \n                      <td><b>From Account</b> </td>               \n                      <td class=\"col-md-9\" style=\"padding-left:3%;\">            \n                        <div class=\"form-group\" style=\"font-size:15px\">\n                          <rp-input rpType=\"text\" rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[28].caption\" rpLabel=\"Caption\" rpRequired=\"true\"></rp-input>                      \n                        </div>                            \n                        <div class=\"form-group\" style=\"font-size:15px\">\n                          <rp-input  rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[28].fOrder\" (keypress)=\"fOrderKeyup($event,obj)\" rpRequired=\"true\" rpLabel=\"Display Order\"  required autofocus ></rp-input>                      \n                        </div>\n                        <div class=\"col-md-8\"> \n                          <label class=\"col-md-3 control-label\" ></label>\n                          <label class=\"checkbox-inline\" >\n                            <input type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[28].isCheckPayment\" (click)=\"checkMandatory26($event.options, _paymentConfigobj.data[28].isCheckPayment,'btnmandatory26')\" >\n                            Online\n                          </label>\n                          <label class=\"checkbox-inline\" for=\"btnmandatory26\">\n                            <input type=\"checkbox\"   [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[28].isCheckMandatory\" id=\"btnmandatory26\"  disabled >\n                            Mandatory\n                          </label> \n                          <label class=\"checkbox-inline\" >\n                            <input type=\"checkbox\"   [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[28].isCheckPortal\"  >\n                            Portal\n                          </label> \n                        </div>                                                                             \n                      </td>                                                                   \n                    </tr> \n                  </tbody>\n                  <tbody>\n                    <tr> \n                      <td><b>To Account</b></td>               \n                      <td class=\"col-md-9\" style=\"padding-left:3%;\">            \n                        <div class=\"form-group\" style=\"font-size:15px\">\n                          <rp-input rpType=\"text\" rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[29].caption\" rpLabel=\"Caption\" rpRequired=\"true\"></rp-input>                      \n                        </div>                            \n                        <div class=\"form-group\" style=\"font-size:15px\">\n                          <rp-input  rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[29].fOrder\" (keypress)=\"fOrderKeyup($event,obj)\" rpRequired=\"true\" rpLabel=\"Display Order\"  required autofocus ></rp-input>                      \n                        </div>\n                        <div class=\"col-md-8\" > \n                          <label class=\"col-md-3 control-label\" ></label>\n                          <label class=\"checkbox-inline\" >\n                            <input type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[29].isCheckPayment\" (click)=\"checkMandatory27($event.options, _paymentConfigobj.data[29].isCheckPayment,'btnmandatory27')\" >\n                            Online\n                          </label>\n                          <label class=\"checkbox-inline\" for=\"btnmandatory27\">\n                            <input type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[29].isCheckMandatory\" id=\"btnmandatory27\"  disabled >\n                            Mandatory\n                          </label> \n                          <label class=\"checkbox-inline\" >\n                            <input type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[29].isCheckPortal\"  >\n                            Portal\n                          </label> \n                        </div>                                                                           \n                      </td>                                                                   \n                    </tr> \n                  </tbody>\n                  <tbody>\n                    <tr> \n                      <td><b>Cheque Number</b></td>               \n                      <td class=\"col-md-9\" style=\"padding-left:3%;\">            \n                        <div class=\"form-group\" style=\"font-size:15px\">\n                          <rp-input rpType=\"text\" rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[30].caption\" rpLabel=\"Caption\" rpRequired=\"true\"></rp-input>                      \n                        </div>                            \n                        <div class=\"form-group\" style=\"font-size:15px\">\n                          <rp-input  rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[30].fOrder\" (keypress)=\"fOrderKeyup($event,obj)\" rpRequired=\"true\" rpLabel=\"Display Order\"  required autofocus ></rp-input>                      \n                        </div>\n                        <div class=\"col-md-8\"> \n                          <label class=\"col-md-3 control-label\" ></label>\n                          <label class=\"checkbox-inline\" >\n                            <input type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[30].isCheckPayment\" (click)=\"checkMandatory28($event.options, _paymentConfigobj.data[30].isCheckPayment,'btnmandatory28')\" >\n                            Online\n                          </label>\n                          <label class=\"checkbox-inline\" for=\"btnmandatory28\">\n                            <input type=\"checkbox\"   [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[30].isCheckMandatory\" id=\"btnmandatory28\"  disabled >\n                            Mandatory\n                          </label> \n                          <label class=\"checkbox-inline\" >\n                            <input type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[30].isCheckPortal\"  >\n                            Portal\n                          </label> \n                        </div>                                                                            \n                      </td>                                                                   \n                    </tr> \n                  </tbody>\n                  <tbody>\n                    <tr> \n                      <td><b>Currency Code </b></td>               \n                      <td class=\"col-md-9\" style=\"padding-left:3%;\">            \n                        <div class=\"form-group\" style=\"font-size:15px\">\n                          <rp-input rpType=\"text\" rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[31].caption\" rpLabel=\"Caption\" rpRequired=\"true\"></rp-input>                      \n                        </div>                            \n                        <div class=\"form-group\" style=\"font-size:15px\">\n                          <rp-input  rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[31].fOrder\" (keypress)=\"fOrderKeyup($event,obj)\" rpRequired=\"true\" rpLabel=\"Display Order\"  required autofocus ></rp-input>                      \n                        </div>\n                        <div class=\"col-md-8\" >\n                          <label class=\"col-md-3 control-label\" ></label> \n                          <label class=\"checkbox-inline\" >\n                            <input type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[31].isCheckPayment\" (click)=\"checkMandatory29($event.options, _paymentConfigobj.data[31].isCheckPayment,'btnmandatory29')\" >\n                            Online\n                          </label>\n                          <label class=\"checkbox-inline\" for=\"btnmandatory29\">\n                            <input type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[31].isCheckMandatory\" id=\"btnmandatory29\"  disabled >\n                            Mandatory\n                          </label> \n                          <label class=\"checkbox-inline\" >\n                            <input type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[31].isCheckPortal\"  >\n                            Portal\n                          </label> \n                        </div>                                                                             \n                      </td>                                                                   \n                    </tr> \n                  </tbody>\n                  <tbody>\n                    <tr> \n                      <td><b>Is VIP</b></td>               \n                      <td class=\"col-md-9\" style=\"padding-left:3%;\">            \n                        <div class=\"form-group\" style=\"font-size:15px\">\n                          <rp-input rpType=\"text\" rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[32].caption\" rpLabel=\"Caption\" rpRequired=\"true\"></rp-input>                      \n                        </div>                            \n                        <div class=\"form-group\" style=\"font-size:15px\">\n                          <rp-input  rpType=\"text\"  rpClass = \"col-md-4\"  rpLabelClass = \"col-md-2 control-label\" [(rpModel)]=\"_paymentConfigobj.data[32].fOrder\" (keypress)=\"fOrderKeyup($event,obj)\" rpRequired=\"true\" rpLabel=\"Display Order\"  required autofocus ></rp-input>                      \n                        </div>\n                        <div class=\"col-md-8\"> \n                          <label class=\"col-md-3 control-label\" ></label>\n                          <label class=\"checkbox-inline\" >\n                            <input type=\"checkbox\"  [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[32].isCheckPayment\" (click)=\"checkMandatory30($event.options, _paymentConfigobj.data[32].isCheckPayment,'btnmandatory30')\" >\n                            Online\n                          </label>\n                          <label class=\"checkbox-inline\" for=\"btnmandatory30\">\n                            <input type=\"checkbox\"   [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[32].isCheckMandatory\" id=\"btnmandatory30\"  disabled >\n                            Mandatory\n                          </label> \n                          <label class=\"checkbox-inline\" >\n                            <input type=\"checkbox\"   [ngModelOptions]=\"{standalone: true}\"  [(ngModel)]=\"_paymentConfigobj.data[32].isCheckPortal\"  >\n                            Portal\n                          </label> \n                        </div>                                                                         \n                      </td>                                                                   \n                    </tr> \n                  </tbody>          \n                </table>\n              </table>             \n            </form>\n          </div> \n          <div>\n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"goList();\" >List</button> \n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"goNew();\" >New</button>\n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"Validation();\">Save</button> \n            <button class=\"btn btn-primary\" id=\"mydelete2\" type=\"button\" (click)=\"goDelete();\" disabled>Delete</button>\n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"lookuppreview();\">Preview</button>\n          </div>               \n        </form> \n      </div>\n    </div>\n  </div> \n\n  <div id=\"lookupLOV\" class=\"modal fade\" role=\"dialog\"><!--Edit Lov popup -->\n    <div id=\"lookupLOVSize\" class=\"modal-dialog modal-lg\">  \n      <div class=\"modal-content\" style=\"\">\n        <div class=\"modal-header\">\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n        </div>\n        <div class=\"modal-body\"   style=\"  margin-left: 70px;\">\n\n          <form class=\"form-horizontal\" (ngSubmit)=\"goSaveLov()\" >\n            <fieldset>  \n              <legend>List of Values</legend>\n              <div class=\"form-group\"> \n                <div class=\"col-md-12\">\n                  <button type=\"button\" class=\"btn btn-primary btn-md\" (click)=\"goNewLov()\">New</button>\n                  <button type=\"submit\" class=\"btn btn-primary btn-md\">Save</button>\n                </div>\n              </div>\n\n              <div class=\"col-md-12\">&nbsp;</div>\n              <div class=\"col-md-12\" id=\"custom-form-alignment-margin\">\n                <div class=\"col-md-6\">\n                  <div class=\"form-group\">\n                    <rp-input [(rpModel)]=\"_lovsetupobj.lovNo\" rpClass = \"col-md-8\" rpLabelClass = \"col-md-4 control-label\" rpType=\"text\" rpLabel=\"Code\" rpPlaceHolder = \"TBA\" rpReadonly = \"true\">TBA</rp-input>         \n                  </div>         \n                  <div class=\"form-group\">\n                    <rp-input rpRequired = \"true\" [(rpModel)]=\"_lovsetupobj.lovDesc2\" rpClass = \"col-md-8\" rpLabelClass = \"col-md-4 control-label\" rpType=\"text\" rpLabel=\"Description\" ></rp-input>                            \n                  </div> \n                  <div class=\"col-md-12\">&nbsp;</div>         \n                </div>\n\n                <div class=\"form-group\">\n                  <div class=\"col-md-10\">\n                    <table class=\"table table-striped table-condensed table-hover tblborder\" style=\"font-size:14px;margin-left:30px;\">\n                      <thead>\n                        <tr>\n                          <th>No.</th>\n                          <th>Value</th>\n                          <th>Description</th>\n                          <th>Price</th>\n                          <th>&nbsp;</th>\n                        </tr>\n                      </thead>\n                      <tbody>\n                        <tr *ngFor=\"let obj of _lovsetupobj.data\">\n                          <td style=\"vertical-align:middle;text-align:right;\" class=\"col-md-1\">{{obj.srno}}</td>\n                          <td class=\"col-md-3\">\n                            <rp-input rpRequired = \"true\" [(rpModel)]=\"obj.lovCde\" rpType=\"text\"  rpClass = \"col-md-0\"></rp-input>\n                          </td>\n                          <td>\n                            <rp-input rpRequired = \"true\" [(rpModel)]=\"obj.lovDesc1\" rpType=\"text\"  rpClass = \"col-md-0\"></rp-input>\n                          </td>\n                          <td class=\"col-md-2\">\n                            <input id=\"numinput\"  class=\"form-control\" type = \"number\" [(ngModel)]=\"obj.price\" [ngModelOptions]=\"{standalone: true}\" required>                    \n                          </td>\n                          <td><img src=\"image/remove.png\" alt=\"remove.png\" height=\"20\" width=\"20\"  (click)=\"goRemove(obj)\"/></td>\n                        </tr> \n                        <tr>\n                          <td>             \n                            <button class=\"btn btn-primary\" type=\"button\" (click)=\"goAddLovsetup()\">Add</button>              \n                          </td>\n                        </tr>\n                      </tbody>\n                    </table>\n                  </div>\n                </div>\n              </div>\n              <div class=\"form-group\"> \n                <div class=\"col-md-12\">\n                  <button type=\"button\" class=\"btn btn-primary btn-md\" (click)=\"footergoNewLov\">New</button>\n                  <button type=\"submit\" class=\"btn btn-primary btn-md\">Save</button>            \n                </div>\n              </div>\n            </fieldset>\n          </form>\n          <br>\n          <div class=\"modal-footer\">\n            <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\">Close</button>\n          </div>\n        </div>\n      </div>\n    </div> \n  </div>\n\n  <div id=\"sessionalert\" class=\"modal fade\">\n    <div class=\"modal-dialog\">\n      <div class=\"modal-content\">\n        <div class=\"modal-body\">\n          <p>{{sessionAlertMsg}}</p>\n        </div>\n        <div class=\"modal-footer\">\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div [hidden] = \"_mflag\">\n    <div class=\"modal\" id=\"loader\"></div>\n  </div>\n  "
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, router_1.Router, rp_client_util_1.ClientUtil, router_1.ActivatedRoute, rp_http_service_1.RpHttpService, rp_references_1.RpReferences, platform_browser_1.DomSanitizer])
    ], FrmPaymentTransactionConfigComponent);
    return FrmPaymentTransactionConfigComponent;
}());
exports.FrmPaymentTransactionConfigComponent = FrmPaymentTransactionConfigComponent;
//# sourceMappingURL=frmpaymenttransactionconfig.component.js.map