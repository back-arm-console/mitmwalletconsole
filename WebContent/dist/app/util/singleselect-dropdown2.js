/*
 * Angular 2 Dropdown Multiselect for Bootstrap
 * Current version: 0.1.0
 *
 * Simon Lindh
 * https://github.com/softsimon/angular-2-dropdown-multiselect
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var SingleselectDropdown2 = (function () {
    function SingleselectDropdown2(element) {
        this.element = element;
        this.selectedModel = [];
        this.model = new core_1.EventEmitter();
        this.selectionLimitReached = new core_1.EventEmitter();
        this.search = new forms_1.FormControl();
        this.numSelected = 0;
        this.isVisible = false;
        this.searchFilterText = '';
        this.defaultSettings = {
            pullRight: false,
            enableSearch: false,
            checkedStyle: 'checkboxes',
            buttonClasses: 'btn btn-default',
            selectionLimit: 0,
            closeOnSelect: false,
            dynamicTitleMaxItems: 3,
            maxHeight: '300px',
        };
        this.defaultTexts = {
            checked: 'selected',
            checkedPlural: 'selected',
            searchPlaceholder: 'Search...',
            defaultTitle: 'Select',
        };
    }
    SingleselectDropdown2.prototype.onClick = function (target) {
        var parentFound = false;
        while (target !== null && !parentFound) {
            if (target === this.element.nativeElement) {
                parentFound = true;
            }
            target = target.parentElement;
        }
        if (!parentFound) {
            this.isVisible = false;
        }
    };
    SingleselectDropdown2.prototype.ngOnInit = function () {
        var _this = this;
        this.settings = Object.assign(this.defaultSettings, this.settings);
        this.texts = Object.assign(this.defaultTexts, this.texts);
        this.updateNumSelected();
        this.search.valueChanges
            .subscribe(function (text) {
            _this.searchFilterText = text;
        });
    };
    SingleselectDropdown2.prototype.clearSearch = function () {
        this.search.setValue('');
    };
    SingleselectDropdown2.prototype.toggleDropdown = function () {
        this.search.setValue('');
        this.isVisible = !this.isVisible;
    };
    SingleselectDropdown2.prototype.modelChanged = function () {
        this.updateNumSelected();
        this.model.emit(this.selectedModel);
    };
    SingleselectDropdown2.prototype.isSelected = function (option) {
        return this.selectedModel.indexOf(parseInt(option.id)) > -1;
    };
    SingleselectDropdown2.prototype.setSelected = function (event, option) {
        this.selectedModel[0] = 0;
        var index = this.selectedModel.indexOf(parseInt(option.id));
        if (index > 0) {
        }
        else {
            this.selectedModel[0] = parseInt(option.id);
        }
        jQuery("#test").hide();
        if (this.settings.closeOnSelect) {
            this.toggleDropdown();
        }
        this.modelChanged();
    };
    SingleselectDropdown2.prototype.getTitle = function () {
        var _this = this;
        if (this.selectedModel[0] == 0) {
            return this.texts.defaultTitle;
        }
        if (this.numSelected === 0) {
            return this.texts.defaultTitle;
        }
        if (this.settings.dynamicTitleMaxItems >= this.numSelected) {
            return this.options
                .filter(function (option) { return _this.selectedModel.indexOf(parseInt(option.id)) > -1; })
                .map(function (option) { return option.name; })
                .join(', ');
        }
        return this.numSelected + ' ' + (this.numSelected === 1 ? this.texts.checked : this.texts.checkedPlural);
    };
    SingleselectDropdown2.prototype.updateNumSelected = function () {
        this.numSelected = this.selectedModel.length;
    };
    SingleselectDropdown2.prototype.getFullManualAmount = function (str) {
        if (str != undefined && str != null) {
            if (localStorage.getItem("systemfont") != "uni") {
                if (isMyanmar(str))
                    str = ZgtoUni(str);
            }
            return str;
        }
        else {
            return '';
        }
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], SingleselectDropdown2.prototype, "options", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], SingleselectDropdown2.prototype, "settings", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], SingleselectDropdown2.prototype, "texts", void 0);
    __decorate([
        core_1.Input('defaultModel'), 
        __metadata('design:type', Array)
    ], SingleselectDropdown2.prototype, "selectedModel", void 0);
    __decorate([
        core_1.Output('selectedModel'), 
        __metadata('design:type', Object)
    ], SingleselectDropdown2.prototype, "model", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], SingleselectDropdown2.prototype, "selectionLimitReached", void 0);
    __decorate([
        core_1.HostListener('document: click', ['$event.target']), 
        __metadata('design:type', Function), 
        __metadata('design:paramtypes', [Object]), 
        __metadata('design:returntype', void 0)
    ], SingleselectDropdown2.prototype, "onClick", null);
    SingleselectDropdown2 = __decorate([
        core_1.Component({
            selector: 'ss-singleselect-dropdown2',
            styles: ["\n\t\ta { outline: none; }\n\t"],
            template: "\n        <div class=\"btn-group\">\n            <button id=\"button\" type=\"button\" class=\"dropdown-toggle btn col-md-12\" style=\"min-height:32px;\" [ngClass]=\"settings.buttonClasses\" (click)=\"toggleDropdown()\" title=\"{{getFullManualAmount(getTitle())}}\">{{ getTitle() }}<span class=\"pull-right\"><span class=\"caret\"></span></span></button>\n            <ul id=\"test\" *ngIf=\"isVisible\" class=\"dropdown-menu\" [class.pull-right]=\"settings.pullRight\" [style.max-height]=\"settings.maxHeight\" style=\"display: block; height: auto; overflow-y: auto;\">\n                <li *ngIf=\"settings.enableSearch\">\n                    <div class=\"input-group input-group-sm\" >\n                        <!-- <span class=\"input-group-addon\" id=\"sizing-addon3\"><i class=\"fa fa-search\"></i></span> -->\n                        <span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-search\" style=\"cursor: pointer\"></i></span>\n                        <input type=\"text\" class=\"form-control\" placeholder=\"{{ texts.searchPlaceholder }}\" aria-describedby=\"sizing-addon3\" [formControl]=\"search\">\n                        <span class=\"input-group-btn\" *ngIf=\"searchFilterText.length > 0\">\n                            <button class=\"btn btn-default\" type=\"button\" (click)=\"clearSearch()\"><i class=\"fa fa-times\"></i></button>\n                        </span>\n                    </div>\n                </li>\n                \n                <li *ngFor=\"let option of options | searchFilter:searchFilterText\" title=\"{{getFullManualAmount(option.name)}}\">\n                    <a  role=\"menuitem\" tabindex=\"-1\" (click)=\"setSelected($event, option)\" class=\"singleselectwrap\">\n                        <span *ngIf=\"settings.checkedStyle == 'glyphicon'\" style=\"width: 16px;\" class=\"glyphicon\" [class.glyphicon-ok]=\"isSelected(option)\"></span>\n                        {{ option.name }}\n                    </a>\n                </li>\n            </ul>\n        </div>\n    ",
            styleUrls: ['css/singleselect2.css']
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef])
    ], SingleselectDropdown2);
    return SingleselectDropdown2;
}());
exports.SingleselectDropdown2 = SingleselectDropdown2;
//# sourceMappingURL=singleselect-dropdown2.js.map