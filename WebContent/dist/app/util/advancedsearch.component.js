"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var rp_references_1 = require('../framework/rp-references');
var rp_http_service_1 = require('../framework/rp-http.service');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var core_1 = require('@angular/core');
var AdvancedSearch = (function () {
    function AdvancedSearch(ics, http, ref) {
        var _this = this;
        this.ics = ics;
        this.http = http;
        this.ref = ref;
        this.rpHidden = new core_1.EventEmitter();
        this.rpChanged = new core_1.EventEmitter();
        this._SelectedList = [];
        this._RemainingList = [];
        this._TypeList = [];
        this._FilterList = [];
        this._ItemTemplate = { "itemid": "", "caption": "", "fieldname": "", "datatype": "", "condition": "", "t1": "", "t2": "", "t3": "" };
        this.subscription = this.ics.channel001$.subscribe(function (aCommand) {
            switch (aCommand.toUpperCase()) {
                case "ADD":
                    _this.addFilter();
                    break;
                case "CLEAR":
                    _this.clearFilterList();
                    break;
                case "FILTER":
                    _this.doFilter();
                    break;
                default: break;
            }
        });
    }
    Object.defineProperty(AdvancedSearch.prototype, "TypeList", {
        set: function (aTypeList) {
            if (aTypeList != undefined) {
                this._TypeList = aTypeList;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AdvancedSearch.prototype, "FilterList", {
        set: function (aFilterList) {
            if (aFilterList != undefined) {
                this._FilterList = aFilterList;
                this.loadRemainingList();
                this.addFilter();
            }
        },
        enumerable: true,
        configurable: true
    });
    AdvancedSearch.prototype.ngOnInit = function () {
    };
    AdvancedSearch.prototype.collapseSearch = function () {
        this.rpHidden.emit(false);
    };
    AdvancedSearch.prototype.isSingleType = function (aDataType) {
        var l_isSingleType = false;
        if (aDataType == "simplesearch")
            l_isSingleType = true;
        else
            l_isSingleType = this.isLovType(aDataType);
        return l_isSingleType;
    };
    AdvancedSearch.prototype.isLovType = function (aDataType) {
        var l_Prefix = "";
        var l_isLovType = false;
        if (aDataType.length >= 3) {
            l_Prefix = aDataType.substring(0, 3);
            if (l_Prefix == "lov")
                l_isLovType = true;
        }
        return l_isLovType;
    };
    AdvancedSearch.prototype.getTypeList = function (aDataType) {
        return this._TypeList[aDataType];
    };
    AdvancedSearch.prototype.isSelectedItem = function (aItemID) {
        var l_isSelectedItem = false;
        for (var iIndex = 0; iIndex < this._SelectedList.length; iIndex++) {
            if (this._SelectedList[iIndex].itemid == aItemID) {
                l_isSelectedItem = true;
            }
        }
        return l_isSelectedItem;
    };
    AdvancedSearch.prototype.setItem = function (aItemID, aItem) {
        var l_ItemIndex = -1;
        for (var iIndex = 0; iIndex < this._FilterList.length; iIndex++) {
            if (this._FilterList[iIndex].itemid == aItemID)
                l_ItemIndex = iIndex;
        }
        if (l_ItemIndex != -1) {
            aItem.itemid = this._FilterList[l_ItemIndex].itemid;
            aItem.caption = this._FilterList[l_ItemIndex].caption;
            aItem.fieldname = this._FilterList[l_ItemIndex].fieldname;
            aItem.datatype = this._FilterList[l_ItemIndex].datatype;
            aItem.condition = this._FilterList[l_ItemIndex].condition;
            aItem.t1 = this._FilterList[l_ItemIndex].t1;
            aItem.t2 = this._FilterList[l_ItemIndex].t2;
            aItem.t3 = this._FilterList[l_ItemIndex].t3;
            // Set default value
            this.setDefaultValue(aItem);
        }
    };
    AdvancedSearch.prototype.loadRemainingList = function () {
        this._RemainingList = [];
        for (var iIndex = 0; iIndex < this._FilterList.length; iIndex++) {
            this.addRemainingItem(this._FilterList[iIndex].itemid);
        }
    };
    AdvancedSearch.prototype.addRemainingItem = function (aItemID) {
        this._RemainingList[this._RemainingList.length] = aItemID;
    };
    AdvancedSearch.prototype.removeRemainingItem = function (aItemID) {
        var l_ItemIndex = -1;
        for (var iIndex = 0; iIndex < this._RemainingList.length; iIndex++) {
            if (aItemID == this._RemainingList[iIndex]) {
                l_ItemIndex = iIndex;
                break;
            }
        }
        if (l_ItemIndex != -1) {
            this._RemainingList.splice(l_ItemIndex, 1);
        }
    };
    AdvancedSearch.prototype.Row_MouseEnter = function (aItemID) {
        this._CurrentItemID = aItemID;
    };
    AdvancedSearch.prototype.Row_MouseLeave = function (aItemID) {
        this._CurrentItemID = "";
    };
    AdvancedSearch.prototype.btnRemoveFilter_MouseEnter = function (aItemID) {
        jQuery("#lblRemoveFilter_" + aItemID).css("color", "");
        jQuery("#btnRemoveFilter_" + aItemID).removeClass('btn btn-md btn-default').addClass('btn btn-md btn-danger');
    };
    AdvancedSearch.prototype.btnRemoveFilter_MouseLeave = function (aItemID) {
        jQuery("#lblRemoveFilter_" + aItemID).css("color", "#C73232");
        jQuery("#btnRemoveFilter_" + aItemID).removeClass('btn btn-md btn-danger').addClass('btn btn-md btn-default');
    };
    AdvancedSearch.prototype.addFilter = function () {
        var l_newItemID;
        var l_FilterContainer;
        if (this._RemainingList.length > 0) {
            // extend container height
            //
            if (this._SelectedList.length < this.rpVisibleItems) {
                l_FilterContainer = document.getElementById('divFilterList');
                l_FilterContainer.style.height = (l_FilterContainer.clientHeight + 32) + "px";
            }
            // Get new item id
            l_newItemID = this._RemainingList[0];
            // Reduce item from remaining list
            this.removeRemainingItem(l_newItemID);
            // Mockup item template
            var l_Item = JSON.parse(JSON.stringify(this._ItemTemplate));
            // assign values
            this.setItem(l_newItemID, l_Item);
            // extend new room and assign item
            this._SelectedList[this._SelectedList.length] = l_Item;
            // Last Item ID
            this._LastItemID = l_newItemID;
            // Check Data Type for LovType's Column Span
            this.adjustColumnSpan();
            setTimeout(function () {
                var divFilterContainer = document.getElementById("divFilterList");
                divFilterContainer.scrollTop = divFilterContainer.scrollHeight - divFilterContainer.clientHeight;
            }, 50);
        }
    };
    AdvancedSearch.prototype.removeFilter = function (aItem) {
        var l_ItemID, l_ItemIndex;
        if (this._SelectedList.length > 1) {
            l_ItemID = aItem.itemid;
            l_ItemIndex = this._SelectedList.indexOf(aItem);
            // Push back to remaining list
            this.addRemainingItem(l_ItemID);
            // Remove from selected list
            this._SelectedList.splice(l_ItemIndex, 1);
            // Last Item ID
            this._LastItemID = this._SelectedList[this._SelectedList.length - 1].itemid;
            // Check Data Type for LovType's Column Span
            this.adjustColumnSpan();
            if (this._SelectedList.length < this.rpVisibleItems) {
                var l_Height;
                if (this._SelectedList.length == 1)
                    l_Height = "32";
                else
                    l_Height = document.getElementById('divFilterList').clientHeight - 30;
                document.getElementById('divFilterList').style.height = l_Height + "px";
            }
        }
    };
    AdvancedSearch.prototype.clearFilterList = function () {
        var _this = this;
        this._SelectedList = [];
        document.getElementById('divFilterList').style.height = "0px";
        setTimeout(function () {
            _this.loadRemainingList();
            _this.addFilter();
        }, 10);
    };
    AdvancedSearch.prototype.adjustColumnSpan = function () {
        var _this = this;
        this._spanColumn = false;
        for (var iIndex = 0; iIndex < this._SelectedList.length; iIndex++) {
            if (!this.isSingleType(this._SelectedList[iIndex].datatype)) {
                this._spanColumn = true;
                break;
            }
        }
        setTimeout(function () {
            // Column Span
            //
            if (_this._spanColumn)
                jQuery(".colLovType").attr("colspan", "2");
            else
                jQuery(".colLovType").attr("colspan", "1");
            // Adjust Width
            if (_this._spanColumn)
                jQuery(".colLovType").css("width", "250px");
            else
                jQuery(".colLovType").css("width", "600px");
        }, 40);
    };
    AdvancedSearch.prototype.doFilter = function () {
        var l_IsQualified;
        var l_SelectedItems = [];
        for (var iIndex = 0; iIndex < this._SelectedList.length; iIndex++) {
            l_IsQualified = 0;
            var l_SelectedItem = { "itemid": "", "caption": "", "fieldname": "", "datatype": "", "condition": "", "t1": "", "t2": "" };
            l_SelectedItem.itemid = this._SelectedList[iIndex].itemid;
            l_SelectedItem.caption = this._SelectedList[iIndex].caption;
            l_SelectedItem.fieldname = this._SelectedList[iIndex].fieldname;
            l_SelectedItem.datatype = this._SelectedList[iIndex].datatype;
            l_SelectedItem.condition = this._SelectedList[iIndex].condition;
            l_SelectedItem.t1 = jQuery.trim(this._SelectedList[iIndex].t1);
            l_SelectedItem.t2 = jQuery.trim(this._SelectedList[iIndex].t2);
            if (l_SelectedItem.t1 != "") {
                if (l_SelectedItem.condition != "bt")
                    l_IsQualified = 1;
                else {
                    if (l_SelectedItem.t2 != "")
                        l_IsQualified = 1;
                }
            }
            if (l_IsQualified == 1)
                l_SelectedItems.push(l_SelectedItem);
        }
        if (l_SelectedItems.length > 0)
            this.rpChanged.emit(l_SelectedItems);
    };
    AdvancedSearch.prototype.getIndex = function (aItem) {
        this._ItemIndex = this._SelectedList.indexOf(aItem);
    };
    AdvancedSearch.prototype.enableBet = function (aCondition) {
        if (aCondition == "bt")
            this._SelectedList[this._ItemIndex].t3 = "true";
        else
            this._SelectedList[this._ItemIndex].t3 = "";
    };
    AdvancedSearch.prototype.switchValues = function (aSource, aDestination) {
        aDestination.t1 = aSource.t1;
        aDestination.t2 = aSource.t2;
        aDestination.t3 = aSource.t3;
        aDestination.itemid = aSource.itemid;
        aDestination.caption = aSource.caption;
        aDestination.fieldname = aSource.fieldname;
        aDestination.datatype = aSource.datatype;
        aDestination.condition = aSource.condition;
    };
    AdvancedSearch.prototype.onChange = function (aItemID) {
        var _this = this;
        var l_isSelecteditem;
        var l_Item = this._SelectedList[this._ItemIndex];
        // Mockup item template
        var l_OriginalItem = JSON.parse(JSON.stringify(this._ItemTemplate));
        // Keep Original Values
        this.switchValues(l_Item, l_OriginalItem);
        // Check selected item
        l_isSelecteditem = this.isSelectedItem(aItemID);
        if (!l_isSelecteditem) {
            setTimeout(function () {
                // Push back to remaining list
                _this.addRemainingItem(l_OriginalItem.itemid);
                // Reduce item from remaining list
                _this.removeRemainingItem(aItemID);
            }, 50);
        }
        // Set Item
        this.setItem(aItemID, l_Item);
        if (l_isSelecteditem) {
            setTimeout(function () {
                // Restore back to original values
                _this.switchValues(l_OriginalItem, l_Item);
                // Check span
                //
                if (_this._spanColumn) {
                    setTimeout(function () { jQuery(".colLovType").attr("colspan", "2"); }, 10);
                }
            }, 50);
        }
        else {
            if (this._LastItemID == l_OriginalItem.itemid)
                this._LastItemID = aItemID;
            // Check Data Type for LovType's Column Span
            this.adjustColumnSpan();
        }
    };
    AdvancedSearch.prototype.setDefaultValue = function (aItem) {
        var l_DefaultValue = "";
        if (this.isLovType(aItem.datatype)) {
            l_DefaultValue = this._TypeList[aItem.datatype][0].value;
        }
        if (aItem.t1 == "")
            aItem.t1 = l_DefaultValue;
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], AdvancedSearch.prototype, "rpVisibleItems", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], AdvancedSearch.prototype, "rpHidden", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], AdvancedSearch.prototype, "rpChanged", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object), 
        __metadata('design:paramtypes', [Object])
    ], AdvancedSearch.prototype, "TypeList", null);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object), 
        __metadata('design:paramtypes', [Object])
    ], AdvancedSearch.prototype, "FilterList", null);
    AdvancedSearch = __decorate([
        core_1.Component({
            selector: 'advanced-search',
            template: "  \n    <div>\n        <div class=\"col-md-12\" id=\"divFilterList\" style=\"overflow-y:auto; height:0px;margin-left:-10px;\">\n            <table style=\"font-size:14px;width:70%;\">\n                <tr *ngFor=\"let obj of _SelectedList\" style=\"height:32px;\" (mouseenter)=\"Row_MouseEnter(obj.itemid)\" (mouseleave)=\"Row_MouseLeave(obj.itemid)\">        \n                    <td style=\"width:250px;\">\n                        <select [(ngModel)]=\"obj.itemid\" (click)=\"getIndex(obj)\" (change)=\"onChange($event.target.value)\" class=\"form-control input-sm\">\n                            <option *ngFor=\"let item of _FilterList\" value=\"{{item.itemid}}\">{{item.caption}}</option>\n                        </select>\n                    </td>\n\n                    <td *ngIf=\"!isSingleType(obj.datatype)\" style=\"width:250px;padding-left:3px;\">\n                        <select *ngIf=\"obj.datatype=='string'\" [(ngModel)]=\"obj.condition\" class=\"form-control input-sm\">\n                            <option *ngFor=\"let item of ref._lov1.string\" value=\"{{item.value}}\" >{{item.caption}}</option>\n                        </select>\n\n                        <select *ngIf=\"obj.datatype=='numeric'\" (click)=\"getIndex(obj)\" (change)=\"enableBet($event.target.value)\" [(ngModel)]=\"obj.condition\" class=\"form-control input-sm\">\n                            <option *ngFor=\"let item of ref._lov1.numeric\" value=\"{{item.value}}\" >{{item.caption}}</option>\n                        </select>\n\n                        <select *ngIf=\"obj.datatype=='date'\" (click)=\"getIndex(obj)\" (change)=\"enableBet($event.target.value)\" [(ngModel)]=\"obj.condition\" class=\"form-control input-sm\">\n                            <option *ngFor=\"let item of ref._lov1.date\" value=\"{{item.value}}\" >{{item.caption}}</option>\n                        </select>\n                    </td>\n\n                    <td *ngIf=\"obj.datatype=='simplesearch'\" class=\"colLovType\" style=\"width:250px;padding-left:3px;\">\n                        <input id=\"textinput\" type=\"text\" autocomplete=\"off\" spellcheck=\"false\" [(ngModel)]=\"obj.t1\" class=\"form-control input-sm\" (keyup.enter)=\"doFilter()\">\n                    </td>\n                    \n                    <td *ngIf=\"isLovType(obj.datatype)\" class=\"colLovType\" style=\"width:250px;padding-left:3px;\">\n                        <select *ngIf=\"isLovType(obj.datatype)\" [(ngModel)]=\"obj.t1\" class=\"form-control input-sm\">\n                            <option *ngFor=\"let item of getTypeList(obj.datatype)\" value=\"{{item.value}}\">{{item.caption}}</option>\n                        </select>\n                    </td>\n\n                    <td *ngIf=\"!isSingleType(obj.datatype)\" style=\"width:350px;padding-left:3px;\">\n                        <input id=\"textinput\" *ngIf=\"obj.datatype=='string' && obj.t3!='true'\" type=\"text\" autocomplete=\"off\" spellcheck=\"false\" [(ngModel)]=\"obj.t1\" class=\"form-control input-sm\" (keyup.enter)=\"doFilter()\">\n                        <input id=\"textinput\" *ngIf=\"obj.datatype=='numeric' && obj.t3!='true'\" type=\"number\" autocomplete=\"off\" spellcheck=\"false\" [(ngModel)]=\"obj.t1\" class=\"form-control input-sm\" (keyup.enter)=\"doFilter()\">\n                    \n                        <div *ngIf=\"obj.datatype=='numeric' && obj.t3=='true'\" class=\"input-group\">\n                            <input id=\"textinput\" *ngIf=\"obj.datatype=='numeric' && obj.t3=='true'\" type=\"number\" autocomplete=\"off\" spellcheck=\"false\" [(ngModel)]=\"obj.t1\" class=\"form-control input-sm\" style=\"width: 50%\" (keyup.enter)=\"doFilter()\"> \n                            <input id=\"textinput\" *ngIf=\"obj.datatype=='numeric' && obj.t3=='true'\" type=\"number\" autocomplete=\"off\" spellcheck=\"false\" [(ngModel)]=\"obj.t2\" class=\"form-control input-sm\" style=\"width: 50%\" (keyup.enter)=\"doFilter()\"> \n                        </div>\n\n                        <input id=\"textinput\" *ngIf=\"obj.datatype=='date' && obj.t3!='true'\" type=\"date\" autocomplete=\"off\" spellcheck=\"false\" [(ngModel)]=\"obj.t1\" class=\"form-control input-sm\" (keyup.enter)=\"doFilter()\"> \n\n                        <div *ngIf=\"obj.datatype=='date' && obj.t3=='true'\" class=\"input-group\">\n                            <input id=\"textinput\" *ngIf=\"obj.datatype=='date' && obj.t3=='true'\" type=\"date\" [(ngModel)]=\"obj.t1\" class=\"form-control input-sm\" style=\"width: 50%\" (keyup.enter)=\"doFilter()\"> \n                            <input id=\"textinput\" *ngIf=\"obj.datatype=='date' && obj.t3=='true'\" type=\"date\" [(ngModel)]=\"obj.t2\" class=\"form-control input-sm\" style=\"width: 50%\" (keyup.enter)=\"doFilter()\"> \n                        </div>\n                    </td>\n\n                    <td style=\"width:300px;\" title=\"Remove Filter\">\n                        <button id=\"btnDoFilter_{{obj.itemid}}\" type=\"button\" class=\"btn btn-md btn-default\" style=\"cursor:pointer;background-color:#EEEEEE;text-align:center;margin-left:1px;width:34px;height:30px;\" (click)=\"doFilter(obj)\" title=\"Filter\">\n                            <i id=\"lblDoFilter_{{obj.itemid}}\" style=\"color:#646464;margin-left:-3px;margin-top:-1px;\" class=\"glyphicon glyphicon-search\"></i>\n                        </button>\n\n                        <button id=\"btnRemoveFilter_{{obj.itemid}}\" type=\"button\" class=\"btn btn-md btn-default\" style=\"cursor:pointer;text-align:center;margin-left:8px;width:34px;height:30px;\" (mouseenter)=\"btnRemoveFilter_MouseEnter(obj.itemid)\" (mouseleave)=\"btnRemoveFilter_MouseLeave(obj.itemid)\" (click)=\"removeFilter(obj)\" title=\"Remove Filter\">\n                            <i id=\"lblRemoveFilter_{{obj.itemid}}\" style=\"font-size:120%;font-weight:bold;color:#C73232;margin-left:-4px;margin-top:-1px;\" class=\"glyphicon glyphicon-remove-sign\"></i>\n                        </button>\n\n                        <button *ngIf=\"obj.itemid==_LastItemID\" id=\"btnAddFilter_{{obj.itemid}}\" type=\"button\" class=\"btn btn-md btn-default\" style=\"cursor:pointer;text-align:center;color:#2F4F4F;width:80px;height:30px;padding-top:2px;padding-left:5px;margin-left:1px;margin-top:1px;\" (click)=\"addFilter()\" title=\"Add Filter\">\n                            <i id=\"lblAddFilter_{{obj.itemid}}\" style=\"color:#2F4F4F;font-size:120%;font-weight:bold;margin-left:-4px;margin-right:5px;margin-top:3px;\" class=\"glyphicon glyphicon-plus-sign\"></i><i style=\"color:#2F4F4F;font-style:normal;\">More</i>\n                        </button>\n                    </td>\n                </tr>\n            </table>\n        </div>\n\n        <div *ngIf=\"true\" class=\"col-md-12\" style=\"height:15px;\">&nbsp;</div>\n    </div>\n",
            providers: [rp_http_service_1.RpHttpService]
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, rp_http_service_1.RpHttpService, rp_references_1.RpReferences])
    ], AdvancedSearch);
    return AdvancedSearch;
}());
exports.AdvancedSearch = AdvancedSearch;
//# sourceMappingURL=advancedsearch.component.js.map