"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
core_1.enableProdMode();
var CustomBrowse = (function () {
    function CustomBrowse(ics) {
        this.ics = ics;
        this.cuBrowse = new core_1.EventEmitter();
        this.subscription = ics.rpbean$.subscribe(function (x) { });
    }
    CustomBrowse.prototype.uploadedFile = function (event) {
        if (event.target.files.length == 1) {
            this.uploadedFileName = event.target.files[0].name;
            this.uploadFile = event.target.files[0];
            var data = { "fileName": this.uploadedFileName, "file": this.uploadFile };
            this.cuBrowse.emit(data);
        }
        else {
            this.ics.sendBean({ "t1": "rp-alert", "t2": "info", "t3": "Upload File!" });
        }
    };
    CustomBrowse.prototype.resetValue = function () {
        this.fileInput.nativeElement.value = "";
        this.uploadedFileName = "";
    };
    __decorate([
        core_1.ViewChild('file'), 
        __metadata('design:type', core_1.ElementRef)
    ], CustomBrowse.prototype, "fileInput", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], CustomBrowse.prototype, "cuBrowse", void 0);
    CustomBrowse = __decorate([
        core_1.Component({
            selector: 'custom-browse',
            template: "  \n    <input type=\"file\" (click)=\"resetValue()\" (change)=\"uploadedFile($event)\" #file  style=\"visibility:hidden; width: 2px;\"/>\n    <div class=\"input-group stylish-input-group\" style=\"width: 300px;\">\n        <input type=\"text\" class=\"form-control input-sm\" [(ngModel)]=\"uploadedFileName\" (click)=\"file.click()\"  class=\"form-control input-sm\" style=\"width: 230px;height:34px\">\n        <span class=\"input-group-btn input-sm\"> \n            <button type=\"button\" class=\"btn btn-primary input-sm\" (click)=\"file.click()\">\n                 <span class=\"glyphicon glyphicon-folder-open\"></span> Browse\n            </button>\n        </span>\n    </div>\n    ",
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService])
    ], CustomBrowse);
    return CustomBrowse;
}());
exports.CustomBrowse = CustomBrowse;
//# sourceMappingURL=custom-browse.component.js.map