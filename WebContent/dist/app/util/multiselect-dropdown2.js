/*
 * Angular 2 Dropdown Multiselect for Bootstrap
 * Current version: 0.1.0
 *
 * Simon Lindh
 * https://github.com/softsimon/angular-2-dropdown-multiselect
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var MultiselectDropdown2 = (function () {
    function MultiselectDropdown2(element) {
        this.element = element;
        this.selectedItemChange = new core_1.EventEmitter();
        //@Input() rpReadonly : string;
        this.selectedModel = [];
        this.model = new core_1.EventEmitter();
        this.selectionLimitReached = new core_1.EventEmitter();
        this.checkallflag = false;
        this.search = new forms_1.FormControl();
        this.numSelected = 0;
        this.isVisible = false;
        this.searchFilterText = '';
        this.defaultSettings = {
            pullRight: false,
            enableSearch: false,
            checkedStyle: 'checkboxes',
            buttonClasses: 'btn btn-default',
            selectionLimit: 9,
            closeOnSelect: false,
            showCheckAll: false,
            showUncheckAll: false,
            dynamicTitleMaxItems: 3,
            maxHeight: '300px',
        };
        this.defaultTexts = {
            checkAll: 'Check all',
            uncheckAll: 'Uncheck all',
            checked: 'selected',
            checkedPlural: 'selected',
            searchPlaceholder: 'Search...',
            defaultTitle: 'Select',
        };
    }
    MultiselectDropdown2.prototype.onClick = function (target) {
        var parentFound = false;
        while (target !== null && !parentFound) {
            if (target === this.element.nativeElement) {
                parentFound = true;
            }
            target = target.parentElement;
        }
        if (!parentFound) {
            this.isVisible = false;
        }
    };
    MultiselectDropdown2.prototype.ngOnInit = function () {
        var _this = this;
        this.settings = Object.assign(this.defaultSettings, this.settings);
        this.texts = Object.assign(this.defaultTexts, this.texts);
        this.updateNumSelected();
        this.search.valueChanges
            .subscribe(function (text) {
            _this.searchFilterText = text;
        });
    };
    MultiselectDropdown2.prototype.clearSearch = function () {
        this.search.setValue('');
    };
    MultiselectDropdown2.prototype.toggleDropdown = function () {
        this.search.setValue('');
        this.isVisible = !this.isVisible;
    };
    MultiselectDropdown2.prototype.modelChanged = function () {
        this.updateNumSelected();
        this.model.emit(this.selectedModel);
        this.selectedItemChange.emit(this.selectedItem);
    };
    MultiselectDropdown2.prototype.isSelected = function (option) {
        return this.selectedModel.indexOf(parseInt(option.id)) > -1;
    };
    MultiselectDropdown2.prototype.setSelected = function (id) {
        var index = this.selectedModel.indexOf(parseInt(id));
        if (index > -1) {
            this.selectedModel.splice(index, 1);
            this.selectedItem = "";
            if (this.selectedModel != null && this.selectedModel.length > 0) {
                for (var i = 0; i < this.selectedModel.length; i++) {
                    var val = "";
                    for (var j = 0; j < this.options.length; j++) {
                        if (this.selectedModel[i] == Number(this.options[j].id)) {
                            val = this.options[j].name;
                            break;
                        }
                    }
                    if (val != "") {
                        if (this.selectedItem == "") {
                            this.selectedItem = val;
                        }
                        else {
                            this.selectedItem += "," + val;
                        }
                    }
                }
            }
            this.checkallflag = false;
        }
        else {
            if (this.settings.selectionLimit === 0 || this.selectedModel.length < this.settings.selectionLimit) {
                this.selectedModel.push(parseInt(id));
                this.selectedItem = "";
                if (this.selectedModel != null && this.selectedModel.length > 0) {
                    for (var i = 0; i < this.selectedModel.length; i++) {
                        var val = "";
                        for (var j = 0; j < this.options.length; j++) {
                            if (this.selectedModel[i] == Number(this.options[j].id)) {
                                val = this.options[j].name;
                                break;
                            }
                        }
                        if (val != "") {
                            if (this.selectedItem == "") {
                                this.selectedItem = val;
                            }
                            else {
                                this.selectedItem += "," + val;
                            }
                        }
                    }
                }
            }
            else {
                this.selectionLimitReached.emit(this.selectedModel.length);
                return;
            }
        }
        if (this.settings.closeOnSelect) {
            this.toggleDropdown();
        }
        this.modelChanged();
    };
    MultiselectDropdown2.prototype.getTitle = function () {
        var _this = this;
        if (this.numSelected === 0) {
            return this.texts.defaultTitle;
        }
        if (this.settings.dynamicTitleMaxItems >= this.numSelected) {
            return this.options
                .filter(function (option) { return _this.selectedModel.indexOf(parseInt(option.id)) > -1; })
                .map(function (option) { return option.name; })
                .join(', ');
        }
        return this.numSelected + ' ' + (this.numSelected === 1 ? this.texts.checked : this.texts.checkedPlural);
    };
    MultiselectDropdown2.prototype.updateNumSelected = function () {
        this.numSelected = this.selectedModel.length;
    };
    MultiselectDropdown2.prototype.updateData = function (event) {
        this.selectedItem = event;
        this.selectedItemChange.emit(event);
    };
    MultiselectDropdown2.prototype.checkAll = function () {
        this.selectedModel = this.options.map(function (option) { return parseInt(option.id); });
        this.modelChanged();
    };
    MultiselectDropdown2.prototype.uncheckAll = function () {
        this.selectedModel = [];
        this.modelChanged();
    };
    MultiselectDropdown2.prototype.isAll = function () {
        if (this.options.length == this.selectedModel.length)
            return true;
        return false;
    };
    MultiselectDropdown2.prototype.checkuncheckAll = function (event) {
        if (event.target.checked) {
            if (this.options != null && this.options.length > 0) {
                for (var i = 0; i < this.options.length; i++) {
                    // if (this.settings.selectionLimit === 0 || this.selectedModel.length < this.settings.selectionLimit) {
                    if (this.selectedModel.indexOf(parseInt(this.options[i].id)) < 0) {
                        this.selectedModel.push(parseInt(this.options[i].id));
                        this.selectedItem = "";
                        if (this.selectedModel != null && this.selectedModel.length > 0) {
                            for (var i_1 = 0; i_1 < this.selectedModel.length; i_1++) {
                                var val = "";
                                for (var j = 0; j < this.options.length; j++) {
                                    if (this.selectedModel[i_1] == Number(this.options[j].id)) {
                                        val = this.options[j].name;
                                        break;
                                    }
                                }
                                if (val != "") {
                                    if (this.selectedItem == "") {
                                        this.selectedItem = val;
                                    }
                                    else {
                                        this.selectedItem += "," + val;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else {
            if (this.options != null && this.options.length > 0) {
                for (var i = 0; i < this.options.length; i++) {
                    if (this.selectedModel.indexOf(parseInt(this.options[i].id)) > -1) {
                        this.selectedModel.splice(this.selectedModel.indexOf(parseInt(this.options[i].id)), 1);
                        this.selectedItem = "";
                        if (this.selectedModel != null && this.selectedModel.length > 0) {
                            for (var i_2 = 0; i_2 < this.selectedModel.length; i_2++) {
                                var val = "";
                                for (var j = 0; j < this.options.length; j++) {
                                    if (this.selectedModel[i_2] == Number(this.options[j].id)) {
                                        val = this.options[j].name;
                                        break;
                                    }
                                }
                                if (val != "") {
                                    if (this.selectedItem == "") {
                                        this.selectedItem = val;
                                    }
                                    else {
                                        this.selectedItem += "," + val;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if (this.settings.closeOnSelect) {
            this.toggleDropdown();
        }
        this.modelChanged();
    };
    MultiselectDropdown2.prototype.getFullName = function (str) {
        if (str != undefined && str != null) {
            if (localStorage.getItem("systemfont") != "uni") {
                if (isMyanmar(str))
                    str = ZgtoUni(str);
            }
            return str;
        }
        else {
            return '';
        }
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], MultiselectDropdown2.prototype, "options", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], MultiselectDropdown2.prototype, "settings", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], MultiselectDropdown2.prototype, "texts", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], MultiselectDropdown2.prototype, "selectedItemChange", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], MultiselectDropdown2.prototype, "selectedItem", void 0);
    __decorate([
        core_1.Input('defaultModel'), 
        __metadata('design:type', Array)
    ], MultiselectDropdown2.prototype, "selectedModel", void 0);
    __decorate([
        core_1.Output('selectedModel'), 
        __metadata('design:type', Object)
    ], MultiselectDropdown2.prototype, "model", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], MultiselectDropdown2.prototype, "selectionLimitReached", void 0);
    __decorate([
        core_1.HostListener('document: click', ['$event.target']), 
        __metadata('design:type', Function), 
        __metadata('design:paramtypes', [Object]), 
        __metadata('design:returntype', void 0)
    ], MultiselectDropdown2.prototype, "onClick", null);
    MultiselectDropdown2 = __decorate([
        core_1.Component({
            selector: 'ss-multiselect-dropdown2',
            styles: ["\n\t\ta { outline: none; }\n\t"],
            template: "\n        <div class=\"btn-group\">\n        <input id=\"button\" type=\"button\" class=\"dropdown-toggle btn col-md-12\" style=\"min-height:32px;\" [ngClass]=\"settings.buttonClasses\" [(ngModel)]=\"selectedItem\" [ngModelOptions]=\"{standalone: true}\" (ngModelChange)=\"updateData($event)\" (click)=\"toggleDropdown()\" title=\"{{getFullName(getTitle())}}\"><span class=\"pull-right\"><span class=\"caret\"></span></span>\n         <!--   <button id=\"button\" type=\"button\" class=\"dropdown-toggle btn\" style=\"min-height:32px;min-width:360px;\" [ngClass]=\"settings.buttonClasses\" (click)=\"toggleDropdown()\" title=\"{{getFullName(getTitle())}}\" >{{ getTitle() }}<span class=\"pull-right\"><span  class=\"caret\"></span></span></button> -->\n            <ul *ngIf=\"isVisible\" class=\"dropdown-menu\" [class.pull-right]=\"settings.pullRight\" [style.max-height]=\"settings.maxHeight\" style=\"display: block; height: auto; overflow-y: auto;\">\n                <li style=\"margin: 0px 5px 5px 5px;\" *ngIf=\"settings.enableSearch\">\n                    <div class=\"input-group input-group-sm\">\n                        <span class=\"input-group-addon\" id=\"sizing-addon3\"><i class=\"fa fa-search\"></i></span>\n                        <input type=\"text\" class=\"form-control\"  placeholder=\"{{ texts.searchPlaceholder }}\" aria-describedby=\"sizing-addon3\" [formControl]=\"search\">\n                        <span class=\"input-group-btn\" *ngIf=\"searchFilterText.length > 0\">\n                            <button class=\"btn btn-default\" type=\"button\" (click)=\"clearSearch()\"><i class=\"fa fa-times\"></i></button>\n                        </span>\n                    </div>\n                </li>\n                <li class=\"divider\" *ngIf=\"settings.enableSearch\"></li>\n                <li *ngIf=\"settings.showCheckAll\">\n                    <a role=\"menuitem\" tabindex=\"-1\" (click)=\"checkAll()\">\n                        <span style=\"width: 16px;\" class=\"glyphicon glyphicon-ok\"></span>\n                        {{ texts.checkAll }}\n                    </a>\n                </li>\n                <li *ngIf=\"settings.showUncheckAll\">\n                    <a role=\"menuitem\" tabindex=\"-1\" (click)=\"uncheckAll()\">\n                        <span style=\"width: 16px;\" class=\"glyphicon glyphicon-remove\"></span>\n                        {{ texts.uncheckAll }}\n                    </a>\n                </li>\n                <li >\n                    <a  role=\"menuitem\" tabindex=\"-1\" >\n                         <input *ngIf=\"settings.checkedStyle == 'checkboxes'\" type=\"checkbox\" [(ngModel)]=\"checkallflag\" name=\"checkallflag\" [ngModelOptions]='{standalone: true}'  (click)=\"checkuncheckAll($event)\" [checked]=\"isAll()\"/>\n                        <span *ngIf=\"settings.checkedStyle == 'glyphicon'\" style=\"width: 16px;\" class=\"glyphicon\"></span>\n                        ALL\n                    </a>\n                </li>\n                <li *ngIf=\"settings.showCheckAll || settings.showUncheckAll\" class=\"divider\"></li>\n                <li *ngFor=\"let option of options | searchFilter:searchFilterText\" title=\"{{getFullName(option.name)}}\">\n                    <a  role=\"menuitem\" tabindex=\"-1\" (click)=\"setSelected(option.id)\" class=\"singleselectwrap\">\n                        <input *ngIf=\"settings.checkedStyle == 'checkboxes'\" type=\"checkbox\" [checked]=\"isSelected(option)\" />\n                        <span *ngIf=\"settings.checkedStyle == 'glyphicon'\" style=\"width: 16px;\" class=\"glyphicon\" [class.glyphicon-ok]=\"isSelected(option)\"></span>\n                        {{ option.name }}\n                    </a>\n                </li>\n            </ul>\n        </div>\n    ",
            styleUrls: ['css/multiselect2.css']
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef])
    ], MultiselectDropdown2);
    return MultiselectDropdown2;
}());
exports.MultiselectDropdown2 = MultiselectDropdown2;
//# sourceMappingURL=multiselect-dropdown2.js.map