"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var rp_intercom_service_1 = require('../framework/rp-intercom.service');
var rp_http_service_1 = require('../framework/rp-http.service');
var Pager6 = (function () {
    function Pager6(ics, http) {
        this.ics = ics;
        this.http = http;
        this.rpModelChange = new core_1.EventEmitter();
        this.rpChanged = new core_1.EventEmitter();
        this._pages = [];
        this._sizes = [];
        this.currentPage = 1;
        this.prev = 1;
        this.last = 1;
        this.next = 2;
        this.start = 1;
        this.end = 10;
        this.pageSize = 10;
        this.totalCount = 1;
        this._flag = false;
        this._obj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 10, "size": 10, "totalcount": 1 };
        if (this.rpId == null || this.rpId == "")
            this.rpId = "myid";
        if (this.rpClass == null || this.rpClass == "")
            this.rpClass = "col-md-3";
        if (this.rpPageSize == null || this.rpPageSize == "")
            this.rpPageSize = "10";
        if (this.rpPageSizeMax == null || this.rpPageSizeMax == 0)
            this.rpPageSizeMax = 100;
        if (this.rpModel == null || this.rpModel < 0)
            this.rpModel = 1;
        if (this.rpCurrentPage == null || this.rpCurrentPage < 0)
            this.rpCurrentPage = 1;
    }
    Pager6.prototype.ngOnInit = function () {
        jQuery("#lblFirst").css("height", "34px");
        jQuery("#lblPrevious").css("height", "34px");
        jQuery("#lblNext").css("height", "34px");
        jQuery("#lblLast").css("height", "34px");
    };
    Pager6.prototype.ngAfterContentInit = function () {
        this.totalCount = +this.rpModel;
        this.last = Math.ceil(this.totalCount / this.pageSize);
        this.fillPageSizes(this.rpPageSizeMax);
        this.fillPages();
    };
    Pager6.prototype.ngOnChanges = function (rpModel) {
        this.totalCount = +this.rpModel;
        if (this.rpCurrentPage != null && this.rpCurrentPage > 0)
            this.currentPage = +this.rpCurrentPage;
        else
            this.currentPage = 1;
        if (this.rpPageSize != null && this.rpPageSize > "10")
            this.pageSize = +this.rpPageSize;
        else
            this.pageSize = 10;
        this.end = 10;
        this.last = Math.ceil(this.totalCount / this.pageSize);
        this.fillPages();
        this.updatePager(this.currentPage);
    };
    Pager6.prototype.initializePager = function (num) {
        this._obj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 1, "size": 10, "totalcount": this.totalCount };
    };
    Pager6.prototype.updatePager = function (k) {
        var num = +k;
        this.currentPage = num;
        this.prev = num > 1 ? num - 1 : 1;
        this.next = (num + 1) < this.last ? (num + 1) : this.last;
        this.start = this.totalCount < 1 ? this.totalCount : ((num - 1) * this.pageSize) + 1;
        this.end = (num * this.pageSize) > this.totalCount ? this.totalCount : (num * this.pageSize);
        this._obj =
            {
                "current": this.currentPage, "prev": this.prev, "last": this.last, "next": this.next, "start": this.start, "end": this.end, "size": this.pageSize, "totalcount": this.totalCount
            };
        var data = { "obj": this._obj, "flag": this._flag };
        this.rpChanged.emit(data);
        this._flag = false;
    };
    Pager6.prototype.fillPageSizes = function (pg) {
        var size = +pg;
        this._obj.current = 1;
        var total = size / 10;
        for (var i = 1; i <= total; i++) {
            var k = { "value": 0, "caption": "" };
            k.value = i * 10;
            k.caption = "" + (i * 10);
            this._sizes.push(k);
        }
    };
    Pager6.prototype.fillPages = function () {
        this._pages = [];
        this._obj = { "current": 1, "prev": 1, "last": 1, "next": 2, "start": 1, "end": 1, "size": 10, "totalcount": this.totalCount };
        var num = Math.ceil(this.totalCount / this.pageSize);
        for (var i = 1; i <= num; i++) {
            var k = { "value": 0, "caption": "" };
            k.value = i;
            k.caption = "" + i;
            this._pages.push(k);
        }
    };
    Pager6.prototype.goFirst = function () {
        this._flag = true;
        this.currentPage = 1;
        this.updatePager(this.currentPage);
    };
    Pager6.prototype.goLast = function () {
        this._flag = true;
        this.currentPage = Math.ceil(this.totalCount / this.pageSize);
        this.updatePager(this.currentPage);
    };
    Pager6.prototype.goPrev = function () {
        this._flag = true;
        var k = this.prev;
        this.updatePager(k);
    };
    Pager6.prototype.goNext = function () {
        this._flag = true;
        var k = this.next;
        this.updatePager(k);
    };
    Pager6.prototype.changRows = function () {
        this.fillPages();
        this.last = Math.ceil(this.totalCount / this.pageSize);
        this.updatePager(1);
    };
    Pager6.prototype.changPage = function () {
        this._flag = true;
        this.updatePager(this.currentPage);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], Pager6.prototype, "rpId", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], Pager6.prototype, "rpClass", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], Pager6.prototype, "rpPageSize", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], Pager6.prototype, "rpPageSizeMax", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], Pager6.prototype, "rpModel", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], Pager6.prototype, "rpCurrentPage", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], Pager6.prototype, "rpModelChange", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], Pager6.prototype, "rpChanged", void 0);
    Pager6 = __decorate([
        core_1.Component({
            selector: 'pager6',
            template: "  \n    <div>\n        <nav>\n            <ul class=\"pagination\" style=\"margin:0px;\">\n                <li class=\"page-item\" title=\"First\">\n                    <a class=\"page-link\" style=\"height:34px;\" (click)=\"goFirst()\"><i class=\"glyphicon glyphicon-fast-backward\" style=\"margin-top:2px;\"></i></a>\n                </li>\n                \n                <li class=\"page-item\" title=\"Previous\">\n                    <a class=\"page-link\" style=\"height:34px;\" (click)=\"goPrev()\"><i class=\"glyphicon glyphicon-backward\" style=\"margin-top:2px;\"></i></a>\n                </li>\n\n                <li class=\"page-item\" title=\"Page No\">\n                    <span id=\"lblPageNo\" class=\"page-link\" style=\"height:34px;\">\n                        <select [(ngModel)]=\"currentPage\" class=\"page-item\" (ngModelChange)=\"changPage()\" style=\"min-width:50px;margin-top:-5px\">\n                            <option *ngFor=\"let item of _pages\" value=\"{{item.value}}\" >{{item.caption}}</option>\n                        </select>\n                    </span>\n                </li>\n\n                <li class=\"page-item\" title=\"Next\"> \n                    <a class=\"page-link\" style=\"height:34px;\" (click)=\"goNext()\"><i class=\"glyphicon glyphicon-forward\" style=\"margin-top:2px;\"></i></a> \n                </li>\n                \n                <li class=\"page-item\" title=\"Last\">\n                    <a class=\"page-link\" style=\"height:34px;\" (click)=\"goLast()\"><i class=\"glyphicon glyphicon-fast-forward\" style=\"margin-top:2px;\"></i></a>\n                </li>\n\n                <li class=\"page-item active\" title=\"Page Size\">\n                    <span class=\"page-link\" style=\"background:#337ab7;border-color:#337ab7;height:34px;\">\n                        <select [(ngModel)]=\"pageSize\" class=\"page-item text-primary\" style=\"margin-top:-5px;\" (ngModelChange)=\"changRows()\">\n                            <option class=\"text-primary\" *ngFor=\"let item of _sizes\" value=\"{{item.value}}\" >{{item.caption}}</option>\n                        </select>\n                    </span>\n                </li>\n            </ul>\n        </nav>\n    </div>\n    ",
            providers: [rp_http_service_1.RpHttpService]
        }), 
        __metadata('design:paramtypes', [rp_intercom_service_1.RpIntercomService, rp_http_service_1.RpHttpService])
    ], Pager6);
    return Pager6;
}());
exports.Pager6 = Pager6;
//# sourceMappingURL=pager6.component.js.map